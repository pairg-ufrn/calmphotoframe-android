package br.ufrn.dimap.pairg.calmphotoframe.test;


import android.support.test.rule.ActivityTestRule;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;

import static org.mockito.Mockito.mock;


public class PhotoCollectionActivity_InitializationTest {

    @Rule
    public ActivityTestRule<PhotoCollectionActivity> mActivityRule = new ActivityTestRule<>(
            PhotoCollectionActivity.class);
    static ApiClient mockClient;

    @BeforeClass
    public static void setupTestSuite() {
        mockClient = mock(ApiClient.class);
        ApiClient.setInstance(mockClient);
    }

    @AfterClass
    public static void tearDownSuite(){
        ApiClient.setInstance(null);
    }


    @Test
    public void get_forbidden_photocontext() {
        given_that_this_user_has_no_permission_on_the_current_context_anymore();

        when_trying_to_initialize_the_collections_activity();

        then_an_notification_error_should_appear();

        then_it_should_try_get_the_default_photocontext();
    }


    private void given_that_this_user_has_no_permission_on_the_current_context_anymore() {
//        when(mockClient.getCurrentContext()).thenReturn(1L);
//        when(mockClient.contexts().get(eq(1L), Matchers.<ResultListener<? super PhotoContext>>any())).then(new Answer<Object>() {
//            @Override
//            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
//                ResultListener<?> listener = (ResultListener<?>)invocationOnMock.getArguments()[1];
//
//                ResultListenerHelper.onFailure(listener, new ApiException("", new Throwable(), 403));
//
//                return new FinishedRequestHandler();
//            }
//        });
    }

    private void when_trying_to_initialize_the_collections_activity() {

    }

    private void then_an_notification_error_should_appear() {

    }

    private void then_it_should_try_get_the_default_photocontext() {

    }
}
