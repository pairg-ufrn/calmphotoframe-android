package br.ufrn.dimap.pairg.calmphotoframe.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import br.ufrn.dimap.pairg.calmphotoframe.utils.MemoryUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils;

import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class ImageSizeLimitsTest{
    private Context context;

	@Before
	public void setUp() {
		MemoryUtils.setInstance(new MemoryUtils(){
			@Override
			public long getMaxAvailableMemory() {
				return 1000*10;
			}
		});

        Context instrumentationContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        context= new RenamingDelegatingContext(instrumentationContext, "test_");
	}

    @Test
	public void test_maxImageByteLength_shouldNotBeGreaterThanTheApplicationMemory(){
		long estimatedSize = getMaxBitmapByteLength();
		
		long appMemory = MemoryUtils.maxAvailableMemory();

        Assert.assertThat(estimatedSize, lessThan(appMemory));
	}

    @Test
	public void test_maxImageByteLength_shouldNotBeGreaterThan_HalfOfTheDefaultMaxCacheSize(){
		long estimatedSize = getMaxBitmapByteLength();
		
		long cacheSize = MemoryUtils.defaultImageCacheSize();

        Assert.assertThat(estimatedSize, lessThanOrEqualTo(cacheSize / 2));
	}

	private int getMaxBitmapByteLength() {
		int[] maxImageSize = ImageUtils.instance().getDefaultMaxImageSize(context);
		
		Log.d(getClass().getSimpleName(), "Max image size : " + Arrays.toString(maxImageSize));
		
		return ImageUtils.instance().estimateImageSize(maxImageSize);
	}
}
