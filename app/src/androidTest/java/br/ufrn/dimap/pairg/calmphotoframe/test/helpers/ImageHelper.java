package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;

public class ImageHelper {
	
	public static InputStream openImage(Context context, String filename) throws IOException {
		AssetManager assetManager = context.getResources().getAssets();
		return assetManager.open(filename);
	}
}
