package br.ufrn.dimap.pairg.calmphotoframe.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import android.graphics.Bitmap;
import android.test.InstrumentationTestCase;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ImageHelper;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.BitmapDecoder;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.BitmapDecoders;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.FitInsideDimensionsCalculator;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils.ReduceOptions;

public class LoadReducedBitmapTest extends InstrumentationTestCase{

	private static final String LOG_TAG = LoadReducedBitmapTest.class.getSimpleName();
	private static final String TEST_IMAGE 		= "test_image.jpg";

	public void test_loadReducedImageShould_NotLoadAnImageBiggerThanTheMaxSize() throws IOException{
		InputStream stream = givenSomeImageStream();
		
		Bitmap result = whenLoadingAnReducedBitmap(from(stream), with(wantedDimensions()), true);
		
		assertThat_bitmapDimensionsShouldBeSmallerThan(result, wantedDimensions());		
	}
	
	public void test_loadReducedImage_shouldDecodeAnImageSmallerThanTheMaxSize_ifGiven() throws IOException{
		InputStream stream = givenSomeImageStream();
		
		int[] maxSize = scale(wantedDimensions(), 1.2f);
		
		Bitmap result = whenLoadingAnReducedBitmap(from(stream), with(wantedDimensions()), false, maxSize);
		
		assertThat_bitmapDimensionsShouldBeSmallerThan(result, wantedDimensions());		
	}

	public void test_calculateInSampleSize_shouldNotExceedMaxImageSize_whenGiven(){
		int[] wantedSize = givenAWantedImageSize();
		int[] imageSize = givenSomeImageSize();
		int[] maxImageSize = givenAMaxImageSize();
		
		int inSampleSize = ImageUtils.instance().calculateInSampleSize(imageSize, wantedSize, maxImageSize);
	
		Log.d(LOG_TAG, "in sample size: " + inSampleSize);
		
		assertThat_resultantSizeShouldNotBeGreaterThanMaxSize(inSampleSize, imageSize, maxImageSize);
	}
	

	private void assertThat_resultantSizeShouldNotBeGreaterThanMaxSize(int inSampleSize, int[] imageSize,
			int[] maxImageSize) {
		int[] resultantSize = getSampledDownSize(inSampleSize, imageSize);
		
		Log.d(LOG_TAG, "Resultant size: " + Arrays.toString(resultantSize));
		

		assertIsLessOrEqual(resultantSize[0], maxImageSize[0]);
		assertIsLessOrEqual(resultantSize[1], maxImageSize[1]);
	}

	private int[] getSampledDownSize(int inSampleSize, int[] imageSize) {
		int[] resultantSize = new int[]{imageSize[0], imageSize[1]};
		for(int i=1; i < inSampleSize; i*=2){
			resultantSize[0] *= 0.5f;
			resultantSize[1] *= 0.5f;
		}
		return resultantSize;
	}

	private int[] givenSomeImageSize() {
		return new int[]{1200, 1600};
	}

	private int[] givenAWantedImageSize() {
		return new int[]{1000, 1000};
	}
	private int[] givenAMaxImageSize() {
		return new int[]{1024, 1024};
	}


	private int[] scale(int[] arr, float scl) {
		int[] result = new int[arr.length];
		for(int i=0; i < arr.length; ++i){
			result[i] = (int) (arr[i] * scl);
		}
		return result;
	}
	
	private void assertThat_bitmapDimensionsShouldBeSmallerThan(Bitmap img, int[] maxDimensions) {
		assertIsLessOrEqual(img.getWidth(), maxDimensions[0]);
		assertIsLessOrEqual(img.getHeight(), maxDimensions[1]);
	}

	private void assertIsLessOrEqual(int[] arr1, int[] arr2) {
		for(int i=0; i < arr1.length; ++i){
			assertIsLessOrEqual(arr1[i], arr2[i]);
		}
	}
	
	private void assertIsLessOrEqual(int value1, int value2) {
		String msg = String.format("%d <= %d", value1, value2);
		assertTrue(msg, value1 <= value2);
	}

	private <T> T with(T obj) {
		return obj;
	}
	private <T> T from(T obj) {
		return obj;
	}

	private Bitmap whenLoadingAnReducedBitmap(InputStream inputStream
			, int[] maxDimensions, boolean allowDownScale) throws IOException 
	{
		BitmapDecoder decoder = BitmapDecoders.decoder(inputStream);
		return ImageUtils.instance()
				.loadReducedBitmap(decoder, new ReduceOptions(maxDimensions, allowDownScale
						, new FitInsideDimensionsCalculator()));
	}
	private Bitmap whenLoadingAnReducedBitmap(InputStream inputStream
			, int[] wantedDimensions, boolean allowDownScale, int[] maxDims) throws IOException 
	{
		BitmapDecoder decoder = BitmapDecoders.decoder(inputStream);
		ReduceOptions opts = new ReduceOptions(wantedDimensions, allowDownScale);
		opts.setDimensionsCalculator(new FitInsideDimensionsCalculator());
		opts.setMaxDimensions(maxDims);
		
		return ImageUtils.instance().loadReducedBitmap(decoder, opts);
	}

	private int[] wantedDimensions() {
		return new int[]{400, 400};
	}

	private InputStream givenSomeImageStream() throws IOException {
		return ImageHelper.openImage(getInstrumentation().getContext(), TEST_IMAGE);
	}
}
