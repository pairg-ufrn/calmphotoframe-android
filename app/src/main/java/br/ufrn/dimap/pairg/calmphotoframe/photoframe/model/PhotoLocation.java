package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class PhotoLocation implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8784709227817499413L;
	
	private Double altitude;
	private Double latitude;
	private Double longitude;
	
	public Double getAltitude() {
		return altitude;
	}
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
}
