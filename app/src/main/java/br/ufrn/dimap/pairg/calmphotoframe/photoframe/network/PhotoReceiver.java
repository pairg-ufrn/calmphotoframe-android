package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.image.ImageOrientationHelper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;

/** Class to help load an photo image and a description asynchronously.*/
public class PhotoReceiver {
	private PhotoCache photoCache;
	private final PhotoContainer photoContainer;
	
	public PhotoReceiver(int photoId, PhotoCache photoCache) {
		this(new PhotoContainer(photoId), photoCache);
	}

	public PhotoReceiver(Photo photo, PhotoCache photoCache) {
		this(new PhotoContainer(photo), photoCache);
	}
	protected PhotoReceiver(PhotoContainer container, PhotoCache photoCache){
		this.photoCache = photoCache;
		this.photoContainer = container;
	}

	public PhotoContainer getPhotoContainer() {
		return this.photoContainer;
	}
	public int getPhotoId() {
		return getPhotoContainer().getPhotoId();
	}
	
	public void onPhotoLoadReceivedDescription(ResultListener<? super PhotoImageWrapper> photoListener,
                                               Photo photo)
    {
		photoContainer.setPhotoDescriptor(photo);
		onReceiveResponse(photoListener);
	}
	
	public void onPhotoLoadReceiveImage(ResultListener<? super PhotoImageWrapper> photoListener
			, Bitmap bitmap, boolean fromCache)
	{
		synchronized (photoContainer){
			if(photoContainer.hasFailed()){
				return;
			}
			
			this.photoCache.putImage(photoContainer.getPhotoId(), bitmap);
			this.photoContainer.setImage(bitmap, fromCache);
			onReceiveResponse(photoListener);
		}
	}
	//FIXME: (?) ResultListener deve se tornar um atributo de PhotoReceiver
	public void onPhotoLoadError(ResultListener<? super PhotoImageWrapper> photoListener
            , Throwable throwable)
    {
		if(!hasNotifiedFailure()){
            Listeners.onFailure(photoListener, throwable);
        }
	}

	private boolean hasPhotoListener(final ResultListener<? super PhotoImageWrapper> photoListener) {
		return photoListener != null && photoListener.isListening();
	}

	private void onReceiveResponse(ResultListener<? super PhotoImageWrapper> photoListener) {
		if(photoContainer.containsDescriptionAndImage()){
			fixOrientation(photoContainer);
			if(photoListener != null){
                PhotoImageWrapper response =
                        new PhotoImageWrapper(photoContainer.getPhotoDescriptor(), photoContainer.getImage());
                Listeners.onResult(photoListener, response);
			}
		}
	}

	private boolean hasNotifiedFailure() {
		synchronized (photoContainer){
			if(photoContainer.hasFailed()){
				return true;
			}
			photoContainer.setFail(true);
			return false;
		}
	}
	
	private void fixOrientation(PhotoContainer photoContainer) {
		if(photoContainer.isImageFromCache()){
			return;
		}
		Bitmap fixedImage = ImageOrientationHelper.fixImage(photoContainer.getPhotoDescriptor(), photoContainer.getImage());
		if(fixedImage != photoContainer.getImage()){
			this.photoCache.putImage(photoContainer.getPhotoId(), fixedImage);
			photoContainer.setImage(fixedImage);
		}
	}

	public boolean photoIsOnCache(int photoId) {
		return photoCache != null && photoCache.containsImage(photoId);
	}

	public boolean tryReceiveImageFromCache(int photoId,
                                            ResultListener<? super PhotoImageWrapper> photoListener) {
		if(photoIsOnCache(photoId)){
			this.onPhotoLoadReceiveImage(photoListener, photoCache.getImage(photoId), true);
			return true;
		}
		return false;
	}
}
