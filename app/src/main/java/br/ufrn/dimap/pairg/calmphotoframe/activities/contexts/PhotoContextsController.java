package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.contexts.ReloadContextsResultsListener.ReloadListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntitySelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext.CreatePhotoContextCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Permission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator.FragmentBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextsFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

public class PhotoContextsController implements ReloadListener {

	private static final String TAG_CONTEXTS_FRAG = PhotoContextsController.class.toString() + ".ContextsFragment";
	
	private PhotoContextsFragment contextsFragment;
	private BaseActivity activity;
	private FragmentCreator fragCreator;
	
	private PhotoContextDetailsController detailsController;

	private ContextSelector contextSelector;

	public PhotoContextsController(BaseActivity activity) {
		this.activity = activity;
		fragCreator = new FragmentCreator(activity.getSupportFragmentManager());

		contextSelector = new ContextSelector();
	}

	public PhotoContextsFragment getContextsFragment() {
		return contextsFragment;
	}

	public PhotoContextDetailsController getDetailsController() {
		return detailsController;
	}

    public ContextSelector getContextSelector() {
        return contextSelector;
    }

    public void initialize() {
		this.contextsFragment = buildPhotoContextsFragment();
        if(detailsController == null){
            detailsController = new PhotoContextDetailsController(activity, R.id.fragment_photocontextMaster);
            detailsController.initialize();
        }
		new PhotoContextsActionMode(R.menu.contextualmenu_photocontext, this);
	}

	@Override
	public void reloadContent() {
		ApiClient.instance().contexts().list(new ActivityResultListener<Collection<? extends PhotoContext>>(activity) {
			@SuppressWarnings("unchecked")
			@Override
			public void onResult(Collection<? extends PhotoContext> contexts) {
				contextsFragment.items().set(contexts);
				verifyCurrentContext(contexts);
			}

			@Override
			public boolean isListening() {
				return true;
			}
		});
	}

    private PhotoContextsFragment buildPhotoContextsFragment() {
		final PhotoContextsFragment contextsFragment = fragCreator
				.addFragment(R.id.fragment_photocontextMaster, TAG_CONTEXTS_FRAG, new ContextsFragBuilder());

		contextsFragment.setCreateContextListener(new CreateContextActionListener());
		contextsFragment.setOnChangeContextListener(new PutContextAsActivityResult(activity));

		return contextsFragment;
	}

    private void verifyCurrentContext(Collection<? extends PhotoContext> contexts) {
        if(!containsCurrentContext(contexts)){
            setDefaultCtxAsCurrent();
        }
    }

    private boolean containsCurrentContext(Collection<? extends PhotoContext> contexts) {
        Long currentCtx = ApiClient.instance().getCurrentContext();
        if(currentCtx != null){
            for(PhotoContext ctx : contexts){
                if(currentCtx.equals(ctx.getId())){
                    return true;
                }
            }
        }
        return false;
    }

    private void setDefaultCtxAsCurrent() {
        final PhotoContextsFragment contextsFragment = this.contextsFragment;

        ResultListener<PhotoContext> listener = new CompositeResultListener<>(
                new PutContextAsActivityResult(activity),
                new BasicResultListener<PhotoContext>()
                {
                    @Override
                    public void onResult(PhotoContext result) {
                        ApiClient.instance().setCurrentContext(result.getId());
                        contextsFragment.refreshItems();
                    }
                }
        );

        ApiClient.instance().contexts().get(null, listener);
    }
	/* **************************************************************************************/
	
	private static class ContextsFragBuilder implements FragmentBuilder {
		@Override
		public Fragment buildFragment() {
			PhotoContextsFragment frag = new PhotoContextsFragment();
			frag.setContextIdSelector(new ContextSelector());
			
			return frag;
		}
	}
	private static class ContextSelector implements EntitySelector<Long> {
		@Override
		public Long getEntity() {
			return ApiClient.instance().getCurrentContext();
		}

		@Override
		public void select(Long contextId) {
			ApiClient.instance().setCurrentContext(contextId);
		}
	}

    private class CreateContextActionListener implements ActionListener<Void> {
		@Override
		public void execute(Void v) {
			CreatePhotoContextCommand cmd = new CreatePhotoContextCommand(activity);

			ResultListener<PhotoContext> listener = new CompositeResultListener<>(
					new SetCreatedContextAsCurrent()
					, new PutContextAsActivityResult(activity)
					, new BasicResultListener<PhotoContext>(){
						public void onResult(PhotoContext result) {
							contextsFragment.getAdapter().items().addItem(result);
						}
			});

			cmd.execute(new OnCreateContextPutPermission(listener));
		}

    }

    public static class OnCreateContextPutPermission extends CommonListenerDelegator<PhotoContext> {
        public OnCreateContextPutPermission(ResultListener<PhotoContext> listener) {
            super(listener);
        }

        @Override
        protected PhotoContext convertResult(PhotoContext result) {
            if(result instanceof  PhotoContextWithPermission){
                return result;
            }

            Permission p = synthesizePermission(result);
            result = new PhotoContextWithPermission(result)
                        .setPermission(p);

            return result;
        }

        @NonNull
        private Permission synthesizePermission(PhotoContext result) {
            User user = ApiClient.instance().getCurrentUser();
            Long userId = user != null ? user.getId() : null;

            return new Permission(userId, result.getId(), PermissionLevel.Admin);
        }
    }
}