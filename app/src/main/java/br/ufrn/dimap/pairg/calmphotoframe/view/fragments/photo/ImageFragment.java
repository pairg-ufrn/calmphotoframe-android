package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import com.ortiz.touch.TouchImageView;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class ImageFragment extends Fragment{
	private static final float DEFAULT_MIN_ZOOM = 0.2f;
	
	private ImageView imgView;
	private int defaultImageId;
	private int errorImageId;
	private Bitmap image;
	
	public ImageFragment(){
		super();
//		defaultImageId = R.drawable.default_image;
		defaultImageId = android.R.color.transparent;
		errorImageId   = R.drawable.image_not_found;
	}
	
	public int getDefaultImageId() {
		return defaultImageId;
	}
	public void setDefaultImageId(int defaultImageId) {
		this.defaultImageId = defaultImageId;
	}
	public int getErrorImageId() {
		return errorImageId;
	}
	public void setErrorImageId(int errorImageId) {
		this.errorImageId = errorImageId;
	}

	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_image, container, false);
        
        TouchImageView touchImageView = (TouchImageView)rootView.findViewById(R.id.img_frag_view);
        setupTouchView(getActivity(), touchImageView);
        
        imgView = touchImageView;
        if(image != null){
        	imgView.setImageBitmap(image);
        }
        
        return rootView;
    }

	private void setupTouchView(Context context, TouchImageView touchImageView) {
        String  sharedPrefKey = context.getResources().getString(R.string.preferences_imagezoom);
        String  minZoomKey = context.getResources().getString(R.string.preferences_imagezoom_minzoom);
		SharedPreferences sharedPref = context.getSharedPreferences(sharedPrefKey, Context.MODE_PRIVATE);
		
		float minZoomValue =sharedPref.getFloat(minZoomKey, DEFAULT_MIN_ZOOM);
        touchImageView.setMinZoom(minZoomValue);
	}

	public void setImage(Bitmap bitmap) {
		this.image = bitmap;
		if(imgView != null){
			this.imgView.setImageBitmap(bitmap);
		}
	}

	public void showErrorImage() {
		Log.w(getClass().getName(), "Show Error Image!!");
		imgView.setImageResource(this.errorImageId);		
	}
	public void showDefaultImage() {
		Log.w(getClass().getName(), "Show Default Image!!");
		imgView.setImageResource(this.defaultImageId);
	}

	public Drawable getImageDrawable() {
		return imgView != null ? imgView.getDrawable() : null;
	}
}
