package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemLongClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.RecyclerItemClickListener;

/**
 */
public class ItemsViewRecycler implements ItemsSelectionManager.ItemsView {
    RecyclerView recyclerView;
    RecyclerItemClickListener recyclerItemClickListener;
    ItemsRecycleAdapter<?> adapter;

    public ItemsViewRecycler(Context context, ItemsRecycleAdapter<?> adapter) {
        this(context, adapter, null);
    }

    public ItemsViewRecycler(Context context, ItemsRecycleAdapter<?> adapter, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        recyclerItemClickListener = new RecyclerItemClickListener(context);
        recyclerItemClickListener.attachToView(recyclerView);
        this.adapter = adapter;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        recyclerItemClickListener.attachToView(recyclerView);
    }


    @Override
    public void addOnItemClickListener(OnItemClickListener listener) {
        recyclerItemClickListener.addItemClickListener(listener);
    }

    @Override
    public void removeOnItemClickListener(OnItemClickListener listener) {
        recyclerItemClickListener.removeItemClickListener(listener);
    }

    @Override
    public void addOnItemLongClickListener(OnItemLongClickListener listener) {
        recyclerItemClickListener.addItemLongClickListener(listener);
    }

    @Override
    public void removeOnItemLongClickListener(OnItemLongClickListener listener) {
        recyclerItemClickListener.removeItemLongClickListener(listener);
    }

    @Override
    public long getIdFromPosition(int position) {
        return recyclerView.getChildItemId(getViewAt(position));
    }

    @Override
    public int getViewCount() {
        return recyclerView.getChildCount();
    }

    @Override
    public View getViewAt(int position) {
        return recyclerView.getChildAt(position);
    }

    @Override
    public void notifyItemChanged(int position) {
        adapter.rebind(position);
//            recyclerView.getAdapter().notifyItemChanged(position);
    }
}
