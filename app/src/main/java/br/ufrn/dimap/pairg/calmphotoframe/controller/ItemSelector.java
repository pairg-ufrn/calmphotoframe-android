package br.ufrn.dimap.pairg.calmphotoframe.controller;

import java.util.Collection;

public interface ItemSelector<Item>{
	Collection<? extends Item> getSelectedItems();
	Item getSelectedItem();
}