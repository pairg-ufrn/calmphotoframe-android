package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.support.v4.app.FragmentActivity;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog;

public class OnResultSetSaveDialogPath extends BasicResultListener<String[]> {
    private String dialogTag;
    private FragmentActivity activity;

    public OnResultSetSaveDialogPath(FragmentActivity activity, String dialogTag) {
        this.activity = activity;
        this.dialogTag = dialogTag;
    }

    @Override
    public void onResult(String[] result) {
        Logging.d(getClass().getSimpleName(), "onResult: " + result);

        if (result != null && result.length > 0) {
            setSaveDialogDir(result[0]);
        }
    }

    protected void setSaveDialogDir(String filepath) {
        SaveDialog dialogFragment = (SaveDialog) activity.getSupportFragmentManager()
                .findFragmentByTag(dialogTag);


        Logging.d(getClass().getSimpleName(), "set dialog(" + dialogFragment+ ") filepath: " + filepath);
        dialogFragment.setFilepath(filepath);
    }
}
