package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.Arrays;

import android.view.View;
import android.view.ViewGroup;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.PermissionLevelViewBuilder;

public class PermissionLevelAdapter extends ViewBuilderAdapter<PermissionLevel>{
	private boolean enabled;
	
	public PermissionLevelAdapter() {
		super(new PermissionLevelViewBuilder());
		this.setItems(Arrays.asList(PermissionLevel.values()));
	}

	public boolean isEnabled(){
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		if(enabled != isEnabled()){
			this.enabled = enabled;
			notifyDataSetChanged();
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);
		
		v.setEnabled(isEnabled());
		
		return v;
	}
}
