package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import java.io.IOException;
import java.net.SocketException;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;

public class NotifyUserOnLoginOrSignupFailure extends BasicResultListener<Object> {
    private UserNotifier notifier;
	private LoginMode loginMode;

	public NotifyUserOnLoginOrSignupFailure(UserNotifier notifier, LoginMode loginMode) {
        this.notifier = notifier;
		this.loginMode = loginMode;
    }

	@Override
    public void onFailure(Throwable error) {
		int msgError = R.string.error_unknown;

		if (ExceptionUtils.isCausedBy(error, ApiException.class)) {
			int statusCode = ((ApiException) error).getStatusCode();

            if (statusCode == 401 && loginMode == LoginMode.Login) {
				msgError = R.string.login_failure_unauthorized;
            } else if (statusCode >= 500) {
				msgError = R.string.login_failure_server_error;
			} else {
				msgError = R.string.login_failure_api_generic;
			}
        } else if (ExceptionUtils.isCausedBy(error, SocketException.class)
				|| ExceptionUtils.isCausedBy(error, IOException.class)) {
			msgError = R.string.login_failure_connection;
		}

		notifier.notifyUser().text(msgError).show();
    }
}
