package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

/**
 */
public class EmptyStateView {
    View emptyViewContainer;
    ImageView iconView;
    TextView messageView;

    public EmptyStateView setup(View rootView, int viewId) {
        emptyViewContainer = ViewUtils.getView(rootView, viewId);
        iconView = ViewUtils.getView(emptyViewContainer, R.id.emptyState_icon);
        messageView = ViewUtils.getView(emptyViewContainer, R.id.emptyState_text);

        return this;
    }

    public View getEmptyViewContainer(){
        return emptyViewContainer;
    }

    public EmptyStateView setState(int iconId, int stringId) {
        return setIcon(iconId).setText(stringId);
    }

    public EmptyStateView setIcon(int iconId) {
        iconView.setImageResource(iconId);
        return this;
    }

    public EmptyStateView setText(int stringId) {
        return setText(getContext().getText(stringId));
    }

    public EmptyStateView setText(CharSequence text) {
        messageView.setText(text);
        return this;
    }

    public EmptyStateView setVisible(boolean visible) {
        ViewUtils.setVisible(emptyViewContainer, visible);
        return this;
    }

    public boolean isVisible() {
        return emptyViewContainer != null
                && emptyViewContainer.getVisibility() == View.VISIBLE;
    }

    public Context getContext() {
        return emptyViewContainer != null ? emptyViewContainer.getContext() : null;
    }
}
