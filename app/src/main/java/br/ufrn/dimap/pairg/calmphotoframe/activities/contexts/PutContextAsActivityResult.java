package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import android.app.Activity;
import android.content.Intent;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;

public class PutContextAsActivityResult extends BasicResultListener<PhotoContext> {
	private Activity activity;
	private boolean finishActivity;

	public PutContextAsActivityResult(Activity activity) {
		this(activity, false);
	}
	
	public PutContextAsActivityResult(Activity activity, boolean finishActivity) {
		this.activity = activity;
		this.finishActivity = finishActivity;
	}
	@Override
	public void onResult(PhotoContext result) {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(Intents.Extras.SelectedContext, result);
		activity.setResult(Activity.RESULT_OK, resultIntent);
		if(finishActivity){
			activity.finish();
		}
	}
}