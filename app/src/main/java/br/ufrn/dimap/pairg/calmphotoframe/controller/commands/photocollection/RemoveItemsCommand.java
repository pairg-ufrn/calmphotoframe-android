package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.CompositeCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class RemoveItemsCommand implements ArgsCommand<Collection<PhotoComponent>, Collection<PhotoComponent>> {


    private int concurrencyLevel;
    private ResultListener<? super PhotoComponent> progressListener;

    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public void setConcurrencyLevel(int concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
    }

    public ResultListener<? super PhotoComponent> getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(ResultListener<? super PhotoComponent> progressListener) {
        this.progressListener = progressListener;
    }

    @Override
    public void execute(Collection<PhotoComponent> toRemove, ResultListener<? super Collection<PhotoComponent>> listener) {
        Collection<Photo> photosToRemove = PhotoComponent.filterPhotos(toRemove);
        Collection<PhotoCollection> collectionsToRemove = PhotoComponent.filterPhotoCollections(toRemove);

        executeRemove(photosToRemove, collectionsToRemove, listener);
    }

    private void executeRemove(Collection<Photo> photosToRemove,
                               Collection<PhotoCollection> collectionsToRemove,
                               ResultListener<? super Collection<PhotoComponent>> listener) {

        int numCalls = 0;
        numCalls += (!collectionsToRemove.isEmpty() ? 1 : 0);
        numCalls += (!photosToRemove.isEmpty() ? 1 : 0);

        ComponentsResultAgreggator agreggator = new ComponentsResultAgreggator(numCalls, listener);

        removeCollections(collectionsToRemove, progressListener, agreggator);
        removePhotos(photosToRemove, progressListener, agreggator);
    }

    private void removePhotos(Collection<Photo> photosToRemove,
                              ResultListener<? super PhotoComponent> progressListener,
                              ComponentsResultAgreggator agreggator)
    {
        if (!photosToRemove.isEmpty()) {
            CompositeCommand
                    .build(photosToRemove, new RemovePhotoCommand())
                    .setConcurrencyLevel(getConcurrencyLevel())
                    .onProgress(new ConvertToComponentDelegator(progressListener))
                    .execute(new ConvertToComponentCollectionDelegator(agreggator));
        }
    }

    private void removeCollections(Collection<PhotoCollection> collectionsToRemove,
                                   ResultListener<? super PhotoComponent> progressListener,
                                   ComponentsResultAgreggator agreggator)
    {
        if (!collectionsToRemove.isEmpty()) {
            ResultListener<Collection<PhotoCollection>> collListener
                    = buildCollectionsListener(agreggator, progressListener, collectionsToRemove.size());

            ApiClient.instance()
                    .collections()
                    .removeAll(collectionsToRemove, collListener);
        }
    }

    @NonNull
    private ResultListener<Collection<PhotoCollection>>
        buildCollectionsListener(
            ResultListener<? super Collection<PhotoComponent>> listener,
            ResultListener<? super PhotoComponent> progressListener,
            int expectedResults)
    {
        return new OnResultConvertToComponentsCollection(Listeners.join(
                listener,
                new OnRemoveCollectionsNotifyProgress(progressListener, expectedResults)
        ));
    }

    private static class ConvertToComponentCollectionDelegator extends ResultListenerDelegator<Collection<Photo>, Collection<PhotoComponent>> {
        public ConvertToComponentCollectionDelegator(ComponentsResultAgreggator agreggator) {
            super(agreggator);
        }

        @Override
        protected Collection<PhotoComponent> convertResult(Collection<Photo> result) {
            List<PhotoComponent> components = new ArrayList<>();
            for(Photo p : result){
                components.add(new PhotoComponent(p));
            }
            return components;
        }
    }

    /** Combine responses from remove PhotoCollections and remove photos and deliver to listener*/
    static class ComponentsResultAgreggator extends BasicResultListener<Collection<PhotoComponent>> {

        ResultListener<? super Collection<PhotoComponent>> delegate;

        int numberOfCalls;

        Collection<PhotoComponent> aggregate;
        boolean started=false;

        public ComponentsResultAgreggator(int numberOfCalls,
                                          ResultListener<? super Collection<PhotoComponent>> delegate)
        {
            this.delegate = delegate;
            this.numberOfCalls = numberOfCalls;
            aggregate = new ArrayList<>();
        }

        @Override
        public void onStart() {
            super.onStart();

            if(!started){
                Listeners.onStart(delegate);
                started = true;
            }
        }

        @Override
        protected void handleResult(Collection<PhotoComponent> result) {

            for(PhotoComponent c : result){
                aggregate.add(c);
            }

            checkFinish();
        }

        @Override
        protected void handleError(Throwable error) {
            checkFinish();
        }

        private void checkFinish() {
            --numberOfCalls;
            if(numberOfCalls <= 0){
                Listeners.onResult(delegate, aggregate);
            }
        }
    }

    private static class OnRemoveCollectionsNotifyProgress extends BasicResultListener<Collection<PhotoComponent>> {

        ResultListener<? super PhotoComponent> progressListener;
        int expectedResults;

        public OnRemoveCollectionsNotifyProgress(ResultListener<? super PhotoComponent> progressListener, int expectedResults) {
            this.progressListener = progressListener;
            this.expectedResults = expectedResults;
        }

        @Override
        protected void handleResult(Collection<PhotoComponent> result) {
            super.handleResult(result);
            if(progressListener != null){
                for (PhotoComponent comp : result) {
                    Listeners.onResult(progressListener, comp);
                }
            }
        }

        @Override
        protected void handleError(Throwable error) {
            for(int i=0; i < expectedResults; ++i){
                Listeners.onFailure(progressListener, error);
            }
        }
    }

    private class ConvertToComponentDelegator extends ResultListenerDelegator<Photo, PhotoComponent> {
        public ConvertToComponentDelegator(ResultListener<? super PhotoComponent> delegate) {
            super(delegate);
        }

        @Override
        protected PhotoComponent convertResult(Photo result) {
            return new PhotoComponent(result);
        }
    }
}
