package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public interface ResultListener<T>{
	void onStart();
	void onResult(T result);
	void onFailure(Throwable error);
	boolean isListening();
}
