package br.ufrn.dimap.pairg.calmphotoframe.controller;

public interface EntitySelector<T> extends EntityGetter<T> {
    void select(T item);
}
