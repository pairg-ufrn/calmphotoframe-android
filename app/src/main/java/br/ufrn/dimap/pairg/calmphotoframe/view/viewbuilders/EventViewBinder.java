package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class EventViewBinder extends InflaterViewBinder<Event>{
    private TextView messageView, dateView;
    private UserImageViewBinder userImageViewBinder;

    private DateFormatter dateFormatter;

    public EventViewBinder(){
        this(R.layout.view_event);
    }

    public EventViewBinder(int layoutId) {
        super(layoutId);

        userImageViewBinder = new UserImageViewBinder().setAlwaysTryGetImage(true);
        dateFormatter = new DateFormatter();
    }

    public EventViewBinder setDateFormatter(DateFormatter dateFormatter){
        this.dateFormatter = dateFormatter;
        return this;
    }

    public EventViewBinder setUserImageViewBinder(UserImageViewBinder userImageViewBinder){
        this.userImageViewBinder = userImageViewBinder;
        this.userImageViewBinder.setAlwaysTryGetImage(true);
        return this;
    }

    @Override
    public void setup(View view) {
        messageView = ViewUtils.getView(view, R.id.view_event_message);
        dateView = ViewUtils.getView(view, R.id.view_event_date);

        if(userImageViewBinder != null) {
            userImageViewBinder.setup(view);
        }
    }

    @Override
    public void bind(Event evt) {
        if(evt != null){
            messageView.setText(evt.getMessage());
            dateView.setText(evt.getCreatedAt() != null ? makeDateText(evt) : "");

            if(userImageViewBinder != null) {
                userImageViewBinder.bind(evt.getUser());
            }
        }
    }


	/* *********************************************************************************/

    public CharSequence makeDateText(Event evt) {
        if(evt == null || evt.getCreatedAt() == null || dateFormatter == null){
            return "";
        }

        return dateFormatter.getRelative(evt.getCreatedAt());
    }
}
