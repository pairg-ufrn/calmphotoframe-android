package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UpdatePhotoRequest extends CommonGenericRequest<Photo>{
	private Photo photoToUpdate;
	
	public UpdatePhotoRequest(ApiConnector apiConnector, Photo photo, ResultListener<? super Photo> listener) {
		super(apiConnector, Photo.class, listener);
		this.photoToUpdate = photo;
	}

	@Override
	public RequestHandler execute(){
		return postJsonRequest(photoToUpdate);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().photoUrl(photoToUpdate.getId());
	}
}