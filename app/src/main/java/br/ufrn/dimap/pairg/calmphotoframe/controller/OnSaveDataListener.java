package br.ufrn.dimap.pairg.calmphotoframe.controller;

public interface OnSaveDataListener<T>{
	public boolean onSaveData(T ctx);
}