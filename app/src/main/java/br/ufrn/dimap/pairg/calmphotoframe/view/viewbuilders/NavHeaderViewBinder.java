package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

public class NavHeaderViewBinder extends InflaterViewBinder<ApiEndpoint> {
    private UserViewBinder userViewBinder;
    private TextView photoFrameAddressView;
    private View photoFrameContainer;
    private FloatingActionButton fabEditProfile;

    private ApiEndpoint boundApiEndpoint;
    private ActionListener<? super User> editProfileListener;

    public NavHeaderViewBinder() {
        this(R.layout.navdrawer_header);
    }
    public NavHeaderViewBinder(int layoutId) {
        super(layoutId);

        userViewBinder = new UserViewBinder();
        userViewBinder.setLoginViewId(R.id.user_login);
    }

    public ActionListener getEditProfileListener() {
        return editProfileListener;
    }

    public void setEditProfileListener(ActionListener<? super User> editProfileListener) {
        this.editProfileListener = editProfileListener;
    }

    public UserViewBinder getUserViewBinder() {
        return userViewBinder;
    }

    @Override
    public void setup(View view) {
        this.userViewBinder.setup(view);

        this.photoFrameAddressView = ViewUtils.getView(view, R.id.photoframe_address);
        this.photoFrameContainer = ViewUtils.getView(view, R.id.photoframe_address_container);
        this.fabEditProfile = ViewUtils.getView(view, R.id.editprofile_button);
        if(fabEditProfile != null) {
            fabEditProfile.setVisibility(View.GONE);
            fabEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickEditProfile(v);
                }
            });
        }
    }

    @Override
    public void bind(ApiEndpoint apiEndpoint) {
        bindPhotoFrame(apiEndpoint);

        userViewBinder.bind(getUser(apiEndpoint));
    }

    @Nullable
    private User getUser(ApiEndpoint apiEndpoint) {
        User user = null;
        if(apiEndpoint != null && apiEndpoint.getSession() != null){
           user = apiEndpoint.getSession().getUser();
        }
        return user;
    }

    protected void onClickEditProfile(View v) {
        if(getEditProfileListener() != null){
            getEditProfileListener().execute(getUser(this.boundApiEndpoint));
        }
    }

    private void bindPhotoFrame(ApiEndpoint apiEndpoint) {
        this.boundApiEndpoint = apiEndpoint;

        if(apiEndpoint != null && photoFrameAddressView != null){
            StringBuilder builder = new StringBuilder();
            builder.append(apiEndpoint.getHostname());
            if(!apiEndpoint.portIsDefault()){
                builder.append(":").append(apiEndpoint.getPort());
            }

            photoFrameAddressView.setText(builder);
        }


        ViewUtils.setVisible(fabEditProfile, hasUser(apiEndpoint));
        ViewUtils.setVisible(photoFrameContainer, showAddress(apiEndpoint));
    }

    private boolean showAddress(ApiEndpoint apiEndpoint) {
        return (apiEndpoint != null && !apiEndpoint.hostnameIsDefault());
    }

    private boolean hasUser(ApiEndpoint apiEndpoint) {
        return apiEndpoint != null
                && apiEndpoint.getSession() != null
                && apiEndpoint.getSession().getUser() != null;
    }
}
