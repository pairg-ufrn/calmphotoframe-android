package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class NotifierProgressListener implements ProgressListener {
	private int notificationId;
	private NotificationCompat.Builder notifBuilder;
	private NotificationManager notifyManager;
	private CharSequence contentText;
	private CharSequence onProgressText;
	private CharSequence onSuccessText;
	private CharSequence onFailureText;
	private int notificationIcon;
	private boolean listening;
	
	public NotifierProgressListener(int notificationId, Context context) {
    	this.notificationId = notificationId;
    	notifyManager = (NotificationManager) context.getApplicationContext()
    												 .getSystemService(Context.NOTIFICATION_SERVICE);
    	notifBuilder = new NotificationCompat.Builder(context.getApplicationContext());

    	contentText    = context.getString(R.string.default_progress_ContentTitle);
    	onProgressText = context.getString(R.string.default_progress_onProgressText);
    	onSuccessText  = context.getString(R.string.default_progress_onSuccessText);
    	onFailureText  = context.getString(R.string.default_progress_onFailureText);
    	notificationIcon = R.drawable.ic_action_download;
    	
    	this.listening = true;
	}
	
	public CharSequence getContentText()   { return contentText; }
	public CharSequence getOnProgressText() { return onProgressText; }
	public CharSequence getOnSuccessText()  { return onSuccessText; }
	public CharSequence getOnFailureText()  { return onFailureText; }
	public int getNotificationIcon()  { return notificationIcon; }

	public void setContentText(CharSequence contentText) {
		this.contentText = contentText;
	}
	public void setOnProgressText(CharSequence onProgressText) {
		this.onProgressText = onProgressText;
	}
	public void setOnSuccessText(CharSequence onSuccessText) {
		this.onSuccessText = onSuccessText;
	}
	public void setOnFailureText(CharSequence onFailureText) {
		this.onFailureText = onFailureText;
	}
	public void setNotificationIcon(int notificationIcon) {
		this.notificationIcon = notificationIcon;
	}
	@Override
	public void onStart() {
		notifBuilder.setContentTitle(getOnProgressText())
	    .setContentText(getContentText())
	    .setSmallIcon(getNotificationIcon())
		.setAutoCancel(true);
	}

	@Override
	public void onProgress(long bytesWritten, long totalSize) {
		// Sets the progress indicator to a max value, the
        // current completion percentage, and "determinate"
        // state
        notifBuilder.setProgress((int)totalSize, (int)bytesWritten, false);
        // Displays the progress bar for the first time.
        notifyManager.notify(notificationId, notifBuilder.build());
	}

	@Override
	public void onFinish(boolean successful){
		notifyManager.cancel(notificationId);
		if(successful){
			// When the loop is finished, updates the notification
			notifBuilder.setContentTitle(getOnSuccessText());
		}
		else{
			// When the loop is finished, updates the notification
			notifBuilder.setContentTitle(getOnFailureText());
		}
        // Removes the progress bar
        notifBuilder.setProgress(0,0,false);
        notifyManager.notify(notificationId, notifBuilder.build());
	}

	@Override
	public boolean isListening() {
		return listening;
	}
	public void setListening(boolean listening) {
		this.listening = listening;
	}
}