package br.ufrn.dimap.pairg.calmphotoframe.photoframe;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicReference;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiEndpointHolder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiRouteBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ServerCacheKeyGenerator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.UpdateSessionUserOnResult;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts.ContextsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.events.EventsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.PhotoCollectionsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.PhotosApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session.SessionApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.GetUserRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.UsersApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.Cache;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.CacheFactory;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import br.ufrn.dimap.pairg.calmphotoframe.utils.MemoryUtils;

public class ApiClient implements ApiEndpointHolder.OnChangeApiEndpoint {

    private static AtomicReference<ApiClient> instance = new AtomicReference<>(null);

	public static ApiClient instance() {
        ApiClient client = instance.get();
		if(client == null){
			instance.compareAndSet(null, new ApiClient());
            client = instance.get();
		}
		return client;
	}
	public static void setInstance(ApiClient apiClient){
		instance.set(apiClient);
	}

    private final ApiEndpointHolder apiEndpointHolder;
    private final ApiConnector apiConnector;

    private final SessionApi sessionApi;
	private final UsersApi usersApi;
    private final PhotoCollectionsApi photoCollectionsApi;
    private final ContextsApi photoContextsApi;
    private final PhotosApi photosApi;
    private final EventsApi eventsApi;


	public ApiClient(){
		this(CacheFactory.instance().<String, Bitmap>buildCache(
						MemoryUtils.defaultImageCacheSize()));
	}

	public ApiClient(Cache<String, Bitmap> imageCache){
		this(defaultApiConnector(imageCache));
	}

    public ApiClient(ApiConnector apiConnector){
        this(apiConnector, new ApiEndpointHolder());
    }

    public ApiClient(ApiConnector apiConnector, ApiEndpointHolder apiEndpointHolder){
        if(apiEndpointHolder == null){
            throw new IllegalArgumentException("ApiEndpointHolder cannot be null");
        }

        this.apiEndpointHolder = apiEndpointHolder;

        this.apiConnector = apiConnector;
        this.apiConnector.getApiBuilder().setEndpointGetter(this.apiEndpointHolder);
        apiConnector.getPhotoCache().setKeyGenerator(new ServerCacheKeyGenerator(this.apiEndpointHolder));

        this.apiEndpointHolder.setOnChangeListener(this);

        this.sessionApi = new SessionApi(apiConnector, this.apiEndpointHolder);
        this.usersApi = new UsersApi(apiConnector);
        this.photoCollectionsApi = new PhotoCollectionsApi(apiConnector);
        this.photoContextsApi = new ContextsApi(apiConnector);
        this.photosApi = new PhotosApi(apiConnector);
        this.eventsApi = new EventsApi(apiConnector);
    }

    public static ApiConnector defaultApiConnector(Cache<String, Bitmap> imageCache)
    {
        return new ApiConnector(
                new HttpConnector(),
                new ApiRouteBuilder(),
                ObjectMapperConfiguration.build(),
                new PhotoCache(imageCache));
    }

	/* *********************************** Submodules **************************************/

    public UsersApi users(){
        return this.usersApi;
    }

    public PhotoCollectionsApi collections(){
        return this.photoCollectionsApi;
    }

    public ContextsApi contexts(){
        return this.photoContextsApi;
    }

    public PhotosApi photos(){
        return this.photosApi;
    }

    public EventsApi events(){
        return this.eventsApi;
    }

    public SessionApi session(){
        return this.sessionApi;
    }

    /* *********************************** getters and setters *****************************/

    protected ApiConnector connector() {
        return apiConnector;
    }

    protected ApiRouteBuilder getApiBuilder() {
        return connector().getApiBuilder();
    }

    public User getCurrentUser(){
        ApiEndpoint info = getEndpoint();
		return info == null ? null : info.getSession().getUser();
	}
    public void updateCurrentUser(User user) {
        User currentUser = getCurrentUser();
        if(currentUser != null && currentUser.getId().equals(user.getId())){
            getEndpoint().setUser(user);
        }
    }

	public boolean isAdminSession(){
        ApiEndpoint info = getEndpoint();
		return info != null && info.getSession().isAdmin();
	}

    public Long getCurrentContext(){
        ApiEndpoint apiEndpoint = getEndpoint();
        return apiEndpoint != null ? apiEndpoint.getCurrentContext() : null;
    }
	public void setCurrentContext(PhotoContext ctx) {
		Long ctxId = ctx != null ? ctx.getId() : null;
		setCurrentContext(ctxId);
	}
	public void setCurrentContext(Long contextId) {
		getEndpoint().setCurrentContext(contextId);
		saveCurrentContext(contextId);
	}

	public void invalidateContext(Long contextId) {
		if(getCurrentContext() != null && (contextId == null || getCurrentContext().equals(contextId))){
			setCurrentContext((Long)null);
		}
	}

    //TODO: remover redundâncias

    public boolean hasEndpoint(){
        return endpoint() != null;
    }
    public ApiEndpoint endpoint(){
        return getEndpoint();
    }

	public ApiEndpoint getEndpoint() {
		return apiEndpointHolder.get();
	}

    public boolean isConnected() {
        return getEndpoint() != null;
    }

    @Override
    public void onChange(ApiEndpoint apiEndpoint) {
        if(apiEndpoint == null){
            return;
        }

        if(apiEndpoint.getUser() != null) {
            restoreCurrentContext();
        }
        else{//Invalidate current context (should be restored when user be loaded)
            apiEndpoint.setCurrentContext(null);

            reloadUser(apiEndpoint);
        }
    }

    protected void reloadUser(ApiEndpoint info) {
        ResultListener<User> listener = new CompositeResultListener<>(
                new UpdateSessionUserOnResult(info.getSession())
                , new BasicResultListener<Object>(){
            @Override
            public void onResult(Object result) {
                restoreCurrentContext();
            }
        });

        new GetUserRequest(apiConnector, listener).execute();
    }

	/* *********************** PhotoFrame preferences ***********************************************/
	private void restoreCurrentContext() {
        setCurrentContext(LocalPersistence.instance().getCurrentPhotoContext(this));
	}
	private void saveCurrentContext(Long contextId){
		LocalPersistence.instance().storeCurrentPhotoContext(this, contextId);
	}
}