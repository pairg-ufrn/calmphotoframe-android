package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.UsersManagementController;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.utils.FragmentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UsersFragment;

public class UsersManagementActivity extends ConnectedActivity {

	UsersManagementController controller;
	
	public UsersManagementActivity() {
		controller = new UsersManagementController(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_users_management);
        setSnackbarViewId(R.id.root_view);
		setupToolbar();
		
		controller.setView(FragmentUtils.<UsersFragment>findById(this, R.id.usersFragment));
	}

	@Override
	protected void loadData() {
		controller.requestData();
	}
}
