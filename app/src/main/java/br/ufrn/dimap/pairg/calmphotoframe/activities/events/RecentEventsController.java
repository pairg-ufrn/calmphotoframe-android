package br.ufrn.dimap.pairg.calmphotoframe.activities.events;

import java.util.Collection;

import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.AsyncGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.FragmentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.RecentEventsFragment;

public class RecentEventsController{

		private static final int POOLING_TIME_MILLIS = 60000;
		
		private RecentEventsFragment view;
		private BaseActivity activity;

	    private ToggleDrawer drawerToggle;
		private final Handler handler;
	    private Runnable refreshViewRunnable;
	    
		public RecentEventsController(BaseActivity activity, int drawerLayoutId) {
			super();
			this.activity = activity;
			drawerToggle = new ToggleDrawer(activity, drawerLayoutId, Gravity.RIGHT);
			handler = new Handler();
		}

		public void initialize(){
			view = FragmentUtils.findById(activity, R.id.recentEventsFragment);
			
			view.setToolbarNavigationOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawerToggle.close();
				}
			});
			
			view.setEventGetter(new EventsAsyncGetter(activity));
			
		}
		
		public void onResume(){
			if(refreshViewRunnable == null){
				refreshViewRunnable = new Runnable() {
					@Override
					public void run() {
						if(activity.isActive()){
							view.refreshData();
							handler.postDelayed(this, POOLING_TIME_MILLIS);
						}
					}
				};
			}
			
			handler.postDelayed(refreshViewRunnable, POOLING_TIME_MILLIS);
		}
		public void onPause(){
			if(refreshViewRunnable != null){
				handler.removeCallbacks(refreshViewRunnable);
			}
		}
		
		public void onPostCreate(){
//			mDrawerToggle.syncState();
		}

		public boolean onBackPressed() {
			return drawerToggle.tryClose();
		}

		public void setupMenu(Menu menu, CommandExecutor cmdExecutor) {
			cmdExecutor.putCommand(R.id.action_recentEvents, drawerToggle, null);
		}

		static class EventsAsyncGetter implements AsyncGetter<Collection<Event>> {
			private EventsFormatter eventsFormatter;
			
			public EventsAsyncGetter(Context context) {
				User user = ApiClient.instance().getCurrentUser();
				Long userId = user != null ? user.getId() : null;
				this.eventsFormatter = new EventsFormatter(context, userId);
			}
			
			@Override
			public void getAsync(final ResultListener<? super Collection<Event>> listener) {
				ApiClient.instance()
								.events().listRecent(buildDelegator(listener));
			}

			private ResultListener<Collection<Event>> buildDelegator(final ResultListener<? super Collection<Event>> listener) {
				return new CommonListenerDelegator<Collection<Event>>(listener){
					@Override
					protected Collection<Event> convertResult(Collection<Event> result) {
						return eventsFormatter.format(result);
					}
				};
			}
		}
		
		public static class ToggleDrawer extends ActivityStatelessCommand<Void> {
			private DrawerLayout drawer;
			private final int drawerId;
			private final int drawerGravity;
			
			public ToggleDrawer(BaseActivity activity, int drawerId, int drawerGravity) {
				super(activity);
				this.drawerId = drawerId;
				this.drawerGravity = drawerGravity;
			}
	
			public boolean tryClose() {
				if(isOpen()){
					close();
					return true;
				}
				return false;
			}
			
			public boolean isOpen() {
				return getDrawer().isDrawerOpen(drawerGravity);
			}

			public void close() {
				getDrawer().closeDrawer(drawerGravity);
			}

			@Override
			public void execute(ResultListener<? super Void> cmdResultListener) {
				if(isOpen()){
					close();
				}
				else{
					getDrawer().openDrawer(drawerGravity);
				}
			}


			protected DrawerLayout getDrawer() {
				if(drawer == null){
					drawer = ViewUtils.getView(getActivity(), drawerId);
				}
				return drawer;
			}
			
		}
	}