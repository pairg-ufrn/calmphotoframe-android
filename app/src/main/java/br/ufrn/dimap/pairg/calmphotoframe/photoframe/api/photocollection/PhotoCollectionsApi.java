package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.CollectionsOperation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class PhotoCollectionsApi {
    private ApiConnector apiConnector;

    public PhotoCollectionsApi(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }
    /** List PhotoCollections of the current PhotoContext*/
    public void listByContext(ResultListener<? super List<PhotoCollection>> listener) {
        listByContext(null, listener);
    }
    /** List PhotoCollections by PhotoContext
     * @param contextId - the PhotoContext id used to filter the listed collections*/
    public void listByContext(Long contextId
            , ResultListener<? super List<PhotoCollection>> listener) {
        new ListPhotoCollectionByContextRequest(apiConnector, contextId, listener).execute();
    }

    public RequestHandler get(Long collectionId
            , ResultListener<? super PhotoCollection> listener)
    {
        return new GetPhotoCollectionRequest(apiConnector, collectionId, listener).execute();
    }
    public RequestHandler getFullCollection(Long photoCollectionId
            , final ResultListener<? super ExpandedPhotoCollection> listener)
    {
        ResultListener<PhotoCollection> delegator = new ResultListenerDelegator<PhotoCollection, ExpandedPhotoCollection>(listener) {
            @Override
            protected ExpandedPhotoCollection convertResult(PhotoCollection result) {
                return (ExpandedPhotoCollection)result;
            }
        };

        return new GetPhotoCollectionRequest(apiConnector, photoCollectionId, delegator, true).execute();
    }

    public void create(final String contextName, final PhotoCollection parentCollection, ResultListener<? super PhotoCollection> listener) {
        new CreatePhotoCollectionRequest(apiConnector, contextName, parentCollection, listener).execute();
    }

    public RequestHandler update(PhotoCollection collection, ResultListener<? super PhotoCollection> listener) {
        return new UpdatePhotoCollectionRequest(apiConnector, collection, listener).execute();
    }

    public void addItems(Collection<? extends PhotoCollection> targetContexts
            , Collection<PhotoComponent> items
            , ResultListener<? super Collection<PhotoCollection>> listener)
    {
        new ChangeCollectionItemsRequest(apiConnector, targetContexts, items, CollectionsOperation.Operation.Add, listener).execute();
    }

    public RequestHandler removeItems(Collection<? extends PhotoCollection> targetContexts
            , Collection<PhotoComponent> items
            , ResultListener<? super Collection<PhotoCollection>> listener)
    {
        return new ChangeCollectionItemsRequest(apiConnector, targetContexts, items
                , CollectionsOperation.Operation.Remove, listener).execute();
    }
    public RequestHandler moveItems(PhotoCollection currentContext,
                                    Collection<PhotoComponent> items,
                                    Collection<? extends PhotoCollection> contexts,
                                    ResultListener<? super PhotoCollection> listener)
    {
        return new MoveFromPhotoCollectionRequest(apiConnector, currentContext, contexts, items, listener).execute();
    }

    public RequestHandler removeAll(Collection<PhotoCollection> selectedContexts
            , ResultListener<? super Collection<PhotoCollection>> listener)
    {
        return new RemoveCollectionsRequest(apiConnector, selectedContexts, listener).execute();
    }
}
