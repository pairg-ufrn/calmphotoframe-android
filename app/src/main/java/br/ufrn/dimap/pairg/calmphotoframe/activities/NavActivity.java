package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.AboutCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.StartActivityCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users.LogoffCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.CachedView;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.NavHeaderViewBinder;

/**
 */
public class NavActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected CommandExecutor navDrawerCommandExecutor;
    protected DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;

    private NavHeaderViewBinder navHeaderViewBinder; //binder to navigation drawer

    public NavActivity(){
        super();
        navDrawerCommandExecutor = new CommandExecutor();

        navHeaderViewBinder = new NavHeaderViewBinder();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        putActivityNavigation(R.id.nav_mainactivity, MainActivity.class);
        putActivityNavigation(R.id.nav_albumsactivity, PhotoCollectionActivity.class);
        putActivityNavigation(R.id.nav_contextsactivity, PhotoContextsActivity.class);
        putActivityNavigation(R.id.nav_usersactivity, UsersManagementActivity.class);

        navDrawerCommandExecutor.putCommand(R.id.nav_about, new AboutCommand(this), null);
        navDrawerCommandExecutor.putCommand(R.id.nav_logoff, new LogoffCommand(this), null);
    }

    private void putActivityNavigation(int menuId, Class<? extends Activity> targetActivity) {
        if(getClass().equals(targetActivity)){
            return;
        }
        navDrawerCommandExecutor.putCommand(menuId
                , new StartActivityCommand(this, targetActivity), null);
    }

    /** Bind navigation header view to current ApiEndpoint instance. */
    public void rebindApiEndpoint(){
        bindApiEndpoint(ApiClient.instance().getEndpoint());
    }
    protected void bindApiEndpoint(ApiEndpoint photoFrame){
        this.navHeaderViewBinder.bind(photoFrame);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_navdrawer);
        setupView(layoutResID);

        if(snackbarViewCache == null){
            snackbarViewCache = new CachedView<>(R.id.navdrawer_content);
        }

        refreshNavigationMenu();
    }

    protected void setupView(int layoutResID) {
        this.drawerLayout = ViewUtils.getView(this, R.id.drawer_layout);

        ViewGroup content = ViewUtils.getView(this, R.id.navdrawer_content);
        LayoutInflater.from(this).inflate(layoutResID, content);

        this.navigationView = ViewUtils.getView(this, R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        this.navHeaderViewBinder.setup(headerView);

        this.navHeaderViewBinder.setEditProfileListener(new ActionListener<User>() {
            @Override
            public void execute(User value) {
                editProfile();
            }
        });

        rebindApiEndpoint();
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();


        Toolbar toolbar = ViewUtils.getView(this, R.id.toolbar);

        if(toolbar != null) {
            drawerToggle
                    = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);

            drawerLayout.addDrawerListener(drawerToggle);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(drawerToggle != null){
            drawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void refreshNavigationMenu() {
        setupNavigationMenu(getNavigationDrawerMenu());
    }

    protected void setupNavigationMenu(Menu navigationMenu) {
        MenuItem usersManagementMenu = navigationMenu.findItem(R.id.nav_usersactivity);
        usersManagementMenu.setVisible(ApiClient.instance().isAdminSession());
    }

    @Override
    public void onBackPressed() {
        if(!isDrawerOpen()){
            super.onBackPressed();
        }
        else{
            closeDrawer();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull  MenuItem item) {
        //Close drawer first to try reduce animation hanging
        closeDrawer();
        navDrawerCommandExecutor.execute(item.getItemId());

        return false;
    }

    @Nullable
    protected Menu getNavigationDrawerMenu(){
        if(this.navigationView == null){
            return null;
        }
        return this.navigationView.getMenu();
    }

    public boolean isDrawerOpen(){
        return this.drawerLayout != null
                && this.drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    public void closeDrawer(){
        if(this.drawerLayout != null){
            drawerLayout.closeDrawers();
        }
    }


    protected void editProfile() {
        IntentUtils.startActivity(this, UserProfileActivity.class);
    }
}
