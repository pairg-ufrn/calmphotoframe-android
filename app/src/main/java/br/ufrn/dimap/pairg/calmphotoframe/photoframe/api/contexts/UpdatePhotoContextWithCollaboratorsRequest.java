package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import java.util.Collection;
import java.util.LinkedList;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts.UpdatePhotoContextWithCollaboratorsRequest.ContextAndCollaborators;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UpdatePhotoContextWithCollaboratorsRequest extends BaseGenericRequest<ContextAndCollaborators, PhotoContext>{
	private ContextAndCollaborators contextWithCollaborators;

	public UpdatePhotoContextWithCollaboratorsRequest(ApiConnector apiConnector
			, PhotoContext ctx, Collection<Collaborator> collabs ,ResultListener<? super PhotoContext> listener) 
	{
		super(apiConnector, ContextAndCollaborators.class, listener);
		contextWithCollaborators = new ContextAndCollaborators(ctx, collabs);
	}

	@Override
	public RequestHandler execute(){
		return postJsonRequest(contextWithCollaborators);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextUrl(contextWithCollaborators.getId());
	}

	@Override
	protected PhotoContext convertToResultType(ContextAndCollaborators response) {
		return response;
	}
	
	public static class ContextAndCollaborators extends PhotoContext{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ContextAndCollaborators() {
			this(null, null);
		}
		
		public ContextAndCollaborators(PhotoContext ctx, Collection<Collaborator> collaborators) {
			super(ctx);
			
			this.collaborators = new LinkedList<>();
			if(collaborators != null){
				for(Collaborator c : collaborators){
					this.collaborators.add(new UserWithPermission(c));
				}
			}
		}
		
		public Collection<UserWithPermission> collaborators;
	}

}