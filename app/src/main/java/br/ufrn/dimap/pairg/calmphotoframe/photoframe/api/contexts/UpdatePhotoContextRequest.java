package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UpdatePhotoContextRequest extends CommonGenericRequest<PhotoContext>{
	private PhotoContext photoContext;

	public UpdatePhotoContextRequest(ApiConnector apiConnector
			, PhotoContext contextToUpdate, ResultListener<? super PhotoContext> listener) 
	{
		super(apiConnector, PhotoContext.class, listener);
		this.photoContext = contextToUpdate;
	}

	@Override
	public RequestHandler execute(){
		return postJsonRequest(photoContext);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextUrl(photoContext.getId());
	}
}