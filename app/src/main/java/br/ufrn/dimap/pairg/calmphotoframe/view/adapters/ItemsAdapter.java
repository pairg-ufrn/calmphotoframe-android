package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.widget.BaseAdapter;

import br.ufrn.dimap.pairg.calmphotoframe.utils.CollectionsUtils;

public abstract class ItemsAdapter<Item> extends BaseAdapter {

	protected List<Item> items;

	public ItemsAdapter() {
		super();
	}

	public void add(Item item) {
		this.add(item, true);
	}

	public void add(Item item, boolean notifyChanges) {
		_getItems().add(item);
		this.notifyDataChanges(notifyChanges);
	}

	public void putItem(int position, Item item) {
		this.putItem(position, item, true);
	}

	public void putItem(int position, Item item, boolean notifyChanges) {
		_getItems().set(position, item);
		this.notifyDataChanges(notifyChanges);
	}

	public void addAll(Collection<Item> itemCollection) {
		this.addAll(itemCollection, true);
	}

	public void addAll(@SuppressWarnings("unchecked") Item ...items) {
		addAll(Arrays.asList(items));
	}

	public void addAll(Collection<Item> itemCollection, boolean notifyChanges) {
		_getItems().addAll(itemCollection);
		this.notifyDataChanges(notifyChanges);
	}

	public void removeAll(Collection<Item> itemCollection) {
		this.removeAll(itemCollection, true);
	}

	public void removeAll(Collection<Item> itemCollection, boolean notifyChanges) {
		_getItems().removeAll(itemCollection);
		this.notifyDataChanges(notifyChanges);
	}

	public void remove(Item item) {
		this.remove(item, true);
	}

	public void remove(Item item, boolean notifyChanges) {
		_getItems().remove(item);
		this.notifyDataChanges(notifyChanges);
	}

	public void clear() {
		this.clear(true);
	}

	public void clear(boolean notifyChanges) {
		_getItems().clear();
		notifyDataChanges(notifyChanges);
	}

	public boolean contains(Item item) {
		return _getItems().contains(item);
	}

	public int indexOf(Item item) {
		return _getItems().indexOf(item);
	}

	protected List<Item> _getItems(){
		return this.items;
	}
	public List<Item> getItems() {
		return Collections.unmodifiableList(_getItems());
	}

	public void setItems(Collection<Item> itemCollection) {
		this.setItems(itemCollection, true);
	}

	public void setItems(Collection<Item> itemCollection, boolean notifyChanges) {
		this.clear(false);
		this.addAll(itemCollection, notifyChanges);
	}

	private void notifyDataChanges(boolean notifyChanges) {
		if(notifyChanges){
			this.notifyDataSetChanged();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return _getItems().size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Item getItem(int position) {
		return _getItems().get(position);
	}

	public int getItemPosition(Item item) {
		return _getItems().indexOf(item);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	public Item findItem(Item item, Comparator<Item> comparator) {
		for(Item i : getItems()){
			if(comparator != null && comparator.compare(i, item) == 0){
				return i;
			}
			else if(comparator == null && i.equals(item)){
				return i;
			}
		}
		return null;
	}
	public Item findItem(CollectionsUtils.PredicateFilter<Item> filter) {
		for(Item i : getItems()){
			if(filter.verify(i)){
				return i;
			}
		}
		return null;
	}

	public void replaceItem(Item newItem, Comparator<Item> comparator) {
		Item item = findItem(newItem, comparator);
		if(item != null){
			int position = getItemPosition(item);
			putItem(position, newItem);
		}
	}

	protected List<Item> buildItemList() {
		return new ArrayList<Item>();
	}

}