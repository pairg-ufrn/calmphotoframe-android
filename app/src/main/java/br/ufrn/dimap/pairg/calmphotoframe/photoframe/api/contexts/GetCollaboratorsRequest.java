package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts.GetCollaboratorsRequest.CollaboratorsWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetCollaboratorsRequest extends BaseGenericRequest<CollaboratorsWrapper, Collection<Collaborator>>{
	
	private Long ctxId;
	
	public GetCollaboratorsRequest(ApiConnector apiConnector, Long ctxId,
			ResultListener<? super Collection<Collaborator>> listener) {
		super(apiConnector, CollaboratorsWrapper.class, listener);
		this.ctxId = ctxId;
	}

	@Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextCollaboratorsUrl(ctxId);
	}

	@Override
	protected Collection<Collaborator> convertToResultType(CollaboratorsWrapper response) {
		List<Collaborator> collaborators = new LinkedList<>();
		if(response != null){
			for(UserWithPermission userWithPermission : response.collaborators){
				collaborators.add(new Collaborator(userWithPermission, userWithPermission.permission));
			}
		}
		
		return collaborators;
	}

	public static  class CollaboratorsWrapper{
		public List<UserWithPermission> collaborators;
	}
}
