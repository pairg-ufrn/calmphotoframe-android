package br.ufrn.dimap.pairg.calmphotoframe.view.actionmode;

import android.support.v7.view.ActionMode;

public interface ActionModeHandler {
    ActionMode startActionMode(ActionMode.Callback callback);
}
