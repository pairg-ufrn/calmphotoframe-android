package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UsersFragment;

public class OnChangeUserUpdateView extends ActivityResultListener<User> {
	private UsersFragment usersView;
	
	public OnChangeUserUpdateView(BaseActivity activity, UsersFragment usersView) {
		super(activity);
		this.usersView = usersView;
	}

	@Override
	public void onResult(User result) {
		usersView.getAdapter().items().replaceItem(result, new UserIdComparator());
	}
}