package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer.CheckMode;

public class CheckableLayoutViewBuilder implements ViewBuilder{
	private ViewBuilder viewBuilderDelegate;
	private int layoutId, layoutContainerId;
	private CheckMode checkMode;

	public CheckableLayoutViewBuilder(ViewBuilder viewBuilder) {
		this(viewBuilder, CheckMode.recursive);
	}
	
	public CheckableLayoutViewBuilder(ViewBuilder viewBuilder, CheckMode checkMode) {
		this.viewBuilderDelegate = viewBuilder;
		this.layoutId = R.layout.checkable_layout;
		this.layoutContainerId = R.id.container;
		this.checkMode = checkMode;
	}
	
	public CheckMode getCheckMode() {
		return checkMode;
	}

	public void setCheckMode(CheckMode checkMode) {
		this.checkMode = checkMode;
	}

	@Override
	public View buildView(Object data, View recycleView, ViewGroup parent) {
		View checkableLayout;
		if(recycleView == null){
			LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
			checkableLayout = layoutInflater.inflate(layoutId, parent, false);
		}
		else{
			checkableLayout = recycleView;
		}
		ViewGroup childContainer = (ViewGroup) checkableLayout.findViewById(layoutContainerId);
		
		if(childContainer instanceof CheckableContainer){
			CheckableContainer checkableContainer = (CheckableContainer)childContainer;
			checkableContainer.setCheckMode(this.checkMode);
		}
		
		View childRecycleView = childContainer.getChildCount() == 0 ? null : childContainer.getChildAt(0);

		if(viewBuilderDelegate != null) {
			View child = viewBuilderDelegate.buildView(data, childRecycleView, childContainer);
			if (child.getParent() == null) {
				childContainer.addView(child);
			}
		}
		
		return checkableLayout;
	}

}
