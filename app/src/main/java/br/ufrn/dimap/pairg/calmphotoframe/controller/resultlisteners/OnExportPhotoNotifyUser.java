package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;

public final class OnExportPhotoNotifyUser extends ActivityResultListener<Object> {
	
	public OnExportPhotoNotifyUser(BaseActivity activity) {
		super(activity);
	}
	
	@Override
	public void onResult(Object result) {
		CharSequence resultMsg = getActivity().getText(R.string.exportPhoto_msg);
		getActivity().notifyUser(String.format(resultMsg.toString(), result)).show();
	}

	@Override
	public void onFailure(Throwable error) {
		getActivity().notifyError(R.string.exportPhoto_error);
	}
}