package br.ufrn.dimap.pairg.calmphotoframe.utils;

import java.util.Properties;

public class PlatformUtils {
    public static boolean isAndroid(){
        Properties properties = System.getProperties();
        return properties.getProperty("java.vm.vendor","").toLowerCase().contains("android")
                || properties.getProperty("java.vm.name","").toLowerCase().contains("dalvik");
    }
}
