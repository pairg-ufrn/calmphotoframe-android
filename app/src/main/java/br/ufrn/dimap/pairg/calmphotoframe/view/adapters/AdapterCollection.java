package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.widget.Adapter;
import android.widget.BaseAdapter;

public class AdapterCollection<Item>{
	private List<Item> items;
	private Adapter adapter;
	
	public AdapterCollection(Adapter adapter) {
		items = buildItemList();
		this.adapter = adapter;
	}
	protected List<Item> buildItemList(){
		return new ArrayList<Item>();
	}
		
	public void add(Item item){
		this.add(item, true);
	}
	public void add(Item item, boolean notifyChanges){
		this.items.add(item);
		this.notifyDataChanges(notifyChanges);
	}
	
	public void addAll(Collection<Item> itemCollection) {
		this.addAll(itemCollection, true);
	}
	public void addAll(Collection<Item> itemCollection, boolean notifyChanges) {
		this.items.addAll(itemCollection);
		this.notifyDataChanges(notifyChanges);
	}
	
	public void removeAll(Collection<Item> itemCollection) {
		this.removeAll(itemCollection, true);
	}
	public void removeAll(Collection<Item> itemCollection, boolean notifyChanges) {
		this.items.removeAll(itemCollection);
		this.notifyDataChanges(notifyChanges);
	}

	public void remove(Item item){
		this.remove(item, true);
	}
	public void remove(Item item, boolean notifyChanges){
		this.items.remove(item);
		this.notifyDataChanges(notifyChanges);
	}
	
	public void clear(){
		this.clear(true);
	}
	public void clear(boolean notifyChanges){
		this.items.clear();
		notifyDataChanges(notifyChanges);
	}
	
	public boolean contains(Item item){
		return items.contains(item);
	}
	public int indexOf(Item item) {
		return this.items.indexOf(item);
	}
	
	public List<Item> getItems(){
		return Collections.unmodifiableList(items);
	}
	public void setItems(Collection<Item> itemCollection){
		this.setItems(itemCollection, true);
	}
	public void setItems(Collection<Item> itemCollection, boolean notifyChanges){
		this.clear(false);
		this.addAll(itemCollection, notifyChanges);
	}

	private void notifyDataChanges(boolean notifyChanges) {
		if(notifyChanges && adapter instanceof BaseAdapter){
			((BaseAdapter)adapter).notifyDataSetChanged();
		}
	}
	
	public int getCount() {
		return items.size();
	}
	public Item getItem(int position) {
		return items.get(position);
	}
	public long getItemId(int position) {
		return position;
	}
}