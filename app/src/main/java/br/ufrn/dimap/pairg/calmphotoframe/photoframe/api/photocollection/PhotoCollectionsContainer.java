package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.Collection;
import java.util.Collections;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

/** Class to help in deserialization*/
public class PhotoCollectionsContainer{
	public Collection<PhotoCollection> collections;
	public PhotoCollectionsContainer(){
		collections = Collections.emptyList();
	}
}