package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.SaveInput;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

import static android.content.ContentValues.TAG;

public class UserProfileBinder extends InflaterViewBinder<User>{

    private UserImageViewBinder userImageViewBinder;
    private View changePasswordTrigger;
    private Context context;

    private User boundUser;

    private ActionListener<? super User> changePassword;
    private ArgsCommand<User, ? extends User> saveNameCommand;
    private ArgsCommand<User, ? extends User> changePhotoCommand;

    private SaveInput saveName;

    public UserProfileBinder() {
        this(R.layout.fragment_userprofile);
    }
    public UserProfileBinder(int layoutId) {
        super(layoutId);

        userImageViewBinder = new UserImageViewBinder()
                .imageViewId(R.id.profile_userimage)
                .useCircularDrawable(false);

        saveName = new SaveInput();
    }

    @Override
    public void setup(View view) {
        this.context = view.getContext();

        this.userImageViewBinder.setup(view);

        changePasswordTrigger = ViewUtils.getView(view, R.id.profile_changepassword_trigger);
        changePasswordTrigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePasswordTrigger();
            }
        });

        View changePhotoTrigger = ViewUtils.getView(view, R.id.profile_changePhoto_trigger);
        changePhotoTrigger.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onChangePhotoTrigger();
            }
        });

        setupChangeName(view);

        refreshView();
    }

    protected void setupChangeName(View view) {
        EditText nameEditor = extractEditText(view);
        saveName.bindView(nameEditor);

        Activity activity = getActivity();
        if(activity instanceof AppCompatActivity) {
            saveName.setActionModeHandler((AppCompatActivity) activity);
        }
    }

    protected EditText extractEditText(View view) {
        View nameEdit = ViewUtils.getView(view, R.id.profile_name_edit);

        EditText nameEditor;
        if(nameEdit instanceof TextInputLayout){
            nameEditor = ((TextInputLayout)nameEdit).getEditText();
        }
        else{
            nameEditor = (EditText)nameEdit;
        }
        return nameEditor;
    }

    @Override
    public void bind(User entity) {
        bind(entity, null);
    }
    public void bind(User entity, Drawable drawable) {
        Logging.i(TAG, "bind(" + entity + ", " + drawable + ")");

        this.boundUser = entity;
        this.userImageViewBinder.bind(entity, drawable);
        this.saveName.setText(entity.getName());

        refreshView();
    }

    protected void refreshView() {
        changePasswordTrigger.setEnabled(this.boundUser != null);
    }

    public ActionListener<? super User> getChangePasswordAction() {
        return changePassword;
    }

    public void setChangePasswordAction(ActionListener<? super User> changePassword) {
        this.changePassword = changePassword;
    }

    @SuppressWarnings("unused")
    public ArgsCommand<User, ?> getSaveNameCommand() {
        return saveNameCommand;
    }

    public void setSaveNameCommand(ArgsCommand<User, ? extends User> saveNameCommand) {
        this.saveNameCommand = saveNameCommand;

        if(this.saveNameCommand == null){
            this.saveName.setSaveAction(null);
            return;
        }

        this.saveName.setSaveAction(new SaveNameCommandWrapper(this.saveNameCommand));
    }


    public Drawable getUserImage() {
        return this.userImageViewBinder.getDrawable();
    }

    @SuppressWarnings("unused")
    public void setUserImage(Drawable drawable){
        this.userImageViewBinder.setDrawable(drawable);
    }

    private void rebind() {
        userImageViewBinder.bind(boundUser);
    }

    public void setActionModeHandler(ActionModeHandler actionModeHandler){
        this.saveName.setActionModeHandler(actionModeHandler);
    }
    @SuppressWarnings("unused")
    public void setActionModeHandler(AppCompatActivity activity){
        this.saveName.setActionModeHandler(activity);
    }

    protected void onChangePasswordTrigger() {
        fireAction(getChangePasswordAction(), boundUser);
    }
    protected void onChangePhotoTrigger() {
        //fireAction(getChangePhotoAction(), boundUser);
        if(changePhotoCommand != null){
            changePhotoCommand.execute(boundUser, new OnResultChangePhoto());
        }
    }

    protected <T> void fireAction(ActionListener<T> action, T value){
        if(action != null){
            action.execute(value);
        }
    }

    private Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    private Context getContext(){
        return context;
    }

    @SuppressWarnings("unused")
    public ArgsCommand<User, ?> getChangePhotoCommand() {
        return changePhotoCommand;
    }

    public void setChangePhotoCommand(ArgsCommand<User, ? extends User> changePhotoCommand) {
        this.changePhotoCommand = changePhotoCommand;
    }

    public void setUserImageLoader(ImageLoader<User> userImageLoader) {
        this.userImageViewBinder.setUserImageLoader(userImageLoader);
    }

    public ImageLoader<User> getUserImageLoader() {
        return userImageViewBinder.getUserImageLoader();
    }

    private class SaveNameCommandWrapper implements ActionListener<String> {
        private final ArgsCommand<User, ?> saveNameCommand;

        public SaveNameCommandWrapper(ArgsCommand<User, ?> saveNameCommand) {
            this.saveNameCommand = saveNameCommand;
        }

        @Override
        public void execute(String newName) {
            if(boundUser == null){
                return;
            }
            User user = boundUser.clone();

            if(!isNameEquals(user.getName(), newName)){
                user.setName(newName);

                //TODO: handle command response
                saveNameCommand.execute(user, null);
            }
        }

        protected boolean isNameEquals(String currentName, String newName) {
            if(currentName == null || newName == null){
                //noinspection StringEquality
                return currentName == newName;
            }

            return currentName.equals(newName);
        }
    }

    private class OnResultChangePhoto extends BasicResultListener<User> {
        @Override
        public void onResult(User result) {
            super.onResult(result);

            bind(result);
        }
    }
}
