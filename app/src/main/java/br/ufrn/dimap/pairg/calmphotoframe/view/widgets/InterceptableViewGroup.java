package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class InterceptableViewGroup extends FrameLayout {
	public interface OnInterceptTouchListener{
		boolean onInterceptTouchEvent(MotionEvent event);
	}

	private OnInterceptTouchListener onInterceptTouchListener;

	public InterceptableViewGroup(Context context) {
		super(context);
		init();
	}
	public InterceptableViewGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	public InterceptableViewGroup(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		onInterceptTouchListener = null;
	}

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handled = super.dispatchTouchEvent(ev);

        return delegateEvent(ev) || handled;
    }

    private boolean delegateEvent(MotionEvent ev) {
        return onInterceptTouchListener != null
                && onInterceptTouchListener.onInterceptTouchEvent(ev);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
        //Ensure that the touch event will be handled, even if not handled by any child
		return true;
	}

	public OnInterceptTouchListener getOnInterceptTouchListener() {
		return onInterceptTouchListener;
	}
	public void setOnInterceptTouchListener(OnInterceptTouchListener touchListener) {
		this.onInterceptTouchListener = touchListener;
	}
}
