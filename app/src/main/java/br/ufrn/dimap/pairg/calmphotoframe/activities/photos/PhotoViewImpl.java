package br.ufrn.dimap.pairg.calmphotoframe.activities.photos;

import android.util.Log;

import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoView;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo.PhotoViewerFragment;

public class PhotoViewImpl implements PhotoView {
    private PhotoViewerFragment photoViewFragment;

    public PhotoViewImpl() {
        this(null);
    }

    public PhotoViewImpl(PhotoViewerFragment photoViewFragment) {
        this.photoViewFragment = photoViewFragment;
    }

    public PhotoViewerFragment getPhotoViewFragment() {
        return photoViewFragment;
    }

    public void setPhotoViewFragment(PhotoViewerFragment photoViewFragment) {
        this.photoViewFragment = photoViewFragment;
    }

    @Override
    public Photo getEntity() {
        return getCurrentPhoto();
    }

    @Override
    public Photo getCurrentPhoto() {
        return photoViewFragment == null ? null : photoViewFragment.getPhotoDescriptor();
    }

    @Override
    public void setCurrentPhoto(Photo photo) {
        Photo currentPhoto = getCurrentPhoto();
        if (currentPhoto == null || !currentPhoto.equals(photo)) {
            if (changeImage(photo)) {//Only change photo description, if can change image
                this.photoViewFragment.setPhotoDescriptor(photo);
                photoViewFragment.showState(PhotoViewerFragment.State.Default);
            }
        }
    }

    @Override
    public void refreshView() {
        displayRemoteImage(getCurrentPhoto());
    }

    @Override
    public void updatePhoto(Photo photo) {
        if (getCurrentPhoto() != null && getCurrentPhoto().getId().equals(photo.getId())) {
            photoViewFragment.setPhotoDescriptor(photo.clone());
        }
    }

    @Override
    public void removeCurrentPhoto() {
        setCurrentPhoto(null);
        if (isViewActive()) {
            photoViewFragment.showState(PhotoViewerFragment.State.Empty);
        }
    }

    private boolean isViewActive() {
        return photoViewFragment != null;
    }

    private boolean changeImage(Photo photo) {
        return displayRemoteImage(photo);
    }

    private boolean displayRemoteImage(Photo photo) {
        if (isViewActive()) {
            if (photo == null) {
//                getPhotoViewFragment().showDefaultPhoto();
                getPhotoViewFragment().showState(PhotoViewerFragment.State.Empty);
            } else {
                getPhotoViewFragment().showWait();
                getPhotoFrame().photos().getImage(photo, new OnResultShowImage(getPhotoViewFragment()));
            }

            return true;
        }
        return false;
    }

    private ApiClient getPhotoFrame() {
        return ApiClient.instance();
    }

    private static class OnResultShowImage extends BasicResultListener<PhotoImageWrapper> {
        PhotoViewerFragment photoViewFragment;

        public OnResultShowImage(PhotoViewerFragment photoViewFragment) {
            this.photoViewFragment = photoViewFragment;
        }

        @Override
        public void onResult(PhotoImageWrapper result) {
            showPhoto(result);
        }

        @Override
        public void onFailure(Throwable error) {
            Log.e(getClass().getName(), "Failed to load image", error);
            showFailure();
        }

        protected void showPhoto(PhotoImageWrapper result) {
            photoViewFragment.setPhotoImage(result.getImage());
            photoViewFragment.hideWait();
        }

        protected void showFailure() {
            photoViewFragment.hideWait();
            photoViewFragment.showState(PhotoViewerFragment.State.Error);
        }
    }
}