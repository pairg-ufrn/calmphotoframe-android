package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.BuildConfig;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMarker;
import br.ufrn.dimap.pairg.calmphotoframe.view.DragController;
import br.ufrn.dimap.pairg.calmphotoframe.view.DragTouchListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnChangeViewSizeListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.PhotoMarkerView;
import br.ufrn.dimap.pairg.calmphotoframe.view.Size;
import br.ufrn.dimap.pairg.calmphotoframe.view.ViewHelper;

/**
 * **/
public class PhotoMarkersViewerFragment extends Fragment implements OnClickListener{

	public interface PhotoMarkerViewerListener {
		void onPhotoMarkerClicked(PhotoMarker photoMark);
	}
	
	private class DragPhotoMarkersListener extends DragController.SimpleDragListener{
		@Override
		public void onStopDrag(View v, float viewScreenX, float viewScreenY) {				
			PhotoMarkerView photoMarkerView = PhotoMarkerView.getFromView(v);
			if(photoMarkerView != null){
				photoMarkerView.updatePhotoMarkLocationIn(markersImageView);
			}
			
			//TODO: Notificar que photoMark mudou
		}
	}
	
	private PhotoMarkerViewerListener photoMarkerViewerListener;
	private final List<PhotoMarker> photoMarkers;
	
	private Bitmap image;
	private ImageView markersImageView;
	private TextView photoLabelsView;
	private int labelsVisibility;
	private String undefinedLabelText;
	
	private boolean viewsReady;
	private final List<PhotoMarkerView> photoMarkerViews;
	private ViewGroup markersContainer;
    private ViewGroup.LayoutParams defaultContainerLayout;

	private DragController dragController;
	private OnTouchListener onTouchImageListener;
	
	public PhotoMarkersViewerFragment(){
		photoMarkers = new ArrayList<>();
		photoMarkerViews = new ArrayList<>();
		
		dragController = new DragController();
		dragController.setDragListener(new DragPhotoMarkersListener());
        dragController.setBoundsController(new PhotoMarkerView.BoundsController());
		
		labelsVisibility = View.VISIBLE;
		undefinedLabelText = "";
	}

	/* *************************************** View Creation ************************************/
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photomarkersviewer, container, false);
        setupViews(rootView);
        
        if(this.photoMarkers.size() > 0){
        	//Precisa criar uma nova instância, pois em setPhotoMarkers 'photoMarkers' terá seus filhos removidos
        	this.setPhotoMarkers(new ArrayList<>(photoMarkers));
        }
        
        return rootView;
    }
	private void setupViews(View rootView) {
		this.photoLabelsView = (TextView)rootView.findViewById(R.id.photoMarkersViewer_namesList);
		this.photoLabelsView.setVisibility(labelsVisibility);
        setupMarkersContainer(rootView);
    	setupImageView(rootView);
		
		this.viewsReady = true;
	}

    private void setupMarkersContainer(View rootView) {
        markersContainer = (ViewGroup)rootView.findViewById(R.id.photoMarkersViewer_markContainer);
        if(markersContainer != null){
            markersContainer.getViewTreeObserver().addOnGlobalLayoutListener(
                    new OnChangeLayoutPositionMarkers(markersContainer));
            this.defaultContainerLayout = markersContainer.getLayoutParams();
        }
    }

    private void setupImageView(View rootView) {
		markersImageView = (ImageView)rootView.findViewById(R.id.photoMarkersViewer_image);
    	markersImageView.setClickable(true);
    	markersImageView.setOnTouchListener(onTouchImageListener);
    	bindImageToView(getImage());

    	//Adiciona listener para modificar layout dos marcadores quando imagem for modificada
//    	markersImageView.getViewTreeObserver().addOnGlobalLayoutListener(new OnChangeImageLayoutMarkers(markersImageView));
	}
    /* *****************************************************************************************/

	@SuppressWarnings("unused")
	public String getUndefinedLabelText() {
		return undefinedLabelText;
	}
	public void setUndefinedLabelText(String undefinedLabelText) {
		this.undefinedLabelText = undefinedLabelText;
		if(viewsReady){
			layoutPhotoLabels();
		}
	}
	public boolean isEditable() {
		return dragController.isEnable();
	}
	public void setEditable(boolean editable) {
		this.dragController.setEnable(editable);
	}
	
	/* ***************************************** Labels *************************************/

    @SuppressWarnings("unused")
	public void showLabels(){
		if(!isLabelsVisible()){
			this.labelsVisibility = View.VISIBLE;
			updateLabelsVisibility();
			layoutPhotoLabels();//Atualiza labels exibidos
		}
	}
	public void hideLabels(){
		this.labelsVisibility = View.GONE;
		updateLabelsVisibility();
	}
	public boolean isLabelsVisible() {
		return (this.labelsVisibility == View.VISIBLE);
	}
	private void updateLabelsVisibility() {
		if(photoLabelsView != null){
			this.photoLabelsView.setVisibility(labelsVisibility);
		}
	}

	/* ****************************************** Image ************************************************/

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap photoImage) {
		this.image = photoImage;
        bindImageToView(photoImage);
    }


    protected void bindImageToView(final Bitmap photoImage) {

        if(this.markersImageView != null){

            resizeContainer(photoImage);

            if(photoImage != null){
                Log.d(getClass().getSimpleName(), "markersImageView.setImageBitmap");
                this.markersImageView.setImageBitmap(photoImage);
            }
            else{
                showDefaultImage();
            }
        }
    }

    public void showDefaultImage() {
		image = null;
		if(markersImageView != null){
            //TODO: maybe change default image
			this.markersImageView.setImageResource(android.R.color.transparent);
		}
	}

	/* ***********************************PhotoMarkerPositionListener*********************/

    @SuppressWarnings("unused")
	public PhotoMarkerViewerListener getPhotoMarkerPositionListener() {
		return photoMarkerViewerListener;
	}
	public void setPhotoMarkerPositionListener(PhotoMarkerViewerListener photoMarkerViewerListener) {
		this.photoMarkerViewerListener = photoMarkerViewerListener;
	}

	/* ***********************************Controle de marcadores da imagem*********************/
	public void addPhotoMarkerAt(PhotoMarker photoMarker, final float screenX, final float screenY) {
		final PhotoMarkerView photoMarkerView = createPhotoMarkView(photoMarker);
		this.photoMarkerViews.add(photoMarkerView);
		this.markersContainer.addView(photoMarkerView.getView());
		int relativeLoc[] = ViewHelper.getLocationRelativeTo(new int[]{(int)screenX, (int)screenY}, this.markersImageView);
		ViewHelper.setViewTranslation(photoMarkerView.getView(), relativeLoc[0], relativeLoc[1]);
		
		//Adia atualização do model p/ quando View (do marcador) estiver pronta.
        // Isto é necessario para "salvar" posição inicial do marcador
		photoMarkerView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {
				//Atualiza posicionamento de PhotoMark com base na localização da View
				photoMarkerView.updatePhotoMarkLocationIn(markersContainer);
				photoMarkerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
		
		this.photoMarkers.add(photoMarker);
		
		assertMarkersAndViewsAreConsistent();
		
		//TODO: (?) notificar a listener que marcador foi adicionado
	}

	private void assertMarkersAndViewsAreConsistent() {
		if(photoMarkers.size() != this.photoMarkerViews.size()
				&& BuildConfig.DEBUG){
			throw new AssertionError("PhotoMarker list and PhotoMarkerView have different sizes.");
		}
	}

	public void removePhotoMarker(PhotoMarker photoMarker) {
		int index = photoMarkers.indexOf(photoMarker);
		this.markersContainer.removeView(photoMarkerViews.get(index).getView());
		this.photoMarkers.remove(index);
		this.photoMarkerViews.remove(index);

		for(int i =0; i<photoMarkers.size(); ++i){
			PhotoMarker marker = photoMarkers.get(i);
			marker.setIndex(i + 1);
			photoMarkerViews.get(i).update(marker);
		}
		
		//TODO: (?) notificar a listener que marcador foi removido???
	}

	public PhotoMarkerView getMarkerView(PhotoMarker photoMarker) {
        int index = photoMarkers.indexOf(photoMarker);
        if(index >=0){
            return photoMarkerViews.get(index);
        }
		return null;
	}

    @SuppressWarnings("unused")
	public List<PhotoMarker> getPhotoMarkers(){
		return Collections.unmodifiableList(this.photoMarkers);
	}
	public void setPhotoMarkers(List<PhotoMarker> markers){
		this.clearMarkers();
		if(markers != null){
			this.photoMarkers.addAll(markers);
		}

		if(this.viewsReady){
			adaptPhotoMarkersViewCount(photoMarkers);
			layoutPhotoMarkers();
		}
	}
	
	public void clearMarkers() {
		this.photoMarkers.clear();
	}

	/** Atualiza o posicionamento dos marcadores na tela, com base no definido no model.
	 * Caso o posicionamento dos marcadores seja modificado fora deste fragmento, esta 
	 * função deve ser chamada.*/
	public void layoutPhotoMarkers() {
		if(viewsReady){
            Log.d(getClass().getSimpleName(), "layoutMarkers");

			for(int i=0; i < this.photoMarkers.size(); ++i){
				PhotoMarkerView markView = photoMarkerViews.get(i);
				markView.setPhotoMark(photoMarkers.get(i));
				markView.positionIn(markersImageView);
			}
			layoutPhotoLabels();
			markersContainer.invalidate();
		}
	}
	/** Cria ou destroi views de marcadores para que seu número corresponda aos marcadores armazenados. 
	 * Busca reaproveitar estrutura existente.*/
	private void adaptPhotoMarkersViewCount(List<PhotoMarker> somePhotoMarks) {
		if(this.photoMarkerViews.size() < somePhotoMarks.size()){
			for(int i = photoMarkerViews.size(); i < somePhotoMarks.size(); ++i){
				this.photoMarkerViews.add(createPhotoMarkView(somePhotoMarks.get(i)));
				this.markersContainer.addView(photoMarkerViews.get(i).getView());
			}
		}
		else if(this.photoMarkerViews.size() > somePhotoMarks.size()){
			final int diffSize = (photoMarkerViews.size() - somePhotoMarks.size());
			
			for(int i = diffSize - 1; i >= 0; --i){			
				this.markersContainer.removeView(photoMarkerViews.get(i).getView());
				this.photoMarkerViews.remove(i);
			}
		}
	}
	@SuppressLint("ClickableViewAccessibility")
	private PhotoMarkerView createPhotoMarkView(PhotoMarker photoMark) {
		PhotoMarkerView markView = new PhotoMarkerView(markersContainer);
		markView.setPhotoMark(photoMark);

        DragTouchListener dragTouchListener = new DragTouchListener(getContext(), dragController, markersImageView);
        dragTouchListener.setOnClickListener(this);
		markView.setOnTouchListener(dragTouchListener);

		return markView;
	}
	
	private void layoutPhotoLabels() {
		StringBuilder marksText = new StringBuilder();
		int markIndex = 1;
		for(PhotoMarker mark : this.photoMarkers){
			marksText.append(markIndex);
			marksText.append(". ");
			String labelValue = mark.getIdentification().length() == 0 
												? undefinedLabelText 
												: mark.getIdentification();
			marksText.append(labelValue);
			marksText.append("   ");
			++markIndex;
		}
		photoLabelsView.setText(marksText.toString());
	}

	/* *********************************** Listeners *********************************************** */

    @SuppressWarnings("unused")
	public OnTouchListener getOnTouchImageListener() {
		return this.onTouchImageListener;
	}

	public void setOnTouchImageListener(OnTouchListener onTouchListener) {
		this.onTouchImageListener = onTouchListener;
		if(markersImageView != null){
			this.markersImageView.setOnTouchListener(onTouchListener);
		}
	}
	@Override
	public void onClick(View v) {
		PhotoMarkerView photoMarkerView = PhotoMarkerView.getFromView(v);

		if(photoMarkerView != null && this.photoMarkerViewerListener != null){
			this.photoMarkerViewerListener.onPhotoMarkerClicked(photoMarkerView.getPhotoMark());
		}
	}



    /* ************************************ Layout Containers **************************************/

    private void resizeContainer(final Bitmap photoImage) {
        //clear layout
        markersContainer.setLayoutParams(defaultContainerLayout);
        markersContainer.requestLayout();
        markersContainer.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                onClearContainerLayout(photoImage);

                ViewTreeObserver obs = markersContainer.getViewTreeObserver();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });

    }

    protected void onClearContainerLayout(Bitmap photoImage) {
        Size requiredSize = calculateExpectedImageSize(markersContainer, photoImage);

        if(requiredSize != null){
            markersContainer.setLayoutParams(
                    buildMarkersContainerParams(requiredSize, new Size(markersContainer.getWidth(), markersContainer.getHeight())));
            markersContainer.requestLayout();
        }
    }

    @NonNull
    protected ViewGroup.LayoutParams buildMarkersContainerParams(Size size, Size maxSize) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size.getWidth(), size.getHeight());

        if(size.getHeight() == maxSize.getHeight()){
            Log.d(PhotoMarkersViewerFragment.class.getSimpleName(), "max height!");
            params.height = 0;
            params.weight = 1;
        }

        if(size.getWidth() == maxSize.getWidth()){
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        }

        if(size.getWidth() < maxSize.getWidth()){
            photoLabelsView.getLayoutParams().width = size.getWidth();
        }
        else{
            photoLabelsView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }

//        photoLabelsView.requestLayout();

        return params;
    }

    private Size calculateExpectedImageSize(ViewGroup container, Bitmap photoImage) {
        if(photoImage == null){
            return null;
        }
        float ratio = photoImage.getWidth()/(float)photoImage.getHeight();

        Size maxSize = new Size(container.getWidth(), container.getHeight());

        int wDiff = Math.abs(photoImage.getWidth() - maxSize.getWidth());
        int hDiff = Math.abs(photoImage.getHeight() - maxSize.getHeight());

        Size size = new Size(maxSize);
        if(wDiff < hDiff){
            size.setHeight((int) (maxSize.getWidth()/ratio));
        }
        else{
            size.setWidth((int) (maxSize.getHeight() * ratio));
        }

        return size;
    }

    private class OnChangeLayoutPositionMarkers extends OnChangeViewSizeListener<ViewGroup> {

        public OnChangeLayoutPositionMarkers(ViewGroup container) {
            super(container);
        }

        protected void onSizeChanged(ViewGroup container, Size lastSize){
            layoutPhotoMarkers();
        }
    }
}
