package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;
import java.util.Collections;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class DelegatePhotoResultAsPhotoComponents extends ResultListenerDelegator<Photo, Collection<PhotoComponent>> {
	public DelegatePhotoResultAsPhotoComponents(ResultListener<? super Collection<PhotoComponent>> delegate) {
		super(delegate);
	}

	@Override
	protected Collection<PhotoComponent> convertResult(Photo result) {
		return Collections.singleton(new PhotoComponent(result));
	}
}