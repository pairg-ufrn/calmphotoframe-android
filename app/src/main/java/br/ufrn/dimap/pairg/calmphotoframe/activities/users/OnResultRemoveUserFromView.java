package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class OnResultRemoveUserFromView extends ActivityResultListener<User> {
	UsersManagementController controller;
	public OnResultRemoveUserFromView(BaseActivity activity
			, UsersManagementController usersController) 
	{
		super(activity);
		this.controller = usersController;
	}

	@Override
	public void onResult(User result) {
		controller.removeFromView(result);
	}
}