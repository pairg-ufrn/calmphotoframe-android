package br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class DefaultCollectionFilter<T> implements CollectionFilter<T> {
    @Override
    public List<T> filter(Collection<? extends T> items, CharSequence constraint) {
        if(constraint == null || constraint.length() == 0){
            return new ArrayList<>(items);
        }

        List<T> filtered = new ArrayList<>(items.size());

        filterCollection(items, constraint, filtered);

        return filtered;
    }

    protected void filterCollection(Collection<? extends T> items, CharSequence constraint, List<T> filtered) {
        String prefix = constraint.toString().toLowerCase();

        for (T item : items){
            if(item != null && match(item, prefix)){
                filtered.add(item);
            }
        }
    }

    protected boolean match(T item, String prefix) {
        return matchItem(item, prefix);
    }

    protected <ItemType> boolean matchItem(ItemType item, String prefix){
        String itemStr = item.toString().toLowerCase();

        if (itemStr.startsWith(prefix)) {
            return true;
        }

        final String[] words = itemStr.split(" ");
        for (String word : words) {
            if (word.startsWith(prefix)) {
                return true;
            }
        }

        return false;
    }
}
