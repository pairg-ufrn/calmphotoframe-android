package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.UserProfileController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

import static br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginMode.Login;

public class UserProfileActivity extends NavActivity {

    UserProfileFragment userProfileFragment;
    UserProfileController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setupToolbar();

        userProfileFragment =
                (UserProfileFragment) getSupportFragmentManager().findFragmentById(R.id.userprofile_fragment);

        controller = new UserProfileController(this);
        controller.setup(userProfileFragment);
    }

    @Override
    protected void onResume() {
        super.onResume();

        userProfileFragment.setUser(ApiClient.instance().getCurrentUser());
    }
}
