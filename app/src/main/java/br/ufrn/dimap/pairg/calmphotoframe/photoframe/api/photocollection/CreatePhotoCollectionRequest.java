package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class CreatePhotoCollectionRequest extends CommonGenericRequest<PhotoCollection>{
	private String collectionName;
	private Long parentCollectionId;
	
	public CreatePhotoCollectionRequest(ApiConnector apiConnector, 
			String contextName, ResultListener<? super PhotoCollection> listener) 
	{
		this(apiConnector, contextName, null, listener);
	}

	public CreatePhotoCollectionRequest(ApiConnector apiConnector,
			String contextName, PhotoCollection parentContext,
			ResultListener<? super PhotoCollection> listener) {
		super(apiConnector, PhotoCollection.class, listener);
		this.collectionName = contextName;
		this.parentCollectionId = parentContext == null ? null : parentContext.getId();
	}

	@Override
	public RequestHandler execute(){
		return postJsonRequest(new CollectionWithParent(collectionName, parentCollectionId));
	
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().collectionsUrl();
	}
	
	public static class CollectionWithParent extends PhotoCollection{
		private static final long serialVersionUID = 3445143463350758705L;
		public Long parentId;
		
		public CollectionWithParent() {
			this(null, null);
		}

		public CollectionWithParent(String collectionName, Long parentCollectionId) {
			super();
			this.parentId = parentCollectionId;
			this.setName(collectionName);
		}
	}
}
