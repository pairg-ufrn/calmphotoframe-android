package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/* adaptado de: https://developer.android.com/guide/topics/ui/dialogs.html#DialogFragment*/
@Deprecated //Use {@link SimpleMaterialDialogFragment}
public class SimpleDialogFragment extends DialogFragment {
	public interface DialogResponseListener{
		boolean onResponse(SimpleDialogFragment dialog, boolean positive);
	}
	private CharSequence dialogTitle;
	private CharSequence dialogMessage;
	private CharSequence okMessage, cancelMessage;
	private DialogResponseListener dialogResponseListener;
	private int layoutId;
	private boolean isModal;
	
	public SimpleDialogFragment() {
		super();
		this.dialogMessage = "";
		this.okMessage = null;
		this.cancelMessage = null;
		this.dialogResponseListener = null;
		this.layoutId = 0;
		isModal = false;
	}

	public CharSequence getDialogTitle()   { return dialogTitle;   }
	public CharSequence getDialogMessage() { return dialogMessage; }
	public CharSequence getOkMessage() 	   { return okMessage;     }
	public CharSequence getCancelMessage() { return cancelMessage; }
	public DialogResponseListener getDialogResponseListener() { return dialogResponseListener;}

	public void setDialogTitle(CharSequence dialogTitle) {
		this.dialogTitle = dialogTitle;
	}
	public void setDialogMessage(CharSequence dialogMessage) {
		this.dialogMessage = dialogMessage;
	}
	public void hideDialogMessage(){
		this.setDialogMessage(null);
	}
	public void setOkMessage(CharSequence okMessage) {
		this.okMessage = okMessage;
	}
	public void hideOkButton() {
		this.okMessage = null;
	}
	public void setCancelMessage(CharSequence cancelMessage) {
		this.cancelMessage = cancelMessage;
	}
	public void hideCancelButton() {
		this.cancelMessage = null;
	}
	public SimpleDialogFragment setDialogResponseListener(DialogResponseListener dialogResponseListener) {
		this.dialogResponseListener = dialogResponseListener;
		return this;
	}

	public int getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(int layoutId) {
		this.layoutId = layoutId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(layoutId > 0 && !isModal){
        	return createView(layoutId, inflater, container);
        }
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		isModal = true;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if(layoutId > 0 && isModal){
        	LayoutInflater inflater = getActivity().getLayoutInflater();
        	View view = createView(layoutId, inflater, null);
        	builder.setView(view);
        }
        builder.setMessage(this.getDialogMessage());
        if(isValidMessage(dialogTitle)){
        	builder.setTitle(dialogTitle);
        }
        if(isValidMessage(getOkMessage())){
        	builder.setPositiveButton(getOkMessage(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    notifyResponse(true);
                }
            });
        };
        if(isValidMessage(getCancelMessage())){
        	builder.setNegativeButton(getCancelMessage(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    notifyResponse(false);
                }
            })
            .setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
                    notifyResponse(false);
				}
			});
        };
               
        return builder.create();
	}	

	public void onStart(){
		super.onStart();
		
		final AlertDialog d = (AlertDialog)getDialog();
	    if(d != null)
	    {
	        Button positiveButton = (Button) d.getButton(Dialog.BUTTON_POSITIVE);
	        positiveButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(notifyResponse(true)){
                        d.dismiss();
                    }
                }
            });
	    }
	}
	
	protected View createView(int layoutId, LayoutInflater inflater, ViewGroup container) {
		return inflater.inflate(layoutId, container, false);
	}

	private boolean isValidMessage(CharSequence message) {
		return message != null && message.length() != 0;
	}

	protected boolean notifyResponse(boolean response) {
		if(dialogResponseListener != null){
			return dialogResponseListener.onResponse(this, response);
		}
		return true;
	}

}
