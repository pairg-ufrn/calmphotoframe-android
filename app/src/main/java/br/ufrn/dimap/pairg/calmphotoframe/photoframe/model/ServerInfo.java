package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerInfo{
    private String name, address;
    private Endpoint endpoint;

    public ServerInfo(){
        this("","");
    }

    public ServerInfo(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        this.endpoint = null;
    }

    public boolean isLocal() {
        if (endpoint == null){
            this.endpoint = new Endpoint();

            try {
                this.endpoint.setFromUrl(address);
            }
            catch (Endpoint.InvalidUrlException ex){
                return false;
            }
        }

        return endpoint.isLocal();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if(getAddress() != null){
            builder.append(getAddress());
        }

        if(getName() != null && !getName().isEmpty()) {
            if(getAddress() != null && !getAddress().isEmpty()){
                builder.append(" ");
            }
            builder.append("[").append(getName()).append("]");
        }

        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ServerInfo) || obj == null){
            return false;
        }

        ServerInfo other = (ServerInfo)obj;

        return equalsOrNull(getName(), other.getName())
                && equalsOrNull(getAddress(), other.getAddress());
    }

    boolean equalsOrNull(Object obj1, Object obj2){
        final boolean isNull1 = (obj1 == null);
        final boolean isNull2 = (obj2 == null);

        if(isNull1 || isNull2){
            return isNull1 && isNull2;
        }

        return obj1.equals(obj2);
    }



    //(<palavra><espaço em branco>*)*[<endereço:palavras>]
    protected static final String WORD  = "[^\\s\\[\\]]+\\s*";
    protected static final String WORDS = "(?:" + WORD +")+";
//    protected static final String PATTERN_STR = String.format("(<name>%s)\\[(<address>%s)\\]", WORDS, WORDS);
    protected static final String PATTERN_STR =  String.format("[\\s]*(%s)[\\s]*(?:\\[(%s)\\][\\s]*)?", WORD, WORDS);
    protected static final Pattern SERVER_PATTERN = Pattern.compile(PATTERN_STR, Pattern.CASE_INSENSITIVE);

    public static ServerInfo fromText(CharSequence sequence) {
        Matcher matcher = SERVER_PATTERN.matcher(sequence);

        if(!matcher.matches()){
            return null;
        }

        String address = trim(matcher.group(1));
        String name = trim(matcher.group(2));

        return new ServerInfo(name, address);
    }

    protected static String trim(String text){
        return text == null ? "" : text.trim();
    }
}
