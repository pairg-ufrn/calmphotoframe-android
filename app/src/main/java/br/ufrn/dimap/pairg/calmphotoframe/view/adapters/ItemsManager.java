package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Manage the items on a RecyclerView adapter
 */
public class ItemsManager<T> {
    private RecyclerView.Adapter<?> adapter;

    private List<T> items;

    public ItemsManager() {
        this(null);
    }
    public ItemsManager(@Nullable RecyclerView.Adapter<?> adapter) {
        this.items = new ArrayList<>();
        this.adapter = adapter;
    }

    public RecyclerView.Adapter<?> getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter<?> adapter) {
        this.adapter = adapter;
    }

    public boolean hasAdapter(){
        return adapter != null;
    }

    public int count(){
        return  items.size();
    }

    public boolean isEmpty() {
        return count() == 0;
    }

    public T get(int idx) {
        return items.get(idx);
    }

    public int indexOf(T item){
        return items.indexOf(item);
    }

    public void set(Collection<? extends T> items){
        this.items.clear();
        if(items != null){
            this.items.addAll(items);
        }

        if(hasAdapter()){
            getAdapter().notifyDataSetChanged();
        }
    }

    public void addItem(T item) {
        this.items.add(item);
        if(hasAdapter()){
            getAdapter().notifyItemInserted(items.size() - 1);
        }
    }

    public void remove(T item) {
        int index = indexOf(item);
        items.remove(index);
        adapter.notifyItemRemoved(index);
    }

    public T findItem(T item, Comparator<T> comparator) {
        int idx = findItemIdx(item, comparator);

        return idx < 0 ? null : get(idx);
    }
    protected int findItemIdx(T item, Comparator<T> comparator) {
        int idx=0;
        for(T i : this.items){
            if(comparator != null && comparator.compare(i, item) == 0){
                return idx;
            }
            else if(comparator == null && i.equals(item)){
                return idx;
            }

        }
        return -1;
    }

    public void replaceItem(T item, Comparator<T> comparator) {
        int index = findItemIdx(item, comparator);

        if(index >= 0 && index < items.size()){
            items.set(index, item);

            adapter.notifyItemChanged(index);
        }
    }
}
