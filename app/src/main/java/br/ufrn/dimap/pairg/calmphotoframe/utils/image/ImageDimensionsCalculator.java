package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

public interface ImageDimensionsCalculator{
	public int[] calculateSize(int[] imageDimensions, int[] expectedDimensions);
}