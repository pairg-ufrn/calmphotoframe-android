package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class ListPhotosFromCollectionRequest extends PhotoListRequest{
	private Long collectionId;
	private boolean recursive;
	private String query;
	
	public ListPhotosFromCollectionRequest(ApiConnector apiConnector, Long collectionId, boolean recursive
			, String queryFilter, ResultListener<? super List<Photo>> listener) 
	{
		super(apiConnector, listener);
		this.collectionId = collectionId;
		this.recursive = recursive;
		this.query = queryFilter;
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().collectionPhotosUrl(collectionId, recursive, query);
	}
}
