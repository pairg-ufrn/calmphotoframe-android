package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;

public class OnResultUpdateViewAndState extends BasicResultListener<CollectionAndContextContainer> {
	private final PhotoCollectionView collectionView;
	private final CollectionAndContextContainer state;
	
	public OnResultUpdateViewAndState(PhotoCollectionView collectionView, CollectionAndContextContainer state) {
		this.collectionView = collectionView;
		this.state = state;
	}
	
	@Override
	public void onResult(CollectionAndContextContainer result) {
		state.set(result);
		
		collectionView.updateView(state);
	}
}