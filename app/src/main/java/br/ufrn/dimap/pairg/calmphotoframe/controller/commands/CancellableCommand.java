package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public interface CancellableCommand<Arg, Result> {
    RequestHandler execute(Arg arg, ResultListener<? super Result> listener);
}
