package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.util.Log;

import java.io.File;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ChooseFileCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.SavePhotoCommand.SavePhotoResult;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.SaveImageRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.NotifierProgressListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog.SaveDialogResponseListener;

public class SavePhotoCommand
		extends ActivityStatelessCommand<SavePhotoResult>
{
	public static class SavePhotoResult {
		private Photo photoToSave;
		private File imageFile;
		
		public SavePhotoResult(Photo photoToSave, File imageFile) {
			this.photoToSave = photoToSave;
			this.imageFile = imageFile;
		}
		public Photo getPhotoToSave() {
			return photoToSave;
		}
		public File getImageFile() {
			return imageFile;
		}
	}

	private static final String SAVEPHOTO_DIALOG_TAG = "SAVEPHOTO_DIALOG_TAG";
	
	private PhotoHolder photoHolder;
	
	public SavePhotoCommand(BaseActivity activity, PhotoHolder photoHolder) {
		super(activity);
		this.photoHolder = photoHolder;
	}

	@Override
	public void execute(ResultListener<? super SavePhotoResult> listener) {
		saveCurrentPhoto(listener);
	}

	private void saveCurrentPhoto(ResultListener<? super SavePhotoResult> listener) {
		final Photo photoToSave = photoHolder.getCurrentPhoto();
		if(photoToSave == null){
			return;
		}
		
    	SaveDialog dialogFragment = new SaveDialog();
    	
    	String dialogMessage = getContext().getResources().getString(R.string.saveImage_title);
    	dialogFragment.setDialogMessage(dialogMessage);
    	
    	if(photoToSave.getMetadata() != null && photoToSave.getMetadata().getOriginalFilename() != null){
        	dialogFragment.setFilename(photoToSave.getMetadata().getOriginalFilename());
    	}
    	
    	dialogFragment.setDialogResponseListener(new SavePhotoDialogResponseListener(listener, photoToSave));
    	dialogFragment.show(getActivity().getSupportFragmentManager(), SAVEPHOTO_DIALOG_TAG);
	}

	/* ***************************************************************************************/
	private final class SavePhotoDialogResponseListener implements SaveDialogResponseListener {
		private final Photo photoToSave;
        private ResultListener<? super SavePhotoResult> listener;

        private ChooseFileCommand chooseFileCommand;
        private ResultListener<? super String[]> onChooseDir;

		private SavePhotoDialogResponseListener(ResultListener<? super SavePhotoResult> listener, Photo photoToSave) {
            this.listener = listener;
			this.photoToSave = photoToSave;

            chooseFileCommand = new ChooseFileCommand(getActivity());
            this.onChooseDir = Listeners.join(
                    new OnResultSetSaveDialogPath(getActivity(), SAVEPHOTO_DIALOG_TAG),
                    new BasicResultListener<Object>(){
                        @Override
                        protected void handleError(Throwable error) {
                            Listeners.onFailure(SavePhotoDialogResponseListener.this.listener, error);
                        }
                    }
            );
		}

		/** User canceled saving dialog.*/
		@Override
		public void onCancel() {
			//Do nothing.
		}

		@Override
		public void onChoosePath(String currentPath) {
			Log.d(getClass().getName(), "select path from: " + currentPath);
			
			showFileChooser(currentPath, true);
		}

		@Override
		public void onSelectFile(File savePath) {
			NotifierProgressListener progressListener 
					= new NotifierProgressListener(savePath.hashCode(), getContext());
			progressListener.setContentText(savePath.getName());
			progressListener.setOnProgressText(getContext().getText(R.string.saveImage_onProgress));
			progressListener.setOnFailureText(getContext().getText(R.string.saveImage_onError));
			progressListener.setOnSuccessText(getContext().getText(R.string.saveImage_onSuccess));
			
			ApiClient.instance().photos()
					.saveImage(photoToSave,savePath, new ResultListenerConverter(listener),progressListener);
		}

		private void showFileChooser(String startPath, boolean selectDir) {
			chooseFileCommand.execute(
					new ChooseFileCommand.FileChooserArguments(startPath, selectDir),
					onChooseDir);
		}
    }

    private class ResultListenerConverter extends ResultListenerDelegator<SaveImageRequest.SavedImageResponse, SavePhotoResult> {
        public ResultListenerConverter(ResultListener<? super SavePhotoResult> listener) {
        	super(listener);
		}

        @Override
        protected SavePhotoResult convertResult(SaveImageRequest.SavedImageResponse result) {
            return new SavePhotoResult(result.getPhoto(), result.getSavedFile());
        }
    }
}
