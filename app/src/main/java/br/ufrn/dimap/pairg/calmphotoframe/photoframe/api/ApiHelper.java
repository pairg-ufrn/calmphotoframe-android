package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import cz.msebera.android.httpclient.message.BasicHeader;

public class ApiHelper {
    public static final String TOKEN_COOKIE_NAME = "token";

    public static Cookie buildTokenCookie(String token, String domain){
        BasicClientCookie newCookie = new BasicClientCookie(TOKEN_COOKIE_NAME, token);
        newCookie.setVersion(0);
        newCookie.setDomain(domain);
        newCookie.setPath("/");
        newCookie.setSecure(false);

        return newCookie;
    }

    public static Header buildTokenCookieHeader(String token){
        return cookieToHeader(buildTokenCookie(token, ""));
    }

    public static Header cookieToHeader(Cookie cookie){
        return new BasicHeader("Cookie", cookie.getName() + "=" + cookie.getValue());
    }
}
