package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class PhotoMarker implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6152861061364021665L;
	private String identification;
	private float relativeX, relativeY;
	private int index;
	
	public PhotoMarker(){
		this(0, "",0f,0f);
	}
	public PhotoMarker(int numberInPhoto) {
		this();
		this.index = numberInPhoto;
	}
	
	public PhotoMarker(int numberInPhoto, String identification, float relativeX, float relativeY) {
		super();
		this.index = numberInPhoto;
		this.identification = identification;
		this.relativeX = relativeX;
		this.relativeY = relativeY;
	}

	public PhotoMarker(PhotoMarker photoMarker) {
		this(photoMarker.getIndex(), photoMarker.getIdentification()
				, photoMarker.getX(), photoMarker.getY());
	}
	public String getIdentification() { return identification;}
	public float getX() { return relativeX;}
	public float getY() {  return relativeY; }
	public int getIndex	 () { return index;}
	
	public void setIdentification(String identification) { this.identification = identification; }
	public void setX(float relativeX) { this.relativeX = relativeX;}
	public void setY(float relativeY) { this.relativeY = relativeY;}
	public void setIndex	(int number) 	  { this.index = number;}

	public void setRelativePosition(float relX, float relY) {
		setX(relX);
		setY(relY);
	}

	@Override
	public boolean equals(Object other) {
		if(other == this){
			return true;
		}

		if(!(other instanceof PhotoMarker)){
			return false;
		}
		PhotoMarker otherMarker = (PhotoMarker)other;
		return (compare(this.getIdentification(), otherMarker.getIdentification()) && 
				compare(this.getX()	 	, otherMarker.getX()) 	   &&
				compare(this.getY()	 	, otherMarker.getY()) 	   &&
				compare(this.getIndex()	 	, otherMarker.getIndex()));
	}
	private boolean compare(Object obj1, Object obj2) {
		if(obj1 == null || obj2 == null){
			return (obj1 == null && obj2 == null);
		}
		return obj1.equals(obj2);
	}
	
	@SuppressWarnings("StringBufferReplaceableByString")
    @Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(": ");
		builder.append(this.getIndex()).append("º; ");
		builder.append(this.getIdentification()).append("; ");
		builder.append(this.getX()).append("%, ")
			   .append(this.getY()).append("%, ");
		return builder.toString();
	}
	
}
