package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection;

import android.content.Context;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.localserver.StartLocalServerCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;

public abstract class ConnectionCommand<T>
    extends BaseStatelessCommand<T>
{

    private  ConnectionInfo connectionInfo;

    private Context context;

    public Context getContext() {
        return context;
    }

    public ConnectionCommand<T> setContext(Context context) {
        this.context = context;
        return  this;
    }

    public ConnectionInfo connectionInfo(){
        return this.connectionInfo;
    }

    public ConnectionCommand<T> connectionInfo(ConnectionInfo connectionInfo){
        this.connectionInfo = connectionInfo;
        return this;
    }

    @Override
    public void execute(ResultListener<? super T> listener) {
        assertHasConnectionInfo();
        execute(connectionInfo(), listener);
    }

    public void execute(final ConnectionInfo connInfo, final ResultListener<? super T> listener){
        Listeners.onStart(listener);
        
        if(!connInfo.isLocal()) {
            doConnection(connInfo, listener);
        }
        else{
            startLocalServer(new BasicResultListener<Endpoint>(){
                @Override
                public void onResult(Endpoint result) {
                    doConnection(new ConnectionInfo(result, connInfo.getLoginInfo()), listener);
                }
            });
        }
    }

    public abstract void doConnection(ConnectionInfo connInfo, ResultListener<? super T> listener);

    public void startLocalServer(ResultListener<? super Endpoint> listener){
        new StartLocalServerCommand(getContext()).execute(listener);
    }

    private void assertHasConnectionInfo() {
        if(connectionInfo() == null){
            throw new NullPointerException("ConnectionInfo not set (is null)");
        }
    }
}
