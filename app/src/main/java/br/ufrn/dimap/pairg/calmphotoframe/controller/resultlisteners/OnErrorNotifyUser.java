package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.activities.NotificationBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;

public class OnErrorNotifyUser extends BasicResultListener<Object> {
	protected UserNotifier userNotifier;

    private CharSequence defaultMessage;

	public OnErrorNotifyUser(UserNotifier userNotifier) {
		this.userNotifier = userNotifier;
	}

    public CharSequence getDefaultMessage() {
        return defaultMessage;
    }

    public OnErrorNotifyUser setDefaultMessage(CharSequence defaultMessage) {
        this.defaultMessage = defaultMessage;
        return this;
    }

    @Override
	public void onFailure(Throwable error) {
		notifyErrorToUser(error);
	}

	protected void notifyErrorToUser(Throwable error) {
        NotificationBuilder notificationBuilder = notifyUser();

        putErrorOnNotifier(notificationBuilder, error);
		notificationBuilder.show();
	}

    protected void putErrorOnNotifier(NotificationBuilder notificationBuilder, Throwable error){
        notificationBuilder.text(getErrorText(error));
    }
	protected CharSequence getErrorText(Throwable error){
        if(getDefaultMessage() != null){
            return getDefaultMessage();
        }
		return error.getLocalizedMessage();
	}

	protected UserNotifier notifier(){
        return userNotifier;
    }

	protected NotificationBuilder notifyUser() {
        return userNotifier.notifyUser();
    }

	@Override
    public boolean isListening() {
        return true;
    }
}
