package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.contexts.PhotoContextDetailsController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;

public class EditContextCommand extends ActivityStatelessCommand<PhotoContext> {
	private ItemSelector<PhotoContext> contextSelector;
	
	private PhotoContextDetailsController controller;
	
	public EditContextCommand(BaseActivity activity
			, ItemSelector<PhotoContext> selector, int fragmentContainerId) {
		super(activity);
		this.contextSelector = selector;
		//TODO: maybe require an explicit container id
		controller = new PhotoContextDetailsController(activity, fragmentContainerId);
		controller.initialize();
	}
	
	public int getFragmentContainerId() {
		return controller.getFragmentContainerId();
	}
	public void setFragmentContainerId(int fragmentContainerId) {
		controller.setFragmentContainerId(fragmentContainerId);
	}

	@Override
	public void execute(final ResultListener<? super PhotoContext> cmdResultListener) {
		controller.show(contextSelector.getSelectedItem(), cmdResultListener);
	}
}
