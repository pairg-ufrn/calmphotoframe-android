package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import android.graphics.Bitmap;

import java.io.File;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.PhotoReceiver;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.ProgressListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ImageReceiverAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils;

public class PhotosApi {
    private ApiConnector apiConnector;

    public PhotosApi(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }

    protected ApiConnector connector(){
        return apiConnector;
    }

    public RequestHandler listFromCollectionTree(Long photoCollectionId
            , ResultListener<? super List<Photo>> listener)
    {
        return listFromCollection(photoCollectionId, true, null, listener);
    }
    public RequestHandler listFromCollection(Long photoCollectionId
            , ResultListener<? super List<Photo>> listener)
    {
        return listFromCollection(photoCollectionId, false, null, listener);
    }
    public RequestHandler listFromCollection(Long photoCollectionId
            , boolean recursive
            , String queryFilter
            , ResultListener<? super List<Photo>> listener)
    {
        return new ListPhotosFromCollectionRequest(apiConnector, photoCollectionId, recursive, queryFilter, listener).execute();
    }

    public RequestHandler get(int photoId, ResultListener<? super Photo> photoListener){
        return get(photoId, false, photoListener);
    }
    public RequestHandler get(int photoId, boolean include_permission, ResultListener<? super Photo> photoListener){
        return new GetPhotoRequest(apiConnector, photoId, include_permission, photoListener).execute();
    }

    public RequestHandler getThumbnail(final long photoId, ResultListener<? super PhotoImageWrapper> listener) {
        return getThumbnail(new Photo(photoId), listener);
    }
    public RequestHandler getThumbnail(final Photo photo, ResultListener<? super PhotoImageWrapper> listener) {
        return new ThumbnailRequest(this.apiConnector, photo, listener).execute();
    }

    public RequestHandler getImageBitmap(final Photo photo, final ResultListener<? super Bitmap> photoListener)
    {
        return getImage(photo, new PhotoListenerBitmapAdapter(photoListener));
    }

    public RequestHandler getImage(final Photo photoDescription,
                                   final ResultListener<? super PhotoImageWrapper> photoListener)
    {
        int[] maxPhotoSize = ImageUtils.instance().getDefaultMaxImageSize();

        return this.getImage(photoDescription, maxPhotoSize[0], maxPhotoSize[1], photoListener);
    }

    public RequestHandler getImage(final Photo photoDescription,
                                   int maxWidth,
                                   int maxHeight,
                                   ResultListener<? super PhotoImageWrapper> photoListener)
    {
        int photoId = photoDescription.getId();
        PhotoReceiver photoReceiver =
                new PhotoReceiver(photoId, connector().getPhotoCache());

        PhotoContainer container = photoReceiver.getPhotoContainer();
        container.setPhotoDescriptor(photoDescription);
        return getImageBitmap(photoReceiver, new int[]{maxWidth, maxHeight}, photoListener);
    }

    private RequestHandler getImageBitmap(final PhotoReceiver photoReceiver,
                                          int[] maxDimensions,
                                          ResultListener<? super PhotoImageWrapper> photoListener)
    {
        Photo photo = new Photo(photoReceiver.getPhotoId());
        if(photoReceiver.getPhotoContainer().containsDescription()){
            photo = photoReceiver.getPhotoContainer().getPhotoDescriptor();
        }

        ImageReceiverAdapter imageReceiver = new ImageReceiverAdapter(photoReceiver, photoListener);

        return new GetImageRequest(apiConnector, photo, maxDimensions, imageReceiver).execute();
    }

    public RequestHandler update(Photo photoDescriptor
            , ResultListener<? super Photo> updateListener)
    {
        assertValidPhotoDescriptorArgument(photoDescriptor);
        return new UpdatePhotoRequest(apiConnector, photoDescriptor, updateListener).execute();
    }

    /** Create photo with the given image content at some photocollection
     * @param imageContent - container to the image that will be inserted
     * @param collectionId - if not null, the created photo will be added to the PhotoCollection with this id.
     * 							  Otherwise, the photo will be added to the root collection of the current context.
     * @param listener - if not null, callback this listener with the response or failure
     * */
    public RequestHandler insert(ImageContent imageContent, Long collectionId, ResultListener<? super Photo> listener) {
        return new InsertPhotoRequest(apiConnector, imageContent, collectionId, listener).execute();
    }

    public RequestHandler remove(Photo photoToRemove, ResultListener<? super Photo> removeListener) {
        return new RemovePhotoRequest(apiConnector, photoToRemove, removeListener).execute();
    }

    public RequestHandler saveImage(final Photo photoToSave, File savePath
            , final ResultListener<? super SaveImageRequest.SavedImageResponse> saveImageListener
            , final ProgressListener progressListener)
    {
        return new SaveImageRequest(apiConnector, photoToSave, savePath)
                .listener(saveImageListener)
                .progressListener(progressListener)
                .execute();
    }

    private void assertValidPhotoDescriptorArgument(
            Photo photoDescriptor) {
        if(photoDescriptor == null){
            String errMessage = "Invalid argument! PhotoDescriptor should not be null!";
            throw new IllegalArgumentException(errMessage);
        }
        else if(photoDescriptor.getId() == null || photoDescriptor.getId() <= 0){
            throw new IllegalArgumentException("Invalid argument! photoDescriptor should have a valid id (not null and greater than 0)");
        }
    }
}
