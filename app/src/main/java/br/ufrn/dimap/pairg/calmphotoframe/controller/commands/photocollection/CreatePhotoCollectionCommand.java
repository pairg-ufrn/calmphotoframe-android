package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.InputDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.InputDialogFragment.InputResponseListener;

public class CreatePhotoCollectionCommand extends ActivityStatelessCommand<PhotoCollection>{
	private static final String ALBUMCREATION_DIALOG_TAG = CreatePhotoCollectionCommand.class + ".AlbumCreationDialog";
	
	public interface PhotoCollectionGetter{
		PhotoCollection getPhotoCollection();
	}
	
	private PhotoCollectionGetter parentCollectionGetter;
	public CreatePhotoCollectionCommand(BaseActivity activity)
	{
		this(activity, null);
	}

	public CreatePhotoCollectionCommand(BaseActivity activity, PhotoCollectionGetter parentCollectionGetter)
	{
		super(activity);
		this.parentCollectionGetter = parentCollectionGetter;
	}


	@Override
	public void execute(final ResultListener<? super PhotoCollection> listener) {
		InputDialogFragment dialogFragment = new InputDialogFragment();
    	dialogFragment.setDialogTitle(getString(R.string.dialog_createAlbum_title));
    	dialogFragment.setDialogMessage(getString(R.string.dialog_createAlbum_message));
    	dialogFragment.setOkMessage(getString(R.string.dialog_createAlbum_okMessage));
    	dialogFragment.setCancelMessage(getString(R.string.dialog_defaultCancel));
    	dialogFragment.show(getActivity().getSupportFragmentManager(), ALBUMCREATION_DIALOG_TAG);
    	dialogFragment.setInputResponseListener(new InputResponseListener() {
			@Override
			public boolean onResponse(InputDialogFragment inputDialog, boolean responseIsPositive, String albumName) {
				if(responseIsPositive){
					PhotoCollection parentCollection = (parentCollectionGetter == null 
																	? null 
																	: parentCollectionGetter.getPhotoCollection());
					
					ApiClient.instance().collections().create(albumName, parentCollection
							, listener);
				}
				return true;
			}
		});
	}

	public String getString(int stringId) {
		return getActivity().getResources().getString(stringId);
	}
	
}