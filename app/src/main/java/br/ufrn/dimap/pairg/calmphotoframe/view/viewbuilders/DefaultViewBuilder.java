package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.widget.TextView;

public class DefaultViewBuilder<T> extends InflaterViewBuilder {

    private int viewId;
    public DefaultViewBuilder(int layoutId) {
        this(layoutId, 0);
    }


    public DefaultViewBuilder(int layoutId, int viewId){
        super(layoutId);
        this.viewId = viewId;
    }

    @Override
    protected void setupView(View view, Object data) {
        TextView txtView = (TextView) (viewId == 0 ? view : view.findViewById(viewId));

        if(txtView != null){
            if(data == null){
                txtView.setText("");
            }
            else{
                txtView.setText(data.toString());
            }
        }
    }
}
