package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import java.util.Arrays;
import java.util.Collection;

public class CompositeRequestHandler implements RequestHandler{
	private Collection<RequestHandler> requestHandlerComponents;
	
	public CompositeRequestHandler(RequestHandler ... handlers) {
		requestHandlerComponents = Arrays.asList(handlers);
	}
	
	@Override
	public boolean isFinished() {
		for(RequestHandler handler : requestHandlerComponents){
			if(!handler.isFinished()){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isCancelled() {
		for(RequestHandler handler : requestHandlerComponents){
			if(!handler.isCancelled()){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean cancel() {
		boolean canceledAll = true;

		for(RequestHandler handler : requestHandlerComponents){
			canceledAll = canceledAll && handler.cancel();
		}
		return canceledAll;
	}

}
