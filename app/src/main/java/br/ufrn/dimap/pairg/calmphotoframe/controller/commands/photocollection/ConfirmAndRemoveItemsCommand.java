package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import android.content.Context;
import android.content.res.Resources;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.Builder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.ProgressNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.UpdateNotifierListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleMaterialDialogFragment;

import static br.ufrn.dimap.pairg.calmphotoframe.R.plurals.photo;
import static br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleMaterialDialogFragment.DialogResponseListener;

public class ConfirmAndRemoveItemsCommand extends ActivityStatelessCommand<Collection<PhotoComponent>> {
	private static final String DIALOG_TAG = ConfirmAndRemoveItemsCommand.class + ".DIALOG_TAG";

	private PhotoComponentSelector selector;

    private ResultListener<PhotoComponent> progressListener;
    private Builder<ProgressNotifier> notifierBuilder;
    private int concurrencyLevel;

    public ConfirmAndRemoveItemsCommand(BaseActivity baseActivity, PhotoComponentSelector selector) {
		super(baseActivity);
		this.selector = selector;
        this.notifierBuilder = new RemoveItemsNotifierBuilder(baseActivity);
	}

	public PhotoComponentSelector getSelector() {
		return this.selector;
	}

    public ConfirmAndRemoveItemsCommand onProgress(ResultListener<PhotoComponent> progressListener) {
        this.progressListener = progressListener;
        return this;
    }

    @SuppressWarnings("unused")
    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public void setConcurrencyLevel(int concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
    }

    public Builder<ProgressNotifier> getNotifierBuilder() {
        return notifierBuilder;
    }

    public void setNotifierBuilder(Builder<ProgressNotifier> notifierBuilder) {
        this.notifierBuilder = notifierBuilder;
    }

    private String getString(int stringId){
        return getContext().getResources().getString(stringId);
    }

	@Override
	public void execute(ResultListener<? super Collection<PhotoComponent>> listener) {
		Collection<PhotoComponent> selectedItems = selector.getSelectedItems();
		if(!selectedItems.isEmpty()){
			
	    	SimpleMaterialDialogFragment dialogFragment = buildDialog(selectedItems, listener);
	    	dialogFragment.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
		}
	}

	protected SimpleMaterialDialogFragment buildDialog(
            final Collection<PhotoComponent> selectedItems,
            final ResultListener<? super Collection<PhotoComponent>> listener)
    {
		SimpleMaterialDialogFragment dialogFragment = new SimpleMaterialDialogFragment();

        dialogFragment.content()
                .setOkMessage    (getString(R.string.remove_photo_dialog_ok))
				.setCancelMessage(getString(R.string.remove_photo_dialog_cancel))
				.setDialogTitle(getDialogTitle(selectedItems))
				.setDialogMessage(getString(R.string.dialog_remove_warning));

        dialogFragment.setDialogResponseListener(new DialogResponseListener() {
			@Override
			public boolean onResponse(SimpleMaterialDialogFragment dialog, boolean positive) {
				if(positive){
                    executeRemove(selectedItems, listener);
                }
				return true;
			}
		});

		return dialogFragment;
	}

    protected String getDialogTitle(Collection<PhotoComponent> selectedItems) {
        Collection<Photo> photosToRemove
                = PhotoComponent.filterPhotos(selectedItems);
        Collection<PhotoCollection> collectionsToRemove
                = PhotoComponent.filterPhotoCollections(selectedItems);

		int photoCount = photosToRemove.size();
		int albumsCount = collectionsToRemove.size();
		
		Resources res = getContext().getResources();
		String photosStr = res.getQuantityString(photo, photoCount);
		String albumsStr = res.getQuantityString(R.plurals.album, albumsCount);
		
		if(!photosToRemove.isEmpty() && !collectionsToRemove.isEmpty()){
			String formatString = getString(R.string.dialog_remove_photos_and_collections_message);
			return String.format(formatString, photoCount, photosStr, albumsCount, albumsStr);
		}
		else {
			int count = photosToRemove.isEmpty() ? albumsCount : photoCount;
			String itemStr = photosToRemove.isEmpty() ? albumsStr : photosStr;
			String formatString = getString(R.string.dialog_remove_itens_message);
			return String.format(formatString, count, itemStr);
		}
	}


    protected void executeRemove(Collection<PhotoComponent> selectedItems, ResultListener<? super Collection<PhotoComponent>> listener) {
        RemoveItemsCommand removeItemsCommand = new RemoveItemsCommand();
        removeItemsCommand.setConcurrencyLevel(getConcurrencyLevel());
        removeItemsCommand.setProgressListener(buildProgressListener(this.progressListener, selectedItems));
        removeItemsCommand.execute(selectedItems, listener);
    }

    protected ResultListener<? super PhotoComponent> buildProgressListener(
            ResultListener<PhotoComponent> progressListener, Collection<PhotoComponent> selectedItems)
    {
        ResultListener<PhotoComponent> notifierListener = null;

        ProgressNotifier notifier = (getNotifierBuilder() != null) ? getNotifierBuilder().build() : null;
        if(notifier != null){
            notifierListener = new UpdateNotifierListener<>(notifier, selectedItems.size());
        }
        return Listeners.join(notifierListener, progressListener);
    }

    public static class RemoveItemsNotifierBuilder implements Builder<ProgressNotifier> {
        private final Context context;

        public RemoveItemsNotifierBuilder(Context context) {
            this.context = context;
        }

        @Override
        public ProgressNotifier build() {
            ProgressNotifier progressNotifier = new ProgressNotifier(context, R.string.removeItems_progress);
            progressNotifier.setSuccessPlurals(R.plurals.removeItems_success);
            progressNotifier.setFailurePlurals(R.plurals.removeItems_failed);
            progressNotifier.setContentIntent(
                    IntentUtils.instance().buildPendingIntent(context, PhotoCollectionActivity.class, 0, 0));

            return progressNotifier;
        }
    }
}
