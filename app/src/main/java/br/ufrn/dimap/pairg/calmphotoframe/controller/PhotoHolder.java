package br.ufrn.dimap.pairg.calmphotoframe.controller;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public interface PhotoHolder extends EntityGetter<Photo>{

	Photo getCurrentPhoto();

	void setCurrentPhoto(Photo photo);

	void removeCurrentPhoto();

	/** Updates the the given photo.
	 * If the photo is not being hold, the implementation can ignore it.*/
	void updatePhoto(Photo photo);
}
