package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.view.View;

import java.util.HashMap;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnItemClickDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;

/**
 */
public class ItemSelectionOnClickAdapter<T> {
    Map<OnSelectItemListener<T>, OnItemClickListener> mapToRealListener = new HashMap<>();
    ItemsManager<T> itemsManager;
    OnItemClickDelegator delegator;

    public ItemSelectionOnClickAdapter(ItemsManager<T> itemsManager, OnItemClickDelegator delegator) {
        this.itemsManager = itemsManager;
        this.delegator = delegator;
    }

    public void addSelectItemListener(OnSelectItemListener<T> listener) {
        OnClickSelectItemAdapter<T> adapter = new OnClickSelectItemAdapter<>(itemsManager, listener);
        mapToRealListener.put(listener, adapter);
        delegator.addListener(adapter);
    }

    public void removeSelectItemListener(OnSelectItemListener<T> listener) {
        delegator.removeListener(mapToRealListener.get(listener));
        mapToRealListener.remove(listener);
    }


    public static class OnClickSelectItemAdapter<T> implements OnItemClickListener {
        private final OnSelectItemListener<T> listener;
        private ItemsManager<T> itemsManager;

        public OnClickSelectItemAdapter(ItemsManager<T> itemsManager, OnSelectItemListener<T> listener) {
            this.itemsManager = itemsManager;
            this.listener = listener;
        }

        @Override
        public void onItemClick(View view, int position, long itemId) {
            T item = itemsManager.get(position);
            listener.onItemSelection(item);
        }
    }
}
