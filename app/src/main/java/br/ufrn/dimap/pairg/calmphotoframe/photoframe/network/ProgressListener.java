package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

public interface ProgressListener {
	void onStart();
	void onProgress(long bytesWritten, long totalSize);
	void onFinish(boolean successful);
	boolean isListening();
}