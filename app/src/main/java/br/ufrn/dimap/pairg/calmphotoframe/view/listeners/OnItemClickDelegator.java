package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemClickListener;

/**
 */
public class OnItemClickDelegator extends ListenerDelegator<OnItemClickListener>
        implements OnItemClickListener {
    @Override
    public void onItemClick(View view, int position, long itemId) {
        for (OnItemClickListener listener : delegates) {
            listener.onItemClick(view, position, itemId);
        }
    }
}
