package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;

public class ActivityResultListener<T> extends BasicResultListener<T>{
	private BaseActivity activity;

	public ActivityResultListener(BaseActivity activity) {
		this.activity = activity;
	}
	
	public BaseActivity getActivity() {
		return activity;
	}
	public void setActivity(BaseActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onFailure(Throwable error) {
		activity.notifyError(error);
	}
	@Override
	public boolean isListening() {
		return activity.isActive();
	}
}
