package br.ufrn.dimap.pairg.calmphotoframe.photoframe;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

import br.ufrn.dimap.pairg.calmphotoframe.PhotoFrameApplication;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class LocalPersistence {

    private static AtomicReference<LocalPersistence> singleton = new AtomicReference<>(null);
    public static LocalPersistence instance() {
        LocalPersistence persistence = singleton.get();
        if(persistence == null){
            singleton.compareAndSet(null, new LocalPersistence(PhotoFrameApplication.getContext()));
            persistence = singleton.get();
        }
        return persistence;
    }
    public static LocalPersistence instance(LocalPersistence persistence){
        singleton.set(persistence);
        return singleton.get();
    }

    public static class PreferenceKeys {
        public static final String ConnectedServers = "preferences_connectedservers";
    }

    public static class StorageException extends RuntimeException{
        /** {@inheritDoc} */
        public StorageException() {
            super();
        }

        /** {@inheritDoc} */
        public StorageException(String message) {
            super(message);
        }

        /** {@inheritDoc} */
        public StorageException(String message, Throwable cause) {
            super(message, cause);
        }

        /** {@inheritDoc} */
        public StorageException(Throwable cause) {
            super(cause);
        }
    }

	private static final int INVALID_Integer = Integer.MIN_VALUE;
	private static final long INVALID_Long = Long.MIN_VALUE;
	
	private Context context;
    private ObjectMapper objectMapper;

	public LocalPersistence(Context context) {
		this.context = context;
        this.objectMapper = ObjectMapperConfiguration.build();
	}

	public Integer getCurrentPhoto(){
		return getCurrentPhoto(defaultPhotoFrame());
	}
	public Integer getCurrentPhoto(ApiClient photoFrame){
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastPhoto);
		return getIntPreference(getPhotoFramePreferences(), key);
	}
	
	public Long getCurrentPhotoContext(){
		return getCurrentPhotoContext(defaultPhotoFrame());
	}
	public Long getCurrentPhotoContext(ApiClient photoFrame){
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastContext);
		return getLongPreference(getPhotoFramePreferences(), key);
	}

	public Long getCurrentPhotoCollection(){
		return getCurrentPhotoCollection(defaultPhotoFrame());
	}
	public Long getCurrentPhotoCollection(ApiClient photoFrame){
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastPhotoCollection);
		return getLongPreference(getPhotoFramePreferences(), key);
	}

	public boolean storeCurrentPhoto(Integer photoId){
		return storeCurrentPhoto(defaultPhotoFrame(), photoId);
	}
	public boolean storeCurrentPhoto(ApiClient photoFrame, Integer photoId){
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastPhoto);
		return saveNumberProperty(key, photoId);
	}

	public boolean storeCurrentPhotoContext(Long contextId){
		return storeCurrentPhotoContext(defaultPhotoFrame(), contextId);
	}
	public boolean storeCurrentPhotoContext(ApiClient photoFrame, Long contextId){
		invalidateCurrentContext();
		
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastContext);
		return saveNumberProperty(key, contextId);
	}

	protected void invalidateCurrentContext() {
		//TODO: é possível otimizar operação (abrir e salvar SharedPreferences apenas uma vez)
		removeCurrentPhoto();
		removeCurrentPhotoCollection();
	}
	
	public boolean storeCurrentPhotoCollection(Long collectionId){
		return storeCurrentPhotoCollection(defaultPhotoFrame(), collectionId);
	}
	public boolean storeCurrentPhotoCollection(ApiClient photoFrame, Long collectionId){
		String key = getKey(photoFrame, R.string.preferences_photoframe_lastPhotoCollection);
		return saveNumberProperty(key, collectionId);
	}

	/** Remove the photo with id {@code photoId} if it is stored on the local persistence*/
	public void removePhoto(Integer photoId) {
		Integer currentPhoto = getCurrentPhoto();
		if(currentPhoto != null && currentPhoto.equals(photoId)){
			removeCurrentPhoto();
		}
	}
	public void removeCurrentPhoto() {
		storeCurrentPhoto(null);
	}
	public void removeCurrentPhotoCollection() {
		storeCurrentPhotoCollection(null);
	}
	public void removeCurrentPhotoContext(){
		storeCurrentPhotoContext(null);
	}

    public void setCurrentPhotoAndCollection(Photo photo, PhotoCollection photoCollection){
        Long photoCollId = photoCollection != null ? photoCollection.getId() : null;
        storeCurrentPhotoCollection(photoCollId);

        if(photo != null){
            LocalPersistence.instance().storeCurrentPhoto(photo.getId());
        }
    }


    public void addConnectedServer(@NonNull Endpoint info) {
        String address = info.getHostname();
        if(info.getPort() != Endpoint.DEFAULT_PORT){
            address += ":" + String.valueOf(info.getPort());
        }

        addConnectedServer(new ServerInfo("", address));
    }
    public void addConnectedServer(ServerInfo serverInfo){
        List<ServerInfo> connectedServers = getConnectedServers();
        connectedServers.remove(serverInfo);
        connectedServers.add(0, serverInfo);

        saveList(getPhotoFramePreferences(), PreferenceKeys.ConnectedServers, connectedServers);
    }

    public void removeConnectedServer(ServerInfo serverInfo){
        List<ServerInfo> connectedServers = getConnectedServers();
        connectedServers.remove(serverInfo);

        saveList(getPhotoFramePreferences(), PreferenceKeys.ConnectedServers, connectedServers);
    }

    public List<ServerInfo> getConnectedServers(){
        List<ServerInfo> list =
                this.getJson(getPhotoFramePreferences(),
                        PreferenceKeys.ConnectedServers,
                        new TypeReference<ArrayList<ServerInfo>>(){});

        return list == null ? new ArrayList<ServerInfo>() : list;
    }
	/* ************************************* Helpers ****************************************************/
	
	protected Context getContext(){
		return this.context;
	}

	protected ApiClient defaultPhotoFrame() {
		return ApiClient.instance();
	}
	
	protected String getKey(ApiClient photoFrame, int stringId) {
		if(photoFrame == null || !photoFrame.hasEndpoint() || photoFrame.getCurrentUser() == null){
			return null;
		}
		User user = photoFrame.getCurrentUser();
		String host = String.format(Locale.US, "%s.%d@%s:%d",
                user.getLogin(), user.getId()
                , photoFrame.endpoint().getHostname(), photoFrame.endpoint().getPort());

		return String.format(getText(stringId), host);
	}

	private String getTextPreference(SharedPreferences preferences, String prefName) {
		if(prefName == null){
			return null;
		}
		return preferences.getString(prefName, null);
	}
	private Integer getIntPreference(SharedPreferences preferences, String prefName) {
		if(prefName == null){
			return null;
		}
		int id = preferences.getInt(prefName, INVALID_Integer);
		return id != INVALID_Integer ? id : null;
	}
	private Long getLongPreference(SharedPreferences preferences, String prefName) {
		if(prefName == null){
			return null;
		}
		
		long id = preferences.getLong(prefName, INVALID_Long);
		return id != INVALID_Long ? id : null;
	}

	protected  boolean saveNumberProperty(String prefProperty, Number number) {
		Editor editor = getPhotoFramePrefEditor();
		if(number == null){
			editor.remove(prefProperty);
		}
		else{
			if(number instanceof Integer){
				editor.putInt(prefProperty, number.intValue());
			}
			else if(number instanceof Double || number instanceof Float){
				editor.putFloat(prefProperty, number.floatValue());
			}
			else{
				editor.putLong(prefProperty, number.longValue());
			}
		}

		return editor.commit();
	}


    protected <T> List<T> getList(SharedPreferences preferences, String key) {
        return getJson(preferences, key, List.class);
    }


    private <T> void saveList(SharedPreferences preferences, String key, List<T> items) {
        if(! (items instanceof Serializable)){
            items = new ArrayList<>(items);
        }

        saveObject(preferences, key, (Serializable)items);
    }
    private <T> void saveObject(SharedPreferences preferences, String key, Serializable items) {
        Editor editor = preferences.edit();

        if(items == null){
            editor.remove(key);
        }
        else{
            try {
                String jsonValue = objectMapper.writeValueAsString(items);
                editor.putString(key, jsonValue);
            } catch (JsonProcessingException e) {
                throw new StorageException("Failed to serialize item.", e);
            }

            editor.apply();
        }
    }

    protected <T> T getJson(SharedPreferences preferences, String key, Class<T> clazz) {
        return getJson(preferences, key, clazz, null);
    }
    protected <T> T getJson(SharedPreferences preferences, String key, TypeReference<T> typeReference) {
        return getJson(preferences, key, null, typeReference);
    }
    protected <T> T getJson(SharedPreferences preferences, String key, Class<T> clazz, TypeReference<T> typeReference) {
        String txt = this.getTextPreference(preferences, key);

        if(txt != null){
            try {
                if(clazz != null) {
                    return objectMapper.readValue(txt, clazz);
                }
                else{
                    return objectMapper.readValue(txt, typeReference);
                }
            } catch (IOException e) { //TODO: should throw exception
                e.printStackTrace();
            }
        }

        return null;
    }

	private SharedPreferences getPhotoFramePreferences() {
		String prefName = getText(R.string.preferences_photoframe).toString();
		return getContext().getSharedPreferences(prefName, Context.MODE_PRIVATE);
	}
	
	protected Editor getPhotoFramePrefEditor() {
		SharedPreferences photoFramePreferences = getPhotoFramePreferences();
		return photoFramePreferences.edit();
	}
	
	private String getText(int stringId) {
		return getContext().getString(stringId);
	}
}
