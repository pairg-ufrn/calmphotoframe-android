package br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable;

import android.widget.Filterable;

import java.util.Collection;
import java.util.List;

public interface FilterableAdapter<T> extends Filterable {
    void setFilteredItems(List<T> values);

	void clearFilteredItems();

	Collection<T> getItems();

	Collection<T> getOriginalItems();
}
