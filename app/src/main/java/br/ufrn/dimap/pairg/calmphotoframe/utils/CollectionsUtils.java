package br.ufrn.dimap.pairg.calmphotoframe.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class CollectionsUtils {

    public interface PredicateFilter<T>{
        boolean verify(T item);
    }

    public static class ComparatorFilter<T> implements PredicateFilter<T>{
        private Comparator<T> comparator;
        private T other;

        public ComparatorFilter(Comparator<T> comparator, T other) {
            this.comparator = comparator;
            this.other = other;
        }

        @Override
        public boolean verify(T item) {
            return comparator.compare(item, other) == 0;
        }
    }

    static CollectionsUtils singleton;

    public static CollectionsUtils instance(){
        if(singleton == null){
            singleton = new CollectionsUtils();
        }

        return singleton;
    }

    public static <T> PredicateFilter<T> toFilter(Comparator<T> c, T instance){
        return new ComparatorFilter<>(c, instance);
    }

    /** Filters the collection {@code param} to remove all items that {@code filter} matches.*/
    public <T> List<T> filter(@NonNull List<T> items, @NonNull PredicateFilter<? super T> filter) {
        List<T> result = new ArrayList<>(items.size());

        for (T item : items){
            if(!filter.verify(item)){
                result.add(item);
            }
        }

        return result;
    }

    public <T> int indexOf(T item, List<? extends T> list, @NonNull PredicateFilter<? super T> filter){
        int index = 0;
        for(T current : list){
            if(filter.verify(current)){
                return index;
            }
            ++index;
        }
        return -1;
    }


    @Nullable
    public <T> T find(Collection<? extends T> items, @NonNull PredicateFilter<? super T> filter) {
        for(T current : items){
            if(filter.verify(current)){
                return current;
            }
        }
        return null;
    }

    public <T> boolean removeIf(Collection<? extends T> items, @NonNull PredicateFilter<? super T> filter) {
        T found = find(items, filter);

        if(found != null){
            items.remove(found);
            return true;
        }

        return false;
    }
}
