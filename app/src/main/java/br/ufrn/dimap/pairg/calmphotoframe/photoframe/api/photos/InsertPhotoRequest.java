package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.InputStreamEntity;


public class InsertPhotoRequest extends CommonGenericRequest<Photo>{
    private static final String JSON_CONTENT_TYPE = "application/json; encoding=UTF-8";
    private static final String PART_METADATA = "metadata";
    private static final String PART_IMAGE_NAME = "image";
    private ImageContent imageContent;
	private Long photoCollectionId;
	
	public InsertPhotoRequest(ApiConnector apiConnector, ImageContent imageContent){
		this(apiConnector, imageContent, null);
	}
	public InsertPhotoRequest(ApiConnector apiConnector, ImageContent imageContent
			,  ResultListener<? super Photo> listener)
	{
		this(apiConnector, imageContent, null, listener);
	}
	public InsertPhotoRequest(ApiConnector apiConnector, ImageContent imageContent, Long photoCollectionId
			,  ResultListener<? super Photo> listener)
	{
		super(apiConnector, Photo.class, listener);
		
		this.imageContent = imageContent;
		this.photoCollectionId = photoCollectionId;
	}
	
//	public RequestHandler execute(){
//		RequestParams params = new RequestParams();
//		putImagePart(imageContent, params);
//		tryPutFilenamePart(imageContent, params);
//
//		return postRequest(params);
//	}
    @Override
    public RequestHandler execute() {
        HttpEntity entity = getHttpEntity();

        return request()
                .listener(listener)
                .method(HttpMethods.Post)
                .content(entity).send();
    }

    public HttpEntity getHttpEntity() {
        ContentType contentType = ContentType.create(imageContent.getMimeType());

//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//
//        String filenameJson = getFilenameAsJson(imageContent);
//        if(filenameJson != null){
//            builder.addTextBody(PART_METADATA, filenameJson, ContentType.APPLICATION_JSON);
//        }
//
//        builder.addBinaryBody(PART_IMAGE_NAME, imageContent.getImageStream(), contentType,
//                getFilename(imageContent));
//
//        return builder.build();

        //Work-around for bug with Multipart requests: using simple image request
        return new InputStreamEntity(imageContent.getImageStream(), contentType);
    }

    @Override
	protected String genUrl() {
		if(photoCollectionId == null){
			return getApiBuilder().photosInDefaultContextUrl();
		}
		else{
			return getApiBuilder().collectionPhotosUrl(photoCollectionId);
		}
	}

	private void putImagePart(ImageContent imageDescription, RequestParams params) {
		params.put(PART_IMAGE_NAME, imageDescription.getImageStream(),
                    PART_IMAGE_NAME, imageDescription.getMimeType());
	}

	private void tryPutFilenamePart(ImageContent imageDescription, RequestParams params) {
        String filenameJson = getFilenameAsJson(imageDescription);
		if(filenameJson != null){
			InputStream jsonStream = textToInputStream(filenameJson);
			params.put(PART_METADATA, jsonStream, PART_IMAGE_NAME, JSON_CONTENT_TYPE);
		}
	}

    @Nullable
    private String getFilenameAsJson(ImageContent imageDescription) {
        String filename = getFilename(imageDescription.getOriginalFilepath());
        return filename == null ? null : filenameToJson(filename);
    }

    private String filenameToJson(String filename) {
		try {
			return getObjectMapper().writeValueAsString(new PhotoInsertionMetadata(filename));
		} catch (JsonProcessingException e) { //this should not happen
			throw new RuntimeException(e);
		}
	}

	private InputStream textToInputStream(String text) {
		byte[] asBytes = text.getBytes(Charset.forName("UTF-8"));
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(asBytes);
		return byteArrayInputStream;
	}
    private String getFilename(ImageContent imageContent) {
        return imageContent == null ? null : getFilename(imageContent.getOriginalFilepath());
    }
	private String getFilename(String originalFilepath) {
		if(originalFilepath == null){
			return null;
		}
		return new File(originalFilepath).getName();
	}
	
	/* Class to help in serialization**/
	static class PhotoInsertionMetadata{
		static class InsertionMetadata{
			public String originalFilename;
			
			public InsertionMetadata(String filename) {
				this.originalFilename = filename;
			}
		}
		
		public InsertionMetadata metadata;
		
		public PhotoInsertionMetadata(String filename) {
			this.metadata = new InsertionMetadata(filename);
		}
	}
}