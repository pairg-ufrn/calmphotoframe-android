package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoContextVisibilityAdapter;

public class PhotoContextViewBinder extends InflaterViewBinder<PhotoContext> {

    private EntityGetter<Long> selectedCtxGetter;

	private TextView contextNameTxt;
	private Spinner visibilitySpinner;
	private CompoundButton contextSelectedView;
	private ImageView userPermissionIcon;

	private Context context;

	private static final PhotoContextVisibilityAdapter visibilityAdapter = new PhotoContextVisibilityAdapter();
	
	public PhotoContextViewBinder(@Nullable EntityGetter<Long> selectedCtxGetter) {
		super(R.layout.item_photocontext);

		this.selectedCtxGetter = selectedCtxGetter;
	}

	@Override
	public void setup(View v) {
		this.context = v.getContext();

		this.contextNameTxt 	= ViewUtils.getView(v, R.id.photocontext_name);
		this.visibilitySpinner 	= ViewUtils.getView(v, R.id.photocontext_visibility);
		this.contextSelectedView= ViewUtils.getView(v, R.id.photocontext_selected);
		this.userPermissionIcon = ViewUtils.getView(v, R.id.photocontext_userpermission_icon);

		contextSelectedView.setFocusable(false);
		contextSelectedView.setFocusableInTouchMode(false);
		
		visibilitySpinner.setAdapter(visibilityAdapter);
		visibilitySpinner.setFocusable(false);
		visibilitySpinner.setFocusableInTouchMode(false);
		visibilitySpinner.setEnabled(false);

		setupSelectorVisibility();
	}
	@Override
	public void bind(PhotoContext photoContext) {
//		this.contextId = photoContext.getId();
		contextNameTxt.setText(getContextName(photoContext));
		visibilitySpinner.setSelection(getCurrentVisibilityPos(photoContext));

		if(photoContext instanceof PhotoContextWithPermission){
			PermissionLevel permissionLevel = ((PhotoContextWithPermission)photoContext).getPermission().getLevel();

			userPermissionIcon.setVisibility(View.VISIBLE);
			userPermissionIcon.setImageResource(getPermissionIcon(permissionLevel));
		}
		else{
			userPermissionIcon.setVisibility(View.GONE);
		}

		if(selectedCtxGetter != null){
			Long selectedCtx =  selectedCtxGetter.getEntity();
			boolean isSelected = selectedCtx != null && photoContext.getId() == selectedCtx;

			contextSelectedView.setChecked(isSelected);
		}
	}
	private CharSequence getContextName(PhotoContext photoContext) {
		return ContentViewHelper.getContextName(context, photoContext);
	}

	private int getCurrentVisibilityPos(PhotoContext photoContext) {
		return photoContext.getVisibility() == null ? 0 : visibilityAdapter.indexOf(photoContext.getVisibility());
	}

	private void setupSelectorVisibility() {
		if(selectedCtxGetter == null){
			contextSelectedView.setVisibility(View.GONE);
		}
		else{
			contextSelectedView.setVisibility(View.VISIBLE);
		}
	}

	private int getPermissionIcon(PermissionLevel level){
		switch (level){
			case Read:
				return R.drawable.eye;
			case Write:
				return R.drawable.pencil;
			case Admin:
				return R.drawable.crown;
		}
		return 0;
	}

}