package br.ufrn.dimap.pairg.calmphotoframe.activities;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnErrorNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

public abstract class ConnectedActivity extends NavActivity{

    ConnectionHelper connectionHelper;

    public ConnectedActivity(){
        connectionHelper = new ConnectionHelper(this);

        connectionHelper.listeners().setOnLocalServerFinish(new OnLocalServerFinished());
        connectionHelper.listeners().setOnSessionUpdate(new SessionUpdateListener());
        connectionHelper.listeners().setOnSessionFinish(new OnSessionFinishListener());
        connectionHelper.listeners().setOnConnectionError(new OnConnectionErrorListener());
    }

    @Override
    protected void onStart() {
        super.onStart();
        connectionHelper.start();

        /* NOTE: check on every 'onStart' may be redundant (ex.: when changing from activities), but
         * is more 'safe'. */
        connectionHelper.checkConnection();
    }

    @Override
    protected void onStop() {
        super.onStop();
        connectionHelper.stop();
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        connectionHelper.checkConnection();
//    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        //Activity was restored, so check if it is connected
//        connectionHelper.checkConnection();
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if(ApiClient.instance().hasEndpoint()){
//            refreshEndpointView(ApiClient.instance().getEndpoint());
//            loadData();
//        }
//        else{
//            onDisconnect();
//        }
//    }

    protected void onConnect(ApiEndpoint apiEndpoint){
        refreshEndpointView(apiEndpoint);
        loadData();
    }


    /** Called when the activity should load the data from api. */
    protected void loadData()
    {}

    protected void refreshEndpointView(ApiEndpoint apiEndpoint) {
        bindApiEndpoint(apiEndpoint);
        refreshNavigationMenu();
    }

    protected void onDisconnect() {
        notifyUser(getText(R.string.connection_error_disconnected))
            .duration(NotificationBuilder.Duration.Indefinite)
            .action(getText(R.string.action_signin), new Runnable() {
                @Override
                public void run() {
                    IntentUtils.startActivity(ConnectedActivity.this, LoginActivity.class);
                }
            })
            .show();
    }

    protected void onLocalServerDisconnected(final ConnectionHelper connectionHelper) {
        if(connectionHelper.isConnectedToLocal()) {
            notifyUser()
                    .text(R.string.connection_message_local_service_finished)
                    .action(R.string.connection_action_restart, new RestartAction(connectionHelper))
                    .duration(NotificationBuilder.Duration.Indefinite)
                    .show();
        }
    }

    private static class RestartAction implements Runnable {
        private final ConnectionHelper connectionHelper;

        public RestartAction(ConnectionHelper connectionHelper) {
            this.connectionHelper = connectionHelper;
        }

        @Override
        public void run() {
            connectionHelper.restartService();
        }
    }

    /* *********************************************************************************************/

    protected class SessionUpdateListener extends BasicResultListener<ApiEndpoint> {
        @Override
        protected void handleResult(ApiEndpoint result) {
            onConnect(result);
        }
    }

    protected class OnLocalServerFinished extends BasicResultListener<ConnectionHelper> {
        @Override
        public void handleResult(ConnectionHelper connectionHelper) {
            onLocalServerDisconnected(connectionHelper);
        }
    }

    protected class OnConnectionErrorListener extends OnErrorNotifyUser {
        public OnConnectionErrorListener() {
            super(ConnectedActivity.this);
        }

        @Override
        protected void putErrorOnNotifier(NotificationBuilder notificationBuilder, Throwable error) {
            notificationBuilder.text(R.string.connection_error_generic)
                    .duration(NotificationBuilder.Duration.Indefinite)
                    .action(R.string.generic_retry_action, new ReconnectAction());
        }
    }

    protected class OnSessionFinishListener extends BasicResultListener<Endpoint> {
        @Override
        protected void handleResult(Endpoint result) {
            onDisconnect();
        }
    }

    protected class ReconnectAction implements Runnable {
        @Override
        public void run() {
            connectionHelper.checkConnection();
        }
    }
}
