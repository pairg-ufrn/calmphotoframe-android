package br.ufrn.dimap.pairg.calmphotoframe.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReference;

import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;

public class IntentUtils {
	private static AtomicReference<IntentUtils> singleton = new AtomicReference<>();
    public static IntentUtils instance(){
        if(singleton.get() == null){
            singleton.compareAndSet(null, new IntentUtils());
        }
        return singleton.get();
    }
    public static void instance(IntentUtils instance){
        singleton.set(instance);
    }

	public static void startActivity(Context ctx, Class<? extends Activity> classObj){
		startActivity(ctx, classObj, null);
	}

	public static void startActivity(Context ctx, Class<? extends Activity> classObj, Bundle extras) {
		startActivity(ctx, classObj, extras, 0);
	}
	public static void startActivity(Context ctx, Class<? extends Activity> classObj, Bundle extras, int flags) {
		Intent intent = new Intent(ctx, classObj);
		intent.addFlags(flags);

		if(extras != null){
			intent.putExtras(extras);
		}
		ctx.startActivity(intent);
	}

    public PendingIntent buildPendingIntent(Context context, Class targetClass, int requestCode, int flags){
        if(targetClass.isInstance(Activity.class)) {
            Intent intent = new Intent(context, targetClass);
            return PendingIntent.getActivity(context, requestCode, intent, flags);
        }
        return null;
    }

	public Intent buildGetImageIntent(boolean allowMultipleImages) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");

        //Se for possível, permite a seleção de múltiplas imagens de uma vez
        photoPickerIntent.putExtra(Intents.Extras.allowMultiple(), allowMultipleImages);

		return photoPickerIntent;
	}

	@SuppressWarnings("unchecked")
	public <T extends Serializable> T getSerializable(Intent startupIntent, String extraKey) {
		if(startupIntent != null){
			try{
				return (T)startupIntent.getSerializableExtra(extraKey);
			}
			catch(ClassCastException ex){
				String errMsg = "ClassCastException when trying to get extra '"+ extraKey +"' from intent.";
				throw new IllegalArgumentException(errMsg,ex);
			}
		}
		return null;
	}

	public String getImageMimeType(InputStream input){
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(input, null, options);
		return options.outMimeType;
	}

    /** Get file mime type based on extension */
    public String getFileMimeType(String path) {
        return FileUtils.getMimeType(new File(path));
    }

    public String getImagePath(Context context, Uri uri){
        return getImagePath(context, uri, true);
    }

    public String getImagePath(Context context, Uri uri, boolean onlyLocal) {
        if( uri == null ) {
            return null;
        }

        String filepath = tryGetPath(context, uri);
        boolean isLocal = FileUtils.isLocal(filepath);
        if(onlyLocal){
            return isLocal ? filepath : null;
        }
        else{
            return filepath != null ? filepath : uri.toString();
        }
    }

    protected String tryGetPath(Context context, Uri uri) {
        try{
            return FileUtils.getPath(context, uri);
        }
        catch (IllegalArgumentException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
