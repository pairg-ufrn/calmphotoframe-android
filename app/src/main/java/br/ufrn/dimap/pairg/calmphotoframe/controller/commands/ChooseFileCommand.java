package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;


import android.content.DialogInterface;
import android.content.pm.PackageManager;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;

public class ChooseFileCommand extends BaseArgsCommand<ChooseFileCommand.FileChooserArguments, String[]>{

    public static class FileChooserArguments{
        private boolean selectDir;
        private String startPath;

        private int titleRes=0;
        private CharSequence title;

        public FileChooserArguments(String startPath, boolean selectDir) {
            this.startPath = startPath;
            this.selectDir = selectDir;
        }

        public boolean getSelectDir() {
            return selectDir;
        }

        public void setSelectDir(boolean selectDir) {
            this.selectDir = selectDir;
        }

        public String getStartPath() {
            return startPath;
        }

        public void setStartPath(String startPath) {
            this.startPath = startPath;
        }

        public CharSequence getTitle() {
            return title;
        }

        public void setTitle(CharSequence title) {
            this.title = title;
        }

        public int getTitleRes() {
            return titleRes;
        }

        public void setTitleRes(int titleRes) {
            this.titleRes = titleRes;
        }
    }

    private BaseActivity activity;

    public ChooseFileCommand(BaseActivity activity) {
        this.activity = activity;

    }

    @Override
    public void execute(FileChooserArguments arguments, final ResultListener<? super String[]> listener) {


        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type =
                (arguments.getSelectDir() ? DialogConfigs.DIR_SELECT : DialogConfigs.FILE_SELECT);

        final FilePickerDialog dialog = new FilePickerDialog(getActivity(), properties);
        dialog.setTitle(getTitle(arguments));
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                Listeners.onResult(listener, files);
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(listener);
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                cancel(listener);
            }
        });

        //This is needed for android Marshmallow and greater
        activity.listenToPermissionRequest(FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT,
                new ShowDialogOnPermissionGrant(dialog));

        dialog.show();
    }

    private void cancel(ResultListener<? super String[]> listener) {
        Listeners.onResult(listener, new String[0]);
    }

    private CharSequence getTitle(FileChooserArguments arguments) {
        if(arguments != null){
            if(arguments.getTitle() != null) {
                return arguments.getTitle();
            }
            else if(arguments.getTitleRes() != 0){
                return getActivity().getText(arguments.getTitleRes());
            }
        }

        return null;
    }

    public BaseActivity getActivity() {
        return activity;
    }

    private class ShowDialogOnPermissionGrant implements BaseActivity.PermissionRequestListener {
        private final FilePickerDialog dialog;

        public ShowDialogOnPermissionGrant(FilePickerDialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(dialog !=null){   //Show dialog if the read permission has been granted.
                    dialog.show();
                }
            }
            else {
                getActivity().notifyUser().text("Permission is Required for getting list of files").show();
            }
        }
    }
}
