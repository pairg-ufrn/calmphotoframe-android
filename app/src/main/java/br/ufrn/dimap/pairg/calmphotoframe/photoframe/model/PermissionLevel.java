package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public enum PermissionLevel {
	Read, Write, Admin
}
