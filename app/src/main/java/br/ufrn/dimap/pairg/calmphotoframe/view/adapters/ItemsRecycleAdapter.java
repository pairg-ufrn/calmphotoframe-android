package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.Collection;
import java.util.LinkedList;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.ItemsSelectionManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.ItemsViewRecycler;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;

/**
 */
public class ItemsRecycleAdapter<T> extends ViewBuilderRecyclerAdapter<T> implements ItemSelector<T> {

    public interface BinderBuilder<T>{
        BinderViewHolder<T> buildViewBinder();
    }

    public interface IdGenerator<T>{
        long getId(T item, ItemsManager<T> items);
        boolean hasStableIds();
    }

    private BinderBuilder<T> binderBuilder;
    private ItemsManager itemsManager;
    private IdGenerator<T> idGenerator;
    private ItemsSelectionManager selectionManager;

    public ItemsRecycleAdapter() {
        this(null);
    }

    public ItemsRecycleAdapter(@Nullable BinderBuilder<T> binderBuilder) {
        itemsManager = new ItemsManager();
        this.binderBuilder = binderBuilder;
        selectionManager = new ItemsSelectionManager();

        idGenerator = new DefaultIdGenerator<>();
        setHasStableIds(idGenerator.hasStableIds());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        itemsManager.setAdapter(this);

        ItemsViewRecycler itemsView = new ItemsViewRecycler(recyclerView.getContext(), this, recyclerView);
        selectionManager.setItemsView(itemsView);
    }

    @SuppressWarnings("unused")
    public IdGenerator<T> getIdGenerator() {
        return idGenerator;
    }

    public void setIdGenerator(IdGenerator<T> idGenerator) {
        this.idGenerator = (idGenerator == null) ? new DefaultIdGenerator<T>() : idGenerator;
        setHasStableIds(this.idGenerator.hasStableIds());
    }


    public BinderBuilder<T> getBinderBuilder() {
        return binderBuilder;
    }

    public void setBinderBuilder(BinderBuilder<T> binderBuilder) {
        this.binderBuilder = binderBuilder;
    }

    public BinderViewHolder<T> buildViewBinder(){
        if(getBinderBuilder() != null){
            return getBinderBuilder().buildViewBinder();
        }
        return null;
    }

    public ItemsManager<T> items(){
        return itemsManager;
    }

    public ItemsSelectionManager getSelectionManager(){
        return selectionManager;
    }

    @Override
    public int getItemCount() {
        return items().count();
    }

    @Override
    public T getEntity(int pos) {
        return items().get(pos);
    }

    @Override
    public long getItemId(int position) {
        return idGenerator == null ? -1 : idGenerator.getId(getEntity(position), items());
    }

    @Override
    public Collection<? extends T> getSelectedItems() {
        return getSelectedItemsImpl(null);
    }

    @Override
    public T getSelectedItem() {
        Collection<T> selectedItems = getSelectedItemsImpl(1);
        return selectedItems.isEmpty() ? null : selectedItems.iterator().next();
    }

    @NonNull
    protected Collection<T> getSelectedItemsImpl(Integer limit) {
        Collection<Long> selected = getSelectionManager().getSelectedIds();
        Collection<T> selectedItems = new LinkedList<>();

        for(int i=0; i < getItemCount(); ++i){
            if(selected.contains(getItemId(i))){
                selectedItems.add(items().get(i));

                if(limit != null && selectedItems.size() >= limit){
                    return  selectedItems;
                }
            }
        }

        return selectedItems;
    }

    public static class DefaultIdGenerator<ItemType> implements IdGenerator<ItemType> {
        @Override
        public long getId(ItemType item, ItemsManager<ItemType> items) {
            return items.indexOf(item);
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}
