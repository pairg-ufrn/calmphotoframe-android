package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.ViewGroup;

public interface BinderViewHolder<EntityType>{
	View buildView(ViewGroup viewParent, int viewType);
	void setup(View view);
	void bind(EntityType entity);
}