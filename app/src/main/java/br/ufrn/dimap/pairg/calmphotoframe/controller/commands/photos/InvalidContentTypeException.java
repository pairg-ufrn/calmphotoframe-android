package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

@SuppressWarnings("unused")
public class InvalidContentTypeException extends RuntimeException {
	public InvalidContentTypeException() {
		super();
	}

	public InvalidContentTypeException(String detailMessage) {
		super(detailMessage);
	}

	public InvalidContentTypeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public InvalidContentTypeException(Throwable throwable) {
		super(throwable);
	}
}
