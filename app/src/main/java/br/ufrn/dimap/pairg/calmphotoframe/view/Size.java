package br.ufrn.dimap.pairg.calmphotoframe.view;

/**
 */
public class Size {
    int width, height;

    @SuppressWarnings("unused")
    public Size() {
        this(0, 0);
    }

    public Size(Size size) {
        this(size.getWidth(), size.getHeight());
    }

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Size{" + getWidth() + ", " + getHeight() + "}";
    }
}
