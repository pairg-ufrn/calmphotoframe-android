package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class PhotoContext implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long INVALID_PHOTOCONTEXTID;
	
	private long id;
	private String name;
	
	private Long rootCollectionId;
	
	private PhotoContextVisibility visibility;

	/** (optional) Store the root photoCollection of this PhotoContext*/
	private PhotoCollection rootCollection;
	
	public PhotoContext() {
		this(INVALID_PHOTOCONTEXTID);
	}
	public PhotoContext(long contextId) {
		this(contextId, "");
	}
	public PhotoContext(long contextId, String name) {
		this(contextId, name, (Long)null);
	}
	public PhotoContext(long contextId, String name, PhotoCollection rootCollection){
		this(contextId, name, getCollId(rootCollection));
		this.rootCollection = rootCollection;
	}
	public PhotoContext(long contextId, String name, Long rootCollectionId){
		this(contextId, name, rootCollectionId, null);
	}
	public PhotoContext(long contextId, String name, Long rootCollectionId, PhotoContextVisibility visibility){
		this.id = contextId;
		this.name = name;
		this.rootCollectionId = rootCollectionId;
		this.rootCollection = null;
		this.visibility = visibility;
	}
	
	public PhotoContext(PhotoContext other) {
		if(other != null){
			this.set(other);
		}
	}
	
	private static Long getCollId(PhotoCollection collection){
		return collection != null ? Long.valueOf(collection.getId()) : null;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getRootCollectionId() {
		return rootCollectionId;
	}
	public void setRootCollectionId(Long rootCollectionId) {
		this.rootCollectionId = rootCollectionId;
	}

	public PhotoCollection getRootCollection() {
		return rootCollection;
	}
	public void setRootCollection(PhotoCollection rootCollection) {
		this.rootCollection = rootCollection;
	}
	
	public PhotoContextVisibility getVisibility() {
		return visibility;
	}
	public void setVisibility(PhotoContextVisibility visibility) {
		this.visibility = visibility;
	}
	
	public void set(PhotoContext other) {
		if(other == this){//same instance
			return;
		}
		
		this.setId(other.getId());
		this.setName(other.getName());
		this.setRootCollection(other.getRootCollection());
		this.setRootCollectionId(other.getRootCollectionId());
		this.setVisibility(other.getVisibility());
	}
	@Override
	public PhotoContext clone() {
		PhotoContext ctx = new PhotoContext();
		ctx.set(this);
		
		return ctx;
	}
	
	@Override
	public String toString() {
		return "PhotoContext [id=" + id + ", name=" + name + ", rootCollectionId=" + rootCollectionId + "]";
	}
}
