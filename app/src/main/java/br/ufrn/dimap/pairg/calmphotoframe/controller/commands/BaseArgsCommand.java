package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import android.support.annotation.Nullable;

public abstract  class BaseArgsCommand<Arg, Result> extends BaseStatelessCommand<Result> implements ArgsCommand<Arg, Result> {
    @Override
    public abstract void execute(@Nullable  Arg arg, ResultListener<? super Result> listener);

    @Override
    public void execute(ResultListener<? super Result> listener) {
        execute(null, listener);
    }
}
