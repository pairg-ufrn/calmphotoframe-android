package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu;

import android.view.MotionEvent;
import android.view.View;

import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu.FabMenu;

/**
 */
public class ShowOverlayOnMenuUpdate implements FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
    private final View overlayView;
    private final FabMenu fabMenu;

    public ShowOverlayOnMenuUpdate(View overlayView, FabMenu fabMenu) {
        this.overlayView = overlayView;
        this.fabMenu = fabMenu;
    }

    @Override
    public void onMenuExpanded() {
        overlayView.setVisibility(View.VISIBLE);
        overlayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fabMenu.collapse();
                return true;
            }
        });
    }

    @Override
    public void onMenuCollapsed() {
        overlayView.setVisibility(View.INVISIBLE);
        overlayView.setOnTouchListener(null);
    }
}
