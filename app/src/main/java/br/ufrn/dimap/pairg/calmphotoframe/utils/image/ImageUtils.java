package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import java.io.IOException;
import java.util.Arrays;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.PhotoFrameApplication;
import br.ufrn.dimap.pairg.calmphotoframe.utils.MemoryUtils;

public class ImageUtils {
	private static ImageUtils singleton;
	public static ImageUtils instance(){
		if(singleton == null){
			singleton = new ImageUtils();
		}
		return singleton;
	}

	private ImageDimensionsCalculator dimensionCalculator;
	
	public ImageUtils() {
		dimensionCalculator = new ApproximateFitDimensionsCalculator();
	}

	public int[] getDefaultMaxImageSize() {
		return getDefaultMaxImageSize(PhotoFrameApplication.getContext());
	}
	public int[] getDefaultMaxImageSize(Context context) {
		//Por padrão, redimensiona imagem carregada para que tenha no máximo o tamanho da tela
		final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int[] maxPhotoSize = new int[]{displayMetrics.widthPixels, displayMetrics.heightPixels};
		return maxPhotoSize;
	}

	public int[] getLimitImageSize() {
		return getLimitImageSize(PhotoFrameApplication.getContext());
	}
	public int[] getLimitImageSize(Context context) {
		int[] maxSize =  getDefaultMaxImageSize(context);
		
		int cacheSize = MemoryUtils.defaultImageCacheSize();
		int maxImageSize = cacheSize/2;
		
		int estimatedSize = estimateImageSize(maxSize);
		if(estimatedSize > maxImageSize){
			double scaleSize =maxImageSize/(double)estimatedSize;
			double scaleDimension = Math.sqrt(scaleSize);
			maxSize[0] = (int)(maxSize[0] * scaleDimension);
			maxSize[1] = (int)(maxSize[1] * scaleDimension);
		}
		
		return maxSize;
	}

	public int estimateImageSize(int[] maxImageSize) {
		int numPixels = maxImageSize[0] * maxImageSize[1];
		int estimateBytesPerPixel = 4;
		
		return numPixels * estimateBytesPerPixel;
	}

	public Bitmap getStoredMiniThumbnail(Context context, long imgId){
		return getStoredMiniThumbnail(context, imgId, null);
	}
	public Bitmap getStoredMiniThumbnail(Context context, long imgId, @Nullable BitmapFactory.Options options){
		return MediaStore.Images.Thumbnails.getThumbnail(
				context.getContentResolver(), imgId, MediaStore.Images.Thumbnails.MINI_KIND, options);
	}
	public Bitmap loadThumbnail(BitmapDecoder bitmapDecoder, int thumbnailSize)
			 throws IOException
	{
		Bitmap minifiedImage = loadReducedBitmap(bitmapDecoder, new int[]{thumbnailSize, thumbnailSize}, false);
		
		if(minifiedImage != null){
			Bitmap thumbnail = ThumbnailUtils.extractThumbnail(minifiedImage, thumbnailSize, thumbnailSize);
		
			minifiedImage.recycle();
		
			return thumbnail;
		}
		return null;
	}
	
	public Bitmap loadReducedBitmap(BitmapDecoder bitmapDecoder, int[] maxDimensions) 
			throws IOException 
	{
		return loadReducedBitmap(bitmapDecoder, maxDimensions, true);
	}


	public Bitmap loadReducedBitmap(BitmapDecoder bitmapDecoder, int[] wantedDimensions, boolean forceMaxSize)  throws IOException
	{
		return loadReducedBitmap(bitmapDecoder, new ReduceOptions(wantedDimensions, forceMaxSize, this.dimensionCalculator));
	}
	
	public static class ReduceOptions{
		int[] wantedDimensions;
		int[] maxDimensions;
		boolean scaleDownIfNeeded;
		ImageDimensionsCalculator dimensionsCalculator;
		
		public ReduceOptions(int[] maxDimensions) {
			this(maxDimensions, true);
		}

		public ReduceOptions(int[] maxDimensions, boolean scaleDownIfNeeded) {
			this(maxDimensions, scaleDownIfNeeded, null);
		}
		public ReduceOptions(int[] maxDimensions, boolean scaleDownIfNeeded,
				ImageDimensionsCalculator dimensionsCalculator) {
			super();
			this.wantedDimensions = maxDimensions;
			this.scaleDownIfNeeded = scaleDownIfNeeded;
			this.dimensionsCalculator = dimensionsCalculator;
			this.maxDimensions = null;
		}

		public int[] getWantedDimensions() {
			return wantedDimensions;
		}
		public void setWantedDimensions(int[] dimensions) {
			this.wantedDimensions = dimensions;
		}

		public int[] getMaxDimensions() {
			return maxDimensions;
		}
		public void setMaxDimensions(int[] maxDimensions) {
			this.maxDimensions = maxDimensions;
		}

		public boolean getScaleDownIfNeeded() {
			return scaleDownIfNeeded;
		}
		public void setScaleDownIfNeeded(boolean scaleDownIfNeeded) {
			this.scaleDownIfNeeded = scaleDownIfNeeded;
		}

		public ImageDimensionsCalculator getDimensionsCalculator() {
			return dimensionsCalculator;
		}
		public void setDimensionsCalculator(ImageDimensionsCalculator dimensionsCalculator) {
			this.dimensionsCalculator = dimensionsCalculator;
		}
	}
	
	
	public Bitmap loadReducedBitmap(BitmapDecoder bitmapDecoder, ReduceOptions reduceOpts)  throws IOException
	{
		/** Código adaptado de : https://developer.android.com/training/displaying-bitmaps/load-bitmap.html*/
	
		int[] maxSize = new int[]{reduceOpts.getWantedDimensions()[0], reduceOpts.getWantedDimensions()[1]};
		final BitmapFactory.Options options = new BitmapFactory.Options();
		int[] imgSize = null;

		try{
			if(needReduce(maxSize, null)){
			    imgSize = decodeImageSize(bitmapDecoder, reduceOpts.getWantedDimensions(), options);
				maxSize = getCalculator(reduceOpts).calculateSize(imgSize, maxSize);
			}
	
		    options.inJustDecodeBounds = false;
		    
		    if(!needReduce(maxSize, imgSize)){
		    	return bitmapDecoder.decodeBitmap(options);
		    }
		    
		    int inSampleSize = calculateInSampleSize(imgSize, maxSize, reduceOpts.getMaxDimensions());
	
			Log.d(ImageUtils.class.getSimpleName(), 
					String.format("inSampleSize: %d, maxSize : %s, imgSize: %s"
							, inSampleSize, Arrays.toString(maxSize), Arrays.toString(imgSize)));
			
		    return decodeScaledDownImage(bitmapDecoder, maxSize, inSampleSize, options, reduceOpts.getScaleDownIfNeeded());
		}
		finally{
			bitmapDecoder.clear();
		}
	}

	private ImageDimensionsCalculator getCalculator(ReduceOptions reduceOpts) {
		ImageDimensionsCalculator dimCalculator = reduceOpts.getDimensionsCalculator();
		if(dimCalculator == null){
			dimCalculator = this.dimensionCalculator;
		}
		return dimCalculator;
	}
	private Bitmap decodeScaledDownImage(BitmapDecoder bitmapDecoder, int[] maxSize, int inSampleSize,
			final BitmapFactory.Options options, boolean forceMaxSize) throws IOException 
	{
		try{
			options.inSampleSize = inSampleSize;
			
			Bitmap bitmap = bitmapDecoder.decodeBitmap(options);
			bitmapDecoder.clear();
			
		    if(forceMaxSize && bitmapIsGreater(bitmap, maxSize)){
		        // If necessary, scale down to the maximal acceptable size.
		        bitmap = scaleDownBitmap(bitmap, maxSize); 
		    }
			return bitmap;
		}
		catch(OutOfMemoryError err){
			bitmapDecoder.clear();
			throw new IOException(err);
		}
	}

	public Bitmap scaleDownBitmap(Bitmap bitmap, int[] maxSize) {
		if (bitmap != null && bitmapIsGreater(bitmap, maxSize)) {
		    Bitmap outBitmap = Bitmap.createScaledBitmap(bitmap, maxSize[0], maxSize[1], true);
		    bitmap.recycle();
		    return outBitmap;
		}
		return bitmap;
	}
	
	/** Decodes the image size (and other informations) and stores in options.
	 * @return an array of length two, with the width and height of the decoded image. */
	private int[] decodeImageSize(BitmapDecoder bitmapDecoder, int[] maxDimensions, BitmapFactory.Options options)  throws IOException
	{
		try{
		    // Use inJustDecodeBounds=true to check dimensions
			options.inJustDecodeBounds = true;
		    bitmapDecoder.decodeBitmap(options);
		    return new int[]{options.outWidth, options.outHeight};
		}
		catch(OutOfMemoryError err){
			bitmapDecoder.clear();
			throw new IOException(err);
		}
	}
	
	private boolean bitmapIsGreater(Bitmap bitmap, int[] maxSize) {
		return bitmap.getWidth() > maxSize[0] || bitmap.getHeight() > maxSize[1];
	}
	
	private boolean needReduce(int[] maxSize, int[] imgSize) {
		if(imgSize == null){
			return maxSize[0] < Integer.MAX_VALUE || maxSize[1] < Integer.MAX_VALUE;
		}
		else{
			return imgSize[0] > maxSize[0] || imgSize[1] > maxSize[1];
		}
	}
	/** Adaptado de : https://developer.android.com/training/displaying-bitmaps/load-bitmap.html*/
	public int calculateInSampleSize(int[] imageSize, int[] requestedSize) {
	    return calculateInSampleSize(imageSize, requestedSize, null);
	}
	public int calculateInSampleSize(int[] imageSize, int[] requestedSize, int[] maxImageSize) {
	    // Raw height and width of image[
	    int inSampleSize = 1;
	    
		if (imageSize[0] > requestedSize[0] || imageSize[1] > requestedSize[1]) {
	
	        final int halfWidth = imageSize[0] / 2;
	        final int halfHeight = imageSize[1] / 2;
	
	        int sampledDownWidth = imageSize[0];
	        int sampledDownHeight = imageSize[1];
	        
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > requestedSize[1]
	                && (halfWidth / inSampleSize) > requestedSize[0]) {
	            inSampleSize *= 2;

	            sampledDownWidth = sampledDownWidth/2;
	            sampledDownHeight = sampledDownHeight/2;
	        }
	        
	        if(maxImageSize != null){
	        	while(sampledDownWidth > maxImageSize[0] 
	        			|| sampledDownHeight > maxImageSize[1])
	        	{
		        	inSampleSize *= 2;
		            sampledDownWidth = sampledDownWidth/2;
		            sampledDownHeight = sampledDownHeight/2;
	        	}
	        }
	    }
	    
	    return inSampleSize;
	}
	
}
