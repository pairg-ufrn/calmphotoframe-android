package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.util.Log;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class PhotosSearchEngine implements SearchHandler.SearchEngine {
    CollectionAndContextContainer collectionAndContext;
    ResultListener<? super List<Photo>> onQueryResultListener;

	String currentQuery;

	public PhotosSearchEngine(CollectionAndContextContainer collectionAndContext) {
        this.collectionAndContext = collectionAndContext;
    }

	public ResultListener<? super List<Photo>> getOnQueryResultListener() {
        return onQueryResultListener;
    }

	public void setOnQueryResultListener(ResultListener<? super List<Photo>> listener) {
        this.onQueryResultListener = listener;
    }

    public String getCurrentQuery() {
        return currentQuery;
    }

	public void executeQuery() {
        ApiClient.instance()
                .photos().listFromCollection(collectionAndContext.getCollectionId()
				, true
				, getCurrentQuery()
				, getOnQueryResultListener());
    }

	public void setCurrentQuery(String currentQuery) {
        this.currentQuery = currentQuery;
    }

	public void clearQuery() {
        setCurrentQuery(null);
    }

	public boolean hasQuery() {
        return getCurrentQuery() != null;
    }

    @Override
    public void doSearch(String query) {
        Log.d(getClass().getSimpleName(), "do search");

		setCurrentQuery(query);
        executeQuery();
    }
}
