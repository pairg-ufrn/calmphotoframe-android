package br.ufrn.dimap.pairg.calmphotoframe.utils;

import android.app.Activity;
import android.view.View;

public class ViewUtils {

	@SuppressWarnings("unchecked")
	static public <T extends View> T getView(View rootView, int viewId){
		return (T)rootView.findViewById(viewId);
	}
	@SuppressWarnings("unchecked")
	static public <T extends View> T getView(Activity activity, int viewId){
		return (T)activity.findViewById(viewId);
	}

	public static boolean setVisible(View view, boolean visible) {
		if(view != null){
			view.setVisibility(visible ? View.VISIBLE : View.GONE);
		}

		return visible && view != null;
	}
}
