package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

/** */
public class ApproximateFitDimensionsCalculator extends ScaleDimensionsCalculator{
//	@Override
//	public int[] calculateSize(int currentWidth, int currentHeight, int maxWidth, int maxHeight){	
//		maxWidth = Math.min(currentWidth, maxWidth);
//		maxHeight = Math.min(currentHeight, maxHeight);
//		
//		int width = currentWidth, height = currentHeight;
//		
//		if(Math.abs(currentHeight - maxHeight) < Math.abs(currentWidth - maxWidth)){
//			height = maxHeight;
//			double scale = ((double)height) /currentHeight;
//			width = (int) (scale * currentWidth);
//		}
//		else{
//			width = maxWidth;
//			double scale = ((double)width) /currentWidth;
//			height = (int) (scale * currentHeight);
//		}		
//		
//		return new int[]{width, height};
//	}

	@Override
	protected int choiceScaleDimension(int[] imageDims, int[] maxDims) {
		if(Math.abs(imageDims[1] - maxDims[1]) < Math.abs(imageDims[0] - maxDims[0]))
			return 1;
		else{
			return 0;
		}
	}

	
}