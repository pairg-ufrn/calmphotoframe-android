package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.ViewGroup;

public abstract class ViewBinderDelegator<T> implements BinderViewHolder<T>{
	BinderViewHolder<T> delegate;
	
	public ViewBinderDelegator(BinderViewHolder<T> delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public View buildView(ViewGroup viewParent, int viewType) {
		return delegate.buildView(viewParent, viewType);
	}

	@Override
	public void setup(View view) {
		delegate.setup(view);
	}

	@Override
	public void bind(T entity) {
		delegate.bind(entity);
	}
}
