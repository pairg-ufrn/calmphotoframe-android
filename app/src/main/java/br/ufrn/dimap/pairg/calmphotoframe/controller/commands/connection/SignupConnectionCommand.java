package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class SignupConnectionCommand extends ConnectionCommand<User> {
    @Override
    public void doConnection(ConnectionInfo connInfo, ResultListener<? super User> listener) {
        ApiClient.instance().users().create(connInfo.getLoginInfo(), connInfo.getEndpoint(), listener);
    }
}
