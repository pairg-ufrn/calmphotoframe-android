package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.AsyncGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.EventViewBinder;

public class RecentEventsFragment extends ItemsRecycleFragment<Event>
        implements OnClickListener, ItemsRecycleAdapter.BinderBuilder<Event>
{
	private OnClickListener toolbarNavigationOnClickListener;
	private AsyncGetter<Collection<Event>> eventGetter;

    public RecentEventsFragment() {
        setBinderBuilder(this);
    }

    @SuppressWarnings("unused")
    public AsyncGetter<Collection<Event>> getEventGetter() {
		return eventGetter;
	}
	public void setEventGetter(AsyncGetter<Collection<Event>> eventGetter) {
		this.eventGetter = eventGetter;
	}

	public OnClickListener getToolbarNavigationOnClickListener() {
		return toolbarNavigationOnClickListener;
	}
	public void setToolbarNavigationOnClickListener(OnClickListener toolbarNavigationOnClickListener) {
		this.toolbarNavigationOnClickListener = toolbarNavigationOnClickListener;
	}

	/* *******************************************************************************************/

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		Toolbar toolbar = ViewUtils.getView(rootView, R.id.recent_events_toolbar);
		toolbar.setTitle(R.string.recentEvents_title);
		
		toolbar.setNavigationIcon(R.drawable.ic_action_back);
		toolbar.setNavigationOnClickListener(this);
		
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		refreshData();
	}

	public void refreshData() {
		if(eventGetter != null){
			eventGetter.getAsync(new BasicResultListener<Collection<Event>>(){
				@Override
				public void onResult(Collection<Event> result) {
					getAdapter().items().set(result);
				}
				@Override
				public void onFailure(Throwable error) {
					Log.e(getClass().getSimpleName(), "Failed to get events", error);
				}
			});
		}
	}

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recent_events;
    }

	/* *******************************************************************************************/
	
	@Override
	public void onClick(View view) {
		if(getToolbarNavigationOnClickListener() != null){
			getToolbarNavigationOnClickListener().onClick(view);
		}
	}

    @Override
    public BinderViewHolder<Event> buildViewBinder() {
        return new EventViewBinder();
    }
}
