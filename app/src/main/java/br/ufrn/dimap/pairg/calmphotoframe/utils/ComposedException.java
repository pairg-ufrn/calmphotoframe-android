package br.ufrn.dimap.pairg.calmphotoframe.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ComposedException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Collection<Throwable> exceptions;
	
	public ComposedException() {
		super();
		exceptions = Collections.emptyList();
	}

	public ComposedException(Throwable ...throwables) {
		this("", throwables);
	}

	public ComposedException(String detailMessage, Throwable ...throwables) {
		super(detailMessage, throwables[0]);
		exceptions = Arrays.asList(throwables);
	}

	public Collection<Throwable> getExceptions() {
		return exceptions;
	}
}
