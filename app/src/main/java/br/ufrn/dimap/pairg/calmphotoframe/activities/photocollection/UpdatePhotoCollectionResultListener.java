package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;

public class UpdatePhotoCollectionResultListener extends BasicResultListener<PhotoCollection> {
	ItemsManager<? super PhotoComponent> itemsManager;

	public UpdatePhotoCollectionResultListener(ItemsRecycleFragment<? super PhotoComponent> itemsRecycleFragment) {
		this(itemsRecycleFragment.getAdapter().items());
	}
	public UpdatePhotoCollectionResultListener(ItemsManager<? super PhotoComponent> itemsManager) {
		this.itemsManager = itemsManager;
	}

	@Override
	public void onResult(PhotoCollection photoColl) {
		itemsManager.addItem(new PhotoComponent(photoColl));
	}
}