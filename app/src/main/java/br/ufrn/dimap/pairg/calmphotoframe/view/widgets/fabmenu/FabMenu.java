package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnClickDelegator;

/**
 * Created by noface on 07/05/16.
 */
public class FabMenu {
    private FloatingActionsMenu fabMenuView;

    private boolean visible;
    private OnClickDelegator onClickDelegator;

    public FabMenu() {
        onClickDelegator = new OnClickDelegator();
        visible = true;
    }

    public void create(FloatingActionsMenu fabMenu) {
        this.fabMenuView = fabMenu;
        bindMenuVisibility();
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        bindMenuVisibility();
    }

    protected void bindMenuVisibility() {
        if (fabMenuView != null) {
            fabMenuView.setVisibility(this.visible ? View.VISIBLE : View.GONE);
        }
    }

    public void addItem(int id, int iconRes, int titleRes) {
        Context context = fabMenuView.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        FloatingActionButton fabItem = (FloatingActionButton) inflater.inflate(R.layout.view_fab_menu_item, fabMenuView, false);
        fabItem.setId(id);
        fabItem.setIcon(iconRes);
        fabItem.setTitle(context.getString(titleRes));
        fabItem.setOnClickListener(onClickDelegator);

        fabMenuView.addButton(fabItem);
    }

    public void addOnClickMenuItemListener(View.OnClickListener listener) {
        onClickDelegator.addListener(listener);
    }

    @SuppressWarnings("unused")
    public void removeOnClickMenuItemListener(View.OnClickListener listener) {
        onClickDelegator.removeListener(listener);
    }

    public FloatingActionsMenu getView() {
        return fabMenuView;
    }

    public void setOnUpdateMenuListener(FloatingActionsMenu.OnFloatingActionsMenuUpdateListener listener) {
        fabMenuView.setOnFloatingActionsMenuUpdateListener(listener);
    }

    public void collapse() {
        fabMenuView.collapse();
    }
}
