package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

/**
 */
public class StartActivityCommand extends BaseStatelessCommand<Void> {
    private Context context;
    private Class<? extends Activity> activityClass;
    private int flags;
    private Bundle extras;

    public StartActivityCommand(Context ctx, Class<? extends Activity> activityClass) {
        this.context = ctx;
        this.activityClass = activityClass;
        this.extras = null;
        this.flags = 0;
    }

    public StartActivityCommand flags(int flags){
        this.flags = flags;
        return this;
    }

    public StartActivityCommand extras(Bundle extras){
        this.extras = extras;
        return this;
    }

    @Override
    public void execute(ResultListener<? super Void> cmdResultListener) {
        Listeners.onStart(cmdResultListener);

        IntentUtils.startActivity(context, activityClass, extras, flags);
    }
}
