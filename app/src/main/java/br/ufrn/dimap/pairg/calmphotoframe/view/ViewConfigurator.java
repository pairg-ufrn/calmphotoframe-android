package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.view.View;

/**
 */
public interface ViewConfigurator {
    void configureView(View rootView);
}
