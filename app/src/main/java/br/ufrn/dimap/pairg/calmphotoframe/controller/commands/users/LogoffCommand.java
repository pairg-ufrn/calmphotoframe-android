package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import br.ufrn.dimap.pairg.calmphotoframe.activities.LoginActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;

public class LogoffCommand extends BaseStatelessCommand<Endpoint> {
    private Context context;

    public LogoffCommand(Context context) {
        this.context = context;
    }

    @Override
    public void execute(ResultListener<? super Endpoint> listener) {
        ResultListener<? super Endpoint> resultListener = new CompositeResultListener<>(
                listener,
                new StartActivityOnResult(this.context));

        ApiClient.instance().session().disconnect(resultListener);
    }

    private static class StartActivityOnResult extends BasicResultListener<Endpoint> {
        private final Context context;

        public StartActivityOnResult(Context context) {
            this.context = context;
        }

        @Override
        public void onFailure(Throwable error) {
            super.onFailure(error);

            Log.w(LogoffCommand.class.getSimpleName(), "Error on logoff: " + String.valueOf(error));

            startLoginActivity();
        }

        @Override
        public void onResult(Endpoint result) {
            startLoginActivity();
        }

        protected void startLoginActivity() {
            Intent detailsIntent = new Intent(this.context, LoginActivity.class);

            // Use TaskStackBuilder to build the back stack and get the PendingIntent
            PendingIntent pendingIntent =
                    TaskStackBuilder.create(this.context)
                            // add all of DetailsActivity's parents to the stack,
                            // followed by DetailsActivity itself
                            .addNextIntentWithParentStack(detailsIntent)
                            .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
    }
}
