package br.ufrn.dimap.pairg.calmphotoframe.controller;

/**
 */
public class GetterSelectorAdapter<T> implements EntityGetter<T>{
    ItemSelector<? extends T> selector;

    public GetterSelectorAdapter(ItemSelector<? extends T> selector) {
        this.selector = selector;
    }

    @Override
    public T getEntity() {
        return selector.getSelectedItem();
    }
}
