package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ObjectMapperResponseHandler;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;

public class RequestBuilder<ResType, ListType> {

    ApiConnector connector;
    HttpMethods method = HttpMethods.Get;
    HttpEntity content;
    String url;
    List<Header> headers;

    Converter<ResType, ListType> converter;
    ResultListener<? super ListType> listener;
    Class<? extends ResType> responseClass;


    public RequestBuilder(ApiConnector connector) {
        this.connector = connector;
    }

    public String url() {
        return this.url;
    }

    public ResultListener<? super ListType> listener() {
        return this.listener;
    }

    public RequestBuilder<ResType, ListType> url(String url) {
        this.url = url;
        return this;
    }

    public RequestBuilder<ResType, ListType> method(HttpMethods method) {
        this.method = method;
        return this;
    }

    public RequestBuilder<ResType, ListType> json(String content) {
        return content(connector.http().jsonEntity(content));
    }

    public RequestBuilder<ResType, ListType> json(Object content) throws JsonProcessingException {
        return json(connector.getJsonConverter().writeValueAsString(content));
    }

    public RequestBuilder<ResType, ListType> content(HttpEntity content) {
        this.content = content;
        return this;
    }

    public RequestBuilder<ResType, ListType> converter(
            Converter<ResType, ListType> converter,
            Class<? extends ResType> responseClass) {
        this.responseClass = responseClass;

        return this.converter(converter);
    }
    public RequestBuilder<ResType, ListType> converter(Converter<ResType, ListType> converter) {
        this.converter = converter;

        return this;
    }

    public RequestBuilder<ResType, ListType> listener(ResultListener<? super ListType> listener) {
        this.listener = listener;
        return this;
    }

    public RequestBuilder<ResType, ListType> addHeader(Header header) {
        if (headers == null) {
            headers = new ArrayList<>();
        }
        headers.add(header);

        return this;
    }

    public RequestHandler send() {
        Header[] headers = (Header[]) (this.headers == null ? null : this.headers.toArray());
        return connector.http().sendMessage(method, url, buildResponseHandler(), content, headers);
    }

    public RequestHandler get() {
        return method(HttpMethods.Get).send();
    }

    public RequestHandler post() {
        return method(HttpMethods.Post).send();
    }

    public RequestHandler post(RequestParams params) {
        return connector.http().post(url, buildResponseHandler(), params);
    }

    public RequestHandler put() {
        return method(HttpMethods.Put).send();
    }

    public RequestHandler delete() {
        return method(HttpMethods.Delete).send();
    }

    private ResponseHandlerInterface buildResponseHandler() {
        ObjectResponseListenerAdapter<ResType, ListType> adapter
                = new ObjectResponseListenerAdapter<>(url, getConverter(), listener);

        return new ObjectMapperResponseHandler<>(
                connector.getJsonConverter(), responseClass, adapter);
    }

    private Converter<ResType, ListType> getConverter() {
        if(converter != null) {
            return converter;
        }

        return new Converter<ResType, ListType>() {
            @SuppressWarnings("unchecked")
            @Override
            public ListType convert(ResType source) {
                if(source == null || (responseClass != null && responseClass.isInstance(source))) {
                    return (ListType)source;
                }
                return null;
            }
        };
    }
}
