package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class OnResultUpdateCurrentUser extends BasicResultListener<User> {
    @Override
    public void onResult(User result) {
        ApiClient.instance().updateCurrentUser(result);
    }
}
