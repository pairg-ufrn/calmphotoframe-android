package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

public class NotificationBuilder {
    public enum Duration{ Short, Long, Indefinite}

    private Context context;
    private CharSequence text;
    private View anchorView;
    private CharSequence actionName;
    private Runnable actionCallback;
    private Duration duration;

    public NotificationBuilder(Context context) {
        this.context = context;
        this.duration = Duration.Long;
    }

    public NotificationBuilder anchorView(View anchorView) {
        this.anchorView = anchorView;
        return this;
    }

    public NotificationBuilder action(int resId, Runnable actionCallback) {
        return action(getTextResouce(resId), actionCallback);
    }
    public NotificationBuilder action(CharSequence actionName, Runnable actionCallback) {
        this.actionName = actionName;
        this.actionCallback = actionCallback;
        return this;
    }

    public NotificationBuilder text(int textId){
        return this.text(getTextResouce(textId));
    }

    public CharSequence getTextResouce(int textId) {
        if(context == null){
            throw new NullPointerException("Could not load text. Context is null.");
        }
        return this.context.getText(textId);
    }

    public NotificationBuilder text(CharSequence txt){
        this.text = txt;
        return this;
    }

    public NotificationBuilder duration(Duration duration) {
        this.duration = duration;
        return this;
    }

    public void show(){
        if(anchorView == null){
            //noinspection WrongConstant
            Toast.makeText(context, text, getToastDuration()).show();
        }
        else{
            //noinspection WrongConstant
            Snackbar snackbar = Snackbar.make(anchorView, text, getSnackbarDuration());
            if(actionCallback != null){
                snackbar.setAction(actionName, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actionCallback.run();
                    }
                });
            }
            snackbar.show();
        }
    }

    private int getToastDuration() {
        switch (duration){
            case Short:
                return Toast.LENGTH_SHORT;
            default:
                return Toast.LENGTH_LONG;
        }
    }
    private int getSnackbarDuration() {
        switch (duration){
            case Indefinite:
                return  Snackbar.LENGTH_INDEFINITE;
            case Short:
                return Snackbar.LENGTH_SHORT;
            default:
                return Snackbar.LENGTH_LONG;
        }
    }
}
