package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginMode;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable.DefaultCollectionFilter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable.FilterableViewBuilderAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.validators.HostnameValidator;
import br.ufrn.dimap.pairg.calmphotoframe.view.validators.LoginValidator;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.DefaultViewBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.InstantAutoCompleteText;

import static br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginMode.Login;

public class LoginSignupFragment extends Fragment{

    public interface SubmitListener{
		void onSubmit(ConnectionInfo connInfo, LoginMode loginMode);
	}

    private static final String CLASS = LoginSignupFragment.class.getName();
	private static final String ARGS_ISLOGIN = CLASS + "args.islogin";
	private static final String ARGS_SHOWINGLOGIN = CLASS + "args.showingLogin";
	
	private boolean isLogin;
	private boolean showingTitle;
	
	private SubmitListener submitListener;

	private LoginValidator loginValidator;
    private HostnameValidator photoFrameValidator;

    private LoginViewBuilder loginViewBuilder;

    public static LoginSignupFragment build(boolean isLogin, boolean showTitle){
        LoginSignupFragment fragment = new LoginSignupFragment();
        fragment.saveState(isLogin, showTitle);

        return fragment;
    }

    @NonNull
    private static Bundle makeArgsBundle(boolean isLogin, boolean showTitle) {
        Bundle args = new Bundle();
        args.putBoolean(ARGS_ISLOGIN, isLogin);
        args.putBoolean(ARGS_SHOWINGLOGIN, showTitle);
        return args;
    }

    public LoginSignupFragment(){
        this.isLogin = false;
        this.showingTitle = false;

        this.loginValidator = new LoginValidator(
                R.id.login_loginContainer, R.id.login_passwordContainer, R.id.login_confirmPasswordContainer);

        this.loginViewBuilder = new LoginViewBuilder();
        this.photoFrameValidator = new HostnameValidator();
    }

    @SuppressWarnings("unused")
    public SubmitListener getSubmitListener() {
        return submitListener;
    }

    public void setSubmitListener(SubmitListener submitListener) {
        this.submitListener = submitListener;
    }

    public boolean isShowingTitle() {
        return showingTitle;
    }

    public void setShowingTitle(boolean showingTitle) {
        this.showingTitle = showingTitle;

        loginViewBuilder.updateTitleVisibility();
    }

    public boolean isLogin(){
        return isLogin;
    }

    public void setIsLogin(boolean isLogin){
        this.isLogin = isLogin;
        this.loginValidator.setLoginMode(isLogin);
        this.loginViewBuilder.updateView();
    }

    public int getSubmitText() {
        return isLogin() ? R.string.action_signin : R.string.action_signup;
    }

    public int getTitle() {
        return isLogin() ? R.string.login_signin_title : R.string.login_signup_title;
    }

    public void setServerInfos(List<ServerInfo> servers) {
        this.loginViewBuilder.setItems(servers);
    }

    @SuppressWarnings("unused")
    public OnSelectItemListener<ServerInfo> getRemoveItemListener() {
        return this.loginViewBuilder.viewBuilder.getOnSelectItemListener();
    }
    public void setRemoveItemListener(OnSelectItemListener<ServerInfo> onSelectItemListener) {
        this.loginViewBuilder.viewBuilder.setOnSelectItemListener(onSelectItemListener);
    }

    /* **************************************************************************************************/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		restoreState();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_loginsignup, container, false);
		
		loginViewBuilder.setupViews(rootView);
		
		loginValidator.bindViews(rootView);

        photoFrameValidator.bindView(rootView, R.id.login_serverInput);
		
		return rootView;
	}

	/* **************************************************************************************************/

    protected void submit() {
		if(validate() && submitListener != null){
            LoginMode loginMode = isLogin() ? Login : LoginMode.Signup;
			submitListener.onSubmit(extractConnInfo(), loginMode);
		}
	}
    public boolean validate(){
        boolean isValid = loginValidator.validate();
        isValid &= photoFrameValidator.validate();
        return isValid;
    }

	private ConnectionInfo extractConnInfo() {
		LoginInfo loginInfo = new LoginInfo(getLogin(), getPassword());
		Endpoint photoFrameInfo = photoFrameValidator.parsePhotoFrameInfo();
		
		return new ConnectionInfo(photoFrameInfo, loginInfo);
	}

	private String getLogin() {
		return getViewText(R.id.login_loginInput);
	}
	private String getPassword() {
		return getViewText(R.id.login_passwordInput);
	}
	private String getViewText(int photoframeaddressInput) {
		TextView txt = findView(getView(), photoframeaddressInput);
		return txt.getText().toString();
	}
	
	protected void saveState(boolean isLogin, boolean showTitle) {
        this.isLogin = isLogin;
        this.showingTitle = showTitle;
		setArguments(makeArgsBundle(isLogin, showTitle));
	}

    protected void restoreState() {
		Bundle args = getArguments();
		if(args != null){
			isLogin = args.getBoolean(ARGS_ISLOGIN, isLogin);
			showingTitle = args.getBoolean(ARGS_SHOWINGLOGIN, showingTitle);
		}

        this.loginValidator.setLoginMode(this.isLogin());
	}
	
	protected int getVisibility(boolean visible) {
		return visible ? View.VISIBLE : View.GONE;
	}

	@SuppressWarnings("unchecked")
	public <T extends View> T findView(View root, int id){
		return (T)root.findViewById(id);
	}
	public <T extends View> T findView(int id){
		return findView(getView(), id);
	}

	private boolean isLastField(TextView v) {
		if(v == null){
			return false;
		}

		int viewId = v.getId();

		if(!isLogin()){
			return viewId == R.id.login_confirmPassword;
		}
		else{
			return viewId == R.id.login_passwordInput;
		}
	}


    private boolean hideKeyboard(Activity activity) {
        View focusView = activity.getCurrentFocus();
        if (focusView != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            return imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
        return false;
    }

	/* **************************************************************************************************/
    class LoginViewBuilder{

        ServerInfoViewBuilder viewBuilder;
        FilterableViewBuilderAdapter<ServerInfo> serversAdapter;

        EditText passEditor, confirmPass;
        View confirmPassContainer;
        Button submitBtn;
        TextView titleView;
        InstantAutoCompleteText serverInput;
        ImageButton toggleMode;
        Drawable loginIcon, signupIcon;

        boolean viewsCreated = false;

        public LoginViewBuilder(){
            this.viewBuilder = new ServerInfoViewBuilder();
            serversAdapter = new FilterableViewBuilderAdapter<>(viewBuilder);
            serversAdapter.setCollectionFilter(new ServerInfoCollectionFilter());
        }

        public void setupViews(View rootView) {
            onCreateView(rootView);
            updateView();
        }

        public void updateView(){
            if(this.viewsCreated) {
                updateLoginFields();
                updateText();
                updateTitleVisibility();
                loadIcons();

                toggleMode.setImageDrawable(getToggleDrawable());
            }
        }

        private void loadIcons() {
            loginIcon = AppCompatResources.getDrawable(getActivity(), R.drawable.ic_account_key);
            signupIcon = AppCompatResources.getDrawable(getActivity(),  R.drawable.account_plus);
        }


        public void updateTitleVisibility() {
            if(titleView != null) {
                //noinspection WrongConstant
                titleView.setVisibility(getVisibility(isShowingTitle()));
            }
        }

        void onCreateView(View rootView){
            findViews(rootView);
            configureViews();

            this.viewsCreated = true;
        }

        void findViews(View rootView){
            passEditor             = findView(rootView, R.id.login_passwordInput);
            confirmPass            = findView(rootView, R.id.login_confirmPassword);
            confirmPassContainer   = findView(rootView, R.id.login_confirmPasswordContainer);

            titleView = findView(rootView, R.id.loginSignup_title);
            submitBtn = findView(rootView, R.id.login_submitButton);
            serverInput = findView(rootView, R.id.login_serverInput);

            toggleMode = findView(rootView, R.id.login_toggleMode);
        }

        private void configureViews() {
            OnEditorActionListener actionListener = new SubmitOnEditorActionListener();

            confirmPass.setOnEditorActionListener(actionListener);
            passEditor.setOnEditorActionListener(actionListener);

            submitBtn.setOnClickListener(new SubmitOnClickListener());

            AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // Change focus to next view
                    serverInput.onEditorAction(EditorInfo.IME_ACTION_NEXT);
                }
            };

            serverInput.setAdapter(this.serversAdapter);
            serverInput.setOnItemClickListener(listener);

            toggleMode.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setIsLogin(!isLogin());
                }
            });
        }

        @SuppressWarnings("WrongConstant")
        private void updateLoginFields() {
            confirmPassContainer.setVisibility(getVisibility(!isLogin()));

            hideKeyboard(getActivity());
            passEditor.clearFocus();

            if(isLogin()){
                passEditor.setNextFocusDownId(submitBtn.getId());
                passEditor.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }
            else{
                passEditor.setNextFocusDownId(confirmPass.getId());
                passEditor.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                confirmPass.setNextFocusDownId(submitBtn.getId());
            }
        }

        private void updateText() {
            submitBtn.setText(getSubmitText());

            titleView.setText(getTitle());
        }

        public Drawable getToggleDrawable() {
            return isLogin() ? signupIcon : loginIcon;
        }

        public void setItems(List<ServerInfo> servers) {
            this.serversAdapter.setItems(servers);
        }
    }

    public static class ServerInfoViewBuilder extends DefaultViewBuilder<ServerInfo> {
        Drawable iconDrawable;

        OnSelectItemListener<ServerInfo> onSelectItemListener;

        public ServerInfoViewBuilder() {
            super(R.layout.view_removable_item, android.R.id.text1);

        }

        public OnSelectItemListener<ServerInfo> getOnSelectItemListener() {
            return onSelectItemListener;
        }

        public void setOnSelectItemListener(OnSelectItemListener<ServerInfo> onSelectItemListener) {
            this.onSelectItemListener = onSelectItemListener;
        }

        @Override
        protected void setupView(View view, final Object data) {
            super.setupView(view, data);

            ImageButton btn = ViewUtils.getView(view, R.id.item_button);

            if(iconDrawable == null){
                iconDrawable = AppCompatResources.getDrawable(view.getContext(), R.drawable.ic_action_discard);
            }

            btn.setImageDrawable(iconDrawable);
            btn.setOnClickListener(new SelectOnClickListener(data));
        }

        private class SelectOnClickListener implements OnClickListener {
            private final Object data;

            public SelectOnClickListener(Object data) {
                this.data = data;
            }

            @Override
            public void onClick(View v) {
                Log.d(this.getClass().getSimpleName(), "click. Remove item: " + data);

                if(getOnSelectItemListener() != null){
                    getOnSelectItemListener().onItemSelection((ServerInfo) data);
                }
            }
        }
    }

	protected class SubmitOnEditorActionListener implements OnEditorActionListener {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if((actionId == EditorInfo.IME_ACTION_DONE ||  isEnterKey(event)) && isLastField(v)){
				submit();
				return true;
			}
			
			return false;
		}

		protected boolean isEnterKey(KeyEvent event) {
			return event != null && event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER;
		}
	}

	protected class SubmitOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			submit();
		}
	}

    public static class ServerInfoCollectionFilter extends DefaultCollectionFilter<ServerInfo> {
        @Override
        protected void filterCollection(Collection<? extends ServerInfo> items, CharSequence constraint, List<ServerInfo> filtered) {
            ServerInfo serverInfo = ServerInfo.fromText(constraint);
            if(serverInfo != null){
                filterServer(items, serverInfo, filtered);
            }
            else{
                super.filterCollection(items, constraint, filtered);
            }
            Log.d(ServerInfoCollectionFilter.class.getSimpleName(), "filterCollection. from " + items.size() + " items to " + filtered.size());
        }

        private void filterServer(Collection<? extends ServerInfo> items, ServerInfo serverInfo, List<ServerInfo> filtered) {
            serverInfo.setName(toLower(serverInfo.getName()));
            serverInfo.setAddress(toLower(serverInfo.getAddress()));

            for (ServerInfo item : items){
                if(match(item, serverInfo)){
                    filtered.add(item);
                }
            }
        }

        @Nullable
        private String toLower(String str) {
            return str == null ? null : str.toLowerCase();
        }

        @Override
        protected boolean match(ServerInfo item, String prefix) {
            return matchString(item.getName(), prefix)
                    || matchString(item.getAddress(), prefix);
        }

        protected boolean match(ServerInfo item, ServerInfo required) {
            return (required.getName() == null || matchString(item.getName(), required.getName()))
                    && (required.getAddress() == null || matchString(item.getAddress(), required.getAddress()));
        }

        boolean matchString(String str, String prefix){
            return str != null && str.toLowerCase().startsWith(prefix);
        }
    }
}
