package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.AboutFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;

public class AboutCommand extends ActivityStatelessCommand<Void> {
	private static final String ABOUT_DIALOG_TAG = "ABOUT_DIALOG_TAG";

    public AboutCommand(BaseActivity activity) {
        super(activity);
    }

    @Override
    public void execute(ResultListener<? super Void> cmdResultListener) {
        showAbout();
	}
	private void showAbout() {
		SimpleDialogFragment aboutDialog = new AboutFragment();
		aboutDialog.show(getActivity().getSupportFragmentManager(), ABOUT_DIALOG_TAG);
	}
}
