package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.ArrayList;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.CollectionsOperation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.CollectionsOperation.Operation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class MoveFromPhotoCollectionRequest extends CommonGenericRequest<PhotoCollection> {
	private long sourceCollectionId;
	private Collection<PhotoComponent> photoItems;
	private Collection<PhotoCollection> targets;

    public MoveFromPhotoCollectionRequest(ApiConnector apiConnector,
                                          PhotoCollection sourceCollection,
                                          Collection<? extends PhotoCollection> targetCollections,
                                          Collection<PhotoComponent> items,
                                          ResultListener<? super PhotoCollection> listener)
    {
        this(apiConnector, sourceCollection.getId(), targetCollections, items, listener);
    }

	public MoveFromPhotoCollectionRequest(ApiConnector apiConnector,
			long sourceCollectionId,
			Collection<? extends PhotoCollection> targetCollections,
			Collection<PhotoComponent> items,
			ResultListener<? super PhotoCollection> listener)
	{
		super(apiConnector, PhotoCollection.class, listener);
		this.sourceCollectionId = sourceCollectionId;
		this.photoItems = new ArrayList<>(items);
		this.targets = new ArrayList<>(targetCollections);
	}

	@Override
	public RequestHandler execute(){
		CollectionsOperation collectionsOperation = new CollectionsOperation(Operation.Move, photoItems, targets);
		return postJsonRequest(collectionsOperation);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().moveCollectionItemsUrl(sourceCollectionId);
	}
}