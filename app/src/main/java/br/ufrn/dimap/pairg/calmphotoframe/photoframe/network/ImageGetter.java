package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;

public interface ImageGetter {
    RequestHandler getImage(String url, ResultListener<? super Bitmap> listener);

    void invalidateImageCache(String url);
}
