package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class PhotoCollectionTextViewBuilder extends SimpleTextViewBuilder {
	private Context context;
	private final Long rootCollectionId;

	public PhotoCollectionTextViewBuilder(Context context, Long rootCollectionId) {
		this.context = context;
		this.rootCollectionId = rootCollectionId;
	}

	@Override
	protected String getText(Object data) {
		PhotoCollection coll = (PhotoCollection) data;
		if(coll != null && isRootCollection(coll))
		{
			return context.getText(R.string.default_rootCollectionName).toString();
		}
		return super.getText(data);
	}

	private boolean isRootCollection(PhotoCollection coll) {
		return isNullOrEmpty(coll.getName())
				&& coll.getId() == rootCollectionId;
	}

	protected boolean isNullOrEmpty(String txt) {
		return txt == null || txt.isEmpty();
	}
}