package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class CreateUserRequest extends CommonGenericRequest<User>{
	private LoginInfo userInfo;
	private Endpoint endpoint;
	
	public CreateUserRequest(ApiConnector apiConnector, LoginInfo userLoginInfo, ResultListener<? super User> listener) {
		this(apiConnector, userLoginInfo, null, listener);
	}

	public CreateUserRequest(ApiConnector apiConnector, LoginInfo loginInfo, Endpoint endpoint,
			ResultListener<? super User> listener) 
	{
		super(apiConnector, User.class, listener);
		this.userInfo = loginInfo;
		this.endpoint = endpoint;
	}

	@Override
	public RequestHandler execute() {
		return postJsonRequest(userInfo);
	}

	@Override
	protected String genUrl() {
		// If endpoint is null, it will use the current endpoint (set in apiBuilder)
		return getApiBuilder().usersUrl(endpoint);
	}

}
