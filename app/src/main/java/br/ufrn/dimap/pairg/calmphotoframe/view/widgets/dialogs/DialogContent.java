package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

public class DialogContent {
    CharSequence dialogTitle;
    CharSequence dialogMessage;
    private CharSequence okMessage, cancelMessage;
    int layoutId;

    public DialogContent() {
        this.dialogMessage = "";
        this.okMessage = null;
        this.cancelMessage = null;
        this.layoutId = 0;
    }

    public int getCustomLayoutId() {
        return layoutId;
    }

    public DialogContent setCustomLayoutId(int layoutId) {
        this.layoutId = layoutId;
        return this;
    }

    public CharSequence getDialogTitle() {
        return dialogTitle;
    }

    public CharSequence getDialogMessage() {
        return dialogMessage;
    }

    public CharSequence getOkMessage() {
        return okMessage;
    }

    public CharSequence getCancelMessage() {
        return cancelMessage;
    }

    public DialogContent setDialogTitle(CharSequence dialogTitle) {
        this.dialogTitle = dialogTitle;
        return this;
    }

    public DialogContent setDialogMessage(CharSequence dialogMessage) {
        this.dialogMessage = dialogMessage;
        return this;
    }

    public DialogContent setOkMessage(CharSequence okMessage) {
        this.okMessage = okMessage;
        return this;
    }

    public DialogContent setCancelMessage(CharSequence cancelMessage) {
        this.cancelMessage = cancelMessage;
        return this;
    }

    public DialogContent hideDialogMessage() {
        setDialogMessage(null);
        return this;
    }
    public DialogContent hideOkButton() {
        this.okMessage = null;
        return this;
    }

    public DialogContent hideCancelButton() {
        this.cancelMessage = null;
        return this;
    }

    public boolean hasTitle() {
        return getDialogTitle() != null;
    }
    public boolean hasMessage() {
        return getDialogMessage() != null;
    }
    public boolean hasOkButton(){
        return getOkMessage() != null;
    }
    public boolean hasCancelButton(){
        return getCancelMessage() != null;
    }
    public boolean hasCustomLayout(){
        return getCustomLayoutId() != 0;
    }
}