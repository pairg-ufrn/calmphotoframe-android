package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.events;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class EventsApi {
    private ApiConnector apiConnector;

    public EventsApi(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }

    public RequestHandler listRecent(ResultListener<? super List<Event>> listener) {
        return new ListRecentEventsRequest(apiConnector, listener).execute();
    }
}
