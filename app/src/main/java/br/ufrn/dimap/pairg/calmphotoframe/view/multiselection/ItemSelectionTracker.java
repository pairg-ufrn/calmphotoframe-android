package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/** FIXME: code  duplication with {@link ItemsSelectionManager}*/
public class ItemSelectionTracker implements SelectionTracker, OnItemClickListener, OnItemLongClickListener {

    private final Set<Long> selectedIds;
	private HashSet<MultiSelectionListener> listeners;
	private boolean selecting;
	
	private OnItemClickListener onItemClickListenerDelegate;
	private OnItemLongClickListener onItemLongClickListenerDelegate;

	private AdapterView<?> adapterView;

	private boolean notifyClickOnSelection;

	/** If set allows more than one item selected. */
	private boolean multiSelectionEnabled;

	/** If set to false, does not allow any selection. */
	private boolean selectionEnabled;

    /** If true, enable the use of single click to (de)select an item.*/
	private boolean selectOnClick;

    /** If true, does not select Views that are not enabled (getEnabled() == false)*/
	private boolean ignoreDisabledViews;
	
	public ItemSelectionTracker() {
		this(null);
	}
	public ItemSelectionTracker(AdapterView<? extends Adapter> adapterView) {
        selectionEnabled = true;
		multiSelectionEnabled = true;
		ignoreDisabledViews = true;
        notifyClickOnSelection = false;
        selecting = false;
        selectedIds = new HashSet<>();

		listeners = new HashSet<>();

		attachToView(adapterView);
	}

    private void attachToView(AdapterView<? extends Adapter> adapterView) {
		this.adapterView = adapterView;
		if(this.adapterView != null){
			this.adapterView.setOnItemClickListener(this);
			this.adapterView.setOnItemLongClickListener(this);
		}
	}
	/** Removes any bound to the view.*/
	private void detachFromView() {
		if(adapterView != null){
			this.deselectAll();
			adapterView.setOnItemClickListener(null);
			adapterView.setOnItemLongClickListener(null);
		}
	}

    @Override
    public void addSelectionListener(MultiSelectionListener listener) {
        this.listeners.add(listener);
    }

	@Override
	public void removeSelectionListener(MultiSelectionListener listener) {
	    this.listeners.remove(listener);
    }

	public AdapterView<? extends Adapter> getAdapterView(){
		return this.adapterView;
	}
	public void setAdapterView(AdapterView<? extends Adapter> adapterView){
		detachFromView();
		attachToView(adapterView);
	}

    public boolean isNotifyingClickOnSelection() { return notifyClickOnSelection;}
	public void setNotifyClickOnSelection(boolean notifyClickOnSelection) {
		this.notifyClickOnSelection = notifyClickOnSelection;
	}

    public boolean isSelectingOnClick()                 { return selectOnClick;}
	public void setSelectOnClick(boolean selectOnClick) {
		this.selectOnClick = selectOnClick;
	}

    public boolean isSelectionEnabled() {
        return selectionEnabled;
    }
    public void setSelectionEnabled(boolean selectionEnabled) {
        this.selectionEnabled = selectionEnabled;
    }

    public boolean isMultiSelectionEnabled() {
		return multiSelectionEnabled;
	}
	public void setMultiSelectionEnabled(boolean enabled) {
		if(enabled != this.multiSelectionEnabled){
			this.multiSelectionEnabled = enabled;
			if(!enabled){
				deselectAll();
			}
		}
	}

	public boolean getIgnoreDisabledViews() {
		return ignoreDisabledViews;
	}
	public void setIgnoreDisabledViews(boolean ignoreDisabledViews) {
		this.ignoreDisabledViews = ignoreDisabledViews;
	}

	@Deprecated
	/** Remove this. Because, getChildAt will only return the visible/created views.
     * I.e. the adapter position is not the same of viewGroup view index/position. */
    public View getView(int position) {
        return getAdapterView().getChildAt(position);
    }

    public void notifyChangedPosition(int position) {
        Adapter adapter = this.getAdapterView().getAdapter();
        if(adapter instanceof BaseAdapter){
            ((BaseAdapter)adapter).notifyDataSetChanged();
        }
    }

    public boolean isSelecting() {
        return selecting;
    }

    public boolean isChecked(long itemId) {
        return selectedIds.contains(itemId);
    }

	public OnItemClickListener getOnItemClickListener() {
        return onItemClickListenerDelegate;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListenerDelegate) {
        this.onItemClickListenerDelegate = onItemClickListenerDelegate;
    }

    public OnItemLongClickListener getOnItemLongClickListener() {
        return onItemLongClickListenerDelegate;
    }
    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListenerDelegate) {
        this.onItemLongClickListenerDelegate = onItemLongClickListenerDelegate;
    }

    public Collection<Long> getSelectedItems() {
        return this.selectedIds;
    }

    public void deselectAll() {
        boolean wasSelecting = hasSelection();
        deselectAllImpl(wasSelecting);
        verifyFinished(wasSelecting);
    }

    /* ********************************** Selection Impl *******************************************/

    protected void checkItem(View view, int position, long id) {
		if(!isSelectionEnabled() || isIgnored(view)){
			return;
		}
		
		boolean wasSelecting = hasSelection();
		
		//If is in single selection mode, clear the current selection
		if(!isMultiSelectionEnabled() && !selectedIds.contains(id)){
//			selectedIds.clear();
            deselectAllImpl(wasSelecting);
            wasSelecting = false;
		}

        toggleItemSelection(view, position, id, wasSelecting);
	}

    protected void deselectAllImpl(boolean wasSelecting) {
        if(adapterView != null){
            for(int i=0; i < adapterView.getChildCount(); ++i){
                long positionId = adapterView.getItemIdAtPosition(i);
                if(selectedIds.contains(positionId)){
                    View v = getView(i);
//					checkItem(v, i, positionId);
                    toggleItemSelection(v, i, positionId, wasSelecting);
                }
            }
        }
        selectedIds.clear();
    }

    private void toggleItemSelection(View view, int position, long id, boolean wasSelecting) {
        boolean checked = toggleSelection(id);
        view.setSelected(checked);
        onSelection(position, id, checked, wasSelecting);
    }

    protected boolean hasSelection() {
        return selectedIds.size() > 0;
    }

    /** If item with this {@code id} is selected, deselect it, otherwise select it.
	 * @return true if item is now selected 
	 * */
	private boolean toggleSelection(long id) {
		if(selectedIds.contains(id)){
			selectedIds.remove(id);
			return false;
		}
		else{
			selectedIds.add(id);
			return true;
		}
	}

    private void onSelection(int position, long id, boolean checked, boolean wasSelecting) {
        if(listeners.isEmpty()){
            if(selectedIds.size() == 1 && !wasSelecting){
                startSelection();
            }
            notifySelectionChanged(position, id, checked);
            verifyFinished(wasSelecting);
        }
    }

    private void verifyFinished(boolean wasSelecting) {
        if(selectedIds.size() == 0 && wasSelecting){
            finishSelection();
        }
    }

    protected void startSelection() {
		selecting = true;
		notifyStartSelection();
	}
	protected void finishSelection() {
		notifyFinishSelection();
		selecting = false;
	}

    private void notifySelectionChanged(int position, long id, boolean checked) {
        int selectionCount = selectedIds.size();

        for(MultiSelectionListener listener : this.listeners){
            listener.onItemSelectionChanged(selectionCount, position, id, checked);
        }
    }

    protected void notifyStartSelection() {
        for (MultiSelectionListener listener: listeners) {
            listener.onStartSelection();
        }
    }

    protected void notifyFinishSelection() {
        for (MultiSelectionListener listener: listeners) {
            listener.onEndSelection();
        }
    }

    /* *************************************** Click ***********************************************/

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(isIgnored(view)){
			return;
		}
		if(onItemClickListenerDelegate != null && (!isSelecting() || isNotifyingClickOnSelection())){
			onItemClickListenerDelegate.onItemClick(parent, view, position, id);
		}
		if(isSelecting() || isSelectingOnClick()){
			checkItem(view, position, id);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		boolean wasSelected = isSelecting();
		if(wasSelected){
			//If already was selected, call the delegate before any change happens
			delegateOnItemLongClick(parent, view, position, id);
		}
		checkItem(view, position, id);
		if(!wasSelected){
			//If not was selected, call the delegate after start selection
			delegateOnItemLongClick(parent, view, position, id);
		}
		
		return true;
	}

	protected void delegateOnItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		if(onItemLongClickListenerDelegate != null){
			onItemLongClickListenerDelegate.onItemLongClick(parent, view, position, id);
		}
	}

	protected boolean isIgnored(View view) {
		return getIgnoreDisabledViews() && !view.isEnabled();
	}
}