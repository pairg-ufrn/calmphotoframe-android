package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class LogoffRequest extends CommonGenericRequest<Endpoint>{
	private final Endpoint endpointToDisconnect;
	
	public LogoffRequest(ApiConnector apiConnector, Endpoint endpointToDisconnect,
			ResultListener<? super Endpoint> listener) {
		super(apiConnector, ApiEndpoint.class, listener);
		this.endpointToDisconnect = endpointToDisconnect;
	}

	@Override
	public RequestHandler execute() {
		if(endpointToDisconnect == null){
			Listeners.onResult(this.listener, null);
			return new FinishedRequestHandler();
		}

		return postRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().logoffUrl(endpointToDisconnect);
	}

	@Override
	protected Endpoint convertToResultType(Endpoint response) {
		return this.endpointToDisconnect;
	}
}
