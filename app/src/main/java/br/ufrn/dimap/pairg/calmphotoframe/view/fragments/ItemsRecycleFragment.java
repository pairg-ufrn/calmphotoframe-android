package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.ViewConfigurator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemSelectionOnClickAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiChoiceActionModeCompat;

public class ItemsRecycleFragment<Item> extends Fragment {

    ItemsRecycleAdapter<Item> adapter;
    RecyclerView recyclerView;
    List<ViewConfigurator> viewConfigurators;
    MultiChoiceActionModeCompat multiChoiceActionModeCompat;

    private ItemSelectionOnClickAdapter<Item> itemSelectionOnClickAdapter;

    public ItemsRecycleFragment() {
        adapter = new ItemsRecycleAdapter<>();
        viewConfigurators = new LinkedList<>();
        multiChoiceActionModeCompat = new MultiChoiceActionModeCompat(adapter.getSelectionManager());
        multiChoiceActionModeCompat.setCheckViews(false);

        itemSelectionOnClickAdapter = new ItemSelectionOnClickAdapter<>(
                                            adapter.items(),
                                            adapter.getSelectionManager().getItemClickDelegator());
    }

    public ItemsRecycleAdapter<Item> getAdapter() {
        return adapter;
    }

    public ItemsManager<Item> items() {
        return getAdapter().items();
    }

    public ItemsRecycleFragment<Item> setBinderBuilder(ItemsRecycleAdapter.BinderBuilder<Item> builder) {
        getAdapter().setBinderBuilder(builder);
        return this;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public MultiChoiceActionModeCompat getActionModeCompat() {
        return multiChoiceActionModeCompat;
    }

    @SuppressWarnings("unused")
    public List<ViewConfigurator> getViewConfigurators() {
        return this.viewConfigurators;
    }

    public void addViewConfigurator(ViewConfigurator viewConfigurator) {
        this.viewConfigurators.add(viewConfigurator);
        if (getView() != null) {
            viewConfigurator.configureView(getView());
        }
    }
    @SuppressWarnings("unused")
    public void removeViewConfigurator(ViewConfigurator viewConfigurator) {
        this.viewConfigurators.remove(viewConfigurator);
    }
    @SuppressWarnings("unused")
    public void removeViewConfigurator(int index) {
        this.viewConfigurators.remove(index);
    }

    public void addOnSelectItemListener(OnSelectItemListener<Item> listener){
        itemSelectionOnClickAdapter.addSelectItemListener(listener);
    }

    public void removeOnSelectItemListener(OnSelectItemListener<Item> listener){
        itemSelectionOnClickAdapter.removeSelectItemListener(listener);
    }

    public void scrollToItem(Item item) {
        if(recyclerView != null){
            int position = getAdapter().items().indexOf(item);
            if(position >= 0) {
                recyclerView.scrollToPosition(position);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //TODO: use interface instead of Actvity subclass
        if(getActivity() instanceof AppCompatActivity && getActionModeCompat().getActionModeHandler() == null){
            getActionModeCompat().setActionModeHandler((AppCompatActivity) getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        setupViews(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        configureView(view);
    }

    protected void configureView(View view){
        for(ViewConfigurator conf : viewConfigurators){
            conf.configureView(view);
        }
    }

    protected void setupViews(View rootView) {
        this.recyclerView = setupRecycleView(rootView);
    }

    protected RecyclerView setupRecycleView(View rootView) {
        RecyclerView recyclerView = ViewUtils.getView(rootView, getRecycleViewId());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        return recyclerView;
    }

    protected int getLayoutId() {
        return R.layout.layout_recycleview;
    }

    protected int getRecycleViewId() {
        return R.id.recycleview;
    }
}