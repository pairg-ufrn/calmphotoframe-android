package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.util.Log;

import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoWithPermission;

public class GetPhotoCommand extends BaseStatelessCommand<PhotoWithPermission>{
	private PhotoHolder photoHolder;
	private Integer photoId;

	public GetPhotoCommand(PhotoHolder photoHolder) {
		this(photoHolder, null);
	}
	public GetPhotoCommand(PhotoHolder photoHolder, Integer photoId) {
		this.photoHolder = photoHolder;
		this.photoId = photoId;
	}


	@Override
	public void execute(ResultListener<? super PhotoWithPermission> listener) {
		Integer currentPhoto = getPhotoId();

		Log.d(getClass().getSimpleName(), "getPhoto: " + currentPhoto);

		OnGetPhotoListener photoListener = new OnGetPhotoListener(photoHolder, currentPhoto, listener);
		doRequest(currentPhoto, photoListener);
	}
	
	private Integer getPhotoId() {
		if(photoId == null){
			return LocalPersistence.instance().getCurrentPhoto();
		}
		return photoId;
	}

	private void doRequest(Integer currentPhoto, OnGetPhotoListener photoListener) {
		if(currentPhoto != null){
			Log.i(getClass().getName(), "Get current photo = " + currentPhoto);
			ApiClient.instance().photos().get(currentPhoto, true, photoListener);
		}
		else{
			getPhotoFromContextOrCollection(photoListener);
		}
	}
	private void getPhotoFromContextOrCollection(OnGetPhotoListener photoListener) {
		Log.i(getClass().getName(), "No photo to Get");
		Long currentCollection = LocalPersistence.instance().getCurrentPhotoCollection();
		if(currentCollection != null){
			Log.i(getClass().getName(), "Try Get photo on current collection");
			new GetPhotoFromCollectionCommand(currentCollection, true).execute(photoListener);
		}
		else{
			Log.i(getClass().getName(), "Try Get photo from current context");
			new GetPhotoFromContextCommand().setIncludePermission(true).execute(photoListener);
		}
	}

	static class OnGetPhotoListener extends ResultListenerDelegator<Photo, PhotoWithPermission>{
		Long expectedPhotoId;
		private PhotoHolder photoHolder;
		
		public OnGetPhotoListener(PhotoHolder photoHolder, Integer expectedPhotoId
				, ResultListener<? super PhotoWithPermission> delegate)
		{
			this(photoHolder, expectedPhotoId == null ? null : expectedPhotoId.longValue(), delegate);
		}
		public OnGetPhotoListener(PhotoHolder photoHolder, Long expectedPhotoId
				, ResultListener<? super PhotoWithPermission> delegate)
		{
			super(delegate);
			
			this.photoHolder = photoHolder;
			this.expectedPhotoId = expectedPhotoId;
		}
		
		@Override
		public void onResult(Photo photo) {
			if(photo == null){//Não obteve nenhuma foto
				removeCurrentPhoto();
			}
			else{
				updateCurrentPhoto(photo);
			}
			super.onResult(photo);
		}
		protected void updateCurrentPhoto(Photo photo) {
			LocalPersistence.instance().storeCurrentPhoto(photo.getId());
			if(photoHolder != null){
				photoHolder.setCurrentPhoto(photo);
			}
		}
		protected void removeCurrentPhoto() {
			LocalPersistence.instance().removeCurrentPhoto();
			if(photoHolder != null){
				photoHolder.removeCurrentPhoto();
			}
		}
		@Override
		protected void handleError(Throwable error) {
			Log.w(getClass().getSimpleName(), "Failed to receive photo", error);
			//TODO: se erro for de 404 remover de LocalPersistence elemento não encontrado
			super.onFailure(error);
		}
		
		@Override
		public boolean isListening() {
			return true;
		}

        @Override
        protected PhotoWithPermission convertResult(Photo result) {
            return (PhotoWithPermission)result;
        }
    }

}
