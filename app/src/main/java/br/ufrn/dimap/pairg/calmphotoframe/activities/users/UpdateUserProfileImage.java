package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.SelectImagesCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class UpdateUserProfileImage extends BaseArgsCommand<User, User> {
    BaseActivity activity;

    public UpdateUserProfileImage(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void execute(@Nullable User user, ResultListener<? super User> listener) {
        ResultListener<? super User> compositeListener = new CompositeResultListener<>(
                new OnResultUpdateCurrentUser(), //update user if is equals to current
                listener
        );

        new SelectImagesCommand(activity)
                .setAllowMultiple(false)
                .execute(new OnResultUpdateImage(user, compositeListener));
    }

    private static class OnResultUpdateImage extends BasicResultListener<Collection<String>> {
        User user;
        ResultListener<? super User> listener;

        public OnResultUpdateImage(User user, ResultListener<? super User> listener) {
            this.user = user;
            this.listener = listener;
        }

        @Override
        public void onResult(Collection<String> result) {
            super.onResult(result);

            if (result != null && !result.isEmpty()) {
                String imageUri = result.iterator().next();
                try {
                    ImageContent content = ImageContent.builder().fromString(imageUri);

                    updateImage(content);
                } catch (IOException e) {
                    delegateError(e);
                }
            }
        }

        @Override
        public void onFailure(Throwable error) {
            delegateError(error);
        }

        public void updateImage(ImageContent content) {
            ApiClient.instance().users().updateProfileImage(content, user.getId(), new BasicResultListener<Object>() {
                @Override
                public void onResult(Object result) {
                    ApiClient.instance().users().get(user.getId(),listener);
                }

                @Override
                public void onFailure(Throwable error) {
                    delegateError(error);
                }
            });
        }

        public void delegateError(Throwable error) {
            Listeners.onFailure(listener, error);
        }
    }
}
