package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class PhotoMetadata implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3630477605564440285L;
	
	private String originalFilename;
	private String cameraMaker;
	private String cameraModel;
	//FIXME: modificar tipo de originalDate por dateTime
	private String originalDate;
	
	private Integer imageSize;
	private Integer imageWidth;
	private Integer imageHeight;
	private PhotoOrientation orientation;
	private PhotoLocation location;

	public String getOriginalFilename() {
		return this.originalFilename;
	}
	public String getCameraMaker() {
		return cameraMaker;
	}
	public String getCameraModel() {
		return cameraModel;
	}
	public Integer getImageSize() {
		return imageSize;
	}
	public Integer getImageWidth() {
		return imageWidth;
	}
	public Integer getImageHeight() {
		return imageHeight;
	}
	public PhotoLocation getLocation() {
		return this.location;
	}
	public PhotoOrientation getOrientation() {
		return orientation;
	}
	public String getOriginalDate() {
		return originalDate;
	}
	
	public void setOriginalFilename(String filename) {
		this.originalFilename = filename;
	}
	public void setCameraMaker(String cameraMaker) {
		this.cameraMaker = cameraMaker;
	}
	public void setCameraModel(String cameraModel) {
		this.cameraModel = cameraModel;
	}
	public void setOriginalDate(String originalDate) {
		this.originalDate = originalDate;
	}
	public void setImageSize(Integer imageSize) {
		this.imageSize = imageSize;
	}
	public void setImageWidth(Integer imageWidth) {
		this.imageWidth = imageWidth;
	}
	public void setImageHeight(Integer imageHeight) {
		this.imageHeight = imageHeight;
	}
	public void setLocation(PhotoLocation location) {
		this.location = location;
	}
	public void setOrientation(PhotoOrientation orientation) {
		this.orientation = orientation;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append(" originalFilename: ").append(getOriginalFilename());
		builder.append(" cameraMaker: ").append(getCameraMaker());
		builder.append(" cameraModel: ").append(getCameraModel());
		builder.append(" originalDate: ").append(getOriginalDate());
		builder.append(" imageSize: ").append(getImageSize());
		builder.append(" imageWidth: ").append(getImageWidth());
		builder.append(" imageHeight: ").append(getImageHeight());
		builder.append(" location: ").append(getLocation());
		builder.append(" orientation: ").append(getOrientation());
		builder.append("}");
		return builder.toString();
	}
}
