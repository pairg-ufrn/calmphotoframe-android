package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.ArrayList;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Permission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;

public class CollectionAndContextContainer{
	PhotoContext photoContext;
	PhotoCollection photoCollection;
	Collection<PhotoComponent> photoComponents;
	
	public PhotoContext getPhotoContext() {
		return photoContext;
	}
	public void setPhotoContext(PhotoContext photoContext) {
		this.photoContext = photoContext == null ? null : photoContext.clone();
	}
	public PhotoCollection getPhotoCollection() {
		return photoCollection;
	}
	public void setPhotoCollection(PhotoCollection photoCollection) {
		this.photoCollection = photoCollection == null ? null : photoCollection.clone();
		if(photoCollection instanceof ExpandedPhotoCollection){
			photoComponents = getExpandedCollection().getPhotoComponents();
		}
		else{
			photoComponents = null;
		}
	}

	public Long getContextId() {
		return getPhotoContext() == null ? null : getPhotoContext().getId();
	}
	public Long getCollectionId() {
		return getPhotoCollection() == null ? null : getPhotoCollection().getId();
	}

    public boolean isEditable(){
		if(getPhotoCollection() == null){
			return false;
		}

        Permission permission = getContextUserPermission();
        return  permission != null && permission.getLevel().ordinal() >= PermissionLevel.Write.ordinal();
    }
	public Permission getContextUserPermission() {
		PhotoContext ctx = getPhotoContext();
		return ctx instanceof PhotoContextWithPermission
					? ((PhotoContextWithPermission)ctx).getPermission()
					: null;
	}

	public PhotoComponent addPhotoComponent(PhotoCollection photoCollection) {
		addToExpanded(photoCollection);

		return addToList(new PhotoComponent(photoCollection));
	}

	public PhotoComponent addPhotoComponent(Photo photoCollection) {
		addToExpanded(photoCollection);

		return addToList(new PhotoComponent(photoCollection));
	}

	private PhotoComponent addToList(PhotoComponent component) {
		this.photoComponents.add(component);

		return  component;
	}

	public Collection<PhotoComponent> getPhotoComponents(){
		return photoComponents;
	}
	public void setPhotoComponents(Collection<PhotoComponent> photoComponents) {
		this.photoComponents = photoComponents == null ? null : new ArrayList<>(photoComponents);
		if(isExpandedCollection()){
			getExpandedCollection().clearCollections();
			getExpandedCollection().clearPhotos();
			for(PhotoComponent c : this.photoComponents){
				if(c.isPhoto()){
					addToExpanded(c.getPhoto());
				}
				else{
					addToExpanded(c.getPhotoCollection());
				}
			}
		}
	}

	public void set(CollectionAndContextContainer result) {
		setPhotoCollection(result.getPhotoCollection());
		setPhotoContext(result.getPhotoContext());
		if(!isExpandedCollection()){
			this.setPhotoComponents(result.getPhotoComponents());
		}
	}

    public void clear(){
        this.setPhotoCollection(null);
        this.setPhotoContext(null);
    }

	@Override
	public String toString() {
		return "CollectionActivityState [photoContext=" + photoContext + ", photoCollection=" + photoCollection + "]";
	}


	private void addToExpanded(PhotoCollection photoCollection) {
		if(isExpandedCollection()){
			getExpandedCollection().addPhotoCollection(photoCollection);
		}
	}
	private void addToExpanded(Photo photo) {
		if(isExpandedCollection()){
			getExpandedCollection().addPhoto(photo);
		}
	}
	
	private ExpandedPhotoCollection getExpandedCollection() {
		return (ExpandedPhotoCollection)photoCollection;
	}
	private boolean isExpandedCollection() {
		return photoCollection != null && photoCollection instanceof ExpandedPhotoCollection;
	}
}