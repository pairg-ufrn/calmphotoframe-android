package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

import android.view.View;
import android.view.ViewTreeObserver;

import br.ufrn.dimap.pairg.calmphotoframe.view.Size;

/**
 */
public abstract class OnChangeViewSizeListener<T extends View> implements ViewTreeObserver.OnGlobalLayoutListener {

    private T view;
    private int lastWidth, lastHeight;

    public OnChangeViewSizeListener(T view) {
        this.view = view;
        lastWidth = this.view.getWidth();
        lastHeight = this.view.getHeight();
    }

    @Override
    public void onGlobalLayout() {
        int width = view.getWidth();
        int height = view.getHeight();

        if (lastWidth != width || lastHeight != height) {
            onSizeChanged(getView(), new Size(lastWidth, lastHeight));
        }

        lastWidth = width;
        lastHeight = height;
    }

    public T getView() {
        return view;
    }

    public int getLastWidth() {
        return lastWidth;
    }

    public int getLastHeight() {
        return lastHeight;
    }

    protected abstract void onSizeChanged(T view, Size lastSize);
}
