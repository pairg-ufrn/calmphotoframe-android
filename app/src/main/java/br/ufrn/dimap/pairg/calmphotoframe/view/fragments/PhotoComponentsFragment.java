package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.CheckSelectedItemDecorator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.PhotoComponentsBinderBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu.FabMenu;

/**
 */
public class PhotoComponentsFragment extends ItemsRecycleFragment<PhotoComponent> {
    PhotoAlbumFabMenu fabMenu;
    EmptyStateView emptyStateView;
    boolean onSearchMode;

    public PhotoComponentsFragment() {
        fabMenu = new PhotoAlbumFabMenu();
        emptyStateView = new EmptyStateView();
        onSearchMode = false;
    }

    public FabMenu getFabMenu(){
        return fabMenu;
    }

    public void setPhotoCollection(ExpandedPhotoCollection photoCollection) {
       setCollection(photoCollection.getPhotoComponents());
    }
    public void setCollection(Collection<PhotoComponent> items) {
        getAdapter().items().set(items);
    }

    public boolean isOnSearchMode() {
        return onSearchMode;
    }

    public void setOnSearchMode(boolean onSearchMode) {
        this.onSearchMode = onSearchMode;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_photoalbum;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fabMenu.create(view, R.id.fab_menu, R.id.overlay);
        emptyStateView.setup(view, R.id.emptyView);
        emptyStateView.setState(R.drawable.mdi_image_album_big, R.string.album_empty);
        emptyStateView.getEmptyViewContainer().setBackgroundResource(R.color.light_background);

        getAdapter().registerAdapterDataObserver(new EmptyDataObserver());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setBinderBuilder(new PhotoComponentsBinderBuilder(getContext()));

        addViewConfigurator(new RecycleGridViewConfigurator(this));

        ItemsRecycleAdapter<PhotoComponent> adapter = getAdapter();
        adapter.addDecorator(new CheckSelectedItemDecorator<>(adapter.getSelectionManager(), adapter));
        adapter.setIdGenerator(new PhotoComponentIdGenerator());
    }

    public static class PhotoComponentIdGenerator implements ItemsRecycleAdapter.IdGenerator<PhotoComponent> {
        @Override
        public long getId(PhotoComponent item, ItemsManager<PhotoComponent> items) {
            long id = item.getId();
            if(item.isCollection()){
                //Shift PhotoCollection ids to avoid conflicts
                id += Long.MAX_VALUE/2;
            }
            return id;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    private class EmptyDataObserver extends RecyclerView.AdapterDataObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            boolean empty = getAdapter().items().isEmpty();

            //Empty view should be visible when items is empty and hidden otherwise
            if(emptyStateView.isVisible() == empty){
                return;
            }

            ViewUtils.setVisible(getRecyclerView(), !empty);
            emptyStateView.setVisible(empty);

            if(empty){
                /*Changes the emptyView text if is or not on the searchMode
                * NOTE: change only when data changes to avoid the text modify before a query occur*/
                int strRes = !isOnSearchMode()
                        ?  R.string.album_empty
                        : R.string.album_emptyPhotoSearch;
                emptyStateView.setText(strRes);
            }
        }
    }
}
