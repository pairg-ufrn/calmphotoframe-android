package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.EditText;

import java.util.Arrays;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.UserProfileController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UsersFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserProfileBinder;

import static android.R.attr.value;

public class SampleActivity extends NavActivity {

    UsersFragment usersFragment;

    public SampleActivity() {
        super();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_management);
        setupToolbar();

        usersFragment =
                (UsersFragment) getSupportFragmentManager().findFragmentById(R.id.usersFragment);

        BaseActionModeCallback actionMode = new BaseActionModeCallback(R.menu.contextualmenu_users);
        usersFragment.getActionModeCompat().setActionModeCallback(actionMode);
    }

    @Override
    protected void onResume() {
        super.onResume();

        usersFragment.getAdapter().items().set(Arrays.asList(
                new User(1L, "lorem").setName("Lorem Ipsum"),
                new User(2L, "john").setName("John Smith"),
                new User(3L, "rlp").setName("Raulph Laren")
        ));
    }

}
