package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.OnResultSaveConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;

public class LoginConnectionCommand extends ConnectionCommand<Endpoint>{
    @Override
    public void doConnection(ConnectionInfo connInfo, ResultListener<? super Endpoint> listener) {
        ResultListener<? super Endpoint> onConnectListener = new CompositeResultListener<>(
                new OnResultSaveConnectionInfo(),
                listener);

        ApiClient.instance().session().connect(connInfo, onConnectListener);
    }
}
