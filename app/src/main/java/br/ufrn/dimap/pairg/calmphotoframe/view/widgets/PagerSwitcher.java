package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/** ViewPager whose paging by swipe can be disabled */
public class PagerSwitcher extends ViewPager{
	private boolean pageSwipeEnabled;

	public PagerSwitcher(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public PagerSwitcher(Context context) {
		super(context);
		init();
	}

	private void init(){
		pageSwipeEnabled = true;
	}
	
	public boolean isPageSwipeEnabled() {
		return pageSwipeEnabled;
	}

	public void setPageSwipeEnabled(boolean pagingEnabled) {
		this.pageSwipeEnabled = pagingEnabled;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent evt) {
		//noinspection SimplifiableConditionalExpression
		return isPageSwipeEnabled() ? super.onTouchEvent(evt) : false;
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent evt) {
		//noinspection SimplifiableConditionalExpression
		return isPageSwipeEnabled() ? super.onInterceptTouchEvent(evt) : false;
	}
	
}
