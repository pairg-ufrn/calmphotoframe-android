package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common;

import java.util.Collection;
import java.util.Collections;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CancellableCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class CompositeCommand<Arg, Result> {
    public static <ArgT, ResultT> CompositeCommand<ArgT, ResultT> build(
            Collection<ArgT> args,
            CancellableCommand<ArgT, ResultT> command)
    {
        return new CompositeCommand<>(args, command);
    }

    private final int totalImages;
    private final Collection<Result> results;
    private AtomicInteger insertionCounter;
    private AtomicInteger failureCounter;
    private ResultListener<? super Collection<Result>> listener;
    private Queue<Arg> argsToProcess;
    private ConcurrentHashMap<Arg, RequestHandler> activeRequests;
    private int concurrencyLevel;
    private ResultListener<? super Result> progressListener;

    private CancellableCommand<Arg, Result> command;

    public CompositeCommand(Collection<Arg> args, CancellableCommand<Arg, Result> command) {
        if(args == null){
            throw new NullPointerException("Arguments for RequestsExecutor should not be null!");
        }

        this.command = command;
        failureCounter = new AtomicInteger(0);
        argsToProcess = new ConcurrentLinkedQueue<>(args);
        activeRequests = new ConcurrentHashMap<>();
        this.totalImages = args.size();
        insertionCounter = new AtomicInteger(0);
        results = new ConcurrentLinkedQueue<>();
    }

    /* ********************************************************************************************/

    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public CompositeCommand<Arg, Result> setConcurrencyLevel(int concurrencyLevel) {
        this.concurrencyLevel = (concurrencyLevel > 1 ? concurrencyLevel : 1);
        return this;
    }

    public CompositeCommand<Arg, Result> onProgress(ResultListener<? super Result> progressListener) {
        this.progressListener = progressListener;
        return this;
    }

    public ResultListener<? super Collection<Result>> getListener() {
        return listener;
    }

    public CompositeCommand<Arg, Result> setListener(ResultListener<? super Collection<Result>> listener) {
        this.listener = listener;
        return this;
    }

    public int getSuccessCount(){
        return this.insertionCounter.get();
    }

    public int getFailureCount(){
        return this.failureCounter.get();
    }

    public int getArgumentsCount(){
        return totalImages;
    }

    public int getProgressCount() {
        return insertionCounter.get() + failureCounter.get();
    }

    /* ********************************************************************************************/

    public void execute() {
		onStart();
        doNextRequests();
    }

    public void forceFinish(Throwable t) {
        onRequestError(t);
        if(!hasFinished()){//Force finish
            onFinishAll();
        }
    }

    protected void doNextRequests() {
        while(activeRequests.size() < getConcurrencyLevel() && !argsToProcess.isEmpty()){
            doNext(argsToProcess.poll());
        }
    }

    protected void doNext(Arg arg) {
        try{
            RequestHandler handler = executeRequest(arg, new OnResultListener(arg));

            if(handler != null && !handler.isFinished() && !handler.isCancelled()) {

                activeRequests.put(arg, handler);
            }
        }
        catch (Throwable t){
            forceFinish(t);
        }
    }

    public void execute(ResultListener<? super Collection<Result>> listener) {
        this.setListener(listener);

        execute();
    }

    protected RequestHandler executeRequest(Arg arg, ResultListener<? super Result> listener){
        return command.execute(arg, listener);
    }

    /* ****************************************** Lifecycle ***************************************/

    protected void onStart() {
        Listeners.onStart(this.getListener());
        Listeners.onStart(progressListener);
    }

    protected void onRequestResult(Result result) {
        insertionCounter.incrementAndGet();
        results.add(result);

        handleResult(result);
        Listeners.onResult(progressListener, result);
        checkFinish();
    }

    protected void onRequestError(Throwable e) {
        failureCounter.incrementAndGet();
        handleError(e);

        Listeners.onFailure(progressListener, e);
        checkFinish();
    }

    protected void onRequestFinish(Arg imgPath) {
        activeRequests.remove(imgPath);
        doNextRequests();
    }

    private void onFinishAll() {
        handleOnFinish();

        Listeners.onResult(getListener(), Collections.unmodifiableCollection(results));
    }


    @SuppressWarnings("UnusedParameters")
    protected void handleResult(Result result){}
    @SuppressWarnings("UnusedParameters")
    protected void handleError(Throwable e){}
    protected void handleOnFinish(){}

    private void checkFinish() {
        if(hasFinished()){
            onFinishAll();
        }
    }

    private boolean hasFinished() {
        return getProgressCount() == totalImages;
    }

    /* *******************************************************************************************/
    class OnResultListener extends BasicResultListener<Result> {
        Arg arg;

        public OnResultListener(Arg arg) {
            this.arg = arg;
        }

        @Override
        protected void handleResult(Result result) {
            onRequestResult(result);
        }

        @Override
        protected void handleError(Throwable error) {
            onRequestError(error);
        }

        @Override
        protected void onFinish() {
            onRequestFinish(arg);
        }
    }
}
