package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.ViewConfigurator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer.CheckMode;

public class CheckableLayoutViewBinder<T> extends InflaterViewBinder<T>{
	private BinderViewHolder<T> viewBinderDelegate;
	private int layoutContainerId;
	private CheckMode checkMode;

    private CheckableContainer checkableContainer;
    private View delegateView;

    private ViewConfigurator configurator;

	public CheckableLayoutViewBinder(BinderViewHolder<T> viewBinder) {
		this(viewBinder, CheckMode.recursive);
	}

	public CheckableLayoutViewBinder(BinderViewHolder<T> viewBinder, CheckMode checkMode) {
		super(R.layout.checkable_layout);
        this.layoutContainerId = R.id.container;
		this.viewBinderDelegate = viewBinder;
		this.checkMode = checkMode;
	}

    public int getLayoutContainerId() {
        return layoutContainerId;
    }
    public void setLayoutContainerId(int layoutContainerId) {
        this.layoutContainerId = layoutContainerId;
    }

    public ViewConfigurator getConfigurator() {
        return configurator;
    }

    public CheckableLayoutViewBinder setConfigurator(ViewConfigurator configurator) {
        this.configurator = configurator;
        return this;
    }

    public CheckMode getCheckMode() {
		return checkMode;
	}

	public void setCheckMode(CheckMode checkMode) {
		this.checkMode = checkMode;
        if(checkableContainer != null){
            checkableContainer.setCheckMode(checkMode);
        }
	}

    @Override
    public View buildView(ViewGroup viewParent, int viewType) {
        View view = super.buildView(viewParent, viewType);

        ViewGroup childContainer = ViewUtils.getView(view, layoutContainerId);

        if(childContainer instanceof CheckableContainer){
            checkableContainer = (CheckableContainer)childContainer;
            checkableContainer.setCheckMode(this.checkMode);
        }
        else{
            checkableContainer = null;
        }

        if(viewBinderDelegate != null) {
            delegateView = viewBinderDelegate.buildView(childContainer, viewType);
            if(delegateView != null && delegateView.getParent() == null){
                childContainer.addView(delegateView);
            }
        }

        return view;
    }

    @Override
    public void setup(View view) {
        if(getConfigurator() != null){
            getConfigurator().configureView(view);
        }
        if(viewBinderDelegate != null) {
            viewBinderDelegate.setup(this.delegateView);
        }

    }

    @Override
    public void bind(T entity) {
        if(viewBinderDelegate != null){
            viewBinderDelegate.bind(entity);
        }
    }

}
