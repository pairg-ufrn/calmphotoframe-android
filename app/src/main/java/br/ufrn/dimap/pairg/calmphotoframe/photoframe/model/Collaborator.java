package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public class Collaborator {
	private User user;
	private Permission permission;

	public Collaborator() {
		this(null, null);
	}
	public Collaborator(User user, Permission p) {
		super();
		this.user = user;
		this.permission = p != null 
								? new Permission(p.getUserId(), p.getContextId(), p.getLevel()) 
								: new Permission();
	}
	
	public Permission getPermission(){
		return permission;
	}
	
	public Long getContextId() {
		return permission.getContextId();
	}
	public void setContextId(Long contextId) {
		permission.setContextId(contextId);
	}
	public Long getUserId() {
		return getUser() == null ? null : getUser().getId();
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
		this.permission.setUserId(user == null ? null : user.getId());
	}
	public PermissionLevel getPermissionLevel() {
		return permission.getLevel();
	}
	public void setPermissionLevel(PermissionLevel permissionLevel) {
		permission.setLevel(permissionLevel);
	}
}
