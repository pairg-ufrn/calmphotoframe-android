package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import android.graphics.Bitmap;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.BitmapHttpResponseHandler;
import cz.msebera.android.httpclient.Header;

public class ImageResponseHandler extends BitmapHttpResponseHandler {
	private final ResultListener<? super Bitmap> imageListener;

	public ImageResponseHandler(ResultListener<? super Bitmap> imageListener) 
	{
		super();
		this.imageListener = imageListener;
	}
	public ImageResponseHandler(int[] wantedMaxImgSize
			, ResultListener<? super Bitmap> imageListener) 
	{
		this(wantedMaxImgSize, null, imageListener);
	}
	public ImageResponseHandler(int[] wantedMaxImgSize, int[] maxImgSize
			, ResultListener<? super Bitmap> imageListener) 
	{
		super(wantedMaxImgSize, maxImgSize);
		this.imageListener = imageListener;
	}

	@Override
	public void onSuccess(int arg0, Header[] arg1, Bitmap bitmap) {
		Listeners.onResult(imageListener, bitmap);
	}

	@Override
	public void onFailure(int statusCode, Header[] arg1, byte[] content, Throwable err) {
		if(statusCode < 200 && statusCode >= 300){
			err = new ApiException("", err, statusCode);
		}
		Listeners.onFailure(imageListener, err);
	}
}