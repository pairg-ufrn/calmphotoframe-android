package br.ufrn.dimap.pairg.calmphotoframe.activities.photos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo.PhotoViewerFragment;

public class PhotoGalleryController {

	private BaseActivity activity;
	private PhotoGallery gallery;
	
	private Long photoCollectionId;
	private String queryFilter;

	public PhotoGalleryController(BaseActivity activity) {
		this(activity, null);
	}
	public PhotoGalleryController(BaseActivity activity, Long photoCollectionId) {
		this(activity, photoCollectionId, null);
	}
	public PhotoGalleryController(BaseActivity activity, Long photoCollectionId, String queryFilter) {
		this.activity = activity;
		gallery = new PhotoGallery();
		
		this.photoCollectionId = photoCollectionId;
		this.queryFilter = queryFilter;
	}
	
	public void setup(){
		gallery.setupView(getActivity());
		getPhotos();
	}

	public void refreshView() {
		getPhotos();
	}
	
	public Long getPhotoCollectionId() {
		return photoCollectionId;
	}
	public void setPhotoCollectionId(Long photoCollectionId) {
		this.photoCollectionId = photoCollectionId;
	}
	
	public String getQueryFilter() {
		return queryFilter;
	}
	public void setQueryFilter(String queryFilter) {
		this.queryFilter = queryFilter;
	}
	protected BaseActivity getActivity(){
		return activity;
	}
	
	private void getPhotos() {
		ApiClient.instance()
		.photos().listFromCollection(photoCollectionId, true, queryFilter, new BasicResultListener<List<Photo>>(){
			@Override
			public void onResult(List<Photo> result) {
				gallery.setPhotos(result);
			}
		});
	}
	

	static class PhotoGallery implements OnMenuItemClickListener{
		PhotoGalleryPagerAdapter pagerAdapter;
		List<Photo> photos;
		ViewPager pager;
		
		/** If true, keep all photos displaying on same layer*/ 
		boolean samePhotoLayer;
		
		public PhotoGallery() {
			photos = new ArrayList<>();
			pager = null;
			samePhotoLayer = true;
		}
		
		public void setupView(FragmentActivity activity) {
			pagerAdapter = new PhotoGalleryPagerAdapter(activity.getSupportFragmentManager());
			
			pager = ViewUtils.getView(activity, R.id.gen_page_view);
			
			pager.setAdapter(pagerAdapter);
			
			Toolbar toolbar = ViewUtils.getView(activity, R.id.toolbar);
			toolbar.inflateMenu(R.menu.menu_photogallery);
			toolbar.setOnMenuItemClickListener(this);
		}

		public void setPhotos(List<Photo> photos){
			this.photos.clear();
			this.photos.addAll(photos);
			
			pagerAdapter.setPhotos(this.photos);
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			if(item.getItemId() == R.id.action_flipView){
				flipView();
			}
			return false;
		}

		private void flipView() {
			Log.d(getClass().getSimpleName(), "Flip view");
			if(pagerAdapter.getCount() > 0){
				if(!samePhotoLayer){
					PhotoViewerFragment frag = (PhotoViewerFragment)pagerAdapter.getFragment(pager.getCurrentItem());
					frag.flipPage();
				}
				else{
					pagerAdapter.flipAll();
				}
			}
		}
	}
	static class PhotoGalleryPagerAdapter extends FragmentStatePagerAdapter {
		List<Photo> photos;
		final Map<Integer, Fragment> posToFragment;
		
		Integer currentPhotoLayer;
		
		public PhotoGalleryPagerAdapter(FragmentManager fm) {
			super(fm);
			photos = null;
			posToFragment = new HashMap<>();
			
			currentPhotoLayer = null;
		}

		public void flipAll() {
			if(currentPhotoLayer == null){
				currentPhotoLayer = 0;
			}
			currentPhotoLayer = (currentPhotoLayer + 1) % 3;
			
			for(Fragment f : posToFragment.values()){
				PhotoViewerFragment frag = (PhotoViewerFragment)f;
				frag.setSelectedPage(currentPhotoLayer, true);
			}
		}

		public Fragment getFragment(int pos) {
			if(pos < 0 || pos >= getCount()){
				throw new IndexOutOfBoundsException();
			}
			
			return getItem(pos);
		}

		public List<Photo> getPhotos() {
			return photos;
		}
		public void setPhotos(List<Photo> photos) {
			this.photos = photos;
			this.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return photos == null ? 0 : photos.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			super.destroyItem(container, position, object);
			posToFragment.remove(position);
		}
		
		@Override
		public Object instantiateItem(ViewGroup arg0, int arg1) {
			PhotoViewerFragment frag = (PhotoViewerFragment)super.instantiateItem(arg0, arg1);
			
			if(currentPhotoLayer != null){
				frag.setCurrentPage(currentPhotoLayer);
			}
			
			return frag;
		}
		
		@Override
		public Fragment getItem(int page) {
			Fragment frag = posToFragment.get(page);
			if(frag == null){
				frag = createFragment(getPhotos().get(page));
				posToFragment.put(page, frag);
			}
			return frag;
		}

		private Fragment createFragment(Photo photo) {
			final PhotoViewerFragment fragment = new PhotoViewerFragment();
			
			fragment.setPageSwipeEnabled(false);
			fragment.setPhotoDescriptor(photo);
			
			ApiClient.instance().photos().getImageBitmap(photo, new BasicResultListener<Bitmap>(){
				@Override
				public void onResult(Bitmap result) {
					fragment.setPhotoImage(result);
				}
			});
			
			return fragment;
		}
	}
}
