package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.support.annotation.Nullable;

public interface UserNotifier {
    NotificationBuilder notifyUser();
    NotificationBuilder notifyUser(CharSequence text);

    @Nullable CharSequence getTextResouce(int msgId);
}
