package br.ufrn.dimap.pairg.calmphotoframe.controller.model;

/**
 */
public class Image {
    /**
	 * Id specified by Android to gallery images
	 */
    private long imageId;
    private String url;
    private String mimeType;

	public Image() {
        this("");
    }

	public Image(String url) {
        this.url = url;
        imageId = 0;
    }

	public long getImageId() {
        return imageId;
    }

	public void setImageId(long imageId) {
        this.imageId = imageId;
    }

	public String getUrl() {
        return url;
    }

	public void setUrl(String url) {
        this.url = url;
    }

	public String getMimeType() {
        return mimeType;
    }

	public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
