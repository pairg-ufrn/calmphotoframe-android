package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;


/** A RequestHandler to Requests already finished. E.g.: got from cache*/
public class FinishedRequestHandler implements RequestHandler {
	@Override
	public boolean isFinished() {
		return true;
	}
	@Override
	public boolean isCancelled() {
		return false;
	}
	@Override
	public boolean cancel() {
		return false;
	}
}
