package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Checkable;

import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActivityActionModeHandler;

public class MultiChoiceActionModeCompat implements MultiSelectionListener
{

	private ActionMode actionMode;
	private ActionModeHandler actionModeHandler;
	private ActionMode.Callback actionModeCallback;
    private SelectionTracker selectionTracker;
	private MultiSelectionListener selectionListenerDelegate;
	private boolean notifyDataSetChanged;
	private boolean checkViews;
	public MultiChoiceActionModeCompat() 
	{
		this(null, null);
	}
	public MultiChoiceActionModeCompat(SelectionTracker selectionTracker)
	{
		this(selectionTracker, null);
	}
	public MultiChoiceActionModeCompat(SelectionTracker selectionTracker, ActionMode.Callback actionModeCallback)
	{
        this.selectionTracker = selectionTracker;
		this.actionModeHandler = null;
		this.actionModeCallback = actionModeCallback;
		this.notifyDataSetChanged = true;
		this.checkViews = true;
		selectionTracker.addSelectionListener(this);
	}

    public ActionModeHandler getActionModeHandler() {
        return actionModeHandler;
    }
	public ActionMode.Callback    getActionModeCallback()       { return actionModeCallback;}
	public boolean 				  getCheckViews() 			    { return checkViews;}
	public boolean                isNotifyingDataSetChanged()   { return notifyDataSetChanged;}
	public MultiSelectionListener getSelectionListener()      { return this.selectionListenerDelegate;}

    public MultiChoiceActionModeCompat setActionModeHandler(AppCompatActivity activity) {
        return setActionModeHandler(new ActivityActionModeHandler(activity));
    }
    public MultiChoiceActionModeCompat setActionModeHandler(ActionModeHandler actionModeHandler) {
        this.actionModeHandler = actionModeHandler;
        return this;
    }
    public void setActionModeCallback(ActionMode.Callback actionModeCallback) {
		this.actionModeCallback = actionModeCallback;
	}
	public void setCheckViews(boolean checkViews) {
		this.checkViews = checkViews;
	}
	public void setNotifyDataSetChanged(boolean notifyDataSetChanged) {
		this.notifyDataSetChanged = notifyDataSetChanged;
	}
	public void setSelectionListener(MultiSelectionListener listener) {
		this.selectionListenerDelegate= listener;
	}

	public void checkView(View view, long id) {
		checkView(view, isChecked(id));
	}

    private boolean isChecked(long id) {
        return this.selectionTracker.isChecked(id);
    }

    public static void checkView(View view, boolean checked) {
		if(view instanceof Checkable){
			Checkable checkable = (Checkable)view;
			checkable.setChecked(checked);
			//TODO: check if is really necessary invalidate the view
			view.invalidate();
		}
	}
	/* ***************************************** MultiSelectionListenr ***************************************/
	@Override
	public void onStartSelection() {
		if(actionModeCallback != null){
			if(getActionModeHandler() == null){
				throw new IllegalStateException("Trying to start action mode, but actionModeHandler was not set");
			}
			actionMode = getActionModeHandler().startActionMode(new ActionModeCallbackDelegator(selectionTracker, actionModeCallback));
		}
		if(selectionListenerDelegate != null){
			selectionListenerDelegate.onStartSelection();
		}
	}

	@Override
	public void onItemSelectionChanged(int selectionCount, int position, long id, boolean checked) {
        View view = selectionTracker.getView(position);
        if(getCheckViews() && view instanceof Checkable){
            checkView(view, checked);
        }

		if(isNotifyingDataSetChanged()){
            selectionTracker.notifyChangedPosition(position);
		}
		if(selectionListenerDelegate != null){
			selectionListenerDelegate.onItemSelectionChanged(selectionCount, position, id, checked);
		}
	}

    @Override
	public void onEndSelection() {
		if(selectionListenerDelegate != null){
			selectionListenerDelegate.onEndSelection();
		}

		if(actionMode != null){
			actionMode.finish();
			actionMode = null;
		}


	}

	/* ********************************** ActionModeCallback ************************************/

    public class ActionModeCallbackDelegator implements ActionMode.Callback{
        ActionMode.Callback delegate;
        SelectionTracker selectionTracker;

        public ActionModeCallbackDelegator(SelectionTracker selectionTracker, ActionMode.Callback delegate) {
            this.delegate = delegate;
            this.selectionTracker = selectionTracker;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
            return delegate.onActionItemClicked(mode, menuItem);
        }
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menuItem) {
            return delegate.onCreateActionMode(mode, menuItem);
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            delegate.onDestroyActionMode(mode);
            selectionTracker.deselectAll();
        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menuItem) {
            return delegate.onPrepareActionMode(mode, menuItem);
        }
    }
}