package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class ProgressBarDialogFragment extends DialogFragment{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullscreenDialogFragmentStyle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Window w = getDialog().getWindow();
		w.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		return inflater.inflate(R.layout.fragment_fullprogressbar, container, false);
	}
}
