package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;

/**
 */
public class ImageLoaderScrollableDecorator<T> implements ImageLoader<T> {
    public interface ScrollObserver{
        void onStartScroll();
        void onStoppingScroll();
        void onFinishScroll();
    }
    public interface ScrollMonitor{
        boolean isScrolling();
        void addScrollObserver(ScrollObserver observer);
        void removeScrollObserver(ScrollObserver observer);
    }

    ImageLoader<T> delegateImageLoader;
    ScrollMonitor scrollMonitor;
    Queue<QueueItem<T>> loadQueue;

    public ImageLoaderScrollableDecorator(ImageLoader<T> delegateImageLoader, ScrollMonitor scrollMonitor) {
        this.delegateImageLoader = delegateImageLoader;
        this.scrollMonitor = scrollMonitor;
        loadQueue = new ConcurrentLinkedQueue<>();
        scrollMonitor.addScrollObserver(new InnerScrollObserver());
    }

    @Override
    public RequestHandler loadImage(T entity, ResultListener<? super Drawable> listener) {
        RequestHandler handler = enqueue(entity, listener);

        loadQueued();

        return handler;
    }

    @Override
    public RequestHandler loadImage(T entity, ImageView target) {
        RequestHandler handler = enqueue(entity, target);

        loadQueued();

        return handler;
    }

    @NonNull
    protected RequestHandler enqueue(T entity, ResultListener<? super Drawable> listener) {
        return enqueue(new QueueItem<>(entity, listener));
    }

    @NonNull
    protected RequestHandler enqueue(T entity, ImageView view) {
        return enqueue(new QueueItem<>(entity, view));
    }

    @NonNull
    protected RequestHandler enqueue(QueueItem<T> queueItem) {
        this.loadQueue.add(queueItem);

        return queueItem.reqHandler;
    }

    private void loadQueued() {
        loadQueued(false);
    }

    private void loadQueued(boolean force) {
        while(!loadQueue.isEmpty() && canLoad(force)){
            QueueItem<T> item = loadQueue.poll();

//            Log.d(getClass().getSimpleName(), "Handle: " + item);
            if(item != null && item.isCancelled()){
                Log.d(getClass().getSimpleName(), "Cancelled: " + item);
            }
            if(item != null && !item.isCancelled()){
                item.doRequest(this.delegateImageLoader);
            }
        }
    }

    private boolean canLoad(boolean force) {
        return !scrollMonitor.isScrolling() || force;
    }

    protected static class QueueItem<Item>{
        public final Item item;
        public final DelegateRequestHandler reqHandler;
        public ResultListener<? super Drawable> listener;
        ImageView imgView;

        public QueueItem(Item item, ResultListener<? super Drawable> listener) {
            this(item);
            this.listener = listener;
        }
        public QueueItem(Item item, ImageView view) {
            this(item);
            this.imgView = view;
        }
        public QueueItem(Item item) {
            this.item = item;
            reqHandler = new DelegateRequestHandler();
            this.imgView = null;
            listener = null;
        }

        public boolean isCancelled(){
            return reqHandler.isCancelled();
        }
        public void doRequest(ImageLoader<Item> loader){
            RequestHandler handler = (imgView == null)
                    ? loader.loadImage(item, listener)
                    : loader.loadImage(item, imgView);

            reqHandler.setDelegate(handler);
        }

        @Override
        public String toString() {
            return String.format("{ item: %s; cancelled: %b }", item, isCancelled());
        }
    }

    public static class DelegateRequestHandler implements RequestHandler {
        boolean cancelled = false;

        RequestHandler delegate;

        public RequestHandler getDelegate() {
            return delegate;
        }

        public void setDelegate(RequestHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean isFinished() {
            return getDelegate() != null && getDelegate().isFinished();
        }

        @Override
        public boolean isCancelled() {
            return cancelled || (getDelegate() != null && getDelegate().isCancelled());
        }

        @Override
        public boolean cancel() {
            this.cancelled = true;
            //noinspection SimplifiableIfStatement
            if(getDelegate() != null){
                return getDelegate().cancel();
            }
            return true;
        }
    }

    private class InnerScrollObserver implements ScrollObserver {

        @Override
        public void onStartScroll(){
        }

        @Override
        public void onStoppingScroll() {
            loadQueued(true);
        }

        @Override
        public void onFinishScroll() {
            loadQueued();
        }
    }

}
