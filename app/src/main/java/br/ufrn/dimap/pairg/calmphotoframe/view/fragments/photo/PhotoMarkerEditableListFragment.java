package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoMarksNameAdapter;

import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;
import com.mobeta.android.dslv.DragSortListView.RemoveListener;

public class PhotoMarkerEditableListFragment extends Fragment implements DropListener, RemoveListener{
    private PhotoMarksNameAdapter markersAdapter;
    private DragSortListView draggableListView;
    private DragSortController mController;

    private int dragStartMode;
    private int removeMode;
    private boolean removeEnabled;
    private boolean sortEnabled;
    private boolean dragEnabled;

    public PhotoMarkerEditableListFragment() {
    	markersAdapter = new PhotoMarksNameAdapter();
    	
    	dragStartMode = DragSortController.ON_DRAG;
        removeEnabled = true;
        removeMode = DragSortController.FLING_REMOVE;
        sortEnabled = true;
        dragEnabled = true;
	}

    public DragSortController getController() {
        return mController;
    }
	public PhotoMarksNameAdapter getPhotoMarkerAdapter() {
		return this.markersAdapter;
	}

	/* ***********************************************************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        draggableListView = (DragSortListView) inflater.inflate(R.layout.fragment_editablemarkerlist
        		, container, false);
        setupView();
        
        draggableListView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				markersAdapter.onParentChangeFocus(draggableListView, hasFocus);
			}
		});
        
        return draggableListView;
    }

	@SuppressLint("ClickableViewAccessibility")
	private void setupView() {
		mController = buildController(draggableListView);
        draggableListView.setFloatViewManager(mController);
        draggableListView.setOnTouchListener(mController);
        draggableListView.setDragEnabled(dragEnabled);

        draggableListView.setDropListener(this);
        draggableListView.setRemoveListener(this);

        setupAdapter(getActivity());
	}
   
    private void setupAdapter(Activity activity) {
    	markersAdapter.setContext(activity);
        draggableListView.setAdapter(markersAdapter);
    }
    /**
     * Called in onCreateView. Override this to provide a custom
     * DragSortController.
     */
    protected DragSortController buildController(DragSortListView dslv) {
        DragSortController controller = new DragSortController(dslv);
        controller.setDragHandleId(R.id.photoMarkerItem_dragHandle);
        controller.setRemoveEnabled(removeEnabled);
        controller.setSortEnabled(sortEnabled);
        controller.setDragInitMode(dragStartMode);
        controller.setRemoveMode(removeMode);
        return controller;
    }

	/* *************************************** DropListener ***************************************/
	@Override
	public void drop(int from, int to) {
		markersAdapter.moveItem(from, to);
	}
	/* *************************************** RemoveListener ***************************************/
	@Override
	public void remove(int which) {
        markersAdapter.removeItem(which);
	}
}
