package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session.LoginRequest.TokenResponse;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class LoginRequest extends BaseGenericRequest<TokenResponse, ApiEndpoint>{
	public static class TokenResponse{
		public String token;
	}
	
	private ConnectionInfo connectionInfo;
	
	public LoginRequest(ApiConnector apiConnector, ConnectionInfo connectionInfo
			, ResultListener<? super ApiEndpoint> listener)
	{
		super(apiConnector, TokenResponse.class, listener);
		this.connectionInfo = connectionInfo;
	}

	@Override
	public RequestHandler execute() {
		return postJsonRequest(connectionInfo.getLoginInfo());
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().loginUrl(connectionInfo.getEndpoint());
	}

	@Override
	protected ApiEndpoint convertToResultType(TokenResponse response) {
		ApiEndpoint apiEndpoint = new ApiEndpoint(connectionInfo.getEndpoint().clone());
		apiEndpoint.setConnectionToken(response.token);
		
		return apiEndpoint;
	}
}
