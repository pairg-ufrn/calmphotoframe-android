package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.AutoFitGridLayoutManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.ItemOffsetDecoration;
import br.ufrn.dimap.pairg.calmphotoframe.view.ViewConfigurator;

/**
 */
public class RecycleGridViewConfigurator implements ViewConfigurator {
    ItemsRecycleFragment<?> itemsFragment;
    Integer columnWidth;
    int itemsSpacing;

    AutoFitGridLayoutManager layoutManager;

    public RecycleGridViewConfigurator(ItemsRecycleFragment<?> itemsFragment) {
        this.itemsFragment = itemsFragment;
        itemsSpacing = 1;
        layoutManager = new AutoFitGridLayoutManager(itemsFragment.getContext());
    }

    public AutoFitGridLayoutManager getLayoutManager() {
        return layoutManager;
    }

    public Integer getColumnWidth() {
        return columnWidth;
    }

    protected int getColumnWidth(Context context) {
        int defaultWidth = (int) getContext().getResources().getDimension(R.dimen.thumbnail_size);
        return getColumnWidth() != null ? getColumnWidth() : defaultWidth;
    }

    public void setColumnWidth(Integer columnWidth) {
        this.columnWidth = columnWidth;
    }

    public int getItemsSpacing() {
        return itemsSpacing;
    }

    public void setItemsSpacing(int itemsSpacing) {
        this.itemsSpacing = itemsSpacing;
    }

    Context getContext() {
        return itemsFragment.getContext();
    }

    @Override
    public void configureView(View rootView) {
        RecyclerView recyclerView = itemsFragment.getRecyclerView();
        recyclerView.setLayoutManager(layoutManager.setColumnWidth(getColumnWidth(getContext())));
        recyclerView.addItemDecoration(new ItemOffsetDecoration(getItemsSpacing()));
    }
}
