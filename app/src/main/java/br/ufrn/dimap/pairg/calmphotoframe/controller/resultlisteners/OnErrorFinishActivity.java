package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import android.os.Handler;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;

public class OnErrorFinishActivity<T> extends ActivityResultListener<T>
{
	public OnErrorFinishActivity(BaseActivity activity) {
		super(activity);
	}
	
	@Override
	public void onFailure(Throwable error) {
		finishActivity();
	}

	protected void finishActivity() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				getActivity().finish();
			}
		}, getActivity().getResources().getInteger(R.integer.time_to_finish_activity));
	}

	@Override
	public boolean isListening() {
		return true;
	}
}