package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class CheckConnectionRequest extends BaseGenericRequest<Object, Endpoint>{

    private Endpoint endpoint;

    public CheckConnectionRequest(ApiConnector apiConnector, Endpoint endpoint, ResultListener<? super Endpoint> listener) {
        super(apiConnector, Object.class, listener);
        this.endpoint = endpoint;
    }

    @Override
    public RequestHandler execute() {
        return getRequest();
    }

    @Override
    protected String genUrl() {
        return getApiBuilder().checkConnectionUrl(endpoint);
    }

    @Override
    protected Endpoint convertToResultType(Object response) {
        return this.endpoint;
    }
}
