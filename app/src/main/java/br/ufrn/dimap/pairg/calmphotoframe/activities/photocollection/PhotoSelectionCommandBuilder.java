package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CopyToPhotoCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CutFromPhotoCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.EditPhotoCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.RemoveFromAlbumCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.ConfirmAndRemoveItemsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.PhotoEditionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class PhotoSelectionCommandBuilder{
	BaseActivity activity;
	PhotoComponentSelector selector;
	PhotoCollectionController controller;
	PhotoCollectionView collView;
	
	public PhotoSelectionCommandBuilder(PhotoCollectionController collectionController
			, PhotoComponentSelector selector
			, PhotoCollectionView collView) 
	{
		super();
		this.activity = collectionController.getActivity();
		this.selector = selector;
		this.controller = collectionController;
		this.collView = collView;
	}

	public void buildCommands(BaseActionModeCallback callback){
        EntityGetter<Photo> photoGetter = new PhotoGetterFromComponent(collView.getComponentsSelector());

		Command<?> addToContextCmd  = new CopyToPhotoCollectionCommand(activity, selector);
		Command<?> removeFrom       = new RemoveFromAlbumCommand(activity, selector);
		Command<?> moveCommand      = new CutFromPhotoCollectionCommand(activity, selector);
		Command<?> removeCommand    = new ConfirmAndRemoveItemsCommand(activity, selector);
        Command<Photo> editPhoto    = new PhotoEditionCommand(activity, photoGetter);
		Command<PhotoCollection> editAlbum = new EditPhotoCollectionCommand(activity, selector);

		ReloadViewResultListener reloadViewOnResult = new ReloadViewResultListener(activity, controller);
        UpdatePhotoCollectionResultListener onUpdateAlbum = new UpdatePhotoCollectionResultListener(collView.getCollectionFragment());

		callback.putMenuCommand(R.id.action_copyToAlbum     , addToContextCmd, reloadViewOnResult);
		callback.putMenuCommand(R.id.action_removeFromAlbum , removeFrom, reloadViewOnResult);
		callback.putMenuCommand(R.id.action_cutToAlbum      , moveCommand    , reloadViewOnResult);
		callback.putMenuCommand(R.id.action_removeItem      , removeCommand  , reloadViewOnResult);
		callback.putMenuCommand(R.id.action_editAlbum       , editAlbum      , onUpdateAlbum);
		callback.putMenuCommand(R.id.action_editPhoto       , editPhoto);
	}

    public static class PhotoGetterFromComponent implements EntityGetter<Photo> {
        ItemSelector<PhotoComponent> componentItemSelector;

        public PhotoGetterFromComponent(ItemSelector<PhotoComponent> componentItemSelector) {
            this.componentItemSelector = componentItemSelector;
        }

        @Override
        public Photo getEntity() {
            PhotoComponent selected = componentItemSelector.getSelectedItem();

            return selected != null && selected.isPhoto() ? selected.getPhoto() : null;
        }
    }
}