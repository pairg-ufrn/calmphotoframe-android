package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public abstract class BaseStatelessCommand<T> implements Command<T>{

	protected void notifyOnStart(ResultListener<?> listener){
		Listeners.onStart(listener);
	}
	protected <ResultType extends T> void notifyOnResult(ResultType result, ResultListener<? super T> listener){
		Listeners.onResult(listener, result);
	}
	protected void notifyOnFailure(Throwable error, ResultListener<?> listener){
		Listeners.onFailure(listener, error);
	}
	
	@Override
	public abstract void execute(ResultListener<? super T> cmdResultListener);
}
