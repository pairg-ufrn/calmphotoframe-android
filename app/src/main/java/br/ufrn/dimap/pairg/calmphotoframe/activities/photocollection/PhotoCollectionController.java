package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.content.Intent;
import android.view.Menu;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection.SearchHandler.SearchUiListener;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photos.PhotoComponentSelectorImpl;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.WaitingForResult;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.GetCollectionAndContextCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnApiErrorDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnApiErrorNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnErrorInvalidateContext;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnResultInvalidateActivityMenu;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ComposedException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class PhotoCollectionController {
	private BaseActivity activity;

	private final CollectionAndContextContainer state;
	private PhotoCollectionView collectionView;
	private final PhotoCollectionNavigator navigator;

	private final SearchHandler searchHandler;
	private final PhotosSearchEngine searchEngine;
	
	private final WaitingForResult<CollectionAndContextContainer> waitingFor;

	public PhotoCollectionController(BaseActivity activity) {
		this.activity = activity;
		state = new CollectionAndContextContainer();

		collectionView = new PhotoCollectionView(activity, state);
		navigator = new PhotoCollectionNavigator(activity, state);

		waitingFor = new WaitingForResult<>();
		
		searchEngine = new PhotosSearchEngine(state);
		searchEngine.setOnQueryResultListener(new OnGotPhotosUpdateView(collectionView));
		searchHandler = new SearchHandler(searchEngine);
		searchHandler.setSearchUiListener(new SearchUiController());
	}
	
	public BaseActivity 					getActivity()	{ return activity;}
	public PhotoCollectionView 				getView()	   	{ return collectionView;}
	public PhotoCollectionNavigator 	  	getNavigator() 	{ return navigator;}
	public CollectionAndContextContainer 	getState()		{ return state;}

	public PhotoCollection 	getPhotoCollection(){ return state.getPhotoCollection();}
	public PhotoContext 	getCurrentContext() { return state.getPhotoContext();}
	public Long 			getContextId() 		{ return state.getContextId();}
	public Long 			getCollectionId() 	{ return state.getCollectionId();}

    public void setCollectionView(PhotoCollectionView view){
        this.collectionView = view;
        this.collectionView.setState(this.state);
    }

	/* **********************************************************************************************/
	
	public void initialize() {

		getView().onCreateView(getActivity());
		getView().addOnSelectItemListener(getNavigator());
		getView().addOnClickMenuListener(new ExecuteCommandOnClick());
		
		setupContextActionMode(collectionView);
	}

	public void setupMenu(Menu menu, CommandExecutor cmdExecutor) {
		PhotoCollectionCommandBuilder commandBuilder  
			= new PhotoCollectionCommandBuilder(this, new QueryGetter(searchEngine));
		
		commandBuilder.createCommands(menu, cmdExecutor);
		
		searchHandler.setup(menu, R.id.action_searchPhotos);
	}

	protected  boolean canEditCollection(){
		return state.isEditable();
	}

    public void requestData() {
		if(searchEngine.hasQuery()){
			searchEngine.executeQuery();
			//TODO: (maybe)also refresh context and collection, but not collection content
		}
		else{
			requestCollectionAndContext(activity.getIntent());
		}
	}

    public void refreshCollectionData() {
		Logging.i(getClass().getSimpleName(), "refreshCollectionData");
        //FIXME: do request to get only the collection
		requestCollectionAndContext(getCurrentContext(), null);
	}


	public void onAddPhoto(Photo photo) {
        this.state.addPhotoComponent(photo);
        getView().onAddPhotoComponent(new PhotoComponent(photo));
	}

    public boolean isWaitingResponse(){
        return this.waitingFor.isWaiting();
    }

	@Override
	public String toString() {
		return "PhotoCollectionController [state=" + state + "]";
	}
	
	/* **************************************** Auxiliary Methods *************************************/
	
//	protected boolean isSameContext(PhotoContext newCtx) {
//		return (newCtx == null)
//                || (getCurrentContext() != null && newCtx.getId() == getCurrentContext().getId());
//	}

	protected void setupContextActionMode(PhotoCollectionView collView){
		PhotoComponentSelector selector = new PhotoComponentSelectorImpl(getState(), collView.getComponentsSelector());
		
		PhotoCollectionActionMode callback = new PhotoCollectionActionMode(selector);
		
		PhotoSelectionCommandBuilder cmdBuilder = new PhotoSelectionCommandBuilder(this, selector, collView);
		cmdBuilder.buildCommands(callback);

        collView.setActionModeCallback(callback);
        collView.setMultiSelectionListener(callback);
	}


    protected void requestCollectionAndContext(Intent intent) {
        PhotoContext ctx = (intent == null)
                ? null
                : IntentUtils.instance().<PhotoContext>getSerializable(intent, Intents.Extras.PhotoContext);
        PhotoCollection collection = (intent == null)
                ? null
                : IntentUtils.instance().<PhotoCollection>getSerializable(intent, Intents.Extras.PhotoCollection);

        requestCollectionAndContext(ctx, collection);

    }

	protected void requestCollectionAndContext(PhotoContext context, PhotoCollection collection) {
		if(isWaitingResponse()){
			Logging.d(getClass().getSimpleName(),
					"Not started request because already is waiting for response.");
			return;
		}
		
//		Long ctxId = getContextIdOrCurrent();
        Long ctxId = getContextId(), currentCtx = ApiClient.instance().getCurrentContext();
        if(ctxId == null){
            ctxId = currentCtx;
        }
        else if(!ctxId.equals(currentCtx)){ //Changed current context
            this.state.clear();
            ctxId = currentCtx;
        }

		ResultListener<CollectionAndContextContainer> resultListener = new CompositeResultListener<>(
            new OnResultUpdateViewAndState(this.collectionView, state)
            , new OnResultInvalidateActivityMenu(getActivity())
			, new OnApiErrorDelegator<>(new CompositeResultListener<>(
					new OnErrorInvalidateContext(ctxId),
                    new NotifyUserOnError(activity),
					new OnErrorReloadOrGoBack(ctxId != null)
		          ), 404, 403
			)
		);

		waitingFor.startWaiting(resultListener);
		
		GetCollectionAndContextCommand cmd = new GetCollectionAndContextCommand()
			.context(ctxId)
			.collection(getCollectionId());

		if(collection != null){
			cmd.collection(collection);
		}

		if(context != null){
			cmd.context(context);
		}
		cmd.execute(waitingFor);
	}

	protected void invalidateWaitingRequest() {
        waitingFor.cancel();
	}

    public void setCollection(ExpandedPhotoCollection expandedPhotoCollection) {
        this.state.setPhotoCollection(expandedPhotoCollection);
        this.getView().setState(this.state);
    }

	/* **************************************** Auxiliary Classes *************************************/

    private class SearchUiController implements SearchUiListener {
		@Override
		public void onPrepareSearchUi() {
            getView().setOnSearchMode(true);
		}

		@Override
		public void onFinishSearchUi() {
            getView().setOnSearchMode(false);
			searchEngine.clearQuery();
			refreshCollectionData();
		}
	}

	private static class OnGotPhotosUpdateView extends BasicResultListener<List<Photo>> {
		final PhotoCollectionView collectionView;
		
		public OnGotPhotosUpdateView(PhotoCollectionView collectionView) {
			this.collectionView = collectionView;
		}
		
		@Override
		public void onResult(List<Photo> result) {
			List<PhotoComponent> components = new ArrayList<>();
			for(Photo p : result){
				components.add(new PhotoComponent(p));
			}
			collectionView.setCollection(components);
		}
	}

    private static class NotifyUserOnError extends OnApiErrorNotifyUser<Object> {
        public NotifyUserOnError(BaseActivity activity) {
            super(activity);
        }

        @Override
        protected void notifyErrorToUser(Throwable error) {
            ComposedException ex = ExceptionUtils.getCause(error, ComposedException.class);
            if(ex != null){//Could not found neither context nor collection
                notifyUser().text(R.string.error_contextNotAccessible).show();
            }
            else{
                super.notifyErrorToUser(error);
            }
        }
    }

	private class OnErrorReloadOrGoBack extends BasicResultListener<Object> {
        private boolean allowRetry; //Used to avoid infinite loop (should retry only first time)

        public OnErrorReloadOrGoBack(boolean allowRetry){
            this.allowRetry = allowRetry;
        }

		@Override
		public void onFailure(Throwable error) {
			Intent startIntent = activity.getIntent();
            //Se não é a raiz, então devem haver outras instâncias da activity antes desta.
            //então, retorna para nível anterior (Rever isto)
			if(!getNavigator().isRootPhotoCollection()){
				activity.finish();
			}
			else if(allowRetry){
				invalidateWaitingRequest();
				state.clear();
				requestCollectionAndContext(null);
			}
		}
	}

    private class ExecuteCommandOnClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getActivity().executeCommand(v.getId());
        }
    }
}
