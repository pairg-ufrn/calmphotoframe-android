package br.ufrn.dimap.pairg.calmphotoframe.activities;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnErrorNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.localserver.StartLocalServerCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerFinishEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameBroadcastReceiver;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.ServerStatusListener;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class ConnectionHelper implements ServerStatusListener {

    private ConnectedActivity activity;
    private PhotoFrameBroadcastReceiver receiver;

    private final ConnectionListeners connectionListener;

    private StartLocalServerCommand startLocalServerCommand;

    public ConnectionHelper(ConnectedActivity activity) {
        this.activity = activity;
        receiver = new PhotoFrameBroadcastReceiver(this);

        connectionListener = new ConnectionListeners();

        startLocalServerCommand = new StartLocalServerCommand(activity);
    }

    public ConnectionListeners listeners() {
        return connectionListener;
    }

    public void start() {
        receiver.register(activity);
    }

    public void stop() {
        receiver.unregister(activity);
    }

    public void restartService() {
        checkLocalServer();
    }

    public void checkConnection() {
        Logging.instance().i(getClass().getSimpleName(), "connect");

        ApiClient client = ApiClient.instance();

        if (client.hasEndpoint()) {
            if (client.getEndpoint().isLocal()) {
                checkLocalServer();
            } else {
                checkSession();
            }
        }
    }

    private void checkLocalServer() {
        //Listener already registered to server events, so sets listener to null
        startLocalServerCommand.execute(null);
    }

    public void checkSession() {
        ApiClient client = ApiClient.instance();

        if (client.isConnected()) {
            client.session().checkConnection(new BasicResultListener<ApiEndpoint>(){
                @Override
                protected void handleResult(ApiEndpoint result) {
                    listeners().sessionUpdate(result);
                }

                @Override
                protected void handleError(Throwable error) {
                    if(ExceptionUtils.isCausedBy(error, ApiException.class)){
                        ApiException apiExceptionCause = ExceptionUtils.getCause(error, ApiException.class);

                        if(apiExceptionCause != null && apiExceptionCause.getStatusCode() == 401){//unauthorized
                            listeners().sessionFinished(ApiClient.instance().getEndpoint());

                            return;
                        }
                    }

                    listeners().connectionError(error);
                }
            });
        }
    }

    @Override
    public void onServerStart(ServerStartEvent startEvent) {
        connectionListener.localServerStarted(this);

        if(isConnectedToLocal()) {
            checkSession();
        }
    }

    @Override
    public void onServerStop(ServerFinishEvent finishEvent) {
        connectionListener.localServerStopped(this);
    }

    @Override
    public boolean isListening() {
        return true;
    }

    public boolean isConnectedToLocal() {
        Endpoint endpoint = ApiClient.instance().getEndpoint();
        return endpoint != null && endpoint.isLocal();
    }


    public static class ConnectionListeners{
        ResultListener<? super ConnectionHelper>
                onLocalServerStart, onLocalServerFinish;

        ResultListener<? super Endpoint> onSessionFinish;
        ResultListener<? super ApiEndpoint> onSessionUpdate;
        private ResultListener<? super ApiEndpoint> onConnectionError;

        public ResultListener<? super ConnectionHelper> getOnLocalServerStart() {
            return onLocalServerStart;
        }
        public void setOnLocalServerStart(ResultListener<? super ConnectionHelper> onLocalServerStart) {
            this.onLocalServerStart = onLocalServerStart;
        }

        public ResultListener<? super ConnectionHelper> getOnLocalServerFinish() {
            return onLocalServerFinish;
        }
        public void setOnLocalServerFinish(ResultListener<? super ConnectionHelper> onLocalServerFinish) {
            this.onLocalServerFinish = onLocalServerFinish;
        }

        public ResultListener<? super Endpoint> getOnSessionFinish() {
            return onSessionFinish;
        }
        public void setOnSessionFinish(ResultListener<? super Endpoint> onSessionFinish) {
            this.onSessionFinish = onSessionFinish;
        }

        public ResultListener<? super ApiEndpoint> getOnSessionUpdate() {
            return onSessionUpdate;
        }
        public void setOnSessionUpdate(ResultListener<? super ApiEndpoint> onSessionUpdate) {
            this.onSessionUpdate = onSessionUpdate;
        }

        public void setOnConnectionError(ResultListener<? super ApiEndpoint> onConnectionError) {
            this.onConnectionError = onConnectionError;
        }
        public ResultListener<? super ApiEndpoint> getOnConnectionError() {
            return onConnectionError;
        }

        public void sessionFinished(Endpoint endpoint){
            Listeners.onResult(getOnSessionFinish(), endpoint);
        }
        public void sessionUpdate(ApiEndpoint endpoint){
            Listeners.onResult(getOnSessionUpdate(), endpoint);
        }
        public void localServerStarted(ConnectionHelper connectionHelper){
            Listeners.onResult(getOnLocalServerStart(), connectionHelper);
        }
        public void localServerStopped(ConnectionHelper connectionHelper){
            Listeners.onResult(getOnLocalServerFinish(), connectionHelper);
        }
        public void connectionError(Throwable error) {
            Listeners.onFailure(getOnConnectionError(), error);
        }
    }
}
