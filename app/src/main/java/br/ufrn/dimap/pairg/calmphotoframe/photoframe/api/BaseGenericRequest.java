package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.loopj.android.http.RequestParams;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.ParseException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public abstract class BaseGenericRequest<ResponseType, ListenerResultType>
        extends BaseApiRequest implements Converter<ResponseType, ListenerResultType>
{
    private Class<? extends ResponseType> entityClass;

    public BaseGenericRequest(ApiConnector apiConnector, Class<? extends ResponseType> entityClass) {
		this(apiConnector, entityClass, null);
	}
	public BaseGenericRequest(ApiConnector apiConnector, Class<? extends ResponseType> entityClass
			, ResultListener<? super ListenerResultType> listener) {
		super(apiConnector, listener);
        this.entityClass = entityClass;
	}

    public Class<? extends ResponseType> getEntityClass() {
		return entityClass;
	}

	@Override
	public abstract RequestHandler execute();

    @Override
	protected abstract String genUrl();

    @Override
    public ListenerResultType convert(ResponseType source) {
        return convertToResultType(source);
    }

    protected abstract ListenerResultType convertToResultType(ResponseType response);

    protected RequestBuilder<ResponseType, ListenerResultType> request(){
        //noinspection unchecked
        return getApiConnector()
                .<ResponseType, ListenerResultType>request(genUrl())
                .converter(this, getEntityClass())
                .listener(listener);
    }

	protected RequestHandler getRequest(){
		return request().get();
	}
	protected RequestHandler deleteRequest(){
		return request().delete();
	}
	protected RequestHandler postRequest() {
		return request().post();
	}
    protected RequestHandler postRequest(RequestParams params){
        return request().post(params);
    }
	protected RequestHandler postJsonRequest(Object content){
        return jsonRequest(HttpMethods.Post, content);
    }
	protected RequestHandler jsonRequest(HttpMethods method, Object content) {
        try {
            return request().json(content).method(method).send();

        } catch (JsonProcessingException e) {
            Listeners.onFailure(listener, new ParseException(e));
            return new FinishedRequestHandler();
        }
	}

}
