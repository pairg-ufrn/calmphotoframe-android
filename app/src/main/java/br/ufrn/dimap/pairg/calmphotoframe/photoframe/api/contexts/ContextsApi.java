package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ContextsApi {
    private ApiConnector apiConnector;

    public ContextsApi(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }

    public RequestHandler get(Long contextId, ResultListener<? super PhotoContext> resultListener) {
        return get(contextId, null, resultListener);
    }
    public RequestHandler get(Long contextId, Boolean includePermission
            , ResultListener<? super PhotoContext> listener)
    {
        return new GetPhotoContextRequest(apiConnector, contextId, includePermission, listener).execute();
    }
    public RequestHandler list(ResultListener<? super List<? extends PhotoContext>> listener) {
        return new ListContextsRequest(apiConnector, listener).execute();
    }

    public RequestHandler create(PhotoContext ctx, ResultListener<? super PhotoContext> listener) {
        return new CreatePhotoContextRequest(apiConnector, ctx, listener).execute();
    }

    public RequestHandler remove(PhotoContext ctxToRemove , ResultListener<? super PhotoContext> listener)
    {
        return new RemovePhotoContextRequest(apiConnector, ctxToRemove.getId(), listener).execute();
    }

    public RequestHandler update(PhotoContext contextToUpdate, ResultListener<? super PhotoContext> listener) {
        return new UpdatePhotoContextRequest(apiConnector, contextToUpdate, listener).execute();
    }

    public RequestHandler update(
            PhotoContext contextToUpdate,
            Collection<Collaborator> collaborators,
            ResultListener<? super PhotoContext> listener)
    {
        return new UpdatePhotoContextWithCollaboratorsRequest(apiConnector, contextToUpdate, collaborators, listener).execute();
    }

    /** Get the PhotoCollection that is root of some PhotoContext
     * @param contextId - the id of the PhotoContext or null to use the current photo context
     * @param expandedColl - if true, gets the PhotoCollection together with its photos and subcontexts
     * @param listener - callback to return the PhotoCollection or an failure, if the request failed
     * */
    public RequestHandler getRootCollection(Long contextId, boolean expandedColl
            , ResultListener<? super PhotoCollection> listener) {
        return new GetRootCollectionRequest(apiConnector, contextId, listener, expandedColl).execute();
    }

    public void getCollaborators(Long ctxId, ResultListener<Collection<Collaborator>> listener) {
        new GetCollaboratorsRequest(apiConnector, ctxId, listener).execute();
    }

}
