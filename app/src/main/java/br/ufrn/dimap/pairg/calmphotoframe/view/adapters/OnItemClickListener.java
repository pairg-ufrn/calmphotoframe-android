package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.view.View;

/**
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position, long itemId);
}
