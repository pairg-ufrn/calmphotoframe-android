package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public class Listeners {

    @Deprecated
	public static <T> void onResult(ResultListener<? super T> listener, Object cmdId, T result){
		onResult(listener, result);
	}

	public static boolean isListening(ResultListener<?> listener) {
		return listener != null && listener.isListening();
	}

    public static <T> void onResult(ResultListener<? super T> listener, T result){
        if(isListening(listener)){
            listener.onResult(result);
        }
    }
    public static void onFailure(ResultListener<?> listener, Throwable error){
        if(isListening(listener)){
            listener.onFailure(error);
        }
    }
    public static <T> void onStart(ResultListener<T> listener) {
        if(isListening(listener)){
            listener.onStart();
        }
    }

    @SafeVarargs
    public static <T> CompositeResultListener<? super T> join(ResultListener<? super T>... listeners) {
        return new CompositeResultListener<>(listeners);
    }
}
