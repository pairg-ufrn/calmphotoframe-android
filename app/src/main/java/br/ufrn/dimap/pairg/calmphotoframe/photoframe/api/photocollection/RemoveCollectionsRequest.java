package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.Collection;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class RemoveCollectionsRequest 
	  extends BaseGenericRequest<PhotoCollectionsContainer, Collection<PhotoCollection>> 
{
	/**Classe auxiliar para serialização de colleções de fotos em json*/
	public static class CollectionsIdsContainer{
		public Set<Long> collectionIds;
		public CollectionsIdsContainer(Set<Long> collectionsIds) {
			this.collectionIds = collectionsIds;
		}
	}

	private Set<Long> collectionsToRemove;
	
	public RemoveCollectionsRequest(ApiConnector apiConnector
			, Collection<PhotoCollection> photoCollections
			, ResultListener<? super Collection<PhotoCollection>> listener) 
	{
		super(apiConnector, PhotoCollectionsContainer.class, listener);
		collectionsToRemove = PhotoCollection.extractCollectionIds(photoCollections);
	}

	@Override
	public RequestHandler execute() {
		return jsonRequest(HttpMethods.Delete, new CollectionsIdsContainer(this.collectionsToRemove));
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().collectionsUrl();
	}

	@Override
	protected Collection<PhotoCollection> convertToResultType(PhotoCollectionsContainer response) {
		return response.collections;
	}
}
