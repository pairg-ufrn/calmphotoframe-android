package br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable;

import android.widget.Filter;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ViewBuilderAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;

public class FilterableViewBuilderAdapter<T> extends ViewBuilderAdapter<T> implements FilterableAdapter<T> {
    private class NonFilter extends Filter {
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // TODO Auto-generated method stub
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            results.count = getCount();
            results.values = getItems();
            return results;
        }
    }


    private Filter filter;
    private List<T> filteredItems;
    private CollectionFilter<T> collectionFilter;

	public FilterableViewBuilderAdapter(ViewBuilder builder) {
        this(builder, null);
    }

	public FilterableViewBuilderAdapter(ViewBuilder builder, Filter filter) {
        super(builder);

        this.filter = (filter == null) ? new NonFilter() : filter;
        this.filteredItems = null;
    }

	public CollectionFilter<T> getCollectionFilter() {
        return collectionFilter;
    }

	public void setCollectionFilter(CollectionFilter<T> collectionFilter) {
        this.collectionFilter = collectionFilter;
        this.filter = collectionFilter == null ? new NonFilter() : new FilterWrapper<>(this, collectionFilter);
    }

    @Override
    public Collection<T> getOriginalItems() {
        return super.getItems();
    }

    @Override
    protected List<T> _getItems() {
        return filteredItems == null ? super._getItems() : filteredItems;
    }

    @Override
    public void setItems(Collection<T> ts, boolean notifyChanges) {
        this.filteredItems = null;
        super.setItems(ts, notifyChanges);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

	@Override
    public void clearFilteredItems() {
        filteredItems = null;
    }


    public List<T> getFilteredItems() {
        return filteredItems;
    }

	@Override
    public void setFilteredItems(List<T> values) {
        this.filteredItems = values;
        if(values.size() > 0){
            this.notifyDataSetChanged();
        }
        else{
            this.notifyDataSetInvalidated();
        }
    }
}
