package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/** Uma implementação de PageAdapter que permite manipular os fragmentos como em uma lista.
 * Esta implementação não instancia os fragmentos para os seus clientes.*/
public class ListPageAdapter extends PagerAdapter {	
	protected class FragmentContainer{
		public Fragment fragment;
		public int id;
	}
	final List<Fragment> fragments;
	public ListPageAdapter() {
		fragments = new ArrayList<>();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		Fragment fragment = (Fragment)object;
		return fragment.getView() == view;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Log.d(getClass().getName(), "pos: " + position + "; Container: " + container);
		Fragment fragment = fragments.get(position);
		View view = fragment.getView();		
		
		if(view == null){//TODO: testar isto
			LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = fragment.onCreateView(inflater, container, null);
		}
		if(view.getParent() != null && view.getParent() != container){
			ViewGroup parent = (ViewGroup) view.getParent();
			parent.removeView(view);
		}
		container.addView(view);
		
		return fragment;
	}
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		Fragment fragment = (Fragment)object;

		if(fragment.getView() != null){
			container.removeView(fragment.getView());
		}
	}
	
	public Fragment getItem(int index) {
		return getFragment(index);
	}

	protected Fragment getFragment(int index) {
		return fragments.get(index);
	}

	public void moveFragment(int startIndex, int finalIndex){
		Fragment frag = fragments.get(startIndex);
		fragments.remove(startIndex);
		fragments.add(startIndex < finalIndex ? finalIndex - 1 : finalIndex, frag);
		this.notifyDataSetChanged();
	}
	public void addItem(Fragment fragment){
		fragments.add(fragment);
		this.notifyDataSetChanged();
	}
	public void addItem(Fragment fragment, int position){
		fragments.add(position, fragment);
		this.notifyDataSetChanged();
	}
	public void removeItem(Fragment fragment){
		fragments.remove(fragment);
		this.notifyDataSetChanged();
	}
	public void removeItem(int index){
		fragments.remove(index);
		this.notifyDataSetChanged();
	}
	public boolean containsItem(Fragment fragment){
		return fragments.contains(fragment);
	}
	public int indexOf(Fragment fragment){
		return fragments.indexOf(fragment);
	}
	
	@Override
	public int getCount() {
		return fragments.size();
	}
	
	@Override
	public int getItemPosition(Object object) {
		if(fragments.contains(object)){
			return fragments.indexOf(object);
		}
		return PagerAdapter.POSITION_NONE;
	}



}
