package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import android.graphics.Bitmap;

public class PhotoContainer {
	private Photo photoDescriptor;
	private Bitmap image;
	private final int photoId;
	private boolean failed;
	private boolean fromCache;

	public PhotoContainer(Photo photo) {
		this(photo.getId());
		this.setPhotoDescriptor(photo);
	}
	public PhotoContainer(int photoId) {
		photoDescriptor = null;
		image = null;
		this.failed = false;
		this.photoId = photoId;
		this.fromCache = false;
	}
	
	public int getPhotoId(){
		return photoId;
	}
	public synchronized Photo getPhotoDescriptor() {
		return photoDescriptor;
	}
	public synchronized Bitmap getImage() {
		return image;
	}
	public synchronized void setPhotoDescriptor(Photo photoDescriptor) {
		this.photoDescriptor = photoDescriptor;
	}
	public synchronized void setImage(Bitmap image) {
		this.image = image;
	}
	public synchronized void setImage(Bitmap image, boolean fromCache) {
		this.image = image;
		this.fromCache = fromCache;
	}
	public synchronized boolean isImageFromCache(){
		return fromCache;
	}
	public synchronized boolean containsImage(){
		return image != null;
	}
	public synchronized boolean containsDescription(){
		return photoDescriptor != null;
	}
	public synchronized boolean containsDescriptionAndImage(){
		return containsDescription() && containsImage();
	}

	public synchronized void setFail(boolean failed) {
		this.failed = failed;
	}
	public synchronized boolean hasFailed(){
		return failed;
	}
}