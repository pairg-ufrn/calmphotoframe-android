package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class CreatePhotoContextRequest extends CommonGenericRequest<PhotoContext>{
	private PhotoContext context;

	
	public CreatePhotoContextRequest(ApiConnector apiConnector, PhotoContext context
			, ResultListener<? super PhotoContext> listener)
	{
		super(apiConnector, PhotoContext.class, listener);
		this.context = context;
	}
	
	@Override
	public RequestHandler execute(){
		return postJsonRequest(context);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextsUrl();
	}
}