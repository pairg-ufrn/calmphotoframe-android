package br.ufrn.dimap.pairg.calmphotoframe.controller;

public interface Builder<T> {
    T build();
}
