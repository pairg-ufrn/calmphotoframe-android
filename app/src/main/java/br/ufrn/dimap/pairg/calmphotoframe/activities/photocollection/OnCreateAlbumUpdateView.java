package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class OnCreateAlbumUpdateView extends ActivityResultListener<PhotoCollection>{
	private PhotoCollectionView photoCollectionView;
	
	public OnCreateAlbumUpdateView(BaseActivity activity, PhotoCollectionView collectionView)
	{
		super(activity);
		this.photoCollectionView = collectionView;
	}
	
	@Override
	public void onResult(PhotoCollection result) {
		photoCollectionView.addPhotoComponent(result);
	}
	@Override
	public void onFailure(Throwable error) {
		getActivity().notifyError("Failed to create photo context", error);
	}
}