package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class InflaterViewBinder<T> implements BinderViewHolder<T>{
	private int layoutId;
	
	public InflaterViewBinder(int layoutId) {
		this.layoutId = layoutId;
	}

	@Override
	public View buildView(ViewGroup viewParent, int viewType) {
		return inflateView(viewParent, viewType);
	}

	public View buildView(LayoutInflater inflater, int viewType) {
		return inflateView(null, inflater, viewType);
	}
	
	protected View inflateView(ViewGroup parent, int viewType){
		return inflateView(parent, LayoutInflater.from(parent.getContext()), viewType);
	}

	protected View inflateView(ViewGroup parent, LayoutInflater layoutInflater, int viewType) {
		return layoutInflater.inflate(getLayoutId(), parent, false);
	}

	public int getLayoutId(){
		return layoutId;
	}
	public InflaterViewBinder<T> setLayoutId(int layoutId){
		this.layoutId = layoutId;
		return this;
	}
}
