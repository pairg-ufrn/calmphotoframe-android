package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.Cache.SizeCalculator;

public class PhotoCache {
	public interface CacheKeyGenerator<T> {
		String genKey(T item);
	}
    public static class DefaultCacheKeyGenerator implements CacheKeyGenerator<Integer> {
        @Override
        public @NonNull  String genKey(Integer item) {
            return String.valueOf(item);
        }
    }

	private CacheKeyGenerator<? super Integer> keyGenerator;
	private final Cache<String, Bitmap> imageCache;

    public PhotoCache(Cache<String, Bitmap> imageCache) {
        this(imageCache, new DefaultCacheKeyGenerator());
    }
    public PhotoCache(Cache<String, Bitmap> imageCache, CacheKeyGenerator<? super Integer> keyGenerator) {
        this.keyGenerator = keyGenerator;
        this.imageCache = imageCache;
        this.imageCache.setSizeCalculator(new SizeCalculator<Bitmap>() {
            @Override
            public int sizeOf(Bitmap bitmap) {
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        });
    }

	public synchronized void setKeyGenerator(CacheKeyGenerator<? super Integer> photoOrigin){
		this.keyGenerator = photoOrigin;
	}

	public synchronized boolean containsImage(Integer photoId) {
		return getImage(photoId) != null;
	}

	@SuppressWarnings("unused")
    public synchronized boolean containsThumbnail(Integer photoId) {
		return getThumbnail(photoId) != null;
	}
	
	public synchronized Bitmap getImage(Integer photoId) {
		return imageCache.getItem(generateCacheKey(photoId));
	}
	public synchronized Bitmap getThumbnail(Integer photoId) {
		return imageCache.getItem(generateThumbnailCacheKey(photoId));
	}
	public synchronized void putImage(Integer photoId, Bitmap bitmap) {
		putItem(generateCacheKey(photoId), bitmap);
	}
	public synchronized void putThumbnail(Integer photoId, Bitmap image) {
		putItem(generateThumbnailCacheKey(photoId), image);
	}
	private synchronized void putItem(String key, Bitmap image) {
		this.imageCache.removeItem(key);
		this.imageCache.putItem(key, image);
	}

	/** If cache has less than {@code numBytes} of free size, try release itens until
	 * accomplish the required space. If {@code numBytes} is greater than the maximum cache size,
	 * throws an exception.*/
	public synchronized void reserve(int numBytes) {
		this.imageCache.reserve(numBytes);
	}
	public synchronized void remove(Integer photoId) {
		imageCache.removeItem(generateCacheKey(photoId));
		imageCache.removeItem(generateThumbnailCacheKey(photoId));
	}

    protected String generateCacheKey(Integer photoId) {
        return keyGenerator.genKey(photoId);
    }
    protected String generateThumbnailCacheKey(Integer photoId) {
        return keyGenerator.genKey(photoId) + "_thumbnail";
    }
}
