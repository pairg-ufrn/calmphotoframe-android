package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.util.Log;

import java.io.File;
import java.io.IOException;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ChooseFileCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.PhotoJsonFileParser;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog.SaveDialogResponseListener;

public class ImportPhotoCommand extends ActivityStatelessCommand<Photo> {

	private static final String IMPORT_DIALOG_TAG = "IMPORT_DIALOG_TAG";

	private PhotoHolder photoHolder;
	
	public ImportPhotoCommand(BaseActivity activity, PhotoHolder photoHolder) {
		super(activity);
		this.photoHolder = photoHolder;
	}

	@Override
	public void execute(ResultListener<? super Photo> listener) {
		importPhotoDescription(listener);
	}
	private void importPhotoDescription(ResultListener<? super Photo> listener) {
		Photo photoToImport = photoHolder.getCurrentPhoto();
		
    	SaveDialog dialogFragment = new SaveDialog();
    	dialogFragment.setDialogMessage(getContext().getResources().getString(R.string.import_photo_dialog_message));
    	dialogFragment.setFilenameEditable(false);
    	dialogFragment.setDialogResponseListener(new ImportPhotoDialogListener(photoToImport, getActivity(), listener));
    	dialogFragment.show(getActivity().getSupportFragmentManager(), IMPORT_DIALOG_TAG);
	}

	/* *****************************************************************************************/
	private final static class ImportPhotoDialogListener 
					implements SaveDialogResponseListener
	{
		private Photo photoToImport;
		private BaseActivity activity;
		private ResultListener<? super Photo> listener;
		private PhotoJsonFileParser parser = new PhotoJsonFileParser();

		private ChooseFileCommand chooseFileCommand;

		public ImportPhotoDialogListener(Photo photoToImport, BaseActivity activity
				, ResultListener<? super Photo> listener) 
		{
            this.chooseFileCommand = new ChooseFileCommand(activity);
			this.photoToImport = photoToImport;
			this.activity = activity;
			this.listener = listener;
		}
		
		@Override
		public void onCancel() {
			Log.d(getClass().getName(), "Cancel import");
		}

		@Override
		public void onChoosePath(String currentPath) {
			showFileChooser(currentPath, true);
		}
		@Override
		public void onSelectFile(File importPath) {
			if(!importPath.exists()){
				Log.w(getClass().getName(), "Trying to import non existent file: " + importPath);
				getActivity().notifyUser().text(R.string.error_fileNotExist).show();
				return;
			}
		    importPhoto(importPath);
		}

		private BaseActivity getActivity(){
			return activity;
		}
		
		private void importPhoto(File importPath) {
			Photo descriptor = tryGetPhotoFromJsonFile(importPath); 
			if(descriptor != null){
				descriptor.setId(photoToImport.getId());
				ApiClient.instance().photos().update(descriptor, listener);
			}
		}

		private Photo tryGetPhotoFromJsonFile(File importPath) {
			try {
				return parser.readPhoto(importPath);
			}catch (IOException e) {
				notifyFailure(e);
			}
			return null;
		}
		
		private void showFileChooser(String startPath, boolean selectFile) {
			chooseFileCommand.execute(new ChooseFileCommand.FileChooserArguments(startPath, !selectFile),
                    new OnResultSetSaveDialogPath(getActivity(), IMPORT_DIALOG_TAG));
		}
		
		private void notifyFailure(Throwable error){
			Listeners.onFailure(listener, error);
		}

    }

}
