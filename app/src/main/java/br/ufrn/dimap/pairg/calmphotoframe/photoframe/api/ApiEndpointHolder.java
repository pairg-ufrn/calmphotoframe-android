package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import java.util.concurrent.atomic.AtomicReference;

import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;

public class ApiEndpointHolder implements EntityGetter<ApiEndpoint>{

    public interface OnChangeApiEndpoint {
        void onChange(ApiEndpoint apiEndpoint);
    }

    private final AtomicReference<ApiEndpoint> currentApiEndpoint = new AtomicReference<>();

    private OnChangeApiEndpoint onChangeListener;

    @Override
    public ApiEndpoint getEntity() {
        return get();
    }

    public ApiEndpoint get(){
        return currentApiEndpoint.get();
    }

    public void set(ApiEndpoint apiEndpoint){
        currentApiEndpoint.set(apiEndpoint);
        if(onChangeListener != null){
            onChangeListener.onChange(apiEndpoint);
        }
    }

    public boolean hasValue() {
        return get() != null;
    }

    public OnChangeApiEndpoint getOnChangeListener() {
        return onChangeListener;
    }

    public void setOnChangeListener(OnChangeApiEndpoint onChangeListener) {
        this.onChangeListener = onChangeListener;
    }
}
