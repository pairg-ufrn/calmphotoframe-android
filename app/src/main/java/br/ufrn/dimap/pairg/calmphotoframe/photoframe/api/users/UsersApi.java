package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import android.graphics.Bitmap;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UsersApi {
    private ApiConnector apiConnector;

    public UsersApi(ApiConnector connector) {
        this.apiConnector = connector;
    }

    public RequestHandler get(Long userId, ResultListener<? super User> listener) {
        return new GetUserRequest(apiConnector).userId(userId).listener(listener).execute();
    }
    public RequestHandler list(ResultListener<? super List<User>> listener) {
        return new ListUsersRequest(apiConnector, listener).execute();
    }
    public RequestHandler create(LoginInfo info, ResultListener<? super User> listener) {
        return new CreateUserRequest(apiConnector, info, listener).execute();
    }

    /** CreateUser at remote endpoint */
    public RequestHandler create(LoginInfo loginInfo, Endpoint endpoint, ResultListener<? super User> listener) {
        return new CreateUserRequest(apiConnector, loginInfo, endpoint, listener).execute();
    }
    public RequestHandler remove(long userId, ResultListener<? super User> listener){
        return new RemoveUserRequest(apiConnector, userId, listener).execute();
    }
    public RequestHandler update(User user, ResultListener<? super User> listener){
        return new UpdateUserRequest(apiConnector, user, listener).execute();
    }

    public RequestHandler changePassword(String newPassword, ResultListener<Object> listener) {
        return new ChangePasswordRequest(apiConnector, newPassword).listener(listener).execute();
    }

    public RequestHandler getProfileImage(Long id, ResultListener<? super Bitmap> listener) {
        return new GetProfileImageRequest(apiConnector, id).listener(listener).execute();
    }

    public RequestHandler updateProfileImage(ImageContent imageContent, Long currentUserId, ResultListener<Object> listener) {
        return new UpdateProfileImageRequest(apiConnector, imageContent, currentUserId).listener(listener).execute();
    }

    public RequestHandler changeActiveState(Long userId, boolean active,
                                            ResultListener<? super UserActivationRequest.ActiveState> listener)
    {
        return new UserActivationRequest(apiConnector, userId, active).listener(listener).execute();
    }
}
