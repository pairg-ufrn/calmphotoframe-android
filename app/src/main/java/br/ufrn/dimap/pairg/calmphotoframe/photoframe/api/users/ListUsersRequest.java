package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.ListUsersRequest.UsersListContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ListUsersRequest extends BaseGenericRequest<UsersListContainer, List<User>> {
	
	public ListUsersRequest(ApiConnector apiConnector, ResultListener<? super List<User>> listener) {
		super(apiConnector, UsersListContainer.class, listener);
	}

	@Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().usersUrl();
	}

	@Override
	protected List<User> convertToResultType(UsersListContainer response) {
		return response.users;
	}
	
	public static class UsersListContainer{
		public List<User> users;
	}
}
