package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class RemovePhotoContextRequest extends CommonGenericRequest<PhotoContext>{
	private long ctxToRemove;
	
	public RemovePhotoContextRequest(ApiConnector apiConnector, long ctxToRemoveId
			, ResultListener<? super PhotoContext> listener) 
	{
		super(apiConnector, PhotoContext.class, listener);
		this.ctxToRemove = ctxToRemoveId;
	}

	@Override
	public RequestHandler execute() {
		return deleteRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextUrl(ctxToRemove);
	}
	
}