package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class Permission implements Cloneable, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private Long contextId;
	private PermissionLevel level;
	
	public Permission() {
		this(null, null, null);
	}
	public Permission(Long userId, Long contextId, PermissionLevel level) {
		super();
		this.userId = userId;
		this.contextId = contextId;
		this.level = level;
	}

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getContextId() {
		return contextId;
	}
	public void setContextId(Long contextId) {
		this.contextId = contextId;
	}
	public PermissionLevel getLevel() {
		return level;
	}
	public void setLevel(PermissionLevel level) {
		this.level = level;
	}
	
	@Override
	public Permission clone(){
		return new Permission(getUserId(), getContextId(), getLevel());
	}
}
