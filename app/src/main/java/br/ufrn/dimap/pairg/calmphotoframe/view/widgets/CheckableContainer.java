package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import com.manuelpeinado.multichoiceadapter.CheckableRelativeLayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;

public class CheckableContainer extends CheckableRelativeLayout{
	public enum CheckMode{non_recursive, children_only, recursive};
	
	private boolean checked;
	private CheckMode checkMode;
	
	public CheckableContainer(Context context) {
		this(context, null);
	}

	public CheckableContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
		checked = false;
		checkMode = CheckMode.recursive;
	}

	public void toggle() {
        setChecked(!isChecked());
    }

	@Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public void setChecked(boolean checked) {
        if (isChecked() != checked) {
            this.checked = checked;
            setCheckableViewState(checked);
        }
    }

	public CheckMode getCheckMode() {
		return checkMode;
	}

	public void setCheckMode(CheckMode checkMode) {
		this.checkMode = checkMode;
	}

	protected void setCheckableViewState(boolean checked) {
		refreshDrawableState();
		setCheckedRecursive(this, checked);
	}

    private void setCheckedRecursive(ViewGroup parent, boolean checked) {
		if(CheckMode.non_recursive.equals(getCheckMode())){
			return;
		}
		
        int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = parent.getChildAt(i);
            if (v instanceof Checkable) {
                ((Checkable) v).setChecked(checked);
            }

            if(!recurseOnlyChildren()){
	            if (v instanceof ViewGroup) {
	                setCheckedRecursive((ViewGroup) v, checked);
	            }
            }
        }
    }

	protected boolean recurseOnlyChildren() {
		return CheckMode.children_only.equals(getCheckMode());
	}
}
