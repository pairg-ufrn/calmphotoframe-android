package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class RemoveUserRequest extends CommonGenericRequest<User>{
	long userId;

	public RemoveUserRequest(ApiConnector apiConnector, User user, ResultListener<? super User> listener) {
		this(apiConnector, user.getId(), listener);
	}
	public RemoveUserRequest(ApiConnector apiConnector, long userId, ResultListener<? super User> listener) {
		super(apiConnector, User.class, listener);
		this.userId = userId;
	}

	@Override
	public RequestHandler execute(){
		return deleteRequest();
	}
	@Override
	protected User convertToResultType(User responseType) {
		return super.convertToResultType(responseType);
	}
	@Override
	protected String genUrl() {
		return getApiBuilder().userUrl(userId);
	}

}
