package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import java.io.FileNotFoundException;
import java.io.IOException;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CancellableCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class InsertImageCommand implements CancellableCommand<String, Photo>{
    Long photoCollectionId;

    public InsertImageCommand(Long photoCollectionId) {
        this.photoCollectionId = photoCollectionId;
    }

    @Override
    public RequestHandler execute(String imgPath, ResultListener<? super Photo> listener) {
        if (imgPath == null) {
            Listeners.onFailure(listener, new FileNotFoundException());
            return null;
        }

        try {
            return insertImage(imgPath, listener);
        } catch (IOException | InvalidContentTypeException e) {
            Listeners.onFailure(listener, e);
        }
        return null;
    }

    public RequestHandler insertImage(String imgPath, ResultListener<? super Photo> listener) throws IOException {
        ImageContent imgContent = ImageContent.builder().fromString(imgPath);

        if((imgContent.getMimeType() != null && !imgContent.getMimeType().startsWith("image/"))){
            throw new InvalidContentTypeException("Invalid mime type: " + imgContent.getMimeType());
        }

        return ApiClient.instance().photos()
                .insert(imgContent, photoCollectionId, listener);
    }

}
