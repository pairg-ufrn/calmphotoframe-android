package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntitySelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext.RemoveContextCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextComposeFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextsFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;

public class PhotoContextsActionMode extends BaseActionModeCallback implements MultiSelectionListener {
    private final PhotoContextsFragment contextsFragment;
    private ReloadContextsResultsListener.ReloadListener reloadListener;
    private EntitySelector<Long> contextIdSelector;
    private PhotoContextDetailsController detailsController;

    public PhotoContextsActionMode(int menuId
            , PhotoContextsController contextsController)
    {
        this(menuId,
                contextsController.getContextsFragment(),
                contextsController.getDetailsController(),
                contextsController.getContextSelector(),
                contextsController);
    }
    public PhotoContextsActionMode(int menuId
            , PhotoContextsFragment contextsFragment
            , PhotoContextDetailsController detailsController
            , EntitySelector<Long> contextIdSelector
            , ReloadContextsResultsListener.ReloadListener reloadListener)
    {
        super(menuId);
        this.contextsFragment = contextsFragment;

        this.contextIdSelector = contextIdSelector;

        this.reloadListener = reloadListener;
        this.detailsController = detailsController;

        this.contextsFragment.getActionModeCompat().setActionModeCallback(this);
        this.contextsFragment.getActionModeCompat().setSelectionListener(this);
    }

    @Override
    protected boolean onCreateActionMode(BaseActionModeCallback callback, ActionMode mode, Menu menu) {
        super.onCreateActionMode(callback, mode, menu);

        BaseActivity activity = detailsController.getActivity();

        ReloadContextsResultsListener reloadContextsListener = new ReloadContextsResultsListener(activity, this.reloadListener);
        ShowContextDetailsCommand showCtxCommand
                = new ShowContextDetailsCommand(contextsFragment, detailsController, this.reloadListener);

        callback.putMenuCommand(R.id.action_viewContext, showCtxCommand, reloadContextsListener);
        callback.putMenuCommand(R.id.action_editContext, showCtxCommand, reloadContextsListener);
        callback.putMenuCommand(R.id.action_removeContext
                    , new RemoveContextCommand(activity, contextsFragment.getAdapter()),  reloadContextsListener);
        callback.putMenuCommand(R.id.action_selectContext,
                new ChangePhotoContextCommand(contextsFragment.getAdapter(), this.contextIdSelector),
                new CompositeResultListener<>(reloadContextsListener
                        , new NotifyUserOnContextChange(activity)
                        , new PutContextAsActivityResult(activity)));

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        boolean changed = super.onPrepareActionMode(mode, menu);

        PhotoContext selectedCtx = contextsFragment.getAdapter().getSelectedItem();

        boolean editable = userHasEditPermission(selectedCtx);

        MenuItem viewContextItem = menu.findItem(R.id.action_viewContext);
        //Item to viewContext should not be visible when it is editable (and vice-versa)
        if (viewContextItem.isVisible() == editable) {
            changed = true;

            menu.setGroupEnabled(R.id.menugroup_editContext, editable);
            menu.setGroupVisible(R.id.menugroup_editContext, editable);

            viewContextItem.setVisible(!editable);
            viewContextItem.setEnabled(!editable);
        }

        MenuItem selectItem = menu.findItem(R.id.action_selectContext);
        selectItem.setIcon(!isCurrentContext(selectedCtx)
                ? R.drawable.checkbox_marked_outline
                : R.drawable.checkbox_blank_outline);

        return changed;
    }

    boolean isCurrentContext(PhotoContext ctx) {
        Long ctxId = contextIdSelector.getEntity();
        if(ctxId != null && ctx != null){
            return ctxId.equals(ctx.getId());
        }
        return false;
    }

    @Override
    public void onStartSelection() {
    }

    @Override
    public void onItemSelectionChanged(int selectionCount, int position, long id, boolean checked) {
        if (getActionMode() != null) {
            getActionMode().invalidate();
        }
    }

    @Override
    public void onEndSelection() {
    }

    private boolean userHasEditPermission(PhotoContext selectedCtx) {
        boolean editable = false;
        if (selectedCtx != null && selectedCtx instanceof PhotoContextWithPermission) {
            PhotoContextWithPermission ctxP = (PhotoContextWithPermission) selectedCtx;
            editable = ctxP.getPermission() != null
                    && ctxP.getPermission().getLevel() == PermissionLevel.Admin;
        }
        return editable;
    }

    /* *********************************************************************************************/

    private class ShowContextDetailsCommand extends BaseStatelessCommand<PhotoContext> {
        ReloadContextsResultsListener.ReloadListener reloadListener;
        BaseActivity activity;

        PhotoContextsFragment contextsFragment;
        PhotoContextDetailsController detailsController;

        public ShowContextDetailsCommand(PhotoContextsFragment contextsFragment, PhotoContextDetailsController detailsController, ReloadContextsResultsListener.ReloadListener reloadListener) {
            this.reloadListener = reloadListener;
            this.contextsFragment = contextsFragment;
            this.detailsController = detailsController;
            this.activity = detailsController.getActivity();
        }

        @Override
        public void execute(ResultListener<? super PhotoContext> cmdResultListener) {
            showContextDetail(cmdResultListener);
        }


        private void showContextDetail(ResultListener<? super PhotoContext> resultListener) {
            activity.getSupportFragmentManager().beginTransaction().hide(contextsFragment).commit();
            final PhotoContext selectedCtx = contextsFragment.getAdapter().getSelectedItem();
            detailsController.setEditable(userHasEditPermission(selectedCtx));
            detailsController.show(selectedCtx, resultListener);
            detailsController.setOnFinishListener(new PhotoContextComposeFragment.OnFinishListener() {
                @Override
                public void onFinish(PhotoContextComposeFragment fragment) {
                    activity.getSupportFragmentManager().beginTransaction()
                            .hide(fragment)
                            .show(contextsFragment)
                            .commit();
                }
            });
        }
    }

    private class ChangePhotoContextCommand extends BaseStatelessCommand<PhotoContext> {
        private final ItemSelector<PhotoContext> contextSelector;
        private final EntitySelector<Long> ctxIdSelector;

        public ChangePhotoContextCommand(ItemSelector<PhotoContext> contextSelector, EntitySelector<Long> ctxIdSelector) {
            this.contextSelector = contextSelector;
            this.ctxIdSelector = ctxIdSelector;
        }

        @Override
        public void execute(ResultListener<? super PhotoContext> cmdResultListener) {
            PhotoContext ctx = contextSelector.getSelectedItem();

            if (ctx != null) {
                ctxIdSelector.select(ctx.getId());
                Listeners.onResult(cmdResultListener, ctx);
            }
        }
    }

    private class NotifyUserOnContextChange extends ActivityResultListener<PhotoContext> {
        public NotifyUserOnContextChange(BaseActivity activity) {
            super(activity);
        }

        @Override
        public void onResult(PhotoContext result) {
            super.onResult(result);
            Log.d(getClass().getSimpleName(), "Notify changed context");
            getActivity().showUserMessage(R.string.notification_changedPhotoContext);
        }
    }
}
