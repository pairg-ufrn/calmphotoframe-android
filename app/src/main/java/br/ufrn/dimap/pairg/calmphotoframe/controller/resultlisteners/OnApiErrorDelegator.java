package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import android.os.Handler;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;

public class OnApiErrorDelegator<T> extends CommonListenerDelegator<T>
{
    private Set<Integer> errorCodes;

	public OnApiErrorDelegator(ResultListener<? super T> delegate, Integer ... errorCodes) {
		super(delegate);
        this.errorCodes = new HashSet<>();
        for(Integer errCode : errorCodes){
            this.errorCodes.add(errCode);
        }
	}

    @Override
    protected boolean interceptFailure(Throwable error) {
        return isCausedByApiErrors(error);
    }

    protected  boolean isCausedByApiErrors(Throwable error){
        ApiException ex = ExceptionUtils.getCause(error, ApiException.class);
        return (ex != null && errorCodes.contains(ex.getStatusCode()));
    }
}