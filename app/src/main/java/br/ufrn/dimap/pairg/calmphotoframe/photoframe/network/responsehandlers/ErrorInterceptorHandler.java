package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class ErrorInterceptorHandler extends AsyncResponseHandlerDelegator {
	public interface ErrorListener{
		void onError(int statusCode, Header[] headers, byte[] responseBody, Throwable error);
	}
	
	private ErrorListener errorListener;

	public ErrorInterceptorHandler(AsyncHttpResponseHandler delegator) {
		this(delegator, null);
	}
	public ErrorInterceptorHandler(AsyncHttpResponseHandler delegator, ErrorListener listener) {
		super(delegator);
		this.errorListener = listener;
	}
	
	public ErrorListener getErrorListener() {
		return errorListener;
	}
	public void setErrorListener(ErrorListener errorListener) {
		this.errorListener = errorListener;
	}
	@Override
	public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
		if(errorListener != null){
			errorListener.onError(statusCode, headers, responseBody, error);
		}
		super.onFailure(statusCode, headers, responseBody, error);
	}
	
}
