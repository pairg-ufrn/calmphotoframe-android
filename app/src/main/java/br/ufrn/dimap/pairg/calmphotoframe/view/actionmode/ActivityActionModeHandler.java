package br.ufrn.dimap.pairg.calmphotoframe.view.actionmode;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;

public class ActivityActionModeHandler implements ActionModeHandler {
	AppCompatActivity activity;

	public ActivityActionModeHandler(AppCompatActivity activity) {
		this.activity = activity;
	}

	@Override
	public ActionMode startActionMode(ActionMode.Callback callback) {
		return activity.startSupportActionMode(callback);
	}
}
