package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/** adaptado de: http://stackoverflow.com/a/26196831
 * */
public class RecyclerItemClickListener extends GestureDetector.SimpleOnGestureListener
        implements RecyclerView.OnItemTouchListener
{

    private GestureDetectorCompat mGestureDetector;
    private RecyclerView recyclerView;

    private final List<OnItemClickListener> clickItemListeners;
    private final List<OnItemLongClickListener> longClickItemsListeners;

    public RecyclerItemClickListener(Context context) {
        mGestureDetector = new GestureDetectorCompat(context, this);
        clickItemListeners = new ArrayList<>();
        longClickItemsListeners = new ArrayList<>();
    }

    public void attachToView(RecyclerView recyclerView){
        if(this.recyclerView != null){
            this.recyclerView.removeOnItemTouchListener(this);
        }
        this.recyclerView = recyclerView;
        if(this.recyclerView != null) {
            this.recyclerView.addOnItemTouchListener(this);
        }
    }

    public void addItemClickListener(OnItemClickListener listener){
        clickItemListeners.add(listener);
    }
    public void removeItemClickListener(OnItemClickListener listener){
        clickItemListeners.remove(listener);
    }

    public void addItemLongClickListener(OnItemLongClickListener listener){
        longClickItemsListeners.add(listener);
    }
    public void removeItemLongClickListener(OnItemLongClickListener listener){
        longClickItemsListeners.remove(listener);
    }

    @Override public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return mGestureDetector.onTouchEvent(e);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        ItemTouchEvent evt = getItemTouchEvent(e);
        if(clickItemListeners.isEmpty() || evt == null){
            return false;
        }

        for(OnItemClickListener listener : this.clickItemListeners){
            listener.onItemClick(evt.childView, evt.position, evt.itemId);
        }

        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        ItemTouchEvent evt = getItemTouchEvent(e);
        if(longClickItemsListeners.isEmpty() || evt == null){
            return;
        }

        for(OnItemLongClickListener listener : this.longClickItemsListeners){
            listener.onItemLongClick(evt.childView, evt.position, evt.itemId);
        }
    }

    protected ItemTouchEvent getItemTouchEvent(MotionEvent e){
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if(childView == null){
            return  null;
        }

        int position = recyclerView.getChildAdapterPosition(childView);
        long itemId = recyclerView.getAdapter().getItemId(position);

        return new ItemTouchEvent(childView, position, itemId);
    }

    @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

    @Override
    public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}

    static class ItemTouchEvent{
        public View childView;
        public int position;
        public long itemId;

        public ItemTouchEvent(View childView, int position, long itemId) {
            this.childView = childView;
            this.position = position;
            this.itemId = itemId;
        }
    }
}
