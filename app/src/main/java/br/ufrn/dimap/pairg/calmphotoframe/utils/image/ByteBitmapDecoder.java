package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

public class ByteBitmapDecoder implements BitmapDecoder{
	private byte[] data;
	public ByteBitmapDecoder(byte[] data) {
		this.data = data;
	}
	@Override
	public Bitmap decodeBitmap(Options options) {
	    return BitmapFactory.decodeByteArray(data, 0, data.length, options);
	}
	@Override
	public void clear() {
		data = null;
	}
}