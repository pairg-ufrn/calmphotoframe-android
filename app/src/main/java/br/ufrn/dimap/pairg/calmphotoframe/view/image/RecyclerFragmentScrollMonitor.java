package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoaderScrollableDecorator;
import br.ufrn.dimap.pairg.calmphotoframe.view.ViewConfigurator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ListenerDelegator;

/**
 */
public class RecyclerFragmentScrollMonitor
        extends ListenerDelegator<ImageLoaderScrollableDecorator.ScrollObserver>
        implements ImageLoaderScrollableDecorator.ScrollMonitor,
        ViewConfigurator
{
    private final ItemsRecycleFragment frag;
    private int oldScrollState = RecyclerView.SCROLL_STATE_IDLE;

    public RecyclerFragmentScrollMonitor(ItemsRecycleFragment frag) {
        this.frag = frag;
        if(getRecyclerView() == null) {
            frag.addViewConfigurator(this);
        }
        else{
            attachToRecyclerView();
        }
    }

    @Override
    public boolean isScrolling() {
        return hasRecyclerView() && isScrollingState(getScrollState());
    }

    protected int getScrollState() {
        return getRecyclerView().getScrollState();
    }

    protected RecyclerView getRecyclerView() {
        return frag.getRecyclerView();
    }

    protected boolean hasRecyclerView() {
        return getRecyclerView() != null;
    }

    @Override
    public void addScrollObserver(ImageLoaderScrollableDecorator.ScrollObserver observer) {
        super.addListener(observer);
    }

    @Override
    public void removeScrollObserver(ImageLoaderScrollableDecorator.ScrollObserver observer) {
        super.removeListener(observer);
    }

    protected void notifyScrollStateChanged(int newState){
        if(isScrollingState(oldScrollState) != isScrollingState(newState)){
            boolean startScroll = isScrollingState(newState);

            for(ImageLoaderScrollableDecorator.ScrollObserver observer : super.delegates){
                switch (newState){
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        observer.onStartScroll();
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        observer.onFinishScroll();
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        observer.onStoppingScroll();
                        break;
                }
            }
        }

        oldScrollState = newState;
    }

    private boolean isScrollingState(int scrollState) {
        return scrollState != RecyclerView.SCROLL_STATE_IDLE;
//        return scrollState == RecyclerView.SCROLL_STATE_DRAGGING;
    }

    @Override
    public void configureView(View rootView) {
        attachToRecyclerView();
    }

    protected void attachToRecyclerView() {
        if (getRecyclerView() != null) {
            oldScrollState = getScrollState();
            getRecyclerView().addOnScrollListener(new InnerOnScrollListener());
        }
    }

    private class InnerOnScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            notifyScrollStateChanged(newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            Log.d(getClass().getSimpleName(), "Scrolled: " + dx + ", " + dy);
        }
    }

}
