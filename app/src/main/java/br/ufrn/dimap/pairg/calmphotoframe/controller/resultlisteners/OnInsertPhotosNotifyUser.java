package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class OnInsertPhotosNotifyUser extends ActivityResultListener<Collection<Photo>> {

	public OnInsertPhotosNotifyUser(BaseActivity activity) {
		super(activity);
	}
	@Override
	public void onStart() {
		getActivity().notifyUser().text(R.string.notification_insertPhoto_Start).show();
	}

	@Override
	public void onResult(Collection<Photo> result) {
		CharSequence text =
				getActivity().getResources().getQuantityString(R.plurals.photoInsertionResult, result.size());
		getActivity().notifyUser(String.format(text.toString(), result.size())).show();
	}

	@Override
	public void onFailure(Throwable error) {
		error.printStackTrace();

        if(error.getLocalizedMessage() != null){
			getActivity().notifyUser(error.getLocalizedMessage()).show();
		}
		else{
			getActivity().notifyUser().text(R.string.notification_insertPhoto_Error_GenericError).show();
		}
	}
}