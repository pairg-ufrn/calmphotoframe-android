package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPParser {
    static String bytePattern = "[1-2]?[1-9]?\\d";
    static String ipv4Regex = String.format("(?:(%s)\\.){3}(%s)", bytePattern, bytePattern);
    static String ipv6Map4Regex_prefix = "((0{1,4}:){5}|::)ffff:";
    static String ipv6Map4Regex = ipv6Map4Regex_prefix + ipv4Regex;

    static Pattern enclosed = Pattern.compile("\\[([^\\[\\]]+)\\]");

    public static InetAddress parse(String addr) {
        byte[] bytes = parseToBytes(addr);
        try {
            return bytes == null ? null : InetAddress.getByAddress(bytes);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] parseToBytes(String addr) {
        addr = addr.trim().toLowerCase();

        if (addr.matches(ipv4Regex)) {
            return parseIpv4(addr);
        } else {
            Matcher m = enclosed.matcher(addr);
            if(m.matches()){
                addr = m.group(1);
            }
            if (addr.matches(ipv6Map4Regex)) {
                return parseIpv6Map4(addr);
            } else {
                return parseIpv6(addr);
            }
        }
    }

    private static byte[] parseIpv6Map4(String addr) {
        byte[] result = new byte[16];

        ;

        String ipv4Part = addr.replaceFirst(ipv6Map4Regex_prefix, "");

        System.out.println(ipv4Part);

        byte[] ipv4Addr = parseIpv4(ipv4Part);

        for (int i = 0; i < ipv4Addr.length; ++i) {
            result[result.length - 1 - i] = ipv4Addr[ipv4Addr.length - 1 - i];
        }

        result[result.length - ipv4Addr.length - 1] = (byte) 0xFF;
        result[result.length - ipv4Addr.length - 2] = (byte) 0xFF;

        return result;
    }

    private static byte[] parseIpv6(String addr) {

        if (addr.equals("::")) {
            return new byte[16];
        }

        String[] parts = addr.split("::");
        if (parts.length > 2) {
            return null;
        }

        String[][] segments = new String[parts.length][];
        for (int i = 0; i < parts.length; ++i) {
            segments[i] = parts[i].split(":");
        }

        try {
            List<Byte> firstBytes = extractBytes(segments[0]);

            byte[] result = new byte[16];

            for (int i = 0; i < firstBytes.size(); ++i) {
                result[i] = firstBytes.get(i);
            }

            if (segments.length == 2) {
                List<Byte> lastBytes = extractBytes(segments[1]);

                if (lastBytes.size() + firstBytes.size() > result.length) {
                    return null;
                }

                int startIdx = result.length - lastBytes.size();
                for (int i = 0; i < lastBytes.size(); ++i) {
                    result[i + startIdx] = lastBytes.get(i);
                }
            }

            return result;

        } catch (NumberFormatException ex) {
            return null;
        }
    }

    private static List<Byte> extractBytes(String[] values) {
        List<Byte> bytes = new ArrayList<>();

        for (String group : values) {
            Integer num = group.isEmpty() ? 0 : Integer.valueOf(group, 16);

            int upper = (num & 0xFF00) >> 8;
            int down = (num & 0xFF);

            bytes.add((byte) upper);
            bytes.add((byte) down);
        }

        return bytes;
    }

    private static byte[] parseIpv4(String address) {
        String[] parts = address.split("\\.");
        if (parts.length != 4) {
            return null;
        }

        try {

            byte[] addr = new byte[4];
            for (int i = 0; i < parts.length; ++i) {
                addr[i] = (byte) Integer.parseInt(parts[i]);
            }
            return addr;
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
