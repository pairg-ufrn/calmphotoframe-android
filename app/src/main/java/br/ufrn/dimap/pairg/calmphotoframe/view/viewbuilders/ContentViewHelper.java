package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class ContentViewHelper {

	public static String getContextName(Context viewContext, PhotoContext photoContext) {
		String ctxName = photoContext == null ? null : photoContext.getName();
		if(ctxName == null || ctxName.isEmpty()){
			ctxName = viewContext.getText(R.string.default_ContextName).toString();
		}
		return ctxName;
	}
	public static String getCollectionName(Context viewContext, PhotoCollection photoCollection) {
		String collName = photoCollection == null ? null : photoCollection.getName();
		
		return collName;
	}
	public static String getUserName(Context context, User user){
		return isLocalUser(user) ? context.getString(R.string.local_username) : user.getLogin();
	}
	private static boolean isLocalUser(User user) {
		//TODO: determinar de forma mais precisa quando usuário é local
		return user.getLogin().isEmpty();
	}
	
	public static boolean hasDefaultName(PhotoContext photoContext) {
		return photoContext == null || photoContext.getName() == null || photoContext.getName().isEmpty();
	}
}
