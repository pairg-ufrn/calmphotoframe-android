package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

import android.view.View;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemLongClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnItemClickDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnItemLongClickDelegator;

/**
 */
public class ItemsSelectionManager implements SelectionTracker, OnItemClickListener, OnItemLongClickListener{

    public interface ItemsView{
        void addOnItemClickListener(OnItemClickListener listener);
        void removeOnItemClickListener(OnItemClickListener listener);
        void addOnItemLongClickListener(OnItemLongClickListener listener);
        void removeOnItemLongClickListener(OnItemLongClickListener listener);

        long getIdFromPosition(int position);
        int getViewCount();
        @Deprecated
        /*this method should be removed, because may not be possible to implement it correctly
         * (due to views being recycled)
         */
        View getViewAt(int position);

        void notifyItemChanged(int position);
    }

    private final Set<Long> selectedIds;
    private Set<MultiSelectionListener> listeners;

    private ItemsView itemsView;

    private boolean notifyClickOnSelection;

    /** If set allows more than one item selected. */
    private boolean multiSelectionEnabled;

    /** If set to false, does not allow any selection. */
    private boolean selectionEnabled;

    /** If true, enable the use of single click to (de)select an item.*/
    private boolean selectOnClick;

    /** If true, does not select Views that are not enabled (getEnabled() == false)*/
    private boolean ignoreDisabledViews;

    OnItemClickDelegator itemClickDelegator = new SelectorClickItemDelegator();
    OnItemLongClickDelegator itemLongClickDelegator = new SelectorLongClickItemDelegator();

    public ItemsSelectionManager() {
        selectionEnabled = true;
        multiSelectionEnabled = true;
        ignoreDisabledViews = true;
        notifyClickOnSelection = false;
        selectedIds = new HashSet<>();
        listeners = new HashSet<>();
    }

    public ItemsView getItemsView(){
        return itemsView;
    }
    public void setItemsView(ItemsView itemsView){
        dettachItemsView();
        attachItemsView(itemsView);
    }

    private void attachItemsView(ItemsView itemsView){
        this.itemsView = itemsView;
        if(this.itemsView != null){
            this.itemsView.addOnItemClickListener(getItemClickDelegator());
            this.itemsView.addOnItemLongClickListener(getItemLongClickDelegator());
        }
    }

    private void dettachItemsView(){
        if(this.itemsView != null){
            this.deselectAll();
            this.itemsView.removeOnItemClickListener(getItemClickDelegator());
            this.itemsView.removeOnItemLongClickListener(getItemLongClickDelegator());
        }
    }

    public OnItemClickDelegator getItemClickDelegator(){
        return itemClickDelegator;
    }
    public OnItemLongClickDelegator getItemLongClickDelegator(){
        return itemLongClickDelegator;
    }

    @Override
    public void addSelectionListener(MultiSelectionListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void removeSelectionListener(MultiSelectionListener listener) {
        this.listeners.remove(listener);
    }

    public boolean isNotifyingClickOnSelection() { return notifyClickOnSelection;}
    public void setNotifyClickOnSelection(boolean notifyClickOnSelection) {
        this.notifyClickOnSelection = notifyClickOnSelection;
    }

    public boolean isSelectingOnClick()                 { return selectOnClick;}
    public void setSelectOnClick(boolean selectOnClick) {
        this.selectOnClick = selectOnClick;
    }

    @Override
    public boolean isSelectionEnabled() {
        return selectionEnabled;
    }
    @Override
    public void setSelectionEnabled(boolean selectionEnabled) {
        this.selectionEnabled = selectionEnabled;
    }

    public int getSelectionCount() {
        return selectedIds.size();
    }

    public boolean isMultiSelectionEnabled() {
        return multiSelectionEnabled;
    }
    public void setMultiSelectionEnabled(boolean enabled) {
        if(enabled != isMultiSelectionEnabled()){
            this.multiSelectionEnabled = enabled;
            if(!enabled){
                deselectAll();
            }
        }
    }

    public boolean getIgnoreDisabledViews() {
        return ignoreDisabledViews;
    }
    public void setIgnoreDisabledViews(boolean ignoreDisabledViews) {
        this.ignoreDisabledViews = ignoreDisabledViews;
    }

    public void deselectAll() {
        if(isSelecting()){
            doDeselectAll();

            notifyFinishSelection();
        }
    }

    protected void checkItem(View view, int position, long id) {
        if(!isSelectionEnabled() || isIgnored(view)){
            return;
        }

        boolean wasSelecting = selectedIds.size() > 0;
        if(!wasSelecting){
            notifyStartSelection();
        }

        doSelection(id, position, view);

        if(wasSelecting && getSelectionCount() == 0){
            notifyFinishSelection();
        }
    }

    private void doSelection(long id, int position, View view) {
        if(isSingleMode() && !isSelected(id)){
            doDeselectAll();
        }

        boolean checked = toggleSelection(id);
        if(view != null) {
            //FIXME: remove dependency with view if possible, ItemSelectionManager should not be
            //responsible for change views (because may not be possible to an adapterView or
            //recycleView to keep all views visible)
            view.setSelected(checked);
        }

        notifyOnItemSelectionChanged(position, id, checked);
    }

    private void doDeselectAll() {
        if(itemsView != null){
            for(int i=0; i < itemsView.getViewCount(); ++i){
                long itemId = itemsView.getIdFromPosition(i);
                if(selectedIds.contains(itemId)){
                    View v = itemsView.getViewAt(i);
                    doSelection(itemId, i, v);
                }
            }
        }
        selectedIds.clear();
    }

    private boolean isSelected(long id) {
        return this.selectedIds.contains(id);
    }

    private boolean isSingleMode() {
        return !isMultiSelectionEnabled();
    }

    /** If item with this {@code id} is selected, deselect it, otherwise select it.
     * @return true if item is now selected
     * */
    private boolean toggleSelection(long id) {
        if(selectedIds.contains(id)){
            selectedIds.remove(id);
            return false;
        }
        else{
            selectedIds.add(id);
            return true;
        }
    }

    protected void notifyOnItemSelectionChanged(int position, long id, boolean checked) {
        int selectionCount = selectedIds.size();

        for(MultiSelectionListener listener : this.listeners){
            listener.onItemSelectionChanged(selectionCount, position, id, checked);
        }
    }

    protected void notifyStartSelection() {
        for (MultiSelectionListener listener: listeners) {
            listener.onStartSelection();
        }
    }

    protected void notifyFinishSelection() {
        for (MultiSelectionListener listener: listeners) {
            listener.onEndSelection();
        }
    }

    public boolean isSelecting() {
        return getSelectionCount() > 0;
    }

    public boolean isChecked(long itemId) {
        return selectedIds.contains(itemId);
    }

    @Override
    public View getView(int position) {
        return getItemsView().getViewAt(position);
    }

    @Override
    public void notifyChangedPosition(int position) {
        getItemsView().notifyItemChanged(position);
    }

    @Override
    public void onItemClick(View view, int position, long id) {
        if(isSelecting() || isSelectingOnClick()){
            checkItem(view, position, id);
        }
    }

    @Override
    public void onItemLongClick(View view, int position, long itemId) {
        checkItem(view, position, itemId);
    }

    protected boolean allowClickDelegate(View viewItem){
        return !isIgnored(viewItem) && (!isSelecting() || isNotifyingClickOnSelection());
    }

    public Collection<Long> getSelectedIds() {
        return this.selectedIds;
    }

    protected boolean isIgnored(View view) {
        return getIgnoreDisabledViews() && !view.isEnabled();
    }

    /* *******************************************************************************************/


    private class SelectorClickItemDelegator extends OnItemClickDelegator {
        @Override
        public void onItemClick(View view, int position, long itemId) {
            if(allowClickDelegate(view)){
                super.onItemClick(view, position, itemId);
            }
            ItemsSelectionManager.this.onItemClick(view, position, itemId);
        }
    }
    private class SelectorLongClickItemDelegator extends OnItemLongClickDelegator {
        @Override
        public void onItemLongClick(View view, int position, long itemId) {
            boolean wasSelected = isSelecting();
            if(wasSelected){
                //If already was selected, call the delegate before any change happens
                super.onItemLongClick(view, position, itemId);
            }
            ItemsSelectionManager.this.onItemLongClick(view, position, itemId);
            if(!wasSelected){
                //If not was selected, call the delegate after start selection
                super.onItemLongClick(view, position, itemId);
            }
        }
    }
}