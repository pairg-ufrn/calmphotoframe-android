package br.ufrn.dimap.pairg.calmphotoframe.utils;

public class MemoryUtils {
	private static MemoryUtils singleton;
	
	public static MemoryUtils instance(){
		if(singleton == null){
			singleton = new MemoryUtils();
		}
		return singleton;
	}
	public static void setInstance(MemoryUtils instance){
		singleton = instance;
	}
	
	public static int defaultImageCacheSize() {
		return instance().getDefaultImageCacheSize();
	}
	public static long maxAvailableMemory() {
		return instance().getMaxAvailableMemory();
	}
	
	// Returns a cache size based on available memory
	public int getDefaultImageCacheSize() {
		final int maxMemory = (int) (maxAvailableMemory());
        final int cacheSize = maxMemory / 6;

        return cacheSize;
	}
	public long getMaxAvailableMemory() {
		return Runtime.getRuntime().maxMemory();
	}
}
