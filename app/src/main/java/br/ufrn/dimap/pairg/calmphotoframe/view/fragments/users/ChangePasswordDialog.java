package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users;

import android.support.design.widget.TextInputLayout;
import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;

public class ChangePasswordDialog extends CreateAccountFragment {


    @Override
    protected int getTitleId() {
        return R.string.users_changepassword_title;
    }

    @Override
    protected int getButtonTextResource() {
        return R.string.generic_confirm;
    }

    @Override
    protected void configureViews(View rootView) {
        super.configureViews(rootView);

        hideLogin();
        getCancelButton().setVisibility(View.VISIBLE);
        getCancelButton().setText(R.string.generic_cancel);

        setCloseOnSubmit(true);
        setClearOnClose(true);
    }

    private void hideLogin() {
        TextInputLayout container = loginValidator.getLoginContainer();

        View loginView = (container != null ? container : getLoginEditor());
        if(loginView != null) {
            loginView.setVisibility(View.GONE);
        }
    }

    @Override
    protected boolean verifyLogin(LoginInfo loginInfo) {
        return verifyPassword() && verifyPasswordConfirmation();
    }

    public boolean verifyPassword() {
        return loginValidator.checkPassword();
    }
    public boolean verifyPasswordConfirmation() {
        return loginValidator.checkPassConfirmation();
    }
}
