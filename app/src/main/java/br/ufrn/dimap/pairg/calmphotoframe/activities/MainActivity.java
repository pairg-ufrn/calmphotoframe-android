package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photos.FullscreenSetup;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photos.PhotoCommandsFactory;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photos.PhotoViewImpl;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.GetPhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnResultInvalidateActivityMenu;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Permission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator.FragmentBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo.PhotoViewerFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen.FullscreenLayout;

public class MainActivity extends ConnectedActivity {
	private static final String PHOTOVIEWER_TAG = "PHOTOVIEWER_TAG";

	private PhotoViewImpl photoView;
	
	private FragmentCreator fragmentCreator;
	
	public MainActivity(){
		super();
		photoView = new PhotoViewImpl();
		
	    fragmentCreator = new FragmentCreator(getSupportFragmentManager());
	}
	
	/* *********************************** Lifecycle Callbacks *******************************/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_main);
        setSnackbarViewId(R.id.root_view);
		setupToolbar();
		setupViews();
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		FullscreenLayout fullscreenLayout  =((FullscreenLayout)findViewById(R.id.root));
        if(fullscreenLayout != null){
            fullscreenLayout.fadeToFull(getAutoHideDelay());
        }
	}

	private int getAutoHideDelay() {
		return getResources().getInteger(R.integer.start_fullscreen_autohidelay);
	}

	@Override
	protected void loadData() {
		new GetPhotoCommand(photoView).execute(new OnResultInvalidateActivityMenu(this));
	}

    /* *****************************************************************************************/
	private void setupViews() {
        setupFragments();
        FullscreenSetup.setupFullscreen(this, R.id.root);
	}
	
	void setupFragments(){
		PhotoViewerFragment photoViewFragment = fragmentCreator.addFragment(
				R.id.fullscreen_content, PHOTOVIEWER_TAG, new FragmentBuilder() {
			@Override
			public Fragment buildFragment() {
				return new PhotoViewerFragment();
			}
		});
		photoView.setPhotoViewFragment(photoViewFragment);
	}

	/* *****************************************************************************************/
    @Override
    protected int getMenuId() {
    	return R.menu.main_activity_menu;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        boolean canEditPhoto = canEditPhoto();
    	menu.setGroupVisible(R.id.actiongroup_photoedition, canEditPhoto);
    	menu.setGroupVisible(R.id.actiongroup_photoexport, canEditPhoto);

        updateTitle(canEditPhoto);

    	return true;
    }

    private void updateTitle(boolean canEditPhoto) {
        StringBuilder builder = new StringBuilder();
        builder.append(getText(R.string.title_activity_main));
        if(hasPhoto() && !canEditPhoto){
            builder.append(" ");
            builder.append(getText(R.string.NonEditable));
        }
        setTitle(builder);
    }

    private boolean canEditPhoto() {
        if(!hasPhoto()){
           return false;
        }
        Photo currentPhoto = photoView.getCurrentPhoto();
        if(! (currentPhoto instanceof  PhotoWithPermission)){
            return true;
        }

        Permission p = ((PhotoWithPermission) currentPhoto).getPermission();
        return p == null || p.getLevel().ordinal() >= PermissionLevel.Write.ordinal();
    }

    private boolean hasPhoto() {
		Photo currentPhoto = photoView.getCurrentPhoto();
		return currentPhoto != null && currentPhoto.getId() != null;
	}
    
	@Override
	protected void createMenuCommands(CommandExecutor cmdExecutor) {
		PhotoCommandsFactory.createCommands(cmdExecutor, this, photoView);
	}
}
