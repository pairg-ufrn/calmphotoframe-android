package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import com.loopj.android.http.ResponseHandlerInterface;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ObjectMapperResponseHandler;

public class GetPhotoCollectionRequest extends CommonGenericRequest<PhotoCollection>{
	private Long photoCollectionId;
	private boolean expandPhotoCollection;
	
	public GetPhotoCollectionRequest(ApiConnector apiConnector, Long photoCollectionId
			, ResultListener<? super PhotoCollection> listener) {
		this(apiConnector, photoCollectionId, listener, false);
	}
	public GetPhotoCollectionRequest(ApiConnector apiConnector, Long photoCollectionId
			, ResultListener<? super PhotoCollection> listener, boolean expandPhotoCollection) {
		super(apiConnector, PhotoCollection.class);
		this.photoCollectionId = photoCollectionId;
		this.listener = listener;
		this.expandPhotoCollection = expandPhotoCollection;
	}

	public Long getPhotoCollectionId() {
		return photoCollectionId;
	}
	protected void setPhotoCollectionId(Long photoCollectionId) {
		this.photoCollectionId = photoCollectionId;
	}
	public boolean getExpandPhotoCollection() {
		return expandPhotoCollection;
	}
	
	@Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	public Class<? extends PhotoCollection> getEntityClass() {
		return expandPhotoCollection ? ExpandedPhotoCollection.class : super.getEntityClass();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().collectionUrl(photoCollectionId, expandPhotoCollection);
	}
}
