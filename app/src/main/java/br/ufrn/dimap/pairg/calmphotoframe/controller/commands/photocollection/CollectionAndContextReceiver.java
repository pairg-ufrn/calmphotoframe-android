package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ComposedException;

public class CollectionAndContextReceiver{
	private ResultListener<? super CollectionAndContextContainer> listener;
	private final CollectionAndContextContainer container;
	
	private Throwable collectionFailure, contextFailure;
	
	public CollectionAndContextReceiver(
			ResultListener<? super CollectionAndContextContainer> listener) 
	{
		this.listener = listener;
		container = new CollectionAndContextContainer();
	}
	public synchronized void putCollection(PhotoCollection collection){
		this.container.setPhotoCollection(collection);
		verifyFinish();
	}
	public synchronized void putContext(PhotoContext context){
		container.setPhotoContext(context);
		verifyFinish();
	}
	
	public synchronized void onCollectionFailure(Throwable error) {
		this.collectionFailure = error;
		verifyFinish();
	}
	public synchronized void onContextFailure(Throwable error) {
		this.contextFailure = error;
		verifyFinish();
	}
	
	public synchronized boolean hasFinished(){
		return  (hasContext() || contextHasFailed()) && (hasCollection() || collectionHasFailed());
	}
	public synchronized boolean hasCollection() {
		return container.getPhotoCollection() != null  
				&& container.getPhotoCollection() instanceof ExpandedPhotoCollection;
	}
	public synchronized boolean hasContext() {
		return container.getPhotoContext() != null;
	}
	public synchronized boolean collectionHasFailed() {
		return collectionFailure != null;
	}
	public synchronized boolean contextHasFailed() {
		return contextFailure != null;
	}
	
	public synchronized void clear(){
		container.setPhotoCollection(null);
		container.setPhotoContext(null);
		collectionFailure = null;
		contextFailure = null;
	}

	protected void verifyFinish() {
		if(hasFinished()){
			Throwable error = null;
			if(collectionHasFailed() && contextHasFailed()){
				error = new ComposedException("Failed to load PhotoContext and PhotoCollection"
														, collectionFailure, contextFailure);
			}
			else if(collectionHasFailed() || contextHasFailed()){
				error = collectionHasFailed() ? collectionFailure : contextFailure;
			}
			
			if(error != null){
				Listeners.onFailure(listener, error);
			}
			else{
				Listeners.onResult(listener, container);
			}
		}
	}
}