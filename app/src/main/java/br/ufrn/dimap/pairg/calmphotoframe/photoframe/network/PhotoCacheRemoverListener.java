package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;

public class PhotoCacheRemoverListener extends BasicResultListener<Photo> {
	private final PhotoCache photoCache;
	private final ResultListener<? super Photo> removeListener;

	public PhotoCacheRemoverListener(PhotoCache photoCache, ResultListener<? super Photo> removeListener) {
		this.photoCache = photoCache;
		this.removeListener = removeListener;
	}

	@Override
	public void onResult(Photo photo) {
		photoCache.remove(photo.getId());
		LocalPersistence.instance().removePhoto(photo.getId());
		
		Listeners.onResult(removeListener, photo);
	}
	@Override
	public void onFailure(Throwable error) {
		Listeners.onFailure(removeListener, error);
	}
}