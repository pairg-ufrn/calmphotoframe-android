package br.ufrn.dimap.pairg.calmphotoframe.photoframe.image;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.annotation.Nullable;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMetadata;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoOrientation;

import static android.R.attr.bitmap;

public class ImageOrientationHelper {
	public static Bitmap fixImage(Photo photo, Bitmap bitmap) {
		return (photo == null) ? bitmap : fixImage(photo.getMetadata(), bitmap);
	}

	public static Bitmap fixImage(PhotoMetadata photoMetadata, Bitmap bitmap) {
        return photoMetadata == null ? bitmap : fixImage(photoMetadata.getOrientation(), bitmap);
	}

	public static Bitmap fixImage(PhotoOrientation orientation, Bitmap bitmap) {
		if(orientation == null){
			return bitmap;
		}

		if(orientation.isFlipped() ||
                (orientation.getClockwiseOrientation() != 0
                && orientation.getClockwiseOrientation() != 360))
        {

            float rotation = -orientation.getClockwiseOrientation();

            Bitmap rotatedBitmap = rotateBitmap(bitmap, rotation, orientation.isFlipped());
            return rotatedBitmap;
        }

		return bitmap;
	}

	static private Bitmap rotateBitmap(Bitmap bitmap, float rotation, boolean flip) {
		Matrix matrix = new Matrix();
		//matrix.preScale(-1, 1);
		matrix.preRotate(rotation);
		if(flip){
			matrix.postScale(-1, 1);
		}
		
		Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap .getHeight(), matrix, true);
		return rotatedBitmap;
	}
}
