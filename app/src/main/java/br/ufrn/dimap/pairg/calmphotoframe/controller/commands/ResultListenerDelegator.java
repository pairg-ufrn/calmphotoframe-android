package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public abstract class ResultListenerDelegator<ResultType, DelegateType> extends BasicResultListener<ResultType>{
	private ResultListener<? super DelegateType> delegate;
	
	public ResultListenerDelegator(ResultListener<? super DelegateType> delegate) {
		this.delegate = delegate;
	}
	
	public ResultListener<? super DelegateType> getDelegate(){
		return delegate;
	}

	@Override
	public void onStart() {
		Listeners.onStart(delegate);
	}

	@Override
	public void handleResult(ResultType result) {
		delegateResult(result);
	}

	@Override
	protected void handleError(Throwable error) {
		delegateFailure(error);
	}


	@Override
	public boolean isListening() {
		return Listeners.isListening(delegate);
	}
	
	protected void delegateResult(ResultType result) {
		DelegateType convertedResult = convertResult(result);
		if(interceptResult(convertedResult)){
			Listeners.onResult(delegate, convertedResult);
		}
	}
	public void delegateFailure(Throwable error) {
		if(interceptFailure(error)){
			Listeners.onFailure(delegate, error);
		}
	}

	/** Allows subclasses to intercept the converted result.
	 * @return true to delegate result, or false to prevent delegation. */
	protected boolean interceptResult(DelegateType result){
		return true;
	}

	/** Allows subclasses to intercept failures.
	 * @return true to delegate failure, or false to prevent delegation. */
	protected boolean interceptFailure(Throwable error){
		return true;
	}

	protected abstract DelegateType convertResult(ResultType result);
}