package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;

public class OnSaveImageNotifyUser extends ActivityResultListener<Object> {
	
	public OnSaveImageNotifyUser(BaseActivity activity) {
		super(activity);
	}
	@Override
	public void onFailure(Throwable error) {
		getActivity().notifyError(R.string.saveImage_onError_message);
	}
}