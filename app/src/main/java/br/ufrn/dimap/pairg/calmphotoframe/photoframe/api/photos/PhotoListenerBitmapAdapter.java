package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;

public final class PhotoListenerBitmapAdapter extends ResultListenerDelegator<PhotoImageWrapper, Bitmap> {
	public PhotoListenerBitmapAdapter(ResultListener<? super Bitmap> listener) {
		super(listener);
	}

	@Override
	protected Bitmap convertResult(PhotoImageWrapper result) {
		return result.getImage();
	}
}