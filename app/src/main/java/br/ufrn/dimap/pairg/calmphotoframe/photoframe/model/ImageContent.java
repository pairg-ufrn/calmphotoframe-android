package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicReference;

import br.ufrn.dimap.pairg.calmphotoframe.PhotoFrameApplication;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.InvalidContentTypeException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class ImageContent{
    private static AtomicReference<Builder> builderRef = new AtomicReference<>(new Builder());

    public static Builder builder(){
        return builderRef.get();
    }
    public static void builder(Builder imageContentBuilder){
        builderRef.set(imageContentBuilder);
    }

	private InputStream imageStream; 
	private String mimeType;
	private String originalFilepath;
	
	public ImageContent(InputStream imageStream, String mimeType) 
	{
		this(imageStream, mimeType, null);
	}
	public ImageContent(InputStream imageStream, String mimeType,
			String originalFilepath) {
		super();
		this.imageStream = imageStream;
		this.mimeType = mimeType;
		this.originalFilepath = originalFilepath;
	}

	public InputStream getImageStream()    { return imageStream; }
	public String getMimeType()            { return mimeType; }
	public String getOriginalFilepath()    { return originalFilepath; }

	public void setImageStream(InputStream imageStream) {
		this.imageStream = imageStream;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public void setOriginalFilepath(String originalFilepath) {
		this.originalFilepath = originalFilepath;
	}

	public static class Builder{
        @NonNull
        public ImageContent fromString(String imgPathOrUri) throws IOException {

            File imgFile = createFile(imgPathOrUri);
            if(imgFile != null && imgFile.exists()){
                return fromFile(imgFile);
            }
            else{
                Uri uri = Uri.parse(imgPathOrUri);
                return fromUri(uri);
            }
        }

        @NonNull
        public ImageContent fromFile(File imgFile) throws IOException{
            Logging.instance().d(getClass().getSimpleName(),  "build ImageContent from file: " + imgFile);

            InputStream imageStream = openStream(imgFile);
            try{
                String mimeType = IntentUtils.instance().getImageMimeType(openStream(imgFile));

                if(mimeType == null){
                    throw new InvalidContentTypeException("File " + imgFile + " is not a valid image.");
                }

                return new ImageContent(imageStream,mimeType, imgFile.getName());
            }
            catch (Throwable t){
                imageStream.close();
                throw t;
            }
        }

        @NonNull
        public ImageContent fromUri(Uri uri) throws IOException {
            Logging.instance().d(getClass().getSimpleName(),  "build ImageContent from uri: " + uri);

            ContentResolver resolver = getContext().getContentResolver();

            InputStream stream = openInputStream(resolver, uri);
            String mimeType = resolver.getType(uri);

            return new ImageContent(stream, mimeType);
        }

        @NonNull
        public InputStream openInputStream(ContentResolver resolver, Uri uri) throws IOException {
            InputStream in = resolver.openInputStream(uri);

            if(in == null){
                throw new IOException("Failed to open input stream from uri: " + uri);
            }

            return new BufferedInputStream(in);
        }

        protected Context getContext() {
            return PhotoFrameApplication.getContext();
        }

        @NonNull
        private File createFile(String imgPath) {
            return new File(imgPath);
        }

        private InputStream openStream(File imgFile) throws FileNotFoundException {
            return new BufferedInputStream(new FileInputStream(imgFile));
        }
	}
}