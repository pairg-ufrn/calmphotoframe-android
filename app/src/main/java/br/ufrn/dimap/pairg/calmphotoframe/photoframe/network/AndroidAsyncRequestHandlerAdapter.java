package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import com.loopj.android.http.RequestHandle;

public class AndroidAsyncRequestHandlerAdapter implements RequestHandler{
	private RequestHandle requestHandle;
	public AndroidAsyncRequestHandlerAdapter() {
		this(null);
	}
	public AndroidAsyncRequestHandlerAdapter(RequestHandle handle) {
		this.requestHandle = handle;
	}
	public RequestHandle getHandle(){
		return this.requestHandle;
	}
	public void setHandle(RequestHandle handle){
		this.requestHandle = handle;
	}
	@Override
	public boolean isFinished() {
		return requestHandle.isFinished();
	}

	@Override
	public boolean isCancelled() {
		return requestHandle.isCancelled();
	}

	@Override
	public boolean cancel() {
		return requestHandle.cancel(true);
	}

}
