package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import android.content.Context;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class DialogBuilder {
	public static SimpleDialogFragment buildRemoveDialog(Context context, int dialogTitleId){
		SimpleDialogFragment dialogFragment = new SimpleDialogFragment();
		dialogFragment.setOkMessage    (context.getString(R.string.dialog_generic_remove_ok));
    	dialogFragment.setCancelMessage(context.getString(R.string.generic_cancel));
    	dialogFragment.setDialogMessage(context.getString(R.string.dialog_remove_warning));
    	dialogFragment.setDialogTitle  (context.getString(dialogTitleId));
    	
		return dialogFragment;
	}
}
