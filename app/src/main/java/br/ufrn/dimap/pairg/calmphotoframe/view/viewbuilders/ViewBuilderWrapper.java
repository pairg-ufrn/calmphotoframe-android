package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.ViewGroup;

public abstract class ViewBuilderWrapper<Entity> implements ViewBuilder{

	@SuppressWarnings("unchecked")
	@Override
	public View buildView(Object data, View recycleView, ViewGroup parent) {
		BinderViewHolder<Entity> binder;
		if(recycleView == null){
			binder = buildBinder();
			recycleView = binder.buildView(parent, 0);
			binder.setup(recycleView);
			recycleView.setTag(binder);
		}
		else{
			binder = (BinderViewHolder<Entity>)recycleView.getTag();
		}

		binder.bind((Entity)data);
		
		return recycleView;
	}

	protected abstract BinderViewHolder<Entity> buildBinder();
}
