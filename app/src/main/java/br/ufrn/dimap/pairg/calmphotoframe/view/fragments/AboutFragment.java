package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.app.Dialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;

public class AboutFragment extends SimpleDialogFragment {
	public AboutFragment() {
		super();
		this.setLayoutId(R.layout.fragment_about);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		this.setOkMessage(getString(R.string.generic_confirm));
		this.hideCancelButton();
		return super.onCreateDialog(savedInstanceState);
	}
	@Override
	protected View createView(int layoutId, LayoutInflater inflater, ViewGroup container) {
		View aboutView =  super.createView(layoutId, inflater, container);
		
		TextView versionName = (TextView) aboutView.findViewById(R.id.fragmentAbout_versionNameValue);
		
		try {
			PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(
					getActivity().getPackageName(), 0);
			
			versionName.setText(pInfo.versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		return aboutView;
	}
}
