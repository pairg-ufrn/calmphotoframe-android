package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.ProgressListener;
import cz.msebera.android.httpclient.Header;

public class FileResponseHandler extends FileAsyncHttpResponseHandler {

	private ResultListener<? super File> resultListener;
	private ProgressListener progressListener;
    private String sourceUrl;

	public FileResponseHandler(File destPath) {
		super(destPath);
		this.resultListener = null;
		this.progressListener = null;
        this.sourceUrl = null;
	}

    public FileResponseHandler sourceUrl(String url){
        this.sourceUrl = url;
        return this;
    }

    public FileResponseHandler listener(ResultListener<? super File> resultListener){
        this.resultListener = resultListener;
        return this;
    }

    public ProgressListener progressListener(){
        return this.progressListener;
    }
    public FileResponseHandler progressListener(ProgressListener progressListener){
        this.progressListener = progressListener;
        return this;
    }

	@Override
	public void onSuccess(int arg0, Header[] arg1, File file) {
		Listeners.onResult(this.resultListener, file);
		finalizeProgressListener(progressListener, true);
	}

	@Override
	public void onFailure(int statusCode, Header[] arg1, Throwable error, File imageFile) {
		Listeners.onFailure(this.resultListener, getError(statusCode, error));
		finalizeProgressListener(progressListener, false);
		deleteTargetFile(); //TODO: (?) use option to define if file will be deleted
	}

    private Throwable getError(int statusCode, Throwable error) {
        return ApiException.getError(statusCode, "", error, this.sourceUrl);
    }

    @Override
	public void onStart() {
		super.onStart();
		if(hasProgressListener()){
			progressListener.onStart();
		}
        Listeners.onStart(resultListener);
	}

    private Class<? extends FileResponseHandler> getRequestId() {
        return getClass();
    }

    @Override
	public void onProgress(long bytesWritten, long totalSize) {
		super.onProgress(bytesWritten, totalSize);
		if(hasProgressListener()){
			progressListener.onProgress((int)bytesWritten, totalSize);
		}
	}

	private void finalizeProgressListener(ProgressListener progressListener, boolean successful) {
		if(hasProgressListener()){
			progressListener.onFinish(successful);
		}
	}

	private boolean hasProgressListener() {
		return progressListener != null && progressListener.isListening();
	}
}