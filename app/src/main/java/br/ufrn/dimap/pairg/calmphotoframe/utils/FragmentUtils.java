package br.ufrn.dimap.pairg.calmphotoframe.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class FragmentUtils {

	@SuppressWarnings({"unchecked", "unused"})
	public static <T extends Fragment> T findByTag(FragmentManager fragmentManager,
			String fragmentTag) {
		return (T)fragmentManager.findFragmentByTag(fragmentTag);
	}

	public static <T extends Fragment> T findById(FragmentActivity activity,
												  int id) {
		return findById(activity.getSupportFragmentManager(), id);
	}

	@SuppressWarnings("unchecked")
	public static <T extends Fragment> T findById(FragmentManager fragmentManager,
												   int id) {
		return (T)fragmentManager.findFragmentById(id);
	}

	public static <T extends Fragment> T create(FragmentManager fragmentManager,
			T fragment, String tag) {
		fragmentManager.beginTransaction().add(fragment, tag).commit();
		return fragment;
	}

}
