package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

public interface ChangeModelListener<T>{
	public void onChangeModel(T user);
}