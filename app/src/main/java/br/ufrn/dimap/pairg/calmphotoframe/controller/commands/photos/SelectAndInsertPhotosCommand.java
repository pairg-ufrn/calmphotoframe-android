package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.content.Context;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class SelectAndInsertPhotosCommand extends ActivityStatelessCommand<Collection<Photo>> {

	private EntityGetter<Long> albumIdGetter;

    private SelectImagesCommand selectImagesCommand;
    private ResultListener<Photo> onInsertPhotosProgressListener;

	public SelectAndInsertPhotosCommand(BaseActivity activity, EntityGetter<Long> albumIdGetter) {
		super(activity);
	    this.albumIdGetter = albumIdGetter;

        selectImagesCommand = new SelectImagesCommand(activity);
        selectImagesCommand.setAllowMultiple(true);
    }

    public SelectAndInsertPhotosCommand allowRemoteUris(boolean allowRemote){
        selectImagesCommand.setAllowRemoteUris(allowRemote);

        return this;
    }

	@Override
	public void execute(ResultListener<? super Collection<Photo>> listener) {
        this.selectImagesCommand.execute(
                new OnSelectionInsertPhotos(listener)
                        .setAlbumIdGetter(albumIdGetter)
                        .onProgress(onInsertPhotosProgressListener)
                        .setContext(getContext()));
	}

    public SelectAndInsertPhotosCommand onProgress(ResultListener<Photo> listener) {
        this.onInsertPhotosProgressListener = listener;
        return this;
    }

	
	/* ******************************** Helper Classes *******************************************/

	public static class OnSelectionInsertPhotos extends ResultListenerDelegator<Collection<String>, Collection<Photo>> {
        private EntityGetter<Long> albumIdGetter;

        private Context context;

        private ResultListener<? super Photo> onInsertProgressListener;

		public OnSelectionInsertPhotos(ResultListener<? super Collection<Photo>> listener) {
			super(listener);
		}

        public OnSelectionInsertPhotos setAlbumIdGetter(EntityGetter<Long> albumIdGetter) {
            this.albumIdGetter = albumIdGetter;
            return this;
        }

        public Context getContext() {
            return context;
        }

        public OnSelectionInsertPhotos setContext(Context context) {
            this.context = context;
            return this;
        }

        public OnSelectionInsertPhotos onProgress(ResultListener<? super Photo> progressListener){
            this.onInsertProgressListener = progressListener;
            return this;
        }

        @Override
		protected Collection<Photo> convertResult(Collection<String> result) {
			//do nothing
			return null;
		}

		@Override
		public void handleResult(Collection<String> imgPaths) {
			insertPhotos(imgPaths, getDelegate());
		}

        public void insertPhotos(Collection<String> imgPaths, ResultListener<? super Collection<Photo>> listener) {
            if(imgPaths != null && !imgPaths.isEmpty()){
                Listeners.onStart(listener);

                Long photoCollectionId = albumIdGetter == null ? null : albumIdGetter.getEntity();
                InsertImagesCommand cmd = new InsertImagesCommand(getContext(), photoCollectionId);
                cmd.execute(imgPaths, listener, onInsertProgressListener);
            }
        }
	}
}
