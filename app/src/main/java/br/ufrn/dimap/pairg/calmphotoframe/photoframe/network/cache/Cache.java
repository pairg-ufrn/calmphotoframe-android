package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache;


public interface Cache<K, V> {
	interface SizeCalculator<T>{
		int sizeOf(T obj);
	}
	boolean contains(K key);
	V getItem(K key);
	void putItem(K key, V value);
	void removeItem(K key);
	/** Remove itens on cache until has {@code size} of free space*/
	void reserve(int size);
	int getMaxSize();
	void setSizeCalculator(SizeCalculator<V> sizeCalculator);
}
