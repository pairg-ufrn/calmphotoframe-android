package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.view.View;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Commands;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users.AccountCreationCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users.ListUsersCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users.RemoveSelectedUserCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users.UpdateUserCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnErrorNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.CollectionsUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UsersFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ChangeModelListener;

public class UsersManagementController implements ChangeModelListener<User>{

	private final ListUsersCommand listUsersCmd;
	
	private UsersFragment view;
	private BaseActivity activity;
	
	public UsersManagementController(BaseActivity baseActivity) {
		listUsersCmd = new ListUsersCommand();
		this.activity = baseActivity;
	}
	
	public UsersFragment getView() {
		return view;
	}

	public void setView(UsersFragment view) {
		this.view = view;
		
		if(this.view != null){
            this.view.getActionModeCompat().setActionModeCallback(createActionModeCallback(view));
			this.view.setOnClickAddButton(new AddUserListener());

			this.view.setActivateUserCommand(
                    Commands.bindArgListeners(new ActivateUserCommand(),
                            new OnErrorNotifyUser(activity))
            );
		}
	}

	public void requestData() {
		listUsers();
	}
	public void removeFromView(User user){
		User u = getView().getAdapter().items().findItem(user, new UserIdComparator());
		getView().getAdapter().items().remove(u);
	}
	
	protected void listUsers() {
		listUsersCmd.execute(new BasicResultListener<List<User>>(){
			@Override
			public void onResult(List<User> result) {
				//remove current user
                User currentUser = ApiClient.instance().getCurrentUser();
                if(currentUser != null) {
                    result = CollectionsUtils.instance().filter(result, CollectionsUtils.toFilter(new UserIdComparator(), currentUser));
                }

				view.setItems(result);
			}
		});
	}
	
	protected void updateUser(User user) {
		new UpdateUserCommand(user).execute(new OnChangeUserUpdateView(getActivity(), getView()));
	}

	protected BaseActivity getActivity(){
		return activity;
	}

	@Override
	public void onChangeModel(User user) {
		updateUser(user);
	}

	protected BaseActionModeCallback createActionModeCallback(UsersFragment view) {
		BaseActionModeCallback actionMode = new BaseActionModeCallback(R.menu.contextualmenu_users);
		
		actionMode.putMenuCommand(R.id.action_removeUser
				, new RemoveSelectedUserCommand(getActivity(), view.getAdapter())
				, new CompositeResultListener<>(
						new OnResultRemoveUserFromView(getActivity(), this)
					  , new OnResultRefreshView(getActivity(), this)));
		
		return actionMode;
	}

    private class AddUserListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
			new AccountCreationCommand(activity).execute(new BasicResultListener<User>(){
                @Override
                public void onResult(User result) {
                    getView().getAdapter().items().addItem(result);
                }
            });
        }
    }
}
