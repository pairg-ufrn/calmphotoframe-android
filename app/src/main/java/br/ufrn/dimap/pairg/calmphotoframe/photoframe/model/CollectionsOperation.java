package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;

/** ContextOperation encapsulates the data needed to execute an operation with PhotoContexts 
 * 	(add photos to contexts or remove photos from contexts).*/
public class CollectionsOperation {
	public enum Operation{Add, Remove, Move}
	
	private final Set<Long> photoIds;
	private final Set<Long> subCollectionsIds;
	private final Set<Long> targetCollectionIds;
	private Operation operation;
	
	public CollectionsOperation(Operation changeOperation
				, Collection<PhotoComponent> photoComponents
				, Collection<PhotoCollection> targets) 
	{
		this(changeOperation
				, extractPhotoIds(photoComponents)
				, extractContextItemsIds(photoComponents)
				, PhotoCollection.extractCollectionIds(targets));
	}
	public CollectionsOperation(Operation changeOperation, Set<Long> photoIds, Set<Long> subcontextIds, Set<Long> targetIds) {
		super();
		this.operation = changeOperation;
		this.photoIds = new HashSet<>(photoIds);
		this.targetCollectionIds = new HashSet<>(targetIds);
		this.subCollectionsIds = new HashSet<>(subcontextIds);
	}

	public static Set<Long> extractContextItemsIds(Collection<PhotoComponent> photoComponents) {
		Set<Long> ids = new HashSet<>();
		for(PhotoComponent component : photoComponents){
			if(!component.isPhoto()){
				ids.add((long)component.getPhotoCollection().getId());
			}
		}
		return ids;
	}
	public static Set<Long> extractPhotoIds(Collection<PhotoComponent> photoComponents) {
		Set<Long> ids = new HashSet<>();
		for(PhotoComponent component : photoComponents){
			if(component.isPhoto()){//TODO: dar suporte a subcontextos tbm
				ids.add(component.getPhoto().getId().longValue());
			}
		}
		return ids;
	}

	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public Set<Long> getPhotoIds() {
		return photoIds;
	}
	public Set<Long> getSubCollectionsIds() {
		return subCollectionsIds;
	}
	public Set<Long> getTargetCollectionIds() {
		return targetCollectionIds;
	}

	public void setTargetCollectionIds(Set<Long> contextIds) {
		this.targetCollectionIds.clear();
		this.targetCollectionIds.addAll(contextIds);
	}
	public void setSubCollectionsIds(Set<Long> subcontextsIds) {
		this.subCollectionsIds.clear();
		this.subCollectionsIds.addAll(subcontextsIds);
	}
	public void setPhotoIds(Set<Long> photoIds) {
		this.photoIds.clear();
		this.photoIds.addAll(photoIds);
	}
}
