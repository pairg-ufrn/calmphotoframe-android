package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Build;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PickImagesActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

public class SelectImagesCommand extends ActivityStatelessCommand<Collection<String>>{

    private boolean allowRemoteUris, allowMultiple;

	public SelectImagesCommand(BaseActivity activity) {
		super(activity);
        allowRemoteUris = false;
		this.allowMultiple = true;
	}

    public boolean getAllowRemoteUris() {
        return allowRemoteUris;
    }

    public SelectImagesCommand setAllowRemoteUris(boolean allowRemoteUris) {
        this.allowRemoteUris = allowRemoteUris;
        return this;
    }

	public boolean getAllowMultiple() {
		return allowMultiple;
	}

	public SelectImagesCommand setAllowMultiple(boolean allowMultiple) {
		this.allowMultiple = allowMultiple;
        return this;
	}

	@Override
    public void execute(ResultListener<? super Collection<String>> listener) {
        selectImages(listener);
	}


	public void selectImages(ResultListener<? super Collection<String>> listener) {
    	Intent photoPickerIntent =  IntentUtils.instance().buildGetImageIntent(getAllowMultiple());

    	getActivity().startActivityForResult(photoPickerIntent, new OnSelectImages(listener));
	}


	protected Collection<String> extractImagePathsFromIntent(Intent intent) {
		
		Collection<String> imgPaths = tryGetImagePaths(intent);
		if(imgPaths != null && !imgPaths.isEmpty()){
			return imgPaths;
		}
		
		if(intent.getData() != null){
			String filepath = IntentUtils.instance().getImagePath(getContext(), intent.getData(), allowRemoteUris);
            if(filepath != null) {
                return Collections.singleton(filepath);
            }
		}
		
		return null;
	}
	
	private Collection<String> tryGetImagePaths(Intent intent) {
		Collection<String> paths = tryGetPathsFromClipData(intent);
		if(paths == null && intent.hasExtra(PickImagesActivity.EXTRA_SELECTED_IMAGES)){
			String[] pathsArray = intent.getStringArrayExtra(PickImagesActivity.EXTRA_SELECTED_IMAGES);
			paths = Arrays.asList(pathsArray);
		}
		return paths;
	}


	@TargetApi(16)
	private Collection<String> tryGetPathsFromClipData(Intent intent) {
		if(Build.VERSION.SDK_INT >= 16){
			ClipData clipData = intent.getClipData();
			if(clipData != null){
				Collection<String> paths = new LinkedList<>();

				for(int i=0; i < clipData.getItemCount(); ++i){
					ClipData.Item item = clipData.getItemAt(i);
					String path = IntentUtils.instance().getImagePath(getContext(), item.getUri());
                    paths.add(path);
				}
				return paths;
			}
		}
		
		return null;
	}

    class OnSelectImages implements OnActivityResultListener {
        ResultListener<? super Collection<String>> listener;

        public OnSelectImages(ResultListener<? super Collection<String>> listener) {
            this.listener = listener;
        }

        @Override
		public void onActivityResult(int resultCode, Intent intent) {
			if(resultCode == Activity.RESULT_OK){
				notifyOnResult(extractImagePathsFromIntent(intent), listener);
			}
			else{
				notifyOnResult(null, listener);
			}
		}
	}
}
