package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.support.v7.app.AppCompatActivity;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.ProgressBarDialogFragment;

public class WaitResultDialogController extends BasicResultListener<Object> {
    private static final String DIALOG_TAG = WaitResultDialogController.class + ".DIALOG";

    AppCompatActivity activity;
    ProgressBarDialogFragment waitFragment;

	public WaitResultDialogController(AppCompatActivity activity) {
        this.activity = activity;
        waitFragment = new ProgressBarDialogFragment();
    }

    @Override
    public void onStart() {
        showDialog();
    }

    @Override
    public void onResult(Object result) {
		hideDialog();
	}

	@Override
    public void onFailure(Throwable error) {
        hideDialog();
    }

    public void showDialog() {
        waitFragment.show(activity.getSupportFragmentManager(), DIALOG_TAG);
    }

	public void hideDialog() {
		if (waitFragment.isVisible()) {
			waitFragment.dismiss();
		}
	}
}
