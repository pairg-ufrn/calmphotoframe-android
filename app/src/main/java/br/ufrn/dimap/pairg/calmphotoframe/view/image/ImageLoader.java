package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

/**
 */
public interface ImageLoader<T> {
    RequestHandler loadImage(T entity, ResultListener<? super Drawable> listener);
    RequestHandler loadImage(T entity, ImageView target);
}
