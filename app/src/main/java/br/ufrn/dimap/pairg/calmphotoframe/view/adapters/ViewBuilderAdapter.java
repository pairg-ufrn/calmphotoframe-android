package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;
import android.view.View;
import android.view.ViewGroup;

public class ViewBuilderAdapter<Item> extends ItemsAdapter<Item>{
	private ViewBuilder viewBuilder;

	public ViewBuilderAdapter() {
		this(null);
	}
	public ViewBuilderAdapter(ViewBuilder builder) {
		items = buildItemList();
		this.viewBuilder = builder;
	}

	public ViewBuilder getViewBuilder(){
		return viewBuilder;
	}
	public void setViewBuilder(ViewBuilder viewBuilder){
		this.viewBuilder = viewBuilder;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return viewBuilder.buildView(getItem(position), convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getView(position, convertView, parent);
	}
}