package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.provider.MediaStore;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.Image;

public class LoadGalleryImagesCommand extends BaseStatelessCommand<Collection<Image>>{

	private Context context;
	private Handler handler;
	
	public LoadGalleryImagesCommand( Context context) {
		this.context = context;
		handler = new Handler();
	}

	@Override
	public void execute(ResultListener<? super Collection<Image>> listener) {
        //TODO: (maybe) use AsyncTask instead of Thread
		new Thread(new LoadImagesInBackground(context, handler, listener)).start();
	}

	public static class LoadImagesInBackground implements Runnable{
        private Collection<Image> loadedImages;

        private Context context;
        private Handler handler;
		ResultListener<? super Collection<Image>> listener;

		public LoadImagesInBackground(Context context, Handler handler, ResultListener<? super Collection<Image>> listener) {
            this.context = context;
            this.handler = handler;
			this.listener = listener;
		}

		@Override
		public void run() {
            Cursor imageCursor = buildCursor();
            if(imageCursor == null){
                Listeners.onFailure(listener, new RuntimeException("Failed to read gallery images"));
                return;
            }

            loadedImages = readImages(imageCursor);

            imageCursor.close();

			handler.post(new Runnable() {
				@Override
				public void run() {
					Listeners.onResult(listener, loadedImages);
				}
			});
		}

        protected Cursor buildCursor() {
            final String[] columns = { MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.MIME_TYPE,
                    MediaStore.Images.Media._ID };

            final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
            return context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                    null, orderBy + " DESC");
        }

        protected List<Image> readImages(Cursor imageCursor) {
            List<Image> images = new ArrayList<>();

            int dataColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
            int mimeColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE);
            int idColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);

            for (int i = 0; i < imageCursor.getCount(); i++) {
                imageCursor.moveToPosition(i);

                Image img = new Image();
                img.setUrl(imageCursor.getString(dataColumnIndex));
                img.setMimeType(imageCursor.getString(mimeColumnIndex));
                img.setImageId(imageCursor.getLong(idColumnIndex));

                images.add(img);
            }

            return images;
        }
    }
}