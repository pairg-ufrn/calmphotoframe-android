package br.ufrn.dimap.pairg.calmphotoframe.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class ExpandedPhotoCollection extends PhotoCollection implements Serializable{
	private static final long serialVersionUID = -8807834979627247953L;
	
	private Map<Integer, Photo> photos;
	private Map<Long, PhotoCollection> subcollections;

	public ExpandedPhotoCollection() {
		this(0, null);
	}
	public ExpandedPhotoCollection(int id, String name) {
		super();
		super.setId(id);
		super.setName(name);
		
		subcollections = new HashMap<>();
		photos = new HashMap<>();
	}
	public ExpandedPhotoCollection(PhotoCollection other){
		this();
		this.copy(other);
	}
	
	public Collection<Photo> getPhotos() {
		return photos.values();
	}
	public void setPhotos(Collection<Photo> photos) {
		clearPhotos();
		for(Photo photo: photos){
			this.addPhoto(photo);
		}
	}
	public void clearPhotos() {
		super.setPhotoIds(Collections.<Integer> emptySet());
		this.photos.clear();
	}

	public Collection<PhotoCollection> getSubcontexts() {
		return subcollections.values();
	}
	public void setSubcollections(Collection<PhotoCollection> photoCollections) {
		clearCollections();
		for(PhotoCollection collection: photoCollections){
			this.addPhotoCollection(collection);
		}
	}
	public void clearCollections() {
		this.subcollections.clear();
		super.setSubcollectionsIds(Collections.<Long> emptySet());
	}

	public void addPhotoCollection(PhotoCollection photoCollection) {
		if(photoCollection != null){
			super.addPhotoCollectionId(photoCollection.getId());
			this.subcollections.put(photoCollection.getId(), photoCollection);
		}
	}
	public void removePhotoCollection(PhotoCollection photoContext) {
		if(photoContext != null){
			super.removePhotoCollectionId(photoContext.getId());
			this.subcollections.remove(photoContext.getId());
		}
	}
	public boolean containsContext(PhotoCollection photoContext){
		return photoContext != null && this.subcollections.containsKey(photoContext.getId());
	}
	
	public void addPhoto(Photo photo) {
		if(photo != null){
			super.addPhotoId(photo.getId());
			this.photos.put(photo.getId(), photo);
		}
	}
	public void removePhoto(Photo photo) {
		if(photo != null){
			super.removePhotoId(photo.getId());
			this.photos.remove(photo.getId());
		}
	}
	public boolean containsPhoto(Photo photo){
		return photo != null && this.photos.containsKey(photo.getId());
	}

	public Collection<PhotoComponent> getPhotoComponents() {
		Collection<PhotoComponent> components = new ArrayList<>();
		if(subcollections != null && !subcollections.isEmpty()){
			for(PhotoCollection photo : getSubcontexts()){
				components.add(new PhotoComponent(photo));
			}
		}
		if(photos != null && !photos.isEmpty()){
			for(Photo photo : getPhotos()){
				components.add(new PhotoComponent(photo));
			}
		}
		return components;
	}
	public PhotoCollection getContext(Integer id) {
		return this.subcollections.get(id);
	}
	public Photo getPhoto(Integer id) {
		return this.photos.get(id);
	}
	
	@Override
	public void copy(PhotoCollection other) {
		super.copy(other);
		if(other instanceof ExpandedPhotoCollection){
			ExpandedPhotoCollection fullOther = (ExpandedPhotoCollection)other;
			copyPhotos(fullOther.getPhotos());
			copyContexts(fullOther.getSubcontexts());
		}
	}
	public void copyPhotos(Collection<Photo> photos) {
		this.clearPhotos();
		for(Photo photo : photos){
			this.addPhoto(photo.clone());
		}
	}
	public void copyContexts(Collection<PhotoCollection> contexts) {
		this.clearCollections();
		for(PhotoCollection context: contexts){
			this.addPhotoCollection(context);
		}
	}
	
	@Override
	public PhotoCollection clone() {
		ExpandedPhotoCollection clone = new ExpandedPhotoCollection();
		clone.copy(this);
		return clone;
	}
}
