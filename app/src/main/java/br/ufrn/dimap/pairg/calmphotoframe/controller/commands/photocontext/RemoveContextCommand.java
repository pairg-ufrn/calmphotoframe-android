package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.DialogBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;

public class RemoveContextCommand extends ActivityStatelessCommand<PhotoContext> {

	private static final String REMOVE_DIALOG_TAG = null;
	private ItemSelector<PhotoContext> contextSelector;
	
	public RemoveContextCommand(BaseActivity activity
							, ItemSelector<PhotoContext> selector) 
	{
		super(activity);
		this.contextSelector = selector;
	}

	@Override
	public void execute(ResultListener<? super PhotoContext> listener) {
		final PhotoContext contextToRemove = getSelectedContext();

		if(contextToRemove == null){
			return;
		}

		SimpleDialogFragment dialogFragment = buildConfirmRemoveDialog(contextToRemove, listener);
		dialogFragment.show(getActivity().getSupportFragmentManager(), REMOVE_DIALOG_TAG);
	}

	protected SimpleDialogFragment buildConfirmRemoveDialog(final PhotoContext contextToRemove
			, final ResultListener<? super PhotoContext> listener)
	{
		SimpleDialogFragment dialogFragment = 
				DialogBuilder.buildRemoveDialog(getContext(), R.string.dialog_removecontext_title);
		
    	dialogFragment.setDialogResponseListener(new DialogResponseListener() {
			@Override
			public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
				if(positive){
					executeRemoveContext(contextToRemove, listener);
				}
				return true;
			}
		});
		return dialogFragment;
	}

	private PhotoContext getSelectedContext() {
		Collection<? extends PhotoContext> items = contextSelector.getSelectedItems();
		
		return items.isEmpty() ? null : items.iterator().next();
	}

	private void executeRemoveContext(PhotoContext contextToRemove, ResultListener<? super PhotoContext> listener) {
    	if(contextToRemove != null){
    		//FIXME: filtrar resultado antes de repassar para listener
    		ApiClient.instance()
    						.contexts().remove(contextToRemove, new InvalidateRemovedContext(listener));
    	}
	}

	public static class InvalidateRemovedContext extends ResultListenerDelegator<PhotoContext, PhotoContext> {
		public InvalidateRemovedContext(ResultListener<? super PhotoContext> delegate) {
			super(delegate);
		}

		@Override
		public void onResult(PhotoContext result) {
			if(result != null){
				ApiClient.instance().invalidateContext(result.getId());
			}
			super.onResult(result);
		}
		
		@Override
		protected PhotoContext convertResult(PhotoContext result) {
			return result;
		}
	}
}