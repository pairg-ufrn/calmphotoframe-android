package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

/** código adaptado de : http://stackoverflow.com/questions/8842421/android-edittext-with-word-wrap-but-no-hard-returns*/

public class SingleLineWrapWordEditText extends EditText
{

    public SingleLineWrapWordEditText(Context context) {
        super(context);
    }

    public SingleLineWrapWordEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }    

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs)
    {
        InputConnection connection = super.onCreateInputConnection(outAttrs);
        int imeActions = outAttrs.imeOptions & EditorInfo.IME_MASK_ACTION;
        if ((imeActions & EditorInfo.IME_ACTION_DONE) != 0)
        {
            // clear the existing action
            outAttrs.imeOptions ^= imeActions;
            // set the DONE action
            outAttrs.imeOptions |= EditorInfo.IME_ACTION_DONE;
        }
        if ((outAttrs.imeOptions & EditorInfo.IME_FLAG_NO_ENTER_ACTION) != 0)
        {
            outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
        }
        return connection;
    }
}
