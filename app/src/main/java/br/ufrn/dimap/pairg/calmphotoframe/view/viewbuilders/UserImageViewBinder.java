package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.DrawableUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.UserImageLoader;

public class UserImageViewBinder extends InflaterViewBinder<User>{
    private static final int IMAGE_ID = R.id.view_user_thumbnail;

    private int imageViewId;
    private boolean useCircularDrawable;

    private ImageView imgView;
    private Drawable userDrawable;

    private ImageLoader<User> userImageLoader;
    private boolean alwaysTryGetImage;

    private User boundUser;

    public UserImageViewBinder() {
        this(R.layout.view_user_thumbnail);
    }

    public UserImageViewBinder(int layoutId) {
        super(layoutId);
        this.imageViewId = IMAGE_ID;
        this.useCircularDrawable = true;
    }

    @Override
    public void setup(View view) {
        this.imgView = ViewUtils.getView(view, imageViewId());
        if(userImageLoader == null){
            Context context = view == null ? null : view.getContext();
            userImageLoader = new UserImageLoader().setContext(context);
        }
    }

    @Override
    public void bind(User entity) {
        this.bind(entity, null);
    }
    public void bind(User entity, Drawable userDrawable) {
        boolean shouldTry = tryGetImage(entity);

        if(entity != null
                && (!shouldTry || this.boundUser == null || !this.boundUser.equals(entity)))
        {
            this.userDrawable = genUserDrawable(entity, userDrawable);
            this.imgView.setImageDrawable(this.userDrawable);
        }
        if(shouldTry){
            Logging.i(getClass().getSimpleName(), "load image to: " + imgView);

            getUserImageLoader().loadImage(entity, imgView);
        }

        this.boundUser = entity;
    }

    protected boolean tryGetImage(User entity) {
        if(getUserImageLoader() == null || entity == null){
            return false;
        }

        return getAlwaysTryGetImage() || entity.getHasProfileImage();
    }

    public User getBoundUser() {
        return boundUser;
    }

    public ImageLoader<User> getUserImageLoader() {
        return userImageLoader;
    }

    public void setUserImageLoader(ImageLoader<User> userImageLoader) {
        this.userImageLoader = userImageLoader;
    }

    public UserImageViewBinder setAlwaysTryGetImage(boolean alwaysTryGetImage) {
        this.alwaysTryGetImage = alwaysTryGetImage;
        return this;
    }

    public boolean getAlwaysTryGetImage() {
        return alwaysTryGetImage;
    }

    private Drawable genUserDrawable(User entity, Drawable userDrawable) {
        if(userDrawable != null){
            return userDrawable;
        }

        return genTextDrawable(entity);
    }

    public UserImageViewBinder imageViewId(int imageViewId) {
        this.imageViewId = imageViewId;
        return this;
    }

    public int imageViewId() {
        return imageViewId;
    }

    public UserImageViewBinder useCircularDrawable(boolean useCircularDrawable) {
        this.useCircularDrawable = useCircularDrawable;
        return this;
    }

    public boolean isUsingCircularDrawable() {
        return useCircularDrawable;
    }

    public void setEnabled(boolean enabled) {
        imgView.setEnabled(enabled);
        setupDrawableEnabled(enabled);
    }

    public Drawable getDrawable() {
        return imgView == null ? userDrawable : imgView.getDrawable();
    }
    public void setDrawable(Drawable drawable){
        this.userDrawable = drawable;
        if(imgView != null) {
            imgView.setImageDrawable(drawable);
            setupDrawableEnabled(imgView.isEnabled());
        }
    }

    private void setupDrawableEnabled(boolean enabled) {
        DrawableUtils.convertToGrayscale(imgView.getDrawable(), enabled);
    }

    public void setVisible(boolean visible) {
        ViewUtils.setVisible(imgView, visible);
    }

    /**********************************************************************************************/

    public Drawable genTextDrawable(User user) {
        String text = makeUserText(user);
        int color = makeUserColor(user);

        //TODO: rever como definir altura e largura
        TextDrawable.IShapeBuilder builder = TextDrawable.builder()
                .beginConfig().width(96).height(96).endConfig();

        Drawable d = isUsingCircularDrawable() ? builder.buildRound(text, color) : builder.buildRect(text, color);

        Logging.i(getClass().getSimpleName(), "genTextDrawable. Text: " + text + "; drawable: " + d);

        return d;
    }

    private int makeUserColor(User user) {
        ColorGenerator generator = ColorGenerator.MATERIAL;
        return (user == null || user.getId() == null)
                ? generator.getRandomColor()
                : generator.getColor("userid:" + user.getId() + ":" + user.getLogin());
    }

    private String makeUserText(User user) {
        String txt = " ";
        if(user != null){
            if(user.getName() != null && !user.getName().isEmpty()) {
                txt = extractInitials(user.getName());
            }
            else if(user.getLogin() != null && !user.getLogin().isEmpty()){
                txt = extractInitials(user.getLogin());
            }
        }
        return txt;
    }

    private String extractInitials(String login) {
        String[] words = login.split("\\s+");
        String initials = "";
        for(String w : words){
            if(!w.isEmpty()){
                initials += w.charAt(0);
            }
        }
        return initials.substring(0, Math.min(3, initials.length()));
    }
}
