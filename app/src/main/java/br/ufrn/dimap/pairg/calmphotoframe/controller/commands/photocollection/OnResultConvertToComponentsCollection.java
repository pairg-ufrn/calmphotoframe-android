package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;
import java.util.LinkedList;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class OnResultConvertToComponentsCollection
	   extends ResultListenerDelegator<Collection<PhotoCollection>, Collection<PhotoComponent>> 
{
	public OnResultConvertToComponentsCollection(ResultListener<? super Collection<PhotoComponent>> delegate) {
		super(delegate);
	}

	@Override
	protected Collection<PhotoComponent> convertResult(Collection<PhotoCollection> result) {
		Collection<PhotoComponent> components = new LinkedList<>();
		for(PhotoCollection coll : result){
			components.add(new PhotoComponent(coll));
		}
		return components;
	}
}