package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class GetPhotoFromCollectionResult extends BasicResultListener<PhotoCollection> {
	private final ResultListener<? super Photo> listener;
	private boolean include_permission;

	public GetPhotoFromCollectionResult(boolean include_permission, ResultListener<? super Photo> listener) {
		this.listener = listener;
        this.include_permission = include_permission;
	}

	@Override
	public void onResult(PhotoCollection result) {
		if(result == null || result.getPhotoIds().isEmpty()){
			Listeners.onResult(listener, null);
			return;
		}
		
		Integer photoId = result.getPhotoIds().iterator().next();
		ApiClient.instance().photos().get(photoId, include_permission, listener);
	}
}