package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.annotation.PluralsRes;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;

import br.ufrn.dimap.pairg.calmphotoframe.R;

public class ProgressNotifier {
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notifBuilder;
    private int notificationId;

    private int progressMessageId;

    private Context context;
    private int successfullMessagePlural;
    private int failurePlurals;
    private PendingIntent contentIntent;

    public ProgressNotifier(Context context, @StringRes int progressMessageId) {
        this.context = context;

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notifBuilder = new NotificationCompat.Builder(context);

        notificationId = (int) System.currentTimeMillis();

        this.progressMessageId = progressMessageId;
    }

    public Context getContext(){
        return this.context;
    }

    public int getSuccessPlurals() {
        return successfullMessagePlural;
    }

    public void setSuccessPlurals(@PluralsRes int successfullMessagePlural) {
        this.successfullMessagePlural = successfullMessagePlural;
    }

    public int getFailurePlurals() {
        return failurePlurals;
    }
    public ProgressNotifier setFailurePlurals(@PluralsRes int failurePlurals) {
        this.failurePlurals = failurePlurals;
        return this;
    }

    public PendingIntent getContentIntent() {
        return contentIntent;
    }

    public void setContentIntent(PendingIntent contentIntent) {
        this.contentIntent = contentIntent;
    }

    public void setup(){
        notifBuilder.setSmallIcon(R.drawable.ic_action_download);
    }

    public void updateNotification(int progressCount, int total) {
        String progressMessage = getContext().getString(progressMessageId);

        @SuppressLint("StringFormatMatches")
        String title = String.format(progressMessage, progressCount, total);

        notifBuilder.setContentTitle(title);
        notifBuilder.setProgress(0, 0, true);
//        notifBuilder.setProgress(total, progressCount, true);

        if(getContentIntent() != null) {
            notifBuilder.setContentIntent(getContentIntent());
        }

        notificationManager.notify(notificationId, notifBuilder.build());
    }

    public void onFinish(int succesfullCount, int failureCount) {
        StringBuilder finishMessage = new StringBuilder();
        if(succesfullCount > 0 && getSuccessPlurals() > 0){
            String addedImages = getContext().getResources().getQuantityString(
                    getSuccessPlurals(), succesfullCount);
            finishMessage.append(String.format(addedImages, succesfullCount));
        }
        if(failureCount > 0 && getFailurePlurals() > 0){
            String failedImages = getContext().getResources().getQuantityString(
                    getFailurePlurals(), failureCount);
            finishMessage.append(String.format(failedImages, failureCount));
        }

        this.finishNotification(finishMessage);
    }

    protected void finishNotification(CharSequence finishMessage) {
        notifBuilder.setContentText(finishMessage);
        notifBuilder.setProgress(0, 0, false);
        notificationManager.notify(notificationId, notifBuilder.build());
    }
}