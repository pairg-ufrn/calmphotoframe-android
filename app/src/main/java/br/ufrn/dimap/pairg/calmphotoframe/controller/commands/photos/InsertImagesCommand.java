package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.content.Context;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.Builder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.ProgressNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.CompositeCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.UpdateNotifierListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

public class InsertImagesCommand extends BaseArgsCommand<Collection<String>, Collection<Photo>> {

    private Context context;
	private Long photoCollectionId;
	
    private int concurrencyLevel;

    private Builder<ProgressNotifier> notifierBuilder;

    public InsertImagesCommand(Context context) {
		this(context, null);
	}
	public InsertImagesCommand(final Context context, Long photoCollectionId) {
		super();

		this.context = context;
		this.photoCollectionId = photoCollectionId;

        this.notifierBuilder = new InsertImagesNotifierBuilder(context);
	}

	public Context getContext(){
		return context;
	}

    public Builder<ProgressNotifier> getNotifierBuilder() {
        return notifierBuilder;
    }

    public void setNotifierBuilder(Builder<ProgressNotifier> notifierBuilder) {
        this.notifierBuilder = notifierBuilder;
    }

    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public void setConcurrencyLevel(int concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
    }

    @Override
	public void execute(Collection<String> imgPaths, ResultListener<? super Collection<Photo>> listener) {
        execute(imgPaths, listener, null);
    }

    public void execute(Collection<String> imgPaths,
                        ResultListener<? super Collection<Photo>> listener,
                        ResultListener<? super Photo> onInsertProgressListener) {
        if(imgPaths == null){
            throw new NullPointerException("Image paths collection should not be null!");
        }

        CompositeCommand
                .build(imgPaths, new InsertImageCommand(photoCollectionId))
                .setConcurrencyLevel(getConcurrencyLevel())
                .onProgress(Listeners.join(
                        new UpdateNotifierListener<Photo>(getNotifierBuilder().build(), imgPaths.size())
                        , onInsertProgressListener))
                .execute(listener);

    }

    public static class InsertImagesNotifierBuilder implements Builder<ProgressNotifier> {
        private final Context context;

        public InsertImagesNotifierBuilder(Context context) {
            this.context = context;
        }

        @Override
        public ProgressNotifier build() {
            ProgressNotifier progressNotifier = new ProgressNotifier(context, R.string.insertPhotos_progress);
            progressNotifier.setSuccessPlurals(R.plurals.insertPhotos_successfulInsertions);
            progressNotifier.setFailurePlurals(R.plurals.insertPhotos_failedInsertions);
            progressNotifier.setContentIntent(
                    IntentUtils.instance().buildPendingIntent(context, PhotoCollectionActivity.class, 0, 0));

            return progressNotifier;
        }
    }
}