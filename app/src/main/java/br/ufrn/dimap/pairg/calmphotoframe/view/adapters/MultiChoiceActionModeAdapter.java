package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.Collection;
import java.util.HashSet;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;

import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.ItemSelectionTracker;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiChoiceActionModeCompat;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;

public class MultiChoiceActionModeAdapter<Item> extends ViewBuilderAdapter<Item>{
    @Deprecated //adapt to use ItemSelectionManager
	private ItemSelectionTracker selectionTracker;
	private final MultiChoiceActionModeCompat multiChoiceActionModeCompat;

	public MultiChoiceActionModeAdapter(ViewBuilder viewBuilder) {
		this(viewBuilder, null);
	}
	public MultiChoiceActionModeAdapter(ViewBuilder viewBuilder, AppCompatActivity activity) {
		super(viewBuilder);
        selectionTracker = new ItemSelectionTracker(null);

		multiChoiceActionModeCompat = new MultiChoiceActionModeCompat(selectionTracker).setActionModeHandler(activity);
	}

    @SuppressWarnings("unused")
	public ActionModeHandler getActionModeHandler(){
        return multiChoiceActionModeCompat.getActionModeHandler();
    }
	public void setActionModeHandler(AppCompatActivity activity){
		multiChoiceActionModeCompat.setActionModeHandler(activity);
	}
	@SuppressWarnings("unused")
	public void setActionModeHandler(ActionModeHandler actionModeHandler){
		multiChoiceActionModeCompat.setActionModeHandler(actionModeHandler);
	}
	
	public boolean getMultipleSelection() {
		return selectionTracker.isMultiSelectionEnabled();
	}
	public ActionMode.Callback getActionModeCallback() {
		return multiChoiceActionModeCompat.getActionModeCallback();
	}
	public OnItemClickListener getOnItemClickListener(){
		return selectionTracker.getOnItemClickListener();
	}
	public ItemSelectionTracker getSelectionTracker(){
		return selectionTracker;
	}

	public void setMultipleSelection(boolean multipleSelection) {
        selectionTracker.setMultiSelectionEnabled(multipleSelection);
	}
	public void setActionModeCallback(ActionMode.Callback actionModeCallback) {
		multiChoiceActionModeCompat.setActionModeCallback(actionModeCallback);
	}
	public void setAdapterView(AdapterView<? super BaseAdapter> adapterView) {
		if(selectionTracker.getAdapterView() != null){
            selectionTracker.getAdapterView().setAdapter(null);
		}
        selectionTracker.setAdapterView(adapterView);
		if(adapterView != null){
			adapterView.setAdapter(this);
		}
    }
	public void setOnItemClickListener(OnItemClickListener listener) {
        selectionTracker.setOnItemClickListener(listener);
	}

	public Collection<Item> getCheckedItemsCollection() {
		Collection<Item> items = new HashSet<>();
		for(int i=0; i < getCount(); ++i){
			long id = getItemId(i);
			if(selectionTracker.isChecked(id)){
				items.add(getItem(i));
			}
		}
		return items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		multiChoiceActionModeCompat.checkView(view, getItemId(position));
		return view;
	}

    public MultiSelectionListener getSelectionListener() {
        return multiChoiceActionModeCompat.getSelectionListener();
    }

    public void setSelectionListener(MultiSelectionListener listener) {
        multiChoiceActionModeCompat.setSelectionListener(listener);
    }
}
