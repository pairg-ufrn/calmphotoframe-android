package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActivityActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

public class SaveInput  implements TextView.OnEditorActionListener {
    ActionListener<? super String> saveAction;
    BaseActionModeCallback actionModeCallback = new SimpleActionModeCallback();

    EditText editor;
    ActionModeHandler actionModeHandler;

    CharSequence savedInput;

    public void bindView(EditText nameEditor) {
        this.editor = nameEditor;
        this.editor.setOnClickListener(new StartActionModeOnClick());
        this.editor.setOnFocusChangeListener(new ControlActionModeOnFocus());
        this.editor.setOnEditorActionListener(this);

        actionModeCallback.putMenuCommand(R.id.action_confirm, new SaveInputCommand());
    }

    public void setActionModeHandler(AppCompatActivity activity){
        this.actionModeHandler = new ActivityActionModeHandler(activity);
    }
    public void setActionModeHandler(ActionModeHandler actionModeHandler){
        this.actionModeHandler = actionModeHandler;
    }

    public ActionListener<? super String> getSaveAction() {
        return this.saveAction;
    }

    public void setSaveAction(ActionListener<? super String> saveAction) {
        this.saveAction = saveAction;
    }

    public String getText() {
        return editor == null ? null : editor.getText().toString();
    }

    public void setText(CharSequence text){
        this.savedInput = text;
        if(editor != null){
            editor.setText(text);
        }
    }

    public void cancelEdit(boolean closeKeyboard) {
        if(editor != null){
            if(closeKeyboard) {
                closeKeyboard();
            }

            restoreText();
        }
    }

    protected void restoreText() {
        editor.setText(savedInput == null ? "" : savedInput);
    }

    protected void closeKeyboard() {
        Context context = getContext();

        InputMethodManager imm = ((InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE));
        imm.hideSoftInputFromWindow(editor.getWindowToken(), 0);
    }

    public void doSave() {
        executeSaveAction();
        if(actionModeCallback.isActive()) {
            actionModeCallback.finish();
        }
    }

    /* *******************************************************************************************/

    protected Context getContext() {
        return editor == null ? null : editor.getContext();
    }

    protected void executeSaveAction() {
        if(getSaveAction() != null){
            String textToSave = getText();
            getSaveAction().execute(textToSave);
            //TODO: check if save was successfull
            savedInput = textToSave;
        }
    }

    protected void startActionMode() {
        if(actionModeHandler != null) {
            actionModeHandler.startActionMode(actionModeCallback);
        }
    }

    protected void finishActionMode() {
        if(actionModeCallback.isActive()) {
            actionModeCallback.finish();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean shouldFireAction =
                (actionId == EditorInfo.IME_ACTION_DONE) ||
                        (event != null
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                                && !event.isCanceled()
                                && event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getRepeatCount() == 0);

        if(shouldFireAction){
            doSave();

            return true;
        }

        return false;
    }

    /************************************ Helper classes *****************************************/

    private class SaveInputCommand extends BaseStatelessCommand<Object> {
        @Override
        public void execute(ResultListener<? super Object> cmdResultListener) {
            executeSaveAction();
        }
    }

    private class SimpleActionModeCallback extends BaseActionModeCallback {
        public SimpleActionModeCallback() {
            super(R.menu.menu_simple_confirm);
        }

        @Override
        protected void onDestroyActionMode(BaseActionModeCallback callback, ActionMode mode) {
            super.onDestroyActionMode(callback, mode);
            cancelEdit(true);
        }
    }

    private class StartActionModeOnClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if(v.hasFocus() && !actionModeCallback.isActive()){
                startActionMode();
            }
        }
    }

    private class ControlActionModeOnFocus implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus) {
                startActionMode();
            }
            else{
                finishActionMode();
            }
        }
    }
}
