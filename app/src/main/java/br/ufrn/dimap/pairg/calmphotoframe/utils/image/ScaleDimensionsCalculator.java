package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

public abstract class ScaleDimensionsCalculator implements ImageDimensionsCalculator{

	@Override
	public int[] calculateSize(int[] imageDims, int[] desiredDimensions) {
		int[] limitDims = getMinDimensions(imageDims, desiredDimensions);
		
		int mainDimension = choiceScaleDimension(imageDims, limitDims);
		return scaleDimensions(imageDims, limitDims, mainDimension);
	}

	protected int[] getMinDimensions(int[] imageDims, int[] desiredDimensions) {
		int[] maxDims = new int[2];
		for(int i=0; i<2; ++i){
			maxDims[i] = Math.min(imageDims[i], desiredDimensions[i]);
		}
		return maxDims;
	}

	/** Choices the dimension used as reference to scaleDown the image*/
	protected abstract int choiceScaleDimension(int[] imageDims, int[] maxDims);
	
	protected int[] scaleDimensions(int[] imageDims, int[] maxDims, int mainDim) {
		int otherDim = mainDim == 0 ? 1 : 0;
		
		int[] result = new int[2];
		result[mainDim]  = maxDims[mainDim];
		
		double _scale = ((double)maxDims[mainDim]) / imageDims[mainDim];
		result[otherDim] = (int) (_scale * imageDims[otherDim]);
		
		return result;
	}

}