package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import java.io.InputStream;

public class BitmapDecoders {
	public static BitmapDecoder decoder(byte[] data){
		return new ByteBitmapDecoder(data);
	}
	public static BitmapDecoder decoder(String filePath){
		return new FileBitmapDecoder(filePath);
	}
	public static BitmapDecoder decoder(InputStream stream) {
		return new StreamBitmapDecoder(stream);
	}
	public static BitmapDecoder decoder(InputStream stream, int maxContentLength) {
		return new StreamBitmapDecoder(stream, maxContentLength);
	}
}
