package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;

public class InvalidateContextAndTryAgain extends BasicResultListener {
    Command command;
    ResultListener<?> resultListener;

    public InvalidateContextAndTryAgain(Command<?> command, ResultListener<?> resultListener){
        this.command = command;
        this.resultListener = resultListener;
    }

    @Override
    public void onFailure(Throwable error) {
        ApiException ex = ExceptionUtils.getCause(error, ApiException.class);
        if(ex != null && ApiClient.instance().getCurrentContext() != null){
            ApiClient.instance().invalidateContext(ApiClient.instance().getCurrentContext());
            command.execute(resultListener);
        }
    }
}

