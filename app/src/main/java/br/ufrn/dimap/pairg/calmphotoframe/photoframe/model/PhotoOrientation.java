package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;

public class PhotoOrientation implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4036630216806958611L;
	
	private boolean flipped;
	private int clockwiseRotation;
	private int orientationCode;
	
	public int getOrientationCode() {
		return orientationCode;
	}
	public boolean isFlipped() {
		return flipped;
	}
	public int getClockwiseOrientation() {
		return clockwiseRotation;
	}
	
	public void setOrientationCode(int orientationCode) {
		this.orientationCode = orientationCode;
	}
	public void setFlipped(boolean flipped) {
		this.flipped = flipped;
	}
	public void setClockwiseRotation(int clockwiseOrientation) {
		this.clockwiseRotation = clockwiseOrientation;
	}
}
