package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.image.ImageOrientationHelper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.BitmapHttpResponseHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import cz.msebera.android.httpclient.Header;

public class ThumbnailResponseHandler extends BitmapHttpResponseHandler {
	private final Photo photoDescriptor;
	private final ResultListener<? super PhotoImageWrapper> thumbnailLoadListener;
	private final PhotoCache photoCache;
	private String reqUrl;

	public ThumbnailResponseHandler(PhotoCache photoCache, Photo photoDescriptor, ResultListener<? super PhotoImageWrapper> listener) {
		this.photoCache = photoCache;
		this.thumbnailLoadListener = listener;
		this.photoDescriptor = photoDescriptor;
	}

	public ThumbnailResponseHandler requestUrl(String requestUrl){
		this.reqUrl = requestUrl;
		return this;
	}

	@Override
	public void onSuccess(int arg0, Header[] arg1, Bitmap bitmap) {
		Bitmap thumbnail = ImageOrientationHelper.fixImage(photoDescriptor, bitmap);
		photoCache.putThumbnail(photoDescriptor.getId(), thumbnail);
		Listeners.onResult(thumbnailLoadListener, new PhotoImageWrapper(photoDescriptor, thumbnail));
	}
	@Override
	public void onFailure(int statusCode, Header[] arg1, byte[] arg2, Throwable cause) {
		Throwable error = ApiException.getError(statusCode,"", cause, reqUrl);
		Listeners.onFailure(thumbnailLoadListener, error);
	}
}