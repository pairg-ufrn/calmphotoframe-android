package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import android.os.Bundle;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoGalleryActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CreatePhotoCollectionCommand.PhotoCollectionGetter;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

public class ViewPhotosFromCollectionCommand extends ActivityStatelessCommand<Void>{
	PhotoCollectionGetter collectionGetter;
	EntityGetter<String> queryGetter;
	
	public ViewPhotosFromCollectionCommand(BaseActivity activity, PhotoCollectionGetter collGetter) {
		this(activity, collGetter, null);
	}
	public ViewPhotosFromCollectionCommand(BaseActivity activity, PhotoCollectionGetter collGetter
			, EntityGetter<String> queryGetter) {
		super(activity);
		this.collectionGetter = collGetter;
		this.queryGetter = queryGetter;
	}

	@Override
	public void execute(ResultListener<? super Void> cmdResultListener) {
		Long collectionId = getCollectionId();
		String queryFilter = getQuery();
		
		Bundle extras = new Bundle();
		if(collectionId != null){
			extras.putLong(Intents.Extras.PhotoCollectionId, collectionId);
		}
		
		if(queryFilter != null){
			extras.putString(Intents.Extras.Query, queryFilter);
		}

		Log.d(getClass().getSimpleName(), "Start gallery activity with collectionId: " + collectionId);
		
		IntentUtils.startActivity(getActivity(), PhotoGalleryActivity.class, extras);
	}

	private Long getCollectionId() {
		Long collectionId = null;
		if(collectionGetter != null && collectionGetter.getPhotoCollection() != null){
			collectionId = collectionGetter.getPhotoCollection().getId();
		}
		return collectionId;
	}
	
	private String getQuery() {
		return queryGetter == null ? null : queryGetter.getEntity();
	}
}
