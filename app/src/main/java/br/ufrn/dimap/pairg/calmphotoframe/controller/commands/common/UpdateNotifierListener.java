package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class UpdateNotifierListener<T> extends BasicResultListener<T> {
    ProgressNotifier progressNotifier;

    int failureCount = 0, successCount = 0, total;

    public UpdateNotifierListener(ProgressNotifier progressNotifier, int total) {
        this.progressNotifier = progressNotifier;
        this.total = total;
    }

    @Override
    public void onStart() {
        super.onStart();

        this.failureCount = 0;
        this.successCount = 0;

        if (progressNotifier != null) {
            progressNotifier.setup();
        }
        updateNotification();
    }

    @Override
    protected void handleResult(T result) {
        ++successCount;
        updateNotification();
    }

    @Override
    protected void handleError(Throwable e) {
        Logging.d(getClass().getSimpleName(), "Failed to insert image: ", e);
        ++failureCount;
        updateNotification();
    }

    @Override
    protected void onFinish() {
        super.onFinish();

        if (getProgressCount() == getArgumentsCount()) {
            progressNotifier.onFinish(getSuccessCount(), getFailureCount());
        }
    }

    private void updateNotification() {
        if (progressNotifier != null) {
            progressNotifier.updateNotification(getProgressCount(), getArgumentsCount());
        }
    }

    private int getProgressCount() {
        return getSuccessCount() + getFailureCount();
    }

    private int getArgumentsCount() {
        return total;
    }

    private int getSuccessCount() {
        return successCount;
    }

    private int getFailureCount() {
        return failureCount;
    }
}
