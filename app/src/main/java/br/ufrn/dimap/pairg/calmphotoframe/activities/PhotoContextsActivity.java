package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.contexts.PhotoContextsController;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;

public class PhotoContextsActivity extends ConnectedActivity {

	private PhotoContextsController controller;
	
	public PhotoContextsActivity() {
		controller = new PhotoContextsController(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_contexts);
		setSnackbarViewId(R.id.coordinator_layout);
		setupToolbar();
		
		controller.initialize();
	}

	@Override
	protected void loadData() {
		controller.reloadContent();
	}
}
