package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

public interface Converter<SourceType, DestType> {
    DestType convert(SourceType source);
}
