package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class OnEditPhotoUpdate extends BasicResultListener<Photo> {
	private PhotoHolder photoHolder;
	
	public OnEditPhotoUpdate(PhotoHolder photoHolder) {
		this.photoHolder = photoHolder;
	}
	
	@Override
	public void onResult(Photo result) {
		photoHolder.updatePhoto(result);
	}
}