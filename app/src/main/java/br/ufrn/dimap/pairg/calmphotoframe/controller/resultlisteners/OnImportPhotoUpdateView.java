package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class OnImportPhotoUpdateView extends ActivityResultListener<Photo> {
	private PhotoHolder photoHolder;
	
	public OnImportPhotoUpdateView(BaseActivity activity, PhotoHolder photoViewer) {
		super(activity);
		this.photoHolder = photoViewer;
	}
	
	@Override
	public void onResult(Photo photoDescriptor) {
		photoHolder.updatePhoto(photoDescriptor);
		getActivity().showUserMessage(R.string.notification_importedPhoto);
	}
}