package br.ufrn.dimap.pairg.calmphotoframe.controller;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;

public interface AsyncGetter<T> {
	public void getAsync(ResultListener<? super T> listener);
}
