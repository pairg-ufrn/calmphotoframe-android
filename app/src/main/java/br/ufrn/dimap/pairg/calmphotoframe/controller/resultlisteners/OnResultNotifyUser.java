package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;

public class OnResultNotifyUser extends BasicResultListener<Object>{

    private UserNotifier userNotifier;
    private int textId;

    public OnResultNotifyUser(UserNotifier userNotifier, int textId) {
        this.userNotifier = userNotifier;
        this.textId = textId;
    }

    @Override
    public void onResult(Object result) {
//        if(userNotifier != null){
            userNotifier.notifyUser().text(textId).show();
//        }

    }
}
