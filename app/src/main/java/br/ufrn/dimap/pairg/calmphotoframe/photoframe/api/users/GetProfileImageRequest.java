package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseApiRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetProfileImageRequest extends BaseApiRequest<Bitmap> {
    Long userId;

    public GetProfileImageRequest(ApiConnector apiConnector, Long userId) {
        super(apiConnector, null);

        this.userId = userId;
    }

    @Override
    public RequestHandler execute() {
        return http().getImage(genUrl(), listener);
    }

    @Override
    protected String genUrl() {
        return getApiBuilder().userProfileImageUrl(userId);
    }
}
