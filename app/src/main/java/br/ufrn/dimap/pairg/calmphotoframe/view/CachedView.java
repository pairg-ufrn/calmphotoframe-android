package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.app.Activity;
import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class CachedView<T extends View> {
    T view;
    int viewId;

    public CachedView(int viewId) {
        this.viewId = viewId;
        this.view = null;
    }

    public T getView(View rootView){
        if(view == null){
            view = ViewUtils.getView(rootView, viewId);
        }
        return view;
    }

    public T getView(Activity activity){
        if(view == null){
            view = ViewUtils.getView(activity, viewId);
        }
        return view;
    }
}
