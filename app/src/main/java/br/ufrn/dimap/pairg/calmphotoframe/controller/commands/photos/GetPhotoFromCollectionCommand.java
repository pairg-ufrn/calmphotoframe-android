package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class GetPhotoFromCollectionCommand extends BaseStatelessCommand<Photo>{
	private Long collectionId;
	private boolean includePermission;

	public GetPhotoFromCollectionCommand(Long collectionId, boolean include_permission) {
		this.collectionId = collectionId;
		this.includePermission = include_permission;
	}
	
	@Override
	public void execute(final ResultListener<? super Photo> cmdResultListener) {
		ApiClient.instance().collections().get(collectionId
		, new GetPhotoFromCollectionResult(includePermission, cmdResultListener));
	}
}
