package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;

public class PhotoCollectionNavigator implements OnSelectItemListener<PhotoComponent>{
	private BaseActivity activity;
	private CollectionAndContextContainer state;
	
	public PhotoCollectionNavigator(BaseActivity activity, CollectionAndContextContainer state) {
		this.activity = activity;
		this.state = state;
	}
	
	@Override
	public void onItemSelection(PhotoComponent selectedPhotoComponent) {
		if(selectedPhotoComponent.isPhoto()){
			selectPhoto(selectedPhotoComponent.getPhoto(), state.getPhotoCollection());
		}
		else{
			navigateToCollection(selectedPhotoComponent.getPhotoCollection());
		}
	}

	public void selectPhoto(Photo selectedPhoto, PhotoCollection selectedPhotoCollection) {
        LocalPersistence.instance().setCurrentPhotoAndCollection(selectedPhoto, selectedPhotoCollection);

		Intent resultIntent = new Intent();
		resultIntent.putExtra(br.ufrn.dimap.pairg.calmphotoframe.activities.Intents.Extras.SelectedPhoto, selectedPhoto);
		resultIntent.putExtra(br.ufrn.dimap.pairg.calmphotoframe.activities.Intents.Extras.SelectedCollection, selectedPhotoCollection);

		activity.setResult(Activity.RESULT_OK, resultIntent);
		activity.finish();
	}

	private void navigateToCollection(PhotoCollection photoCollection) {
		navigateToCollection(photoCollection, state.getPhotoContext());
	}
	public void navigateToCollection(PhotoCollection photoCollection, PhotoContext photoContext) {
    	Intent intent = new Intent(activity, PhotoCollectionActivity.class);
    	if(photoCollection != null){
    		intent.putExtra(Intents.Extras.PhotoCollection,photoCollection);
    	}
    	if(photoContext != null){
    		intent.putExtra(Intents.Extras.PhotoContext, photoContext);
    	}
    	activity.startActivityForResult(intent,new OnActivityResultListener() {
			@Override
			public void onActivityResult(int resultCode, Intent intent) {
				Log.i(getClass().getSimpleName(), "Returned from activity");
				if(resultCode == Activity.RESULT_OK && intent.hasExtra(br.ufrn.dimap.pairg.calmphotoframe.activities.Intents.Extras.SelectedPhoto)){
					Photo selectedPhoto = (Photo) intent.getSerializableExtra(br.ufrn.dimap.pairg.calmphotoframe.activities.Intents.Extras.SelectedPhoto);
					PhotoCollection selectedCollection = (PhotoCollection)intent
											.getSerializableExtra(br.ufrn.dimap.pairg.calmphotoframe.activities.Intents.Extras.SelectedCollection);
					selectPhoto(selectedPhoto, selectedCollection);
				}
			}
		});
	}

	public boolean isRootPhotoCollection() {
		return activity.getIntent() == null || 
				!activity.getIntent().hasExtra(Intents.Extras.PhotoCollection);
	}
}