package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.SelectionDialog;

public class CopyToPhotoCollectionCommand 
		extends BasePhotoCollectionCommand<Collection<PhotoCollection>>
{
	
	public CopyToPhotoCollectionCommand(BaseActivity activity, PhotoComponentSelector selector) {
		super(activity, selector);
	}

    @Override
    protected void executeItemsCommand(Collection<? extends PhotoCollection> collections,
                                       Collection<PhotoComponent> selectedItems,
                                       ResultListener<? super Collection<PhotoCollection>> listener)
    {
        ApiClient.instance().collections().addItems(collections, selectedItems, listener);
    }
	
	@Override
	protected void customizeSelectionDialog(SelectionDialog<PhotoCollection> albunsSelectionDialog) {
		albunsSelectionDialog.setDialogTitle  (getString(R.string.dialog_copyItemsToAlbum_title));
		albunsSelectionDialog.hideDialogMessage();
	}

}