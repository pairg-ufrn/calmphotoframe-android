package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session;

import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiEndpointHolder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.UpdateEndpointOnResult;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class SessionApi {
    private final ApiConnector apiConnector;
    private final ApiEndpointHolder photoFrameInfoHolder;

    public SessionApi(ApiConnector apiConnector, ApiEndpointHolder photoFrameInfoHolder) {
        this.apiConnector = apiConnector;
        this.photoFrameInfoHolder = photoFrameInfoHolder;
    }

    /** @see #connect(ConnectionInfo, ResultListener)*/
    public RequestHandler connect(Endpoint endpoint, LoginInfo loginInfo
            , final ResultListener<? super ApiEndpoint> onConnectListener) {
        return connect(new ConnectionInfo(endpoint, loginInfo), onConnectListener);
    }

    /** Try connect to a remote Calm Photo Frame.
     * @param connectionInfo describes the data needed to connect to the remote photo frame.
     * @param listener receives notifications about success or failure of connection or disconnection.*/
    public RequestHandler connect(@NonNull ConnectionInfo connectionInfo
            , final ResultListener<? super ApiEndpoint> listener)
    {
        assertParameterNotNull(ConnectionInfo.class, connectionInfo);
        assertParameterNotNull(Endpoint.class, connectionInfo.getEndpoint());
        assertParameterNotNull(LoginInfo.class, connectionInfo.getLoginInfo());

        return new LoginRequest(apiConnector, connectionInfo
                , new UpdateEndpointOnResult(photoFrameInfoHolder, listener)).execute();
    }

    /** Connects to the given getEndpoint using the specified connectionToken.
     * @param endpoint - the server to connect
     * @param connectionToken - the token to use as authentication with this server
     * @param onConnectListener - (optional) listener to receive request result
     * */
    public RequestHandler connect(final Endpoint endpoint, final String connectionToken
            , final ResultListener<? super ApiEndpoint> onConnectListener) {

        ResultListener<ApiEndpoint> delegator = new CommonListenerDelegator<ApiEndpoint>(onConnectListener){
            @Override
            protected boolean interceptResult(ApiEndpoint result) {
                apiConnector.putTokenCookie(connectionToken, endpoint.getHostname());
                photoFrameInfoHolder.set(result);
                return true;
            }
        };
        return new TokenLoginRequest(apiConnector, connectionToken, endpoint)
                .listener(delegator)
                .execute();
    }

    public void reconnect(final ResultListener<? super Endpoint> onConnectListener) {
        checkConnection(getPhotoFrameInfo(), onConnectListener);
    }

    public RequestHandler disconnect(ResultListener<? super Endpoint> listener){
        return disconnect(getPhotoFrameInfo(), listener);
    }
    protected RequestHandler disconnect(final Endpoint photoFrameToDisconnect, ResultListener<? super Endpoint> listener) {
        return new LogoffRequest(apiConnector, photoFrameToDisconnect, new CommonListenerDelegator<Endpoint>(listener){
            @Override
            protected boolean interceptResult(Endpoint result) {
                onPhotoFrameLogoff(result);
                return true;
            }
        }).execute();
    }

    private void onPhotoFrameLogoff(Endpoint photoFrameInfo){
        if(photoFrameInfo == null){
            return;
        }
        if(photoFrameInfo.equals(getPhotoFrameInfo())){
            setPhotoFrameInfo(null);
        }
    }

    public boolean isConnected() {
        return getPhotoFrameInfo() != null;
    }

    public RequestHandler checkConnection(ResultListener<? super ApiEndpoint> listener) {
        return this.checkConnection(getPhotoFrameInfo(), listener);
    }
    protected RequestHandler checkConnection(final ApiEndpoint apiEndpoint, final ResultListener<? super ApiEndpoint> listener) {
        ResultListener<Endpoint> delegator = new ResultListenerDelegator<Endpoint, ApiEndpoint>(listener) {
            @Override
            protected ApiEndpoint convertResult(Endpoint result) {
                return apiEndpoint;
            }
        };

        return new CheckConnectionRequest(apiConnector, apiEndpoint, delegator).execute();
    }

    private ApiEndpoint getPhotoFrameInfo() {
        return photoFrameInfoHolder.get();
    }

    private void setPhotoFrameInfo(ApiEndpoint info) {
        this.photoFrameInfoHolder.set(info);
    }

    private void assertParameterNotNull(Class<?> someClass, Object parameter) {
        if(parameter == null){
            throw new NullPointerException("Parameter of type \"" + someClass.getName()
                    + "\" should not be null!");
        }
    }
}
