package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.ListPhotoCollectionByContextRequest.CollectionsContainer;

public class ListPhotoCollectionByContextRequest
		extends BaseGenericRequest<CollectionsContainer, List<PhotoCollection>>
{
	
	private Long collectionsContextId;

	public ListPhotoCollectionByContextRequest(ApiConnector apiConnector
			, Long contextId, ResultListener<? super List<PhotoCollection>> listener) 
	{
		super(apiConnector, CollectionsContainer.class, listener);
		this.collectionsContextId = contextId;
	}

	@Override
	protected String genUrl(){
		return getApiBuilder().collectionsByContextUrl(collectionsContextId);
	}

	@Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected List<PhotoCollection> convertToResultType(CollectionsContainer response) {
		return response.collections;
	}

	/** Classe auxiliar para realizar conversão de json*/
	public static class CollectionsContainer{
		public List<PhotoCollection> collections;
	}
}
