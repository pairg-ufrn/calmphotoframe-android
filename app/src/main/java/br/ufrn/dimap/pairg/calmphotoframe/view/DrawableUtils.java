package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

public class DrawableUtils {
	public static void highlight(Context context, Drawable drawable, int colorRes, boolean highlight) {
		if (highlight) {
			int color = ContextCompat.getColor(context, colorRes);
			drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
		} else {
			drawable.clearColorFilter();
		}
	}

	public static void convertToGrayscale(Drawable d, boolean enabled){
	    if(d == null){
	    	return;
	    }
		if(!enabled){
//			ColorMatrix matrix = new ColorMatrix();
//		    matrix.setSaturation(0);
			//From http://stackoverflow.com/questions/8381514/android-converting-color-image-to-grayscale
//		    float[] mat = new float[]{
//		            0.3f, 0.59f, 0.11f, 0, 0, 
//		            0.3f, 0.59f, 0.11f, 0, 0, 
//		            0.3f, 0.59f, 0.11f, 0, 0, 
//		            0, 0, 0, 1, 0,};
		    float oneThird = 1/3f;
		    float[] mat = new float[]{
		            oneThird, oneThird, oneThird, 0, 0, 
		            oneThird, oneThird, oneThird, 0, 0, 
		            oneThird, oneThird, oneThird, 0, 0, 
		            0, 0, 0, 1, 0,};

		    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(mat);
		    d.setColorFilter(filter);
		}
		else{
			d.setColorFilter(null);
		}
	}
}
