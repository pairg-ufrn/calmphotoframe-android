package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextVisibility;

public class ContextVisibilityViewBuilder extends EnumViewBuilder<PhotoContextVisibility> {
	
	public ContextVisibilityViewBuilder() {
		this(R.layout.item_simple_text, android.R.id.text1);
	}
	public ContextVisibilityViewBuilder(int layoutId, int viewId) {
		super(layoutId, viewId);
	}

	@Override
	protected int enumToTextId(PhotoContextVisibility data) {
		switch (data) {
		case EditableByOthers:
			return R.string.photocontext_creation_visibility_othersWrite;
		case VisibleByOthers:
			return R.string.photocontext_creation_visibility_othersRead;
		case Private:
			return R.string.photocontext_creation_visibility_private;
		default:
			return 0;
		}
	}
}