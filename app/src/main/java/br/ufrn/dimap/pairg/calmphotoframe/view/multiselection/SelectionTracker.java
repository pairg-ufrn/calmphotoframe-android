package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

import android.view.View;

/**
 */
public interface SelectionTracker {
    void addSelectionListener(MultiSelectionListener listener);
    void removeSelectionListener(MultiSelectionListener listener);

    boolean isChecked(long itemId);

    View getView(int position);

    void notifyChangedPosition(int position);

    void deselectAll();


    boolean isSelectionEnabled();
    void setSelectionEnabled(boolean selectionEnabled);
}
