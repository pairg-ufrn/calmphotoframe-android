package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import cz.msebera.android.httpclient.client.CookieStore;

public class ApiConnector {

	private HttpConnector httpConnector;
	private ApiRouteBuilder apiBuilder;
	private ObjectMapper jsonConverter;
	private PhotoCache photoCache;
	
	public ApiConnector() {
		this(null,null,null);
	}
	public ApiConnector(HttpConnector connector, ApiRouteBuilder apiBuilder, ObjectMapper objectMapper) {
		this(connector, apiBuilder, objectMapper, null);
	}
	public ApiConnector(HttpConnector connector, ApiRouteBuilder apiBuilder
			, ObjectMapper objectMapper, PhotoCache photoCache) 
	{
		this.httpConnector = connector;
		this.apiBuilder = apiBuilder;
		this.jsonConverter = objectMapper;
		this.photoCache = photoCache;
	}
	
	public HttpConnector getHttpConnector() {
		return httpConnector;
	}
	public void setHttpConnector(HttpConnector httpConnector) {
		this.httpConnector = httpConnector;
	}
	public ApiRouteBuilder getApiBuilder() {
		return apiBuilder;
	}
	public void setApiBuilder(ApiRouteBuilder apiBuilder) {
		this.apiBuilder = apiBuilder;
	}
	public ObjectMapper getJsonConverter() {
		return jsonConverter;
	}
	public void setJsonConverter(ObjectMapper jsonConverter) {
		this.jsonConverter = jsonConverter;
	}
	public PhotoCache getPhotoCache() {
		return photoCache;
	}
	public void setPhotoCache(PhotoCache photoCache) {
		this.photoCache = photoCache;
	}

	public HttpConnector http(){ return getHttpConnector(); }
    public CookieStore cookies(){
        return http().getCookieStore();
    }

    public void putTokenCookie(String token, String domain) {
        cookies().addCookie(ApiHelper.buildTokenCookie(token, domain));
    }


    public <ResultType, ListenerType>
    RequestBuilder<ResultType, ListenerType> request(String url){
        return new RequestBuilder<ResultType, ListenerType>(this).url(url);
    }

}
