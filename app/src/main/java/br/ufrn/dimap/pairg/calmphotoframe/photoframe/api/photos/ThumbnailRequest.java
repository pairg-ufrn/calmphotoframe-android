package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ThumbnailResponseHandler;

public class ThumbnailRequest extends CommonGenericRequest<PhotoImageWrapper>{
	private Photo photo;
	public ThumbnailRequest(ApiConnector apiConnector, Photo photo, ResultListener<? super PhotoImageWrapper> thumbnailListener) {
		super(apiConnector, PhotoImageWrapper.class, thumbnailListener);
		this.photo = photo;
	}

	@Override
	public RequestHandler execute() {
		int photoId = photo.getId();
		Bitmap cachedBitmap = getPhotoCache().getThumbnail(photoId);
		if(cachedBitmap == null){
			Listeners.onStart(listener);

			return getHttpConnector().get(genUrl(), new ThumbnailResponseHandler(getPhotoCache(), photo, listener));
		}
		else{
			Listeners.onResult(listener, new PhotoImageWrapper(photo, cachedBitmap));
			return new FinishedRequestHandler();
		}
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().thumbnailUrl(photo.getId());
	}

}