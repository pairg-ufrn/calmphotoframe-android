package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import java.io.File;
import java.io.UnsupportedEncodingException;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.FileResponseHandler;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.BasicCookieStore;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class HttpConnector {

	private static final String CONTENTTYPE_JSON = "application/json";

	private static final int DEFAULT_TIMEOUT  = 10000;

    private static final String TAG = HttpConnector.class.getSimpleName();

	private AsyncHttpClient httpClient;
	private CookieStore cookieStore;
    private ImageGetter imageGetter;

	public HttpConnector() {
		this(new AsyncHttpClient());
	}
	public HttpConnector(AsyncHttpClient httpClient) {
		this(httpClient, new BasicCookieStore());
	}
	public HttpConnector(AsyncHttpClient httpClient, CookieStore cookieStore) {
		this.httpClient = httpClient;
		this.httpClient.setTimeout(DEFAULT_TIMEOUT);
		this.cookieStore = cookieStore;
		this.httpClient.setCookieStore(cookieStore);

        this.imageGetter = new PicassoImageGetter(cookieStore);
	}

	public AsyncHttpClient getHttpClient(){
		return httpClient;
	}
	public CookieStore getCookieStore() {
		return cookieStore;
	}

    public ImageGetter getImageGetter() {
        return imageGetter;
    }
    public void setImageGetter(ImageGetter imageGetter) {
        this.imageGetter = imageGetter;
    }

    public RequestHandler get(String url, ResponseHandlerInterface responseHandler) {
		Log.i(TAG, "GET " + url);
		return handler(httpClient.get(url, responseHandler));
	}
	public RequestHandler get(String url, Header[] headers, ResponseHandlerInterface responseHandler) {
		Log.i(TAG, "GET " + url);
		return handler(httpClient.get(null, url, headers, null, responseHandler));
	}

	public RequestHandler getImage(String url, ResultListener<? super Bitmap> listener){
		Log.i(TAG, "GET <image> " + url);
        return imageGetter.getImage(url, listener);
    }

	public void invalidateImageCache(String url) {
		Log.i(TAG, "Invalidate image: " + url);
		imageGetter.invalidateImageCache(url);
	}

	public RequestHandler getFile(String url,
								  File savePath,
								  ResultListener<? super File> listener,
								  ProgressListener progressListener)
	{
		Log.i(TAG, "GET <file> " + url);

		FileResponseHandler saveImageResponseHandler = new FileResponseHandler(savePath);
		saveImageResponseHandler.progressListener(progressListener);
		saveImageResponseHandler.listener(listener);

		return get(url, saveImageResponseHandler);
	}

	public RequestHandler post(String url, ResponseHandlerInterface responseHandler) {
		Log.i(TAG, "POST " + url);
		return handler(httpClient.post(url, responseHandler));
	}
	public RequestHandler post(String url, ResponseHandlerInterface responseHandler,
			RequestParams params) {
		Log.i(TAG, "POST " + url);
		return handler(httpClient.post(url, params, responseHandler));
	}
	public RequestHandler postJson(String url, String jsonString, 
			ResponseHandlerInterface responseHandler) throws UnsupportedEncodingException 
	{
		Log.i(TAG, "POST " + url);
		/* Baseado em exemplo: https://github.com/loopj/android-async-http/issues/125#issuecomment-9783689*/
		StringEntity stringEntity = jsonEntity(jsonString);

		return handler(httpClient.post(null, url, stringEntity, CONTENTTYPE_JSON,responseHandler));
	}

	public RequestHandler delete(String url, ResponseHandlerInterface responseHandler) {
		Log.i(TAG, "DELETE " + url);
		return handler(httpClient.delete(url,  responseHandler));
	}
	public RequestHandler sendJson(HttpMethods method, String url
			, String jsonString, ResponseHandlerInterface responseHandler) throws UnsupportedEncodingException 
	{
		StringEntity stringEntity = jsonEntity(jsonString);
		return sendMessage(method, url, responseHandler, stringEntity);
	}

	@NonNull
	public StringEntity jsonEntity(String jsonString) {
		StringEntity stringEntity = new StringEntity(jsonString,"UTF-8");
		stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, CONTENTTYPE_JSON));
		return stringEntity;
	}


    public RequestHandler sendMessage(HttpMethods method, String url,
                                      ResponseHandlerInterface responseHandler, HttpEntity entity)
    {
        return sendMessage(method, url, responseHandler, entity, null);
    }
    public RequestHandler sendMessage(HttpMethods method, String url,
                                      ResponseHandlerInterface responseHandler,
                                      HttpEntity entity,
                                      Header[] headers)
    {
        return handler(_sendMessage(method, url, responseHandler, entity, headers));
    }
	protected RequestHandle _sendMessage(HttpMethods method, String url,
                                         ResponseHandlerInterface responseHandler,
                                         HttpEntity entity,
                                         Header[] headers)
    {
        String contentType = getContentType(entity, headers);

		switch (method) {
		case Get:
			return httpClient.get(null, url, headers, null, responseHandler);
		case Post:
			return httpClient.post(null, url, headers, entity, contentType, responseHandler);
		case Put:
			return httpClient.put(null, url, headers, entity, contentType, responseHandler);
		case Delete:
            //TODO: how to allow headers
			return httpClient.delete(null, url, entity, contentType, responseHandler);
		default:
			throw new IllegalArgumentException("Illegal Method: " + method);
		}
	}

    protected String getContentType(HttpEntity entity, Header[] headers) {
        Header contentType = null;

        if(headers != null){
            for (Header h : headers){
                if(h.getName().equalsIgnoreCase("ContentType")){
                    contentType = h;
                    break;
                }
            }
        }

        if(contentType == null && entity != null){
            contentType = entity.getContentType();
        }

        return contentType == null ? CONTENTTYPE_JSON : contentType.getValue();
    }

    protected RequestHandler handler(RequestHandle handle){
		return new AndroidAsyncRequestHandlerAdapter(handle);
	}
}
