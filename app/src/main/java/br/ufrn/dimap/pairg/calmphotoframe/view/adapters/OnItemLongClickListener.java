package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.view.View;

/**
 */
public interface OnItemLongClickListener {
    void onItemLongClick(View view, int position, long itemId);
}
