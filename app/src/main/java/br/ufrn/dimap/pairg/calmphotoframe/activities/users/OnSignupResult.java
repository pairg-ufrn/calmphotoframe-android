package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;

public class OnSignupResult extends BasicResultListener<Object> {
	private static String DIALOG_TAG = OnSignupResult.class.getName() + ".DIALOG_TAG";
	
	BaseActivity activity;
	
	public OnSignupResult(BaseActivity activity) {
		this.activity = activity;
	}
	BaseActivity getActivity(){
		return activity;
	}
	
	@Override
	public void onResult(Object result) {
		SimpleDialogFragment dialog = new SimpleDialogFragment();
		dialog.setDialogTitle(getActivity().getText(R.string.signup_success_title));
		dialog.setDialogMessage(getActivity().getText(R.string.signup_success_message));
		dialog.setOkMessage(getActivity().getText(R.string.dialog_defaultOk));
		dialog.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
	}
}