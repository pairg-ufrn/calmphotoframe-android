package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;

public class PermissionLevelViewBuilder extends EnumViewBuilder<PermissionLevel> {
	@Override
	protected int enumToTextId(PermissionLevel data) {
		switch (data) {
		case Read:
			return R.string.permissionlevel_read;
		case Write:
			return R.string.permissionlevel_write;
		case Admin:
			return R.string.permissionlevel_admin;
		default:
			return 0;
		}
	}
}
