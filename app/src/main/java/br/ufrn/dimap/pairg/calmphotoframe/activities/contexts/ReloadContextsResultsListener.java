package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;

public class ReloadContextsResultsListener extends ActivityResultListener<Object> {
	public interface ReloadListener{
		public void reloadContent();
	}
	
	private ReloadContextsResultsListener.ReloadListener reloadListener;
	
	public ReloadContextsResultsListener(BaseActivity activity, ReloadListener reloadListener) {
		super(activity);
		this.reloadListener = reloadListener;
	}

	@Override
	public void onResult(Object result) {
		if(result != null && reloadListener != null){
			reloadListener.reloadContent();
		}
	}
}