package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetPhotoRequest extends CommonGenericRequest<Photo>{
	private int photoId;
    private boolean includePermission;

    public GetPhotoRequest(ApiConnector apiConnector, int photoId, ResultListener<? super Photo> photoListener) {
        this(apiConnector, photoId, false, photoListener);
    }
    public GetPhotoRequest(ApiConnector apiConnector, int photoId, boolean includePermission, ResultListener<? super Photo> photoListener) {
        super(apiConnector, Photo.class, photoListener);
        this.photoId = photoId;
        this.includePermission = includePermission;
    }

    @Override
    public Class<? extends Photo> getEntityClass() {
        return includePermission ? PhotoWithPermission.class : Photo.class;
    }

    @Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().photoUrl(photoId, this.includePermission);
	}
}