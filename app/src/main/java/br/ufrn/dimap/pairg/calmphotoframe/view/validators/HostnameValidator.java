package br.ufrn.dimap.pairg.calmphotoframe.view.validators;

import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class HostnameValidator {
	private EditText editor;
    private TextInputLayout editorContainer;

    private TextWatcher validateAfterTextChange;
    private View.OnFocusChangeListener validateOnLostFocus;

    public HostnameValidator(){
        validateAfterTextChange = new ValidateAfterTextChange();
        validateOnLostFocus = new ValidateOnFocusChange();
    }


    public void bindView(View rootView, int editorId){
        bindView(rootView, editorId, 0);
    }

    public void bindView(View rootView, int editorId, int editorContainerId){
        this.editor = ViewUtils.getView(rootView, editorId);
        this.editorContainer = ViewUtils.getView(rootView, editorContainerId);

        if(editor == null){
            throw new NullPointerException("Failed to find text editor view");
        }

        unbindView();

        this.editor.addTextChangedListener(validateAfterTextChange);
        this.editor.setOnFocusChangeListener(validateOnLostFocus);
    }

    protected void unbindView() {
        if(this.editor != null){
            this.editor.removeTextChangedListener(validateAfterTextChange);
            if(this.editor.getOnFocusChangeListener() == this.validateOnLostFocus){
                this.editor.setOnFocusChangeListener(null);
            }
        }
    }

    public boolean validate(){
		if(!editor.isEnabled()){
			return true;
		}
        if(editor.getText().length() == 0){
            setError(R.string.error_field_required);
            return false;
        }

		Endpoint info = parsePhotoFrameInfo();

		return info != null;
	}

    private void setError(int strId) {
        CharSequence txt = editor.getContext().getString(strId);
        if(editorContainer != null){
            editorContainer.setError(txt);
        }
        else{
            editor.setError(txt);
        }
    }

    protected void clearError() {
        editor.setError(null);
        if(editorContainer != null){
            editorContainer.setError(null);
            editorContainer.setErrorEnabled(false);
        }
    }

    @Nullable
	public Endpoint parsePhotoFrameInfo() {
        if(editor == null){
            return null;
        }

        CharSequence text = editor.getText();

		ServerInfo serverInfo = ServerInfo.fromText(text);

		String address = (serverInfo == null ? editor.getText().toString() : serverInfo.getAddress());

		Endpoint info = ApiEndpoint.fromUrl(address);
		if(info == null){
			setError(R.string.error_invalidUrl);
		}
		return info;
	}

    private class ValidateAfterTextChange implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() == 0 && editor.hasFocus()) {
                //has focus but content is empty
                clearError();
            }
            else{
                validate();
            }
        }
    }

    private class ValidateOnFocusChange implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus) { //lost focus
                validate();
            }
            else{
                if(editor.getText().length() == 0){
                    clearError();
                }
            }
        }
    }
}