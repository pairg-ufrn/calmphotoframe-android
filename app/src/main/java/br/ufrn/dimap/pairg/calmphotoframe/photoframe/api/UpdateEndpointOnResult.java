package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;

public class UpdateEndpointOnResult extends CommonListenerDelegator<ApiEndpoint> {
    ApiEndpointHolder holder;

    public UpdateEndpointOnResult(ApiEndpointHolder holder, ResultListener<? super ApiEndpoint> listener) {
        super(listener);
        this.holder = holder;
    }

    @Override
    protected boolean interceptResult(ApiEndpoint result) {
        holder.set(result);
        return super.interceptResult(result);
    }
}
