package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.util.Log;

import java.io.File;
import java.io.IOException;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ChooseFileCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.PhotoJsonFileParser;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SaveDialog.SaveDialogResponseListener;

public class ExportPhotoCommand extends ActivityStatelessCommand<File> {
	private static final String EXPORT_DIALOG_TAG = "EXPORT_DIALOG_TAG";
	private PhotoHolder photoHolder;
	
	public ExportPhotoCommand(BaseActivity activity, PhotoHolder photoHolder) {
		super(activity);
		this.photoHolder = photoHolder;
	}

	@Override
	public void execute(ResultListener<? super File> listener) {
		exportPhotoDescription(listener);
	}

	private void exportPhotoDescription(ResultListener<? super File> listener) {
		Photo photoToExport = photoHolder.getCurrentPhoto();
		
    	SaveDialog dialogFragment = new SaveDialog();
    	dialogFragment.setDialogMessage(getContext().getResources().getString(R.string.export_photo_dialog_message));
    	dialogFragment.setDialogResponseListener(new ExportPhotoDialogResponseListener(photoToExport, listener));
    	dialogFragment.show(getActivity().getSupportFragmentManager(), EXPORT_DIALOG_TAG);
	}

	/* ****************************************************************************************/
	private final class ExportPhotoDialogResponseListener implements SaveDialogResponseListener {
		private Photo photoToExport;
        private ResultListener<? super File> listener;
        private PhotoJsonFileParser parser = new PhotoJsonFileParser();
		private ChooseFileCommand chooseFileCommand;
        private ResultListener<? super String[]> onChooseDir;

		public ExportPhotoDialogResponseListener(Photo photoToExport, final ResultListener<? super File> listener) {
			this.photoToExport = photoToExport;
            this.listener = listener;

            this.chooseFileCommand = new ChooseFileCommand(getActivity());
            this.onChooseDir = Listeners.join(
                new OnResultSetSaveDialogPath(getActivity(), EXPORT_DIALOG_TAG),
                new BasicResultListener<Object>(){
                    @Override
                    protected void handleError(Throwable error) {
                        Listeners.onFailure(listener, error);
                    }
                }
            );
		}
		
		@Override
		public void onCancel() {
			Log.d(getClass().getName(), "Cancel import");
		}

		@Override
		public void onSelectFile(File exportPath) {
			if(photoToExport != null){
                writePhoto(exportPath);
            }
		}

        private void writePhoto(File exportPath) {
            try {
                parser.writePhoto(photoToExport, exportPath);
                notifyOnResult(exportPath, listener);
            } catch (IOException e) {
                e.printStackTrace();
                notifyOnFailure(e, listener);
            }
        }

        @Override
		public void onChoosePath(String currentPath) {
			showFileChooser(currentPath, true);
		}

        private void showFileChooser(String startPath, boolean selectDir) {
            chooseFileCommand.execute(
                    new ChooseFileCommand.FileChooserArguments(startPath, selectDir),
                    onChooseDir);
        }
	}

}
