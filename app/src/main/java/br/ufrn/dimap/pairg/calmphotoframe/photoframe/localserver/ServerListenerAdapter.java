package br.ufrn.dimap.pairg.calmphotoframe.photoframe.localserver;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerFinishEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.ServerStatusListener;

/**
 */
public class ServerListenerAdapter implements ServerStatusListener {
    private ResultListener<? super ServerStartEvent> listener;
    private ResultListener<? super ServerFinishEvent> disconnectListener;

    public ServerListenerAdapter() {
        this.listener = null;
        this.disconnectListener = null;
    }

    public ServerListenerAdapter onConnect(ResultListener<? super ServerStartEvent> listener) {
        this.listener = listener;
        return this;
    }

    public ServerListenerAdapter onDisconnect(ResultListener<? super ServerFinishEvent> listener) {
        this.disconnectListener = listener;
        return this;
    }


    @Override
    public void onServerStart(ServerStartEvent startEvent) {
        Listeners.onResult(listener, startEvent);
    }

    @Override
    public void onServerStop(ServerFinishEvent finishEvent) {
        Listeners.onResult(disconnectListener, finishEvent);
    }

    @Override
    public boolean isListening() {
        return true;
    }
}
