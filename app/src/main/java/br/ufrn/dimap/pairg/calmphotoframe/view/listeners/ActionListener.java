package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

public interface ActionListener<T> {
    void execute(T value);
}
