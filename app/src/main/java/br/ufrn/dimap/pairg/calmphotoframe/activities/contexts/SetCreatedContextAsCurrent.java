package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;

public class SetCreatedContextAsCurrent extends BasicResultListener<PhotoContext> {
	@Override
	public void onResult(PhotoContext result) {
		ApiClient.instance().setCurrentContext(result);
	}
}