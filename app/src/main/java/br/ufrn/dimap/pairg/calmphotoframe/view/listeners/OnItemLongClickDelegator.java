package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

import android.view.View;

import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.OnItemLongClickListener;

/**
 */
public class OnItemLongClickDelegator extends ListenerDelegator<OnItemLongClickListener>
        implements OnItemLongClickListener {

    @Override
    public void onItemLongClick(View view, int position, long itemId) {
        for (OnItemLongClickListener listener : delegates) {
            listener.onItemLongClick(view, position, itemId);
        }
    }
}
