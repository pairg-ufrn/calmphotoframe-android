package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu;

import android.view.View;

/**
 */
public class CloseFabMenuOnClickListener implements View.OnClickListener {
    FabMenu menu;

    public CloseFabMenuOnClickListener(FabMenu menu) {
        this.menu = menu;
    }

    @Override
    public void onClick(View v) {
        menu.collapse();
    }
}
