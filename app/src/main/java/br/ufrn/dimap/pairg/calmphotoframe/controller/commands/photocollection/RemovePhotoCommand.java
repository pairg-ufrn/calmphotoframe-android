package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CancellableCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class RemovePhotoCommand implements CancellableCommand<Photo, Photo> {

    @Override
    public RequestHandler execute(Photo photo, ResultListener<? super Photo> listener) {
        return ApiClient.instance().photos().remove(photo, listener);
    }
}
