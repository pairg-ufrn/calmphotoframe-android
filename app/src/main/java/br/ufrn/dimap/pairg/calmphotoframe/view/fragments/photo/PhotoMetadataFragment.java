package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoLocation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMetadata;

public class PhotoMetadataFragment extends Fragment{
	private Photo photo;
	private final Map<Integer, Integer>  txtToLabel;
	private final Map<Integer, TextView> metadataTxtViews;
	private final Map<Integer, TextView> metadataLabelViews;
	private final Map<Integer, TextView> metadataGroupsHeaders;

	private OnClickListener showLocationOnClickListener;
	
	private boolean viewsCreated;
	
	public PhotoMetadataFragment() {
		metadataTxtViews = new HashMap<>();
		metadataLabelViews = new HashMap<>();
		txtToLabel = new HashMap<>();
		metadataGroupsHeaders = new HashMap<>();
		
		viewsCreated = false;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_photometadata, container,false);
		setupView(rootView);
		
		return rootView;
	}
	private void setupView(View rootView) {
		retrieveViews(rootView);
		
		showLocationOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				PhotoLocation location = getPhotoLocation();
				if(location != null){
					showMap(location);
				}
			}
		};
		
		updateView();
	}

	private void retrieveViews(View rootView) {
		putTextView(rootView, R.id.photoMetadata_cameraGroup, 
				R.id.photoMetadata_cameraMakerTxt , R.id.photoMetadata_cameraMakerLbl);
		putTextView(rootView, R.id.photoMetadata_cameraGroup, 
				R.id.photoMetadata_cameraModelTxt , R.id.photoMetadata_cameraModelLbl);
		putTextView(rootView, R.id.photoMetadata_fileGroup, 
				R.id.photoMetadata_filenameTxt    , R.id.photoMetadata_filenameLbl);
		putTextView(rootView, R.id.photoMetadata_fileGroup, 
				R.id.photoMetadata_imageSizeTxt   , R.id.photoMetadata_imageSizeLbl);
		putTextView(rootView, R.id.photoMetadata_localizationGroup, 
				R.id.photoMetadata_latitudeTxt    , R.id.photoMetadata_latitudeLbl);
		putTextView(rootView, R.id.photoMetadata_localizationGroup, 
				R.id.photoMetadata_longitudeTxt   , R.id.photoMetadata_longitudeLbl);
		putTextView(rootView, R.id.photoMetadata_localizationGroup, 
				R.id.photoMetadata_originalDateTxt, R.id.photoMetadata_originalDateLbl);
		putTextView(rootView, R.id.photoMetadata_creditsGroup, 
				R.id.photoMetadata_credisTxt, 0);
		
		viewsCreated = true;
	}
	
	protected void showMap(PhotoLocation location) {
		Uri geoUri = buildLocationUri(location.getLatitude(), location.getLongitude(), "");
		
		Log.d(getClass().getName(), "Uri: " + geoUri);
		
		Intent intent = new Intent(Intent.ACTION_VIEW);
	    intent.setData(geoUri);
	    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
	        startActivity(intent);
	    }

	}

	private Uri buildLocationUri(Double latitude, Double longitude, String label) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("geo:");
		builder.append(latitude).append(",").append(longitude);
		if(label != null){
			builder.append("?q=").append(latitude).append(",").append(longitude)
								 .append("(").append(label).append(")");		
		}
		
		return Uri.parse(builder.toString());
	}

	protected PhotoLocation getPhotoLocation() {
		if(photo != null && photo.getMetadata() != null){
			return photo.getMetadata().getLocation();
		}
		return null;
	}

	private void putTextView(View rootView, int metadataGroup
			, int viewId, int labelViewId)
	{
		metadataTxtViews.put(viewId, (TextView)rootView.findViewById(viewId));
		if(labelViewId != 0){
			metadataLabelViews.put(labelViewId, (TextView)rootView.findViewById(labelViewId));
		}
		metadataGroupsHeaders.put(metadataGroup, (TextView)rootView.findViewById(metadataGroup));
		
		txtToLabel.put(viewId, labelViewId);
	}

	public Photo getPhotoDescriptor() {
		return this.photo;
	}
	public void setPhotoDescriptor(Photo descriptor) {
		this.photo = descriptor;
		updateView();
	}

	protected void updateView() {
		if(!isAdded() || !viewsCreated){
			return;
		}
		
		clearViews();
		if(photo != null && photo.getMetadata() != null){
			PhotoMetadata metadata = photo.getMetadata();
			putText(metadata.getCameraMaker()   	, R.id.photoMetadata_cameraMakerTxt);
			putText(metadata.getCameraModel()   	, R.id.photoMetadata_cameraModelTxt);
			putText(metadata.getOriginalFilename() 	, R.id.photoMetadata_filenameTxt);
			putText(metadata.getImageSize()/1024	, R.id.photoMetadata_imageSizeTxt);
			putText(metadata.getOriginalDate()  	, R.id.photoMetadata_originalDateTxt);
			putText(photo.getCredits()				, R.id.photoMetadata_credisTxt);
			updateLocalizationView(metadata);
		}
	}

	private void updateLocalizationView(PhotoMetadata metadata) {
		TextView localizationGroupView = metadataGroupsHeaders.get(R.id.photoMetadata_localizationGroup);
		if(metadata.getLocation() != null){
			setupLocalizationText(localizationGroupView);
			
			putText(metadata.getLocation().getLatitude() , R.id.photoMetadata_latitudeTxt);
			putText(metadata.getLocation().getLongitude(), R.id.photoMetadata_longitudeTxt);
		}
	}


	private void setupLocalizationText(TextView localizationGroupView) {
		CharSequence headerText = getText(R.string.photoMetadata_localizationGroup);
	    SpannableString spannableString = new SpannableString(headerText);
	    spannableString.setSpan(new ClickableSpan() {
			@Override
			public void onClick(View widget) {
				showLocationOnClickListener.onClick(widget);
			}
		}, 0, headerText.length(), 0);
	    localizationGroupView.setMovementMethod(LinkMovementMethod.getInstance());
	    localizationGroupView.setText(spannableString);
	}

	private void clearViews() {
		putText(null, R.id.photoMetadata_cameraMakerTxt);
		putText(null, R.id.photoMetadata_cameraModelTxt);
		putText(null, R.id.photoMetadata_filenameTxt);
		putText(null, R.id.photoMetadata_imageSizeTxt);
		putText(null, R.id.photoMetadata_originalDateTxt);
		putText(null, R.id.photoMetadata_latitudeTxt);
		putText(null, R.id.photoMetadata_longitudeTxt);
		putText(null, R.id.photoMetadata_credisTxt);
		TextView localizationGroupView = metadataGroupsHeaders.get(R.id.photoMetadata_localizationGroup);
		localizationGroupView.setText(getText(R.string.photoMetadata_localizationGroup));
	}

	private void putText(Object value, int viewId) {
		TextView view = this.metadataTxtViews.get(viewId);
		Integer labelViewId = this.txtToLabel.get(viewId);
		TextView labelView = this.metadataLabelViews.get(labelViewId);
		
		if(view != null){
			if(value != null){
				view.setText(value.toString());
				view.setVisibility(View.VISIBLE);
				if(labelView != null){
					labelView.setVisibility(View.VISIBLE);
				}
			}
			else{
				view.setText("");
				view.setVisibility(View.GONE);
				if(labelView != null){
					labelView.setVisibility(View.GONE);
				}
			}
		}
	}
}
