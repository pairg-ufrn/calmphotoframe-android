package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Permission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PermissionLevelAdapter;

public class CollaboratorViewBinder extends InflaterViewBinder<Collaborator> {
	public ImageButton deleteButton;
	public Spinner permissionSpinner;
	public PermissionLevelAdapter permissionAdapter;
	private boolean enabled;
	private boolean isCurrentUser;
	private User user;
	
	private UserViewBinder userViewBinder;

	private OnClickListener onClickDeleteListener;
//	private OnItemSelectedListener onChangePermissionSelector;

	public CollaboratorViewBinder() {
		super(R.layout.view_collaborator);
		userViewBinder = new UserViewBinder();
		isCurrentUser = false;
	}

	@Override
	public void setup(View itemView) {
		this.userViewBinder.setup(itemView);

		deleteButton = ViewUtils.getView(itemView, R.id.colaborator_remove);
		permissionSpinner = ViewUtils.getView(itemView, R.id.colaborator_permission);
		this.permissionAdapter = new PermissionLevelAdapter();
		permissionSpinner.setAdapter(permissionAdapter);
	}

	@Override
	public void bind(Collaborator collaborator) {
		this.user = collaborator.getUser();
		userViewBinder.bind(this.user);
		if(getIsCurrentUser()){
			bindUserName();
		}
		
		setPermissionSelection(collaborator.getPermission());
	}

	public OnClickListener getOnClickDeleteListener() {
		return onClickDeleteListener;
	}

	public void setOnClickDeleteListener(OnClickListener onClickDeleteListener) {
		this.onClickDeleteListener = onClickDeleteListener;
//		if(deleteButton != null){
//			deleteButton.setOnClickListener(onClickDeleteListener);
//		}
		deleteButton.setOnClickListener(onClickDeleteListener);
	}

	public OnItemSelectedListener getOnChangePermissionSelector() {
//		return onChangePermissionSelector;
		return permissionSpinner.getOnItemSelectedListener();
	}

	public void setOnChangePermissionSelector(OnItemSelectedListener onChangePermissionSelector) {
//		this.onChangePermissionSelector = onChangePermissionSelector;
//		if(permissionSpinner != null){
//			permissionSpinner.setOnItemSelectedListener(onChangePermissionSelector);
//		}
		permissionSpinner.setOnItemSelectedListener(onChangePermissionSelector);
	}

	public void setPermissionSelection(PermissionLevel permissionLevel) {
		int selectionIndex = (permissionLevel == null) ? 0 : permissionAdapter.indexOf(permissionLevel);
		permissionSpinner.setSelection(selectionIndex);
	}

	public void setPermissionSelection(Permission permission) {
		setPermissionSelection(permission == null ? null : permission.getLevel());
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;

		setViewsEnabled(enabled);
	}

	public boolean getIsCurrentUser(){
		return isCurrentUser;
	}
	public void setIsCurrentUser(boolean isCurrentUser) {
		if(isCurrentUser != this.isCurrentUser){
			this.isCurrentUser = isCurrentUser;
			bindUserName();
		}
	}

	private void bindUserName() {
		TextView userName = userViewBinder.getUserNameView();
		if(userName == null){
			return;
		}
		
		if(getIsCurrentUser()){
			userName.setText(R.string.currentUsername);
		}
		else if(user != null){
			userName.setText(user.getLogin());
		}
		else{
			userName.setText("");
		}
	}
	
	protected void setViewsEnabled(boolean enabled) {
		deleteButton.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
//		DrawableUtils.convertToGrayscale(deleteButton.getDrawable(), enabled);
		
		permissionSpinner.setEnabled(enabled);
		userViewBinder.setEnabled(enabled);
		permissionAdapter.setEnabled(enabled);
	}
}