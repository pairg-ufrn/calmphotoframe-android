package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.LinkedList;
import java.util.List;

/**
 * Use a ViewGroup as a RadioGroup and allows custom layouts with RadioButtons.
 *
 * Warning: this class override onClickListener on each RadioButton that is under the monitored view hierarchy.
 * Warning: does not use
 *
 * Based on code from: http://www.codemansion.com/2011/11/custom-radiogroup-layout-on-android.html?showComment=1365844419926#c6455609773398237738
 * and https://github.com/worker8/RadioGroupPlus
 */
public class RadioGroupManager implements RadioButton.OnCheckedChangeListener, ViewHierarchyVisitor.ViewVisitor, View.OnClickListener {

    private final List<CompoundButton> buttonList = new LinkedList<>();
    private final ViewHierarchyVisitor hierarchyVisitor = new ViewHierarchyVisitor();
    private CompoundButton checkedButton = null;

    private ViewGroup groupRoot;

    public ViewGroup getGroupRoot(){
        return groupRoot;
    }
    public void setGroupRoot(ViewGroup viewGroup){
        if(this.groupRoot == viewGroup){
            return;
        }
        if(this.groupRoot != null){
            clearButtons();
        }

        this.groupRoot = viewGroup;
        this.notifyChangeOnHierarchy();
        this.notifyChangeOnViewsState();
    }

    public void clearButtons() {
        for(CompoundButton btn : buttonList){
            btn.setOnClickListener(null);
        }
        this.buttonList.clear();
    }

    public void notifyChangeOnHierarchy(){
        this.clearButtons();
        this.hierarchyVisitor.visit(this.groupRoot, this);
    }

    @Override
    public boolean onVisit(View view, View parent, View root) {
        if(view instanceof RadioButton){
            this.buttonList.add((RadioButton)view);
            view.setOnClickListener(this);
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if(!this.buttonList.contains(v)){
            return;
        }

        CompoundButton button = (CompoundButton)v;

        //Selected other button
        boolean changedChecked = (button != this.checkedButton);
        //Tried deselect the current selected button
        boolean tryDeselectChecked = (button == this.checkedButton && !button.isChecked());

        if(changedChecked || tryDeselectChecked)
        {
            this.setCheckedButton(button);
        }
    }

    public void notifyChangeOnViewsState(){
        //Set the first checked button as the checked, and deselect others
        this.setCheckedButton(null);
    }

    public void setCheckedButton(CompoundButton checkedButton){
        if(checkedButton != null){
            checkedButton.setChecked(true);
        }

        for(CompoundButton btn: this.buttonList){
            if(btn.isChecked() && btn != checkedButton){
                if(checkedButton == null){
                    checkedButton = btn;
                }
                else{//Already has a selected button, so deselect this
                    btn.setChecked(false);
                }
            }
        }


        this.checkedButton = checkedButton;
    }

    public void onCheckedChanged(CompoundButton buttonView,
                                 boolean isChecked) {
        if (isChecked) {
            checkedButton = buttonView;
            for(CompoundButton cb : buttonList) {
                cb.setChecked(false);
            }
            buttonView.setChecked(true);
        }

    }

    public CompoundButton getCheckedButton() {
        return checkedButton;
    }
}
