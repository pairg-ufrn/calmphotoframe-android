package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.pickimages.FinishImageSelectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.PicassoLocalImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.Image;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.LoadGalleryImagesCommand;
import br.ufrn.dimap.pairg.calmphotoframe.view.AutoFitGridLayoutManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.Size;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.CheckSelectedItemDecorator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator.FragmentBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.RecycleGridViewConfigurator;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ImageViewBinder;

public class PickImagesActivity extends BaseActivity implements OnSelectItemListener<Image>{
	private static final String THUMBNAILS_FRAGMENT = PickImagesActivity.class.getName() + ".ThumbnailsFragment";
	
	public static final String EXTRA_SELECTED_IMAGES = "EXTRA_SELECTED_IMAGES";
	
	private boolean multiSelectionEnabled;

    private ItemsRecycleFragment<Image> imagesFragment;

	public boolean isMultiSelectionEnabled() {
		return multiSelectionEnabled;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_toolbar_fragment);
		setupToolbar();

        this.multiSelectionEnabled = getAllowMultipleExtra();

        imagesFragment = buildView();
        setupImageSelection(imagesFragment);

		loadImages();
	}

    private boolean getAllowMultipleExtra() {
        Intent args = getIntent();
        return args != null &&
                args.getBooleanExtra(Intents.Extras.allowMultiple(), false);
    }

    @Override
    protected void configureToolbar(Toolbar t) {
        super.configureToolbar(t);

        t.setNavigationIcon(R.drawable.ic_close);
        t.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    private void loadImages() {
        new LoadGalleryImagesCommand(this).execute(new OnLoadImagesListener());
	}

    protected void setImages(Collection<Image> result) {
        imagesFragment.getAdapter().items().set(result);
    }

    private ItemsRecycleFragment<Image> buildView() {
        FragmentCreator fragCreator = new FragmentCreator(getSupportFragmentManager());

        return fragCreator.addFragment(R.id.fragment_area,
                                        THUMBNAILS_FRAGMENT,
                                        new ImagesFragmentBuilder(this, isMultiSelectionEnabled()));
    }

    private void setupImageSelection(ItemsRecycleFragment<Image> imagesFragment) {
        imagesFragment.getAdapter().getSelectionManager().setMultiSelectionEnabled(isMultiSelectionEnabled());
        if(isMultiSelectionEnabled()){
            imagesFragment.getActionModeCompat().setActionModeCallback(buildActionModeCallback());
        }
        else{
            imagesFragment.addOnSelectItemListener(this);
        }
    }

    @NonNull
    private ImageSelectionActionMode buildActionModeCallback() {
        ItemSelector<Image> selector = imagesFragment.getAdapter();
        return new ImageSelectionActionMode(this, selector, multiSelectionEnabled);
    }

	/* **********************************  OnSelectItemListener ***********************************/

	@Override
	public void onItemSelection(final Image image) {
		if(!isMultiSelectionEnabled()){
            ItemSelector<Image> selector = new ItemSelector<Image>() {
                @Override
                public Collection<? extends Image> getSelectedItems() {
                    return null;
                }
                @Override
                public Image getSelectedItem() {
                    return image;
                }
            };

			new FinishImageSelectionCommand(this, selector).execute(null);
		}
	}

	/* ************************************** Helper Classes **************************************/

    public static class ImageSelectionActionMode extends BaseActionModeCallback {
        public ImageSelectionActionMode(BaseActivity activity, ItemSelector<Image> imageSelector,
                                        boolean multiSelectionEnabled)
        {
            super(R.menu.menu_simple_confirm);

            addCommands(activity, imageSelector, multiSelectionEnabled);
        }

        private void addCommands(BaseActivity activity, ItemSelector<Image> imageSelector, boolean multiSelectionEnabled){

            Command<?> finishSelectionCmd = new FinishImageSelectionCommand(activity, imageSelector)
                    .setMultiSelection(multiSelectionEnabled);
            this.putMenuCommand(R.id.action_confirm, finishSelectionCmd);
        }
    }


    private static class ImagesFragmentBuilder implements FragmentBuilder {
        Context context;
        boolean selectOnClick;

        public ImagesFragmentBuilder(Context context, boolean selectOnClick) {
            this.context = context;
            this.selectOnClick = selectOnClick;
        }

        @Override
        public Fragment buildFragment() {
            final ItemsRecycleFragment frag = new ItemsRecycleFragment<>();

            final RecycleGridViewConfigurator gridViewConf = new RecycleGridViewConfigurator(frag);
            frag.addViewConfigurator(gridViewConf);
            frag.setBinderBuilder(buildBinderBuilder(frag, gridViewConf));

            ItemsRecycleAdapter<Image> adapter = frag.getAdapter();
            adapter.getSelectionManager().setSelectOnClick(selectOnClick);
            adapter.addDecorator(new CheckSelectedItemDecorator<>(adapter.getSelectionManager(), adapter));
            adapter.setIdGenerator(new ImageIdGenerator());

            return  frag;
        }

        @NonNull
        protected EntityGetter<Size> buildSizeGetter(RecycleGridViewConfigurator gridViewConf) {
            return new SquaredSizeGetter(gridViewConf.getLayoutManager());
        }

        @NonNull
        protected ImageBinderBuilder buildBinderBuilder(ItemsRecycleFragment frag,
                                                        RecycleGridViewConfigurator gridViewConf)
        {
            EntityGetter<Size> sizeGetter = buildSizeGetter(gridViewConf);

//            ImageLoaderScrollableDecorator.ScrollMonitor monitor = new RecyclerFragmentScrollMonitor(frag);

            ImageLoader<Image> loader = new PicassoLocalImageLoader(context).setSize(sizeGetter);

//            return new ImageBinderBuilder(new ImageLoaderScrollableDecorator<>(loader, monitor));
            return new ImageBinderBuilder(loader);
        }

        private static class ImageIdGenerator implements ItemsRecycleAdapter.IdGenerator<Image> {
            @Override
            public long getId(Image item, ItemsManager<Image> items) {
                return item.getImageId();
            }

            @Override
            public boolean hasStableIds() {
                return true;
            }
        }

    }

    private static class SquaredSizeGetter implements EntityGetter<Size> {
        AutoFitGridLayoutManager layoutManager;
        Size size;

        public SquaredSizeGetter(AutoFitGridLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
            size = new Size();
        }

        @Override
        public Size getEntity() {
            size.setWidth(layoutManager.getRealColumnWidth());
            size.setHeight(layoutManager.getRealColumnWidth());
            return size;
        }
    }

    private static class ImageBinderBuilder implements ItemsRecycleAdapter.BinderBuilder<Image> {
        ImageLoader imageLoader;

        public ImageBinderBuilder(ImageLoader<Image> imageLoader) {
            this.imageLoader = imageLoader;
        }

        @Override
        public BinderViewHolder<Image> buildViewBinder() {
            return new ImageViewBinder<Image>()
                        .setLoader(imageLoader)
                        .setSquared(true);
        }
    }

    private class OnLoadImagesListener extends BasicResultListener<Collection<Image>> {
        @Override
        public void onResult(Collection<Image> result) {
            setImages(result);
        }

        @Override
        public void onFailure(Throwable error) {
            notifyError("Failed to load gallery images.");
        }
    }
}
