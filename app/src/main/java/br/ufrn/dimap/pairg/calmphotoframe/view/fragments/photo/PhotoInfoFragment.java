package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMarker;
import br.ufrn.dimap.pairg.calmphotoframe.utils.FragmentUtils;

public class PhotoInfoFragment extends Fragment {
	private TextView imageDescriptionView;
	private TextView imageMomentView;
	private TextView imagePlaceView;
	private TextView imageDateView;
	
	private Photo photoDescriptor;
	
	private PhotoMarkersViewerFragment photoMarkersViewerFragment;
	private boolean viewsCreated;
	
	private Bitmap imageCache;
	
	public PhotoInfoFragment(){
		super();
		photoDescriptor = null;
		viewsCreated = false;
		
		imageCache = null;
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photoinfo, container, false);
        setupViews(rootView);

        return rootView;
    }

	public void setPhoto(Photo descriptor) {
		this.photoDescriptor = descriptor == null ? null : descriptor.clone();
		populateViewsFromDescriptor();
	}
	public Photo getPhoto(){
		return photoDescriptor;
	}

	private void setupViews(View rootView) {
        createViews(rootView);
    	populateViewsFromDescriptor();
	}

	private void createViews(View rootView) {
		imageDescriptionView = (TextView)rootView.findViewById(R.id.image_description);
        imageMomentView = (TextView)rootView.findViewById(R.id.frag_imginfo_moment);
        imageDateView = (TextView)rootView.findViewById(R.id.frag_imginfo_date);
        imagePlaceView = (TextView)rootView.findViewById(R.id.frag_imginfo_location);
        
        setupPhotoMarkersViewer();
        
        this.viewsCreated = true;
	}
	private void setupPhotoMarkersViewer() {
		photoMarkersViewerFragment = FragmentUtils.findById(getChildFragmentManager(), R.id.photoinfo_markersviewer);
		
		photoMarkersViewerFragment.setEditable(false);
    	photoMarkersViewerFragment.setUndefinedLabelText(getString(R.string.photoInfo_UndefinedLabelText));
    	
    	if(imageCache != null){
    		photoMarkersViewerFragment.setImage(imageCache);
    	}
	}
	
	//TODO: para os campos nulos ou vazios, esconder view
	void populateViewsFromDescriptor(){
		if(!viewsCreated || photoDescriptor == null){
			return;
		}
		this.imageDescriptionView.setText(photoDescriptor.getDescription());
		this.imageMomentView.setText(photoDescriptor.getMoment());
		this.imageDateView.setText(photoDescriptor.getDate());
		this.imagePlaceView.setText(photoDescriptor.getPlace());

		populateViewsFromMarks(photoDescriptor.getMarkers());
	}
	
	
	private void populateViewsFromMarks(List<PhotoMarker> photoMarks) {
		this.photoMarkersViewerFragment.setPhotoMarkers(photoMarks);
	}

	public void setImage(int imageResId) {
		Bitmap image = BitmapFactory.decodeResource(getResources(), imageResId);
		this.setImage(image);
	}
	public void setImage(Bitmap bitmap) {
		this.imageCache = bitmap;
		if(photoMarkersViewerFragment != null){
			this.photoMarkersViewerFragment.setImage(bitmap);
		}
	}
	public void showDefaultImage() {
		this.photoMarkersViewerFragment.showDefaultImage();
	}
}
