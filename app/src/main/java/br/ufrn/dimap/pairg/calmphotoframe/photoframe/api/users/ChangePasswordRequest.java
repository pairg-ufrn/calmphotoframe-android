package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import java.io.Serializable;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ChangePasswordRequest extends CommonGenericRequest<Object>{
    PasswordHolder passwordHolder;

    public ChangePasswordRequest(ApiConnector apiConnector, String newPassword) {
        super(apiConnector, Object.class);

        this.passwordHolder = new PasswordHolder(newPassword);
    }

    @Override
    public RequestHandler execute() {
        return postJsonRequest(passwordHolder);
    }

    @Override
    protected String genUrl() {
        return getApiBuilder().myPasswordUrl();
    }

    public static class PasswordHolder implements Serializable{
        public String password;

        public PasswordHolder(String password) {
            this.password = password;
        }
    }
}
