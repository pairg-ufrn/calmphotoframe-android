package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.content.Intent;
import android.os.Build;

public class Intents {
	public static class Extras{
		public static final String CLASS = Extras.class.getName();
		public static final String Photo				= CLASS + ".Photo";
		public static final String PhotoContext  		= CLASS + ".PhotoContext";
		public static final String PhotoCollection 		= CLASS + ".PhotoCollection";
		public static final String PhotoCollectionId 	= CLASS + ".PhotoCollectionId";
		public static final String SelectedPhoto     	= CLASS + ".SelectedPhoto";
		public static final String SelectedCollection 	= CLASS + ".SelectedPhotoCollection";
		public static final String SelectedContext	 	= CLASS + ".SelectedPhotoContext";
		public static final String Query 				= CLASS + ".Query";

        public static final String AllowMultiple        = "android.intent.extra.ALLOW_MULTIPLE";

        public static String allowMultiple() {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                    ? Intent.EXTRA_ALLOW_MULTIPLE
                    : AllowMultiple;

        }
	}

}
