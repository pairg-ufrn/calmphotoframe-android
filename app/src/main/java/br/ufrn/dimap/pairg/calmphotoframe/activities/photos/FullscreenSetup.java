package br.ufrn.dimap.pairg.calmphotoframe.activities.photos;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen.FullscreenLayout;

public class FullscreenSetup {

	public static void setupFullscreen(Activity activity, int fullscreenView){
		FullscreenLayout fullscreenLayout =  (FullscreenLayout)activity.findViewById(fullscreenView);
		
		String fullscreenPrefKey = activity.getString(R.string.preferences_fullscreen);
		String autoHideDelayKey = activity.getString(R.string.preferences_fullscreen_autohidedelay);
		
		SharedPreferences sharedPref = activity.getSharedPreferences(fullscreenPrefKey, Context.MODE_PRIVATE);
		int defaultValue = activity.getResources().getInteger(R.integer.default_fullscreen_autohidelay);
		int autoHideDelay = sharedPref.getInt(autoHideDelayKey, defaultValue);
		
		fullscreenLayout.setAutoFullscreen(true);
		fullscreenLayout.setAutoFullscreenMillisecondsDelay(autoHideDelay);
		fullscreenLayout.setToggleOnGesture(FullscreenLayout.Gestures.TAP);
		fullscreenLayout.setShowGesture(FullscreenLayout.Gestures.NONE);
	}
}
