package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Integer statusCode;
	private String url;

    public static Throwable getError(int statusCode, String message, Throwable error, String url) {
        if(statusCode >= 300 && statusCode < 600){
            ApiException exception = new ApiException(message, error, statusCode);
            if(url != null){
                exception.setUrl(url);
            }
            return exception;
        }
        return error;
    }

    /** {@inheritDoc} */
    public ApiException() {
    	super();
    }

    /** {@inheritDoc} */
    public ApiException(String detailMessage) {
        super(detailMessage);
    }

    /** {@inheritDoc} */
    public ApiException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /** {@inheritDoc} */
    public ApiException(Throwable throwable) {
        super(throwable);
    }

	public ApiException(String message, Throwable error, int statusCode) {
		super(message, error);
		this.statusCode = statusCode;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public ApiException setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
        return this;
	}

    public String getUrl() {
        return url;
    }

    public ApiException setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
	public String toString() {
        StringBuilder builder = new StringBuilder();
		builder.append("Api error");

		if(getStatusCode() != null){
            builder.append("(").append(getStatusCode()).append(")");
		}
        if(getUrl() != null){
            builder.append(" with url \"").append(getUrl()).append("\"");
        }

        builder.append(": ").append(super.toString());
        return builder.toString();

	}
}
