package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache;

public class CacheFactory {
	public interface CacheBuilder{
		<K,V> Cache<K,V> buildCache(int maxSize);
	}
	private static CacheFactory singleton;
	public static CacheFactory instance() {
		if (singleton == null) {
			singleton = new CacheFactory();
		}
		return singleton;
	}
	
	private CacheBuilder cacheBuilder;

	public CacheFactory() {
		this(new MemoryCacheBuilder());
	}
	public CacheFactory(CacheBuilder builder) {
		this.cacheBuilder = builder;
	}
	
	public CacheBuilder getCacheBuilder() {
		return cacheBuilder;
	}

	public void setCacheBuilder(CacheBuilder cacheBuilder) {
		this.cacheBuilder = cacheBuilder;
	}
	
	public <K,V> Cache<K,V> buildCache(int maxSize){
		return this.cacheBuilder.buildCache(maxSize);
	}
}
