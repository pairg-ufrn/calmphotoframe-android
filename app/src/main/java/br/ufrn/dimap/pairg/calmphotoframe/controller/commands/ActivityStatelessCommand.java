package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import android.content.Context;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;

public abstract class ActivityStatelessCommand<T> extends BaseStatelessCommand<T>{
	private BaseActivity activity;

	public ActivityStatelessCommand(BaseActivity activity) {
		super();
		this.activity = activity;
	}
	public BaseActivity getActivity() {
		return activity;
	}
	public void setActivity(BaseActivity activity) {
		this.activity = activity;
	}
	public Context getContext(){
		return getActivity();
	}
}
