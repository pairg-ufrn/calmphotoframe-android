package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;

public class GetCollectionAndContextCommand extends BaseStatelessCommand<CollectionAndContextContainer>{
//	private Intent dataIntent;
	private Long contextId, collectionId;
	private PhotoContext photoContext;
	private PhotoCollection photoCollection;

//	public GetCollectionAndContextCommand(Intent dataIntent, Long photoContextId, Long photoCollectionId) {
//		this.dataIntent = dataIntent;
//		this.contextId = photoContextId;
//		this.collectionId = photoCollectionId;
//
//		if(collectionId == null){
//			PhotoCollection coll = getCollectionFromIntent(dataIntent);
//			if(coll != null){
//				collectionId = coll.getId();
//			}
//		}
//	}

    public GetCollectionAndContextCommand collection(PhotoCollection photoCollection){
        this.photoCollection = photoCollection;
        this.collectionId = photoCollection != null ? photoCollection.getId() : null;

        return this;
    }

    public GetCollectionAndContextCommand collection(Long photoCollectionId){
        this.collectionId = photoCollectionId;

        //Clear photocollection if differente from new collection id
        if(photoCollectionId == null || (photoCollection != null && photoCollection.getId() != photoCollectionId)){
            this.photoCollection = null;
        }

        return this;
    }


    public GetCollectionAndContextCommand context(PhotoContext photoContext){
        this.photoContext = photoContext;
        this.contextId = photoContext != null ? photoContext.getId() : null;

        return this;
    }

    public GetCollectionAndContextCommand context(Long id){
        this.contextId = id;

        //Clear photoContext if differente from new context id
        if(id == null || (photoContext != null && photoContext.getId() != id)){
            this.photoContext = null;
        }

        return this;
    }

	@Override
	public void execute(ResultListener<? super CollectionAndContextContainer> cmdResultListener) {
		CollectionAndContextReceiver receiver = new CollectionAndContextReceiver(cmdResultListener);
//		extractCollection(dataIntent, receiver);
//		extractContext(dataIntent, receiver);

        if(photoContext != null){
            receiver.putContext(photoContext);
        }
        if(photoCollection != null){
            receiver.putCollection(photoCollection);
        }

		if(!receiver.hasFinished()){
			Listeners.onStart(cmdResultListener);
			
			doRequest(receiver, contextId, collectionId);
		}
	}
	
//	protected void extractContext(Intent intent, CollectionAndContextReceiver receiver) {
//		if(intent != null){
//			PhotoContext ctx = IntentUtils.instance()
//										  .getSerializable(intent, Intents.Extras.PhotoContext);
//			receiver.putContext(ctx);
//		}
//	}
//
//	protected void extractCollection(Intent intent, CollectionAndContextReceiver receiver) {
//		if(intent != null){
//			PhotoCollection coll = getCollectionFromIntent(intent);
//			receiver.putCollection(coll);
//		}
//	}

//	private PhotoCollection getCollectionFromIntent(Intent intent) {
//		return IntentUtils.instance().getSerializable(intent, Intents.Extras.PhotoCollection);
//	}
	
	protected void doRequest(CollectionAndContextReceiver receiver, Long ctxId, Long collId) {
		if(!receiver.hasCollection()){
			requestCollection(receiver, ctxId, collId);
		}
		if(!receiver.hasContext()){
			requestContext(receiver, ctxId);
		}
	}
	protected void requestContext(final CollectionAndContextReceiver receiver, Long ctxId) {
		ApiClient.instance().contexts().get(ctxId, true
				, new ReceivePhotoContextListener(receiver));
	}
	
	private void requestCollection(final CollectionAndContextReceiver receiver, Long ctxId, Long collId) {
		if(collId == null){
			ApiClient.instance().contexts().getRootCollection(ctxId, true
					, new ReceivePhotoCollectionListener(receiver));
		}
		else{
			ApiClient.instance().collections().getFullCollection(collId
					, new ReceivePhotoCollectionListener(receiver));
		}
	}

	/* *******************************************************************************************/
	
	public static class ReceivePhotoCollectionListener extends BasicResultListener<PhotoCollection> {
		private final CollectionAndContextReceiver receiver;

		public ReceivePhotoCollectionListener(CollectionAndContextReceiver receiver) {
			this.receiver = receiver;
		}

		@Override
		public void onResult(PhotoCollection result) {
			receiver.putCollection(result);
		}

		@Override
		public void onFailure(Throwable error) {
			receiver.onCollectionFailure(error);
		}
	}

	public static class ReceivePhotoContextListener extends BasicResultListener<PhotoContext> {
		private final CollectionAndContextReceiver receiver;

		public ReceivePhotoContextListener(CollectionAndContextReceiver receiver) {
			this.receiver = receiver;
		}

		@Override
		public void onResult(PhotoContext result) {
			receiver.putContext(result);
		}

		@Override
		public void onFailure(Throwable error) {
			receiver.onContextFailure(error);
		}
	}

}
