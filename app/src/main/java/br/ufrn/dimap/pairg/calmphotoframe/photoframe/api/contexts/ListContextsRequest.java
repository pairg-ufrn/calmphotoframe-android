package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts.ListContextsRequest.ContextsContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ListContextsRequest extends BaseGenericRequest<ContextsContainer, List<? extends PhotoContext> >{

	public ListContextsRequest(ApiConnector apiConnector
			, ResultListener<? super List<? extends PhotoContext>> listener) {
		super(apiConnector, ContextsContainer.class, listener);
	}

	@Override
	public RequestHandler execute(){
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextsUrl();
	}
	
	@Override
	protected List<? extends PhotoContext> convertToResultType(ContextsContainer response) {
		return response.contexts;
	}

	/** Helper class to allow deserialize a collection of PhotoContexts*/
	static class ContextsContainer{
		public List<PhotoContextWithPermission> contexts;
	}
}