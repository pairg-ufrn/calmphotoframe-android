package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextWithPermission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetPhotoContextRequest extends CommonGenericRequest<PhotoContext>{
	private Long ctxId;
	private Boolean includePermission;

    public GetPhotoContextRequest(ApiConnector apiConnector, Long ctxId, Boolean includePermission
            , ResultListener<? super PhotoContext> listener)
    {
        super(apiConnector, getDeserializeClass(includePermission), listener);
        this.ctxId = ctxId;
        this.includePermission = includePermission;
    }

    @Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().contextUrl(ctxId, includePermission);
	}

    private static Class<? extends PhotoContext> getDeserializeClass(Boolean includePermission) {
        return (includePermission == null || !includePermission) ? PhotoContext.class : PhotoContextWithPermission.class;
    }
}