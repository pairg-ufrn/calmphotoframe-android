package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentCreator{
	public interface FragmentBuilder{
		Fragment buildFragment();
	}
	public static class FragmentOperation{
		private FragmentTransaction transaction;
		private Fragment fragment;

		public FragmentOperation(Fragment fragment) {
			this(fragment, null);
		}
		public FragmentOperation(Fragment fragment, FragmentTransaction transaction) {
			this.fragment = fragment;
			this.transaction = transaction;
		}
		
		public FragmentOperation addToBackStack(String backstackEntry){
			if(transaction != null){
				transaction.addToBackStack(backstackEntry);
			}
			return this;
		}
		public FragmentOperation setCustomAnimations(int enterAnim, int exitAnim){
			if(transaction != null){
				transaction.setCustomAnimations(enterAnim, exitAnim);
			}
			return this;
		}
		
		@SuppressWarnings("unchecked")
		public <T extends Fragment> T get(){
			if(transaction != null){
				transaction.commit();
			}
			
			return (T) fragment;
		}
		
		public FragmentTransaction getTransaction(){
			return transaction;
		}
	}
	
	private FragmentManager fragmentManager;
	public FragmentCreator(FragmentManager fragmentManager){
		this.fragmentManager = fragmentManager;
	}
	
	protected FragmentManager getFragManager(){
		return fragmentManager;
	}

	/** Add Fragment to container with id {@code containerId}, and create it if necessary.
	 * @param containerId - id of the view that will contain the created fragment
	 * @param tag - use this tag to verify if the fragment already exist. 
	 * 	If a new fragment is created, this tag will be added to it.
	 * @param builder - {@code FragmentBuilder} used to create the fragment if necessary.*/
	public <T extends Fragment> T addFragment(int containerId, String tag, FragmentBuilder builder){
		T fragment = getOrBuildFragment(tag, builder);
		if(!fragment.isAdded()){
			getFragManager().beginTransaction()
						   .add(containerId, fragment, tag)
						   .commit();
		}
		return fragment;
	}
	/** Create Fragment if not exist or return the fragment with the tag {@code fragmentTag}.
	 * The fragment created is not attached to any view yet.*/
	public <T extends Fragment> T createFragment(String tag, FragmentBuilder builder){
		T fragment = getOrBuildFragment(tag, builder);
		if(!fragment.isAdded()){
			getFragManager().beginTransaction()
						   .add(fragment, tag)
						   .commit();
		}
		return fragment;
	}

	@SuppressWarnings("unchecked")
	public <T  extends Fragment> T getOrBuildFragment(String tag, FragmentBuilder builder) {
		Fragment fragment = getFragment(tag);
		if(fragment == null){
			fragment = builder.buildFragment();
		}
		return (T)fragment;
	}

	/** Calls {@code #replaceFragment(int, String, boolean, FragmentBuilder)} with {@code addToBackstack} = false.*/
	public <T  extends Fragment> T replaceFragment(int container, String tag, FragmentBuilder builder) {
		return replaceFragment(container, tag, false, builder);
	}

	/** Remove the fragment on {@code container} view and adds a fragment created by {@code builder}.
	 * @param container - id of the view that will hold the created fragment
	 * @param tag - tag that identify the fragment to be created. 
	 * 	If the fragment is already on view, do nothing. This tag will be added to the new fragment on replace.
	 * @param addToBackstack - if true, adds this transaction to backstack
	 * @param builder - used to create (if needed) the fragment that will added to view*/
	public <T  extends Fragment> T replaceFragment(int container, String tag, boolean addToBackstack
			, FragmentBuilder builder) 
	{
		T fragment = getOrBuildFragment(tag, builder);

		if(!fragment.isAdded()){
			FragmentTransaction t = getFragManager().beginTransaction();
			t.replace(container, fragment, tag);
			if(addToBackstack){
				t.addToBackStack(null);
			}
			t.commit();
		}
		
		return fragment;
	}

	public FragmentOperation replace(int container, String tag, FragmentBuilder builder) 
	{
		Fragment fragment = getOrBuildFragment(tag, builder);

		FragmentTransaction t = null;
		if(!fragment.isAdded()){
			t = getFragManager().beginTransaction();
			t.replace(container, fragment, tag);
		}

		if(fragment.isHidden() && t != null){
			t.show(fragment);
		}
		
		return new FragmentOperation(fragment, t);
	}

	private Fragment getFragment(String fragmentTag) {
		return getFragManager().findFragmentByTag(fragmentTag);
	}

	public Fragment createOrReplaceFragment(int containerId, String fragmentTag, FragmentBuilder fragmentBuilder) 
	{
		Fragment frag = getFragManager().findFragmentById(containerId);
		if(frag == null){
			return addFragment(containerId, fragmentTag, fragmentBuilder);
		}
		else{
			return replaceFragment(containerId, fragmentTag, fragmentBuilder);
		}
	}

}