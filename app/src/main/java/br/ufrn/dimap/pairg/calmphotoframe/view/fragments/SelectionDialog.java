package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.MultiChoiceActionModeAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;

public class SelectionDialog<Item> extends SimpleDialogFragment implements OnItemClickListener{
	public static class ViewBuilderDisabler implements ViewBuilder {
		private final ViewBuilder viewBuilder;
		private final Collection<?> disabledItems;
		
		public ViewBuilderDisabler(Collection<?> disabledItems, ViewBuilder viewBuilder) {
			this.viewBuilder = viewBuilder;
			this.disabledItems = disabledItems;
		}

		@Override
		public View buildView(Object data, View recycleView, ViewGroup parent) {
			View v = viewBuilder.buildView(data, recycleView, parent);
			boolean isDisabled = (v != null && disabledItems.contains(data));
			v.setEnabled(!isDisabled);
			
			return v;
		}
	}

	public interface OnSelectItemListener<Item>{
		void onSelectItems(Collection<? extends Item> selectedItems);
	}
	private final MultiChoiceActionModeAdapter<Item> adapter;
	private ViewBuilder collectionViewBuilder;
	private AdapterView<? super BaseAdapter> collectionsView;

	private OnSelectItemListener<Item> selectItemListener;
	private final Set<Item> disabledItems;

    public static <Item> SelectionDialog<Item> build(ViewBuilder viewBuilder){
        SelectionDialog<Item> dialog = new SelectionDialog<>();
        dialog.setItemViewBuilder(viewBuilder);

        return dialog;
    }

	public SelectionDialog(){
		disabledItems = new HashSet<>();
		adapter = new MultiChoiceActionModeAdapter<Item>(null);
		adapter.setMultipleSelection(false);

		setLayoutId(R.layout.view_listview);
	}

    public void setItems(Collection<Item> items){
        this.adapter.setItems(items);
    }

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //FIXME: utilizar textos genericos
        String dialogTitle = getActivity().getResources().getQuantityString(R.plurals.dialog_selectItems
                , isMultipleSelection() ? 3 : 1);

        setDialogTitle  (dialogTitle);

		setupSelectionView(getActivity());
	}

	private void setupSelectionView(Activity activity) {
		setCancelMessage(activity.getResources().getString(R.string.dialog_defaultCancel));
		
		if(adapter.getMultipleSelection()){
			setOkMessage    (activity.getResources().getString(R.string.dialog_defaultOk));
		}
		else{
			adapter.setOnItemClickListener(this);
		}
	}
	
	public List<Item> getItems(){
		return adapter.getItems();
	}
	public void setItems(List<Item> items){
		adapter.setItems(items);
	}

	public Set<Item> getDisabledItems(){
		return disabledItems;
	}
	public void setDisabledItens(Collection<Item> items) {
		this.disabledItems.clear();
		disabledItems.addAll(items);
	}
	
	public boolean isMultipleSelection(){
		return adapter.getMultipleSelection();
	}
	public SelectionDialog<Item> setMultipleSelection(boolean multipleSelection){
		adapter.setMultipleSelection(multipleSelection);
		return this;
	}
	
	public OnSelectItemListener<Item> getSelectItemListener() {
		return selectItemListener;
	}
	public void setSelectItemListener(
			OnSelectItemListener<Item> selectContextListener) {
		this.selectItemListener = selectContextListener;
	}

	public ViewBuilder getCollectionViewBuilder() {
		return collectionViewBuilder;
	}
	public void setCollectionViewBuilder(ViewBuilder collectionViewBuilder) {
		this.collectionViewBuilder = collectionViewBuilder;
	}
	public Collection<Item> getSelectedContexts() {
		return adapter.getCheckedItemsCollection();
	}
	public ViewBuilder getItemViewBuilder(){
		return adapter.getViewBuilder();
	}
	public void setItemViewBuilder(ViewBuilder viewBuilder){
		adapter.setViewBuilder(new ViewBuilderDisabler(disabledItems, viewBuilder));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected View createView(int layoutId, LayoutInflater inflater, ViewGroup container) {
		View dialogView;
		
		if(collectionViewBuilder == null){
			dialogView = super.createView(layoutId, inflater, container);
		}
		else{
			dialogView = collectionViewBuilder.buildView(this, null, container);
		}
		
		collectionsView = (AdapterView<? super BaseAdapter>)dialogView.findViewById(android.R.id.list);
		if(collectionsView == null){
			throw new IllegalStateException("Could not find collection view with id 'android.R.id.list'");
		}
		
		if(adapter.getMultipleSelection() && collectionsView instanceof AbsListView){
			if(Build.VERSION.SDK_INT >= 11){
				setMultichoiceMode_v11((AbsListView)collectionsView);
			}
			else{
				if(collectionsView instanceof ListView){
					((ListView)collectionsView).setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
				}
			}
		}
		
		collectionsView.setAdapter(adapter);
		adapter.setAdapterView(collectionsView);
		
		return dialogView;
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void setMultichoiceMode_v11(AbsListView listView) {
		listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	protected boolean notifyResponse(boolean response) {
		boolean close =super.notifyResponse(response);
		if(response && selectItemListener != null){
			Collection<Item> selectedItems = this.getSelectedContexts();
			notifyPhotoItemSelection(selectedItems);
		}
		return close;
	}
	private void notifyPhotoItemSelection(Collection<Item> selectedItems) {
		selectItemListener.onSelectItems(selectedItems);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(!adapter.getMultipleSelection()){
			Item item = this.adapter.getItem(position);
			this.dismiss();
			notifyPhotoItemSelection(Collections.singleton(item));
		}
	}
}
