package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;

public class DragController {

	public interface DragListener {
		//TODO: encapsular informações passadas, através de um objeto evento
		void onStartDrag(View v, float screenX, float screenY);
		void onDrag(View v, float screenX, float screenY);
		void onStopDrag(View v, float screenX, float screenY);
	}

    public interface BoundsController{
        boolean isInside(View draggedView, View container);
    }
	
	public static class SimpleDragListener implements DragListener {
		@Override
		public void onStartDrag(View v, float screenX, float screenY) 
		{}

		@Override
		public void onDrag(View v, float screenX, float screenY) 
		{}

		@Override
		public void onStopDrag(View v, float screenX, float screenY) 
		{}
	}

	private static final String LOG_TAG = DragController.class.getName();

	private boolean isDragging;

	private View containerView;
	private View movingView;
	private int referenceLocation[];
	private float lastDragViewCoordinates[];

	private DragListener dragListener;
    private BoundsController boundsController;

	private boolean enable;
	
	public DragController(){
		containerView = null;
		movingView = null;
		referenceLocation =  null;
		lastDragViewCoordinates = null;
		isDragging = false;
		dragListener = null;
		enable = true;
        boundsController = new DefaultBoundsController();
	}

    public BoundsController getBoundsController() {
        return boundsController;
    }

    public void setBoundsController(BoundsController boundsController) {
        this.boundsController = boundsController;
    }

    public void startDrag(View v, float screenX, float screenY) {
		this.startDrag(v,null, screenX, screenY);
	}
	public void startDrag(View v, View container, float screenX, float screenY) {
		if(!isEnable()){
			return;
		}
		
		if(v == null){
			throw new IllegalArgumentException("View parameter should not be null");
		}
		if(container != null && !isInside(v, container)){
			//TODO: como lidar com este caso? APenas ignorar?
//			throw new IllegalArgumentException("Cannot start dragging view out of container view");
			Log.d(LOG_TAG, "Dragging starting out of container view");
		}
		isDragging = true;
		movingView = v;
		containerView = container;

		referenceLocation =  new int[2];
		referenceLocation[0] = (int)(screenX - ViewCompat.getTranslationX(v));
		referenceLocation[1] = (int)(screenY - ViewCompat.getTranslationY(v));
		
		lastDragViewCoordinates = getFloatScreenLocation(movingView);
		
		if(dragListener != null){
			dragListener.onStartDrag(v, lastDragViewCoordinates[0], lastDragViewCoordinates[1]);
		}
	}

	public void drag(float screenX, float screenY) {
		if(!isEnable()){
			return;
		}
		
		if(!isDragging()){
			throw new IllegalStateException("drag should be called after startDrag");
		}

		translateToIfInside(movingView, containerView
				, screenX - referenceLocation[0]
				, screenY - referenceLocation[1]);

		lastDragViewCoordinates = getFloatScreenLocation(movingView);

		if(dragListener != null){
			dragListener.onDrag(this.movingView, lastDragViewCoordinates[0], lastDragViewCoordinates[1]);
		}
	}


	public void stopDrag() {
		if(!isEnable()){
			return;
		}
		
		if(!isDragging()){
			throw new IllegalStateException("stopDrag should be called after startDrag");
		}
		
		if(dragListener != null){
			Log.d(LOG_TAG, "Stop drag at: " + String.valueOf(lastDragViewCoordinates[0]) + "," + String.valueOf(lastDragViewCoordinates[1]));
			dragListener.onStopDrag(movingView, lastDragViewCoordinates[0], lastDragViewCoordinates[1]);
		}
		
		isDragging = false;
		movingView = null;
	}

	public boolean isDragging(){
		return isDragging;
	}
	
	public void setDragListener(DragListener listener) {
		this.dragListener = listener;
	}

	public View getDraggedView() {
		return movingView;
	}

	/******************************Helper functions****************************************/

	private float[] getTranslation(View view) {
		return new float[]{ViewCompat.getTranslationX(view), ViewCompat.getTranslationY(view)};
	}
	private void setTranslation(View view, float xTranslation, float yTranslation) {
		ViewCompat.setTranslationX(view, xTranslation);
		ViewCompat.setTranslationY(view, yTranslation);
	}

	private int[] getScreenLocation(View view) {
		int screenLoc[] = new int[2];
		view.getLocationOnScreen(screenLoc);
		return screenLoc;
	}
	private float[] getFloatScreenLocation(View view) {
		int screenLoc[] = getScreenLocation(view);
		return new float[]{screenLoc[0], screenLoc[1]};
	}

    protected boolean isInside(View view, View container){
        return boundsController.isInside(view, container);
    }

	private void translateToIfInside(View view, View container, float translationX, float translationY) {
		float previousTranslation[] = getTranslation(view);
		setTranslation(view,  translationX,  translationY);
		if(container != null){
			if(!isInside(view, container)){
				//Rollback
				setTranslation(view,  previousTranslation[0],  previousTranslation[1]);
			}
		}
	}

	public boolean isEnable() {
		return this.enable;
	}

	public void setEnable(boolean enable) {
		if(this.isDragging && enable == false && this.enable == true){
			this.stopDrag();
		}
		this.enable = enable;
	}

    protected class DefaultBoundsController implements BoundsController{
        @Override
        public boolean isInside(View draggedView, View container) {
            return isInside(createAbsoluteRect(draggedView), createAbsoluteRect(container));
        }

        private boolean isInside(Rect inRec, Rect containerRect) {
            return containerRect.contains(inRec);
        }

        private Rect createAbsoluteRect(View inView) {
            int loc[] = getScreenLocation(inView);
            return new Rect( loc[0]
                    , loc[1]
                    , loc[0] + inView.getWidth()
                    , loc[1] + inView.getHeight());
        }
    }

}
