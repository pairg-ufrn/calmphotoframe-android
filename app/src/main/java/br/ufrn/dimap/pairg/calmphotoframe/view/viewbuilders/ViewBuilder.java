package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.ViewGroup;

public interface ViewBuilder {
	View buildView(Object data, View recycleView, ViewGroup parent);
}
