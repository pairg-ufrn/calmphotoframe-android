package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

/* adaptado de: https://developer.android.com/guide/topics/ui/dialogs.html#DialogFragment*/
public class SimpleMaterialDialogFragment extends com.rey.material.app.DialogFragment {

	public interface DialogResponseListener{
		boolean onResponse(SimpleMaterialDialogFragment dialog, boolean positive);
	}

    private final DialogContent dialogContent = new DialogContent();
	private DialogResponseListener dialogResponseListener;

//	private SimpleDialogViewBinder dialogViewBinder;
    private boolean isModal = false;


	public SimpleMaterialDialogFragment() {
		super();
        this.mBuilder = new MaterialDialogBuilder();
		this.dialogResponseListener = null;

//		dialogViewBinder = new SimpleDialogViewBinder();
//        dialogViewBinder.setOkClickListener(new OnClickNotify(true));
//        dialogViewBinder.setOkClickListener(new OnClickNotify(false));
	}

    public DialogContent content() {
        return dialogContent;
    }

    public DialogResponseListener getDialogResponseListener() { return dialogResponseListener;}

	public SimpleMaterialDialogFragment setDialogResponseListener(DialogResponseListener dialogResponseListener) {
		this.dialogResponseListener = dialogResponseListener;
		return this;
	}

//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        if(isModal){
//            return super.onCreateView(inflater, container, savedInstanceState);
//        }
//        return createView(inflater);
//	}

//    public View createView(LayoutInflater inflater) {
//        View rootView = dialogViewBinder.buildView(inflater, 0);
//
//        dialogViewBinder.setup(rootView);
//        dialogViewBinder.bind(content());
//
//        return rootView;
//    }

//    @Override
//	public @NonNull com.rey.material.app.Dialog onCreateDialog(Bundle savedInstanceState) {
//        this.isModal = true;
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setView(createView(getActivity().getLayoutInflater()));
//
//        Dialog dialog = builder.create();
//        //no title
//        if(dialog.getWindow() != null) {
//            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        }
//
//        return dialog;
//	}

	protected boolean notifyResponse(boolean response) {
        if(getDialogResponseListener() != null){
            return getDialogResponseListener().onResponse(this, response);
        }
        return true;
    }

    private class OnClickNotify implements View.OnClickListener {
        boolean response;

        public OnClickNotify(boolean response) {
            this.response = response;
        }

        @Override
        public void onClick(View v) {
            Dialog dialog = getDialog();

            if(notifyResponse(response) && dialog != null){
                dialog.dismiss();
            }
        }
    }

    private class MaterialDialogBuilder extends SimpleDialog.Builder {
        @Override
        protected com.rey.material.app.Dialog onBuild(Context context, int styleId) {
            if(content().hasCustomLayout()) {
                super.contentView(content().getCustomLayoutId());
            }
            if(content().hasOkButton()){
                super.positiveAction(content().getOkMessage());
            }
            if(content().hasCancelButton()){
                super.negativeAction(content().getCancelMessage());
            }
            if(content().hasTitle()){
                super.title(content().getDialogTitle());
            }
            if(content().hasMessage()){
                super.message(content().getDialogMessage());
            }


            return super.onBuild(context, styleId);
        }

        @Override
        public void onNegativeActionClicked(DialogFragment fragment) {
            super.onNegativeActionClicked(fragment);
            notifyResponse(false);
        }

        @Override
        public void onPositiveActionClicked(DialogFragment fragment) {
            super.onPositiveActionClicked(fragment);
            notifyResponse(true);
        }
    }
}
