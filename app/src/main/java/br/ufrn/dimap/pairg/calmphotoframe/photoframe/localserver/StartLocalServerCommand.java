package br.ufrn.dimap.pairg.calmphotoframe.photoframe.localserver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.PhotoFrameControllerService;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameBroadcastReceiver;

public class StartLocalServerCommand extends BaseStatelessCommand<Endpoint>{

    private Context context;
    private PhotoFrameBroadcastReceiver receiver;

    public static StartLocalServerCommand build(Context context){
        return new StartLocalServerCommand(context);
    }

    public StartLocalServerCommand(Context context)
    {
        this.context = context;
        receiver = new PhotoFrameBroadcastReceiver();
    }

    @Override
    public void execute(ResultListener<? super Endpoint> listener) {
        receiver.setServerListener(buildServerListener(new OnResultReturnLocalEndpoint(listener)));
        receiver.register(context);

        context.startService(buildStartServiceIntent(null));
    }

    @NonNull
    private Intent buildStartServiceIntent(Bundle arguments) {
        Intent intent = new Intent(context, PhotoFrameControllerService.class);
        intent.setAction(PhotoFrameControllerService.START_SERVER_ACTION);
        if (arguments != null && !arguments.isEmpty()) {
            intent.putExtras(arguments);
        }
        return intent;
    }

    private ServerListenerAdapter buildServerListener(ResultListener<? super ServerStartEvent> listener) {
        ResultListener<ServerStartEvent> compositeListener = new CompositeResultListener<>(
                new OnResultUnregisterReceiver(receiver, context),
                listener);

        return new ServerListenerAdapter().onConnect(compositeListener);
    }

    private static class OnResultUnregisterReceiver extends BasicResultListener<Object> {
        final PhotoFrameBroadcastReceiver receiver;
        Context context;

        public OnResultUnregisterReceiver(PhotoFrameBroadcastReceiver receiver, Context context) {
            this.receiver = receiver;
            this.context = context;
        }

        @Override
        protected void onFinish() {
            unregister();
        }

        public void unregister() {
            this.receiver.unregister(context);
        }
    }

    private static class OnResultReturnLocalEndpoint extends ResultListenerDelegator<ServerStartEvent, Endpoint> {
        public OnResultReturnLocalEndpoint(ResultListener<? super Endpoint> listener) {
            super(listener);
        }

        @Override
        protected Endpoint convertResult(ServerStartEvent result) {
            return new Endpoint(Endpoint.LOCAL_HOST, ApiEndpoint.DEFAULT_PORT);
        }
    }
}
