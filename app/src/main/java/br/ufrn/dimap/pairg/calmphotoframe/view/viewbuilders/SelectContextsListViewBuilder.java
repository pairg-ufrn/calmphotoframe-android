package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class SelectContextsListViewBuilder implements ViewBuilder {
	public interface OnStartContextEdition{
		public void startContextsEdition();
	}
	
	private SelectContextsListViewBuilder.OnStartContextEdition listener;
	private Context context;
	
	public SelectContextsListViewBuilder(SelectContextsListViewBuilder.OnStartContextEdition listener, Context ctx) {
		this.listener = listener;
		this.context = ctx;
	}
	
	@Override
	public View buildView(Object data, View recycleView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		ListView listView = (ListView)inflater.inflate(R.layout.view_listview, parent, false);
		View footer = inflater.inflate(R.layout.footer_editcontexts, listView, false);
		listView.addFooterView(footer);
		
		Button editContextsBnt = (Button)footer.findViewById(R.id.editContextsButton);
		editContextsBnt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(listener != null){
					listener.startContextsEdition();
				}
			}
		});
		
		return listView;
	}
}