package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;

public class RemoveCurrentPhotoCommand extends ActivityStatelessCommand<Photo>{

	private static final String REMOVE_DIALOG_TAG = null;
	private PhotoHolder photoHolder;
	
	
	public RemoveCurrentPhotoCommand(BaseActivity activity, PhotoHolder photoHolder) {
		super(activity);
		this.photoHolder = photoHolder;
	}

	@Override
	public void execute(ResultListener<? super Photo> cmdResultListener) {
		removePhoto(cmdResultListener);
	}
	private void removePhoto(final ResultListener<? super Photo> cmdResultListener) {
		final Photo photoToRemove = photoHolder.getCurrentPhoto();
    	SimpleDialogFragment dialogFragment = new SimpleDialogFragment();
    	dialogFragment.setOkMessage    (getContext().getString(R.string.remove_photo_dialog_ok));
    	dialogFragment.setCancelMessage(getContext().getString(R.string.remove_photo_dialog_cancel));
    	dialogFragment.setDialogMessage(getContext().getString(R.string.remove_photo_dialog_message));
    	dialogFragment.setDialogResponseListener(new DialogResponseListener() {
			@Override
			public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
				if(positive){
					executeRemovePhoto(cmdResultListener, photoToRemove);
				}
				return true;
			}
		});
    	dialogFragment.show(getActivity().getSupportFragmentManager(), REMOVE_DIALOG_TAG);
	}

	private void executeRemovePhoto(ResultListener<? super Photo> listener, Photo photoToRemove) {
    	if(photoToRemove != null){
    		ApiClient.instance().photos()
					.remove(photoToRemove, new OnRemovePhotoDelegate(listener));
    	}
	}

	/* ***************************************************************************************/
	class OnRemovePhotoDelegate extends BasicResultListener<Photo> {
		ResultListener<? super Photo> listener;
		
		public OnRemovePhotoDelegate(ResultListener<? super Photo> listener) {
			this.listener = listener;
		}

		@Override
		public void onResult(Photo result) {
			notifyOnResult(result, listener);
		}

		@Override
		public void onFailure(Throwable error) {
			notifyOnFailure(error, listener);
		}
	}
}
