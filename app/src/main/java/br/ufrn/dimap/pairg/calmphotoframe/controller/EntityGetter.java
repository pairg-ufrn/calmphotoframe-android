package br.ufrn.dimap.pairg.calmphotoframe.controller;

public interface EntityGetter<T> {
	T getEntity();
}
