package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.FlipPageTransformer;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.EmptyStateView;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator.FragmentBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ListPageAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.PagerSwitcher;

/** Um fragmento que encapsula a interface de visualização das fotos (frente e verso).*/
public class PhotoViewerFragment extends Fragment{
	private static final String CLASS_NAME = PhotoViewerFragment.class.getName();
	private static final String IMAGE_FRAGMENT 			= CLASS_NAME + ".IMAGE_FRAGMENT";
	private static final String PHOTO_INFO_FRAGMENT 	= CLASS_NAME + ".PHOTO_INFO_FRAGMENT";
	private static final String PHOTO_METADATA_FRAGMENT = CLASS_NAME + ".PHOTO_METADATA_FRAGMENT";

    public enum State{Default, Empty, Error};

	private ListPageAdapter pagerAdapter;
	private PhotoInfoFragment photoInfoFragment;
	private ImageFragment imageFragment;
	private PhotoMetadataFragment photoMetadataFragment;
    private EmptyStateView emptyStateView;

	private ViewPager photoPager;
	private ProgressBar progressBar;

	private final ViewCache viewCache;
	
	public PhotoViewerFragment() {
		viewCache = new ViewCache();
        emptyStateView = new EmptyStateView();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_photoviewer, container, false);
		setupFragmentsNavigation(layout);
		
		progressBar = ViewUtils.getView(layout, R.id.photoviewer_progressBar);
        emptyStateView.setup(layout, R.id.emptyView)
                .setState(R.drawable.tooltip_image_big, R.string.photo_emptyState);

        return layout;
	};
	
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		viewCache.restoreState(this, true);
	}
	
	public int getCurrentPage() {
		if(photoPager != null){
			return photoPager.getCurrentItem();
		}
		return viewCache.getCurrentPage();
	}
	public void setCurrentPage(int page){
		setSelectedPage(page, false);
	}
	public void setSelectedPage(int page, boolean smoothScroll){
		if(photoPager != null){
			photoPager.setCurrentItem(page, smoothScroll);
		}
		else{
			viewCache.setCurrentPage(page);
		}
	}
	
	public int flipPage() {
		int newPage = (getCurrentPage() + 1) % getPageCount();
		setSelectedPage(newPage, true);
		
		return newPage;
	}
	private int getPageCount() {
		return 3;
	}

	public Photo getPhotoDescriptor(){
		return getPhotoInfoFragment() != null ? getPhotoInfoFragment().getPhoto() : null;
	}
	public void setPhotoImage(Bitmap bitmap){
		if(getImageFragment() != null){
			setFragmentsImage(bitmap);
		}
		else{
			viewCache.setImage(bitmap);
		}
	}

	public void setPhotoDescriptor(Photo descriptor){
		if(getPhotoInfoFragment() != null){
			setFragmentsPhoto(descriptor);
		}
		else{
			viewCache.setPhoto(descriptor);
		}
	}

	public void showDefaultPhoto() {		
		if(getImageFragment() != null){
			getImageFragment().showDefaultImage();
			getPhotoInfoFragment().showDefaultImage();
		}
	}

    public void showState(State state){
        showState(state, false);
    }
    public void showState(State state, boolean hideWaiting){
        boolean photoVisibility = (state == State.Default),
                emptyVisibility = (state != State.Default);

        if(state == State.Empty){
            emptyStateView.setState(R.drawable.tooltip_image_big, R.string.photo_emptyState);
        }
        else if(state == State.Error){
            emptyStateView.setState(R.drawable.image_broken_variant_big, R.string.photo_loadFailed);
        }

        emptyStateView.setVisible(emptyVisibility);
        ViewUtils.setVisible(photoPager, photoVisibility);

        if(hideWaiting){
            hideWait();
        }
    }

    public void showErrorPhoto() {
		if(getImageFragment() != null){
			getImageFragment().showErrorImage();
			getPhotoInfoFragment().setPhoto(null);
			getPhotoInfoFragment().setImage(getImageFragment().getErrorImageId());
		}
	}

	public void showWait() {
        setProgressVisibility(View.VISIBLE);
    }

    public void hideWait(){
        setProgressVisibility(View.GONE);
    }
    protected void setProgressVisibility(int visible) {
        if (progressBar != null) {
            progressBar.setVisibility(visible);
        }
    }

	public boolean isPageSwipeEnabled(){
		if(photoPager != null){
			return ((PagerSwitcher)photoPager).isPageSwipeEnabled();
		}
		else{
			return viewCache.getPageSwipeEnabledCache();
		}
	}
	public void setPageSwipeEnabled(boolean enabled){
		if(photoPager != null){
			((PagerSwitcher)photoPager).setPageSwipeEnabled(enabled);
		}
		else{
			viewCache.setPageSwipeEnabledCache(enabled);
		}
	}
	/* ************************************* View setup ***********************************************/

	private void setupFragmentsNavigation(View layout) {
		pagerAdapter = new ListPageAdapter();
		createFragments();
        
		photoPager = (ViewPager)layout.findViewById(R.id.gen_page_view);
		
        photoPager.setAdapter(pagerAdapter);
        photoPager.setPageTransformer(true, new FlipPageTransformer());	  
	}

	private void createFragments() {
		FragmentCreator fragCreator = new FragmentCreator(getChildFragmentManager());

		this.imageFragment = fragCreator.createFragment(IMAGE_FRAGMENT, new ImageFragmentBuilder());
		this.photoInfoFragment = fragCreator.createFragment(PHOTO_INFO_FRAGMENT, new PhotoInfoFragmentBuilder());
		this.photoMetadataFragment =fragCreator.createFragment(PHOTO_METADATA_FRAGMENT, new PhotoMetadataFragmentBuilder());

		pagerAdapter.addItem(imageFragment);
		pagerAdapter.addItem(photoInfoFragment);
		pagerAdapter.addItem(photoMetadataFragment);
	}
	
	protected ImageFragment getImageFragment() {
		return imageFragment;
	}
	protected PhotoInfoFragment getPhotoInfoFragment() {
		return this.photoInfoFragment;
	}
	protected PhotoMetadataFragment getPhotoMetadataFragment() {
		return this.photoMetadataFragment;
	}

	void setFragmentsImage(Bitmap image) {
		if(image == null){
			return;
		}

		if(getImageFragment() != null){
			getImageFragment().setImage(image);
		}
		if(getPhotoInfoFragment() != null){
			getPhotoInfoFragment().setImage(image);
		}
	}
	
	void setFragmentsPhoto(Photo descriptor) {
		if(getPhotoInfoFragment() != null){
			getPhotoInfoFragment().setPhoto(descriptor);
		}
		if(getPhotoMetadataFragment() != null){
			getPhotoMetadataFragment().setPhotoDescriptor(descriptor);
		}
	}
	/* *************************************************************************************************/

	private final class PhotoMetadataFragmentBuilder implements FragmentBuilder {
		@Override
		public Fragment buildFragment() {
			return new PhotoMetadataFragment();
		}
	}
	private final class PhotoInfoFragmentBuilder implements FragmentBuilder {
		@Override
		public Fragment buildFragment() {
			return new PhotoInfoFragment();
		}
	}
	private final class ImageFragmentBuilder implements FragmentBuilder {
		@Override
		public Fragment buildFragment() {
			return new ImageFragment();
		}
	}

	/* *************************************************************************************************/

    private class ViewCache{
		private Photo photo;
		private Bitmap image;
		private Boolean pageSwipeEnabledCache;
		private Integer currentPage;
		
		@SuppressWarnings("unused")
		public Photo getPhoto() {
			return photo;
		}
		public void setPhoto(Photo photo) {
			this.photo = photo;
		}
		@SuppressWarnings("unused")
		public Bitmap getImage() {
			return image;
		}
		public void setImage(Bitmap image) {
			this.image = image;
		}
		public boolean getPageSwipeEnabledCache() {
			return pageSwipeEnabledCache == null ? true : pageSwipeEnabledCache;
		}
		public void setPageSwipeEnabledCache(boolean pageSwipeEnabledCache) {
			this.pageSwipeEnabledCache = pageSwipeEnabledCache;
		}
		public int getCurrentPage() {
			return currentPage == null ? 0 : currentPage;
		}
		public void setCurrentPage(int currentPage) {
			this.currentPage = currentPage;
		}
		
		public void restoreState(PhotoViewerFragment frag, boolean clearCache){
			frag.setFragmentsImage(image);
			frag.setFragmentsPhoto(photo);
			
			if(photoPager != null){
				if(currentPage != null && frag.photoPager != null){
					frag.photoPager.setCurrentItem(currentPage);
				}
				if(this.pageSwipeEnabledCache != null){
					((PagerSwitcher)photoPager).setPageSwipeEnabled(pageSwipeEnabledCache);
				}
			}
			
			if(clearCache){
				this.clearCache();
			}
		}
		
		public void clearCache(){
			this.image = null;
			this.photo = null;
			this.currentPage = null;
			this.pageSwipeEnabledCache = null;
		}
	}
}
