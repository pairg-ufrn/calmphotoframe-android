package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class TextListenerResponseHandler extends TextHttpResponseHandler {
	public interface TextResponseListener{
		void onSuccess(int statusCode, Header[] headers, String message);
		void onFailure(int statusCode, Header[] headers, String message, Throwable error);
	}
	
	private TextResponseListener listener;

	public TextListenerResponseHandler() {
		this(null);
	}
	public TextListenerResponseHandler(TextResponseListener listener) {
		this.listener = listener;
	}
	
	public void setTextListener(TextResponseListener listener) {
		this.listener = listener;
	}
	public TextResponseListener getTextListener() {
		return listener;
	}
	
	@Override
	public void onSuccess(int statusCode, Header[] headers, String message) {
		if(existListener()){
			listener.onSuccess(statusCode, headers, message);
		}
	}
	@Override
	public void onFailure(int statusCode, Header[] headers, String message, Throwable error) {
		if(existListener()){
			listener.onFailure(statusCode, headers, message, error);
		}
	}

	protected boolean existListener() {
		return listener != null;
	}
}
