package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import android.graphics.Bitmap;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.PhotoReceiver;

public class ImageReceiverAdapter extends BasicResultListener<Bitmap> {
    private final PhotoReceiver photoReceiver;
    private final ResultListener<? super PhotoImageWrapper> resultListener;

	public ImageReceiverAdapter(PhotoReceiver photoReceiver, ResultListener<? super PhotoImageWrapper> resultListener)
	{
		super();
		this.photoReceiver = photoReceiver;
	    this.resultListener = resultListener;
    }

	@Override
	public void onResult(Bitmap image) {
		Log.i(getClass().getName(), "Received image to photo " + photoReceiver.getPhotoId());
		photoReceiver.onPhotoLoadReceiveImage(resultListener, image, false);
	}
	
	@Override
	public void onFailure(Throwable error) {
		Log.w(getClass().getName()
				, "Ocurred error while trying to receive image to photo " + photoReceiver.getPhotoId() + "."
				, error);
		photoReceiver.onPhotoLoadError(resultListener, error);
	}
}