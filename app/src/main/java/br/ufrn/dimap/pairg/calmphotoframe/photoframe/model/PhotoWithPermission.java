package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.util.Date;

public class PhotoWithPermission extends Photo{
    private Permission permission;

    public PhotoWithPermission() {
        super();
    }
    public PhotoWithPermission(long photoId) {
        super(photoId);
    }
    public PhotoWithPermission(Number photoId, String photoMoment, String photoDescription, String photoPlace, Date photoDate) {
        super(photoId, photoMoment, photoDescription, photoPlace, photoDate);
    }
    public PhotoWithPermission(Photo descriptor) {
        super(descriptor);
    }
    public PhotoWithPermission(PhotoWithPermission photoWithPermission) {
        this((Photo) photoWithPermission);
        this.setPermission(photoWithPermission.getPermission());
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Override
    public PhotoWithPermission clone() {
        return new PhotoWithPermission(this);
    }
}
