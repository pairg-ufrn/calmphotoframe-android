package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;

/**
 */
public class PhotoComponentsViewBinder extends InflaterViewBinder<PhotoComponent> {
    View contextInfoContainer;
    TextView nameView;

    ViewCountBinder albunsCounterBinder, photosCounterBinder;

    ImageViewBinder<PhotoComponent> imageViewBinder;

    public PhotoComponentsViewBinder(@Nullable ImageLoader<PhotoComponent> imageLoader) {
        super(R.layout.view_photoitem);
        imageViewBinder = new ImageViewBinder<PhotoComponent>(0, R.id.thumbnail_imageview)
                                .setSquared(true)
                                .setLoader(imageLoader);

        albunsCounterBinder = new ViewCountBinder(R.id.photoCollection_subcollectionCountContainer,
                R.id.photoCollection_subcollectionCount);

        photosCounterBinder = new ViewCountBinder(R.id.photoCollection_photoCountContainer,
                R.id.photoCollection_photoCount);
    }

    @Override
    public void setup(View itemView) {
        contextInfoContainer = itemView.findViewById(R.id.photoCollection_container);
        nameView = (TextView)itemView.findViewById(R.id.view_photoCollection_Name);

        albunsCounterBinder.setupView(itemView);
        photosCounterBinder.setupView(itemView);

        imageViewBinder.setup(itemView);
    }

    @Override
    public void bind(PhotoComponent entity) {
        if(entity.isCollection()){
            bindToAlbum(entity.getPhotoCollection());
        }
        else{
            bindToPhoto();
        }

        imageViewBinder.bind(entity);
    }


    protected void bindToAlbum(PhotoCollection album) {
        contextInfoContainer.setVisibility(View.VISIBLE);
        nameView.setText(album.getName());

        photosCounterBinder.bind(album.getPhotoIds().size());
        albunsCounterBinder.bind(album.getSubcollectionsIds().size());

//        ImageView imgView = (ImageView)itemView.findViewById(R.id.imageview);
//        imgView.setImageResource(android.R.color.transparent);
//        imgView.invalidate();
    }

    protected void bindToPhoto() {
        contextInfoContainer.setVisibility(View.GONE);
    }

    class ViewCountBinder{
        TextView counter;
        View container;
        int counterViewId, containerId;

        public ViewCountBinder(int containerId, int counterId){
            this.containerId = containerId;
            counterViewId = counterId;
        }
        public void setupView(View view){
            this.container = ViewUtils.getView(view, containerId);
            this.counter = ViewUtils.getView(view, counterViewId);
        }
        public void bind(int count){
            if(count > 0){
                container.setVisibility(View.VISIBLE);
                counter.setText(String.valueOf(count));
            }
            else{
                container.setVisibility(View.GONE);
            }
        }
    }
}
