package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicReference;

public class ApiEndpoint extends Endpoint implements Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1330217033358822612L;

    private final SessionInfo sessionInfo;
    private final AtomicReference<Long> currentContext;

	public static ApiEndpoint fromUrl(String url){
        Endpoint endpoint = Endpoint.fromUrl(url);
        return (endpoint == null) ? null :new ApiEndpoint(endpoint);
	}

    public ApiEndpoint() {
		this(LOCAL_HOST, DEFAULT_PORT);
	}

	public ApiEndpoint(String hostname) {
		this(hostname, null);
	}
	public ApiEndpoint(String hostname, Integer port) {
        super(hostname, port);
        if(hostname == null){
            setHostname(LOCAL_HOST);
        }
        if(port == null){
            setPort(DEFAULT_PORT);
        }

        this.currentContext = new AtomicReference<>(null);
		this.sessionInfo = new SessionInfo();
	}

    public ApiEndpoint(@NonNull Endpoint endpoint){
        this(endpoint.getHostname(), endpoint.getPort());
    }

    public boolean hostnameIsDefault() {
        return isLocal();
    }

    public String  getConnectionToken() { return getSession().getToken();}
	public Long    getCurrentContext()  { return currentContext.get(); }
	public SessionInfo getSession()     { return sessionInfo;}

    public void setCurrentContext(Long currentContext) {
		this.currentContext.set(currentContext);
	}

    public void setConnectionToken(String connectionToken) {
		getSession().setToken(connectionToken);
	}

	public void setSession(SessionInfo session) {
		getSession().set(session);
	}

    public User getUser() {
        return getSession() != null ? getSession().getUser() : null;
    }

    public void setUser(User user) {
        if(getSession() != null){
            getSession().setUser(user);
        }
    }

	@Override
	public String toString() {
		@SuppressWarnings("StringBufferReplaceableByString")
        StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append(" scheme: ").append(getScheme());
		builder.append(", ");
		builder.append("hostname: ").append(getHostname());
		builder.append(", ");
		builder.append("port: ").append(getPort());
		builder.append(", ");
		builder.append("token: ").append(getConnectionToken());
		builder.append(", ");
		builder.append("context: ").append(getCurrentContext());
		builder.append("}");
		return builder.toString();
	}

    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public ApiEndpoint clone(){
        ApiEndpoint clone = new ApiEndpoint();
        clone.copy(this);
        return clone;
    }
    public void copy(ApiEndpoint other) {
        super.copy(other);

        this.setConnectionToken(other.getConnectionToken());
        this.setCurrentContext(other.getCurrentContext());
        this.setSession(other.getSession());
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof ApiEndpoint)){
            return false;
        }

        if(this == o){ //same instance
            return true;
        }

        ApiEndpoint other = (ApiEndpoint)o;
        return super.equals(other)
                && checkEquals(getSession(), other.getSession());
    }
}
