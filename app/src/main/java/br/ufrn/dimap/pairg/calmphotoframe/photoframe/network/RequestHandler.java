package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

public interface RequestHandler {
	boolean isFinished();
	boolean isCancelled();
	boolean cancel();
}
