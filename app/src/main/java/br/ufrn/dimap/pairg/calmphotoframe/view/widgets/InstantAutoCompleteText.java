package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;

import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

public class InstantAutoCompleteText extends MaterialAutoCompleteTextView {
    private static final String TAG  = InstantAutoCompleteText.class.getSimpleName();


    public InstantAutoCompleteText(Context context) {
        super(context);
    }
    public InstantAutoCompleteText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public InstantAutoCompleteText(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if(focused && getAdapter() != null){
            showSuggestion();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean res = super.onTouchEvent(event);

        if(hasFocus() && !isPopupShowing()){
            showSuggestion();
        }

        return res;
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {


    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && handleBackKey(event)){
            return true;
        }

        return super.onKeyPreIme(keyCode, event);
    }

    private boolean handleBackKey(KeyEvent event) {
        if(!isPopupShowing()
                || event.getAction() != KeyEvent.ACTION_UP
                || event.isCanceled())
        {
            return false;
        }

        InputMethodManager imm = (InputMethodManager)
                getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        if(!imm.hideSoftInputFromWindow(getWindowToken(), 0,  new OnHideKeyboard())){
            this.dismissDropDown();
        }

        return true;
    }

    protected void showSuggestion() {
        if(getText().length() != 0) {
            performFiltering(getText(), 0);
            showDropDown();
        }
        else{
            setText("");
        }
    }

    private class OnHideKeyboard extends ResultReceiver {
        AutoCompleteTextView txtView = InstantAutoCompleteText.this;

        public OnHideKeyboard() {
            super(new Handler());
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            Log.d(TAG, "Received result: " + resultCode);

            if(resultCode == InputMethodManager.RESULT_UNCHANGED_HIDDEN){
                //already hidden, so hide dropdown
                if(txtView.isPopupShowing()){
                    txtView.dismissDropDown();
                }
            }
        }
    }
}
