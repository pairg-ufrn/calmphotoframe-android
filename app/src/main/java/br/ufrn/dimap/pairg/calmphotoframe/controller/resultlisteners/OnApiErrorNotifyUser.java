package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import java.util.HashMap;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ExceptionUtils;

public class OnApiErrorNotifyUser<T> extends OnErrorNotifyUser {
	private final Map<Integer, Integer> statusToMessageId;

    public OnApiErrorNotifyUser(UserNotifier userNotifier) {
        super(userNotifier);

        statusToMessageId = new HashMap<>();
        statusToMessageId.put(404, R.string.error_elementOnViewRemoved);
        statusToMessageId.put(403, R.string.error_insufficientpermission);
	}

    public OnApiErrorNotifyUser<T> putErrorMessage(Integer statusCode, Integer msgId){
        this.statusToMessageId.put(statusCode, msgId);
        return this;
    }

    @Override
    protected CharSequence getErrorText(Throwable error) {
        CharSequence text = null;
        ApiException ex = ExceptionUtils.getCause(error, ApiException.class);

        if(ex != null){
            Integer msgId = statusToMessageId.get(ex.getStatusCode());

            if(msgId != null){
                text = notifier().getTextResouce(msgId);
            }
            if(text == null){
                text = ex.getLocalizedMessage();
            }
        }

        return text != null ? text : super.getErrorText(error);
    }

}