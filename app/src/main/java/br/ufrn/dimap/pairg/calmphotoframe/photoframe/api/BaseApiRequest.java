package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;

public abstract class BaseApiRequest<ListenerResultType> {
	protected ResultListener<? super ListenerResultType> listener;
	private ApiConnector apiConnector;
	private Endpoint endpoint;

    public BaseApiRequest(ApiConnector apiConnector) {
        this(apiConnector, null);
    }
    public BaseApiRequest(ApiConnector apiConnector, ResultListener<? super ListenerResultType> listener) {
        super();
        this.apiConnector = apiConnector;
        this.listener = listener;
    }

	public ApiConnector getApiConnector() {
		return apiConnector;
	}
	public void setApiConnector(ApiConnector apiConnector) {
		this.apiConnector = apiConnector;
	}

	public HttpConnector getHttpConnector(){
		return this.apiConnector.getHttpConnector();
	}
	public ApiRouteBuilder getApiBuilder(){
		return this.apiConnector.getApiBuilder();
	}
	public ObjectMapper getObjectMapper(){
		return this.apiConnector.getJsonConverter();
	}
	public PhotoCache getPhotoCache(){
		return this.apiConnector.getPhotoCache();
	}

	public BaseApiRequest<ListenerResultType> listener(ResultListener<? super ListenerResultType> listener) {
        this.listener = listener;
        return this;
    }

	public abstract RequestHandler execute();

    protected abstract String genUrl();

	public Endpoint endpoint(){
        return this.endpoint;
    }

	public BaseApiRequest<ListenerResultType> endpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
        return this;
    }

	protected HttpConnector http(){
        return getHttpConnector();
    }
}