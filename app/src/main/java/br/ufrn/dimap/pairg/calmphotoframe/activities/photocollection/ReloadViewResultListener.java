package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;

public class ReloadViewResultListener extends ActivityResultListener<Object>  {
	PhotoCollectionController stateHolder;
	
	public ReloadViewResultListener(BaseActivity activity, PhotoCollectionController stateHolder) 
	{
		super(activity);
		this.stateHolder = stateHolder;
	}
	
	@Override
	public void onResult(Object result) {
		stateHolder.refreshCollectionData();
	}
}