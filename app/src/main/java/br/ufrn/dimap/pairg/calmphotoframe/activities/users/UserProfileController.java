package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.support.annotation.Nullable;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.NavActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.NotificationBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Commands;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnErrorNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnResultNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment.UserPassword;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

public class UserProfileController {
    private UserProfileFragment profileView;

    private ActionListener<UserPassword> changePasswordAction;
    private ArgsCommand<User, User> changePhotoCommand;
    private BaseArgsCommand<User, User> saveNameCommand;
    private UserNotifier userNotifier;

    private NavActivity activity;

    public UserProfileController(NavActivity activity) {
        this.activity = activity;
        this.userNotifier = activity;
    }

    public void setUserNotifier(UserNotifier userNotifier) {
        this.userNotifier = userNotifier;
    }

    public UserNotifier getUserNotifier() {
        return userNotifier;
    }

    public ActionListener<UserProfileFragment.UserPassword> getChangePasswordAction() {
        return changePasswordAction;
    }

    public ArgsCommand<User, User> getChangePhotoCommand() {
        return changePhotoCommand;
    }

    public BaseArgsCommand<User, User> getSaveNameCommand() {
        return saveNameCommand;
    }

    public void setup(UserProfileFragment profileFragment) {
        this.profileView = profileFragment;

        buildCommands();

        this.profileView.setChangePasswordAction(changePasswordAction);
        this.profileView.setChangePhotoCommand(changePhotoCommand);
        this.profileView.setSaveNameCommand(saveNameCommand);
    }

    protected void buildCommands() {
        changePasswordAction = new UpdatePasswordAction(getUserNotifier());

        changePhotoCommand = Commands.bindArgListeners(
                new UpdateUserProfileImage(activity),
                new OnResultNotifyUser(getUserNotifier(), R.string.profile_changephoto_succeeded),
                new RebindApiEndpointOnResult(activity));

        saveNameCommand = new BaseArgsCommand<User, User>() {
            @Override
            public void execute(@Nullable User user, ResultListener<? super User> listener) {
                ApiClient.instance().users().update(user, new CompositeResultListener<>(
                        new OnResultUpdateCurrentUser(),
                        new OnResultNotifyUser(getUserNotifier(), R.string.profile_savename_succeeded),
                        new RebindApiEndpointOnResult(activity),
                        listener
                ));
            }
        };
    }

    public static class UpdatePasswordAction
            extends BaseArgsCommand<UserProfileFragment.UserPassword, Object>
            implements ActionListener<UserProfileFragment.UserPassword>
    {
        UserNotifier userNotifier;

        public UpdatePasswordAction(UserNotifier userNotifier) {
            this.userNotifier = userNotifier;
        }

        @Override
        public void execute(UserPassword value) {
           execute(value, null);
        }

        @Override
        public void execute(@Nullable final UserPassword userPassword, ResultListener<? super Object> listener) {
            final Runnable retryAction = new Runnable() {
                @Override
                public void run() {
                    execute(userPassword);
                }
            };

            ResultListener<? super Object> l = Listeners.join(
                    new OnErrorRetry(userNotifier, retryAction),
                    new OnResultNotifyUser(userNotifier, R.string.profile_changepassword_succeeded),
                    listener
            );

            ApiClient.instance().users().changePassword(userPassword.getPassword(), l);
        }

        private class OnErrorRetry extends OnErrorNotifyUser {
            private final Runnable retryAction;

            public OnErrorRetry(UserNotifier userNotifier, Runnable retryAction) {
                super(userNotifier);
                this.retryAction = retryAction;
            }

            @Override
            protected CharSequence getErrorText(Throwable error) {
                return userNotifier.getTextResouce(R.string.profile_changepassword_error_generic);
            }

            @Override
            protected NotificationBuilder notifyUser() {
                NotificationBuilder builder = super.notifyUser();

                builder.action(R.string.generic_retry_action, retryAction);

                return builder;
            }
        }
    }

    public static class RebindApiEndpointOnResult extends BasicResultListener<Object> {
        NavActivity activity;

        public RebindApiEndpointOnResult(NavActivity activity) {
            this.activity = activity;
        }

        @Override
        protected void handleResult(Object result) {
            if(activity != null && activity.isActive()) {
                activity.rebindApiEndpoint();
            }
        }
    }
}
