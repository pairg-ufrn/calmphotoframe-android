package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.CheckSelectedItemDecorator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.AccountViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.CheckableLayoutViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer;

public class UsersFragment extends ItemsRecycleFragment<User> {
    private FloatingActionButton addButton;
    private ArgsCommand<AccountViewBinder.UserActivationParam, User> activateUserCommand;

	public UsersFragment() {
        setBinderBuilder(new AccountBinderBuilder());
	}

    @Override
    protected int getLayoutId() {
        return R.layout.layout_recyclerview_page;
    }

    public ArgsCommand<AccountViewBinder.UserActivationParam, User> getActivateUserCommand() {
        return activateUserCommand;
    }

    public void setActivateUserCommand(ArgsCommand<AccountViewBinder.UserActivationParam, User> activateUserCommand) {
        this.activateUserCommand = activateUserCommand;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ItemsRecycleAdapter<User> adapter = getAdapter();
        adapter.addDecorator(new CheckSelectedItemDecorator<>(adapter.getSelectionManager(), adapter));
        adapter.setIdGenerator(new UserIdGenerator());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addButton = ViewUtils.getView(view, R.id.floatingbutton);
        addButton.setVisibility(View.VISIBLE);
        addButton.setImageResource(R.drawable.account_plus);
    }

    public void setOnClickAddButton(View.OnClickListener listener){
        addButton.setOnClickListener(listener);
    }

    public void setItems(Collection<? extends User> items) {
        getAdapter().items().set(items);
    }

    private static class UserIdGenerator implements ItemsRecycleAdapter.IdGenerator<User> {
        @Override
        public long getId(User item, ItemsManager<User> items) {
            return item == null ? 0 : item.getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    private class AccountBinderBuilder implements ItemsRecycleAdapter.BinderBuilder<User> {
        @Override
        public BinderViewHolder<User> buildViewBinder() {
            AccountViewBinder binder = new AccountViewBinder();
            binder.setActivateUserCommand(getActivateUserCommand());

            CheckableLayoutViewBinder<User> checkable = new CheckableLayoutViewBinder<>(binder);
            checkable.setLayoutId(R.layout.checkable_layout_item);
            checkable.setCheckMode(CheckableContainer.CheckMode.non_recursive);

            return checkable;
        }
    }
}
