package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public class BasicResultListener<T> implements ResultListener<T>{

	@Override
	public void onStart() 
	{}

	@Override
	public void onResult(T result) {
        handleResult(result);
        onFinish();
    }

	@Override
	public void onFailure(Throwable error) {
		handleError(error);
        onFinish();
	}


    protected void handleResult(T result) {}
    protected void handleError(Throwable error){error.printStackTrace();}
	protected void onFinish(){}

	@Override
	public boolean isListening() {
		return true;
	}
}
