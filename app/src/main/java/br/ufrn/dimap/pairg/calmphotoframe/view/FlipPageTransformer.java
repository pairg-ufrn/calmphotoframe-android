package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

public class FlipPageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.80f;
    
    public void transformPage(View view, float position) {
    	if (-0.5f <= position && position <= 0.5) { // [-0.5f,0.5] - view está do lado certo
        	view.setVisibility(View.VISIBLE);
        	
        	//Anula deslocamento da view, mantendo-a no mesmo lugar (centro do ViewPager)
            int dist = (int) (view.getWidth() * position);
            ViewCompat.setTranslationX(view, -dist);
            
            //Rotacionar view
            float degrees = (float) (180.0 * position);
        	ViewCompat.setRotationY(view, degrees);
        	
        	//Escalar para dar efeito mais suave à transição
        	float scale = (float) Math.max(MIN_SCALE, (Math.abs(Math.cos(position/3.0 * Math.PI))));
        	ViewCompat.setScaleX(view, scale);
        	ViewCompat.setScaleY(view, scale);
        } else { // (0.5,+Infinity] - view no lado oposto
        	view.setVisibility(View.GONE);
        }
    }
}
