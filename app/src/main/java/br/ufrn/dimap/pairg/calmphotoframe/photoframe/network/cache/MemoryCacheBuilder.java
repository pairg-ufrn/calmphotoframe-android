package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.CacheFactory.CacheBuilder;

public class MemoryCacheBuilder implements CacheBuilder {
	@Override
	public <K, V> Cache<K, V> buildCache(int maxSize) {
		return new MemoryCache<>(maxSize);
	}

}
