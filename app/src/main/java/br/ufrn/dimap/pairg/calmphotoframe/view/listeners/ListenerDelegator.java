package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

import java.util.LinkedList;
import java.util.List;

/**
 */
public class ListenerDelegator<T> {
    protected List<T> delegates = new LinkedList<>();

    public void addListener(T listener) {
        this.delegates.add(listener);
    }

    public void removeListener(T listener) {
        this.delegates.remove(listener);
    }
}
