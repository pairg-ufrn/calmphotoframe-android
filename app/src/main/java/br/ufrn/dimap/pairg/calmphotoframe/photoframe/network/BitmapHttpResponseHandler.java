package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicReference;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.graphics.Bitmap;
import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.BitmapDecoder;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.BitmapDecoders;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils.ReduceOptions;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;

public abstract class BitmapHttpResponseHandler extends AsyncHttpResponseHandler {
	private final int wantedImageWidth, wantedImageHeight;
	private final int[] maxImageSize;
	private final AtomicReference<Bitmap> atomicBitmapReference;

	private boolean sendProgress;
	public BitmapHttpResponseHandler() {
		this(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}
	public BitmapHttpResponseHandler(int maxWidth, int maxHeight) {
		this(new int[]{maxWidth, maxHeight}, null);
	}
	public BitmapHttpResponseHandler(int[] wantedImageSize, int[] maxImageSize) {
		this.wantedImageWidth = wantedImageSize[0];
		this.wantedImageHeight = wantedImageSize[1];
		
		if(maxImageSize == null){
			this.maxImageSize = null;
		}
		else{
			this.maxImageSize = new int[]{maxImageSize[0], maxImageSize[1]};
		}
		
		atomicBitmapReference = new AtomicReference<>();
		this.sendProgress = false;
	}
	@Override
    public void sendResponseMessage(HttpResponse response) throws IOException {
        // do not process if request has been cancelled
        if (!Thread.currentThread().isInterrupted()) {
            StatusLine status = response.getStatusLine();
            final int statusCode = status.getStatusCode();
            if (statusCode < 300 && statusCode >= 200) {
            	try{
	            	Bitmap image = tryDecodeBitmap(response.getEntity());
	            	sendResponse(response, statusCode, image);
            	}
            	catch(IOException ex){
            		Log.e(BitmapHttpResponseHandler.class.getSimpleName(), "Failed decode bitmap", ex);
            		
            		sendFailureMessage(statusCode, response.getAllHeaders(), null, ex);
            	}
            	finally{
            		AsyncHttpClient.endEntityViaReflection(response.getEntity());
            	}
            }
            else{
            	super.sendResponseMessage(response);
            }
        }
    }
	private void sendResponse(HttpResponse response, final int statusCode, Bitmap image) throws IOException {
		if(image != null && !Thread.currentThread().isInterrupted()){
			this.atomicBitmapReference.set(image);
			super.sendSuccessMessage(statusCode, response.getAllHeaders(), new byte[0]);
		}
		else if(image == null){
			super.sendFailureMessage(statusCode, response.getAllHeaders(), new byte[0], new Throwable("Could not parse response as bitmap."));
		}
		else{
		    super.sendResponseMessage(response);
		}
	}
	
	protected Bitmap tryDecodeBitmap(HttpEntity entity) throws IllegalStateException, IOException {
		if(entity != null && entity.getContentType().getValue().startsWith("image/") 
				&& entity.getContentLength() < Integer.MAX_VALUE)
		{
			
			return decodeSampledBitmapFromStream(entity.getContent(), entity.getContentLength(),
				new int[]{this.wantedImageWidth, this.wantedImageHeight});
		}
		return null;
	}
	
	@Override
	public void onProgress(long bytesWritten, long totalSize) {
		
	}
	@Override
	public void onSuccess(int statusCode, Header[] headers, byte[] imageData) {
		onSuccess(statusCode, headers, atomicBitmapReference.get());
	}
	
	public abstract void onSuccess(int arg0, Header[] arg1, Bitmap bitmap);

	/** Código adaptado de : https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
	 * @throws IOException */
	public Bitmap decodeSampledBitmapFromStream(InputStream stream, long contentLength,
	        int[] desiredImgDimensions) throws IOException {
//		byte[] readedBytes = getBytesFromStream(stream, contentLength);
//		if(readedBytes == null){
//			return null;
//		}
		
		BitmapDecoder decoder = BitmapDecoders.decoder(stream, (int)contentLength);
		ReduceOptions options = new ReduceOptions(desiredImgDimensions);
		options.setMaxDimensions(maxImageSize);
		
		return ImageUtils.instance().loadReducedBitmap(decoder, options);
	}
	
//	/**
//     * Returns byte array from stream
//     *
//     * @param stream a network
//     * @return response stream bytes or null
//     * @throws java.io.IOException if reading entity or creating byte array failed
//     */
//	protected byte[] getBytesFromStream(InputStream stream, long contentLength) throws IOException {
//		if (stream != null) {
//            int buffersize = (contentLength <= 0) ? BUFFER_SIZE : (int) contentLength;
//            try {
//                ByteArrayBuffer buffer = new ByteArrayBuffer(buffersize);
//                try {
//                    byte[] tmp = new byte[BUFFER_SIZE];
//                    int l, count = 0;
//                    // do not send messages if request has been cancelled
//                    while ((l = stream.read(tmp)) != -1 && !Thread.currentThread().isInterrupted()) {
//                        count += l;
//                        buffer.append(tmp, 0, l);
//                        if(isSendProgressEnabled()){
//                        	sendProgressMessage(count, (int) (contentLength <= 0 ? 1 : contentLength));
//                        }
//                    }
//                } finally {
//                    AsyncHttpClient.silentCloseInputStream(stream);
//                }
//                return buffer.toByteArray();
//            } catch (OutOfMemoryError e) {
//                System.gc();
//                throw new IOException("File too large to fit into available memory");
//            }
//        }
//		return null;
//	}

//	public void setSendProgressEnabled(boolean sendProgress) {
//		this.sendProgress = sendProgress;
//	}
//	public boolean isSendProgressEnabled() {
//		return this.sendProgress;
//	}
}
