package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMarker;

public class PhotoMarksNameAdapter extends BaseAdapter {
	public interface OnPhotoMarkChangeListener {
		public void onChangePhotoMarker(int markIndex, PhotoMarker mark);
	}
	public interface OnChangePhotoMarkerCollectionListener {
		/** Called when an item is added to the collection.
		 * @param marker the item added.
		 * @param index is the position where the item was added.
		 * */
		public void onAddMarker(PhotoMarker marker, int index);
		/** Called when an item is removed from collection.
		 * @param marker the item removed.
		 * @param from the index where the item was before.
		 * */
		public void onRemoveMarker(PhotoMarker marker, int from);
		/** Called when the order of items in collection change, 
		 * but the items themselves remains the same.
		 * @param initialIndex indicates the position in which the collection started to change.
		 * @param finalIndex indicates the last position reordered in the collection.
		 * */
		public void onReorderMarkers(int initialIndex, int finalIndex);
		/** Called when (perhaps) all the collection was change.
		 * @param newCollection contains all the lists on the new collection.
		 * */
		public void onChangeCollection(List<PhotoMarker> newCollection);
	}

	protected static final String LOG_TAG = null;
	private static final int START_EDITTEXT_ID = 1000;
	private Context context;
	private final List<PhotoMarker> marks;
	private OnPhotoMarkChangeListener photoMarkChangeListener;
	private OnChangePhotoMarkerCollectionListener changeCollectionListener;

	private int lastFocusedEditTextPosition, currentFocusedEditTextPosition;
	private int lastEditTextCursorPosition;
	
	public PhotoMarksNameAdapter(){
		this(null,new PhotoMarker[]{});
	}
	public PhotoMarksNameAdapter(Context context){
		this(context,new PhotoMarker[]{});
	}
	public PhotoMarksNameAdapter(Context context, List<PhotoMarker> photoMarks) {
		this(context, (PhotoMarker[])photoMarks.toArray());
	}
	public PhotoMarksNameAdapter(Context someContext, PhotoMarker[] photoMarks) {
		this.context = someContext;
		marks = new ArrayList<>(Arrays.asList(photoMarks));
		lastFocusedEditTextPosition = -1;
		currentFocusedEditTextPosition = -1;
		lastEditTextCursorPosition = -1;
	}
	
	public Context getContext() {
		return context;
	}
	public void setContext(Context context) {
		this.context = context;
	}

	public void setOnPhotoMarkChangeListener(OnPhotoMarkChangeListener listener) {
		this.photoMarkChangeListener = listener;
	}
	public void setOnChangePhotoMarkerCollectionListener(OnChangePhotoMarkerCollectionListener listener) {
		this.changeCollectionListener = listener;
	}
	
	public void addItem(PhotoMarker mark){
		requireNotNull(mark);
		
		this.marks.add(mark);
		this.notifyDataSetChanged();
		onAddItem(mark, marks.size() -1);
	}
	public void insert(PhotoMarker marker, int location) {
		requireNotNull(marker);
		this.marks.add(location, marker);
		this.notifyDataSetChanged();
		onAddItem(marker, location);
	}
	public boolean removeItem(PhotoMarker mark){
		requireNotNull(mark);
		int index = marks.indexOf(mark);
		if(index >=0){
			return this.removeItem(index);
		}
		return false;
	}
	public boolean removeItem(int index){
		requireValidIndex(index);
		PhotoMarker removed =this.marks.remove(index);
		this.notifyDataSetChanged();
		onRemoveItem(removed, index);	
		return true;
	}
	public void moveItem(int from, int to){
		requireValidIndex(from);
		requireValidIndex(to);
		if (from != to) {
			PhotoMarker marker = this.marks.remove(from);
			marks.add(to, marker);
            this.notifyDataSetChanged();
            onReorderCollection(from, to);
      }
	}
	
	public List<PhotoMarker> getItems(){
		return new CopyOnWriteArrayList<PhotoMarker>(this.marks);
	}
	public void setItems(List<PhotoMarker> mark){
		requireNotNull(mark);
		
		this.marks.clear();
		marks.addAll(mark);
		this.notifyDataSetChanged();
		onChangeCollection(new CopyOnWriteArrayList<>(mark));
	}
	
	@Override
	public int getCount() {
		return marks.size();
	}
	@Override
	public Object getItem(int position) {
		return marks.get(position);
	}
	@Override
	public long getItemId(int position) {
		if(position < 0 || position >= marks.size()){
			throw new IndexOutOfBoundsException();
		}
		return position;
	}

	public void onParentChangeFocus(ViewGroup parent, boolean hasFocus) {
		if(hasFocus && lastFocusedEditTextPosition >= 0 && lastFocusedEditTextPosition < getCount()){
			int lastFocusedEditTextId = createdEditTextId(lastFocusedEditTextPosition);
			boolean focused = focusView(parent, lastFocusedEditTextId);
			
			if(focused && lastEditTextCursorPosition >= 0){
				EditText focusedText = (EditText) parent.findViewById(lastFocusedEditTextId);
				if(lastEditTextCursorPosition < focusedText.getText().length()){
					focusedText.setSelection(lastEditTextCursorPosition);
				}
			}
			Log.d(getClass().getName(), "Focused pos(" + lastFocusedEditTextPosition + ") ? " + focused);
		}
	}
	@Override
	public View getView(final int position, View recycleView, final ViewGroup parent) {
		final PhotoMarker mark = (PhotoMarker) getItem(position);

		View view = createView(position, parent, recycleView);
		TextView numberTextView = (TextView) view.findViewById(R.id.photoMarkerItem_numberView);
		numberTextView.setText(String.valueOf(position + 1));

		setupEditText(position, parent, mark, view);		
		
		return view;
	}
	private void setupEditText(final int position, final ViewGroup parent,
			final PhotoMarker mark, View view) 
	{
//		EditText editText = (EditText) view.findViewById(R.id.photoMarkerItem_textEditor);
		CharSequence editTextTag = getContext().getText(R.string.photoMarkerItem_textEditor_tag);
		EditText editText = (EditText) view.findViewWithTag(editTextTag);
		editText.setText(mark.getIdentification());
		editText.setId(createdEditTextId(position));
		setupEditTextFocus(position, editText);
		
		editText.setOnFocusChangeListener(new PhotoMarkerOnFocusChangeListener(parent, position));
		editText.setOnEditorActionListener(new PhotoMarkerEditorActionListener(parent));
		setupChangeTextListener(position, mark, editText);
	}
	private void setupChangeTextListener(final int position,
			final PhotoMarker mark, EditText editText) {
		TextWatcher textWatcher = new PhotoMarkerTextWatcher(mark, position);
		editText.addTextChangedListener(textWatcher);
	}
	private void setupEditTextFocus(int position, EditText editText) {
		int nextPosition = position + 1 < this.getCount() 
								? position + 1 
								: 0;
		int previousPosition = position - 1 >= 0 
											? position - 1 
											: this.getCount() - 1;
		editText.setNextFocusDownId(createdEditTextId(nextPosition));
		editText.setNextFocusUpId(createdEditTextId(previousPosition));
	}

	private boolean focusView(final ViewGroup parent, int viewToFocus) {
		View focusView = parent.findViewById(viewToFocus);
		if(focusView != null){
			focusView.requestFocus();
			return true;
		}
		return false;
	}
	private int createdEditTextId(int position) {
		return START_EDITTEXT_ID + position;
	}
	private View createView(int position, ViewGroup parent, View recycleView) {
		//FIXME: ver modo de "reciclar" views - o problema está em como remover o TextWatcher do EditText
//		if(recycleView != null){
//			return recycleView;
//		}
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.listitem_photomarker, parent,false);
		return view;
	}
	
	/* **********************************************************************************************/
	private void onAddItem(PhotoMarker mark, int location) {
		if(this.changeCollectionListener != null){
			changeCollectionListener.onAddMarker(mark, location);
		}
	}
	private void onRemoveItem(PhotoMarker removed, int from) {
		if(this.changeCollectionListener != null){
			changeCollectionListener.onRemoveMarker(removed, from);
		}
	}
	private void onReorderCollection(int initialIndex, int finalIndex) {
		if(this.changeCollectionListener != null){
			changeCollectionListener.onReorderMarkers(initialIndex, finalIndex);
		}
	}
	private void onChangeCollection(List<PhotoMarker> newCollection) {
		if(this.changeCollectionListener != null){
			changeCollectionListener.onChangeCollection(newCollection);
		}
	}
	/* **********************************************************************************************/
	private void requireValidIndex(int index) {
		if(index <0 || index >= this.marks.size()){
			throw new IndexOutOfBoundsException("Index " + index + " is invalid.");
		}
	}
	private void requireNotNull(Object obj){
		if(obj == null){
			throw new NullPointerException();
		}
	}
	/* **********************************************************************************************/
	private final class PhotoMarkerTextWatcher implements TextWatcher {
		private final PhotoMarker mark;
		private final int markIndex;
		
		private PhotoMarkerTextWatcher(PhotoMarker mark, int markIndex) {
			this.mark = mark;
			this.markIndex = markIndex;
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) 
		{}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) 
		{}

		@Override
		public void afterTextChanged(Editable s) {
			mark.setIdentification(s.toString());
			if(photoMarkChangeListener != null){
				photoMarkChangeListener.onChangePhotoMarker(markIndex, mark);
			}
		}
	}

	private final class PhotoMarkerEditorActionListener implements OnEditorActionListener {
		private final ViewGroup parent;

		private PhotoMarkerEditorActionListener(ViewGroup parent) {
			this.parent = parent;
		}

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			Log.d(getClass().getName(), "onEditorAction. View: " + v + " action: " + actionId);
			if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT){
				Log.d(getClass().getName(), "Action Done!");
				return focusView(parent, v.getNextFocusDownId());
			}
			else if(actionId == EditorInfo.IME_ACTION_PREVIOUS){
				return focusView(parent, v.getNextFocusUpId());
			}
			return false;
		}
	}

	private final class PhotoMarkerOnFocusChangeListener implements OnFocusChangeListener {
		private final ViewGroup parent;
		private final int position;

		private PhotoMarkerOnFocusChangeListener(ViewGroup parent, int position) {
			this.parent = parent;
			this.position = position;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if(hasFocus && parent instanceof AbsListView){
				AbsListView listView = (AbsListView)parent;
				listView.smoothScrollToPosition(position);
				currentFocusedEditTextPosition = position;
				Log.d(getClass().getName(), v + " hasFocus");
			}
			else if(!hasFocus){
				Log.d(getClass().getName(), v + " lostFocus");
				lastFocusedEditTextPosition = position;
				if(currentFocusedEditTextPosition == position){
					currentFocusedEditTextPosition = -1;
					EditText editText = (EditText)v;
					lastEditTextCursorPosition = editText.getSelectionStart();
				}
			}
		}
	}
}
