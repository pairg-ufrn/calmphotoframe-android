package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

public enum HttpMethods {
    Get, Post, Put, Delete
}
