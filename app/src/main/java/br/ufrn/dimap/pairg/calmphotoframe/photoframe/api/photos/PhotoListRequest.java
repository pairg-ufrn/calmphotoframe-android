package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.PhotoListRequest.PhotoListContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class PhotoListRequest extends BaseGenericRequest<PhotoListContainer, List<Photo>>{
	
	public PhotoListRequest(ApiConnector apiConnector, ResultListener<? super List<Photo>> listener) {
		super(apiConnector, PhotoListContainer.class, listener);
	}

	@Override
	public RequestHandler execute(){
		return getRequest();
	}
	
	@Override
	protected String genUrl() {
		return getApiBuilder().photosUrl();
	}

	@Override
	protected List<Photo> convertToResultType(PhotoListContainer response) {
		return response.photos;
	}
	
	/** Class to help convert the json response to a java object*/
	public static class PhotoListContainer{
		public List<Photo> photos;
	}
}
