package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.text.format.DateUtils;

import java.util.Date;

public class DateFormatter {
    public CharSequence getRelative(Date date){
        long evtTime = date.getTime();
        return DateUtils.getRelativeTimeSpanString(evtTime, new Date().getTime()
                , DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL);
    }
}
