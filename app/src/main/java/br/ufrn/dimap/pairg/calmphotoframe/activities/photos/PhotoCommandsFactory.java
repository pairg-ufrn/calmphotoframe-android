package br.ufrn.dimap.pairg.calmphotoframe.activities.photos;

import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.NavActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.ExportPhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.ImportPhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.PhotoEditionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.RemoveCurrentPhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.SavePhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.SavePhotoCommand.SavePhotoResult;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnEditPhotoUpdate;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnExportPhotoNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnImportPhotoUpdateView;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnRemovePhotoNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnResultGetPhoto;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnSaveImageNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class PhotoCommandsFactory {

	public static void createCommands(CommandExecutor cmdExecutor
			, final BaseActivity activity, final PhotoHolder photoHolder)
	{
        putPhotoEditionCommands(cmdExecutor, activity, photoHolder);
        putPhotoExportCommands(cmdExecutor, activity, photoHolder);
	}

    private static void putPhotoEditionCommands(CommandExecutor cmdExecutor, BaseActivity activity, PhotoHolder photoHolder) {
        cmdExecutor.putCommand(R.id.action_editPhotoinfo
                             , new PhotoEditionCommand(activity, photoHolder)
                             , new OnEditPhotoUpdate(photoHolder));

        cmdExecutor.putCommand(R.id.action_removePhoto,
                                new RemoveCurrentPhotoCommand(activity, photoHolder),
                                new CompositeResultListener<>(
                                    new OnRemovePhotoNotifyUser(activity, photoHolder),
                                    new OnResultGetPhoto(activity, photoHolder)
                             ));
    }

    private static void putPhotoExportCommands(CommandExecutor cmdExecutor, final BaseActivity activity, final PhotoHolder photoHolder) {
        cmdExecutor.putCommand(R.id.action_savePhoto, new CommandBuilder<SavePhotoResult>() {
            @Override
            public @NonNull  Command<SavePhotoResult> build() {
                return new SavePhotoCommand (activity, photoHolder);
            }
        }, new OnSaveImageNotifyUser(activity));


        cmdExecutor.putCommand(R.id.action_importPhotoDescription, new CommandBuilder<Photo>() {
            @Override
            public @NonNull Command<Photo> build() {
                return new ImportPhotoCommand(activity, photoHolder);
            }
        }, new OnImportPhotoUpdateView(activity, photoHolder));

        cmdExecutor.putCommand(R.id.action_exportPhotoDescription, new CommandBuilder<Object>() {
            @Override
            public @NonNull Command<?> build() {
                return new ExportPhotoCommand(activity, photoHolder);
            }
        }, new OnExportPhotoNotifyUser(activity));
    }

//	public static ResultListener<? super Object> createRefreshViewListener(final BaseActivity activity, PhotoHolder photoHolder) {
//		return new OnResultGetPhoto(activity, photoHolder).setPhotoListener(new CompositeResultListener<>(
//                new OnResultInvalidateActivityMenu(activity)
//                , new OnResultRefreshNavMenu(activity)
//        ));
//	}

    private static class OnResultRefreshNavMenu extends BasicResultListener<Photo> {
        private final UserNotifier activity;

        public OnResultRefreshNavMenu(UserNotifier activity) {
            this.activity = activity;
        }

        @Override
        public void onResult(Photo result) {
            if(activity instanceof NavActivity){
                NavActivity navActivity = (NavActivity) activity;
                navActivity.refreshNavigationMenu();
            }
        }
    }
}
