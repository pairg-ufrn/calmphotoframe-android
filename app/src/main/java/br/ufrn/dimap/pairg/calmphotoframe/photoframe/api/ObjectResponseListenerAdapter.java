package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ObjectMapperResponseHandler;
import cz.msebera.android.httpclient.Header;

/**
 */
public class ObjectResponseListenerAdapter<MessageType, ResultType>
        implements ObjectMapperResponseHandler.ObjectMapperResponseListener<MessageType>
{
    String url;
    Converter<MessageType, ResultType> converter;
    final ResultListener<? super ResultType> listener;

    public ObjectResponseListenerAdapter(String url,
                                         Converter<MessageType, ResultType> converter,
                                         ResultListener<? super ResultType> listener)
    {
        this.url = url;
        this.converter = converter;
        this.listener = listener;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String message, MessageType parsedObject) {
        Listeners.onResult(listener, converter.convert(parsedObject));
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String message, Throwable error) {
        Listeners.onFailure(listener, getError(statusCode,message,error));
    }

    private Throwable getError(int statusCode, String message, Throwable error) {
        return ApiException.getError(statusCode, message, error, url);
    }
}
