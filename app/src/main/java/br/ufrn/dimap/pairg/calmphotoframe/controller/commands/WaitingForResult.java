package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public class WaitingForResult<T> extends BasicResultListener<T>{
	ResultListener<T> delegate;
	
	public WaitingForResult() {
		this.delegate = null;
	}

	public synchronized void startWaiting(ResultListener<T> delegate){
		this.delegate = delegate;
	}

	public synchronized void reset() {
		delegate = null;
	}
	public synchronized void cancel() {
		reset();
	}
	
	public synchronized boolean isWaiting(){
		return delegate != null;
	}
	
	@Override
	public synchronized void onStart()
	{
		Listeners.onStart(delegate);
	}

	@Override
	public synchronized void onResult(T result) {
        ResultListener<T> listener = delegate;

		if(isWaiting()){
            this.reset(); //If reset is after, listener could not change this delegator
			Listeners.onResult(listener, result);
		}
	}

	@Override
	public synchronized void onFailure(Throwable error) {
		ResultListener<T> listener = delegate;

		if(isWaiting()){
            this.reset(); //If reset is after, listener could not change this delegator
			Listeners.onFailure(listener, error);
		}
	}
	@Override
	public synchronized boolean isListening() {
		return isWaiting();
	}
}