package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.PhotoCacheRemoverListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class RemovePhotoRequest extends CommonGenericRequest<Photo>{
	private Photo photoToRemove;

	public RemovePhotoRequest(ApiConnector apiConnector, Photo photoToRemove) {
		this(apiConnector, photoToRemove, null);
	}
	public RemovePhotoRequest(ApiConnector apiConnector, Photo photoToRemove
			, ResultListener<? super Photo> removeListener) 
	{
		super(apiConnector, Photo.class);
		
		Photo.assertIsValid(photoToRemove);
		this.photoToRemove = photoToRemove;
		this.listener = new PhotoCacheRemoverListener(
									getApiConnector().getPhotoCache() , removeListener);
	}
	
	@Override
	public RequestHandler execute(){
		return deleteRequest();
	}
	
	@Override
	protected String genUrl() {
		return getApiBuilder().photoUrl(photoToRemove.getId());
	}
}