package br.ufrn.dimap.pairg.calmphotoframe.utils;


import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicReference;

public class Logging {
    public interface Logger{
        void v(String tag, String msg);
        void d(String tag, String msg);
        void i(String tag, String msg);
        void w(String tag, String msg);
        void e(String tag, String msg);
    }

    private static final AtomicReference<Logger> singleton = new AtomicReference<>();

    public static Logger instance(){
        Logger logger = singleton.get();
        if(logger == null){
            singleton.compareAndSet(null, buildDefault());
            logger = singleton.get();
        }

        return logger;
    }

    public static Logger buildDefault() {
        return PlatformUtils.isAndroid() ? new AndroidLogger() : new SysoutLogger();
    }

    public static void v(String tag, String msg){
        instance().v(tag, msg);
    }
    public static void d(String tag, String msg){
        instance().d(tag, msg);
    }
    public static void d(String tag, String msg, Throwable throwable){
        instance().d(tag, msg + "\n" + getMessage(throwable));
    }
    public static void i(String tag, String msg){
        instance().i(tag, msg);
    }
    public static void w(String tag, String msg){
        instance().w(tag, msg);
    }
    public static void w(String tag, Throwable throwable){
        instance().w(tag, getMessage(throwable));
    }

    public static void e(String tag, String msg){
        instance().e(tag, msg);
    }

    public static String getMessage(Throwable throwable) {
        if(throwable == null){
            return "";
        }

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        pw.flush();

        return sw.toString();
    }

    public static class AndroidLogger implements Logger {

        @Override
        public void v(String tag, String msg) {
            Log.v(tag, msg);
        }

        @Override
        public void d(String tag, String msg) {
            Log.d(tag, msg);
        }

        @Override
        public void i(String tag, String msg) {
            Log.i(tag, msg);
        }

        @Override
        public void w(String tag, String msg) {
            Log.w(tag, msg);
        }

        @Override
        public void e(String tag, String msg) {
            Log.e(tag, msg);
        }
    }

    public static class SysoutLogger implements Logger{
        public enum Level{
            Verbose("v"), Debug("d"), Info("i"), Warning("w"), Error("e");

            Level(String id){
                this.priorityId = id;
            }

            public String priorityId;

            public String format(String tag, String msg){
                return String.format("%s/%s: %s", priorityId, tag, msg);
            }
        }

        @Override
        public void v(String tag, String msg) {
            print(Level.Verbose, tag, msg);
        }

        @Override
        public void d(String tag, String msg) {
            print(Level.Debug, tag, msg);
        }

        @Override
        public void i(String tag, String msg) {
            print(Level.Info, tag, msg);
        }

        @Override
        public void w(String tag, String msg) {
            print(Level.Warning, tag, msg);
        }

        @Override
        public void e(String tag, String msg) {
            print(Level.Error, tag, msg);
        }

        private void print(Level level, String tag, String msg) {
            System.out.println(level.format(tag, msg));
        }
    }
}
