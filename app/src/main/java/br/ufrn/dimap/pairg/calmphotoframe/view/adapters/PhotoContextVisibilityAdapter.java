package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import java.util.Arrays;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextVisibility;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ContextVisibilityViewBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;

public class PhotoContextVisibilityAdapter extends ViewBuilderAdapter<PhotoContextVisibility> {
	public PhotoContextVisibilityAdapter() {
		this(new ContextVisibilityViewBuilder());
	}
	public PhotoContextVisibilityAdapter(ViewBuilder builder) {
		super(builder);
		this.setItems(Arrays.asList(PhotoContextVisibility.values()));
	}
}