package br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

/**
 * A base implementation of {@link SystemUiHider}. Uses APIs available in all
 * API levels to show and hide the status bar.
 */
public class SystemUiHiderBase extends SystemUiHider {
	/**
	 * Whether or not the system UI is currently visible. This is a cached value
	 * from calls to {@link #hide()} and {@link #show()}.
	 */
	private boolean mVisible = true;

	/**
	 * Constructor not intended to be called by clients. Use
	 * {@link SystemUiHider#getInstance} to obtain an instance.
	 */
	protected SystemUiHiderBase(Activity activity, View anchorView, int flags) {
		super(activity, anchorView, flags);
	}

	@Override
	public void setup() {
		if ((mFlags & FLAG_LAYOUT_IN_SCREEN_OLDER_DEVICES) == 0) {
			int flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
					| WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
			
			mActivity.getWindow().setFlags(flags, flags);
		}
	}

	@Override
	public boolean isVisible() {
		return mVisible;
	}

	protected Activity getActivity(){
		return mActivity;
	}
	
	@Override
	protected void changeSystemUIVisibility(boolean visible) {
		if ((mFlags & FLAG_FULLSCREEN) != 0) {
			if(visible){
				showStatusBar();
				showActionBar();
			}
			else{
				hideStatusBar();
				hideActionBar();
			}
		}
		mVisible = visible;
		mOnVisibilityChangeListener.onVisibilityChange(visible);
	}

	protected void hideStatusBar() {
		mActivity.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	protected void showStatusBar() {
		//Retira flag FLAG_FULLSCREEN
		mActivity.getWindow().setFlags(0,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	protected void hideActionBar(){
		ActionBar supportActionBar = getSupportActionBar();
		if(supportActionBar != null){
			supportActionBar.hide();
		}
	}

	protected void showActionBar(){
		ActionBar supportActionBar = getSupportActionBar();
		if(supportActionBar != null){
			supportActionBar.show();
		}
	}

	protected ActionBar getSupportActionBar() {
		ActionBar supportActionBar = null;
		if(mActivity instanceof AppCompatActivity){
			AppCompatActivity actionBarActivity = (AppCompatActivity)mActivity;
			supportActionBar = actionBarActivity.getSupportActionBar();
		}
		return supportActionBar;
	}
}
