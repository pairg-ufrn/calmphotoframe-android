package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.PhotoComponentImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;

/**
 */
public class PhotoComponentsBinderBuilder implements ItemsRecycleAdapter.BinderBuilder<PhotoComponent> {
    ImageLoader<PhotoComponent> imageLoader;

    public PhotoComponentsBinderBuilder(Context context) {
        this(new PhotoComponentImageLoader(context));
    }

    public PhotoComponentsBinderBuilder(ImageLoader<PhotoComponent> imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    public BinderViewHolder<PhotoComponent> buildViewBinder() {
        return new PhotoComponentsViewBinder(imageLoader);
    }
}
