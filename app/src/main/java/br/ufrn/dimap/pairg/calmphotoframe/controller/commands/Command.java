package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public interface Command<T> {
	void execute(ResultListener<? super T> cmdResultListener);
}
