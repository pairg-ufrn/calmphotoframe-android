package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiHelper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.RequestBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetUserRequest extends CommonGenericRequest<User>{
	private Long userId;
    private String connectionToken;

	public GetUserRequest(ApiConnector apiConnector) {
		this(apiConnector, null, null);
	}

	public GetUserRequest(ApiConnector apiConnector, ResultListener<? super User> listener) {
		this(apiConnector, null, listener);
	}
	public GetUserRequest(ApiConnector apiConnector, Long userId, ResultListener<? super User> listener) {
		super(apiConnector, User.class, listener);
		this.userId = userId;
	}

    public GetUserRequest userId(Long userId){
        this.userId = userId;
        return this;
    }

    public GetUserRequest token(String connectionToken){
        this.connectionToken = connectionToken;
        return this;
    }

    @Override
    protected RequestBuilder<User, User> request() {
        RequestBuilder<User, User> builder = super.request();

        if(connectionToken != null){
            builder.addHeader(ApiHelper.buildTokenCookieHeader(connectionToken));
        }

        return builder;
    }

    @Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		if(userId == null){
			return getApiBuilder().myUserUrl(endpoint());
		}
		return getApiBuilder().userUrl(endpoint(), userId);
	}

}
