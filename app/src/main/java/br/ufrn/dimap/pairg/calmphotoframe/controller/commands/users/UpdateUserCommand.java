package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class UpdateUserCommand extends BaseStatelessCommand<User>{
	private User user;
	
	public UpdateUserCommand(User user) {
		this.user = user;
	}
	
	@Override
	public void execute(ResultListener<? super User> listener) {
		ApiClient.instance().users().update(user, listener);
	}
}
