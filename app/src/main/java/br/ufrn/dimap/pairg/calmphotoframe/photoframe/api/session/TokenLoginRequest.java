package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session;

import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.GetUserRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class TokenLoginRequest extends CommonGenericRequest<ApiEndpoint> {
    private String connectionToken;

    public TokenLoginRequest(ApiConnector apiConnector, String connectionToken, Endpoint endpoint) {
        super(apiConnector, ApiEndpoint.class);
        this.connectionToken = connectionToken;
        this.endpoint(endpoint);
    }

    @Override
    public RequestHandler execute() {
        return new GetUserRequest(getApiConnector())
                .token(connectionToken)
                .endpoint(this.endpoint())
                .listener(buildDelegator())
                .execute();
    }

    @NonNull
    protected ResultListener<User> buildDelegator() {
        return new OnResultBuildPhotoFrameInfo(this.listener);
    }

    @Override
    protected String genUrl() {
        return null;
    }


    private class OnResultBuildPhotoFrameInfo extends ResultListenerDelegator<User, ApiEndpoint> {
        public OnResultBuildPhotoFrameInfo(ResultListener<? super ApiEndpoint> listener) {
            super(listener);
        }

        @Override
        protected ApiEndpoint convertResult(User result) {
            ApiEndpoint info = new ApiEndpoint(endpoint());
            info.setConnectionToken(connectionToken);
            info.getSession().setUser(result);

            return info;
        }
    }
}
