package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.support.v4.view.ViewCompat;
import android.view.View;

public class ViewHelper {

	public static int[] getScreenLocation(View v) {
		int screenLoc[] = new int[2];
		v.getLocationOnScreen(screenLoc);
		return screenLoc;
	}
	public static int[] getViewCenterOnScreen(View v) {
		int screenLoc[] = getScreenLocation(v);
		int midWidth = v.getWidth()/2;
		int midHeight = v.getHeight()/2;
		
		return new int[]{
				screenLoc[0] + midWidth,
				screenLoc[1] + midHeight
		};
	}
	public static int[] getLocationRelativeTo(int[] screenLocation, View view) {
		int [] viewAbsolutePosition = new int[2];
		view.getLocationOnScreen(viewAbsolutePosition);

		return new int[]{
				screenLocation[0] - viewAbsolutePosition[0],
				screenLocation[1] - viewAbsolutePosition[1]
		};
	}
	public static float[] getPercentualLocationOnView(int[] screenLocation, View view) 
	{
		int originPos[] = getScreenLocation(view);
		float percentualX = (screenLocation[0] - originPos[0])/(float)(view.getWidth());
		float percentualY = (screenLocation[1] - originPos[1])/(float)(view.getHeight());
		return new float[]{percentualX, percentualY};
	}
	public static void setViewTranslation(View view, float relativeX, float relativeY) {
		ViewCompat.setTranslationX(view, relativeX);
		ViewCompat.setTranslationY(view, relativeY);
	}

}
