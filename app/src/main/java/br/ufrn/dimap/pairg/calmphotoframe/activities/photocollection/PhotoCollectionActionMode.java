package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.support.v7.view.ActionMode;
import android.view.Menu;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;

public class PhotoCollectionActionMode extends BaseActionModeCallback
                                              implements MultiSelectionListener
{
	private PhotoComponentSelector selector;
	
	public PhotoCollectionActionMode(PhotoComponentSelector selector) {
		super(R.menu.contextualmenu_photocollection);
		this.selector = selector;
	}
	
	@Override
	public void onItemSelectionChanged(int selectionCount, int position, long id, boolean checked) {
		if(getActionMode() != null){
			getActionMode().invalidate();
		}
	}
	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		Counters counter = countItems();
		
		boolean singleAlbumMode = (counter.photoCount == 0 && counter.collectionCount == 1);
		menu.setGroupVisible(R.id.menugroup_singleCollectionActions, singleAlbumMode);
		menu.setGroupEnabled(R.id.menugroup_singleCollectionActions, singleAlbumMode);

		boolean singlePhotoMode = (counter.photoCount == 1 && counter.collectionCount == 0);
		menu.setGroupVisible(R.id.menugroup_singlePhotoActions, singlePhotoMode);
		menu.setGroupEnabled(R.id.menugroup_singlePhotoActions, singlePhotoMode);
		
		return true;
	}
	private Counters countItems() {
		Counters counters = new Counters();
		
		for(PhotoComponent component : selector.getSelectedItems()){
			if(component.isCollection()){
				counters.collectionCount++;
			}
			else{
				counters.photoCount++;
			}
		}
		
		return counters;
	}
	
	@Override
	public void onStartSelection() 
	{}
	@Override
	public void onEndSelection() 
	{}
	
	/* *************************************************************************************/
	static class Counters{
		public int photoCount, collectionCount;
		public Counters() {
			photoCount = 0;
			collectionCount = 0;
		}
	}
}