package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;


import android.view.View;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class UserViewBinder extends InflaterViewBinder<User>  {

	private TextView userName, loginView;
	private UserImageViewBinder imageViewBinder;

	private int userNameId = R.id.user_name;
	private int loginViewId = 0;


	public UserViewBinder(){
		this(R.layout.view_user);
        this.loginViewId = R.id.user_login;
	}

	public UserViewBinder(int layoutId) {
		super(layoutId);
		imageViewBinder = new UserImageViewBinder();
	}

	@Override
	public void setup(View itemView) {
		this.userName 	= ViewUtils.getView(itemView, getUserNameId());
		this.loginView  = ViewUtils.getView(itemView, getLoginViewId());

		this.imageViewBinder.setup(itemView);
	}

	@Override
	public void bind(User user) {
		boolean has_user = (user != null);

		this.setVisible(has_user);

		if(has_user){
            bindTextView(userName, user.getName());
            bindTextView(loginView, user.getLogin());
		}

		imageViewBinder.bind(user);
	}

    public void bindTextView(TextView textView, String text) {
        if(textView != null) {
            textView.setText(text);
            ViewUtils.setVisible(textView, text != null && !text.isEmpty());
        }
    }

    public void setEnabled(boolean enabled) {
		userName.setEnabled(enabled);
		imageViewBinder.setEnabled(enabled);
	}

	public void setVisible(boolean visible){
		ViewUtils.setVisible(userName, visible);
		imageViewBinder.setVisible(visible);
	}

	public int getUserNameId() {
		return userNameId;
	}

	public void setUserNameId(int userNameId) {
		this.userNameId = userNameId;
	}

	public void setImageViewId(int imageView) {
		this.imageViewBinder.imageViewId(imageView);
	}

	public int getLoginViewId() {
		return loginViewId;
	}

	public void setLoginViewId(int loginViewId) {
		this.loginViewId = loginViewId;
	}

	public TextView getUserNameView() {
        return userName;
    }

	public UserImageViewBinder getImageViewBinder() {
		return this.imageViewBinder;
	}
	public void setImageViewBinder(UserImageViewBinder imageViewBinder) {
		this.imageViewBinder = imageViewBinder;
	}

	public User getBoundUser() {
		return getImageViewBinder().getBoundUser();
	}
}
