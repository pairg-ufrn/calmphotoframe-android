package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public final class SearchHandler implements OnQueryTextListener {
	public interface SearchUiListener{
		void onPrepareSearchUi();
		void onFinishSearchUi();
	}
	
	public interface SearchEngine{
		void doSearch(String query);
	}
	
	private SearchEngine searchEngine;
	private final SearchUiListenerAdapter searchUiListenerAdapter;
	
	public SearchHandler(SearchEngine searchEngine) {
		this.searchEngine = searchEngine;
		searchUiListenerAdapter = new SearchUiListenerAdapter();
	}
	
	public SearchUiListener getSearchUiListener() {
		return searchUiListenerAdapter.getListener();
	}
	public void setSearchUiListener(SearchUiListener searchUiListener) {
		this.searchUiListenerAdapter.setListener(searchUiListener);
	}

	public void setup(Menu menu, int menuItem){
		MenuItem searchMenuItem = menu.findItem(menuItem);
		SearchView searchView = (SearchView)MenuItemCompat.getActionView(searchMenuItem);
		searchView.setOnQueryTextListener(this);
		
		searchView.setOnCloseListener(new OnCloseListener() {
			@Override
			public boolean onClose() {
				Log.d(getClass().getSimpleName(), "Closing search view");
				return false;
			}
		});
		
		MenuItemCompat.setOnActionExpandListener(searchMenuItem, searchUiListenerAdapter);
	
//		final View mSearchEditFrame = searchView
//	            .findViewById(android.support.v7.appcompat.R.id.search_edit_frame);
//
//	    ViewTreeObserver vto = mSearchEditFrame.getViewTreeObserver();
//	    vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
//	        int oldVisibility = -1;
//
//	        @Override
//	        public void onGlobalLayout() {
//
//	            int currentVisibility = mSearchEditFrame.getVisibility();
//
//	            if (currentVisibility != oldVisibility) {
//	                if (currentVisibility == View.VISIBLE) {
//	                    Log.v(getClass().getSimpleName(), "EXPANDED");
//	                } else {
//	                    Log.v(getClass().getSimpleName(), "COLLAPSED");
//	                }
//
//	                oldVisibility = currentVisibility;
//	            }
//
//	        }
//	    });
	}
	
	@Override
	public boolean onQueryTextSubmit(String query) {
		searchEngine.doSearch(query);
		return true;
	}

	@Override
	public boolean onQueryTextChange(String query) {
		return false;
	}
	
	private static class SearchUiListenerAdapter implements OnActionExpandListener {
		
		private SearchUiListener listener;
		
		public SearchUiListener getListener() {
			return listener;
		}
		public void setListener(SearchUiListener listener) {
			this.listener = listener;
		}

		@Override
		public boolean onMenuItemActionExpand(MenuItem arg0) {
			if(getListener() != null){
				getListener().onPrepareSearchUi();
			}
			return true;
		}

		@Override
		public boolean onMenuItemActionCollapse(MenuItem arg0) {
			Log.d(getClass().getSimpleName(), "Collapsed view!");
			if(getListener() != null){
				listener.onFinishSearchUi();
			}
			return true;
		}
	}
}