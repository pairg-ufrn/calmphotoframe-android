package br.ufrn.dimap.pairg.calmphotoframe.controller;

import android.content.Intent;

public interface OnActivityResultListener {
	void onActivityResult(int resultCode, Intent intent);
}
