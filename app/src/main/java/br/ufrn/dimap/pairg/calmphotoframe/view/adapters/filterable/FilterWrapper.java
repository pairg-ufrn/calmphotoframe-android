package br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FilterWrapper<T> extends Filter {
	private CollectionFilter<T> filter;
    private FilterableAdapter<T> adapter;

	private boolean defaultIsEmpty;

	public FilterWrapper(FilterableAdapter<T> adapter, CollectionFilter<T> collectionFilter) {
        this.filter = collectionFilter;
        this.adapter = adapter;
        defaultIsEmpty = false;
    }

	public boolean isDefaultEmpty() {
        return defaultIsEmpty;
    }

	public void setDefaultEmpty(boolean defaultIsEmpty) {
        this.defaultIsEmpty = defaultIsEmpty;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.setFilteredItems((List<T>) results.values);
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

		Collection<T> items = adapter.getOriginalItems();
        if (constraint != null && constraint.length() != 0) {
            List<T> filtered = filter.filter(items, constraint);
            results.count = filtered.size();
            results.values = filtered;
        } else{
            if (isDefaultEmpty()) {
                results.count = 0;
                results.values = Collections.emptyList();
            } else {
                results.count = items.size();
                results.values = new ArrayList<>(items);
            }
        }

		return results;
    }
}
