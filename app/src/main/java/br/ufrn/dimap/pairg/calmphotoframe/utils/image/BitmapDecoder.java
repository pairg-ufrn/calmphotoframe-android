package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public interface BitmapDecoder{
	public Bitmap decodeBitmap(BitmapFactory.Options options)  throws IOException;
	public void clear();
}