package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMetadata;

public class PhotoInfoEditorFragment extends Fragment {
//	private static final String LOG_TAG = PhotoInfoEditorFragment.class.getName();
	
	Photo photoDescriptor;
	EditText editMoment;
	EditText editDescription;
	EditText editPlace;
	EditText editDate;
	EditText editCredits;
	
	boolean viewsCreated;
	
	public PhotoInfoEditorFragment(){
		super();
		photoDescriptor = null;
		viewsCreated = false;
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photoinfoeditor, container, false);

        setupViews(rootView);
        
        if(photoDescriptor != null){
        	setFromPhotoDescriptor(photoDescriptor);
        }
        
        return rootView;
    }
    private void setupViews(View rootView) {
    	editMoment = (EditText)rootView.findViewById(R.id.photoInfoEditor_momentEdit);
		editDescription = (EditText)rootView.findViewById(R.id.photoInfoEditor_descriptionEdit);
		editPlace = (EditText)rootView.findViewById(R.id.photoInfoEditor_placeEdit);
		editDate = (EditText)rootView.findViewById(R.id.photoInfoEditor_dateEdit);
		editCredits = (EditText)rootView.findViewById(R.id.photoInfoEditor_creditsEdit);
		
		viewsCreated = true;
	}

	public void setFromPhotoDescriptor(Photo descriptor){
    	this.photoDescriptor = descriptor;
    	if(viewsCreated){
	    	if(photoDescriptor != null){		
				editMoment.setText(photoDescriptor.getMoment() != null ? photoDescriptor.getMoment() : "");
				editDescription.setText(photoDescriptor.getDescription() != null ? photoDescriptor.getDescription() : "");
				editPlace.setText(photoDescriptor.getPlace() != null ? photoDescriptor.getPlace() : "");
				editDate.setText(photoDescriptor.getDate() != null ? photoDescriptor.getDate() : "");
				editCredits.setText(getCreditsText());
			}
	    	else{
	    		clearViews();
	    	}
    	}
    }

	private void clearViews() {
		editMoment.setText("");
		editDescription.setText("");
		editPlace.setText("");
		editDate.setText("");
		editCredits.setText("");
	}

	public void updatePhotoDescriptor(Photo photoDescriptor) {
		photoDescriptor.setDate(editDate.getText().toString());
		photoDescriptor.setPlace(editPlace.getText().toString());
		photoDescriptor.setMoment(editMoment.getText().toString());
		photoDescriptor.setDescription(editDescription.getText().toString());
		updateCreditsText(photoDescriptor);
	}

	private String getCreditsText() {
		if(photoDescriptor.getMetadata() != null && photoDescriptor.getCredits() != null){
			return photoDescriptor.getCredits();
		}
		return "";
	}
	private void updateCreditsText(Photo photoDescriptor) {
		if(editCredits.getText().length() > 0 || photoDescriptor.getMetadata() != null){
			if(photoDescriptor.getMetadata() == null){
				photoDescriptor.setMetadata(new PhotoMetadata());
			}
			photoDescriptor.setCredits(editCredits.getText().toString());
		}
	}
}
