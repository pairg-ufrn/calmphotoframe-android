package br.ufrn.dimap.pairg.calmphotoframe.activities.photos;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;

/* ******************************* Helper Nested Classes ************************************/
public class PhotoComponentSelectorImpl implements PhotoComponentSelector {
	private final ItemSelector<PhotoComponent> componentsSelector;
	private CollectionAndContextContainer state;

	public PhotoComponentSelectorImpl(CollectionAndContextContainer state,
			ItemSelector<PhotoComponent> photosFragment)
	{
		this.componentsSelector = photosFragment;
		this.state = state;
	}

	@SuppressWarnings("unchecked")
    @Override
	public Collection<PhotoComponent> getSelectedItems() {
		return (Collection<PhotoComponent>) componentsSelector.getSelectedItems();
	}

	@Override
	public PhotoCollection getCurrentPhotoCollection() {
		return state.getPhotoCollection();
	}

	@Override
	public PhotoContext getCurrentContext() {
		return state.getPhotoContext();
	}
}