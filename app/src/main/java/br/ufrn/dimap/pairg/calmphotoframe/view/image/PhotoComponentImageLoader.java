package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;

public class PhotoComponentImageLoader implements ImageLoader<PhotoComponent> {
    Context context;

	public PhotoComponentImageLoader(Context ctx) {
        this.context = ctx;
    }

    @Override
    public RequestHandler loadImage(PhotoComponent entity, ResultListener<? super Drawable> listener) {
        return loadPhotoComponentThumbnail(entity, new ResultListenerDelegator<PhotoImageWrapper,Drawable>(listener) {
            @Override
            protected Drawable convertResult(PhotoImageWrapper result) {
                return new BitmapDrawable(context.getResources(), result.getImage());
            }
        });
    }

    @Override
    public RequestHandler loadImage(PhotoComponent entity, ImageView target) {
        return loadImage(entity, new ViewDrawableResultListener(target));
    }


    protected RequestHandler loadPhotoComponentThumbnail(PhotoComponent photoComponent, ResultListener<? super PhotoImageWrapper> listener) {
        if (photoComponent == null) {
            throw new IllegalStateException("Trying to load image from inexistent item!");
        }

        long photoId;
        if (!photoComponent.isCollection()) {
            photoId = photoComponent.getPhoto().getId();
        } else {
            photoId = photoComponent.getPhotoCollection().getPhotoCoverId();
        }
        if (photoId > 0) {
            return ApiClient.instance().photos().getThumbnail(photoId, listener);
        }

        return new FinishedRequestHandler();
    }

}
