package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderHolderWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.CollaboratorViewBinder;

public class CollaboratorsAdapter extends ViewBuilderRecyclerAdapter<Collaborator> {
	private List<Collaborator> items;
	private final Set<Long> disabledCollaborators;
	private Long currentUserId;
	private boolean editable;
	
	public CollaboratorsAdapter() {
		items = new ArrayList<>();
		disabledCollaborators = new HashSet<>();
		editable = true;
		
		setHasStableIds(true);
	}

	public void setCurrentUserId(Long currentUserId) {
		this.currentUserId = currentUserId;
	}
	
	public Collection<Collaborator> getItems() {
		return items;
	}
	public void setItems(Collection<Collaborator> items) {
		this.items.clear();
		this.items.addAll(items);
		this.notifyDataSetChanged();
	}
	public void addItem(Collaborator item){
		this.items.add(item);
		notifyItemInserted(items.size() - 1);
	}
	@Override
	public int getItemCount() {
		return items.size();
	}
	
	@Override
	public long getItemId(int position) {
		return getItemId(items.get(position));
	}
	
	public Collaborator getItemById(long itemId) {
		int pos = getPositionById(itemId);
		return pos >=0 ? items.get(pos) : null;
	}

	public int getPositionById(long itemId) {
		int pos = 0;
		for(Collaborator item : items){
			if(getItemId(item) == itemId){
				return pos;
			}
			++pos;
		}
		return -1;
	}
	protected long getItemId(Collaborator item) {
		return item.getUser().getId();
	}

	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public void enableCollaborator(Long id) {
		disabledCollaborators.remove(id);
		itemChanged(id);
	}
	public void disableCollaborator(Long id) {
		disabledCollaborators.add(id);
		itemChanged(id);
	}
	public boolean isCollaboratorEnabled(Long id){
		return isEditable() && !disabledCollaborators.contains(id);
	}
	
	private void itemChanged(Long id) {
		int pos = getPositionById(id);
		if(pos >= 0){
			notifyItemChanged(pos);
		}
	}

	@Override
	public Collaborator getEntity(int pos) {
		return items.get(pos);
	}

	@Override
	public BinderViewHolder<Collaborator> buildViewBinder() {
		return new CollaboratorViewBinder();
	}
	@Override
	public void onBindViewHolder(BinderHolderWrapper<Collaborator> holderWrapper, int pos) {
		super.onBindViewHolder(holderWrapper, pos);
		
		final long itemId = getItemId(pos);
		boolean itemEnabled = isCollaboratorEnabled(itemId);
		
		CollaboratorViewBinder collaboratorBinder = (CollaboratorViewBinder)holderWrapper.getBinderViewHolder();
		
		collaboratorBinder.setIsCurrentUser(currentUserId != null && currentUserId == itemId);
		collaboratorBinder.setOnClickDeleteListener(new OnRequestRemoveItem(itemId));
		collaboratorBinder.setOnChangePermissionSelector(new OnChangePermissionLevel(itemId, collaboratorBinder));
		collaboratorBinder.setEnabled(itemEnabled);
	}

	/* ********************************************************************************************/
	
	protected void updateItemPermission(final long itemId, PermissionLevel permissionLevel) {
		Collaborator collaborator = getItemById(itemId);
		if(collaborator != null){
			collaborator.getPermission().setLevel(permissionLevel);
		}
	}

	protected void removeItem(long itemId) {
		Collaborator item = getItemById(itemId);
		if(item != null){
			onRemovedItem(getItemId(item));
		}
	}
	
	private void onRemovedItem(long itemId) {
		int pos = getPositionById(itemId);
		if(pos >= 0){
			this.items.remove(pos);
			this.notifyItemRemoved(pos);
		}
	}

/* ********************************************************************************************/
	
	private class OnRequestRemoveItem implements OnClickListener {
		private final long itemId;

		private OnRequestRemoveItem(long itemId) {
			this.itemId = itemId;
		}

		@Override
		public void onClick(View v) {
			removeItem(itemId);
		}
	}

	private class OnChangePermissionLevel implements OnItemSelectedListener {
		private final long itemId;
		private final CollaboratorViewBinder viewHolder;

		private OnChangePermissionLevel(long itemId, CollaboratorViewBinder viewHolder) {
			this.itemId = itemId;
			this.viewHolder = viewHolder;
		}

		@Override
		public void onItemSelected(AdapterView<?> parent,
		        View view, int pos, long id) 
		{
			PermissionLevel permissionLevel = viewHolder.permissionAdapter.getItem(pos);
			updateItemPermission(itemId, permissionLevel);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		  // Do nothing.
		}
	}
}