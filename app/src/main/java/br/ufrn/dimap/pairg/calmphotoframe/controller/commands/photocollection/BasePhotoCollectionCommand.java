package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.SelectionDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.SelectionDialog.OnSelectItemListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.PhotoCollectionTextViewBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;

public abstract class BasePhotoCollectionCommand<T>  extends ActivityStatelessCommand<T>
{
	public interface PhotoComponentSelector{
		Collection<PhotoComponent> getSelectedItems();
		PhotoCollection getCurrentPhotoCollection();
		PhotoContext getCurrentContext();
	}

	private static final String DIALOG_TAG = BasePhotoCollectionCommand.class + ".DIALOG_TAG";
	
	private PhotoComponentSelector selector;
	private Collection<PhotoComponent> selectedItems;
	
	private boolean multipleTargets;
	
	public BasePhotoCollectionCommand(BaseActivity activity, PhotoComponentSelector selector) {
		super(activity);
		this.selector = selector;
		multipleTargets = false;
	}

	public PhotoComponentSelector getSelector() {
		return this.selector;
	}
	
	public boolean isMultipleTargetsEnabled() {
		return multipleTargets;
	}
	public void setMultipleTargets(boolean useMultipleTargets) {
		this.multipleTargets = useMultipleTargets;
	}

	@Override
	public void execute(ResultListener<? super T> listener) {
		notifyOnStart(listener);
		setSelectedItems(selector.getSelectedItems());
		ApiClient.instance().collections().listByContext(
                new OnGetPhotoCollectionsShowDialog(selector.getCurrentContext(), listener));
	}

	protected SelectionDialog<PhotoCollection> createSelectionDialog(
            final PhotoContext currentContext,
            List<PhotoCollection> collectionsList,
            ResultListener<? super T> listener)
	{
		ViewBuilder itemViewBuilder = new PhotoCollectionTextViewBuilder(getActivity(), currentContext.getRootCollectionId());
		SelectionDialog<PhotoCollection> albunsSelectionDialog 
			= SelectionDialog.build(itemViewBuilder);
		
		albunsSelectionDialog.setMultipleSelection(false);

		albunsSelectionDialog.setDialogTitle(getActivity().getString(R.string.dialog_selectPhotoCollection_title));
		albunsSelectionDialog.setDialogMessage(getActivity().getString(R.string.dialog_selectPhotoCollection_message));
		customizeSelectionDialog(albunsSelectionDialog);
		albunsSelectionDialog.setSelectItemListener(new OnSelectItemsExecuteOperation(listener));
		
		Collection<PhotoCollection> selectedCollections 
				= PhotoComponent.filterPhotoCollections(getSelectedItems());
		albunsSelectionDialog.setDisabledItens(selectedCollections);
		
		return albunsSelectionDialog;
	}

    class OnSelectItemsExecuteOperation implements OnSelectItemListener<PhotoCollection>{
        ResultListener<? super T> listener;

        public OnSelectItemsExecuteOperation(ResultListener<? super T> listener) {
            this.listener = listener;
        }

        @Override
        public void onSelectItems(Collection<? extends PhotoCollection> selectedItems) {
            if(getSelectedItems() != null && getSelectedItems().isEmpty() == false){
                executeItemsCommand(selectedItems, getSelectedItems(), listener);
            }
        }
    }
	
	protected abstract void executeItemsCommand(Collection<? extends PhotoCollection> collections,
                                                Collection<PhotoComponent> selectedItems,
                                                ResultListener<? super T> listener);

	protected void customizeSelectionDialog(SelectionDialog<PhotoCollection> selectAlbumDialog) 
	{}
	
	protected String getString(int stringId){
		return getActivity().getResources().getString(stringId);
	}

	protected Collection<PhotoComponent> getSelectedItems() {
		return selectedItems;
	}
	protected void setSelectedItems(Collection<PhotoComponent> selectedItems) {
		this.selectedItems = selectedItems;
	}
	
	/* *********************************** Helper Classes *****************************************/
	
	class OnGetPhotoCollectionsShowDialog extends BasicResultListener<List<PhotoCollection>> {
		PhotoContext currentCtx;
        ResultListener<? super T> resultListener;
		
		public OnGetPhotoCollectionsShowDialog(PhotoContext currentContext, ResultListener<? super T> listener) {
			this.currentCtx = currentContext;
            this.resultListener = listener;
		}
		
		@Override
		public void onResult(List<PhotoCollection> result) {
			SelectionDialog<PhotoCollection> selectAlbumDialog = createSelectionDialog(currentCtx, result, resultListener);
			selectAlbumDialog.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
		}

		@Override
		public void onFailure(Throwable error) {
			notifyOnFailure(error, resultListener);
		}
	}
}
