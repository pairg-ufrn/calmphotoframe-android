package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.widget.TextView;

public class SimpleTextViewBuilder extends InflaterViewBuilder{
	private boolean multipleChoice;
	
	public SimpleTextViewBuilder() {
		super(0);
	}
	
	public boolean isMultipleChoice() {
		return multipleChoice;
	}
	public void setMultipleChoice(boolean multipleChoice) {
		this.multipleChoice = multipleChoice;
	}

	@Override
	protected void setupView(View view, Object data) {
		TextView txtView = (TextView)view.findViewById(android.R.id.text1);
		txtView.setText(getText(data));
	}
	
	protected String getText(Object data) {
		return String.valueOf(data);
	}

	@Override
	public int getLayoutId() {
		return isMultipleChoice() ? 
							android.R.layout.simple_list_item_multiple_choice
							: android.R.layout.simple_list_item_1;
	}

}
