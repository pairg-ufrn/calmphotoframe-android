package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginController;

public class LoginActivity extends BaseActivity {
	LoginController controller;
	
	public LoginActivity() {
		super();
		controller = new LoginController(this);
	}

	public LoginController getController(){
		return controller;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
        setSnackbarViewId(R.id.root_view);

		controller.setupView();
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		controller.refreshView();
	}
}
