package br.ufrn.dimap.pairg.calmphotoframe.activities.pickimages;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import java.io.File;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PickImagesActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.Image;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;

/**
 */
public class FinishImageSelectionCommand extends ActivityStatelessCommand<Object> {
    private boolean positiveResult;
    ItemSelector<Image> imageSelector;
    boolean isMultiSelection;

    public FinishImageSelectionCommand(BaseActivity activity, ItemSelector<Image> imageSelector) {
        super(activity);
        this.positiveResult = true;
        this.imageSelector = imageSelector;
    }

    public FinishImageSelectionCommand setResult(boolean positiveResult) {
        this.positiveResult = positiveResult;
        return this;
    }

    public FinishImageSelectionCommand setMultiSelection(boolean isMultiSelectionGetter) {
        this.isMultiSelection = isMultiSelectionGetter;
        return this;
    }

    @Override
    public void execute(ResultListener<? super Object> cmdResultListener) {
        finishSelection();
    }

    boolean isMultiSelectionEnabled() {
        return isMultiSelection;
    }

    protected void finishSelection() {
        if (!positiveResult) {
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            if (isMultiSelectionEnabled()) {
                Collection<? extends Image> images = imageSelector.getSelectedItems();
                finishMultiSelection(images);
            } else {
                Image selected = imageSelector.getSelectedItem();
                if (selected != null) {
                    finishSingleSelection(selected);
                }
            }
        }

        getActivity().finish();
    }

    protected void finishMultiSelection(Collection<? extends Image> images) {
        Intent resultIntent = new Intent();

        resultIntent.putExtra(PickImagesActivity.EXTRA_SELECTED_IMAGES, getImagesPaths(images));

        if (Build.VERSION.SDK_INT >= 16) {
            putImagesOnClipData(images, resultIntent);
        }

        getActivity().setResult(Activity.RESULT_OK, resultIntent);
        getActivity().finish();
    }

    protected void finishSingleSelection(Image image) {
        Uri dataUri = imageToUri(image);
        Intent resultIntent = new Intent();
        resultIntent.setDataAndType(dataUri, image.getMimeType());
        getActivity().setResult(Activity.RESULT_OK, resultIntent);
        getActivity().finish();
    }

    private Uri imageToUri(Image image) {
        return Uri.fromFile(new File(image.getUrl()));
    }

    private String[] getImagesPaths(Collection<? extends Image> images) {
        String[] paths = new String[images.size()];

        int i = 0;
        for (Image img : images) {
            paths[i] = img.getUrl();
            ++i;
        }
        return paths;
    }

    @TargetApi(16)
    private void putImagesOnClipData(Collection<? extends Image> images,
                                     Intent resultIntent) {
        ClipData clipData = null;
        for (Image img : images) {
            ClipData.Item imgItem = imageToClipItem(img);
            if (clipData == null) {
                ClipDescription description = new ClipDescription("Selected images", new String[]{"image/*"});
                clipData = new ClipData(description, imgItem);
            } else {
                clipData.addItem(imgItem);
            }
        }
        resultIntent.setClipData(clipData);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private ClipData.Item imageToClipItem(Image img) {
        return new ClipData.Item(imageToUri(img));
    }
}
