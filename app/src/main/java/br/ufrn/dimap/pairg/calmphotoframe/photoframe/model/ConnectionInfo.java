package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;


public class ConnectionInfo implements Cloneable{
	private Endpoint endpoint;
	private LoginInfo loginInfo;
	
	public ConnectionInfo() {
		this(null, null);
	}
	public ConnectionInfo(String hostname, Integer port, LoginInfo loginInfo) {
		this(new ApiEndpoint(hostname,port), loginInfo);
	}
	public ConnectionInfo(Endpoint endpoint, LoginInfo loginInfo) {
		super();
		this.endpoint = endpoint;
		this.loginInfo = loginInfo;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}
	public LoginInfo getLoginInfo() {
		return loginInfo;
	}
	public void setLoginInfo(LoginInfo loginInfo) {
		this.loginInfo = loginInfo;
	}

	public boolean isLocal() {
		return getEndpoint().isLocal();
	}

	@Override
	public ConnectionInfo clone() {
		ConnectionInfo clone = new ConnectionInfo();
		clone.setLoginInfo(getLoginInfo() == null ? null : getLoginInfo().clone());
		clone.setEndpoint(getEndpoint() == null ? null : getEndpoint().clone());
		return clone;
	}
	
	@Override
	public String toString() {
		return "ConnectionInfo [getEndpoint=" + endpoint + ", loginInfo=" + loginInfo + "]";
	}
}