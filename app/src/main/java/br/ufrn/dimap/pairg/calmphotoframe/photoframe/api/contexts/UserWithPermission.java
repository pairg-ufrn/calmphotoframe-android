package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Permission;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class UserWithPermission extends User{
	public Permission permission;
	
	public UserWithPermission(){
		super();
	}

	public UserWithPermission(Collaborator collab){
		this(collab.getUser(), collab.getPermission());
	}
	
	public UserWithPermission(User user, Permission permission){
		super(user.getId(), user.getLogin(), user.isActive(), user.getIsAdmin());
		this.permission = permission;
	}
}