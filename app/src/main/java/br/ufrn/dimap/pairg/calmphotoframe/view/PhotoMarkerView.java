package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMarker;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class PhotoMarkerView {
	private PhotoMarker photoMark;
    private View view;
    private ImageView touchable;
    private TextView textView;

	public PhotoMarkerView(ViewGroup parent) {
        Context context = parent.getContext();
        view = LayoutInflater.from(context).inflate(R.layout.view_photomark, parent, false);
        view.setTag(this);
        touchable = ViewUtils.getView(view, R.id.photomark_touchable);
        touchable.setTag(this);
        textView = ViewUtils.getView(view, android.R.id.text1);
	}

    public Drawable getMarkerDrawable(){
        return touchable != null ? touchable.getDrawable() : null;
    }

	public PhotoMarker getPhotoMark() {
		return this.photoMark;
	}
	public void setPhotoMark(PhotoMarker mark) {
		this.update(mark);
	}
	
	/** update the view from the model 'PhotoMark'. 
	 * Should be called when the instance was changed out of the view.*/
	public void update(PhotoMarker mark) {
		this.photoMark = mark;
		if(this.photoMark != null){
			this.setText(String.valueOf(photoMark.getIndex()));
			//TODO: position view
		}
	}

	public void setText(CharSequence text) {
        textView.setText(text);
	}

	public View getView(){
		return view;
	}

	/** Position view based on the size of container.*/
	public void positionIn(View container){
		int markX = (int)(this.photoMark.getX() * (container.getWidth()));
		int markY = (int)(this.photoMark.getY() * container.getHeight());

		//CENTRALIZANDO VIEW
		markX -= getView().getWidth()/2;
        markY -= getView().getHeight()/2;
//        markY -= getView().getHeight();
		
		ViewCompat.setTranslationX(getView(), markX);
		ViewCompat.setTranslationY(getView(), markY);
	}
	
	/** Sincroniza a posição da PhotoMark associada a esta view em relação à view 'container'*/
	public void updatePhotoMarkLocationIn(View container) {
        float[] percentualLoc = getMarkerRelativePosition(getView(), container);
		this.photoMark.setRelativePosition(percentualLoc[0], percentualLoc[1]);
	}

    public static float[] getMarkerRelativePosition(View markerView, View container) {
        int centeredPos[] = ViewHelper.getViewCenterOnScreen(markerView);

        return ViewHelper.getPercentualLocationOnView(centeredPos, container);
    }

    public void setOnTouchListener(final View.OnTouchListener onTouchListener) {
        if(onTouchListener == null){
            touchable.setOnTouchListener(null);
        }
        else {
            touchable.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return onTouchListener.onTouch(getView(), event);
                }
            });
        }
    }

    public void setOnClickListener(final View.OnClickListener onClickListener) {
//        getView().setOnClickListener(onClickListener);
        if(onClickListener == null){
            touchable.setOnClickListener(null);
        }
        else {
            touchable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(getView());
                }
            });
        }
    }

    public ViewTreeObserver getViewTreeObserver() {
        return getView().getViewTreeObserver();
    }

    public static PhotoMarkerView getFromView(View v) {
        Object viewTag = v.getTag();
        if(viewTag instanceof PhotoMarkerView){
            return  (PhotoMarkerView) viewTag;
        }
        return null;
    }

    public static class BoundsController implements DragController.BoundsController {
        @Override
        public boolean isInside(View draggedView, View container) {
            float[] relativePos = PhotoMarkerView.getMarkerRelativePosition(draggedView, container);

            if(relativePos != null){
                float relX = relativePos[0];
                float relY = relativePos[1];

                return relX >= 0 && relX <= 1.0f &&
                       relY >= 0 && relY <= 1.0;
            }
            return false;
        }
    }
}
