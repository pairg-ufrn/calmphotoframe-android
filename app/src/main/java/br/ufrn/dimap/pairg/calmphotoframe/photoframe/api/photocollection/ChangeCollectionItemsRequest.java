package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import java.util.ArrayList;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.CollectionsOperation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.CollectionsOperation.Operation;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ChangeCollectionItemsRequest extends BaseGenericRequest<PhotoCollectionsContainer, Collection<PhotoCollection>> {

	private Collection<PhotoComponent> photoItems;
	private Collection<PhotoCollection> targets;
	protected Operation changeOperation;

	public ChangeCollectionItemsRequest(ApiConnector apiConnector
			, Collection<? extends PhotoCollection> targetCollections
			, Collection<PhotoComponent> items
			, Operation changeOperation
			, ResultListener<? super Collection<PhotoCollection>> listener)
	{
		super(apiConnector, PhotoCollectionsContainer.class, listener);
		this.photoItems = new ArrayList<>(items);
		this.targets = new ArrayList<>(targetCollections);
		this.changeOperation = changeOperation;
	}

	@Override
	public RequestHandler execute() {
		return postJsonRequest(new CollectionsOperation(changeOperation, photoItems, targets));
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().bulkCollectionsOpUrl();
	}

	@Override
	protected Collection<PhotoCollection> convertToResultType(PhotoCollectionsContainer response) {
		return response.collections;
	}
}