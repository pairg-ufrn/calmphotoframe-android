package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.UrlConnectionDownloader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import br.ufrn.dimap.pairg.calmphotoframe.PhotoFrameApplication;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;

public class PicassoImageGetter implements ImageGetter {
    private CookieEnabledUrlDownloader downloader;
    private Picasso picasso;

    private Set<RequestHandler> activeRequests;

    public PicassoImageGetter(CookieStore cookieStore){
        this.downloader = new CookieEnabledUrlDownloader(cookieStore);

        activeRequests = new HashSet<>();
    }

    @Override
    public RequestHandler getImage(final String url, final ResultListener<? super Bitmap> listener) {
        Logging.d(getClass().getSimpleName(), "getImage: url=" +url + "; listener="+listener+";");

        final PicassoTargetWrapper target = new PicassoTargetWrapper(url);

        Picasso picasso = getPicasso();
        final RequestHandler handler = new PicassoRequestHandler(picasso, target);
        activeRequests.add(handler);

        target.listener(Listeners.join(new RemoveHandlerOnFinish(handler), listener));

        picasso.load(url)
                //does not use default disk cache, because it does not provide clean controls (ex.: invalidation)
                //TODO: (maybe) implement disk cache using Picasso Cache (see Picasso.Builder)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .into(target);

        return handler;
    }

    @Override
    public void invalidateImageCache(String url) {
        getPicasso().invalidate(url);
    }

    protected Picasso getPicasso() {
        if(picasso == null){
            picasso = new Picasso.Builder(getContext())
                    .loggingEnabled(true)
                    .listener(new Picasso.Listener() {
                        @Override
                        public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                            Log.d(PicassoImageGetter.class.getSimpleName(), "Error in uri:" + uri);
                            exception.printStackTrace();
                        }
                    })
                    .downloader(downloader)
                    .build();
        }
        return picasso;
    }

    protected Context getContext(){
        return PhotoFrameApplication.getContext();
    }

    public static class PicassoTargetWrapper implements Target {
        private ResultListener<? super Bitmap> listener;
        private final String url;

        private AtomicBoolean _finished;

        public PicassoTargetWrapper(String url) {
            this(url, null);
        }

        public PicassoTargetWrapper(String url, ResultListener<? super Bitmap> listener) {
            this.listener = listener;
            this.url = url;

            _finished = new AtomicBoolean(false);
        }

        public PicassoTargetWrapper listener(ResultListener<? super Bitmap> listener){
            this.listener = listener;
            return this;
        }

        public boolean finished() {
            return _finished.get();
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Listeners.onResult(listener, bitmap);
            this._finished.set(true);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Listeners.onFailure(listener, new RuntimeException("Failed to load image: " + url));
            this._finished.set(true);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            Listeners.onStart(listener);
        }
    }

    public static class PicassoRequestHandler implements RequestHandler {
        private final PicassoTargetWrapper target;
        AtomicBoolean cancelled;
        Picasso picasso;

        public PicassoRequestHandler(Picasso picasso, PicassoTargetWrapper target) {
            this.target = target;
            cancelled = new AtomicBoolean(false);
            this.picasso = picasso;
        }

        @Override
        public boolean isFinished() {
            return target.finished();
        }

        @Override
        public boolean isCancelled() {
            return cancelled.get();
        }

        @Override
        public boolean cancel() {
            if(!isCancelled() && !isFinished()) {
                picasso.cancelRequest(target);
                cancelled.set(true);
                return true;
            }
            return false;
        }
    }

    private class CookieEnabledUrlDownloader extends UrlConnectionDownloader {
        private CookieStore cookieStore;

        public CookieEnabledUrlDownloader(CookieStore cookieStore) {
            super(PicassoImageGetter.this.getContext());

            this.cookieStore = cookieStore;
        }

        @Override
        protected HttpURLConnection openConnection(Uri path) throws IOException {

            HttpURLConnection connection = super.openConnection(path);

            List<Cookie> cookies = filterCookies(path);

            String cookiesValue = buildCookiesValue(cookies);

            connection.setRequestProperty("Cookie", cookiesValue);
            return connection;
        }

        private String buildCookiesValue(List<Cookie> cookies) {
            StringBuilder stringBuilder = new StringBuilder();
            for(Cookie c : cookies){
                stringBuilder
                        .append(c.getName())
                        .append("=")
                        .append(c.getValue())
                        .append(";");
            }

            return stringBuilder.toString();
        }

        private List<Cookie> filterCookies(Uri path) {
            List<Cookie> cookies = new ArrayList<>();

            for(Cookie cookie : cookieStore.getCookies()){
                if(cookieMatch(path, cookie)){
                    cookies.add(cookie);
                }
            }
            return cookies;
        }

        private boolean cookieMatch(Uri path, Cookie cookie) {
            //Warning: this is not a complete or truly correct implementation of cookie policies, just a workaround.

            Date now = new Date();
            return !cookie.isExpired(now)
                    && (path.getHost() != null && path.getHost().endsWith(cookie.getDomain()))
                    && (cookie.getPath() == null || path.getPath().startsWith(cookie.getPath()));
        }
    }

    private class RemoveHandlerOnFinish extends BasicResultListener<Bitmap> {
        private final RequestHandler handler;

        public RemoveHandlerOnFinish(RequestHandler handler) {
            this.handler = handler;
        }

        @Override
        protected void onFinish() {
            activeRequests.remove(handler);
        }
    }
}
