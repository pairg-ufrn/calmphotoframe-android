package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.DialogBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;

public class RemoveSelectedUserCommand extends ActivityStatelessCommand<User>{

	private static final String DIALOG_TAG = RemoveSelectedUserCommand.class.getName() + ".DIALOG";
	
	private ItemSelector<User> userSelector;
	
	public RemoveSelectedUserCommand(BaseActivity activity, ItemSelector<User> userSelector) {
		super(activity);
		this.userSelector = userSelector;
	}

	@Override
	public void execute(ResultListener<? super User> cmdResultListener) {
		User userToRemove = userSelector.getSelectedItem();
		buildDialog(userToRemove, cmdResultListener)
			.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
	}

	SimpleDialogFragment buildDialog(final User user, final ResultListener<? super User> listener){
		return DialogBuilder.buildRemoveDialog(getContext(), R.string.dialog_removeuser_title)
							.setDialogResponseListener(new ExecuteRemoveOnResponse(listener, user));
	}
	
	private void executeRemove(User userToRemove, ResultListener<? super User> listener) {
		if(userToRemove != null){
			ApiClient.instance().users().remove(userToRemove.getId(), listener);
		}
	}

	public class ExecuteRemoveOnResponse implements DialogResponseListener {
		private final ResultListener<? super User> listener;
		private final User user;

		public ExecuteRemoveOnResponse(ResultListener<? super User> listener, User user) {
			this.listener = listener;
			this.user = user;
		}

		@Override
		public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
			if(positive){
				executeRemove(user, listener);
			}
			return true;
		}
	}
}
