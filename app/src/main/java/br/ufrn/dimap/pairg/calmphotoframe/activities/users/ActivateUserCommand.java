package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.support.annotation.Nullable;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.UserActivationRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.AccountViewBinder;

/**
 * Command that can activate or deactivate an user account.
 */
public class ActivateUserCommand extends BaseArgsCommand<AccountViewBinder.UserActivationParam, User> {
    @Override
    public void execute(@Nullable AccountViewBinder.UserActivationParam userActivationParam, ResultListener<? super User> listener) {
        final User user = (userActivationParam != null) ? userActivationParam.getUser() : null;

        if (user == null) {
            return;
        }

        ApiClient.instance().users().changeActiveState(userActivationParam.getUserId(), userActivationParam.isActive(),
                new ConvertToUserDelegator(listener, user));
    }

    private static class ConvertToUserDelegator extends ResultListenerDelegator<UserActivationRequest.ActiveState, User> {
        private final User user;

        public ConvertToUserDelegator(ResultListener<? super User> listener, User user) {
            super(listener);
            this.user = user;
        }

        @Override
        protected User convertResult(UserActivationRequest.ActiveState result) {
            User userResult = user.clone();
            userResult.setActive(result.active);

            return userResult;
        }

        @Override
        public void onFailure(Throwable error) {
            Logging.d(getClass().getSimpleName(), "onFailure: " + error);
            super.onFailure(error);
        }
    }
}
