package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.view.CachedView;

public class BaseActivity extends AppCompatActivity implements UserNotifier {

    public interface PermissionRequestListener{
        void onRequestPermissionsResult(String[] permissions, int[] grantResults);
    }

	private static final int MAX_REQUEST_CODE = (1 << 16) -1;
	private static final int MIN_REQUEST_CODE = MAX_REQUEST_CODE/4;
	
	private final Map<Integer, OnActivityResultListener> activityResultListeners;
    private final Map<Integer, List<PermissionRequestListener>> permissionListeners;
	private final AtomicInteger requestCodeCounter;
	private boolean active;
	private boolean paused;

	protected CachedView<View> snackbarViewCache;

	protected CommandExecutor menuCommandExecutor;
	
	@SuppressLint("UseSparseArrays")
	public BaseActivity() {
		activityResultListeners = new HashMap<>();
        permissionListeners = new HashMap<>();
		requestCodeCounter = new AtomicInteger(MAX_REQUEST_CODE -1);
		
		menuCommandExecutor = new CommandExecutor();
		
		active = false;
		paused = true;
	}
	
	public boolean isActive() {
		return active;
	}
	protected void setActive(boolean active){
		this.active = active;
	}

	public boolean isPaused() {
		return paused;
	}
	protected void setPaused(boolean paused) {
		this.paused = paused;
	}

	@Override
	protected void onStart() {
		super.onStart();
		setActive(true);
	}
	@Override
	protected void onStop() {
		super.onStop();
		setActive(false);
	}
	@Override
	protected void onResume() {
		super.onResume();
		setPaused(false);
	}
	@Override
	protected void onPause() {
		super.onPause();
		setPaused(true);
	}
	
	public void startActivityForResult(Intent intent, OnActivityResultListener onActivityResponseListener) {
		int requestCode = generateRequestCode();
		putActivityResponseListener(requestCode, onActivityResponseListener);
		startActivityForResult(intent, requestCode);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		OnActivityResultListener responseListener = activityResultListeners.get(requestCode);
		if(responseListener != null){
			responseListener.onActivityResult(resultCode, intent);
		}
	}

	public void listenToPermissionRequest(int requestId, PermissionRequestListener permissionRequestListener) {
	    List<PermissionRequestListener> listeners = permissionListeners.get(requestId);

        if(listeners == null){
            listeners = new LinkedList<>();
        }

        listeners.add(permissionRequestListener);
        permissionListeners.put(requestId, listeners);
    }

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        List<PermissionRequestListener> listeners = getPermissionListeners(requestCode);
        if(listeners != null){
            for(PermissionRequestListener l : listeners){
                if(l != null){
                    l.onRequestPermissionsResult(permissions, grantResults);
                }
            }

            permissionListeners.remove(requestCode);
        }
	}

    public List<PermissionRequestListener> getPermissionListeners(int requestId) {
        return permissionListeners.get(requestId);
    }

	public int generateRequestCode() {
		requestCodeCounter.compareAndSet(MAX_REQUEST_CODE, MIN_REQUEST_CODE);
		return requestCodeCounter.getAndIncrement();
	}
	private void putActivityResponseListener(int requestCode, OnActivityResultListener onActivityResponseListener) {
		this.activityResultListeners.put(requestCode, onActivityResponseListener);
	}

    @Override
    public NotificationBuilder notifyUser(){
        return new NotificationBuilder(this).anchorView(getSnackbarView());
    }
    @Override
    public NotificationBuilder notifyUser(CharSequence text){
        return notifyUser().text(text);
    }

	@Nullable
	@Override
	public CharSequence getTextResouce(int msgId) {
		return getText(msgId);
	}

	@Deprecated
	public void showUserMessage(int messageId) {
		notifyUser(getResources().getText(messageId)).show();
	}

	@Deprecated
	public void showUserMessage(CharSequence message, CharSequence actionText, final Runnable actionCallback) {
		notifyUser(message).action(actionText, actionCallback).show();
	}

	@Deprecated
	public void notifyError(Throwable error) {
		notifyError("Error:\n" + error);
	}
	@Deprecated
	public void notifyError(CharSequence errMessage) {
		notifyUser(errMessage).show();
	}
    @Deprecated
	public void notifyError(int stringId) {
		notifyError(getResources().getString(stringId));
	}
    @Deprecated
	public void notifyError(CharSequence msg, Throwable cause) {
		notifyError(msg + "\nCause: " + cause);
	}
    @Deprecated
	public void notifyLocalizedError(Throwable cause, int fallbackMessage){
		String errorMsg;
		if(cause !=null && cause.getLocalizedMessage() != null){
			errorMsg = cause.getLocalizedMessage();
		}
		else{
			errorMsg = getResources().getString(fallbackMessage);
		}
		notifyError(errorMsg);
	}

	protected View getSnackbarView(){
		return snackbarViewCache == null ? null : snackbarViewCache.getView(this);
	}

    protected void setSnackbarViewId(Integer viewId){
        this.snackbarViewCache = viewId == null ? null : new CachedView<>(viewId);
    }

	protected void setupToolbar(){
        setupToolbar( R.id.toolbar);
	}

    private void setupToolbar(int toolbarId) {
        Toolbar toolbar = (Toolbar) findViewById(toolbarId);
        setSupportActionBar(toolbar);
        configureToolbar(toolbar);
    }

    protected void configureToolbar(Toolbar toolbar){
		//Override on subclasses
	}


	/* ********************************** Menus ******************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		int menuId = getMenuId();
		
		if(menuId == 0){
			return false;
		}
		
		getMenuInflater().inflate(menuId, menu);
		
		createMenuCommands(menu, menuCommandExecutor);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(executeCommand(item.getItemId())){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean executeCommand(Object cmdId){
		return menuCommandExecutor.execute(cmdId);
	}

	protected void createMenuCommands(Menu menu, CommandExecutor menuCommandExecutor) {
		// Override on subclasses
		this.createMenuCommands(menuCommandExecutor);
	}
	protected void createMenuCommands(CommandExecutor menuCommandExecutor) {
		// Override on subclasses
	}
	
	protected int getMenuId(){
		return 0;
	}
}
