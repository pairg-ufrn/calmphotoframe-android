package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class AccountViewBinder extends InflaterViewBinder<User> implements OnCheckedChangeListener {

    public static class UserActivationParam{
        private User user;
        private boolean active;

        public UserActivationParam(User user, boolean active) {
            this.user = user;
            this.active = active;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public Long getUserId() {
            return user == null ? null : user.getId();
        }
    }

    private UserViewBinder userViewBinder;

    private View rootView;
    private CompoundButton isActiveBtn;
    private ArgsCommand<UserActivationParam, User> activateUserCommand;

    private User boundUser;

    public AccountViewBinder() {
		super(R.layout.view_user_account);
        userViewBinder = new UserViewBinder();
	}


    public User getBoundUser() {
        return boundUser;
    }

    public void setImageViewBinder(UserImageViewBinder imageViewBinder) {
        userViewBinder.setImageViewBinder(imageViewBinder);
    }
    public void setActivateUserCommand(ArgsCommand<UserActivationParam, User> activateUserCommand) {
        this.activateUserCommand = activateUserCommand;
    }

    public ArgsCommand<UserActivationParam, User> getActivateUserCommand() {
        return activateUserCommand;
    }


	@Override
	public void setup(View view) {
        userViewBinder.setup(view);

        this.rootView = view;
        this.isActiveBtn = ViewUtils.getView(view, R.id.view_user_isactive);
        isActiveBtn.setOnCheckedChangeListener(this);
	}

	@Override
    public void bind(User user) {
        bindToView(user);
        this.boundUser = user;
    }

    public void bindToView(User user) {
        ViewCompat.setAlpha(this.rootView, !user.isActive() ? 0.4f : 1f);

//        setStrikeThrough(this.userViewBinder.getUserNameView(), user);

        isActiveBtn.setChecked(user.isActive());
        isActiveBtn.setContentDescription(getActiveControlContentDescription(this.isActiveBtn.getContext(), user));

        userViewBinder.bind(user);
    }

    public void rebindView() {
        bindToView(getBoundUser());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Logging.i(getClass().getSimpleName(), "OnCheckedChanged to: " + isChecked);

        if(getBoundUser() != null
                && getBoundUser().isActive() != isChecked
                && getActivateUserCommand() != null)
        {
            User changedUser = getBoundUser().clone();
            changedUser.setActive(isChecked);
            bindToView(changedUser);

            getActivateUserCommand().execute(new UserActivationParam(getBoundUser(), isChecked),
                    new UpdateViewOnChangeActivation());
        }
    }

	private String getActiveControlContentDescription(Context context, User user) {
		int strId = user.isActive() ? R.string.action_disableAccount : R.string.action_enableAccount;

		return context.getString(strId);
	}

	private void setStrikeThrough(TextView txtView, User user) {
		int flags = txtView.getPaintFlags();
		if(user.isActive()){
			flags &= ~Paint.STRIKE_THRU_TEXT_FLAG; 
		}
		else{
			flags |= Paint.STRIKE_THRU_TEXT_FLAG;
		}
		txtView.setPaintFlags(flags);
	}

    private class UpdateViewOnChangeActivation extends BasicResultListener<User> {
        @Override
        public void onResult(User result) {
            if(result != null){
                User currentUser = getBoundUser();
                currentUser.setActive(result.isActive());
                bind(currentUser);
            }
        }

        @Override
        public void onFailure(Throwable error) {
            rebindView();
        }
    }
}
