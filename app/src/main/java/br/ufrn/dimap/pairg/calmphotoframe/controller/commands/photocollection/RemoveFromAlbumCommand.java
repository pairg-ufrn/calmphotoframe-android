package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;
import java.util.Collections;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.SelectionDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleMaterialDialogFragment;

public class RemoveFromAlbumCommand
		extends BasePhotoCollectionCommand<Collection<PhotoCollection>>
{
	private static final String DIALOG_TAG = RemoveFromAlbumCommand.class + ".Dialog";
	
	private boolean multipleTargets;
	private PhotoCollection targetCollection;
	
	public RemoveFromAlbumCommand(BaseActivity activity, PhotoComponentSelector selector) {
		super(activity, selector);
		multipleTargets = false;
	}
	
	public boolean isMultipleTargets() {
		return multipleTargets;
	}
	public void setMultipleTargets(boolean multipleTargets) {
		this.multipleTargets = multipleTargets;
	}

	@Override
	public void execute(ResultListener<? super Collection<PhotoCollection>> listener) {
		if(multipleTargets){
			super.execute(listener);
			return;
		}
		
		this.targetCollection = getSelector().getCurrentPhotoCollection();
		super.setSelectedItems(getSelector().getSelectedItems());
		
		SimpleMaterialDialogFragment confirmDialog = new SimpleMaterialDialogFragment();
		confirmDialog.setDialogResponseListener(new OnConfirmDialogRemoveItem(listener))
            .content()
                .setDialogMessage(getString(R.string.dialog_removefromCurrentAlbumItems))
                .setOkMessage(getString(R.string.generic_remove_action))
                .setCancelMessage(getString(R.string.generic_cancel));
		confirmDialog.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
	}

	class OnConfirmDialogRemoveItem implements DialogResponseListener, SimpleMaterialDialogFragment.DialogResponseListener {
		ResultListener<? super Collection<PhotoCollection>> listener;

		public OnConfirmDialogRemoveItem(ResultListener<? super Collection<PhotoCollection>> listener) {
			this.listener = listener;
		}

		@Override
		public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
			if(positive){
				executeItemsCommand(Collections.singleton(targetCollection), getSelectedItems(), listener);
			}

			return true;
		}

        @Override
        public boolean onResponse(SimpleMaterialDialogFragment dialog, boolean positive) {
            if(positive){
                executeItemsCommand(Collections.singleton(targetCollection), getSelectedItems(), listener);
            }

            return true;
        }
    }
	
	@Override
	protected void customizeSelectionDialog(SelectionDialog<PhotoCollection> selectAlbumDialog) {
		selectAlbumDialog.setDialogTitle  (getString(R.string.dialog_removePhotosFromAlbum_title));
		selectAlbumDialog.setDialogMessage(getString(R.string.dialog_removePhotosFromAlbum_message));
	}
	
	@Override
	protected void executeItemsCommand(
			Collection<? extends PhotoCollection> selectedContexts,
			Collection<PhotoComponent> selectedItems,
			ResultListener<? super Collection<PhotoCollection>> listener)
	{
		ApiClient.instance().collections()
					.removeItems(
								selectedContexts,
								selectedItems,
								listener);
	}
}
