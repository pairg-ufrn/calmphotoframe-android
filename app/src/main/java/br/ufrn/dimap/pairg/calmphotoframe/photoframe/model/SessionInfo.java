package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public class SessionInfo {
	private String token;
	private User user;
	
	public SessionInfo() {
		token = "";
		user = null;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String sessionToken) {
		this.token = sessionToken;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User sessionUser) {
		this.user = sessionUser;
	}

	public boolean isAdmin() {
		return getUser() != null && getUser().getIsAdmin();
	}

	public void set(SessionInfo session) {
		if(session != null){
			this.token = session.getToken();
			this.user = (User) (session.getUser() == null ? null : session.getUser().clone());
		}
	}

    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof SessionInfo)){
            return false;
        }

        if(this == o){ //same instance
            return true;
        }

        SessionInfo other = (SessionInfo) o;
        return checkEquals(this.getToken(), other.getToken());
    }

    private boolean checkEquals(Object first, Object second) {
        return (first == null && second == null)
                || (first != null && first.equals(second));
    }
}
