package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.validators.LoginValidator;

public class CreateAccountFragment extends DialogFragment implements OnEditorActionListener{
	public static final String CLASS = CreateAccountFragment.class.getName();
	public static final String ARGS_ENABLE_REMOTE 	  = CLASS + ".ENABLE_REMOTE";
	public static final String ARGS_ENABLE_SWITCHMODE = CLASS + ".ENABLE_SWITCHMODE";

	private ResultListener<? super ConnectionInfo> listener;
	private boolean closeOnSubmit;

    private TextView titleView;
	private EditText loginEditor, passwordEditor, confirmPassEditor;
    private Button confirmButton;
	private boolean enableRemotePhotoFrame;
	private boolean enableSwitchViewMode;
    private boolean clearOnClose;
	
	protected final LoginValidator loginValidator;
    private Button cancelButton;

    public static CreateAccountFragment build(boolean enableRemotePhotoFrame, boolean enableSwitchViewModes){
        CreateAccountFragment fragment = new CreateAccountFragment();
        fragment.enableRemotePhotoFrame = enableRemotePhotoFrame;
        fragment.enableSwitchViewMode = enableSwitchViewModes;

        fragment.saveState();

        return fragment;
    }

	public CreateAccountFragment() {
		closeOnSubmit = false;
		this.enableRemotePhotoFrame = false;
		this.enableSwitchViewMode = false;

		loginValidator = new LoginValidator(R.id.login_loginContainer, R.id.login_passwordContainer, R.id.login_confirmPasswordContainer);
		loginValidator.setLoginMode(false);
	}

    private void saveState() {
        Bundle args = new Bundle();
        args.putBoolean(ARGS_ENABLE_REMOTE, enableRemotePhotoFrame);
        args.putBoolean(ARGS_ENABLE_SWITCHMODE, this.enableSwitchViewMode);

        this.setArguments(args);
    }

    /** Clears the content and errors.*/
    public void clear() {
        loginValidator.clearErrors();

        clearText(getLoginEditor());
        clearText(getPasswordEditor());
        clearText(getConfirmPassEditor());
    }

    protected void clearText(EditText editor) {
        if(editor != null){
            editor.setText("");
        }
    }
	
	/* ********************************** Getters e Setters ******************************/
	
	public ResultListener<? super ConnectionInfo> getLoginListener(){
		return listener;
	}
	public void setLoginListener(ResultListener<? super ConnectionInfo> listener){
		this.listener = listener;
	}

	public boolean getCloseOnSubmit() {
		return closeOnSubmit;
	}
	public void setCloseOnSubmit(boolean closeOnSubmit) {
		this.closeOnSubmit = closeOnSubmit;
	}

    public boolean isClearOnClose() {
        return clearOnClose;
    }

    public void setClearOnClose(boolean clearOnClose) {
        this.clearOnClose = clearOnClose;
    }

    public EditText getLoginEditor(){
        return this.loginEditor;
    }

    public EditText getPasswordEditor() {
        return passwordEditor;
    }

    public EditText getConfirmPassEditor() {
        return confirmPassEditor;
    }

    public Button getConfirmButton(){
        return confirmButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

	/* ********************************** LifeCycle **************************************/
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        restoreState();
	}

    private void restoreState() {
        Bundle args = getArguments();
        if(args != null){
            this.enableRemotePhotoFrame = args.getBoolean(ARGS_ENABLE_REMOTE, isRemotePhotoFrameEnabled());
            this.enableSwitchViewMode   = args.getBoolean(ARGS_ENABLE_SWITCHMODE, this.enableSwitchViewMode);
        }
    }

    @Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);

        //no title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_createaccount, container, false);

        findViews(rootView);
        configureViews(rootView);
		
		return rootView;
	}

	/* ***********************************************************************************/

    protected void findViews(View rootView) {
        titleView           = ViewUtils.getView(rootView, R.id.createAccountFragment_title);
        loginEditor         = ViewUtils.getView(rootView, R.id.login_loginInput);
        passwordEditor      = ViewUtils.getView(rootView, R.id.login_passwordInput);
        confirmPassEditor   = ViewUtils.getView(rootView, R.id.login_confirmPassword);
        confirmButton       = ViewUtils.getView(rootView, R.id.loginFragment_actionbutton);
        cancelButton        = ViewUtils.getView(rootView, R.id.loginInfo_cancel);
    }

    protected void configureViews(View rootView) {
        titleView.setText(getTitleId());

        confirmButton.setText(getButtonTextResource());
        confirmButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });
        confirmPassEditor.setOnEditorActionListener(this);

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doCancel();
            }
        });

        loginValidator.bindViews(rootView);
    }

    protected void doCancel() {
        Dialog dialog = getDialog();
        dialog.cancel();
        dialog.dismiss();
    }

    protected int getButtonTextResource() {
		return R.string.action_signup;
	}
	
	protected int getTitleId() {
		return R.string.loginFragment_createAccountTitle;
	}
	

	/* ***********************************************************************************/
	
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if(actionId == EditorInfo.IME_ACTION_DONE 
				|| (event != null && event.getAction() != KeyEvent.ACTION_DOWN))
		{
			onSubmit();
		}
		
		return true;
	}
	
	/** Called when signup or login is made*/
	protected void onSubmit() {
		LoginInfo loginInfo = getLoginInfo();
		if(verifyLogin(loginInfo)){
			Listeners.onResult(listener, extractConnectInfo(loginInfo));
			
			if(getCloseOnSubmit() && getShowsDialog()){
				this.dismiss();
			}
		}
	}

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if(!getCloseOnSubmit()){
            return;
        }

        if(getLoginEditor() != null){
            getLoginEditor().setText("");
        }
        if(getPasswordEditor() != null){
            getPasswordEditor().setText("");
        }
        if(getConfirmPassEditor() != null){
            getConfirmPassEditor().setText("");
        }
    }

    protected ConnectionInfo extractConnectInfo(LoginInfo loginInfo) {
		ConnectionInfo info = new ConnectionInfo();
		info.setLoginInfo(loginInfo);
		
		return info;
	}
	protected boolean isRemotePhotoFrameEnabled() {
		return enableRemotePhotoFrame;
	}

	protected LoginInfo getLoginInfo() {
		return new LoginInfo(loginEditor.getText().toString()
							, passwordEditor.getText().toString());
	}

	protected boolean verifyLogin(LoginInfo loginInfo) {
		return loginValidator.validate();
	}
}
