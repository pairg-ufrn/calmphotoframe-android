package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.FragmentBuilderPageAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.LoginSignupFragment;

/**
 */
public class LoginPagerAdapter extends FragmentBuilderPageAdapter {
    private int loginIndex, signupIndex;

	public LoginPagerAdapter(Activity activity, FragmentManager fm) {
        super(fm);

		loginIndex = this.addFragment(activity.getText(R.string.login_signin_title), new LoginSignupBuilder(true));
		signupIndex = this.addFragment(activity.getText(R.string.login_signup_title), new LoginSignupBuilder(false));
    }

    public LoginSignupFragment getSignupFragment() {
        return (LoginSignupFragment) getFragment(signupIndex);
    }

    public LoginSignupFragment getLoginFragment() {
        return (LoginSignupFragment) getFragment(loginIndex);
    }

	class LoginSignupBuilder implements FragmentCreator.FragmentBuilder{
		private boolean isLogin;

		public LoginSignupBuilder(boolean isLogin) {
			this.isLogin = isLogin;
		}

		@Override
		public Fragment buildFragment() {
			return LoginSignupFragment.build(isLogin, false);
		}
	}
}
