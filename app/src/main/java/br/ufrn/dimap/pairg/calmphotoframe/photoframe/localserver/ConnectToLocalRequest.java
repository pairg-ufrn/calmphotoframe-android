package br.ufrn.dimap.pairg.calmphotoframe.photoframe.localserver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseApiRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.PhotoFrameControllerService;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameBroadcastReceiver;;

/**
 */
public class ConnectToLocalRequest extends BaseApiRequest {
    private Context context;
    private ResultListener<? super ApiEndpoint> listener;
    private LoginInfo loginInfo;
    private PhotoFrameBroadcastReceiver receiver;

    public ConnectToLocalRequest(ApiConnector apiConnector, LoginInfo loginInfo, Context context)
    {
        super(apiConnector);
        this.context = context;
        this.loginInfo = loginInfo;
        receiver = new PhotoFrameBroadcastReceiver();
    }


    @Override
    public RequestHandler execute() {
        try {
            //Start local server if not already running
            start();
        }catch (Throwable ex){
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    protected String genUrl() {
        return "";
    }

    protected void start() {
        this.start(new Bundle());
    }

    protected void start(Bundle arguments) {
        receiver.setServerListener(buildServerListener());
        receiver.register(context);

        context.startService(buildStartServiceIntent(arguments));
    }

    @NonNull
    private Intent buildStartServiceIntent(Bundle arguments) {
        Intent intent = new Intent(context, PhotoFrameControllerService.class);
        intent.setAction(PhotoFrameControllerService.START_SERVER_ACTION);
        if (arguments != null && !arguments.isEmpty()) {
            intent.putExtras(arguments);
        }
        return intent;
    }

    private ServerListenerAdapter buildServerListener() {
        ResultListener<ServerStartEvent> compositeListener = new CompositeResultListener<>(
                new OnResultDoLogin(loginInfo, listener),
                new OnResultUnregisterReceiver(receiver, context));

        return new ServerListenerAdapter().onConnect(compositeListener);
    }

    private static class OnResultUnregisterReceiver extends BasicResultListener<Object> {
        final PhotoFrameBroadcastReceiver receiver;
        Context context;

        public OnResultUnregisterReceiver(PhotoFrameBroadcastReceiver receiver, Context context) {
            this.receiver = receiver;
            this.context = context;
        }

        @Override
        public void onResult(Object result) {
            this.receiver.unregister(context);
        }

        @Override
        public void onFailure(Throwable error) {
            this.receiver.unregister(context);
        }
    }

    private class OnResultDoLogin extends BasicResultListener<ServerStartEvent> {
        LoginInfo loginInfo;
        ResultListener<? super ApiEndpoint> listener;

        public OnResultDoLogin(LoginInfo loginInfo, ResultListener<? super ApiEndpoint> listener) {
            this.listener = listener;
            this.loginInfo = loginInfo;
        }

        @Override
        public void onResult(ServerStartEvent result) {

//            PhotoFrameClient.instance().connect(new ConnectionInfo(result, loginInfo), listener);
        }
    }
}
