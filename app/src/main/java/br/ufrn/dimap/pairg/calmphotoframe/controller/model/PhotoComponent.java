package br.ufrn.dimap.pairg.calmphotoframe.controller.model;

import java.util.Collection;
import java.util.HashSet;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class PhotoComponent {
	private Photo photo;
	private PhotoCollection photoCollection;
	
	public PhotoComponent(Photo photo) {
		this(photo, null);
	}
	public PhotoComponent(PhotoCollection photoCollection) {
		this(null, photoCollection);
	}
	protected PhotoComponent(Photo photo, PhotoCollection photoCollection){
		this.photo = photo;
		this.photoCollection = photoCollection;
	}

	public long getId() {
		return isCollection() ? getPhotoCollection().getId() : getPhoto().getId();
	}
	
	public boolean isCollection(){
		return photoCollection != null;
	}
	public boolean isPhoto() {
		return !isCollection();
	}
	public Photo getPhoto(){
		return photo;
	}
	public PhotoCollection getPhotoCollection(){
		return photoCollection;
	}
	
	public static Collection<PhotoCollection> filterPhotoCollections(Collection<PhotoComponent> items) {
		Collection<PhotoCollection> contexts = new HashSet<>();
		for(PhotoComponent component : items){
			if(component.isCollection()){
				contexts.add(component.getPhotoCollection());
			}
		}
		return contexts;
	}
	public static Collection<Photo> filterPhotos(Collection<PhotoComponent> items) {
		Collection<Photo> photos = new HashSet<>();
		for(PhotoComponent component : items){
			if(component.isPhoto()){
				photos.add(component.getPhoto());
			}
		}
		return photos;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PhotoComponent that = (PhotoComponent) o;

		if(getPhoto() != null){
			return that.getPhoto() != null && getPhoto().equals(that.getPhoto());
		}
		if(getPhotoCollection() != null){
			return that.getPhotoCollection() != null && getPhotoCollection().equals(that.getPhotoCollection());
		}

		return that.getPhoto() == null && that.getPhotoCollection() == null;

	}

	@Override
	public int hashCode() {
		int result = getPhoto() != null ? getPhoto().hashCode() : 0;
		result = 31 * result + (getPhotoCollection() != null ? getPhotoCollection().hashCode() : 0);
		return result;
	}
}
