package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DragTouchListener implements OnTouchListener {

    private final DragController dragController;
    private final View containerView;
    private final GestureDetectorCompat gestureDetector;
    private GestureListenerDetector gestureListener;
    private View.OnClickListener onClickListener;

    public DragTouchListener(Context context, DragController controller, View container) {
        this.dragController = controller;
        this.containerView = container;
        gestureListener = new GestureListenerDetector();
        gestureDetector = new GestureDetectorCompat(context, gestureListener);
    }

    @SuppressWarnings("unused")
    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.onClickListener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        gestureListener.setView(v);

        if (event.getAction() == MotionEvent.ACTION_UP) {
            //Handle first the detector gesture
            gestureDetector.onTouchEvent(event);

            if (dragController.isDragging() /*&& dragController.getDraggedView() == v*/) {
                stopDragging();
            }
            return true;
        }

        gestureDetector.onTouchEvent(event);

        return true;
    }

    protected void stopDragging() {
        gestureListener.setView(null);
        this.dragController.stopDrag();
    }

    private class GestureListenerDetector extends GestureDetector.SimpleOnGestureListener {
        View view;

        public View getView() {
            return view;
        }

        public void setView(View view) {
            this.view = view;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            boolean handled = false;

            if (onClickListener != null) {
                onClickListener.onClick(getView());
                handled = true;
            }
            if (dragController.isDragging()) {
                stopDragging();
                handled = true;
            }

            return handled;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (e1 == null) {
                e1 = e2;

                if (e2 == null) {
                    return false;
                }
            }

            if (!dragController.isDragging()) {
                dragController.startDrag(view, containerView, e1.getRawX(), e1.getRawY());
            }

            dragController.drag(e2.getRawX(), e2.getRawY());

            return true;
        }
    }
}