package br.ufrn.dimap.pairg.calmphotoframe.view.fragments;

import android.view.View;
import android.widget.FrameLayout;

import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu.CloseFabMenuOnClickListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu.FabMenu;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.fabmenu.ShowOverlayOnMenuUpdate;

/**
 */
public class PhotoAlbumFabMenu extends FabMenu {

    public void create(View rootView, int menuView, int overlayView) {
        final FrameLayout frameLayout = ViewUtils.getView(rootView, overlayView);
        frameLayout.setVisibility(View.INVISIBLE);

        super.create((FloatingActionsMenu) ViewUtils.getView(rootView, menuView));

        this.addItem(R.id.action_insertPhoto, R.drawable.ic_action_new_picture, R.string.action_insertPhoto);
        this.addItem(R.id.action_addAlbum, R.drawable.ic_image_album, R.string.action_addAlbum);

        this.addOnClickMenuItemListener(new CloseFabMenuOnClickListener(this));
        this.setOnUpdateMenuListener(new ShowOverlayOnMenuUpdate(frameLayout, this));
    }
}
