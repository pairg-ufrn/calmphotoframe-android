package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

public class FitInsideDimensionsCalculator extends ScaleDimensionsCalculator {

	@Override
	protected int choiceScaleDimension(int[] imageDims, int[] maxDims) {
		if(Math.abs(imageDims[1] - maxDims[1]) > Math.abs(imageDims[0] - maxDims[0]))
			return 1;
		else{
			return 0;
		}
	}

}
