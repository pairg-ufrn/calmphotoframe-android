package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.SessionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class UpdateSessionUserOnResult extends BasicResultListener<User> {
    private final SessionInfo session;

    public UpdateSessionUserOnResult(SessionInfo session) {
        this.session = session;
    }

    @Override
    public void onResult(User result) {
        session.setUser(result);
    }
}
