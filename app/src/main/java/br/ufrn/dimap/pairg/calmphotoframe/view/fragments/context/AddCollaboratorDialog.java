package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.afollestad.materialdialogs.internal.MDButton;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.AsyncGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PermissionLevel;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable.FilterableViewBuilderAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PermissionLevelAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable.CollectionFilter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilderWrapper;

public class AddCollaboratorDialog extends DialogFragment{

	private FilterableViewBuilderAdapter<User> usersAdapter;
	private ResultListener<? super Collaborator> onAddListener;
	private Long contextId;
	private AsyncGetter<Collection<User>> usersGetter;
	
	public AddCollaboratorDialog(Long contextId, AsyncGetter<Collection<User>> usersGetter) {
		usersAdapter = new FilterableViewBuilderAdapter<>(new ViewBuilderWrapper<User>(){
			@Override
			protected BinderViewHolder<User> buildBinder() {
				return new UserViewBinder();
			}
		});
		
		usersAdapter.setCollectionFilter(new CollectionFilter<User>() {
			@Override
			public List<User> filter(Collection<? extends User> items, CharSequence constraint) {
				String prefix = constraint.toString().toLowerCase();
				List<User> filtered = new LinkedList<>();
				
				for(User user : items){
					if(user.getLogin() != null){
						String login = user.getLogin().toLowerCase();
						if(login.startsWith(prefix)){
							filtered.add(user);
						}
					}
				}
				
				return filtered;
			}
		});
		this.contextId = contextId;
		this.usersGetter = usersGetter;
	}

	public ResultListener<? super Collaborator> getOnAddListener() {
		return onAddListener;
	}
	public void setOnAddListener(ResultListener<? super Collaborator> onAddListener) {
		this.onAddListener = onAddListener;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		usersGetter.getAsync(new BasicResultListener<Collection<User>>(){
			@Override
			public void onResult(Collection<User> result) {
				usersAdapter.setItems(result);
			}
		});
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
		builder.title(R.string.collaborator_add)
			   .customView(R.layout.layout_collaborator_add, true)
			   .positiveText(R.string.collaborator_add_positiveButton)
			   .negativeText(R.string.generic_cancel)
			   .onPositive(new SingleButtonCallback() {
				   @Override
				   public void onClick(MaterialDialog dialog, DialogAction action) {
					   //TODO: validate input

					   if (DialogAction.POSITIVE.equals(action)) {
						   AddCollaboratorViewHolder viewHolder =
								   (AddCollaboratorViewHolder) dialog.getCustomView().getTag();
						   Listeners.onResult(getOnAddListener(), getClass(), viewHolder.extract());
					   }
				   }
			   });
		
		MaterialDialog dialog = builder.build();
		View view  = dialog.getCustomView();
        view.setTag(new AddCollaboratorViewHolder(view));
        
        final MDButton positiveButton = dialog.getActionButton(DialogAction.POSITIVE);
        positiveButton.setEnabled(false);
        
        final AutoCompleteTextView input = ViewUtils.getView(view, R.id.collaborator_input);
//		input.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if(!input.isPopupShowing()){
//					input.showDropDown();
//				}
//			}
//		});
        input.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				List<User> filteredItems = usersAdapter.getFilteredItems();
				boolean valid = false;
				if(filteredItems != null && filteredItems.size() == 1){
					valid = (filteredItems.get(0).toString().equals(s.toString()));
				}
				positiveButton.setEnabled(valid);
			}
		});
        
		return dialog;
	}

	private class AddCollaboratorViewHolder{
		Spinner spinnerPermission;
		PermissionLevelAdapter permissionAdapter;
//		MaterialBetterSpinner collabInput;
		AutoCompleteTextView collabInput;

		public AddCollaboratorViewHolder(View view){
			permissionAdapter = new PermissionLevelAdapter();
			spinnerPermission = ViewUtils.getView(view, R.id.collaborator_permission);
			spinnerPermission.setAdapter(permissionAdapter);
			
			collabInput = ViewUtils.getView(view, R.id.collaborator_input);
	        collabInput.setAdapter(usersAdapter);
		}
		
		public Collaborator extract(){
			User currentUser = usersAdapter.findItem(new User(collabInput.getText().toString()), new Comparator<User>() {
				@Override
				public int compare(User lhs, User rhs) {
					return lhs.getLogin().compareTo(rhs.getLogin());
				}
			});
			
			PermissionLevel permissionLevel = permissionAdapter.getItem(spinnerPermission.getSelectedItemPosition());
			
			Collaborator collab = new Collaborator();
			collab.setUser(currentUser);
			collab.setContextId(contextId);
			collab.setPermissionLevel(permissionLevel);
			
			return collab;
		}
	}

}
