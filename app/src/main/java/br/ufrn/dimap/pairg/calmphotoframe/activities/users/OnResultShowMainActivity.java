package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import android.content.Context;
import android.content.Intent;
import br.ufrn.dimap.pairg.calmphotoframe.activities.MainActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;

public class OnResultShowMainActivity extends BasicResultListener<Object> {
	Context context;
	
	public OnResultShowMainActivity(Context context) {
		this.context = context;
	}
	
	@Override
	public void onResult(Object result) {
		Intent mainActivityIntent = new Intent(context, MainActivity.class);
		//If MainActivity is on stack, remove all activities after it
		mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(mainActivityIntent);
	}
}