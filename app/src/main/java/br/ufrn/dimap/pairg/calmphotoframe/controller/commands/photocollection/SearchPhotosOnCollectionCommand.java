package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import android.content.Context;
import android.os.Bundle;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoCollectionActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;

public class SearchPhotosOnCollectionCommand extends BaseStatelessCommand<Void>{
	private Context context;
	private PhotoContext currentPhotoContext;
	private PhotoCollection currentCollection;
	private String query;
	
	public SearchPhotosOnCollectionCommand(Context context, String query, PhotoCollection photoCollection, PhotoContext photoContext) {
		this.context = context;
		this.query = query;
		this.currentCollection = photoCollection;
		this.currentPhotoContext = photoContext;
	}
	
	@Override
	public void execute(ResultListener<? super Void> cmdResultListener) {
		Bundle args = new Bundle();
		args.putSerializable(Intents.Extras.PhotoCollection, currentCollection);
		args.putSerializable(Intents.Extras.PhotoContext, currentPhotoContext);
		args.putSerializable(Intents.Extras.Query, query);
		
		IntentUtils.startActivity(context, PhotoCollectionActivity.class, args);
	}
}
