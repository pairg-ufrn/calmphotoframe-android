package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderHolderWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;

/** FIXME: unify code (remove duplications) with {@link ItemsAdapter} and {@link ViewBuilderAdapter}*/
public abstract class ViewBuilderRecyclerAdapter<T> extends RecyclerView.Adapter<BinderHolderWrapper<T>> {

    public interface BinderDecorator<T> {
        void onBind(BinderHolderWrapper<T> holder, int pos, ViewBuilderRecyclerAdapter<T> adapter);
    }

    List<BinderDecorator> decorators;
    SparseArray<BinderHolderWrapper<T>> binderHolders;

    public ViewBuilderRecyclerAdapter() {
        this.decorators = new ArrayList<>();
        binderHolders = new SparseArray<>();
    }

    public void addDecorator(BinderDecorator<T> decorator) {
        decorators.add(decorator);
    }

    public void removeDecorator(BinderDecorator<T> decorator) {
        decorators.remove(decorator);
    }

    @Override
    public abstract int getItemCount();

    public abstract T getEntity(int pos);

    public abstract BinderViewHolder<T> buildViewBinder();

    @Override
    public BinderHolderWrapper<T> onCreateViewHolder(ViewGroup viewParent, int viewType) {
        return new BinderHolderWrapper<>(viewParent, viewType, buildViewBinder());
    }

    @Override
    public void onBindViewHolder(BinderHolderWrapper<T> holderWrapper, int pos) {
        doBind(holderWrapper, pos);
    }

    protected void doBind(BinderHolderWrapper<T> holderWrapper, int pos) {
        binderHolders.put(pos, holderWrapper);
        holderWrapper.bind(getEntity(pos));

        for (BinderDecorator<T> decorator : decorators) {
            decorator.onBind(holderWrapper, pos, this);
        }
    }

    public void rebind(int position) {
        if (position >= getItemCount() || position < 0) {
            throw new IndexOutOfBoundsException();
        }

        doBind(binderHolders.get(position), position);
    }
}
