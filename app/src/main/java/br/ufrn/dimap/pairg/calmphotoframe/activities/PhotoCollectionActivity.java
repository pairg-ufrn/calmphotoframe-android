package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.events.RecentEventsController;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection.PhotoCollectionController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;

public class PhotoCollectionActivity extends ConnectedActivity{
	
	private final PhotoCollectionController collectionController;
	
	private final RecentEventsController eventsController;
	
	public PhotoCollectionActivity() {
		collectionController = new PhotoCollectionController(this);
        eventsController = new RecentEventsController(this, R.id.drawer_layout_albums);
	}

	/* ************************************* LifeCycle ****************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_photo_collection);
        setSnackbarViewId(R.id.root_view);
		setupToolbar();
		
		setTitle("");
		
		collectionController.initialize();
		eventsController.initialize();
	}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		eventsController.onPostCreate();
	}

	@Override
	protected void configureToolbar(Toolbar toolbar) {
		super.configureToolbar(toolbar);
		toolbar.setLogo(R.drawable.ic_image_album);
	}

	@Override
	protected void loadData() {
		collectionController.requestData();
	}

	@Override
	public void onBackPressed() {
		if(!eventsController.onBackPressed()){
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		eventsController.onResume();
	}
	@Override
	protected void onPause() {
		super.onPause();
		eventsController.onPause();
	}
	/* ************************************* Menu Setup ****************************************/

	@Override
	protected int getMenuId() {
		return R.menu.menu_photocollections_activity;
	}

	@Override
	protected void createMenuCommands(Menu menu, CommandExecutor cmdExecutor) {
		collectionController.setupMenu(menu, cmdExecutor);
		eventsController.setupMenu(menu, cmdExecutor);
	}
}
