package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.view.Menu;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CreatePhotoCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CreatePhotoCollectionCommand.PhotoCollectionGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.ViewPhotosFromCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.SelectAndInsertPhotosCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnInsertPhotosNotifyUser;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;

public class PhotoCollectionCommandBuilder{
	BaseActivity activity;
	PhotoCollectionController collectionController;
	PhotoCollectionNavigator navigator;
	PhotoCollectionView photoCollView;
	EntityGetter<String> queryGetter;
	
	public PhotoCollectionCommandBuilder(PhotoCollectionController controller) {
		this(controller, null);
	}
	public PhotoCollectionCommandBuilder(PhotoCollectionController controller, EntityGetter<String> queryGetter) {
		this(controller.getActivity(), controller,controller.getNavigator(), controller.getView());
		this.queryGetter = queryGetter;
	}
	
	public PhotoCollectionCommandBuilder(BaseActivity activity, PhotoCollectionController collectionController,
			PhotoCollectionNavigator navigator, PhotoCollectionView photoCollView) {
		super();
		this.activity = activity;
		this.collectionController = collectionController;
		this.navigator = navigator;
		this.photoCollView = photoCollView;
	}

	public void createCommands(Menu menu, CommandExecutor cmdExecutor){

		PhotoCollectionGetter parentCollGetter = new PhotoCollectionGetter() {
			@Override
			public PhotoCollection getPhotoCollection() {
				return collectionController.getPhotoCollection();
			}
		};

		Command<?> showAlbumPhotos = new ViewPhotosFromCollectionCommand(activity, parentCollGetter, queryGetter);
		Command<PhotoCollection> addAlbumCmd = new CreatePhotoCollectionCommand(activity, parentCollGetter);
		Command<Collection<Photo>> addPhotoCmd = new SelectAndInsertPhotosCommand(activity, new PhotoCollectionIdGetter())
				.onProgress(new BasicResultListener<Photo>(){
                    @Override
                    protected void handleResult(Photo result) {
                        super.handleResult(result);
                        collectionController.onAddPhoto(result);
                    }
                });

		ResultListener<PhotoCollection> onAddAlbum =
				new OnCreateAlbumUpdateView(activity, this.photoCollView);
		ResultListener<? super Collection<Photo>> onAddPhoto = new CompositeResultListener<>(
									new OnInsertPhotosNotifyUser(activity),
									new ReloadViewResultListener(activity, collectionController));

		cmdExecutor.putCommand(R.id.action_viewAlbumPhotos, showAlbumPhotos, null);
		cmdExecutor.putCommand(R.id.action_addAlbum, addAlbumCmd, onAddAlbum);
		cmdExecutor.putCommand(R.id.action_insertPhoto, addPhotoCmd, onAddPhoto);
	}

	private class PhotoCollectionIdGetter implements EntityGetter<Long> {
		@Override
        public Long getEntity() {
            return collectionController.getCollectionId();
        }
	}
}