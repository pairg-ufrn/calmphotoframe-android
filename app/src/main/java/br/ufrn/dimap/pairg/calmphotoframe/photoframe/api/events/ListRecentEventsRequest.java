package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.events;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.events.ListRecentEventsRequest.EventsContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class ListRecentEventsRequest extends BaseGenericRequest<EventsContainer, List<Event>> {


	public ListRecentEventsRequest(ApiConnector apiConnector,
			ResultListener<? super List<Event>> listener) {
		super(apiConnector, EventsContainer.class, listener);
	}

	@Override
	public RequestHandler execute(){
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().eventsUrl();
	}

	@Override
	protected List<Event> convertToResultType(EventsContainer response) {
		return response.events;
	}

	public static class EventsContainer{
		public List<Event> events;
	}
}
