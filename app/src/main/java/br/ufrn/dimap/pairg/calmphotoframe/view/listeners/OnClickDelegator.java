package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

import android.view.View;

import java.util.LinkedList;
import java.util.List;

/**
 */
public class OnClickDelegator implements View.OnClickListener {
    List<View.OnClickListener> listeners;

    public OnClickDelegator() {
        this.listeners = new LinkedList<>();
    }

    public void addListener(View.OnClickListener listener) {
        this.listeners.add(listener);
    }

    public void removeListener(View.OnClickListener listener) {
        this.listeners.remove(listener);
    }

    @Override
    public void onClick(View v) {
        for (View.OnClickListener l : listeners) {
            l.onClick(v);
        }
    }
}
