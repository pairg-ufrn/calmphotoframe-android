package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import com.loopj.android.http.ResponseHandlerInterface;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.image.ImageOrientationHelper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.ImageResponseHandler;
import br.ufrn.dimap.pairg.calmphotoframe.utils.image.ImageUtils;

public class GetImageRequest extends CommonGenericRequest<Bitmap>{
	private Photo photo;
	private int[] wantedMaxImageSize;
	private int[] limitImageSize;

	/** @see GetImageRequest#GetImageRequest(ApiConnector, int, int[], ResultListener) */
	public GetImageRequest(ApiConnector apiConnector, int photoId, ResultListener<? super Bitmap> photoImageListener) {
		this(apiConnector, new Photo(photoId), photoImageListener);
	}
	/** Get image from the photo with id @{code photoId}, but does not flip image if needed*/
	public GetImageRequest(ApiConnector apiConnector, int photoId, int[] wantedImageSize
			, ResultListener<? super Bitmap> photoImageListener) 
	{
		this(apiConnector, new Photo(photoId), wantedImageSize, photoImageListener);
	}

	/** Same as {@link GetImageRequest#GetImageRequest(ApiConnector, int, int[], ResultListener)}, but does not limit the image max size */
	public GetImageRequest(ApiConnector apiConnector, Photo photo, ResultListener<? super Bitmap> photoImageListener) {
		this(apiConnector, photo, new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE}, photoImageListener);
	}

	/** Get image from the photo  @{code photo} with max dimensions limited by {@code maxImageSize}
	 * @param apiConnector -
	 * @param photo - the photo to whom the belongs the image
	 * @param wantedImageSize - an array of length 2 which contains the desired dimensions to scale down the image
	 * @param imageListener - listener to receive the image
	 * */
	public GetImageRequest(ApiConnector apiConnector, Photo photo, int[] wantedImageSize
			, ResultListener<? super Bitmap> imageListener) 
	{
		super(apiConnector, Bitmap.class, imageListener);
		this.photo = photo;
		this.wantedMaxImageSize = wantedImageSize;
		limitImageSize = ImageUtils.instance().getLimitImageSize();
	}

	protected int getMaxWidth(){
		return wantedMaxImageSize[0];
	}
	protected int getMaxHeight(){
		return wantedMaxImageSize[1];
	}
	
	public RequestHandler execute(){
		Bitmap cachedBitmap = getPhotoCache().getImage(photo.getId());
		if(cachedBitmap == null){
			int imgSize = estimateImageOccupation();
			getPhotoCache().reserve(imgSize);
			
			return http().get(genUrl(), buildJsonResponseHandler());
		}
		else{
//			notifySuccess(cachedBitmap);
			Listeners.onResult(listener, cachedBitmap);
			return new FinishedRequestHandler();
		}
	}
	
	private int estimateImageOccupation() {
		int[] imgDimensions = new int[2];

		for(int i=0; i < 2; ++i){
			imgDimensions[i] = wantedMaxImageSize[i] < Integer.MAX_VALUE 
								? wantedMaxImageSize[i] : limitImageSize[i];
		}
		
		return ImageUtils.instance().estimateImageSize(imgDimensions);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().imageUrl(photo.getId());
	}

	protected ResponseHandlerInterface buildJsonResponseHandler() {
		ResultListener<? super Bitmap> imageListener = new OnGetImageDelegator(photo, getPhotoCache(), listener);
		return new ImageResponseHandler(wantedMaxImageSize, limitImageSize, imageListener);
	}
	
	public static class OnGetImageDelegator extends CommonListenerDelegator<Bitmap> {
		private Photo photo;
		private PhotoCache cache;
		
		public OnGetImageDelegator(Photo photo, PhotoCache photoCache, ResultListener<? super Bitmap> delegate) {
			super(delegate);
			this.photo = photo;
			this.cache = photoCache;
		}
		
		@Override
		public void onResult(Bitmap image) {
			Bitmap fixedImage = ImageOrientationHelper.fixImage(photo, image);
			cache.putImage(photo.getId(), fixedImage);

			super.delegateResult(fixedImage);
		}
	}
}