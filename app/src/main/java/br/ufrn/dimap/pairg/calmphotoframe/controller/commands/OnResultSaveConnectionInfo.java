package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;

public final class OnResultSaveConnectionInfo extends BasicResultListener<Endpoint> {

	@Override
	public void onResult(Endpoint result) {
		saveLastConnectionInfo(result);
	}

	protected void saveLastConnectionInfo(Endpoint connectionInfo) {
		if(connectionInfo != null) {
			LocalPersistence.instance().addConnectedServer(connectionInfo);
		}
	}
}