package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.GetPhotoCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class OnResultGetPhoto extends ActivityResultListener<Object>{
	private PhotoHolder photoHolder;
	private ResultListener<? super Photo> onGetPhotoListener;

	public OnResultGetPhoto(BaseActivity activity, PhotoHolder photoHolder)
	{
		super(activity);
		this.photoHolder = photoHolder;
	}

	public ResultListener<? super Photo> getPhotoListener() {
		return onGetPhotoListener;
	}

	public OnResultGetPhoto setPhotoListener(ResultListener<? super Photo> listener) {
		this.onGetPhotoListener = listener;
		return this;
	}

	@Override
	public void onResult(Object result) {
		requestPhoto();
	}

	/* *************************************************************************************/
	public void requestPhoto() {
		new GetPhotoCommand(photoHolder).execute(getPhotoListener());
	}
	
}