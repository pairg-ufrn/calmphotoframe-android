package br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen.FullscreenManager.OnChangeFullscreenListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.InterceptableViewGroup;

public class FullscreenLayout extends InterceptableViewGroup{
	public enum Gestures{NONE, TAP, DOUBLE_TAP}
	
	public class LayoutParams extends ViewGroup.LayoutParams{
		public static final int INVISIBLE_ON_FULLSCREEN = 0;
		public static final int VISIBLE_ON_FULLSCREEN = 1;
		
		public LayoutParams(Context c, AttributeSet attrs) {
			super(c, attrs);
		}
		public LayoutParams(int width, int height) {
			super(width,height);
		}
		public LayoutParams(LayoutParams copy) {
			super(copy);
		}
		
	}

	private static final int DEFAULT_AUTO_FULLSCREEN_DELAY = 4000;

	
	protected final FullscreenManager fullscreenManager = new FullscreenManager();
	private Set<View> invisibleOnFullscreenViews;
	private boolean autoFullscreen;
	private int autoFullscreenMillisecondsDelay;
	private boolean delayAutoFullscreenOnClickControls;
	
	public FullscreenLayout(Context context) {
		super(context);
		init(context);
	}
	public FullscreenLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	public FullscreenLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context){
		this.autoFullscreen = false;
		this.autoFullscreenMillisecondsDelay = DEFAULT_AUTO_FULLSCREEN_DELAY;
		this.delayAutoFullscreenOnClickControls = true;
		
		/** Em modo de edição (visualizando através da IDE), não ativa br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen */
		if(!isInEditMode()){
			this.fullscreenManager.setup((Activity)context, this);
			this.fullscreenManager.setOnChangeFullscreenListener(new OnChangeFullscreenListener() {
				@Override
				public void onChangeFullscreen(boolean fullscreen) {
					/**Se autoFullscreen está habilitado, ao sair do br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen, 
					 * agenda uma transformação de volta à br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen*/
					if(autoFullscreen && fullscreen == false){
						delayFullscreen();
					}
				}
			});
	
			this.invisibleOnFullscreenViews = new HashSet<>();
			this.setOnHierarchyChangeListener(new OnHierarchyChangeListener() {
				@Override
				public void onChildViewRemoved(View parent, View child) {
					//Garantir que views removidas da hierarquia não sejam 
					if(invisibleOnFullscreenViews.contains(child)){
						invisibleOnFullscreenViews.remove(child);
					}
				}
				
				@Override
				public void onChildViewAdded(View parent, View child) {
					// Do nothing
				}
			});

		}
	}

	public void setViewVisibility(View view, int fullscreenVisibility){
		//FIXME: assume que view está na hierarquia
		switch(fullscreenVisibility){
		case LayoutParams.INVISIBLE_ON_FULLSCREEN:
			this.fullscreenManager.addChild(view, false);
			this.invisibleOnFullscreenViews.add(view);
			break;
		case LayoutParams.VISIBLE_ON_FULLSCREEN:
			this.fullscreenManager.addChild(view, true);
			break;
		default:
			throw new RuntimeException(
					"Invalid parameter! Should be "
					+ "\"LayoutParams.INVISIBLE_ON_FULLSCREEN\" or "
					+ "\"LayoutParams.VISIBLE_ON_FULLSCREEN\""); 
		}
	}
	public void setFullscreen(boolean fullscreen) { this.fullscreenManager.setFullscreen(fullscreen);}
	public boolean isFullscreen(){ return this.fullscreenManager.isFullscreen();}
	
	public void delayFullscreen(boolean fullscreen, int millisecondsDelay) {
		this.fullscreenManager.setFullscreen(fullscreen, millisecondsDelay);
	}
	public void toggleFullscreen(){
		this.fullscreenManager.toggleFullscreen();
	}
	public void fadeToFull(int fadeTime){
		fade(fadeTime, true);
	}
	public void fadeFromFull(int fadeTime){
		fade(fadeTime, false);
	}
	public void fade(int fadeTime, boolean toFullscreen){
		setFullscreen(toFullscreen);
		delayFullscreen(toFullscreen, fadeTime);
	}

	public boolean getAutoFullscreen() { return autoFullscreen; }
	public void setAutoFullscreen(boolean auto) {
		if(auto && !autoFullscreen){
			this.delayFullscreen();
		}
		autoFullscreen = auto;
	}
	/** Auxiliar para por a tela em modo br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen após o tempo definido em autoFullscreenMillisecondsDelay.
	 *  Caso já haja uma solicitação para alterar o modo br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen, ela será cancelada.*/
	private void delayFullscreen() {
		this.delayFullscreen(true, this.autoFullscreenMillisecondsDelay);
	}

	public int getAutoFullscreenMillisecondsDelay() { return autoFullscreenMillisecondsDelay;}
	public void setAutoFullscreenMillisecondsDelay(int millisecondsTime) 
	{
		this.autoFullscreenMillisecondsDelay = millisecondsTime;
	}
	
/* ********************************************* Gestures *******************************************/
	public Gestures getToggleGesture(){
		if(hasOnTouchScreenAreaListener())
		{
			return getOnTouchScreenAreaListener().getToggleGesture();
		}
		return null;
	}
	public void setToggleOnGesture(Gestures gesture) {
//		if(gesture != null){
//			this.setOnInterceptTouchListener(new OnTouchScreenArea(this.getContext(), gesture));
//		}
//		else{
//			this.setOnInterceptTouchListener(null);
//		}
		ensureOnTouchScreenAreaListener();
		getOnTouchScreenAreaListener().setToggleGesture(gesture);
	}

	public Gestures getShowGesture(){
		return hasOnTouchScreenAreaListener() 
				? getOnTouchScreenAreaListener().getShowGesture()
				: null;
	}
	public void setShowGesture(Gestures gesture) {
		ensureOnTouchScreenAreaListener();
		getOnTouchScreenAreaListener().setShowGesture(gesture);
	}
	public Gestures getHideGesture(){
		return hasOnTouchScreenAreaListener() 
				? getOnTouchScreenAreaListener().getHideGesture()
				: null;
	}
	public void setHideGesture(Gestures gesture) {
		ensureOnTouchScreenAreaListener();
		getOnTouchScreenAreaListener().setHideGesture(gesture);
	}
	
	private void ensureOnTouchScreenAreaListener(){
		if(!hasOnTouchScreenAreaListener()){
			this.setOnInterceptTouchListener(new OnTouchScreenArea(this.getContext(), Gestures.NONE));
		}
	}
	private boolean hasOnTouchScreenAreaListener(){
		OnInterceptTouchListener onInterceptTouchListener = this.getOnInterceptTouchListener();
		return onInterceptTouchListener != null 
				&& onInterceptTouchListener instanceof OnTouchScreenArea;
	}
	private OnTouchScreenArea getOnTouchScreenAreaListener(){
		return ((OnTouchScreenArea)this.getOnInterceptTouchListener());
	}
	

/* ****************************************************************************************************/
	
	class OnTouchScreenArea extends SimpleOnGestureListener implements OnInterceptTouchListener{
		private Gestures toggleGesture, showGesture, hideGesture;
		private GestureDetectorCompat detector; 
		
		public OnTouchScreenArea(Context context, Gestures gesture){
			detector = new GestureDetectorCompat(context,this);
			toggleGesture = gesture;
			showGesture = Gestures.NONE;
			hideGesture = Gestures.NONE;
		}

		public Gestures getToggleGesture() { return toggleGesture; }
		public Gestures getShowGesture()   { return showGesture; }
		public Gestures getHideGesture()   { return hideGesture; }
		public void setToggleGesture(Gestures gesture) {
			this.toggleGesture = gesture;
		}
		public void setShowGesture(Gestures showGesture) {
			this.showGesture = showGesture;
		}
		public void setHideGesture(Gestures hideGesture) {
			this.hideGesture = hideGesture;
		}

		@Override
		public boolean onInterceptTouchEvent(MotionEvent event) {
			if(!isFullscreen() 
					&& autoFullscreen 
					&& delayAutoFullscreenOnClickControls
					&& hasInterceptedInvisibleOnFullscreenViews(event)){
				Log.d(this.getClass().getPackage().getName(),"intercept touch event on child. Delay autoFullscreen.");
				delayFullscreen();
			}
			
			//Delega detecção de gestos para GestureDetector
			this.detector.onTouchEvent(event);
			
			//Não intercepta eventos
	        return false;
		}

		@Override
		public boolean onDoubleTap(MotionEvent event){
			gestureDetected(Gestures.DOUBLE_TAP);
			return false;
		}
		@Override
		public boolean onSingleTapConfirmed(MotionEvent event){
			gestureDetected(Gestures.TAP);
			return false;
		}
		
		void gestureDetected(Gestures gesture){
			if(gesture == this.toggleGesture){
				toggleFullscreen();
			}
			else if(gesture == this.showGesture){
				setFullscreen(false);
				if(autoFullscreen){
					delayFullscreen(true, autoFullscreenMillisecondsDelay);
				}
			}
			else if(gesture == this.hideGesture){
				setFullscreen(true);
			}
		}
    }
	/** Checa se evento de toque interceptou alguma view que não é visível em modo br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen*/
	private boolean hasInterceptedInvisibleOnFullscreenViews(MotionEvent event) {
		for(View view : this.invisibleOnFullscreenViews){
			Rect rect = new Rect();
			view.getHitRect(rect);
			if(view.getVisibility() == View.VISIBLE && rect != null 
					&& rect.contains((int)event.getX(), (int)event.getY())){
				return true;
			}
		}
		return false;
	}
}
