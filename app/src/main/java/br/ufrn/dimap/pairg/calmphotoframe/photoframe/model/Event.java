package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.util.Date;

public class Event implements Cloneable{
	private CharSequence message;
	private Date createdAt;
	private Long userId;
	private User user;
	private String type;
	
	/** The amount of events grouped on this*/
	private int groupedEvents;
	
	public Event() {
		this(null, null, null, null, null);
	}

	public Event(String evtType, CharSequence mainMessage, Date createdAt, User user, Long userId) {
		super();
		this.type = evtType;
		this.message = mainMessage;
		this.createdAt = createdAt;
		this.userId = userId;
		this.user = user;
		this.groupedEvents = 1;
	}
	
	public CharSequence getMessage() {
		return message;
	}

	public Event setMessage(CharSequence message) {
		this.message = message;
        return this;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public Event setCreatedAt(Date date) {
		this.createdAt = date;
        return this;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Long getUserId() {
		if(user != null){
			return user.getId();
		}
		return userId;
	}
	public Event setUserId(Long userId) {
		if(user != null){
			user.setId(userId);
		}
		this.userId = userId;

        return this;
	}

	public User getUser() {
		return user;
	}
	public Event setUser(User user) {
		this.user   = (user == null) ? null : user.clone();
		this.userId = getId(user);

        return this;
	}

	public boolean hasUser(){
		return user != null;
	}

	public int getGroupedEvents() {
		return groupedEvents;
	}
	public Event setGroupedEvents(int groupedEvents) {
		this.groupedEvents = groupedEvents;

        return this;
	}

	@Override
	public Event clone(){
		Event cloned = new Event(getType(), getMessage(), getCreatedAt(), getUser(), getUserId());
		cloned.setGroupedEvents(getGroupedEvents());
		
		return cloned;
	}
	@Override
	public String toString() {
		String evtFormat = "{type : %s, message : '%s', createdAt: '%s', userId : %d"
				+ ", grouped: %d }";
		return String.format(evtFormat
				, getType(), getMessage(), getCreatedAt(), getUserId(), getGroupedEvents());
	}
	
	
	private static Long getId(User user) {
		return (user == null) ? null : user.getId();
	}
}
