package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers.PhotoImageWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.FragmentBuilderPageAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo.PhotoInfoEditorFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo.PhotoMarkerEditorFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;

public class PhotoDescriptionEditorActivity extends    BaseActivity
{
	public static final String CLASS_NAME 	  = PhotoDescriptionEditorActivity.class.getName();
	public static final String ERROR_KEY 	  = CLASS_NAME + ".Error";
	private static final String TAG_CONFIRMCANCEL_DIALOG     = CLASS_NAME + ".ConfirmCancelDialog";

	private Photo photoInfo;
	private Photo initialPhotoInfo;

    private PhotoEditorPageAdapter adapter;

	/* ********************************* Criação da Activity **********************************/

    public PhotoDescriptionEditorActivity() {
    }

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tabs);
        setupToolbar();

		if(adapter == null) {
            adapter = new PhotoEditorPageAdapter(getSupportFragmentManager(), this);
		}

		createTabs();

        receivePhotoDescriptor();
	}

    @Override
    protected void configureToolbar(Toolbar t) {
        super.configureToolbar(t);

        t.setNavigationIcon(R.drawable.ic_close);
        t.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCancel();
            }
        });

        TextView customTitle = ViewUtils.getView(this, R.id.toolbarTitle);
        customTitle.setText(getTitle());
    }

    private void receivePhotoDescriptor() {
        this.initialPhotoInfo = extractPhoto(getIntent());
        setPhoto(initialPhotoInfo);

        if(initialPhotoInfo != null){
            ApiClient.instance()
                    .photos().getImage(initialPhotoInfo, new OnResultPopulateViews(this, adapter));
        }
    }

    private Photo extractPhoto(Intent intent) {
        if(intent != null){
            if(intent.getExtras().containsKey(Intents.Extras.Photo)){
                return (Photo)intent.getSerializableExtra(Intents.Extras.Photo);
            }
        }
        return null;
    }

    void setPhoto(Photo photo){
        if(photo == null){
            photoInfo = null;
            //TODO: clear views
            return;
        }

        photoInfo = new Photo(photo);
        adapter.photoInfoFragment().setFromPhotoDescriptor(photoInfo);
        adapter.markersFragment().setPhotoMarks(photo.getMarkers());
    }

	private void createTabs() {
        ViewPager viewPager = ViewUtils.getView(this, R.id.viewpager);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = ViewUtils.getView(this, R.id.tabslayout);
        if(tabLayout != null){
            tabLayout.setupWithViewPager(viewPager);
			tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
			tabLayout.setTabMode(TabLayout.MODE_FIXED);
		}
	}

    public static class PhotoEditorPageAdapter extends FragmentBuilderPageAdapter{
        public PhotoEditorPageAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.addFragment(context.getText(R.string.photoDescriptionEditor_descriptionTab), new FragmentCreator.FragmentBuilder() {
                @Override
                public Fragment buildFragment() {
                    return new PhotoInfoEditorFragment();
                }
            });
            this.addFragment(context.getText(R.string.photoDescriptionEditor_markersTab), new FragmentCreator.FragmentBuilder() {
                @Override
                public Fragment buildFragment() {
                    return new PhotoMarkerEditorFragment();
                }
            });
        }

        public PhotoMarkerEditorFragment markersFragment(){
            return fragment(1);
        }
        public PhotoInfoEditorFragment photoInfoFragment(){
            return fragment(0);
        }

        public <T extends Fragment> T fragment(int index){
            return (T) getFragment(index);
        }
    }
	
	/* ********************************** Menu ************************************************/

    @Override
    protected int getMenuId() {
        return R.menu.menu_photodescription_editor;
    }


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_photoDescriptionEditor_save:
			savePhotoDescription();
			break;
		default:
			return false;
		}
		return super.onOptionsItemSelected(item);
	}

	private void savePhotoDescription() {
		updateDescriptorFromView(this.photoInfo);

		ApiClient.instance().photos().update(photoInfo, new OnUpdatePhotoFinishActivity(this));
	}

	private void updateDescriptorFromView(Photo photoDescriptor) {
		adapter.photoInfoFragment().updatePhotoDescriptor(photoDescriptor);
		photoDescriptor.setMarkers(adapter.markersFragment().getPhotoMarks());
	}
	private void cancelEdition() {
		updateDescriptorFromView(photoInfo);
		if(initialPhotoInfo.equals(photoInfo)){
			executeCancel();
		}
		else{
			requestCancelConfirmation();
		}
	}

	/* ******************************** Cancel Action ****************************************/
	private void requestCancelConfirmation() {
		SimpleDialogFragment dialogFragment = new SimpleDialogFragment();
    	dialogFragment.setOkMessage    (getString(R.string.descriptioneditor_confirmcancel_yes));
    	dialogFragment.setCancelMessage(getString(R.string.descriptioneditor_confirmcancel_no));
    	dialogFragment.setDialogMessage(getString(R.string.descriptioneditor_confirmcancel_message));
    	dialogFragment.setDialogResponseListener(new DialogResponseListener() {
			@Override
			public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
				if(positive){
					executeCancel();
				}
				return true;
			}
		});
    	dialogFragment.show(getSupportFragmentManager(), TAG_CONFIRMCANCEL_DIALOG);
	}
	private void executeCancel() {
		this.setResult(RESULT_CANCELED);
		this.finish();
	}

	
	/* *********************************************************************************************/

	class OnResultPopulateViews extends ActivityResultListener<PhotoImageWrapper>{
        PhotoEditorPageAdapter adapter;

        public OnResultPopulateViews(BaseActivity activity, PhotoEditorPageAdapter adapter) {
            super(activity);
            this.adapter = adapter;
        }

        @Override
        public void onResult(PhotoImageWrapper result) {
            photoInfo = result.getPhoto();

            if(adapter.photoInfoFragment() != null){

                Log.d(getClass().getSimpleName(), "OnResultPopulateViews. received image at adapter: " + this.adapter);
                Log.d(getClass().getSimpleName(), "OnResultPopulateViews. info frag: " + adapter.photoInfoFragment()
                        + "; markers frag: " + adapter.markersFragment());

                adapter.photoInfoFragment().setFromPhotoDescriptor(result.getPhoto());
                adapter.markersFragment().setImage(result.getImage());
            }
        }

        @Override
        public void onFailure(Throwable error) {
            notifyError(error);
        }

        @Override
        public boolean isListening() {
            return true;
        }
    }

	/* **********************************OnUpdatePhotoListener*******************************************/
	static class OnUpdatePhotoFinishActivity extends ActivityResultListener<Photo>{
		public OnUpdatePhotoFinishActivity(BaseActivity activity) {
			super(activity);
		}

		@Override
		public void onResult(Photo result) {
			Intent resultIntent = new Intent();
			resultIntent.putExtra(Intents.Extras.Photo, result);
			finishActivity(RESULT_OK, resultIntent);
		}
		@Override
		public void onFailure(Throwable error) {
			Intent resultIntent = new Intent();
			resultIntent.putExtra(ERROR_KEY, error);
			finishActivity(RESULT_CANCELED, resultIntent);
		}

		private void finishActivity(int resultStatus, Intent resultIntent) {
			getActivity().setResult(resultStatus, resultIntent);
			
			getActivity().finish();
		}

	}
}
