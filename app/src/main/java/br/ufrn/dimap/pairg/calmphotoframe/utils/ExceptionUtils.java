package br.ufrn.dimap.pairg.calmphotoframe.utils;

public class ExceptionUtils {

	@SuppressWarnings("unchecked")
	public static <T extends Throwable> T getCause(Throwable error, Class<? extends T> exceptionClass) {
		if(error == null){
			return null;
		}

		if(exceptionClass.isInstance(error)){
			return (T)error;
		}
		else if(error != null && error.getCause() != null){
			return getCause(error.getCause(), exceptionClass);
		}
		return null;
	}
	public static boolean isCausedBy(Throwable error, Class<? extends Throwable> exceptionClass) {
		return getCause(error, exceptionClass) != null;
	}
}
