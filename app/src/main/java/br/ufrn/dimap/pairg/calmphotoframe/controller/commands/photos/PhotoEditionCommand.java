package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import android.app.Activity;
import android.content.Intent;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.Intents;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PhotoDescriptionEditorActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class PhotoEditionCommand extends ActivityStatelessCommand<Photo> {
	private EntityGetter<Photo> photoGetter;

	public PhotoEditionCommand(BaseActivity activity, EntityGetter<Photo> photoGetter) {
		super(activity);
		this.photoGetter = photoGetter;
	}

	@Override
	public void execute(ResultListener<? super Photo> cmdResultListener) {
		startPhotoDescriptionEdition(photoGetter.getEntity(), cmdResultListener);
	}

	private void startPhotoDescriptionEdition(Photo photoDescriptor, final ResultListener<? super Photo> cmdResultListener) {
        if(photoDescriptor == null){
            return;
        }

    	Intent intent = new Intent(getContext(), PhotoDescriptionEditorActivity.class);
    	intent.putExtra(Intents.Extras.Photo, photoDescriptor);
    	
    	getActivity().startActivityForResult(intent, new OnEditedPhotoListener(cmdResultListener));
	}

	private class OnEditedPhotoListener implements OnActivityResultListener {

		private final ResultListener<? super Photo> listener;

		public OnEditedPhotoListener(ResultListener<? super Photo> resultListener) {
			this.listener = resultListener;
		}

		@Override
        public void onActivityResult(int resultCode, Intent intent) {
            if (resultCode == Activity.RESULT_OK) {

                Photo photoDescriptor = (Photo)intent.getSerializableExtra(Intents.Extras.Photo);
                Listeners.onResult(listener, photoDescriptor);
            }
        }

	}
}
