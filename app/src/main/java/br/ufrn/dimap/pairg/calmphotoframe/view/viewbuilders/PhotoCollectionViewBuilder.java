package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ViewBuilder;

public class PhotoCollectionViewBuilder implements ViewBuilder {
	private LayoutInflater inflater;
	private int layoutId;
	public PhotoCollectionViewBuilder(Activity activity) {
		this.inflater = activity.getLayoutInflater();
		layoutId = R.layout.view_photocollection;
	}
	@Override
	public View buildView(Object data, View recycleView, ViewGroup parent) {
		PhotoCollection photoContext = (PhotoCollection)data;
		TextView contextNameView = (TextView) (recycleView == null ? null : recycleView.findViewById(R.id.view_photoContext_Name));
		if(contextNameView == null){
			recycleView = inflater.inflate(layoutId, parent, false);
			contextNameView = (TextView) recycleView.findViewById(R.id.view_photoContext_Name);
		}
		contextNameView.setText(photoContext.getName());
		
		return recycleView;
	}
}