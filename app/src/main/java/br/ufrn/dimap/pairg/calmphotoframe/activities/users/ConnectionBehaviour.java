package br.ufrn.dimap.pairg.calmphotoframe.activities.users;


import android.support.v7.app.AppCompatActivity;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;

public class ConnectionBehaviour {

    protected static class Holder{
        public static ConnectionBehaviour singleton = new ConnectionBehaviour();
    }

    public static ConnectionBehaviour instance(){
        return Holder.singleton;
    }

    public ResultListener<Object> onLogin(BaseActivity activity){
        return onLogin(activity, activity);
    }
    public ResultListener<Object> onLogin(AppCompatActivity activity, UserNotifier userNotifier){
        return new CompositeResultListener<>(
                new WaitResultDialogController(activity),
                new OnResultShowMainActivity(activity),
                new NotifyUserOnLoginOrSignupFailure(userNotifier, LoginMode.Login));
    }

    public ResultListener<Object> onSignup(BaseActivity activity){
        return Listeners.<Object>join(
                new OnSignupResult(activity),
                new NotifyUserOnLoginOrSignupFailure(activity, LoginMode.Signup));
    }
}
