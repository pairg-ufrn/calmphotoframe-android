package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Photo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2240205513322228230L;
	
	private String description;
	private String moment;
	//TODO: modificar tipo de date para um DateTime
	private String date;
	private String place;
	private Integer id;
	private String credits;

	private PhotoMetadata metadata;

	private final ArrayList<PhotoMarker> photoMarks;


	public Photo(){
		this(null,"","","",null);
	}
	public Photo(long photoId){
		this(photoId,"","","",null);
	}

	@SuppressWarnings("deprecation")
	public Photo(
			Number photoId, 
			String photoMoment,  
			String photoDescription, 
			String photoPlace, 
			Date photoDate)
	{
		id = photoId == null ? null : photoId.intValue();
		moment = photoMoment;
		//FIXME : modificar tipo de date
		date = (photoDate == null ?  "" : photoDate.toGMTString());
		place = photoPlace;
		description = photoDescription;
		photoMarks = new ArrayList<>();
	}

	public Photo(Photo descriptor) {
		this();
		if(descriptor != null){
			set(descriptor);
		}
	}
	private void set(Photo descriptor) {
		this.setId(descriptor.getId());
		this.setMoment(descriptor.getMoment());
		this.setDate(descriptor.getDate());
		this.setPlace(descriptor.getPlace());
		this.setDescription(descriptor.getDescription());
		this.setMetadata(descriptor.getMetadata());
		this.photoMarks.clear();
		this.photoMarks.addAll(descriptor.clonePhotoMarkers());
	}

	public List<? extends PhotoMarker> clonePhotoMarkers() {
		List<PhotoMarker> list = new ArrayList<>(this.photoMarks.size());
		for(PhotoMarker marker : this.photoMarks){
			list.add(new PhotoMarker(marker));
		}
		return list;
	}

	public Integer getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMoment() {
		return moment;
	}
	public String getDate() {
		return date;
	}
	public String getPlace() {
		return place;
	}
	public String getCredits() {
		return credits;
	}
	public PhotoMetadata getMetadata() {
		return metadata;
	}
	public List<PhotoMarker> getMarkers() {
		return photoMarks;
	}

	public void setId(Integer someId) {
		this.id = Integer.valueOf(someId);
	}
	public void setMoment(String moment) {
		this.moment = moment;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public void setCredits(String credits) {
		this.credits = credits;
	}
	public void setMetadata(PhotoMetadata metadata) {
		this.metadata = metadata;
	}
	public void setMarkers(final List<PhotoMarker> marks) {
		photoMarks.clear();
		photoMarks.addAll(marks);
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("id: ").append(id).append("; ")
	       .append("moment: ").append(moment).append("; ")
	       .append("date: ").append(date).append("; ")
	       .append("place: ").append(place).append("; ")
	       .append("metadata: ").append(metadata).append("; ")
	       .append("markers: ").append(photoMarks).append("; ");
		return builder.toString();
	}
	@Override
	public boolean equals(Object other) {
		if(other == null || !this.getClass().isInstance(other)){
			return false;
		}
		Photo otherDescriptor = (Photo)other;
		if(this == otherDescriptor){
			return true;
		}
		boolean equals = 
				compare(this.getDate(), otherDescriptor.getDate())
			 && compare(this.getDescription(), otherDescriptor.getDescription())
			 && compare(this.getId(), otherDescriptor.getId())
			 && compare(this.getMoment(), otherDescriptor.getMoment())
			 && compare(this.getPlace(), otherDescriptor.getPlace())
			 && markersAreEquals(this.getMarkers(), otherDescriptor.getMarkers());
		
		return equals;
	}
	@Override
	public Photo clone(){
		return new Photo(this);
	}
	private boolean compare(Object obj1, Object obj2) {
		if(obj1 == null || obj2 == null){
			return (obj1 == null && obj2 == null);
		}
		return obj1.equals(obj2);
	}

	private boolean markersAreEquals(List<PhotoMarker> photoMarkers1,
			List<PhotoMarker> photoMarkers2) {

		if(photoMarkers1.size() == photoMarkers2.size()){
			Iterator<PhotoMarker> iterator1 = photoMarkers1.iterator();
			Iterator<PhotoMarker> iterator2 = photoMarkers2.iterator();
			
			while(iterator1.hasNext() && iterator2.hasNext()){
				if(! compare(iterator1.next(), iterator2.next() )){
					return false;
				}
			}
			return true;
		}
		
		return false;
	}
	public static void assertIsValid(Photo photo) {
		if(photo == null || photo.getId() <= 0){
			String errMessage = "Photo is invalid because" + (photo == null ? " is null!" 
																   : " has invalid id: " + photo.getId());
			throw new IllegalArgumentException(errMessage);
		}
	}
}
