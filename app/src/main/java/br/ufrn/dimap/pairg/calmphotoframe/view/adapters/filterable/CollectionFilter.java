package br.ufrn.dimap.pairg.calmphotoframe.view.adapters.filterable;

import java.util.Collection;
import java.util.List;

public interface CollectionFilter<T> {
	List<T> filter(Collection<? extends T> items, CharSequence constraint);
}
