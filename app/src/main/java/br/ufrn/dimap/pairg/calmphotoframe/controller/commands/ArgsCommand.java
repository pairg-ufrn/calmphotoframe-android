package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;


public interface ArgsCommand<Arg, Result> {
    void execute(Arg arg, ResultListener<? super Result> listener);
}
