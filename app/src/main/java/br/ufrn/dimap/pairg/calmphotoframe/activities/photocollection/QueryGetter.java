package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;

/**
 * Implementation of {@link EntityGetter} that gets the current query from a {@link PhotosSearchEngine}.
 */
public class QueryGetter implements EntityGetter<String> {
    PhotosSearchEngine searchEngine;

    public QueryGetter(PhotosSearchEngine searchEngine) {
        this.searchEngine = searchEngine;
    }

    @Override
    public String getEntity() {
        return searchEngine.getCurrentQuery();
    }
}
