package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public enum PhotoContextVisibility {
	EditableByOthers,
	VisibleByOthers,
	Private;
}
