package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

public class CommonListenerDelegator<T> extends ResultListenerDelegator<T, T>{
	
	public CommonListenerDelegator(ResultListener<? super T> delegate) {
		super(delegate);
	}

	protected T convertResult(T result){
		return result;
	}
}