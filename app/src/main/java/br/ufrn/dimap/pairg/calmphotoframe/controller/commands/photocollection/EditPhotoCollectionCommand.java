package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand.PhotoComponentSelector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.InputDialogFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.InputDialogFragment.InputResponseListener;

public class EditPhotoCollectionCommand extends ActivityStatelessCommand<PhotoCollection>
{
	private static final String DIALOG_TAG = EditPhotoCollectionCommand.class + ".Dialog";
	
	private PhotoCollection targetCollection;
	private PhotoComponentSelector selector;
	
	public EditPhotoCollectionCommand(BaseActivity activity, PhotoComponentSelector selector)
	{
		super(activity);
		this.selector = selector;
	}
	
	public PhotoComponentSelector getSelector() {
		return selector;
	}

	@Override
	public void execute(ResultListener<? super PhotoCollection> listener) {
		PhotoCollection selectedContext = getSelectedCollection();
		if(selectedContext == null){
			return;
		}
		this.targetCollection = selectedContext;
		
		InputDialogFragment dialog = new InputDialogFragment();
		dialog.setInputResponseListener(new OnRenameAlbumDialogResponse(listener));
		dialog.setDialogMessage(getText(R.string.dialog_editAlbumName_message));
		dialog.setOkMessage(getText(R.string.dialog_defaultOk));
		dialog.setInputText(targetCollection.getName());
		dialog.setCancelMessage(getText(R.string.dialog_defaultCancel));
		dialog.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
	}

	private CharSequence getText(int resId) {
		return getActivity().getText(resId);
	}

	private PhotoCollection getSelectedCollection() {
		PhotoCollection selectedContext = null;
		Collection<PhotoComponent> selectedComponents = getSelector().getSelectedItems();
		if(selectedComponents.size() == 1){
			PhotoComponent first = selectedComponents.iterator().next();
			if(first.isCollection()){
				selectedContext = first.getPhotoCollection();
			}
		}
		return selectedContext;
	}

	class OnRenameAlbumDialogResponse implements InputResponseListener{
        ResultListener<? super PhotoCollection> listener;

        public OnRenameAlbumDialogResponse(ResultListener<? super PhotoCollection> listener) {
            this.listener = listener;
        }

        @Override
        public boolean onResponse(InputDialogFragment inputDialog,
                                  boolean responseIsPositive, String response) {
            if(responseIsPositive && !response.equals(targetCollection.getName())){
                PhotoCollection collection = targetCollection.clone();
                collection.setName(response);
                ApiClient.instance().collections().update(collection, listener);
            }
            return true;
        }
    }

}
