package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.AsyncGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnSaveDataListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextVisibility;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.LinearLayoutManagerFix;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.CollaboratorsAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoContextVisibilityAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ViewBuilderAdapter;

public class PhotoContextComposeFragment extends Fragment{
	public static class ContextAndCollaboratorsWrapper{
		public PhotoContext context;
		public Collection<Collaborator> collaborators;
		public ContextAndCollaboratorsWrapper(PhotoContext context, Collection<Collaborator> collaborators) {
			super();
			this.context = context;
			this.collaborators = collaborators;
		}
	}
	
	public interface OnFinishListener{
		void onFinish(PhotoContextComposeFragment fragment);
	}
	
	private ViewBuilderAdapter<PhotoContextVisibility> visibilityAdapter;

	private Spinner visibilitySpinner;
	private TextView nameTxtView;
	private TextInputLayout textInputLayout;
	
	private CollaboratorsAdapter collaboratorsAdapter;
	private FloatingActionButton addCollaboratorBtn;
	
	private PhotoContext photoContext;
	private Long currentUserId;
	private boolean editable;
	
	private OnSaveDataListener<ContextAndCollaboratorsWrapper> onSaveContextListener;
	private OnFinishListener onFinishListener;
	
	private ContextDetailsActionModeCallback actionModeCallback;
	
	private AsyncGetter<Collection<User>> usersAsyncGetter;
	
	public PhotoContextComposeFragment() {
		this.editable = true;
		this.usersAsyncGetter = new UsersAsyncGetterFilter(null);
		
		collaboratorsAdapter = new CollaboratorsAdapter();
		visibilityAdapter = new PhotoContextVisibilityAdapter();
		final CharSequence callbackTitle = "";

		actionModeCallback = new ContextDetailsActionModeCallback(callbackTitle);
		setupActionModeCallback();
	}

	public boolean isEditable(){
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
        actionModeCallback.setEditable(editable);
	}
	
	public AsyncGetter<Collection<User>> getUsersAsyncGetter() {
		return usersAsyncGetter;
	}
	public void setUsersAsyncGetter(AsyncGetter<Collection<User>> usersAsyncGetter) {
		this.usersAsyncGetter = new UsersAsyncGetterFilter(usersAsyncGetter);
	}

	public OnSaveDataListener<ContextAndCollaboratorsWrapper> getOnSaveContextListener() {
		return onSaveContextListener;
	}
	public void setOnSaveContextListener(OnSaveDataListener<ContextAndCollaboratorsWrapper> onSaveContextListener) {
		this.onSaveContextListener = onSaveContextListener;
	}

	public OnFinishListener getOnFinishListener() {
		return onFinishListener;
	}
	public void setOnFinishListener(OnFinishListener onFinishListener) {
		this.onFinishListener = onFinishListener;
	}

	public PhotoContext getPhotoContext() {
		return photoContext;
	}
	public PhotoContext extractPhotoContext() {
		photoContext.setName(nameTxtView.getText().toString());
		photoContext.setVisibility((PhotoContextVisibility)visibilitySpinner.getSelectedItem());
		
		return photoContext;
	}
	public void setPhotoContext(PhotoContext photoContext) {
		this.photoContext = photoContext;
		if(getView() != null){
			syncView();
		}
	}

	public Collection<Collaborator> getCollaborators(){
		return collaboratorsAdapter.getItems();
	}
	public void setCollaborators(Collection<Collaborator> collaborators) {
		collaboratorsAdapter.setItems(collaborators);
	}

	public void enableCollaborator(Long id) {
		collaboratorsAdapter.enableCollaborator(id);
	}
	public void disableCollaborator(Long id) {
		collaboratorsAdapter.disableCollaborator(id);
	}

	public void setCurrentUser(User currentUser) {
		this.currentUserId = currentUser != null ? currentUser.getId() : null;
		disableCollaborator(currentUserId);
		collaboratorsAdapter.setCurrentUserId(currentUserId);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_photocontext_compose, container, false);
		
		visibilitySpinner = ViewUtils.getView(rootView, R.id.photocontext_visibitySelection);
		visibilitySpinner.setAdapter(visibilityAdapter);
		
		nameTxtView = ViewUtils.getView(rootView, R.id.photocontext_inputName);
		textInputLayout = ViewUtils.getView(rootView, R.id.photocontext_inputNameLayout);
		buildColaboratorList(rootView);
		this.addCollaboratorBtn = ViewUtils.getView(rootView, R.id.photocontext_addCollaboratorBtn);
		this.addCollaboratorBtn.setOnClickListener(new OnClickAddCollaborator());
//		syncView();
		
		nameTxtView.addTextChangedListener(new OnChangeTextValidate());
		
		return rootView;
	}
	
	
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		//Setup view state after restore, because automatic restoring is overriding changes
		syncView();
	}
	
	private RecyclerView buildColaboratorList(View rootView) {
		RecyclerView colaboratorsList = ViewUtils.getView(rootView, R.id.photocontext_collaborators);
		colaboratorsList.setLayoutManager(new LinearLayoutManagerFix(getContext()));
		colaboratorsList.setItemAnimator(new DefaultItemAnimator());
		colaboratorsList.setAdapter(collaboratorsAdapter);
		colaboratorsList.requestLayout();
		return colaboratorsList;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		getAppCompatActivity().startSupportActionMode(actionModeCallback);
	}

	/* **********************************************************************************************/
	private AppCompatActivity getAppCompatActivity() {
		return (AppCompatActivity) getActivity();
	}
	
	private void setupActionModeCallback() {
		actionModeCallback.putMenuCommand(R.id.action_confirm, new BaseStatelessCommand<Void>() {
			@Override
			public void execute(ResultListener<? super Void> cmdResultListener) {
				savePhotoContext();
			}
		});
	}

	private void addCollaborator() {
		long ctxId = getPhotoContext().getId();
		AddCollaboratorDialog collabDialog = new AddCollaboratorDialog(ctxId, getUsersAsyncGetter());
		collabDialog.setOnAddListener(new BasicResultListener<Collaborator>(){
			@Override
			public void onResult(Collaborator result) {
				collaboratorsAdapter.addItem(result);
			}
		});
		collabDialog.show(getFragmentManager(), "DIALOG_TAG");
	}
	
	private void savePhotoContext() {
		Log.d(getClass().getSimpleName(), "save photo context");
		if(validate()){
			if(photoContext == null){
				photoContext = new PhotoContext();
			}
			extractPhotoContext();
			
			ContextAndCollaboratorsWrapper wrapper = 
					new ContextAndCollaboratorsWrapper(photoContext, collaboratorsAdapter.getItems());
			if(onSaveContextListener == null 
					|| onSaveContextListener.onSaveData(wrapper))
			{
				actionModeCallback.finish();
			}
		}
	}
	
	private boolean validate() {
		if(nameTxtView.getText().length() == 0){
			textInputLayout.setError(getText(R.string.error_field_required));
			return false;
		}
		else{
			textInputLayout.setErrorEnabled(false);
		}
		return true;
	}

	protected void syncView() {
		textInputLayout.setErrorEnabled(false);
		if(photoContext != null){
			visibilitySpinner.setSelection(visibilityAdapter.indexOf(photoContext.getVisibility()));
			nameTxtView.setText(photoContext.getName());
		}
		else{
			visibilitySpinner.setSelection(0);
			nameTxtView.setText("");
		}
		
		textInputLayout.getEditText().setEnabled(isEditable());
		visibilitySpinner.setEnabled(isEditable());
		addCollaboratorBtn.setVisibility(isEditable() ? View.VISIBLE : View.GONE);
		collaboratorsAdapter.setEditable(isEditable());
	}

	/* **********************************************************************************************/
	private class OnClickAddCollaborator implements OnClickListener {
		@Override
		public void onClick(View v) {
			addCollaborator();
		}
	}
	
	private class OnChangeTextValidate implements TextWatcher {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) 
		{}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) 
		{}

		@Override
		public void afterTextChanged(Editable s) {
			validate();
		}
	}

	
	private class UsersAsyncGetterFilter implements AsyncGetter<Collection<User>> {
		private Collection<User> allUsers;
		private AsyncGetter<Collection<User>> delegate;
		
		public UsersAsyncGetterFilter(AsyncGetter<Collection<User>> asyncGetter) {
			allUsers = new LinkedList<>();
			this.delegate = asyncGetter;
		}
		
		@Override
		public void getAsync(ResultListener<? super Collection<User>> listener) {
			if(allUsers.isEmpty()){
				getUsers(listener);
			}
			else{
				onGetUsers(this.allUsers, listener);
			}
		}

		private void getUsers(final ResultListener<? super Collection<User>> listener) {
			if(delegate == null){
				List<User> empty = Collections.emptyList();
				onGetUsers(empty, listener);
			}
			else{
				delegate.getAsync(new BasicResultListener<Collection<User>>(){
					@Override
					public void onResult(Collection<User> result) {
//                        fixUserNames(result);
                        onGetUsers(result, listener);
					}
				});
			}
		}

//        private void fixUserNames(Collection<User> result) {
//            if(result != null){
//                for(User u : result){
//                    u.setLogin(ContentViewHelper.getUserName(getContext(), u));
//                }
//            }
//        }

        private void onGetUsers(Collection<User> allUsers, ResultListener<? super Collection<User>> listener){
			this.allUsers = allUsers;
			Listeners.onResult(listener, getClass(), filter(allUsers));
		}

		private Collection<User> filter(Collection<User> users) {
			Map<Long, User> usersMap = new HashMap<>();
			
			for(User u : users){
				usersMap.put(u.getId(), u);
			}
			
			for(Collaborator collab : collaboratorsAdapter.getItems()){
				User u = collab.getUser();
				if(u != null){
					usersMap.remove(u.getId());
				}
			}

			return usersMap.values();
		}
	}

	private class ContextDetailsActionModeCallback extends BaseActionModeCallback {
		private final CharSequence callbackTitle;

		private boolean editable;

		public ContextDetailsActionModeCallback(CharSequence callbackTitle) {
			super(R.menu.menu_simple_confirm);
			this.callbackTitle = callbackTitle;
			editable = true;
		}

		public boolean isEditable() {
			return editable;
		}
		public void setEditable(boolean editable) {
			if(editable != isEditable()) {
				this.editable = editable;
				if(getActionMode() != null) {
					this.getActionMode().invalidate();
				}
			}
		}

		@Override
        protected boolean onCreateActionMode(BaseActionModeCallback callback, ActionMode mode, Menu menu) {
            super.onCreateActionMode(callback, mode, menu);
            mode.setTitle(callbackTitle);
            return true;
        }

		protected void onDestroyActionMode(BaseActionModeCallback callback, ActionMode mode) {
            if(onFinishListener != null){
                onFinishListener.onFinish(PhotoContextComposeFragment.this);
            }
        }

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			super.onPrepareActionMode(mode, menu);

			menu.setGroupVisible(R.id.menugroup_confirmItems, isEditable());
			menu.setGroupEnabled(R.id.menugroup_confirmItems, isEditable());

			return true;
		}
	}
}
