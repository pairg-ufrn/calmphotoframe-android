package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.view.View;
import android.widget.Checkable;

import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.ItemsSelectionManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderHolderWrapper;

/**
 */
public class CheckSelectedItemDecorator<ItemType> implements ViewBuilderRecyclerAdapter.BinderDecorator<ItemType>,MultiSelectionListener {
    private ItemsSelectionManager selectionManager;
    private ViewBuilderRecyclerAdapter<ItemType> adapter;

    public CheckSelectedItemDecorator(ItemsSelectionManager selectionManager, ViewBuilderRecyclerAdapter<ItemType> adapter) {
        this.selectionManager = selectionManager;
        this.selectionManager.addSelectionListener(this);
        this.adapter = adapter;
    }

    @Override
    public void onBind(BinderHolderWrapper<ItemType> holder, int pos, ViewBuilderRecyclerAdapter<ItemType> adapter) {
        boolean checked = selectionManager.isChecked(adapter.getItemId(pos));
        checkView(holder, checked);
    }

    public static void checkView(BinderHolderWrapper<?> viewHolder, boolean checked) {
        Checkable checkable = null;

        if(viewHolder instanceof Checkable){
            checkable = (Checkable)viewHolder;
        }
        else if(viewHolder.itemView instanceof Checkable){
            checkable = (Checkable)viewHolder.itemView;
        }

        if (checkable != null) {
            checkable.setChecked(checked);
            //TODO: check if is really necessary invalidate the view
            viewHolder.itemView.invalidate();
        }
    }

    @Override
    public void onStartSelection() {

    }

    @Override
    public void onItemSelectionChanged(int selectionCount, int position, long id, boolean checked) {
        //force rebind the view position
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onEndSelection() {

    }
}
