package br.ufrn.dimap.pairg.calmphotoframe.view.actionmode;

import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Command;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommandExecutor;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;

public class BaseActionModeCallback implements ActionMode.Callback {
	private final CommandExecutor cmdExecutor;
	private int menuId;
	private ActionMode actionMode;
    private boolean enabled;

	public BaseActionModeCallback(int menuId) {
		cmdExecutor = new CommandExecutor();
		this.menuId = menuId;
		this.actionMode = null;
        this.enabled = true;
	}

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if(!enabled && actionMode != null){
            finish();
        }
    }

	public ActionMode getActionMode() {
		return actionMode;
	}

	public boolean isActive(){
		return actionMode != null;
	}

	public void finish(){
		if(isActive()){
            actionMode.finish();
		}
	}
	
	/* *************************** ActionMode Callback *********************/
	// Called each time the action mode is shown. Always called after onCreateActionMode, but
    // may be called multiple times if the mode is invalidated.
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false; // Return false if nothing is done
    }

	@Override
	public final boolean onCreateActionMode(ActionMode mode, Menu menu) {
        if(!isEnabled()){
            return false;
        }
		// Inflate a menu resource providing context menu items
        inflateMenu(mode, menu);
        this.actionMode = mode;
        return onCreateActionMode(this, mode, menu);
	}
	
	protected boolean onCreateActionMode(BaseActionModeCallback callback, ActionMode mode, Menu menu){
		return true;
	}
	
 // Called when the user exits the action mode
    @Override
    public final void onDestroyActionMode(ActionMode mode) {
    	this.actionMode = null;
    	onDestroyActionMode(this, mode);
    }
    
	protected void onDestroyActionMode(BaseActionModeCallback callback, ActionMode mode){
	}
	
	// Called when the user selects a contextual menu item
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if(onMenuItemClicked(mode, item)){
        	mode.finish();
        	return true;
        }
        return false;
    }

	protected void inflateMenu(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(menuId, menu);
	}

	public boolean onMenuItemClicked(ActionMode mode, MenuItem item) {
        return executeMenuCommand(item.getItemId());
    }

    public boolean executeMenuCommand(int itemId) {
        return cmdExecutor.execute(itemId);
    }

    public <T> void putMenuCommand(int menuId, Command<T> command){
		this.putMenuCommand(menuId, command, null);
	}
	public <T> void putMenuCommand(int menuId, Command<T> command
				, ResultListener<? super T> resultListener){
		cmdExecutor.putCommand(menuId, command, resultListener);
	}
	public void removeMenuCommand(int menuId){
		cmdExecutor.removeCommand(menuId);
	}
}