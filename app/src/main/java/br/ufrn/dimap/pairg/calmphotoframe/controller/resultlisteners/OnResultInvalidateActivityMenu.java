package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;

public class OnResultInvalidateActivityMenu extends BasicResultListener<Object> {
	private final AppCompatActivity activity;

	public OnResultInvalidateActivityMenu(AppCompatActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onResult(Object result) {
		activity.supportInvalidateOptionsMenu();
	}
}