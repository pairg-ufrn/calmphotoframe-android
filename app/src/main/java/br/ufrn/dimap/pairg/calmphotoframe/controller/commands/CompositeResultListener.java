package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import java.util.LinkedList;
import java.util.List;

public class CompositeResultListener<T> extends BasicResultListener<T>{
	private final List<ResultListener<? super T> > listeners;
	
	@SafeVarargs
	public CompositeResultListener(ResultListener<? super T> ... listeners) {
		this.listeners = new LinkedList<>();
		for(ResultListener<? super T> listener : listeners){
			if(listener != null) {
				this.listeners.add(listener);
			}
		}
	}
	
	public CompositeResultListener<T> add(ResultListener<? super T> listener){
		listeners.add(listener);
		return this;
	}
	public CompositeResultListener<T> remove(ResultListener<? super T> listener){
		listeners.remove(listener);
		return this;
	}
	public boolean contains(ResultListener<? super T> listener){
		return listeners.contains(listener);
	}

	@Override
	public void onStart() {
		for(ResultListener<? super T> listener : listeners){
			Listeners.onStart(listener);
		}
	}

	@Override
	protected void handleResult(T result) {
		for(ResultListener<? super T> listener : listeners){
			Listeners.onResult(listener, result);
		}
	}

	@Override
	protected void handleError(Throwable error) {
		for(ResultListener<? super T> listener : listeners){
			Listeners.onFailure(listener, error);
		}
	}

	@Override
	public boolean isListening() {
		return true;
	}
}
