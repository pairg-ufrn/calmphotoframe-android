package br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.view.ActionMode;
import android.view.View;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.CollectionAndContextContainer;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.PhotoComponentsFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.SelectionTracker;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.ContentViewHelper;

/* *********************************** PhotoCollectionView *************************************/
public class PhotoCollectionView{
	private Activity activity;
	private CollectionAndContextContainer state;
	private PhotoComponentsFragment collectionFragment;

	public PhotoCollectionView(@Nullable Activity activity, @Nullable CollectionAndContextContainer state) {
		this.activity = activity;
		this.state = state;
	}

    public Activity getActivity() {
        return activity;
    }
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public CollectionAndContextContainer getState() {
        return state;
    }
    public void setState(CollectionAndContextContainer state) {
        this.state = state;
    }

    public ItemSelector<PhotoComponent> getComponentsSelector() {
        return getCollectionFragment().getAdapter();
    }

    public void onCreateView(FragmentActivity activity) {
		this.collectionFragment = getFragment(activity);
	}

    public void addOnSelectItemListener(OnSelectItemListener<PhotoComponent> listener){
        getCollectionFragment().addOnSelectItemListener(listener);
    }

    @SuppressWarnings("unused")
    public void removeOnSelectItemListener(OnSelectItemListener<PhotoComponent> listener){
        getCollectionFragment().removeOnSelectItemListener(listener);
    }

    public void setActionModeCallback(ActionMode.Callback callback) {
        getCollectionFragment().getActionModeCompat().setActionModeCallback(callback);
    }
    public void setMultiSelectionListener(MultiSelectionListener selectionListener) {
        getCollectionFragment().getActionModeCompat().setSelectionListener(selectionListener);
    }

	public void addOnClickMenuListener(View.OnClickListener listener){
        this.getCollectionFragment().getFabMenu().addOnClickMenuItemListener(listener);
	}

	@SuppressWarnings("unchecked")
	private PhotoComponentsFragment getFragment(FragmentActivity activity) {
		return (PhotoComponentsFragment) activity.getSupportFragmentManager().findFragmentById(R.id.photoAlbumFragment);
	}
	
	public PhotoComponentsFragment getCollectionFragment() {
		return collectionFragment;
	}
	public void setCollectionFragment(PhotoComponentsFragment collectionFragment) {
		this.collectionFragment = collectionFragment;
	}

	@SuppressWarnings("unused")
    public boolean isSelectionEnabled(){
		return getSelectionTracker().isSelectionEnabled();
	}

    public SelectionTracker getSelectionTracker() {
        return getCollectionFragment().getAdapter().getSelectionManager();
    }

    public void setSelectionEnabled(boolean enabled){
		getSelectionTracker().setSelectionEnabled(enabled);
	}

	public void setOnSearchMode(boolean onSearchMode) {
        getCollectionFragment().setOnSearchMode(onSearchMode);
	}

	public void updateView(CollectionAndContextContainer state) {
		this.state = state;
		showPhotoCollection();
        setSelectionEnabled(state.isEditable());

        getCollectionFragment().getFabMenu().setVisible(state.isEditable());
	}

    protected void showPhotoCollection() {
		setCollection(state.getPhotoComponents());
		
		updateActivityTitle();
	}

	protected void updateActivityTitle() {
		StringBuilder title = new StringBuilder();

		if(state.getPhotoContext() != null 
				&& !ContentViewHelper.hasDefaultName(state.getPhotoContext()))
		{
			title.append(ContentViewHelper.getContextName(activity, state.getPhotoContext()))
				   .append(" : ");
		}
		
		CharSequence collName = getCollectionName(state.getPhotoCollection(), state.getPhotoContext());
		title.append(collName);

        if(!state.isEditable()) {
            title.append(" ");
            title.append(activity.getText(R.string.NonEditable));
        }

		activity.setTitle(title);
	}

	protected CharSequence getCollectionName(PhotoCollection photoCollection, PhotoContext ctx) {
		if(isDefault(photoCollection, ctx)){
			return activity.getString(R.string.default_rootCollectionName);
		}
		else if(photoCollection != null){
			return photoCollection.getName();
		}
		return null;
	}
	
	boolean isDefault(PhotoCollection collection, PhotoContext ctx){
		if(collection == null){
			return true;
		}
		String collName = collection.getName();
		boolean nameIsNullOrEmpty = collName == null || collName.isEmpty();
        return nameIsNullOrEmpty && collectionIsDefault(collection, ctx);
    }

    private boolean collectionIsDefault(PhotoCollection collection, PhotoContext ctx) {
        if(ctx==null){
            return true;
        }

        Long rootColl = ctx.getRootCollectionId();
        return rootColl != null && rootColl.equals(collection.getId());
    }

    public void setCollection(Collection<PhotoComponent> photoComponents) {
        if(hasFragment()) {
            collectionFragment.setCollection(photoComponents);
        }
	}
	public void addPhotoComponent(PhotoCollection photoCollection) {
		PhotoComponent item = state.addPhotoComponent(photoCollection);
        onAddPhotoComponent(item);
	}

	public void onAddPhotoComponent(PhotoComponent item) {
        if(hasFragment()){
            collectionFragment.getAdapter().items().addItem(item);
            collectionFragment.scrollToItem(item);
        }
	}

	protected boolean hasFragment() {
		return collectionFragment != null;
	}
}
