package br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

@Deprecated
public class ViewHider{
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #autoHideDellayMillis} milliseconds.
	 */
	private boolean autoHide;

	/**
	 * If {@link #autoHide} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private int autoHideDellayMillis;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private boolean toggleOnClick;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private int hiderFlags;

	
	private int animationTime;

	private boolean animateOnHide;
	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	

	final View controlsView;
	final View contentView;
	
	public ViewHider(View fullscreenContent, View fullscreenControl){
		contentView = fullscreenContent;
		controlsView = fullscreenControl;
		autoHide = true;
		autoHideDellayMillis = 3000;
		toggleOnClick = true;
		hiderFlags = SystemUiHider.FLAG_HIDE_NAVIGATION;
		animationTime = 300;
	}
	
	public void setup(Activity activity, View anchorView){
		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(activity, contentView,
				hiderFlags);
		mSystemUiHider.setup();
		mSystemUiHider
				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
					// Cached values.
//					int mControlsHeight;
//					int mShortAnimTime;

					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(final boolean visible) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
							ObjectAnimator scale = ObjectAnimator.ofFloat(controlsView, "scaleY", visible ? 1.0f : 0)
									.setDuration(animationTime);
							ObjectAnimator translation = ObjectAnimator.ofFloat(controlsView, "translationY", 
									visible ? 0 : controlsView.getHeight()).setDuration(animationTime);
							
							AnimatorSet animatorSet = new AnimatorSet();
							animatorSet.play(scale).with(translation);
							animatorSet.addListener(new AnimatorListenerAdapter() {
								@Override
								public void onAnimationEnd(Animator animation){
									controlsView.setVisibility(visible ? View.VISIBLE
											: View.GONE);
								}
							});
							animatorSet.start();
							
						} else {
							// If the ViewPropertyAnimator APIs aren't
							// available, simply show or hide the in-layout UI
							// controls.
							controlsView.setVisibility(visible ? View.VISIBLE
									: View.GONE);
						}

						if (visible && autoHide) {
							// Schedule a hide().
							mSystemUiHider.delayedHide(autoHideDellayMillis);
						}
					}
				});
		

		// Set up the user interaction to manually show or hide the system UI.
		contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (toggleOnClick) {
					mSystemUiHider.toggle();
				} else {
					mSystemUiHider.show();
				}
			}
		});
	}

	public synchronized void delayedHide(int milliseconds){
		Log.d(this.getClass().getPackage().getName(),"Hide " + Integer.toString(milliseconds) + " after");
		mSystemUiHider.delayedHide(milliseconds);
	}
	public synchronized void delayedShow(int milliseconds){
		mSystemUiHider.delayedShow(milliseconds);
	}
	public synchronized void delayedToggle(int milliseconds){
		mSystemUiHider.delayedToggle(milliseconds);
	}
	public synchronized void hide(){
		mSystemUiHider.hide();
	}
	public synchronized void show(){
		mSystemUiHider.show();
	}
	public synchronized void toggle(){
		mSystemUiHider.toggle();
	}

	public int getAutoHideDellayMillis()		 { return this.autoHideDellayMillis;}
	public boolean isEnabledAutoHide()			 { return this.autoHide;}
	public void setAutoHide(boolean autoHide)    { this.autoHide = autoHide;}
	public boolean isEnableToggleOnClick()		 { return toggleOnClick; }
	public void setToggleOnClick(boolean toggle) { toggleOnClick = toggle;}

	public boolean isAnimateOnHide() 					{ return animateOnHide;}
	public void setAnimateOnHide(boolean animateOnHide) { this.animateOnHide = animateOnHide;}

	public View.OnClickListener createDelayHideClickListener(View.OnClickListener clickListener){
		return new DelayHideListener(this, clickListener,null);
	}
	//FIXME: Bugs no touch do LinearLayout 
//	public View.OnTouchListener createDelayHideTouchListener(View.OnTouchListener touchListener){
//		return new DelayHideListener(this, null, touchListener);
//	}

    
    void autoHide(){
		if (isEnabledAutoHide()) {
			delayedHide(autoHideDellayMillis);
		}
    }
    
    public class DelayHideListener implements View.OnClickListener, View.OnTouchListener{
    	final View.OnClickListener delegatedClickListener;
    	final View.OnTouchListener delegatedTouchListener;
    	final ViewHider viewHider;
    	
    	public DelayHideListener(ViewHider hider, 
    			View.OnClickListener clickListener, 
    			View.OnTouchListener touchListener)
    	{
    		viewHider = hider;
    		delegatedClickListener = clickListener;
    		delegatedTouchListener = touchListener;
    	}

		@Override
		public void onClick(View view) {
			Log.d(this.getClass().getPackage().getName(), "View clicked: " + view.toString());
			viewHider.autoHide();
			if(delegatedClickListener != null){
				delegatedClickListener.onClick(view);
			}
		}

		@Override
		public boolean onTouch(View view, MotionEvent event) {
			Log.d(this.getClass().getPackage().getName(), "View touched: " + view.toString());
			viewHider.autoHide();
			if(delegatedTouchListener != null){
				return delegatedTouchListener.onTouch(view, event);
			}
			else{
				return false;
			}
		}
    }
}
