package br.ufrn.dimap.pairg.calmphotoframe.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import br.ufrn.dimap.pairg.calmphotoframe.activities.SampleActivity;

/**
 */
public class AutoFitGridLayoutManager extends GridLayoutManager {
    private int columnWidth;

    public AutoFitGridLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public AutoFitGridLayoutManager(Context context) {
        super(context, 1);
    }

    public AutoFitGridLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, 1, orientation, reverseLayout);
    }

    public AutoFitGridLayoutManager setColumnWidth(int columnWidth) {
        this.columnWidth = columnWidth;
        return this;
    }

    public int getColumnWidth() {
        return columnWidth;
    }

    public int getRealColumnWidth() {
        return getWidth() / getSpanCount();
    }

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        super.onMeasure(recycler, state, widthSpec, heightSpec);
        if (columnWidth > 0) {
            int initSpanCount = getSpanCount();
            int spanCount = Math.max(1, getWidth() / columnWidth);

            if (spanCount != initSpanCount) {
                setSpanCount(spanCount);
                super.onMeasure(recycler, state, widthSpec, heightSpec);
            }
        }
    }
}
