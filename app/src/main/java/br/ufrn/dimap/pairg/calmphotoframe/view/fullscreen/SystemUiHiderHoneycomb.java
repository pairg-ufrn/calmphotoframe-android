package br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

/**
 * An API 11+ implementation of {@link SystemUiHider}. Uses APIs available in
 * Honeycomb and later (specifically {@link View#setSystemUiVisibility(int)}) to
 * show and hide the system UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SystemUiHiderHoneycomb extends SystemUiHiderBase {
	/**
	 * Flags for {@link View#setSystemUiVisibility(int)} to use when showing the
	 * system UI.
	 */
	private int mShowFlags;

	/**
	 * Flags for {@link View#setSystemUiVisibility(int)} to use when hiding the
	 * system UI.
	 */
	private int mHideFlags;

	/**
	 * Flags to test against the first parameter in
	 * {@link android.view.View.OnSystemUiVisibilityChangeListener#onSystemUiVisibilityChange(int)}
	 * to determine the system UI visibility state.
	 */
	private int mTestFlags;

	/**
	 * Whether or not the system UI is currently visible. This is cached from
	 * {@link android.view.View.OnSystemUiVisibilityChangeListener}.
	 */
	private boolean mVisible = true;

	/**
	 * Constructor not intended to be called by clients. Use
	 * {@link SystemUiHider#getInstance} to obtain an instance.
	 */
	@SuppressLint("InlinedApi") 
	/*Embora algumas das flags utilizadas pertençam a APIs superiores a versão suportada por esta classe, 
	 * não terão efeito em APIs inferiores.*/
	protected SystemUiHiderHoneycomb(Activity activity, View anchorView,
			int flags) {
		super(activity, anchorView, flags);

		mShowFlags = View.SYSTEM_UI_FLAG_VISIBLE;
		mHideFlags = View.SYSTEM_UI_FLAG_LOW_PROFILE;
		mTestFlags = View.SYSTEM_UI_FLAG_LOW_PROFILE;

		if (hasFlag(FLAG_FULLSCREEN)) {
			// If the client requested br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen, add flags relevant to hiding
			// the status bar. Note that some of these constants are new as of
			// API 16 (Jelly Bean). It is safe to use them, as they are inlined
			// at compile-time and do nothing on pre-Jelly Bean devices.
			mShowFlags |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN ;
			
			mHideFlags |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_FULLSCREEN ;
		}

		if (hasFlag(FLAG_HIDE_NAVIGATION)) {
			// If the client requested hiding navigation, add relevant flags.
			mShowFlags |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
			mHideFlags |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
			mTestFlags |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
		}
	}

	private boolean hasFlag(int flag) {
		return (mFlags & flag) != 0;
	}

	/** {@inheritDoc} */
	@Override
	public void setup() {
		mAnchorView
				.setOnSystemUiVisibilityChangeListener(mSystemUiVisibilityChangeListener);
	}

	/** {@inheritDoc} */
	@Override
	protected void changeSystemUIVisibility(boolean visible) {
		if (visible) {
			mAnchorView.setSystemUiVisibility(mShowFlags);
		}
		else{
			mAnchorView.setSystemUiVisibility(mHideFlags);
		}
	}

	/** {@inheritDoc} */
	@Override
	public boolean isVisible() {
		return mVisible;
	}


	@Override
	protected void hideActionBar() {
		if(mActivity instanceof AppCompatActivity){
			super.hideActionBar();
		}
		else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			// Pre-Jelly Bean, we must manually hide the action bar
			// and use the old window flags API.
			ActionBar actionBar = mActivity.getActionBar();
			if(actionBar != null){
				actionBar.hide();
			}
			mActivity.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}
	@Override
	protected void showActionBar() {
		if(mActivity instanceof AppCompatActivity){
			super.showActionBar();
		}
		else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			// Pre-Jelly Bean, we must manually show the action bar
			// and use the old window flags API.
			ActionBar actionBar = mActivity.getActionBar();
			if(actionBar != null){
				actionBar.show();
			}
			mActivity.getWindow().setFlags(0,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}
	
	private View.OnSystemUiVisibilityChangeListener mSystemUiVisibilityChangeListener = new View.OnSystemUiVisibilityChangeListener() {
		@Override
		public void onSystemUiVisibilityChange(int vis) {
			// Test against mTestFlags to see if the system UI is visible.
			if ((vis & mTestFlags) != 0) {
				hideActionBar();
				// Trigger the registered listener and cache the visibility
				// state.
				mOnVisibilityChangeListener.onVisibilityChange(false);
				mVisible = false;

			} else {
				mAnchorView.setSystemUiVisibility(mShowFlags);
				showActionBar();

				// Trigger the registered listener and cache the visibility
				// state.
				mOnVisibilityChangeListener.onVisibilityChange(true);
				mVisible = true;
			}
		}

	};
}
