package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.SelectionDialog;

public class CutFromPhotoCollectionCommand extends BasePhotoCollectionCommand<PhotoCollection> {

	public CutFromPhotoCollectionCommand(BaseActivity activity, PhotoComponentSelector selector) {
		super(activity, selector);
	}

	@Override	
	protected void executeItemsCommand(Collection<? extends PhotoCollection> targetCollections,
									   Collection<PhotoComponent> selectedItems,
									   ResultListener<? super PhotoCollection> listener)
	{
		PhotoCollection originContext = getSelector().getCurrentPhotoCollection();
		ApiClient.instance()
			.collections().moveItems(originContext, selectedItems, targetCollections, listener);
	}
	
	@Override
	protected void customizeSelectionDialog(
			SelectionDialog<PhotoCollection> selectAlbumDialog) {
		selectAlbumDialog.setDialogTitle  (getString(R.string.dialog_cutItemsToAlbum_title));
		selectAlbumDialog.hideDialogMessage();
	}
}