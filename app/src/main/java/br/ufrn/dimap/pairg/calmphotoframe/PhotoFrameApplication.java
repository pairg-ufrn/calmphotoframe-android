package br.ufrn.dimap.pairg.calmphotoframe;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

public class PhotoFrameApplication extends Application{
	@SuppressLint("StaticFieldLeak") //Application context is safe to keep
	private static Application context = null;
	
	public static Context getContext(){
		return context;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = this;
	}
}
