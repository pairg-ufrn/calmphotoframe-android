package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import android.graphics.Bitmap;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

/**
 */
public class PhotoImageWrapper {
    private Photo photo;
    private Bitmap image;

    public PhotoImageWrapper(Photo photo, Bitmap image) {
        this.photo = photo;
        this.image = image;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Bitmap getImage() {
        return image;
    }
}
