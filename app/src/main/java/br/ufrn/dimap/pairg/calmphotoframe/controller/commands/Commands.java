package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;


public class Commands {
    public static <T> Command<T> bindListeners(final Command<T> command, ResultListener<? super T> ... listeners){
        final ResultListener<? super T> l = Listeners.<T>join(listeners);

        return new BoundCommand<>(command, l);
    }
    public static <Arg, Result>
    ArgsCommand<Arg, Result> bindArgListeners(
            final ArgsCommand<Arg, Result> command, ResultListener<? super Result> ... listeners)
    {
        final ResultListener<? super Result> l = Listeners.<Result>join(listeners);

        return new BoundArgsCommand(command, l);
    }

    public static class BoundCommand<T> implements Command<T> {
        private Command<T> delegate;
        private ResultListener<? super T> boundListener;

        public BoundCommand(Command<T> command, ResultListener<? super T> l) {
            delegate = command;
            boundListener = l;
        }

        @Override
        public void execute(ResultListener<? super T> listener) {
            if(delegate != null){
                delegate.execute(Listeners.join(boundListener, listener));
            }
        }
    }
    public static class BoundArgsCommand<Arg, Result> implements ArgsCommand<Arg, Result> {
        private ArgsCommand<Arg, Result> delegate;
        private ResultListener<? super Result> boundListener;

        public BoundArgsCommand(ArgsCommand<Arg, Result> command, ResultListener<? super Result> l) {
            delegate = command;
            boundListener = l;
        }

        @Override
        public void execute(Arg value, ResultListener<? super Result> listener) {
            if(delegate != null){
                delegate.execute(value, Listeners.join(boundListener, listener));
            }
        }
    }
}
