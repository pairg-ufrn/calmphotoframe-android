package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

public class FileBitmapDecoder implements BitmapDecoder{
	private String filePath;
	public FileBitmapDecoder(String filePath) {
		this.filePath = filePath;
	}
	@Override
	public Bitmap decodeBitmap(Options options) {
		return BitmapFactory.decodeFile(filePath, options);
	}
	@Override
	public void clear() 
	{}
}