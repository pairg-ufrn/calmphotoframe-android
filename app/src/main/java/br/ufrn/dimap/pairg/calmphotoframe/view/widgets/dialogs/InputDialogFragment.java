package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleDialogFragment.DialogResponseListener;

public class InputDialogFragment extends SimpleDialogFragment implements DialogResponseListener{
	public interface InputResponseListener{
		public boolean onResponse(InputDialogFragment inputDialog, boolean responseIsPositive, String response);
	}
	private static final int DEFAULT_INPUT_LAYOUT = R.layout.dialog_input;
	private static final int DEFAULT_INPUTID = R.id.dialog_defaultInput;
	
	private int inputId;
	private CharSequence inputHint;
	private CharSequence inputText;
	private String responseText;
	private InputResponseListener inputResponseListener;
	private DialogResponseListener externalDialogResponseListener;
	
	public InputDialogFragment() {
		this(DEFAULT_INPUT_LAYOUT, DEFAULT_INPUTID);
	}
	public InputDialogFragment(int layoutId, int inputId) {
		super();
		this.setLayoutId(layoutId);
		this.inputId = inputId;
		super.setDialogResponseListener(this);
	}

	public int getInputId()            { return inputId; }
	public CharSequence getInputHint() { return inputHint; }
	public CharSequence getInputText() { return inputText; }
	
	public void setInputId(int inputId) {
		this.inputId = inputId;
	}
	public void setInputHint(CharSequence inputHint) {
		this.inputHint = inputHint;
	}
	public void setInputText(CharSequence inputText) {
		this.inputText = inputText;
		if(this.responseText == null){
			responseText = inputText.toString();
		}
	}

	@Override
	protected View createView(int layoutId, LayoutInflater inflater, ViewGroup container) {
		View rootView = super.createView(layoutId, inflater, container);

		EditText inputView = getInputView(rootView);
		inputView.setHint(getInputHint());
		inputView.setText(getInputText());
		inputView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				responseText = s.toString();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		return rootView;
	}
	
	public EditText getInputView(){
		return this.getInputView(getView());
	}
	protected EditText getInputView(View rootView){
		if(rootView == null){
			throw new IllegalStateException("getInputView should not be called before the dialog view is created.");
		}
		View inputView = rootView.findViewById(this.inputId);
		if(!(inputView instanceof EditText)){
			String errMessage = "the view with id \"inputId\" should be an EditText, but found: " 
					+ (inputView == null ? null : inputView.getClass().getName());
			throw new ClassCastException(errMessage);
		}
		return (EditText)inputView;
	}
	
	public void setInputResponseListener(InputResponseListener inputResponseListener) {
		this.inputResponseListener = inputResponseListener;
	}
	@Override
	public SimpleDialogFragment setDialogResponseListener(DialogResponseListener dialogResponseListener) {
		this.externalDialogResponseListener = dialogResponseListener;
		return this;
	}
	
	/* ************************************** DialogResponseListener **************************************/
	@Override
	public boolean onResponse(SimpleDialogFragment dialog, boolean positive) {
		boolean close = true;
		if(inputResponseListener != null){
			close &= inputResponseListener.onResponse(this, positive, responseText);
		}
		if(externalDialogResponseListener != null){
			close &= externalDialogResponseListener.onResponse(dialog, positive);
		}
		
		return close;
	}
}
