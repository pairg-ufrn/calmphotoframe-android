package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache;

import android.support.v4.util.LruCache;
//
import android.util.Log;

/* Código adaptado de: https://developer.android.com/training/volley/request.html
 * */
public class MemoryCache<K, V> extends LruCache<K, V> implements Cache<K, V> {
	private SizeCalculator<V> sizeCalculator;
	public MemoryCache(int maxSize) {
		super(maxSize);
	}

	@Override
	public synchronized boolean contains(K key) {
		return super.get(key) != null;
	}

	@Override
	public synchronized V getItem(K key) {
		return super.get(key);
	}
	@Override
	public synchronized void putItem(K key, V value) {
		if(!this.contains(key)){
			int sizeOfValue = sizeOf(key, value);
			
			reserve(sizeOfValue);
			super.put(key, value);
		}
	}

	@Override
	public void reserve(int size) {
		Log.d(getClass().getSimpleName(), "Try reserve " + size + " bytes. With " 
													+ (maxSize() - size()) + " of space");
		
		if(size > maxSize()){
//			String errMsg = "Cannot reserve value (" + size + ") greater than the maximum cache size: " + maxSize();
//			throw new IllegalArgumentException(errMsg);
			Log.w(getClass().getSimpleName(), "trying to reserve space ("
					+ size + ") greater than the maximum (" + maxSize() + ")");
		}
		if(size + this.size() > this.maxSize()){
			this.trimToSize(this.maxSize() - size);
		}
		

		Log.d(getClass().getSimpleName(), "Current space is: "  + (maxSize() - size()));
	}
	@Override
	public void removeItem(K key) {
		super.remove(key);
	}	

	@Override
	public int getMaxSize() {
		return maxSize();
	}
	
	@Override
	protected int sizeOf(K key, V value) {
		if(sizeCalculator != null){
			int sizeOfItem =  sizeCalculator.sizeOf(value);
			return sizeOfItem;
		}
		return super.sizeOf(key, value);
	}

	@Override
	public void setSizeCalculator(SizeCalculator<V> sizeCalculator) {
		this.sizeCalculator = sizeCalculator;
	}

}