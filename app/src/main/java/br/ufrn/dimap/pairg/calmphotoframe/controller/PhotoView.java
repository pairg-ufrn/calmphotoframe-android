package br.ufrn.dimap.pairg.calmphotoframe.controller;


/** Represents an UI component capable of display one Photo*/
public interface PhotoView extends PhotoHolder{
	
	/** Sync the view with possible changes on the currentPhoto*/
	public void refreshView();
}