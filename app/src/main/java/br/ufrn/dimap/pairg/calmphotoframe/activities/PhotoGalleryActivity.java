package br.ufrn.dimap.pairg.calmphotoframe.activities;

import android.content.Intent;
import android.os.Bundle;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photos.PhotoGalleryController;

public class PhotoGalleryActivity extends BaseActivity {

	private static final int INVALID_COLLECTION_ID = -1;
	PhotoGalleryController controller;
	
	public PhotoGalleryActivity() {
		controller = new PhotoGalleryController(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_gallery);

		configureController(controller);
		controller.setup();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		controller.refreshView();
	}

	private void configureController(PhotoGalleryController controller) {
		Intent startIntent = getIntent();
		
		long collectionId = startIntent.getLongExtra(Intents.Extras.PhotoCollectionId, INVALID_COLLECTION_ID);
		if(collectionId != INVALID_COLLECTION_ID){
			controller.setPhotoCollectionId(collectionId);
		}
		
		String queryFilter = startIntent.getStringExtra(Intents.Extras.Query);
		controller.setQueryFilter(queryFilter);
	}
}
