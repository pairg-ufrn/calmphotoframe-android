package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import br.ufrn.dimap.pairg.calmphotoframe.PhotoFrameApplication;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class UserImageLoader implements ImageLoader<User> {
    private Context context;

    public Context getContext() {
        return context;
    }

    public UserImageLoader setContext(Context context) {
        this.context = context;
        return this;
    }

    @Override
    public RequestHandler loadImage(User entity, ResultListener<? super Drawable> listener) {
        return getImage(entity, new OnResultConvertToDrawable(listener));
    }

    @Override
    public RequestHandler loadImage(User entity, final ImageView target) {
        return getImage(entity, new OnResultSetImage(target));
    }

    protected RequestHandler getImage(User entity, ResultListener<? super Bitmap> listener) {
        return ApiClient.instance().users().getProfileImage(entity.getId(), listener);
    }

    private static class OnResultSetImage extends BasicResultListener<Bitmap> {
        private final ImageView target;

        public OnResultSetImage(ImageView target) {
            this.target = target;
        }

        @Override
        public void onResult(Bitmap result) {
            if(target != null){
                target.setImageBitmap(result);
            }
        }
    }

    private class OnResultConvertToDrawable extends ResultListenerDelegator<Bitmap, Drawable> {
        public OnResultConvertToDrawable(ResultListener<? super Drawable> listener) {
            super(listener);
        }

        @Override
        protected Drawable convertResult(Bitmap result) {
            Logging.d(UserImageLoader.class.getSimpleName(), "convert result <" + result + "> to drawable");

            Context context = getContext();
            if(context == null){
                context = PhotoFrameApplication.getContext();
            }

            if(context == null){
                //noinspection deprecation
                return new BitmapDrawable(result);
            }

            return new BitmapDrawable(context.getResources(), result);
        }
    }
}
