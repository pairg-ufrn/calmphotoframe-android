package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class BinderHolderWrapper<T> extends RecyclerView.ViewHolder{
	private BinderViewHolder<T> binderViewHolder;

	public BinderHolderWrapper(ViewGroup viewParent, int viewType, BinderViewHolder<T> buildViewBinder) {
		this(buildViewBinder.buildView(viewParent, viewType), buildViewBinder);
	}
	
	public BinderHolderWrapper(View itemView, BinderViewHolder<T> binderViewHolder) {
		super(itemView);
		this.binderViewHolder = binderViewHolder;
		this.binderViewHolder.setup(itemView);
	}

	public void bind(T entity){
		this.binderViewHolder.bind(entity);
	}

	public BinderViewHolder<T> getBinderViewHolder() {
		return binderViewHolder;
	}
}
