package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.View.OnClickListener;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.utils.CollectionsUtils.PredicateFilter;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.CheckSelectedItemDecorator;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ItemsRecycleAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.ItemsRecycleFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.CheckableLayoutViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.PhotoContextViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.CheckableContainer;

public class PhotoContextsFragment extends ItemsRecycleFragment<PhotoContext> {

	private ActionListener<?> createContextListener;
	private final GetSelectedContextDelegator selectorListenerDelegator;
	private ResultListener<? super PhotoContext> onChangeContextListener;
	
	public PhotoContextsFragment() {
		this.selectorListenerDelegator = new GetSelectedContextDelegator();
//		changeViewBuilder(new ViewBuilderWrapper<PhotoContext>(){
//			@Override
//			protected BinderViewHolder<PhotoContext> buildBinder() {
//				return new PhotoContextViewBinder(selectorListenerDelegator);
//			}
//		});

		setBinderBuilder(new ItemsRecycleAdapter.BinderBuilder<PhotoContext>() {
			@Override
			public BinderViewHolder<PhotoContext> buildViewBinder() {
				return new CheckableLayoutViewBinder<>(
						    new PhotoContextViewBinder(selectorListenerDelegator),
						    CheckableContainer.CheckMode.non_recursive).setLayoutId(R.layout.checkable_layout_item);
			}
		});

//		setFragmentLayout(R.layout.fragment_photocontext);
		getAdapter().getSelectionManager().setSelectOnClick(true);
	}

	public EntityGetter<Long> getContextIdSelector(){
		return selectorListenerDelegator.getDelegator();
	}
	public void setContextIdSelector(EntityGetter<Long> ctxIdSelector){
		selectorListenerDelegator.setDelegator(ctxIdSelector);
		if(getAdapter().items().count() > 0){
			getAdapter().notifyDataSetChanged();
		}
	}
	
	public ActionListener getCreateContextListener() {
		return createContextListener;
	}
	public void setCreateContextListener(ActionListener createContextListener) {
		this.createContextListener = createContextListener;
	}

	public ResultListener<? super PhotoContext> getOnChangeContextListener() {
		return onChangeContextListener;
	}
	public void setOnChangeContextListener(ResultListener<? super PhotoContext> onChangeContextListener) {
		this.onChangeContextListener = onChangeContextListener;
	}
	
	public void refreshItems() {
		getAdapter().notifyDataSetChanged();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.layout_recyclerview_page;
	}

	@Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ItemsRecycleAdapter<PhotoContext> adapter = getAdapter();
        adapter.addDecorator(new CheckSelectedItemDecorator<>(adapter.getSelectionManager(), adapter));
        adapter.setIdGenerator(new PhotoContextIdGenerator());

        getAdapter().getSelectionManager().setMultiSelectionEnabled(false);
    }

    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		FloatingActionButton addContextBtn = ViewUtils.getView(view, R.id.floatingbutton);
        ViewUtils.setVisible(addContextBtn, true);
		addContextBtn.setOnClickListener(new OnClickCreateContextListener());
	}

    private static class PhotoContextIdGenerator implements ItemsRecycleAdapter.IdGenerator<PhotoContext> {
        @Override
        public long getId(PhotoContext item, ItemsManager<PhotoContext> items) {
            return item != null ? item.getId() : 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }


    /* ***********************************************************************************************/
	private class GetSelectedContextDelegator implements EntityGetter<Long> {

		private EntityGetter<Long> delegator;

		public EntityGetter<Long> getDelegator() {
			return delegator;
		}

		public void setDelegator(EntityGetter<Long> delegator) {
			this.delegator = delegator;
		}

		@Override
		public Long getEntity() {
			if(delegator != null){
				return delegator.getEntity();
			}
			return null;
		}
	}

	public static class ContextIdFilter implements PredicateFilter<PhotoContext> {
		private final Long ctxId;

		public ContextIdFilter(Long ctxId) {
			this.ctxId = ctxId;
		}

		@Override
		public boolean verify(PhotoContext item) {
			return item.getId() == ctxId;
		}
	}
	
	private class OnClickCreateContextListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if(getCreateContextListener() != null){
				getCreateContextListener().execute(null);
			}
		}
	}
}
