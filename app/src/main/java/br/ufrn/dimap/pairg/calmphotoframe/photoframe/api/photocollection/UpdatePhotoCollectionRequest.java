package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UpdatePhotoCollectionRequest extends CommonGenericRequest<PhotoCollection>{
	private PhotoCollection photoCollection;
	
	public UpdatePhotoCollectionRequest(ApiConnector apiConnector, 
			PhotoCollection context, ResultListener<? super PhotoCollection> listener)
	{
		super(apiConnector, PhotoCollection.class, listener);
		this.photoCollection = context;
	}

	@Override
	public RequestHandler execute(){
		return postJsonRequest(photoCollection);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().collectionUrl(photoCollection.getId());
	}
}
