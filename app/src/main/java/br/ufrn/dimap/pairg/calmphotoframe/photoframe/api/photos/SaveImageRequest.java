package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos;

import java.io.File;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseApiRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.ProgressListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class SaveImageRequest extends BaseApiRequest<SaveImageRequest.SavedImageResponse> {
	public static class SavedImageResponse{
		private Photo photo;
        private File savedFile;

        public Photo getPhoto() {
            return photo;
        }
        public SavedImageResponse setPhoto(Photo photo) {
            this.photo = photo;
            return this;
        }
        public File getSavedFile() {
            return savedFile;
        }
        public SavedImageResponse setSavedFile(File savedFile) {
            this.savedFile = savedFile;
            return this;
        }
    }

	private Photo photo;
	private File savePath;
	private ProgressListener progressListener;

	public SaveImageRequest(ApiConnector apiConnector, Photo photoToSave, File savePath) {
		super(apiConnector, null);
		this.photo = new Photo(photoToSave);
		this.savePath = savePath;
		this.progressListener = null;
	}

    public SaveImageRequest listener(ResultListener<? super SavedImageResponse> resultListener){
        super.listener(resultListener);
        return this;
    }

    public SaveImageRequest progressListener(ProgressListener listener){
        this.progressListener = listener;
        return this;
    }

	@Override
	public RequestHandler execute(){
		return http().getFile(genUrl(), savePath,
                new SavedImageResultConverter(this.photo, listener), progressListener);
	}

    @Override
    protected String genUrl() {
        return getApiBuilder().imageUrl(photo.getId());
    }

    public static class SavedImageResultConverter extends ResultListenerDelegator<File, SavedImageResponse> {
        private Photo photo;
        public SavedImageResultConverter(Photo photo, ResultListener<? super SavedImageResponse> resultListener) {
            super(resultListener);
            this.photo = photo;
        }

        @Override
        protected SavedImageResponse convertResult(File result) {
            return new SavedImageResponse().setPhoto(this.photo).setSavedFile(result);
        }
    }
}