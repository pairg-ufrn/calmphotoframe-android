package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;

/**
 * Created by noface on 09/05/16.
 */
public class ViewDrawableResultListener extends BasicResultListener<Drawable> {
    ImageView view;
    int onErrorPlaceholder;
    int onLoadingPlaceholder;

    public ViewDrawableResultListener(ImageView view) {
        this.view = view;
        onErrorPlaceholder = 0;
        onLoadingPlaceholder = 0;
    }

    public ViewDrawableResultListener onError(int placeholderRes){
        this.onErrorPlaceholder = placeholderRes;
        return this;
    }
    public ViewDrawableResultListener onLoading(int placeholderRes){
        this.onLoadingPlaceholder = placeholderRes;
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(onLoadingPlaceholder != 0){
            view.setImageResource(onLoadingPlaceholder);
        }
    }

    @Override
    public void onResult(Drawable result) {
        view.setImageDrawable(result);
    }

    @Override
    public void onFailure(Throwable error) {
        if(onLoadingPlaceholder != 0){
            view.setImageResource(onErrorPlaceholder);
        }
    }
}
