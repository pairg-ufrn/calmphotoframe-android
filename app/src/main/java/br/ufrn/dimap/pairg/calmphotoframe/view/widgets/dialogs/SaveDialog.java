package br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs;

import java.io.File;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import br.ufrn.dimap.pairg.calmphotoframe.R;

public class SaveDialog extends DialogFragment {
	public interface SaveDialogResponseListener{
		void onSelectFile(File file);
		void onChoosePath(String currentPath);
		void onCancel();
	}
	private String dialogMessage;
	private String okMessage, cancelMessage;
	private SaveDialogResponseListener dialogResponseListener;
	private String filename;
	private String filepath;
	private EditText filenameView;
	private EditText filepathView;
	private boolean filenameEditable;
	
	public SaveDialog() {
		super();
		this.dialogMessage = "";
		this.okMessage = null;
		this.cancelMessage = null;
		this.dialogResponseListener = null;
		filenameEditable =true;
		filename = "";
		filepath = "";
	}

	public String getDialogMessage() { return dialogMessage; }
	public String getOkMessage() 	 { return okMessage; }
	public String getCancelMessage() { return cancelMessage;}
	public SaveDialogResponseListener getSaveDialogResponseListener() { return dialogResponseListener;}

	public String getFilename() {
		return filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public boolean isFilenameEditable() {
		return filenameEditable;
	}

	public void setFilename(String filename) {
		assertArgumentNotNull(filename, "filename");
		this.filename = filename;
		if(filenameView != null){
			filenameView.setText(filename);
		}
	}

	public void setFilepath(String filepath) {
		assertArgumentNotNull(filepath, "filepath");
		this.filepath = filepath;
		if(filepathView != null){
			filepathView.setText(filepath);
		}
	}

	private void assertArgumentNotNull(Object argument, String argumentName) {
		if(argument == null){
			throw new IllegalArgumentException("Parameter " + argumentName + " should not be null!");
		}
	}

	public void setFilenameEditable(boolean filenameEditable) {
		this.filenameEditable = filenameEditable;
	}

	public void setDialogMessage(String dialogMessage) {
		this.dialogMessage = dialogMessage;
	}
	public void setOkMessage(String okMessage) {
		this.okMessage = okMessage;
	}
	public void setCancelMessage(String cancelMessage) {
		this.cancelMessage = cancelMessage;
	}
	public void setDialogResponseListener(SaveDialogResponseListener dialogResponseListener) {
		this.dialogResponseListener = dialogResponseListener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		if(okMessage == null){
			this.okMessage = getResources().getString(android.R.string.ok);
		}
		if(cancelMessage == null){
			this.cancelMessage = getResources().getString(android.R.string.cancel);
		}
	    View rootView = createView();
	    
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(this.getDialogMessage())
        		.setView(rootView)
               .setPositiveButton(getOkMessage(), new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       notifyResponse(true);
                   }
               })
               .setNegativeButton(getCancelMessage(), new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       notifyResponse(false);
                   }
               })
               .setOnCancelListener(new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
	                    notifyResponse(false);
					}
				});

        return builder.create();

	}

	private View createView() {
		LayoutInflater inflater = getActivity().getLayoutInflater();
	    View rootView = inflater.inflate(R.layout.dialog_savedialog, null);
	    if(filepath.length() == 0){
	    	filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
	    }
	    filenameView = (EditText) rootView.findViewById(R.id.savedialog_filename);
	    filepathView = (EditText) rootView.findViewById(R.id.savedialog_filepath);

	    filenameView.setText(filename);
	    filepathView.setText(filepath);
	    filepathView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(dialogResponseListener !=null){
					dialogResponseListener.onChoosePath(filepath);
				}
			}
		});
	    if(!filenameEditable){
	    	filenameView.setVisibility(View.GONE);
	    }
	    
		return rootView;
	}

	private void notifyResponse(boolean response) {
		if(dialogResponseListener != null){
			if(response){
				if(filenameEditable){
					filename = filenameView.getText().toString();
				}
				dialogResponseListener.onSelectFile(createFile());
			}
			else{
				dialogResponseListener.onCancel();
			}
		}
	}

	private File createFile() {
		return filename == null || filename.length() == 0 ? new File(filepath) : new File(filepath, filename);
	}
}
