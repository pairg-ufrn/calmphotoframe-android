package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

public class Endpoint implements Serializable, Cloneable {
	public static final int    DEFAULT_PORT = 6413;
	public static final String LOCAL_HOST   = "localhost";
    private static final int MIN_VALID_PORT = 1;
	private static final int MAX_VALID_PORT = 65535;
    public static final String DEFAULT_SCHEME = "http";
    protected String serviceScheme;
	protected String hostname;
	protected Integer port;
//	private InetAddress address;

    public static Endpoint fromUrl(String url){
        Endpoint endpoint = new Endpoint();

        try {
            endpoint.setFromUrl(url);
        }
        catch (InvalidUrlException ex){
            return null;
        }

        return endpoint;
    }

	public Endpoint() {
		this(null, null);
	}
    public Endpoint(String hostname) {
        this(hostname, null);
    }
    public Endpoint(String hostname, Integer port) {
        super();
        this.hostname = hostname;
        this.port 			= port;
        this.serviceScheme  = "http";
    }

	public static boolean portIsEmpty(Integer port) {
		return port == null || port.equals(0);
	}

	public static boolean hostnameIsEmpty(String hostname) {
		return hostname == null || hostname.isEmpty();
	}

	public static boolean hostnameIsDefault(String hostname) {
		return LOCAL_HOST.equals(hostname);
	}

	protected static void validateUrl(String hostname) throws URISyntaxException {
		new URI(hostname);
	}

	public void resetHostname() {
		this.hostname = LOCAL_HOST;
//        this.address = null;
	}

	public void resetPort() {
		this.setPort(DEFAULT_PORT);
	}

	public String  getScheme()          { return serviceScheme;}

	public String  getHostname()        { return hostname;}

	public Integer getPort()            { return port;}

	public void setScheme(String scheme) {
		this.serviceScheme = scheme;
	}

	public void setHostname(String hostname) {
		if(!hostnameIsEmpty(hostname)){
			this.hostname = hostname;
//            this.address = null;
		}
		else{
			resetHostname();
		}
	}

	public void setPort(int port) {
		if(!portIsEmpty(port)){
			this.port = port;
		}
		else{
			resetPort();
		}
	}

    public void setFromUrl(String url) throws InvalidUrlException {
        URI uri;

        try {
            uri = new URI(url);

        }
        catch (URISyntaxException ex){
            uri = null;
        }

        try {
            if(uri == null || uri.getHost() == null){
                uri = new URI(DEFAULT_SCHEME, url, null, null);
            }
        } catch (URISyntaxException e) {
            throw new InvalidUrlException("'" + url + "'is not a valid uri or hostname", e);
        }


        Integer port = uri.getPort();
        String hostname = uri.getHost();
        //TODO: get scheme

        if(hostname == null){
            throw new InvalidUrlException("Url " +uri + "has no hostname");
        }

        if(port == -1){
            port = null;
        }

        this.port = port;
        this.hostname = hostname;
    }


	public boolean isLocal(){
//        InetAddress addr = getInetAddress();
//        return addr != null && addr.isLoopbackAddress();
//        throw new RuntimeException("Not implemented");
        if(hostname == null){
            return false;
        }
        if(hostname.equals("localhost")){
            return true;
        }
        InetAddress addr = IPParser.parse(hostname);
        return addr != null && addr.isLoopbackAddress();
	}

//	private InetAddress getInetAddress(){
//        if(this.address == null && getHostname() != null){
//            try{
//                address = InetAddress.getByName(getHostname());
//            } catch (UnknownHostException e) {
//                return null;
//            }
//        }
//        return address;
//    }

	public boolean portIsDefault() {
		return port == null || port.equals(DEFAULT_PORT);
	}

    public boolean isSameHost(Endpoint other) {
        if(other == this){
            return true;
        }

        return other != null
                && other.getHostname() != null
                && other.getHostname().equals(getHostname())
                && other.getPort() != null
                && other.getPort().equals(getPort());
    }

    public void copy(Endpoint other) {
        this.setHostname(other.getHostname());
        this.port = other.getPort(); //Integer is imutable
        this.setScheme(other.getScheme());
    }

    @Override
    public Endpoint clone() {
        Endpoint endpoint = new Endpoint();
        endpoint.copy(this);
        return endpoint;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof Endpoint)){
            return false;
        }

        if(this == o){ //same instance
            return true;
        }

        Endpoint other = (Endpoint) o;
        return checkEquals(this.getHostname(), other.getHostname())
                && checkEquals(getPort(), other.getPort());
    }

    protected boolean checkEquals(Object first, Object second) {
        return (first == null && second == null)
                || (first != null && first.equals(second));
    }

	/* *********************************** Validation ***************************************/
	public static boolean hostIsValid(String hostname) {
		return validateUri(hostname, 1);
	}

	public static boolean portIsValid(int port) {
		return (port >= MIN_VALID_PORT && port <= MAX_VALID_PORT) &&
				validateUri(LOCAL_HOST, port);
	}
    public static boolean validateUri(String hostname, int port){
        return validateUri(DEFAULT_SCHEME, hostname, port);
    }
    public static boolean validateUri(String scheme, String hostname, int port){
        try {
            new URI(scheme, null, hostname, port, "", null, null);
        } catch (URISyntaxException e) {
            return false;
        }
        return true;
    }

    public static class InvalidUrlException extends Exception{

        /** {@inheritDoc} */
        public InvalidUrlException() {
        }

        /** {@inheritDoc} */
        public InvalidUrlException(String message) {
            super(message);
        }


        /** {@inheritDoc} */
        public InvalidUrlException(String message, Throwable cause) {
            super(message, cause);
        }


        /** {@inheritDoc} */
        public InvalidUrlException(Throwable cause) {
            super(cause);
        }
    }
}
