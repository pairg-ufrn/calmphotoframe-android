package br.ufrn.dimap.pairg.calmphotoframe.view.validators;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class LoginValidator implements OnFocusChangeListener{
	private int loginContainerId, passwordContainerId, confirmPassContainerId;
	private EditText loginEditor, passwordEditor, confirmPassEditor;
    private TextInputLayout loginContainer, passwordContainer, confirmPassContainer;

	private boolean loginMode;
	private Context context;
	
	public LoginValidator(int loginContainerId, int passwordContainerId, int confirmPassContainerId) {
		super();
		this.loginContainerId = loginContainerId;
		this.passwordContainerId = passwordContainerId;
		this.confirmPassContainerId = confirmPassContainerId;
	}

	public boolean isLoginMode() {
		return loginMode;
	}

	public void setLoginMode(boolean loginMode) {
		this.loginMode = loginMode;
	}

    public TextInputLayout getLoginContainer() {
        return loginContainer;
    }

	public void bindViews(View rootView){
		this.context = rootView.getContext();

		this.loginContainer = ViewUtils.getView(rootView, loginContainerId);
		this.passwordContainer = ViewUtils.getView(rootView, passwordContainerId);
		this.confirmPassContainer = ViewUtils.getView(rootView, confirmPassContainerId);

        this.loginEditor = setupEditor(loginContainer);
        this.passwordEditor = setupEditor(passwordContainer);
        this.confirmPassEditor = setupEditor(confirmPassContainer);


        loginEditor.addTextChangedListener(new ValidateOnTextChange(loginEditor));
        passwordEditor.addTextChangedListener(new ValidateOnTextChange(passwordEditor));
        confirmPassEditor.addTextChangedListener(new ValidateOnTextChange(confirmPassEditor));
	}

	protected EditText setupEditor(TextInputLayout inputLayout) {
        EditText editor = inputLayout.getEditText();
		
		editor.setOnFocusChangeListener(this);
		
		return editor;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
        TextView txt = (TextView)v;

		if(!hasFocus || txt.getText().length() > 0){
			validate(txt);
		}
        else if(txt.getText().length() == 0){
            clearError(txt);
        }
	}

	public boolean checkLogin() {

		boolean isValid = true;

//		loginEditor.setError(null);
		if(loginEditor.getText().length() == 0){
//            loginEditor.setError(getText(R.string.error_field_required));
            loginContainer.setError(getText(R.string.error_field_required));
			isValid = false;
		}

        loginContainer.setErrorEnabled(!isValid);

		return isValid;
	}
	public boolean checkPassword() {

		boolean isValid = true;
		
//		passwordEditor.setError(null);
		String password = passwordEditor.getText().toString();
		if(password.isEmpty()){
//            passwordEditor.setError(getText(R.string.error_field_required));
            passwordContainer.setError(getText(R.string.error_field_required));
			isValid = false;
		}
		else if(!isLoginMode()){
            int minPassLength = context.getResources().getInteger(R.integer.minimum_password_length);
			if(password.length() < minPassLength){
				String errTemplate = getText(R.string.error_password_min_size).toString();

//                passwordEditor.setError(String.format(errTemplate, minPassLength));
                passwordContainer.setError(String.format(errTemplate, minPassLength));
				isValid = false;
			}
		}
        passwordContainer.setErrorEnabled(!isValid);

		return isValid;			
	}
	public boolean checkPassConfirmation() {
		if(isLoginMode()){
			return true;
		}

		confirmPassEditor.setError(null);

        boolean isValid = true;

		CharSequence confirmPassText = confirmPassEditor.getText().toString();
		if(!confirmPassText.equals(passwordEditor.getText().toString())){
//            confirmPassEditor.setError(getText(R.string.error_password_not_match));
            confirmPassContainer.setError(getText(R.string.error_password_not_match));
			isValid = false;
		}

        confirmPassContainer.setErrorEnabled(!isValid);

		return isValid;
	}



	CharSequence getText(int stringId){
		return context.getText(stringId);
	}
	public boolean validate(){
        boolean isValid = checkLogin();
        isValid &= checkPassword();
        isValid &= checkPassConfirmation();
        return isValid;
	}

	public boolean validate(View v) {
		if(v == null){
			return true;
		}

		if(v.getId() == loginEditor.getId()){
			return checkLogin();
		}
		else if(v.getId() == passwordEditor.getId()){
			return checkPassword();
		}
		else if(v.getId() == confirmPassEditor.getId()){
			return checkPassConfirmation();
		}

        return true;
	}


	public void clearErrors() {
        clearError(loginEditor);
        clearError(passwordEditor);
        clearError(confirmPassEditor);
	}

    public void clearError(@Nullable View v) {
		if(v == null){
			return;
		}

		TextInputLayout container = null;
        if(v.getId() == loginEditor.getId()){
            container = loginContainer;
        }
        else if(v.getId() == passwordEditor.getId()){
            container = passwordContainer;
        }
        else if(v.getId() == confirmPassEditor.getId()){
            container = confirmPassContainer;
        }

		if(container != null){
			container.setError(null);
			container.setErrorEnabled(false);
		}
    }

    private class ValidateOnTextChange implements TextWatcher {
        View view;

        public ValidateOnTextChange(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validate(view);
        }
    }
}