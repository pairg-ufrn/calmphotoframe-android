package br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen;

import java.util.HashSet;
import java.util.Set;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;

//TODO: criar onFullscreenChangeListener
public class FullscreenManager{
	public interface OnChangeFullscreenListener{
		public void onChangeFullscreen(boolean fullscreen);
	}
	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private int hiderFlags;

	
	private int animationTime;

	private boolean animate;
	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	

	final Set<View> fullscreenViews;
	final Set<View> nonFullscreenViews;
	
	private OnChangeFullscreenListener onChangeFullscreenListener;
	
	public FullscreenManager(){
		nonFullscreenViews = new HashSet<>();
		fullscreenViews = new HashSet<>();

		hiderFlags = SystemUiHider.FLAG_HIDE_NAVIGATION;
		animationTime = 300;
	}
	
	public void setup(Activity activity, View anchorView){
		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(activity, anchorView, hiderFlags);
		mSystemUiHider.setup();
		mSystemUiHider.setOnVisibilityChangeListener(
				new SystemUiHider.OnVisibilityChangeListener() {
					
					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(final boolean visible) {
						for(View view : nonFullscreenViews){
							changeViewVisibility(view, visible);
						}

						if(onChangeFullscreenListener != null){
							//Avisa onChangeFullscreenListener da mudança no br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen. Passa '!visible', pois a tela estará
							//em br.ufrn.dimap.pairg.calmphotoframe.view.fullscreen se as barras do sistema estiverem invisiveis
							onChangeFullscreenListener.onChangeFullscreen(!visible);
						}
					}
				});
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	void changeViewVisibility(final View controlsView, final boolean visible){
		if (animate &&
				Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) 
		{
			ObjectAnimator scale = ObjectAnimator.ofFloat(controlsView, "scaleY", visible ? 1.0f : 0)
					.setDuration(animationTime);
			ObjectAnimator translation = ObjectAnimator.ofFloat(controlsView, "translationY", 
					visible ? 0 : controlsView.getHeight()).setDuration(animationTime);
			
			AnimatorSet animatorSet = new AnimatorSet();
			animatorSet.play(scale).with(translation);
			animatorSet.addListener(new AnimatorListenerAdapter() {
				//Quando animação terminar, view é definida como VISIBLE ou GONE
				@Override
				public void onAnimationEnd(Animator animation){
					controlsView.setVisibility(visible ? View.VISIBLE
							: View.GONE);
				}
			});
			animatorSet.start();
			
		} else {
			// If the ViewPropertyAnimator APIs aren't
			// available, simply show or hide the in-layout UI
			// controls.
			controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
		
	}
	
	public synchronized void setFullscreen(boolean fullscreen, int millisecondsDelay){
		if(mSystemUiHider == null){
			return;
		}

		if(fullscreen){
			mSystemUiHider.delayedHide(millisecondsDelay);//Oculta barra de navegação do sistema
		}else{
			mSystemUiHider.delayedShow(millisecondsDelay);//Oculta barra de navegação do sistema
		}
	}

	public synchronized void setFullscreen(boolean fullscreen){
		if(fullscreen){
			mSystemUiHider.hide();//Oculta barra de navegação do sistema
		}else{
			mSystemUiHider.show();//Oculta barra de navegação do sistema
		}
	}
	public synchronized void toggleFullscreen(){
		mSystemUiHider.toggle();
	}
	public synchronized void toggleFullscreen(int millisecondsDelay){
		mSystemUiHider.delayedToggle(millisecondsDelay);
	}
	public synchronized boolean isFullscreen(){
		return !mSystemUiHider.isVisible();
	}

	public void addChild(View view, boolean fullscreenChild){
		if(fullscreenChild){
			this.fullscreenViews.add(view);
			this.nonFullscreenViews.remove(view);
		}
		else{
			this.nonFullscreenViews.add(view);
			this.fullscreenViews.remove(view);
		}
	}
	public void removeChild(View view){
		this.fullscreenViews.remove(view);
		this.nonFullscreenViews.remove(view);
	}

	public boolean isAnimating() 				      { return animate;}
	public void setIsAnimating(boolean animateOnHide) { this.animate = animateOnHide;}

	public OnChangeFullscreenListener getOnChangeFullscreenListener() {
		return onChangeFullscreenListener;
	}
	public void setOnChangeFullscreenListener(OnChangeFullscreenListener onChangeFullscreenListener) {
		this.onChangeFullscreenListener = onChangeFullscreenListener;
	}

	
}
