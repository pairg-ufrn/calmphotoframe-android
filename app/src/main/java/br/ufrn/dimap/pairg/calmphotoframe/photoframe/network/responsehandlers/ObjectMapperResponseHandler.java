package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;

/** A {@link TextHttpResponseHandler} that uses an {@link ObjectMapper} to convert 
 * the received content to an object of type {@code T} */
public class ObjectMapperResponseHandler<T> extends TextListenerResponseHandler {
	public interface ObjectMapperResponseListener<T>{
		void onSuccess(int statusCode, Header[] headers, String message, T parsedObject);
		void onFailure(int statusCode, Header[] headers, String message, Throwable error);
	}
	private ObjectMapper objectMapper;
	private ObjectMapperResponseListener<? super T> listener;
	private Class<? extends T> objClass;

	public ObjectMapperResponseHandler(ObjectMapper objectMapper, Class<? extends T> objClass) 
	{
		this(objectMapper, objClass, null);
	}
	
	public ObjectMapperResponseHandler(ObjectMapper jsonMapper,
			Class<? extends T> objClass, ObjectMapperResponseListener<? super T> listener) 
	{
		super();
		this.objectMapper = jsonMapper;
		this.objClass = objClass;
		this.listener = listener;
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	public ObjectMapperResponseListener<? super T> getListener() {
		return listener;
	}
	public void setListener(ObjectMapperResponseListener<T> listener) {
		this.listener = listener;
	}
	public Class<? extends T> getObjectClass() {
		return objClass;
	}
	public void setObjectClass(Class<? extends T> objClass) {
		this.objClass = objClass;
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, String message) {
		super.onSuccess(statusCode, headers, message);

		if(objClass == null){
			Log.e(ObjectMapperResponseListener.class.getSimpleName(), "Class is null. Message: " + message);
		}

		try {
			T obj = (message == null || message.isEmpty() || objClass == null)
						? null
						: objectMapper.readValue(message, objClass);

			if(listener != null){
				listener.onSuccess(statusCode, headers, message, obj);
			}
		} catch (IOException e) {
			notifyFailure(statusCode, headers, message, e);
		}
	}
	
	@Override
	public void onFailure(int statusCode, Header[] headers, String message, Throwable error) {
		super.onFailure(statusCode, headers, message, error);
		notifyFailure(statusCode, headers, message, error);
	}

	protected void notifyFailure(int statusCode, Header[] headers, String message, Throwable e) {
		if(listener != null){
			listener.onFailure(statusCode, headers, message, e);
		}
	}
}
