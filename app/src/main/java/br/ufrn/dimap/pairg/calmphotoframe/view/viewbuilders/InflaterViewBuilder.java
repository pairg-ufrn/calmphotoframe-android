package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class InflaterViewBuilder implements ViewBuilder{
	private int layoutId;
	
	public InflaterViewBuilder(@LayoutRes int layoutId) {
		this.layoutId = layoutId;
	}
	
	public int getLayoutId() {
		return layoutId;
	}

	@Override
	public View buildView(Object data, View recycleView, ViewGroup parent) {
		View v = recycleView;
		if(v == null){
			v = LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false);
		}
		
		setupView(v, data);
		
		return v;
	}

	protected void setupView(View view, Object data){}
}
