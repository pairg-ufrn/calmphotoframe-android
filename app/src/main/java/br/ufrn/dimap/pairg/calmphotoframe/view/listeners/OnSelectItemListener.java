package br.ufrn.dimap.pairg.calmphotoframe.view.listeners;

public interface OnSelectItemListener<Item> {
    void onItemSelection(Item item);
}
