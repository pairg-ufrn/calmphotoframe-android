package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UserActivationRequest extends CommonGenericRequest<UserActivationRequest.ActiveState>{
    private ActiveState activeState;
    private Long userId;

    public UserActivationRequest(ApiConnector apiConnector, Long userId, boolean active) {
        super(apiConnector, ActiveState.class);

        this.userId = userId;
        activeState = new ActiveState(active);
    }

    @Override
    public RequestHandler execute() {
        return postJsonRequest(activeState);
    }

    @Override
    protected String genUrl() {
        return getApiBuilder().userActiveUrl(userId);
    }

    public static class ActiveState{
        public Boolean active;

        public ActiveState() {
            this(null);
        }

        public ActiveState(Boolean active) {
            this.active = active;
        }
    }
}
