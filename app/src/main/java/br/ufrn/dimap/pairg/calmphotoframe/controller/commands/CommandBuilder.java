package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import android.support.annotation.NonNull;

public abstract class CommandBuilder<T> implements Command<T>{

	public void execute() {
		execute(null);
	}
	@Override
	public void execute(ResultListener<? super T> listener) {
		build().execute(listener);
	}

	public abstract @NonNull Command<? extends T> build();
}
