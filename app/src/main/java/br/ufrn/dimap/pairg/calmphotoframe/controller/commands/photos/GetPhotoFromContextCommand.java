package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class GetPhotoFromContextCommand extends BaseStatelessCommand<Photo>{
	private Long photoContextId;
	private boolean include_permission;

	public GetPhotoFromContextCommand() {
		this(LocalPersistence.instance().getCurrentPhotoContext());
	}
	public GetPhotoFromContextCommand(Long contextId) {
		this.photoContextId = contextId;
        this.include_permission = false;
	}

    public boolean getIncludePermission() {
        return include_permission;
    }

    public GetPhotoFromContextCommand setIncludePermission(boolean include_permission) {
        this.include_permission = include_permission;
        return this;
    }

    @Override
	public void execute(ResultListener<? super Photo> resultListener) {
		ApiClient.instance().contexts().getRootCollection(photoContextId,false
				, new GetPhotoFromCollectionResult(getIncludePermission(), resultListener));
		
	}
}
