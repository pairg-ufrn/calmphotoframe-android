package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public class ProtectedEntity<T> {
	private T entity;
	private Permission permission;
	
	public ProtectedEntity() {
		this(null, null);
	}
	public ProtectedEntity(T entity, Permission permission) {
		super();
		this.entity = entity;
		this.permission = permission;
	}

	public T getEntity() {
		return entity;
	}
	public void setEntity(T entity) {
		this.entity = entity;
	}
	public Permission getPermission() {
		return permission;
	}
	public void setPermission(Permission permission) {
		this.permission = permission;
	}
}
