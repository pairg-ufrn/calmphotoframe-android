package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContextVisibility;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoContextVisibilityAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.ViewBuilderAdapter;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.InputDialogFragment;

public class PhotoContextComposeDialog extends InputDialogFragment{

	private ViewBuilderAdapter<PhotoContextVisibility> adapter;
	private ResultListener<? super PhotoContext> resultListener;
	private Spinner visibilitySpinner;
	private TextView nameTxtView;
	private TextInputLayout textInputLayout;
	
	public PhotoContextComposeDialog() {
		super();
		setLayoutId(R.layout.layout_photocontext_create);
		setInputId(R.id.photocontext_inputName);
		
		adapter = new PhotoContextVisibilityAdapter();
	}
	
	public ResultListener<? super PhotoContext> getResultListener() {
		return resultListener;
	}
	public void setResultListener(ResultListener<? super PhotoContext> resultListener) {
		this.resultListener = resultListener;
	}

	@Override
	protected View createView(int layoutId, LayoutInflater inflater, ViewGroup container) {
		View rootView = super.createView(layoutId, inflater, container);
		
		visibilitySpinner = ViewUtils.getView(rootView, R.id.photocontext_visibitySelection);
		visibilitySpinner.setAdapter(adapter);
		
		nameTxtView = ViewUtils.getView(rootView, getInputId());
		nameTxtView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				validate();
			}
		});
		textInputLayout = ViewUtils.getView(rootView, R.id.photocontext_inputNameLayout);
		
		return rootView;
	}

	@Override
	protected boolean notifyResponse(boolean response) {
		if(!validate()){
			return false;
		}
		
		super.notifyResponse(response);
		
		if(response){
			PhotoContext newContext = new PhotoContext();
			newContext.setName(nameTxtView.getText().toString());
			newContext.setVisibility((PhotoContextVisibility)visibilitySpinner.getSelectedItem());
			
			Listeners.onResult(this.resultListener, getClass(), newContext);
		}
		
		return true;
	}

	
	private boolean validate() {
		if(nameTxtView.getText().length() == 0){
			textInputLayout.setError(getText(R.string.error_field_required));
			return false;
		}
		else{
			textInputLayout.setErrorEnabled(false);
		}
		return true;
	}
}
