package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;

public class OnErrorInvalidateContext extends BasicResultListener<Object> {
	private Long expectedCtxId;
	
	public OnErrorInvalidateContext(Long expectedCtxId)
	{
		this.expectedCtxId = expectedCtxId;
	}

	@Override
	public void onFailure(Throwable error) {
		ApiClient.instance().invalidateContext(expectedCtxId);
	}
}
