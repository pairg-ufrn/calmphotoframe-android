package br.ufrn.dimap.pairg.calmphotoframe.activities.events;

import android.content.Context;
import android.text.Html;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class EventsFormatter{
	private Context context;
	private final Map<String, Integer> typeToMessage;
	private CharSequence currentUserName;
	private Long currentUserId;
	
	public EventsFormatter(Context context) {
		this(context, null);
	}
	
	public EventsFormatter(Context context, Long currentUserId) {
		this.context = context;
		typeToMessage = new HashMap<>();
		typeToMessage.put("photocreation", R.plurals.events_photoCreation);
		typeToMessage.put("photoupdate", R.plurals.events_photoUpdate);
		typeToMessage.put("photoremove", R.plurals.events_photoRemove);
		
		currentUserName = getText(R.string.currentUsername);
		this.currentUserId = currentUserId;
	}
	
	public Long getCurrentUserId() {
		return currentUserId;
	}
	public void setCurrentUserId(Long currentUserId) {
		this.currentUserId = currentUserId;
	}

	public Collection<Event> format(Collection<Event> events){
		Collection<Event> groupedEvents = groupEvents(events);
		for(Event evt : groupedEvents){
			format(evt);
		}
		
		return groupedEvents;
	}

	private Collection<Event> groupEvents(Collection<Event> events) {
		Collection<Event> groupedEvents = new LinkedList<>();
		
		Event lastEvent = null;
		for(Event evt : events){
			if(areJoinable(lastEvent, evt)){
				joinEvents(lastEvent, evt);
			}
			else{
				lastEvent = evt.clone();
				groupedEvents.add(lastEvent);
			}
		}
		
		return groupedEvents;
	}

	public void format(Event evt){
		if(evt.getType() != null){
			CharSequence usernameTxt = getUsernameText(evt);
			
			Integer typeMsg = typeToMessage.get(evt.getType().toLowerCase());
			if(typeMsg != null){
				CharSequence msg = getPlural(typeMsg, evt.getGroupedEvents());
				evt.setMessage(Html.fromHtml(String.format(msg.toString(), evt.getGroupedEvents(), usernameTxt)));
			}
		}
	}

	private CharSequence getPlural(Integer typeMsg, int quantity) {
		return context.getResources().getQuantityText(typeMsg, quantity);
	}

	private CharSequence getUsernameText(Event evt) {
		CharSequence username = getUsername(evt);
		
		CharSequence byUsername = getPlural(R.plurals.events_by_username, username == null ? 0 : 1);
		
		return String.format(byUsername.toString(), username == null ? "" : username);
	}
	
	private CharSequence getUsername(Event evt) {
		if(evt.getUserId() != null && evt.getUserId().equals(getCurrentUserId())){
			return currentUserName;
		}

        User user = evt.getUser();

        if(user != null){
            if(!isNullOrEmpty(user.getName())){
                return user.getLogin();
            }
            else if(!isNullOrEmpty(user.getLogin())){
                return user.getLogin();
            }
        }
		
		return null;
	}

    private boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    private CharSequence getText(Integer msgId) {
		return context.getText(msgId);
	}


	private void joinEvents(Event lastEvent, Event evt) {
		Date recentEvent = getMostRecentDate(lastEvent, evt);
		lastEvent.setCreatedAt(recentEvent);
		lastEvent.setGroupedEvents(lastEvent.getGroupedEvents() + 1);
	}

	private Date getMostRecentDate(Event lastEvent, Event evt) {
		Date recentEvent = lastEvent.getCreatedAt() != null 
						   && lastEvent.getCreatedAt().after(evt.getCreatedAt())
						   ? lastEvent.getCreatedAt()
						   : evt.getCreatedAt();
		return recentEvent;
	}
	private boolean areJoinable(Event evt, Event other) {
		if(evt == null || other == null){
			return false;
		}
		
		return evt.getType().equals(other.getType())
				&& isSameUser(evt, other)
				&& dateDiff(evt.getCreatedAt(), other.getCreatedAt(), TimeUnit.MINUTES) <= 5;
	}

	private boolean isSameUser(Event evt, Event other) {
		if(evt.getUserId() != null && other.getUserId() != null){
			return evt.getUserId().equals(other.getUserId());
		}
		else{//only true if both are null
			return evt.getUserId() == other.getUserId();
		}
	}
	
	/** Got from: http://stackoverflow.com/a/10650881
	 * Get a diff between two dates
	 * @param date1 the oldest date
	 * @param date2 the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	private static long dateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    if(diffInMillies < 0){
	    	diffInMillies = date1.getTime() - date2.getTime();
	    }
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
}