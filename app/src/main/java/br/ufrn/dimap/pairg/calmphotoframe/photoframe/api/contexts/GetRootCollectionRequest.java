package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.GetPhotoCollectionRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class GetRootCollectionRequest extends GetPhotoCollectionRequest{
	private Long contextId;

	public GetRootCollectionRequest(ApiConnector apiConnector, Long photoContextId,
			ResultListener<? super PhotoCollection> listener) {
		this(apiConnector, photoContextId, listener, false);
	}
	public GetRootCollectionRequest(ApiConnector apiConnector, Long photoContextId,
			ResultListener<? super PhotoCollection> listener, boolean expandPhotoCollection) {
		super(apiConnector, null, listener, expandPhotoCollection);
		
		this.contextId = photoContextId;
	}


	@Override
	public RequestHandler execute() {
		return getRequest();
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().rootCollectionUrl(contextId, getExpandPhotoCollection());
	}
}