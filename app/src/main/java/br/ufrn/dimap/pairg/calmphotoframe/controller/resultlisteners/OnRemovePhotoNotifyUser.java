package br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.PhotoHolder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class OnRemovePhotoNotifyUser extends ActivityResultListener<Photo> {
	private PhotoHolder photoHolder;
	
	public OnRemovePhotoNotifyUser(BaseActivity activity, PhotoHolder holder) {
		super(activity);
		this.photoHolder = holder;
	}
	
	@Override
	public void onResult(Photo removedPhoto) {
		getActivity().showUserMessage(R.string.message_removephoto_success);
		if(isHoldingRemovedPhoto(removedPhoto)){
			photoHolder.removeCurrentPhoto();
		}
	}

	protected boolean isHoldingRemovedPhoto(Photo removedPhoto) {
		Photo currentPhoto = photoHolder.getCurrentPhoto();
		return removedPhoto.getId().equals(currentPhoto.getId());
	}

	@Override
	public void onFailure(Throwable cause) {
		getActivity().notifyLocalizedError(cause, R.string.message_removephoto_error);
	}
}