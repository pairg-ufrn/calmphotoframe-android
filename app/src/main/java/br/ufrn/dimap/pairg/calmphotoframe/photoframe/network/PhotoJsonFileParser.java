package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;

public class PhotoJsonFileParser {
	private ObjectMapper jsonObjectMapper;
	
	public PhotoJsonFileParser() {
		jsonObjectMapper = new ObjectMapper();
		jsonObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	public Photo readPhoto(File jsonFile) throws IOException {
		return jsonObjectMapper.readValue(jsonFile, Photo.class);
	}
	
	public void writePhoto(Photo photo, File file) throws IOException
	{
		jsonObjectMapper.writeValue(file, photo);
	}
}
