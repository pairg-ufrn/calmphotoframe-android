package br.ufrn.dimap.pairg.calmphotoframe.utils.image;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

public class StreamBitmapDecoder implements BitmapDecoder{
	private InputStream stream;
	private boolean decoded;
	
	public StreamBitmapDecoder(InputStream inputStream) {
		this(inputStream, Integer.MAX_VALUE);
	}
	public StreamBitmapDecoder(InputStream inputStream, int maxStreamLength) {
		this.stream = new StreamBitmapDecoder.ImutableMarkInputStream(inputStream, maxStreamLength);
		decoded = false;
	}
	@Override
	public Bitmap decodeBitmap(Options options) throws IOException {
		if(decoded){
			stream.reset();
		}
		
		Bitmap decodedBitmap = BitmapFactory.decodeStream(stream, null, options);
		
		this.decoded = true;
		
		return decodedBitmap;
	}
	@Override
	public void clear() {
		try {
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Helper class to avoid bug in decodeStream from Android versions before KITKAT.
	 *  Because, decodeStream function calls inputStream.mark(1024), which overwrite the 
	 *  previous call to mark and prevent the correct reset of stream, when readed more than 1024
	 *  bytes. See http://stackoverflow.com/a/21210681 */
	public static class ImutableMarkInputStream extends BufferedInputStream{
	
		public ImutableMarkInputStream(InputStream in, int markLimit, int size) {
			super(in, size);
			super.mark(markLimit);
		}
	
		public ImutableMarkInputStream(InputStream in, int markLimit) {
			super(in);
			super.mark(markLimit);
		}
		@Override
		public void mark(int readlimit) {
			//Do nothing
		}
	}
}