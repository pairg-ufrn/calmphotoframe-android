package br.ufrn.dimap.pairg.calmphotoframe.view.widgets;

import android.view.View;
import android.view.ViewGroup;

/**
 */
public class ViewHierarchyVisitor {
    public interface  ViewVisitor{
        /** @return true to continue visit or false to interrupt*/
        public boolean onVisit(View view, View parent, View root);
    }

    public void visit(View view, ViewVisitor visitor) {
        visitTree(view, view, view, visitor);
    }

    protected void visitTree(View view, View parent, View root, ViewVisitor visitor) {
        if (!onVisit(view, parent, root, visitor)) {
            return;
        }

        if (!(view instanceof ViewGroup)) {
            return;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        if (viewGroup.getChildCount() == 0) {
            return;
        }
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            visitTree(viewGroup.getChildAt(i), viewGroup, root, visitor);
        }
    }

    private boolean onVisit(View view, View parent, View root, ViewVisitor visitor) {
        return visitor != null ? visitor.onVisit(view, parent, root) : false;
    }
}
