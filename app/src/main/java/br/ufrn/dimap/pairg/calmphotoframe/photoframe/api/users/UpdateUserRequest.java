package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public class UpdateUserRequest extends CommonGenericRequest<User>{
	User userToUpdate;
	
	public UpdateUserRequest(ApiConnector apiConnector, User user, ResultListener<? super User> listener) {
		super(apiConnector, User.class, listener);
		this.userToUpdate = user;
	}

	@Override
	public RequestHandler execute() {
		return postJsonRequest(userToUpdate);
	}

	@Override
	protected String genUrl() {
		return getApiBuilder().userUrl(userToUpdate.getId());
	}

}
