package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CommonListenerDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.CompositeResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.BaseApiRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.CommonGenericRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.InputStreamEntity;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class UpdateProfileImageRequest extends CommonGenericRequest<Object>{

    private ImageContent imageContent;
    private Long currentUserId;

    public UpdateProfileImageRequest(ApiConnector apiConnector, ImageContent imageContent, Long currentUserId) {
        super(apiConnector, Object.class);
        this.imageContent = imageContent;
        this.currentUserId = currentUserId;
    }

    @Override
    public RequestHandler execute() {
        ContentType contentType = ContentType.create(imageContent.getMimeType());
        HttpEntity entity = new InputStreamEntity(imageContent.getImageStream(), contentType);

        return request()
                .listener(buildListener())
                .method(HttpMethods.Post)
                .content(entity).send();
    }

    private ResultListener<? super Object> buildListener() {
        String myUserProfileImageUrl = getApiBuilder().myUserProfileImageUrl();

        @SuppressWarnings("unchecked")
        CompositeResultListener<? super Object> l = Listeners.join(
                this.listener,
                new InvalidateUrlOnResult(myUserProfileImageUrl));

        if(currentUserId != null){
            l.add(new InvalidateUrlOnResult(getApiBuilder().userProfileImageUrl(currentUserId)));
        }

        return l;
    }

    @Override
    protected String genUrl() {
        return getApiBuilder().myUserProfileImageUrl();
    }

    private class InvalidateUrlOnResult extends BasicResultListener<Object> {
        String url;

        public InvalidateUrlOnResult(String url) {
            this.url = url;
        }

        @Override
        public void onResult(Object result) {
            http().invalidateImageCache(url);
        }
    }
}
