package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.ParseException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;

public abstract class CommonGenericRequest<T> extends BaseGenericRequest<T, T> {
	public CommonGenericRequest(ApiConnector apiConnector, Class<? extends T> classObj) {
		this(apiConnector, classObj, null);
	}
	public CommonGenericRequest(ApiConnector apiConnector, Class<? extends T> classObj, ResultListener<? super T> listener) {
		super(apiConnector,classObj, listener);
	}

	@Override
	public abstract RequestHandler execute();

	@Override
	protected abstract String genUrl();

	@Override
	protected T convertToResultType(T responseType) {
		/* Does not need conversion*/
		return responseType;
	}
}
