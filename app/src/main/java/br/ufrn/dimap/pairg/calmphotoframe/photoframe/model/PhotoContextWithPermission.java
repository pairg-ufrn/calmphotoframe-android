package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public class PhotoContextWithPermission extends PhotoContext implements Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Permission permission;

	public PhotoContextWithPermission() {
		super();
	}
	public PhotoContextWithPermission(long contextId, String name, PhotoCollection rootCollection) {
		super(contextId, name, rootCollection);
	}
	public PhotoContextWithPermission(PhotoContext other) {
		super(other);
	}
	public PhotoContextWithPermission(long contextId, String name, Long rootCollectionId,
			PhotoContextVisibility visibility, Permission permission) {
		super(contextId, name, rootCollectionId, visibility);
		this.permission = permission;
	}

    public Permission getPermission() {
        return permission;
    }
	public PhotoContextWithPermission setPermission(Permission userPermission) {
		this.permission = userPermission;
		return this;
	}

	@Override
	public PhotoContextWithPermission clone() {
		PhotoContextWithPermission cloneContext = new PhotoContextWithPermission(this);
		cloneContext.setPermission(getPermission());
		return cloneContext;
	}
}
