package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnChangeViewSizeListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.Size;

/**
 */
public class ImageViewBinder<T> extends InflaterViewBinder<T>{
    int imageViewId;
    ImageView imageView;
    ImageLoader<? super T> loader;
    RequestHandler requestHandler;

    /** If true, forces ImageView to have same width and height (adjusts the height based on width)*/
    boolean squared;

    public ImageViewBinder() {
        this(R.layout.view_thumbnail, R.id.thumbnail_imageview);
    }

    public ImageViewBinder(int layoutId, int imageViewId) {
        super(layoutId);
        this.imageViewId = imageViewId;
    }

    public int getImageViewId() {
        return imageViewId;
    }

    @SuppressWarnings("unused")
    public void setImageViewId(int imageViewId) {
        this.imageViewId = imageViewId;
    }

    public boolean isSquared() {
        return squared;
    }

    @SuppressWarnings("unused")
    public ImageViewBinder<T> setSquared(boolean squared) {
        this.squared = squared;
        return this;
    }

    public ImageLoader<? super T> getLoader() {
        return loader;
    }

    public ImageViewBinder<T> setLoader(ImageLoader<? super T> imageLoader) {
        this.loader = imageLoader;
        return this;
    }

    @Override
    public void setup(View view) {
        imageView = ViewUtils.getView(view, getImageViewId());

        if(isSquared()) {
            imageView.getViewTreeObserver().addOnGlobalLayoutListener(new SquareViewOnChangeSize(imageView));
        }
    }

    @Override
    public void bind(T entity) {
        prepareLoad();

        if(getLoader() != null){
//            requestHandler = getLoader().loadImage(entity, new ImageLoaderListener());
            requestHandler = getLoader().loadImage(entity, imageView);
        }
    }

    protected void prepareLoad() {
        if(requestHandler != null && !requestHandler.isFinished()){
            requestHandler.cancel();
        }
        requestHandler = null;

        if(hasImageView()){
            imageView.setImageResource(android.R.color.transparent);
        }
    }

    protected void onStartLoading() {
        if(hasImageView()){
            imageView.setImageResource(R.color.loading_background_img);
        }
    }
    protected void onReceiveImage(Drawable result) {
        if(hasImageView()){
            imageView.setImageDrawable(result);
        }
        requestHandler = null;
    }

    protected void onLoadFailure() {
        if(hasImageView()){
            imageView.setImageResource(R.color.img_failed);
        }
        requestHandler = null;
    }

    private boolean hasImageView() {
        return imageView != null;
    }

    private class ImageLoaderListener extends BasicResultListener<Drawable> {
        @Override
        public void onStart() {
            onStartLoading();
        }

        @Override
        public void onResult(Drawable result) {
            onReceiveImage(result);
        }

        @Override
        public void onFailure(Throwable error) {
            onLoadFailure();
        }
    }

    public static class SquareViewOnChangeSize extends OnChangeViewSizeListener<View> {
        public SquareViewOnChangeSize(View view) {
            super(view);
        }

        @Override
        protected void onSizeChanged(View view, Size lastSize) {
            if(view.getWidth() != view.getHeight()){
                view.getLayoutParams().height = view.getWidth();
//                view.requestLayout();
            }
        }
    }
}
