package br.ufrn.dimap.pairg.calmphotoframe.photoframe;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperConfiguration {
    public static ObjectMapper build(){
        ObjectMapper jsonObjectMapper = new ObjectMapper();
        jsonObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        jsonObjectMapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
        jsonObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return jsonObjectMapper;
    }

}
