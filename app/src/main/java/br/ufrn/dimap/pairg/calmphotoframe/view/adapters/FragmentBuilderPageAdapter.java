package br.ufrn.dimap.pairg.calmphotoframe.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;

/**
 */
public class FragmentBuilderPageAdapter extends ListPageAdapter {
    private List<NamedFragment> namedFragmentBuilder;
    private FragmentCreator fragmentCreator;

    public FragmentBuilderPageAdapter(FragmentManager fm) {
        namedFragmentBuilder = new ArrayList<>();
        fragmentCreator = new FragmentCreator(fm);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return namedFragmentBuilder.get(position).getName();
    }

    public int addFragment(CharSequence name, FragmentCreator.FragmentBuilder fragmentBuilder){
        namedFragmentBuilder.add(new NamedFragment(fragmentCreator, name, fragmentBuilder));
        int index = namedFragmentBuilder.size() - 1;

        super.addItem(namedFragmentBuilder.get(index).buildFragment());

        return  index;
    }

    public void removeFragment(int index){
        namedFragmentBuilder.remove(index);
        super.removeItem(index);
    }

    static class NamedFragment{
        private CharSequence name;
        private FragmentCreator.FragmentBuilder fragmentBuilder;
        private FragmentCreator fragmentCreator;

        public NamedFragment(FragmentCreator creator, CharSequence name, FragmentCreator.FragmentBuilder builder) {
            this.fragmentCreator = creator;
            this.name = name;
            this.fragmentBuilder = builder;
        }

        public CharSequence getName() {
            return name;
        }

        public Fragment buildFragment() {
            return fragmentCreator.createFragment(getName().toString(), fragmentBuilder);
        }
    }
}