package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityResultListener;

public class OnResultRefreshView extends ActivityResultListener<Object>{
	UsersManagementController controller;
	public OnResultRefreshView(BaseActivity activity, UsersManagementController controller) {
		super(activity);
		this.controller = controller;
	}
	
	@Override
	public void onResult(Object result) {
		controller.requestData();
	}
}