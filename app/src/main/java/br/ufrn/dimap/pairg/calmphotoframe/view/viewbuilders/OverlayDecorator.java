package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public class OverlayDecorator<T> extends InflaterViewBinder<T>{
	
	private BinderViewHolder<T> delegate;
	private boolean enabled;

	private FrameLayout overlayContainer;
	private View overlay;
	private int overlayColor;
	
	private int viewType;
	
	public OverlayDecorator(BinderViewHolder<T> delegateViewBinder) {
		this(delegateViewBinder, R.color.overlay_disabled);
	}
	public OverlayDecorator(BinderViewHolder<T> delegateViewBinder, int overlayColor) {
		super(R.layout.view_overlay);
		this.delegate = delegateViewBinder;
		this.overlayColor = overlayColor;
		this.viewType = 0;
	}

	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		overlay.setVisibility(enabled ? View.GONE : View.VISIBLE);
	}
	
	@Override
	public View buildView(ViewGroup viewParent, int viewType) {
		this.viewType = viewType;
		return super.buildView(viewParent, viewType);
	}
	@Override
	public void setup(View itemView) {
		enabled = true;

		this.overlay = ViewUtils.getView(itemView, R.id.overlay_top);
		this.overlay.setBackgroundColor(overlayColor);
		this.overlay.setVisibility(View.GONE);
		
		this.overlayContainer = ViewUtils.getView(itemView, R.id.overlay_content);
		
		View delegateView = delegate.buildView(overlayContainer, this.viewType);
		
		if(delegateView.getParent() == null){
			overlayContainer.addView(delegateView);
		}
		
		delegate.setup(delegateView);
	}
	
	@Override
	public void bind(T entity) {
		delegate.bind(entity);
	}
	
	public BinderViewHolder<T> getDelegate() {
		return delegate;
	}
	public FrameLayout getOverlayContainer() {
		return overlayContainer;
	}
	public View getOverlay() {
		return overlay;
	}
	public int getOverlayColor() {
		return overlayColor;
	}
}
