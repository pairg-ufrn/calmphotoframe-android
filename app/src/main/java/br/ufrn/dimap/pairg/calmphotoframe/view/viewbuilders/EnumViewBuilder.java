package br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;

public abstract class EnumViewBuilder<T> extends InflaterViewBuilder {
	private int viewId;
	
	public EnumViewBuilder() {
		this(android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);
	}
	public EnumViewBuilder(int layoutId, int viewId) {
		super(layoutId);
		this.viewId = viewId;
	}

	@Override
	protected void setupView(View view, Object data) {
		TextView txtView = ViewUtils.getView(view, viewId);
		if(txtView != null){
			@SuppressWarnings("unchecked")
			CharSequence enumText = enumToText(view.getContext(), (T)data);
			txtView.setText(enumText == null ? "" : enumText);
		}
	}

	protected CharSequence enumToText(Context context, T data){
		return getTextFromId(context, data);
	}
	
	protected CharSequence getTextFromId(Context context, T item) {
		int textId = enumToTextId(item);
		return textId == 0 ? null : context.getText(textId);
	}
	protected int enumToTextId(T data){
		return 0;
	}
	
}