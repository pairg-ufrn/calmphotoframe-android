package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextComposeDialog;

public class CreatePhotoContextCommand extends ActivityStatelessCommand<PhotoContext> {
	private static final String CONTEXTCREATION_DIALOG_TAG = CreatePhotoContextCommand.class.toString() + ".Dialog";

	public CreatePhotoContextCommand(BaseActivity activity) {
		super(activity);
	}

	@Override
	public void execute(final ResultListener<? super PhotoContext> listener) {
		PhotoContextComposeDialog dialogFragment = new PhotoContextComposeDialog();
    	dialogFragment.setDialogMessage(getString(R.string.dialog_createContext_title));
    	dialogFragment.setOkMessage(getString(R.string.dialog_createContext_okMessage));
    	dialogFragment.setCancelMessage(getString(R.string.dialog_defaultCancel));
    	dialogFragment.setResultListener(new BasicResultListener<PhotoContext>(){
    		@Override
    		public void onResult(PhotoContext result) {
    			if(result != null){
    				ApiClient.instance()
									.contexts().create(result, listener);
    			}
    		}
    	});
    	dialogFragment.show(getActivity().getSupportFragmentManager(), CONTEXTCREATION_DIALOG_TAG);
	}
	
	String getString(int stringId){
		return getContext().getString(stringId);
	}
}