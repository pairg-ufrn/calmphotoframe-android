package br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.responsehandlers;

import java.net.URI;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;

public class AsyncResponseHandlerDelegator extends AsyncHttpResponseHandler {
	final AsyncHttpResponseHandler delegate;
	
	public AsyncResponseHandlerDelegator(AsyncHttpResponseHandler delegator) {
		this.delegate = delegator;
		this.delegate.setUseSynchronousMode(this.getUseSynchronousMode());
	}
	@Override
	public Header[] getRequestHeaders() {
		return delegate.getRequestHeaders();
	}
	@Override
	public URI getRequestURI() {
		return delegate.getRequestURI();
	}
	@Override
	public boolean getUseSynchronousMode() {
		return delegate.getUseSynchronousMode();
	}

	@Override
	public void onPostProcessResponse(ResponseHandlerInterface arg0,
			HttpResponse arg1)
	{
		delegate.onPostProcessResponse(arg0, arg1);
	}

	@Override
	public void onPreProcessResponse(ResponseHandlerInterface arg0,
			HttpResponse arg1) {
		delegate.onPreProcessResponse(arg0, arg1);
	}

	@Override
	public void setRequestHeaders(Header[] arg0) {
		super.setRequestHeaders(arg0);
		delegate.setRequestHeaders(arg0);
	}

	@Override
	public void setRequestURI(URI arg0) {
		super.setRequestURI(arg0);
		delegate.setRequestURI(arg0);
	}

	@Override
	public void setUseSynchronousMode(boolean arg0) {
		super.setUseSynchronousMode(arg0);
		if(delegate != null){
			delegate.setUseSynchronousMode(arg0);
		}
	}
	@Override
	public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
		delegate.onFailure(arg0, arg1, arg2, arg3);
	}
	@Override
	public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
		delegate.onSuccess(arg0, arg1, arg2);
	}
	

}
