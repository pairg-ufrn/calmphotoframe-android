package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import java.net.URI;
import java.net.URISyntaxException;

import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import cz.msebera.android.httpclient.client.utils.URIBuilder;

public class ApiRouteBuilder {
    EntityGetter<? extends  Endpoint> endpointGetter;

    public ApiRouteBuilder() {
        this(null);
    }
	public ApiRouteBuilder(EntityGetter<? extends  Endpoint> endpointGetter) {
		this.endpointGetter = endpointGetter;
	}
	
	public Endpoint getEndpoint() {
		return endpointGetter == null ? null : endpointGetter.getEntity();
	}
	public void setEndpointGetter(EntityGetter<? extends  Endpoint> endpointGetter) {
		this.endpointGetter = endpointGetter;
	}

	/* ************************************** URL Generation *************************************************/
	public String loginUrl() {
		return loginUrl(getEndpoint());
	}
	public String loginUrl(Endpoint endpoint) {
		return genUrl(endpoint, Paths.login());
	}
	
	public String logoffUrl() {
		return logoffUrl(getEndpoint());
	}
	public String logoffUrl(Endpoint currentEndpoint) {
		return genUrl(currentEndpoint, Paths.logoff());
	}
	
	public String checkConnectionUrl() {
		return checkConnectionUrl(getEndpoint());
	}
	public String checkConnectionUrl(Endpoint endpoint) {
		return genUrl(endpoint, Paths.isConnected());
	}
	
	/* Photos **/
	public String photosUrl() {		
		return photosUrl(getEndpoint());
	}
	public String photosUrl(Endpoint endpoint) {
		return genUrl(endpoint, Paths.photos());
	}

	public String photoUrl(long photoId) {
		return photoUrl(photoId, false);
	}
    public String photoUrl(long photoId, boolean includePermission) {
        return photoUrl(getEndpoint(), photoId, includePermission);
    }
    public String photoUrl(Endpoint endpoint, long photoId, boolean includePermission) {
        URIBuilder builder =  genUrlBuilder(endpoint, Paths.photos(), Paths.photo(photoId));

        if(includePermission){
            builder.addParameter(Parameters.includePermission, String.valueOf(includePermission));
        }

        return build(builder).toString();
    }
	
	public String imageUrl(long photoId) {
		return imageUrl(getEndpoint(), photoId);
	}
	public String imageUrl(Endpoint endpoint, long photoId) {
		return genUrl(endpoint,
				Paths.photos(), Paths.photo(photoId), Paths.image());
	}
	
	public String photosInDefaultContextUrl(){
		return photosInContextUrl(getEndpoint(), null);
	}
	public String photosInContextUrl(Endpoint info, Long contextId) {
		Endpoint endpoint = endpointOrDefault(info);
		
		return  genUrl(endpoint
				, Paths.contexts(), Paths.context(contextId, endpoint), Paths.photos());
	}

	public String collectionPhotosUrl(Long photoCollectionId) {
		return collectionPhotosUrl(photoCollectionId, false);
	}
	public String collectionPhotosUrl(Long collectionId, boolean recursive) {
		return collectionPhotosUrl(collectionId, recursive, null);
	}
	public String collectionPhotosUrl(Long collectionId, boolean recursive, String query) {
		URIBuilder builder = genUrlBuilder(getEndpoint()
				, Paths.collections()
				, Paths.collectionOrRoot(collectionId)
				, Paths.photos());
		
		if(recursive){
			builder.addParameter(Parameters.recursive, String.valueOf(recursive));
		}
		if(query != null && !query.isEmpty()){
			builder.addParameter(Parameters.query, query);
		}
		
		return build(builder).toString();
	}
	
	public String thumbnailUrl(Integer id) {
		return thumbnailUrl(getEndpoint(), id);
	}
	public String thumbnailUrl(Endpoint endpoint, Integer id) {
		return genUrl(endpoint,
				Paths.photos(), Paths.photo(id), Paths.thumbnail());
	}

	/* Collections **/
	public String collectionsUrl() {
		return genUrl(getEndpoint(), Paths.collections());
	}
	public String collectionsByContextUrl(Long contextId) {
		return genUrl(getEndpoint()
				, Paths.contexts(), Paths.context(contextId, getEndpoint())
				, Paths.collections());
	}
	
	public String collectionUrl(long collectionId) {
		return genUrl(getEndpoint(), Paths.collections(), String.valueOf(collectionId));
	}
	public String collectionUrl(Long collectionId, boolean expandPhotos) {
		URIBuilder builder = genUrlBuilder(getEndpoint()
				, Paths.collections(), Paths.collectionOrRoot(collectionId));
		addExpandItemsParam(builder, expandPhotos);
		
		return build(builder).toString();
	}

    public String bulkCollectionsOpUrl() {
		return bulkCollectionsOpUrl(null);
	}
	public String bulkCollectionsOpUrl(Long ctxId) {
		return genUrl(getEndpoint()
				, Paths.contexts(), Paths.context(ctxId, getEndpoint())
				, Paths.collections(), Paths.bulkCollectionsOp());
	}
	public String moveCollectionItemsUrl(long sourceCollectionId) {
		return moveCollectionItemsUrl(sourceCollectionId, null);
	}
	public String moveCollectionItemsUrl(long sourceCollectionId, Long contextId) {
		return genUrl(getEndpoint()
				, Paths.contexts(), Paths.context(contextId, getEndpoint())
				, Paths.collections(), String.valueOf(sourceCollectionId), "move-items");
	}
	

	/* Contexts */
	public String contextsUrl() {
		return genUrl(getEndpoint(), Paths.contexts());
	}
	public String contextUrl(Long ctxId) {
		return contextUrl(ctxId, null);
	}
	public String contextUrl(Long ctxId, Boolean includePermission) {
		URIBuilder builder = genUrlBuilder(getEndpoint(), Paths.contexts(), Paths.context(ctxId, getEndpoint()));
		if(includePermission != null) {
            builder.addParameter(Parameters.includePermission, includePermission.toString());
		}

		return builder.toString();
	}
	public String contextCollaboratorsUrl(Long ctxId){
		return genUrl(getEndpoint(), Paths.contexts()
				, Paths.context(ctxId, getEndpoint()), Paths.collaborators());
	}
	
	public String rootCollectionUrl(Long contextId, boolean expandItems) {
		URIBuilder builder = genUrlBuilder(getEndpoint()
				, Paths.contexts(), Paths.context(contextId, getEndpoint())
				, Paths.collections(), Paths.rootCollection());
		addExpandItemsParam(builder, expandItems);
		return builder.toString();
	}

	/* ************************************ Users ******************************************/
	public String usersUrl() {
		return usersUrl(null);
	}
	public String usersUrl(Endpoint endpoint, String... paths) {
		return genUrl(endpointOrDefault(endpoint), Paths.users(), joinPath(paths));
	}
    public String userUrl(long userId, String... paths) {
        return userUrl(getEndpoint(), userId, paths);
    }
    public String userUrl(Endpoint endpoint, long userId, String ... paths) {
        return usersUrl(endpoint, String.valueOf(userId), joinPath(paths));
    }

    public String myUserUrl() {
        return myUserUrl(null);
    }
    public String myUserUrl(Endpoint endpoint, String ... paths) {
        return genUrl(endpointOrDefault(endpoint), Paths.users(), Paths.myUser(), joinPath(paths));
    }

	public String myPasswordUrl() {
		return myUserUrl(getEndpoint(), Paths.userPassword());
	}

    public String userActiveUrl(Long id) {
        return userUrl(id, Paths.userActiveFieldPath());
    }

    public String myUserProfileImageUrl() {
        return myUserUrl(getEndpoint(), Paths.profileImage());
    }
    public String userProfileImageUrl(long userId) {
        return userUrl(getEndpoint(), userId, Paths.profileImage());
    }

	/* ************************************ Events ******************************************/
	
	public String eventsUrl() {
		return genUrl(getEndpoint(), Paths.events());
	}


    /* ************************************** Helper Methods *************************************************/
	static class Paths{
		public static String thumbnail() {
			return "thumbnail";
		}
		public static String myUser() {
			return "me";
		}
		public static String users() {
			return "users";
		}
		public static String events() {
			return "events";
		}
		public static String collectionOrRoot(Long collectionId) {
			return (collectionId != null) 
					? String.valueOf(collectionId) 
					: rootCollection();
		}
		public static String photo(long photoId) {
			return String.valueOf(photoId);
		}
		public static String rootCollection() {
			return "root";
		}
		public static String image() {
			return "image";
		}
		public static String photos() {
			return "photos";
		}
		public static String collections() {
			return "collections";
		}
		public static String contexts() {
			return "contexts";
		}
		public static String context(Long contextId, Endpoint info){
			contextId = contextIdOrCurrent(contextId, info);
			return contextId != null ? String.valueOf(contextId) : "default";
		}
		public static String collaborators() {
			return "collaborators";
		}
		public static String login() {
			return "login";
		}
		public static String logoff() {
			return "logoff";
		}
		public static String isConnected() {
			return "isconnected";
		}
		public static String bulkCollectionsOp() {
			return "bulk";
		}

		public static Long contextIdOrCurrent(Long contextId, Endpoint endpoint) {
            if(contextId != null){
                return contextId;
            }
            else if(endpoint instanceof ApiEndpoint){
                ApiEndpoint info = (ApiEndpoint)endpoint;
                return info.getCurrentContext();
            }

            return null;
		}

        public static String userPassword() {
            return "password";
        }

        public static String profileImage() {
            return "profileImage";
        }

        public static String userActiveFieldPath() {
            return "active";
        }
    }
	
	static class Parameters{
		public static final String includePermission = "include-permission";
		public static final String recursive = "recursive";
		public static final String query = "query";
	}

	protected void addExpandItemsParam(URIBuilder builder, boolean expandItems) {
		if(expandItems){
			builder.addParameter("expand_items", String.valueOf(true));
		}
	}

	public Endpoint endpointOrDefault(Endpoint endpoint) {
		return endpoint == null ? getEndpoint() : endpoint;
	}
	
	private String genUrl(Endpoint endpoint, String ... paths) {
        return build(genUrlBuilder(endpoint, paths)).toString();
	}
	private URIBuilder genUrlBuilder(Endpoint endpoint, String... paths) {
		URIBuilder builder = getBaseUri(endpoint);
        builder.setPath(joinPath(paths));
		return  builder;
	}

    private String joinPath(String... paths) {
        StringBuilder builder = new StringBuilder();

        for (String p : paths){
            if(!p.startsWith("/")){
                builder.append("/");
            }
            builder.append(p);
        }

        return builder.toString();
    }

    protected URIBuilder getBaseUri(Endpoint endpoint) {
		int portInt = (endpoint.getPort() == null ? -1 : endpoint.getPort());
        return getBaseUri(endpoint, portInt);
	}
	protected URIBuilder getBaseUri(Endpoint endpoint, int port) {
        URIBuilder builder = new URIBuilder()
                                .setScheme(endpoint.getScheme())
                                .setPort(port);

        if(endpoint.getHostname() != null){
            builder.setHost(endpoint.getHostname());
        }

        return builder;
	}

    private URI build(URIBuilder builder) {
        try {
            return builder.build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
