package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserProfileBinder;

public class UserProfileFragment extends Fragment {

    public static final String CLASSNAME = UserProfileFragment.class.getName();
    public static final String CHANGEPASSWORD_DIALOG = CLASSNAME + ".CHANGEPASS_DIALOG";

    public static class UserPassword{
        private User user;
        private String password;

        public UserPassword(User user, String password) {
            this.user = user;
            this.password = password;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    User user;
    UserProfileBinder userProfileBinder;

    ActionListener<? super UserPassword> changePasswordAction;

    public UserProfileFragment() {
        userProfileBinder = new UserProfileBinder();
        userProfileBinder.setChangePasswordAction(new ShowChangePasswordDialog(new EntityGetter<FragmentManager>() {
            @Override
            public FragmentManager getEntity() {
                return getActivity() == null ? null : getActivity().getSupportFragmentManager();
            }
        }));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_userprofile, container, false);

        userProfileBinder.setup(rootView);

        if(getUser() != null){
            userProfileBinder.bind(getUser());
        }

        return rootView;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;

        if(getView() != null){
            userProfileBinder.bind(user);
        }
    }

    public ImageLoader<User> getUserImageLoader(){
        return userProfileBinder.getUserImageLoader();
    }
    public void setUserImageLoader(ImageLoader<User> userImageLoader) {
        userProfileBinder.setUserImageLoader(userImageLoader);
    }

    public ActionListener<? super UserPassword> getChangePasswordAction() {
        return changePasswordAction;
    }

    public void setChangePasswordAction(ActionListener<? super UserPassword> changePasswordAction) {
        this.changePasswordAction = changePasswordAction;
    }

    public void setSaveNameCommand(ArgsCommand<User, ? extends User> saveNameCommand) {
        getViewBinder().setSaveNameCommand(saveNameCommand);
    }

    public void setChangePhotoCommand(ArgsCommand<User, ? extends User> changePhotoCommand) {
        getViewBinder().setChangePhotoCommand(changePhotoCommand);
    }

    public UserProfileBinder getViewBinder(){
        return this.userProfileBinder;
    }


    private void changePassword(LoginInfo loginInfo) {
        String password = loginInfo.getPassword();

        if(getChangePasswordAction() != null){
            getChangePasswordAction().execute(new UserPassword(getUser(), password));
        }
    }

    class ShowChangePasswordDialog extends BasicResultListener<ConnectionInfo>
            implements ActionListener<Object>
    {

        EntityGetter<FragmentManager> fragmentManagerGetter;

        ChangePasswordDialog dialogFragment = new ChangePasswordDialog();

        public ShowChangePasswordDialog(EntityGetter<FragmentManager> fragmentManagerGetter) {
            this.fragmentManagerGetter = fragmentManagerGetter;

            dialogFragment.setLoginListener(this);
        }

        @Override
        public void execute(Object value) {
            FragmentManager fragmentManager = fragmentManagerGetter.getEntity();
            if(fragmentManager != null){
                dialogFragment.clear();
                dialogFragment.show(fragmentManager, CHANGEPASSWORD_DIALOG);
            }
        }

        @Override
        public void onResult(ConnectionInfo result) {
            changePassword(result.getLoginInfo());
        }

    }
}
