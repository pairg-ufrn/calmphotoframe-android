package br.ufrn.dimap.pairg.calmphotoframe.view.fragments.photo;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.BaseActionModeCallback;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ItemSelector;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoMarker;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.DrawableUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.PhotoMarkerView;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoMarksNameAdapter.OnChangePhotoMarkerCollectionListener;
import br.ufrn.dimap.pairg.calmphotoframe.view.adapters.PhotoMarksNameAdapter.OnPhotoMarkChangeListener;

public class PhotoMarkerEditorFragment extends Fragment 
									implements PhotoMarkersViewerFragment.PhotoMarkerViewerListener
											 , OnPhotoMarkChangeListener
											 , OnChangePhotoMarkerCollectionListener
{
	private static final String PHOTOMARKS_KEY = PhotoMarkerEditorFragment.class.getName() + ".PHOTOMARKS";
	private static final String MARKERSLIST_TAG = null;

	public enum State {
		AddingMark,
        Default
	}
	
	private final List<PhotoMarker> photoMarks;
	private Bitmap imageCache;

	private State currentState;
	private PhotoMarkersViewerFragment photoMarkersViewerFragment;
	private PhotoMarkerEditableListFragment photoMarkerEditableListFragment;
    private View addMarkerButton;

	private boolean viewsReady;
	
    private EditMarkerContextualCallback actionBarCallback;
	
	public PhotoMarkerEditorFragment(){
		super();
		photoMarks = new ArrayList<>();
		
		photoMarkerEditableListFragment = new PhotoMarkerEditableListFragment();
		
		this.currentState = State.Default;
		this.viewsReady = false;
		
        actionBarCallback = buildActionCallback();
	}

    @Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putSerializable(PHOTOMARKS_KEY, (ArrayList<PhotoMarker>)photoMarks);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
		
	    if (savedInstanceState != null) {
	    	@SuppressWarnings("unchecked")
			List<PhotoMarker> restoredPhotoMarks = (List<PhotoMarker>)savedInstanceState.get(PHOTOMARKS_KEY);
	    	this.setPhotoMarks(restoredPhotoMarks);
	    }
	}

    public PhotoMarkersViewerFragment markersViewer() {
        return photoMarkersViewerFragment;
    }

    /* ***********************************Configuração de Views*******************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photomarkeditor, container, false);
        setupViews(rootView);
        if(this.photoMarks.size() > 0){
        	this.setPhotoMarks(photoMarks);
        }
        return rootView;
    }
	
    private void setupViews(View rootView) {
        rootView.setOnClickListener(new DisableEditClickListener());
    	this.photoMarkersViewerFragment = buildPhotoMarkersViewer();
    	setupEditableNamesList(rootView);

        addMarkerButton = ViewUtils.getView(rootView, R.id.markEditor_addMarkerButton);
        addMarkerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentState(State.AddingMark);
            }
        });

    	viewsReady = true;
	}

	private PhotoMarkersViewerFragment buildPhotoMarkersViewer() {

        PhotoMarkersViewerFragment markersViewer = (PhotoMarkersViewerFragment) getChildFragmentManager()
                .findFragmentById(R.id.photoMarkEditor_markContainer);

        markersViewer.setPhotoMarkers(this.photoMarks);
        markersViewer.setImage(this.imageCache);
    	markersViewer.setOnTouchImageListener(new ImageTouchListener());
    	markersViewer.setPhotoMarkerPositionListener(this);
        //TODO: (?) support show labels on editor (not working now)
    	markersViewer.hideLabels();

        return  markersViewer;
	}

	private void setupEditableNamesList(View rootView) {
		if(getChildFragmentManager().findFragmentByTag(MARKERSLIST_TAG) == null){
			getChildFragmentManager().beginTransaction()
									 .add(R.id.photoMarkEditor_editableMarkerList,photoMarkerEditableListFragment, MARKERSLIST_TAG)
									 .commit();
		}
		photoMarkerEditableListFragment.getPhotoMarkerAdapter().setOnPhotoMarkChangeListener(this);
		photoMarkerEditableListFragment.getPhotoMarkerAdapter().setOnChangePhotoMarkerCollectionListener(this);
		
		View editableMarkerListContainer = rootView.findViewById(R.id.photoMarkEditor_editableMarkerList);
		editableMarkerListContainer.setOnClickListener(new DisableEditClickListener());
	}
	
	/* **************************************Toolbar******************************************************/

	private void setCurrentState(State state) {
		this.currentState = state;
		
        if(addMarkerButton != null){
			boolean isAdding = state.equals(State.AddingMark);
			addMarkerButton.setSelected(isAdding);
        }
	}

	private State getCurrentState() {
		return currentState;
	}
	
	/* ***********************************Controle de marcadores da imagem*********************/

	public List<PhotoMarker> getPhotoMarks() {
		return Collections.unmodifiableList(this.photoMarks);
	}
	public void setPhotoMarks(List<PhotoMarker> somePhotoMarks) {		
		if(somePhotoMarks != photoMarks){
			this.photoMarks.clear();
			this.photoMarks.addAll(somePhotoMarks);
		}
        if(markersViewer() != null){
            this.markersViewer().setPhotoMarkers(photoMarks);
        }
		if (this.viewsReady) {
			this.photoMarkerEditableListFragment.getPhotoMarkerAdapter().setItems(photoMarks);
		}
	}

	private void addPhotoMarkerAt(float screenX, float screenY) {
		PhotoMarker photoMarker = new PhotoMarker(this.photoMarks.size() + 1);
		this.markersViewer().addPhotoMarkerAt(photoMarker, screenX, screenY);
		this.photoMarks.add(photoMarker);
		updatePhotoNames();
	}
	
	private void removeMarker(PhotoMarker photoMarker) {
		this.markersViewer().removePhotoMarker(photoMarker);
		this.photoMarks.remove(photoMarker);
		updatePhotoNames();
	}

    public void setEditable(boolean editable){
        markersViewer().setEditable(editable);
    }

    /* **************************************************************************************************/
	private void updatePhotoNames() {
		this.photoMarkerEditableListFragment.getPhotoMarkerAdapter().setItems(photoMarks);
	}
	
	/* **************************************************************************************************/
	public void setImage(Bitmap photoImage) {
        this.imageCache = photoImage;
        if(markersViewer() != null) {
            this.markersViewer().setImage(imageCache);
        }
	}

	/* ****************************** PhotoMarkerViewerListener ****************************************/
	@Override
	public void onPhotoMarkerClicked(PhotoMarker photoMarker) {
        selectMarker(photoMarker);

		this.setCurrentState(State.Default);
	}

    protected void selectMarker(PhotoMarker photoMarker) {
        if(!isSelected(photoMarker)) {
            PhotoMarkerView markerView = markersViewer().getMarkerView(photoMarker);
            showContextualActionBar(photoMarker, markerView);
        }
        else{
            finishContextualActionBar();
        }
    }

    /* ****************************** PhotoMarkerChangeListener ****************************************/
	@Override
	public void onChangePhotoMarker(int markIndex, PhotoMarker mark) {
		if(markersViewer().isLabelsVisible()){
			markersViewer().layoutPhotoMarkers();
		}
	}

	/* ****************************** PhotoMarkerChangeListener ****************************************/
	@Override
	public void onAddMarker(PhotoMarker marker, int index) {
		//Ignore
	}

	@Override
	public void onRemoveMarker(PhotoMarker marker, int from) {
		if(this.photoMarks.contains(marker)){
			removeMarker(marker);
		}
	}

	@Override
	public void onReorderMarkers(int initialIndex, int finalIndex) {
		reorderMarkers(this.photoMarkerEditableListFragment.getPhotoMarkerAdapter().getItems());
	}

	private void reorderMarkers(List<PhotoMarker> items) {
		int index =0;
		for(PhotoMarker marker : items){
			marker.setIndex(index + 1);
			++index;
		}
		this.setPhotoMarks(items);
	}

	@Override
	public void onChangeCollection(List<PhotoMarker> newCollection) {
		// Ignore
	}

    /* ************************************* ImageTouchListener ****************************************/
	private final class ImageTouchListener implements OnTouchListener {
		GestureDetectorCompat gestureDetector = new GestureDetectorCompat(getActivity(),createGestureListener());

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			return gestureDetector.onTouchEvent(event);
		}

		OnGestureListener createGestureListener(){
			return new SimpleOnGestureListener(){
				@Override
				public boolean onSingleTapUp(MotionEvent e) {
					if(getCurrentState() == State.AddingMark){
						addPhotoMarkerAt(e.getRawX(), e.getRawY());
					}
					setCurrentState(State.Default);
					return super.onSingleTapUp(e);
				}
				@Override
				public boolean onSingleTapConfirmed(MotionEvent e) {
                    actionBarCallback.tryFinish();
					return super.onSingleTapConfirmed(e);
				}
			};
		}
	}
	private final class DisableEditClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			actionBarCallback.tryFinish();
		}
	}
/* *****************************************************************************************************/
	private void showContextualActionBar(PhotoMarker photoMarker, PhotoMarkerView markerView) {
        actionBarCallback.start((AppCompatActivity) getActivity(), photoMarker, markerView);

	}

    private boolean isSelected(PhotoMarker photoMarker) {
        PhotoMarker selected = actionBarCallback.getSelectedItem();
        return selected != null && selected.equals(photoMarker);
    }

    private void finishContextualActionBar() {
        actionBarCallback.tryFinish();
    }

	/* ******************************* ContextualActionBarCallback *************************************/

    protected EditMarkerContextualCallback buildActionCallback() {
        EditMarkerContextualCallback callback = new EditMarkerContextualCallback();
        callback.putMenuCommand(R.id.action_removeMarker, new RemoveMarkerCommand(callback));

        return callback;
    }

	static class EditMarkerContextualCallback extends BaseActionModeCallback implements ItemSelector<PhotoMarker>{
        private AppCompatActivity activity;
        private PhotoMarker currentMarker;
        private PhotoMarkerView markerView;

        public EditMarkerContextualCallback() {
            super(R.menu.contextualmenu_photomarker_edit);
        }

        public void start(AppCompatActivity activity, PhotoMarker marker){
            start(activity, marker, null);
        }
        public void start(AppCompatActivity activity, PhotoMarker marker, PhotoMarkerView markerView){
            setCurrentMarker(activity, marker, markerView);
            if(this.getActionMode() == null) {
                activity.startSupportActionMode(this);
            }
        }

        @Override
        protected void onDestroyActionMode(BaseActionModeCallback callback, ActionMode mode) {
            super.onDestroyActionMode(callback, mode);

            clearCurrentMarker();
        }

        protected void setCurrentMarker(AppCompatActivity activity, PhotoMarker marker, PhotoMarkerView markerView) {
            if(currentMarker != null){
                clearCurrentMarker();
            }

            currentMarker = marker;
            this.markerView = markerView;
            this.activity = activity;

            highlightMarker(markerView, true);
        }

        protected void highlightMarker(PhotoMarkerView markerView, boolean highlight) {
            if(this.markerView != null && this.markerView.getMarkerDrawable() != null){
                DrawableUtils.highlight(activity, markerView.getMarkerDrawable(), R.color.button_selected, highlight);
            }
        }

        private void clearCurrentMarker() {
            highlightMarker(markerView, false);
            currentMarker = null;
            markerView = null;
            activity = null;
        }

        @Override
        public Collection<? extends PhotoMarker> getSelectedItems() {
            return Collections.singleton(getSelectedItem());
        }

        @Override
        public PhotoMarker getSelectedItem() {
            return currentMarker;
        }

        public void tryFinish() {
            if(getActionMode() != null){
                finish();
            }
        }
    }

    private class RemoveMarkerCommand extends BaseStatelessCommand<PhotoMarker> {
        private ItemSelector<? extends PhotoMarker> photoMarkerSelector;

        public RemoveMarkerCommand(ItemSelector<? extends PhotoMarker> photoMarkerSelector) {
            this.photoMarkerSelector = photoMarkerSelector;
        }

        @Override
        public void execute(ResultListener<? super PhotoMarker> cmdResultListener) {
            PhotoMarker marker = photoMarkerSelector.getSelectedItem();
            removeMarker(marker);

            Listeners.onResult(cmdResultListener, getClass(), marker);
        }
    }
}
