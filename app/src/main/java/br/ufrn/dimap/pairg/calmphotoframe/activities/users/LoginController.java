package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.ConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.LoginConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.SignupConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.LoginSignupFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.OnSelectItemListener;

public class LoginController {

	private BaseActivity activity;
	private LoginSignupFragment loginSignupFragment;

    private ServerInfo localServer;

    private ConnectionCommand<? super Endpoint> loginCommand;
    private ConnectionCommand<? super User> signupCommand;

	public LoginController(final BaseActivity activity) {
		this.activity = activity;
        this.loginCommand = new LoginConnectionCommand().setContext(activity);
        this.signupCommand = new SignupConnectionCommand().setContext(activity);
	}

    /* ******************************* Getters and Setters ****************************************/

	public BaseActivity getActivity() {
		return activity;
	}

    public ConnectionCommand<? super Endpoint> loginCommand() {
        return loginCommand;
    }

    public LoginController loginCommand(ConnectionCommand<? super Endpoint> loginCommand) {
        this.loginCommand = loginCommand;
        return this;
    }

    public ConnectionCommand<? super User> signupCommand() {
        return signupCommand;
    }

    @SuppressWarnings("unused")
    public LoginController signupCommand(ConnectionCommand<? super User> signupCommand) {
        this.signupCommand = signupCommand;
        return this;
    }

    /* ************************************* Life-Cycle *******************************************/

    public void setupView() {
        localServer = new ServerInfo(
                getActivity().getText(R.string.localserver_name).toString(),
                getActivity().getText(R.string.localserver_address).toString());

        loginSignupFragment = (LoginSignupFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        loginSignupFragment.setShowingTitle(true);
        loginSignupFragment.setIsLogin(true);

        loginSignupFragment.setSubmitListener(new OnSubmitLoginOrSignup(getActivity()));
        loginSignupFragment.setRemoveItemListener(new OnSelectItemListener<ServerInfo>() {
            @Override
            public void onItemSelection(ServerInfo serverInfo) {
                LocalPersistence persistence = LocalPersistence.instance();
                persistence.removeConnectedServer(serverInfo);
                loginSignupFragment.setServerInfos(getServerInfos());
            }
        });
    }

	public void refreshView() {
        loginSignupFragment.setServerInfos(getServerInfos());
	}

    /* *************************************** Helpers *********************************************/

    public List<ServerInfo> getServerInfos() {
        List<ServerInfo> servers = LocalPersistence.instance().getConnectedServers();

        ServerInfo serverLocal = findLocalServer(servers);
        if(serverLocal == null) {
            servers.add(localServer);
        }
        else {
            if(serverLocal.getName() == null || serverLocal.getName().isEmpty()){
                serverLocal.setName(getActivity().getText(R.string.localserver_name).toString());
            }
        }

        return servers;
    }

    private ServerInfo findLocalServer(List<ServerInfo> servers) {
        for (ServerInfo server : servers){
            if(server.isLocal()){
                return server;
            }
        }
        return null;
    }

    protected class OnSubmitLoginOrSignup implements LoginSignupFragment.SubmitListener {
        BaseActivity activity;

        OnSubmitLoginOrSignup(BaseActivity activity){
            this.activity = activity;
        }

        @Override
        public void onSubmit(final ConnectionInfo connInfo, final LoginMode loginMode) {
            switch (loginMode){
                case Login:
                    doLogin(connInfo);
                    break;
                case Signup:
                    doSignup(connInfo);
                    break;
            }
        }

        private void doLogin(ConnectionInfo connInfo) {
            loginCommand().execute(connInfo, ConnectionBehaviour.instance().onLogin(activity));
        }

        private void doSignup(ConnectionInfo connInfo) {
            signupCommand().execute(connInfo, ConnectionBehaviour.instance().onSignup(activity));
        }
    }
}
