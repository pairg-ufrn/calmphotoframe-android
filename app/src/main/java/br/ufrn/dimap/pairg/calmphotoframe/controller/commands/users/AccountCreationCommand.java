package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ActivityStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.CreateAccountFragment;

public class AccountCreationCommand extends ActivityStatelessCommand<User>{

	private static final String DIALOG_TAG = AccountCreationCommand.class.getName() + ".DIALOG";

	public AccountCreationCommand(BaseActivity activity) {
		super(activity);
	}

	@Override
	public void execute(ResultListener<? super User> cmdResultListener) {
		if(getActivity().getSupportFragmentManager().findFragmentByTag(DIALOG_TAG) != null){
			return;
		}
		
		CreateAccountFragment frag = new CreateAccountFragment();
		frag.setLoginListener(new OnSignupListener(frag, cmdResultListener));
		frag.show(getActivity().getSupportFragmentManager(), DIALOG_TAG);
	}

	public static class OnSignupListener extends BasicResultListener<ConnectionInfo> {
		ResultListener<? super User> resultListener;
		CreateAccountFragment createAccountFragment;
		
		public OnSignupListener(CreateAccountFragment frag, ResultListener<? super User> resultListener) {
			super();
			this.createAccountFragment = frag;
			this.resultListener = resultListener;
		}

		@Override
		public void onResult(ConnectionInfo result) {
			createAccountFragment.dismiss();
			if(result != null){
				ApiClient.instance().users().create(result.getLoginInfo(), resultListener);
			}
		}
	}
}
