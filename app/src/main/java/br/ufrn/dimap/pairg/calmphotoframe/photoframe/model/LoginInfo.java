package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

public class LoginInfo implements Cloneable{
	private String login;
	private String password;
	
	public LoginInfo() {
		this("","");
	}
	public LoginInfo(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	@Override
	public LoginInfo clone(){
		LoginInfo clone = new LoginInfo();
		//Strings em java são imutáveis, então não precisa clonar
		clone.setLogin(getLogin());
		clone.setPassword(getPassword());
		return clone;
	}
	
	@Override
	public String toString() {
		return "LoginInfo [login=" + login + ", password=" + password + "]";
	}
}
