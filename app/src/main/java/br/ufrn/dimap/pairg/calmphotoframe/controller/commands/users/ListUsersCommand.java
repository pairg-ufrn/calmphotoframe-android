package br.ufrn.dimap.pairg.calmphotoframe.controller.commands.users;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseStatelessCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class ListUsersCommand extends BaseStatelessCommand<List<User>>{

	@Override
	public void execute(ResultListener<? super List<User>> listener) {
		ApiClient.instance().users().list(listener);
	}
}
