package br.ufrn.dimap.pairg.calmphotoframe.photoframe.api;

import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;

public class ServerCacheKeyGenerator implements PhotoCache.CacheKeyGenerator<Integer> {
    private EntityGetter<? extends Endpoint> endpointHolder;

    public ServerCacheKeyGenerator(EntityGetter<? extends Endpoint> endpointHolder) {
        this.endpointHolder = endpointHolder;
    }

    @Override
    public String genKey(Integer photoId) {
        if (endpoint() == null) { //maybe should throw an exception (or return null)
            return String.valueOf(photoId);
        }

        return new StringBuilder()
                .append(genServerKey())
                .append(".")
                .append(photoId)
                .toString();
    }

    Endpoint endpoint() {
        return endpointHolder.getEntity();
    }

    private String genServerKey() {
        return endpoint().getHostname() + ":" + String.valueOf(endpoint().getPort());
    }
}
