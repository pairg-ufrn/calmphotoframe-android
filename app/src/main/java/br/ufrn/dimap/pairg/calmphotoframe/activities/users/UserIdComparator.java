package br.ufrn.dimap.pairg.calmphotoframe.activities.users;

import java.util.Comparator;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class UserIdComparator implements Comparator<User> {
	@Override
	public int compare(User lhs, User rhs) {
		if(lhs.getId().equals(rhs.getId())){
			return 0;
		}
		else if(lhs.getId() < rhs.getId()){
			return -1;
		}
		else{
			return +1;
		}
	}
}