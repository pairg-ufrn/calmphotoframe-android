package br.ufrn.dimap.pairg.calmphotoframe.photoframe.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User implements Cloneable{
	private Long id;
	private String login;
	private boolean active;
	private boolean isAdmin;
	private String name;
    private boolean hasProfileImage;

	public User() {
		this("");
	}
	public User(String login) {
		this(null, login);
	}
	public User(Long id, String login) {
		this(id, login, true, false);
	}
	public User(Long id, String login, boolean isActive, boolean isAdmin) {
		super();
		this.id = id;
		this.login = login;
		this.active = isActive;
		this.isAdmin = isAdmin;
	}
	public User(@NonNull  User other) {
		super();
		this.id = other.getId();
		this.login = other.getLogin();
		this.active = other.isActive();
		this.isAdmin = other.getIsAdmin();
		this.name = other.getName();
		this.hasProfileImage = other.getHasProfileImage();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(boolean admin) {
		this.isAdmin = admin;
	}

    public String getName() {
        return name;
    }
    public User setName(String name) {
        this.name = name;

		return this;
    }

	@JsonIgnore
    public boolean getHasProfileImage() {
        return hasProfileImage;
    }

	@JsonProperty
    public void setHasProfileImage(boolean hasProfileImage) {
        this.hasProfileImage = hasProfileImage;
    }

    @Override
    public String toString() {
        return getLogin();
    }


    public String reprString() {
        return String.format("{ id:%d, login:%s, name:%s, active: %b}", getId(), getLogin(), getName(), isActive());
    }
	
	@Override
	public User clone(){
		User clone = new User();
        clone.setId(getId());
		clone.setActive(this.isActive());
		clone.setIsAdmin(getIsAdmin());
		clone.setLogin(getLogin());
        clone.setName(getName());
		clone.setHasProfileImage(getHasProfileImage());
		
		return clone;
	}

	@Override
	public boolean equals(Object obj) {
        if(!(obj instanceof User)){
            return false;
        }
        User other = (User)obj;

		return equalsOrNull(getId(), other.getId())
                && (getIsAdmin() == other.getIsAdmin())
                && (isActive() == other.isActive())
                && (getHasProfileImage() == other.getHasProfileImage())
                && equalsOrNull(getLogin(), other.getLogin())
                && equalsOrNull(getName(), other.getName()) ;
	}

    private boolean equalsOrNull(Object o1, Object o2) {
        if(o1 == null || o2 == null){
            return o1 == null && o2 == null;
        }

        return o1.equals(o2);
    }
}
