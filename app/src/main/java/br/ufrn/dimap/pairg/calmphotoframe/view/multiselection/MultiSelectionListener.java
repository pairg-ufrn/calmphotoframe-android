package br.ufrn.dimap.pairg.calmphotoframe.view.multiselection;

/**
 */
public interface MultiSelectionListener {
    void onStartSelection();

    void onItemSelectionChanged(int selectionCount, int position, long id, boolean checked);

    void onEndSelection();
}
