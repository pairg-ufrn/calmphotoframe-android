package br.ufrn.dimap.pairg.calmphotoframe.view.image;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.EntityGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.Image;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.Size;

/**
 */
public class PicassoLocalImageLoader implements ImageLoader<Image> {
    Context context;
    EntityGetter<Size> sizeGetter;

    public PicassoLocalImageLoader(Context context) {
        this(context, null);
    }
    public PicassoLocalImageLoader(Context context, EntityGetter<Size> sizeGetter) {
        this.context = context;
        this.sizeGetter = sizeGetter;
    }

    public PicassoLocalImageLoader setSize(EntityGetter<Size> sizeGetter){
        this.sizeGetter = sizeGetter;
        return this;
    }

    @Override
    public RequestHandler loadImage(Image entity, ResultListener<? super Drawable> listener) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public RequestHandler loadImage(Image entity, ImageView target) {
        PicassoReqHandlerCallback handler = new PicassoReqHandlerCallback(context, target);

        load(entity).centerCrop().error(R.color.img_failed).into(target, handler);

        return handler;
    }

    protected RequestCreator load(Image entity) {
        RequestCreator req = Picasso.with(context).load("file://" + entity.getUrl());
        return resize(req);
    }

    RequestCreator resize(RequestCreator req) {
        Size size = getTargetSize();
        if(size != null){
            req.resize(size.getWidth(), size.getHeight());
        }
        else{
            req.fit();
        }
        return req;
    }

    private Size getTargetSize() {
        if(sizeGetter == null){
            return  null;
        }
        return sizeGetter.getEntity();
    }

    private static class PicassoReqHandlerCallback implements Callback, RequestHandler {
        ImageView target;
        volatile boolean finished, cancelled;
        Context context;

        public PicassoReqHandlerCallback(Context context, ImageView target) {
            this.target = target;
            finished = false;
            cancelled = false;
            this.context = context;
        }

        @Override
        public void onSuccess() {
            finished = true;
        }

        @Override
        public void onError() {
            finished = true;
        }

        @Override
        public boolean isFinished() {
            return finished;
        }

        @Override
        public boolean isCancelled() {
            return cancelled;
        }

        @Override
        public boolean cancel() {
            if(isFinished()){
                return false;
            }
            if(!isCancelled()) {
                Picasso.with(context).cancelRequest(target);
            }
            return true;
        }
    }
}
