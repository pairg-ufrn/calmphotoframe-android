package br.ufrn.dimap.pairg.calmphotoframe.controller.commands;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor{
	private final Map<Object, Command<?>> commands;
	private final Map<Object, ResultListener<?>> commandListeners;

	public CommandExecutor() {
		commands = new HashMap<>();
		commandListeners = new HashMap<>();
	}

	public <T> void putCommand(Object cmdIdentifier, Command<T> cmd, ResultListener<? super T> resultListener){
		commands.put(cmdIdentifier, cmd);
		if(resultListener != null){
			commandListeners.put(cmdIdentifier, resultListener);
		}
		else{
			commandListeners.remove(cmdIdentifier);
		}
	}

	public void removeCommand(Object cmdId) {
		commands.remove(cmdId);
		commandListeners.remove(cmdId);
	}

	public boolean hasCommand(Object cmdId){
		return commands.containsKey(cmdId);
	}
	
	public boolean execute(Object cmdId){
		ResultListener<?> resultListener = commandListeners.get(cmdId);
		return execute(cmdId, resultListener);
	}
	public <T> boolean execute(Object cmdId, ResultListener<T> resultListener){
		@SuppressWarnings("unchecked")
		Command<? extends T> cmd = (Command<? extends T>) commands.get(cmdId);
		if(cmd != null){
			cmd.execute(resultListener);

			return true;
		}
		return false;
	}
}