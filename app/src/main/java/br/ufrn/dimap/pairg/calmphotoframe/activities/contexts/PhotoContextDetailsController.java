package br.ufrn.dimap.pairg.calmphotoframe.activities.contexts;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.AsyncGetter;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnSaveDataListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocontext.EditContextCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Collaborator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoContext;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.FragmentCreator.FragmentBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextComposeFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextComposeFragment.ContextAndCollaboratorsWrapper;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.context.PhotoContextComposeFragment.OnFinishListener;

public class PhotoContextDetailsController {
	private static final String DIALOG_TAG = EditContextCommand.class.getName() + ".Dialog";
	
	private int fragmentContainerId;
	private PhotoContextComposeFragment composeFrag;
	private BaseActivity activity;
    private OnFinishListener onFinishListener;

	public PhotoContextDetailsController(BaseActivity activity, int fragmentContainerId)
	{
		this.activity = activity;
		this.fragmentContainerId = fragmentContainerId;
		composeFrag = new PhotoContextComposeFragment();
	}
	
	public int getFragmentContainerId() {
		return fragmentContainerId;
	}
	public void setFragmentContainerId(int fragmentContainerId) {
		this.fragmentContainerId = fragmentContainerId;
	}

	public boolean isEditable(){
		return composeFrag.isEditable();
	}
	public void setEditable(boolean editable) {
		composeFrag.setEditable(editable);
	}

    public OnFinishListener getOnFinishListener() {
        return onFinishListener;
    }
    public void setOnFinishListener(OnFinishListener onFinishListener) {
        this.onFinishListener = onFinishListener;
    }

	public void initialize(){
		composeFrag.setUsersAsyncGetter(new UsersAsyncGetter());

		composeFrag.setOnFinishListener(new OnFinishListener() {
			OnFinishPopFragment onFinishPopFragment = new OnFinishPopFragment();

			@Override
			public void onFinish(PhotoContextComposeFragment fragment) {
				onFinishPopFragment.onFinish(fragment);
				if(onFinishListener != null){
					onFinishListener.onFinish(fragment);
				}
			}
		});

		composeFrag.setCurrentUser(ApiClient.instance().getCurrentUser());
	}
	
	public void show(PhotoContext selectedContext, final ResultListener<? super PhotoContext> cmdResultListener) {
		if(selectedContext == null){
			return;
		}
		
		composeFrag.setPhotoContext(selectedContext);
		composeFrag.setOnSaveContextListener(new SaveContextListener(cmdResultListener));
		
		ApiClient.instance().contexts().getCollaborators(selectedContext.getId()
				, new OnGetCollaboratorsPopulateFragment(composeFrag));
		
		FragmentCreator fragCreator = new FragmentCreator(activity.getSupportFragmentManager());
		fragCreator.replace(
				fragmentContainerId, DIALOG_TAG, new FragmentBuilder() {
					@Override
					public Fragment buildFragment() {
						return composeFrag;
					}
				}).addToBackStack(null)
	    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
	    .get();

	}
//	private void disableCurrentCollaboratorEdition(final PhotoContextComposeFragment composeFrag) {
//		User currentUser = PhotoFrameClient.instance().getCurrentUser();
//		if(currentUser != null){
//			composeFrag.disableCollaborator(currentUser.getId());
//		}
//	}

//	CharSequence getText(int stringId){
//		return getContext().getText(stringId);
//	}

	protected Context getContext(){
		return getActivity();
	}
	protected BaseActivity getActivity(){
		return activity;
	}
	
	/* *********************************************************************************************/
	
	private class OnGetCollaboratorsPopulateFragment extends BasicResultListener<Collection<Collaborator>> {
		private final PhotoContextComposeFragment composeFrag;

		private OnGetCollaboratorsPopulateFragment(PhotoContextComposeFragment composeFrag) {
			this.composeFrag = composeFrag;
		}

		@Override
		public void onResult(Collection<Collaborator> result) {
			composeFrag.setCollaborators(result);
		}
	}

	private class OnFinishPopFragment implements OnFinishListener {
		@Override
		public void onFinish(PhotoContextComposeFragment fragment) {
			getActivity().getSupportFragmentManager().popBackStack();
		}
	}

	private static class UsersAsyncGetter implements AsyncGetter<Collection<User>> {
		@Override
		public void getAsync(ResultListener<? super Collection<User>> listener) {
			ApiClient.instance().users().list(listener);
		}
	}

	public static class SaveContextListener implements OnSaveDataListener<ContextAndCollaboratorsWrapper> {
		private ResultListener<? super PhotoContext> listener;
		
		public SaveContextListener(ResultListener<? super PhotoContext> listener) {
			this.listener = listener;
		}

		@Override
		public boolean onSaveData(ContextAndCollaboratorsWrapper wrapper) {
			ApiClient.instance().contexts().update(wrapper.context, wrapper.collaborators, listener);
			return true;
		}
	}
}
