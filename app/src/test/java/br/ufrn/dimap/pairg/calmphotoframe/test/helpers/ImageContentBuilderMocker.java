package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;

public class ImageContentBuilderMocker {
    @Mock
    ImageContent.Builder imageContentBuilder;

    public ImageContentBuilderMocker() {
        MockitoAnnotations.initMocks(this);
    }

    public void setup() {
        ImageContent.builder(imageContentBuilder);
        try {
            Mockito.doReturn(Mockito.mock(ImageContent.class))
                    .when(imageContentBuilder)
                    .fromString(Mockito.anyString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
