package br.ufrn.dimap.pairg.calmphotoframe.test.photoframe;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.resultlisteners.OnApiErrorDelegator;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ComposedException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApiErrorDelegatorTest {

    ResultListener<Object> mockListener;
    OnApiErrorDelegator apiErrorDelegator;

    @Before
    public void setUp(){
        mockListener = Mockito.mock(ResultListener.class);
        when(mockListener.isListening()).thenReturn(true);
        apiErrorDelegator = new OnApiErrorDelegator<Object>(mockListener, 403, 404);
    }

    @Test
    public void shouldDelegateOnApiError(){
        ApiException exception = new ApiException("", null, 404);

        implVerifyDelegation(exception);
    }

    @Test
    public void shouldDelegateWhenCauseByApiError(){
        ComposedException exception = new ComposedException(new ApiException("", null, 403));

        implVerifyDelegation(exception);
    }

    private void implVerifyDelegation(Exception exception) {
        apiErrorDelegator.onFailure(exception);

        verify(mockListener).onFailure(eq(exception));
    }
}
