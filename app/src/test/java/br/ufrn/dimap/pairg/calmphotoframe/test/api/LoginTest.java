package br.ufrn.dimap.pairg.calmphotoframe.test.api;

import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiHelper;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.BaseApiSteps;
import cz.msebera.android.httpclient.cookie.Cookie;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.RequestBuilder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;


public class LoginTest extends SimpleScenarioTest<LoginTest.Steps>{

    @Test
    public void doingConnectionWithExistentToken(){
        given()
                .that_the_client_is_disconnected()
                .but().there_is_a_valid_endpoint()
                .and().a_session_token_related_to_some_user_on_that_endpoint()
                .and().some_connection_listener();

        when().doing_a_connection();

        then()
            .the_client_should_have_requested_the_correspondent_user_info()
            .and().the_listener_should_receive_a_correspondent_photoframeinfo()
            .and().the_client_info_should_change_to_match_the_given_endpoint_and_user()
            .and().the_connection_token_should_be_saved_as_cookie();
    }

    public static class Steps extends BaseApiSteps<Steps> {
        Endpoint endpoint;
        String token;
        User testUser;
        ResultListener<? super ApiEndpoint> listener;
        ApiEndpoint infoResult;

        /* ************************************* Given *********************************************/

        public Steps there_is_a_valid_endpoint() {
            endpoint = new Endpoint("www.example.com", 10000);

            return self();
        }

        public Steps a_session_token_related_to_some_user_on_that_endpoint() {
            this.token = "my token";
            testUser = new User("nome");

            return self();
        }

        public Steps some_connection_listener() {
            this.listener = new BasicResultListener<ApiEndpoint>(){
                @Override
                public void onResult(ApiEndpoint result) {
                    infoResult = result;
                }
            };
            this.listener = Mockito.spy(listener);
            return self();
        }

        /* ************************************* When **********************************************/

        public Steps doing_a_connection() {
            String url = apiBuilder.myUserUrl(endpoint);
            this.mockRequestFor(url).successfulResponse(testUser);

            this.apiClient.session().connect(this.endpoint, this.token, this.listener);
            return this;
        }

        /* ************************************* Then **********************************************/

        public Steps the_client_should_have_requested_the_correspondent_user_info() {
            String expectedUrl = apiBuilder.myUserUrl(endpoint);

            assertThat(capturedRequests(), not(empty()));

            RequestBuilder<?, ?> request = capturedRequests().get(0);

            assertThat(request.url(), is(equalTo(expectedUrl)));

            verify(request).send();
            verify(apiConnector).request(expectedUrl);

            return this;
        }

        public Steps the_listener_should_receive_a_correspondent_photoframeinfo() {
            assertThat(infoResult, is(not(nullValue())));
            assertThat(endpoint, is(equalTo((Endpoint) infoResult)));

            verify(listener).onResult((ApiEndpoint) Mockito.notNull());

            return self();
        }

        public Steps the_client_info_should_change_to_match_the_given_endpoint_and_user() {
            ApiEndpoint info = apiClient.getEndpoint();

            assertThat(info                         , is(not(nullValue())));
            assertThat(info.getSession()            , is(not(nullValue())));

            assertThat(info.getSession().getToken() , is(equalTo(token)));
            assertThat(info.getSession().getUser()  , is(equalTo(testUser)));
            assertThat(info.getHostname()           , is(equalTo(endpoint.getHostname())));
            assertThat(info.getPort()               , is(equalTo(endpoint.getPort())));

            return self();
        }

        public Steps the_connection_token_should_be_saved_as_cookie() {
            List<Cookie> cookies = cookieStore.getCookies();

            assertThat(cookies, is(not(empty())));

            Matcher<Cookie> with_given_name_and_value = Matchers.<Cookie>
                    both(hasProperty("name", equalTo(ApiHelper.TOKEN_COOKIE_NAME)))
                    .and(hasProperty("value", equalTo(token)));

            assertThat(cookies, hasItem(with_given_name_and_value));

            return this;
        }
    }
}
