package br.ufrn.dimap.pairg.calmphotoframe.test.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.tngtech.jgiven.annotation.As;
import com.tngtech.jgiven.annotation.IntroWord;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiEndpointHolder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.UserActivationRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpMethods;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.BaseApiSteps;
import cz.msebera.android.httpclient.HttpEntity;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;


public class UsersApiTests extends SimpleScenarioTest<UsersApiTests.UsersApiSteps>{

    @Test
    public void changePassword() throws IOException {
        given()
                .a_logged_in_user()

        .when()
                .a_change_password_request_is_done()

        .then()
                .a_post_request_should_have_been_sent()
                .using().the_users_change_password_route()
                .and().the_new_password_should_have_been_sent_on_body();
    }

    @Test
    public void getUserProfileImage(){
        given()
                .user_with_profile_image()

        .when()
                .getting_this_user_image()

        .then()
                .an_image_request_should_be_made()
                .using().that_user_image_profile_route();
    }

    @Test
    public void updateUserProfileImage() throws IOException {
        given()
                .a_logged_in_user()
                .and().a_valid_image()

        .when()
                .a_request_is_made_to_update_his_profile_image()

        .then()
                .a_post_request_should_have_been_sent()
                .using().the_current_user_image_profile_route()
                .and().the_given_image_should_be_used_as_content()
                .and().the_user_image_cache_should_be_invalidated();
    }

    @Test
    public void updateUserName() throws JsonProcessingException {
        given()
                .a_logged_in_user()

        .when()
                .trying_to_update_his_name()

        .then()
                .a_post_request_should_have_been_sent()
                .using().the_current_user_route()
                .and().the_changed_user_should_have_been_sent_as_content();
    }

    @Test
    public void activateUserAccount() throws IOException {
        given()
                .a_logged_in_admin()
                .and().another_existing_user_account()
        .when()
                .a_request_to_activate_that_user_is_made()

        .then()
                .a_post_request_should_have_been_sent()
                .using().the_route_to_activate_the_other_user()
                .and().the_activate_field_should_be_sent_as_true_on_request_body();
    }

    public static class UsersApiSteps extends BaseApiSteps<UsersApiSteps>{
        String expectedPassword;

        @Captor
        ArgumentCaptor<String> urlCaptor;

        User user;
        ImageContent imageContent;
        String expectedUrl;

        public UsersApiSteps(){
            ApiEndpointHolder.OnChangeApiEndpoint listener = apiEndpointHolder.getOnChangeListener();

            apiEndpointHolder.setOnChangeListener(null);
            that_the_client_is_connected(new ApiEndpoint("localhost"));
            apiEndpointHolder.setOnChangeListener(listener);
        }

        @IntroWord
        @As("using")
        public UsersApiSteps using() {
            return this;
        }

        //region Given steps
        //-----------------------------------------------------------------------------------------

        public UsersApiSteps a_valid_image() {
            InputStream inputStream = Mockito.mock(InputStream.class);

            imageContent = new ImageContent(inputStream, "image/jpeg");
            return this;
        }

        public UsersApiSteps user_with_profile_image() {
            another_existing_user_account();
            user.setHasProfileImage(true);

            return this;
        }

        public UsersApiSteps another_existing_user_account() {
            user = new User(123456L, "login");
            return this;
        }

        //endregion

        //region When steps
        //-----------------------------------------------------------------------------------------

        public UsersApiSteps a_change_password_request_is_done() {
            expectedUrl = apiBuilder.myPasswordUrl();
            this.mockRequestFor(expectedUrl)
                    .successfulResponse();

            expectedPassword = "any password";

            apiClient.users().changePassword(expectedPassword, listenerMockup);
            return this;
        }

        public UsersApiSteps getting_this_user_image() {
            apiClient.users().getProfileImage(user.getId(), listenerMockup);

            return this;
        }

        public UsersApiSteps a_request_is_made_to_update_his_profile_image() {
            expectedUrl = apiBuilder.myUserProfileImageUrl();
            mockRequestFor(expectedUrl).successfulResponse();

            apiClient.users().updateProfileImage(imageContent, loggedInUser.getId(), listenerMockup);

            return this;
        }


        public UsersApiSteps trying_to_update_his_name() {
            user = loggedInUser.clone();
            user.setName("other name");

            expectedUrl = apiBuilder.userUrl(user.getId());
            mockRequestFor(expectedUrl).successfulResponse(user);

            apiClient.users().update(user, listenerMockup);

            return this;
        }

        public UsersApiSteps a_request_to_activate_that_user_is_made() {
            expectedUrl = apiBuilder.userActiveUrl(user.getId());

            mockRequestFor(expectedUrl).successfulResponse(new UserActivationRequest.ActiveState(true));
            apiClient.users().changeActiveState(user.getId(), true, listenerMockup);

            return this;
        }

        //endregion

        //region Then
        //-----------------------------------------------------------------------------------------

        public UsersApiSteps a_post_request_should_have_been_sent() {
            verify(lastRequest()).send();
            verify(lastRequest()).url(urlCaptor.capture());
            verify(lastRequest()).method(Mockito.eq(HttpMethods.Post));

            return this;
        }

        public UsersApiSteps the_users_change_password_route() {
            assertThat(lastRequest().url(), is(equalTo(apiBuilder.myPasswordUrl())));

            return this;
        }

        public UsersApiSteps the_new_password_should_have_been_sent_on_body() throws IOException {
            JsonNode jsonNode = verifyJsonBody();

            assertThat(jsonNode.get("password").asText(), is(equalTo(expectedPassword)));

            return this;
        }

        public UsersApiSteps an_image_request_should_be_made() {
            //noinspection unchecked
            verify(httpConnector).getImage(urlCaptor.capture(), Mockito.any(ResultListener.class));

            return this;
        }

        public UsersApiSteps the_user_image_cache_should_be_invalidated() {
            String userIdUrl = apiBuilder.userProfileImageUrl(loggedInUser.getId());
            String myUserUrl = apiBuilder.myUserProfileImageUrl();

            verify(httpConnector).invalidateImageCache(myUserUrl);
            verify(httpConnector).invalidateImageCache(userIdUrl);
            return this;
        }

        public UsersApiSteps the_current_user_image_profile_route() {
            using_expected_url(expectedUrl);
            return this;
        }
        public UsersApiSteps that_user_image_profile_route() {
            String expectedUrl = apiBuilder.userProfileImageUrl(user.getId());
            using_expected_url(expectedUrl);

            return this;
        }

        public UsersApiSteps the_current_user_route() {
            using_expected_url(expectedUrl);
            return this;
        }

        protected void using_expected_url(String expectedUrl) {
            assertThat(expectedUrl, is(notNullValue()));
            assertThat(urlCaptor.getValue(), equalTo(expectedUrl));
        }

        public UsersApiSteps the_given_image_should_be_used_as_content() throws IOException {
            ArgumentCaptor<HttpEntity> captureEntity = ArgumentCaptor.forClass(HttpEntity.class);

            verify(lastRequest()).content(captureEntity.capture());

            HttpEntity entity = captureEntity.getValue();
            assertThat(entity, is(notNullValue()));
            assertThat(entity.getContentType(), is(notNullValue()));
            assertThat(entity.getContentType().getValue(), equalTo(imageContent.getMimeType()));
            assertThat(entity.getContent(), is(imageContent.getImageStream()));

            return this;
        }

        public UsersApiSteps the_changed_user_should_have_been_sent_as_content() throws JsonProcessingException {
            ArgumentCaptor<Object> objectArgumentCaptor = ArgumentCaptor.forClass(Object.class);
            verify(lastRequest()).json(objectArgumentCaptor.capture());

            assertThat(objectArgumentCaptor.getValue(), is(instanceOf(User.class)));
            assertThat((User)objectArgumentCaptor.getValue(), is(equalTo(user)));
            return this;
        }

        public UsersApiSteps the_route_to_activate_the_other_user() {
            using_expected_url(expectedUrl);
            return this;
        }

        public UsersApiSteps the_activate_field_should_be_sent_as_true_on_request_body() throws IOException {
            JsonNode jsonNode = verifyJsonBody();

            assertThat(jsonNode.get("active").asBoolean(), is(equalTo(true)));

            return this;
        }

        protected JsonNode verifyJsonBody() throws IOException {
            ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);

            verify(lastRequest()).json(stringCaptor.capture());

            String capturedBody = stringCaptor.getValue();
            assertThat(capturedBody, is(not(isEmptyString())));

            return objectMapper.readTree(capturedBody);
        }
        //endregion
    }
}
