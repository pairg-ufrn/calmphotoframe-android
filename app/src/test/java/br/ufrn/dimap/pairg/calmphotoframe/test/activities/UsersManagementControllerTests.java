package br.ufrn.dimap.pairg.calmphotoframe.test.activities;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.UsersManagementController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.UserActivationRequest;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.CaptureAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.RequestAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.UserNotifierMockup;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UsersFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.AccountViewBinder.UserActivationParam;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class UsersManagementControllerTests extends SimpleScenarioTest<UsersManagementControllerTests.UsersManagementControllerSteps>{

    @Test
    public void listUsers(){
        given()
                .the_users_controller_is_configured()
                .and().there_is_some_users_on_server()
                .and().one_of_them_is_logged_in()

        .when()
                .the_controller_data_is_requested()
                .and().it_is_successfully_received()

        .then()
                .the_controller_should_have_requested_the_list_of_users_to_the_server()
                .and().it_should_set_the_result_into_the_fragment()
                .but().the_current_user_should_not_have_been_included_on_list();
    }

    @Test
    public void disableAccount(){
        given()
                .the_controllers_view_has_some_users()

        .when()
                .a_command_to_deactivate_a_user_is_triggered_from_the_view()
                .and().the_command_succeeds()

        .then()
                .the_controller_should_have_sent_a_request_to_deactivate_that_user();
    }

    @Test
    public void disableAccountFail(){
        given()
                .the_controllers_view_has_some_users()

        .when()
                .a_command_to_deactivate_a_user_is_triggered_from_the_view()
                .but().a_fail_response_is_received()

        .then()
                .the_controller_should_have_sent_a_request_to_deactivate_that_user()
                .and().an_error_message_should_have_been_showed_on_view();
    }

    public static class UsersManagementControllerSteps extends Stage<UsersManagementControllerSteps> {
        UsersManagementController controller;

        @Mock
        BaseActivity activity;

        @Mock(answer = Answers.RETURNS_MOCKS)
        UsersFragment usersFragment;

        MockedApi mockedApi;
        UserNotifierMockup userNotifierMockup;

        CaptureAnswer<RequestHandler> captureListUsers = new CaptureAnswer<>();

        @Captor
        ArgumentCaptor<ResultListener> captureListener;

        List<User> users, capturedUsers;
        User loggedInUser;
        User userToUpdate;

        public UsersManagementControllerSteps(){
            MockitoAnnotations.initMocks(this);

            mockedApi = new MockedApi();
            mockedApi.setup();

            userNotifierMockup = new UserNotifierMockup();
            userNotifierMockup.mockup(activity);

            controller = new UsersManagementController(activity);
        }

        public UsersManagementControllerSteps the_users_controller_is_configured() {
            controller.setView(usersFragment);
            return this;
        }

        public UsersManagementControllerSteps there_is_some_users_on_server() {
            users = Arrays.asList(
                    new User(1L, "lorem").setName("Lorem Ipsum"),
                    new User(2L, "john").setName("John Smith"),
                    new User(3L, "rlp").setName("Raulph Laren")
            );

            Mockito.when(mockedApi.usersApi.list(any(ResultListener.class)))
                    .then(captureListUsers.delegate(new RequestAnswer<>(users)));

            return this;
        }


        public UsersManagementControllerSteps one_of_them_is_logged_in() {
            loggedInUser = users.get(0);

            mockedApi.mockCurrentUser(loggedInUser);
            return this;
        }

        public UsersManagementControllerSteps the_controller_data_is_requested() {
            Mockito.when(mockedApi.usersApi.list(any(ResultListener.class))).then(captureListUsers.setReturnValue(null));
            controller.requestData();
            return this;
        }

        public UsersManagementControllerSteps it_is_successfully_received() {
            ResultListener l = captureListUsers.getArg(0, 0);

            if(l != null){
                l.onResult(users);
            }

            return this;
        }

        public UsersManagementControllerSteps the_controller_should_have_requested_the_list_of_users_to_the_server() {
            verify(mockedApi.usersApi).list(any(ResultListener.class));

            return this;
        }

        @SuppressWarnings("unchecked")
        public UsersManagementControllerSteps it_should_set_the_result_into_the_fragment() {
            ArgumentCaptor<List> usersListCaptor = ArgumentCaptor.forClass(List.class);

            verify(usersFragment).setItems(usersListCaptor.capture());

            this.capturedUsers = usersListCaptor.getValue();

            assertThat(capturedUsers, is(not(Matchers.empty())));

            return this;
        }

        public UsersManagementControllerSteps the_current_user_should_not_have_been_included_on_list() {
            assertThat(capturedUsers, not(hasItem(loggedInUser)));

            return this;
        }



        public UsersManagementControllerSteps the_controllers_view_has_some_users() {
            the_users_controller_is_configured()
            .and().there_is_some_users_on_server();

            return this;
        }

        public UsersManagementControllerSteps a_command_to_deactivate_a_user_is_triggered_from_the_view() {
            ArgumentCaptor<ArgsCommand> captor = ArgumentCaptor.forClass(ArgsCommand.class);

            verify(usersFragment).setActivateUserCommand(captor.capture());

            userToUpdate = users.get(0);

            ArgsCommand cmd = captor.getValue();

            cmd.execute(new UserActivationParam(userToUpdate, !userToUpdate.isActive()), Mockito.mock(ResultListener.class));

            return this;
        }

        public UsersManagementControllerSteps the_command_succeeds() {
            ResultListener l = verifyChangeActiveStateCalled();
            Listeners.onResult(l, new UserActivationRequest.ActiveState(!userToUpdate.isActive()));

            return this;
        }

        public UsersManagementControllerSteps a_fail_response_is_received() {
            ResultListener l = verifyChangeActiveStateCalled();
            l.onFailure(Mockito.mock(Throwable.class));

            return this;
        }

        public UsersManagementControllerSteps the_controller_should_have_sent_a_request_to_deactivate_that_user() {
            verify(mockedApi.usersApi).changeActiveState(
                    Mockito.eq(userToUpdate.getId()),
                    Mockito.eq(false),
                    any(ResultListener.class)
            );
            return this;
        }

//        public UsersManagementControllerSteps the_user_active_state_should_have_changed_to(boolean active) {
//
//            return this;
//        }

        public UsersManagementControllerSteps an_error_message_should_have_been_showed_on_view() {
            verify(userNotifierMockup.getNotificationBuilderMockup()).show();

            return this;
        }

        protected ResultListener verifyChangeActiveStateCalled() {
            ArgumentCaptor<ResultListener> listenerCaptor = ArgumentCaptor.forClass(ResultListener.class);

            verify(mockedApi.usersApi).changeActiveState(
                    eq(userToUpdate.getId()), eq(!userToUpdate.isActive()), listenerCaptor.capture());

            return listenerCaptor.getValue();
        }
    }
}
