package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import android.content.Context;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.ufrn.dimap.pairg.calmphotoframe.activities.NotificationBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.activities.UserNotifier;

public class UserNotifierMockup {

    @Mock
    UserNotifier userNotifierMockup;

    NotificationBuilder notificationBuilderMockup;


    public UserNotifierMockup(){
        MockitoAnnotations.initMocks(this);

        notificationBuilderMockup = Mockito.spy(new NotificationBuilder(Mockito.mock(Context.class)));
        Mockito.doNothing().when(notificationBuilderMockup).show();
        Mockito.doReturn("this text was mocked").when(notificationBuilderMockup).getTextResouce(Mockito.anyInt());

        mockup(userNotifierMockup);
    }

    /** Mock {@code userNotifierMockup} to respond with mocks.*/
    public void mockup(UserNotifier userNotifierMockup){
        Mockito.doReturn(notificationBuilderMockup).when(userNotifierMockup).notifyUser();
        Mockito.doReturn(notificationBuilderMockup).when(userNotifierMockup).notifyUser(Mockito.anyString());
    }

    public NotificationBuilder getNotificationBuilderMockup(){
        return notificationBuilderMockup;
    }

    public UserNotifier getUserNotifierMockup() {
        return userNotifierMockup;
    }
}
