package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import android.support.v4.app.FragmentManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.LoginActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.LoginSignupFragment;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

public class LoginActivityTest extends RobolectricTestCase {

    ActivityController<LoginActivity> activityController;
    LoginActivity activity;

    MockedApi mockedApi = new MockedApi();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        LocalPersistence.instance(new LocalPersistence(RuntimeEnvironment.application));

        setupApiMockup();

        activityController = Robolectric.buildActivity(LoginActivity.class);
        activity = activityController.get();
    }

    @Test
    public void saveConnectedServerOnLogin(){
        final String serverAddress = "www.example.com";
        final String userLogin = "mylogin";
        final String userPassword = "mypass";

        whenStartingActivity();

        whenDoingLogin(serverAddress, userLogin, userPassword);

        thenTheClientShouldDoConnection(serverAddress, userLogin, userPassword);

        thenTheServerAddressShouldBeSaved(serverAddress);
    }

    @Test
    public void loadLastConnectedServers(){
        assertThat(LocalPersistence.instance().getConnectedServers(), is(empty()));

        List<ServerInfo> servers = givenSomeSavedConnectedServers();

        whenStartingActivity();

        thenServerAdapterShouldContainTheseServers(servers);
    }

    /* *************************************** Steps ******************************************** */

    public ApiClient getApiClient() {
        return mockedApi.apiClient;
    }

    private List<ServerInfo> givenSomeSavedConnectedServers() {
        List<ServerInfo> serverInfos = Arrays.asList(
                new ServerInfo("Local server", "localhost"),
                new ServerInfo("Example", "www.example.com"),
                new ServerInfo("Private server", "10.0.0.10"));

        for (ServerInfo info : serverInfos){
            LocalPersistence.instance().addConnectedServer(info);
        }
        Collections.reverse(serverInfos);

        return serverInfos;
    }

    protected void whenStartingActivity() {
        System.out.println("Starting activity");

        activityController.create();
        activity.getController().refreshView();

        System.out.println("activity started");
    }

    protected void whenDoingLogin(String serverAddress, String userLogin, String userPassword) {
        LoginSignupFragment frag = getLoginSignupFragment();

        assertNotNull(frag);
        assertThat(frag.isLogin(), is(true));

        EditText serverView   = frag.findView(R.id.login_serverInput);
        EditText loginView    = frag.findView(R.id.login_loginInput);
        EditText passwordView = frag.findView(R.id.login_passwordInput);
        Button submitBtn      = frag.findView(R.id.login_submitButton);

        assertNotNull(serverView);
        assertNotNull(loginView);
        assertNotNull(passwordView);
        assertNotNull(submitBtn);

        serverView.setText(serverAddress);
        loginView.setText(userLogin);
        passwordView.setText(userPassword);

        submitBtn.performClick();
    }

    private LoginSignupFragment getLoginSignupFragment() {
        FragmentManager manager = activity.getSupportFragmentManager();
        return (LoginSignupFragment) manager.findFragmentById(R.id.loginFragment);
    }


    protected void thenTheClientShouldDoConnection(String serverAddress, String userLogin, String userPassword) {
        ArgumentCaptor<ConnectionInfo> connectionCaptor = ArgumentCaptor.forClass(ConnectionInfo.class);

        //FIXME: avoid reference internal details (mock direct dependencies)
        verify(mockedApi.sessionApi).connect(
                connectionCaptor.capture(),
                Mockito.any(ResultListener.class));

        ConnectionInfo connectionInfo = connectionCaptor.getValue();
        Endpoint capturedPhotoFrame = connectionInfo.getEndpoint();
        LoginInfo capturedLoginInfo = connectionInfo.getLoginInfo();
        assertThat(capturedPhotoFrame.getHostname(), is(equalTo(serverAddress)));
        assertThat(capturedLoginInfo.getLogin(), is(equalTo(userLogin)));
        assertThat(capturedLoginInfo.getPassword(), is(equalTo(userPassword)));
    }


    private void thenTheServerAddressShouldBeSaved(String serverAddress) {
        LocalPersistence persistence = LocalPersistence.instance();
        assertNotNull(persistence);

        List<ServerInfo> servers = persistence.getConnectedServers();

        assertThat(servers, is(not(empty())));
        assertThat(servers, hasItem(equalTo(new ServerInfo("", serverAddress))));
    }

    private void thenServerAdapterShouldContainTheseServers(List<ServerInfo> expectedServers) {
        List<ServerInfo> adapterItems = getAdapterItems();

        assertThat("Server input fragment should contain at least " + expectedServers.size() + " items.",
                adapterItems.size(), greaterThanOrEqualTo(expectedServers.size()));

        for (ServerInfo serverInfo : expectedServers) {
            assertThat(adapterItems, hasItem(serverInfo));
        }
    }

    /* ****************************** Helpers *************************************************** */

    private void setupApiMockup() {
        mockedApi.setup();

        Answer onConnectAnswer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ResultListener<Endpoint> listener = (ResultListener<Endpoint>)invocation.getArguments()[1];
                if(listener != null){
                    ConnectionInfo info = (ConnectionInfo) invocation.getArguments()[0];
                    listener.onResult(info.getEndpoint());
                }
                return new FinishedRequestHandler();
            }
        };

        doAnswer(onConnectAnswer)
                .when(mockedApi.sessionApi).connect(
                Mockito.any(ConnectionInfo.class),
                Mockito.any(ResultListener.class));
    }

    private List<ServerInfo> getAdapterItems() {
        LoginSignupFragment frag = getLoginSignupFragment();
        AutoCompleteTextView serverInputView = frag.findView(R.id.login_serverInput);

        ListAdapter adapter = serverInputView.getAdapter();

        return getAdapterItems(adapter);
    }

    private List<ServerInfo> getAdapterItems(ListAdapter adapter){
        List<ServerInfo> items = new ArrayList<>(adapter.getCount());
        for (int i=0; i < adapter.getCount(); ++i){
            items.add((ServerInfo) adapter.getItem(i));
        }

        return items;
    }
}
