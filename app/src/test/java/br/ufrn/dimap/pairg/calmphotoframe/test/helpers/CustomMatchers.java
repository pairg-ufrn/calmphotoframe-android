package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.core.IsCollectionContaining;

public class CustomMatchers {

    /** Got from: http://stackoverflow.com/a/29610402*/
    public static <T> Matcher<Iterable<? super T>> exactlyNItems(final int n, final Matcher<? super T> elementMatcher) {
        return new IsCollectionContaining<T>(elementMatcher) {
            @Override
            protected boolean matchesSafely(Iterable<? super T> collection, Description mismatchDescription) {
                int count = 0;
                boolean isPastFirst = false;

                for (Object item : collection) {

                    if (elementMatcher.matches(item)) {
                        count++;
                    }
                    if (isPastFirst) {
                        mismatchDescription.appendText(", ");
                    }
                    elementMatcher.describeMismatch(item, mismatchDescription);
                    isPastFirst = true;
                }

                if (count != n) {
                    String countText = (count == 0 ? "none" : String.valueOf(count));
                    mismatchDescription.appendText(". Expected exactly " + n + " but got " + countText);
                }
                return count == n;
            }
        };
    }
}
