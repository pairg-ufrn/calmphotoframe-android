package br.ufrn.dimap.pairg.calmphotoframe.test.activities;


import android.support.v4.app.FragmentManager;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginController;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.LoginMode;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.ConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.LoginConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.CaptureAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockingHelper;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ServerInfoIsLocalMatcher;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.UserNotifierMockup;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.LoginSignupFragment;

import static br.ufrn.dimap.pairg.calmphotoframe.test.helpers.CustomMatchers.exactlyNItems;
import static br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.LoginSignupFragment.SubmitListener;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

public class LoginControllerTest extends SimpleScenarioTest<LoginControllerTest.LoginControllerSteps>{

    @Test
    public void doLocalhostLogin(){
        given()
            .that_the_controller_is_configured()
            .and().some_login_info();

        when()
            .a_login_is_made_to("localhost");

        then()
            .the_login_command_should_be_called_with_correspondent_server_and_login_info();
    }

    @Test
    public void local_server_should_be_on_login_servers_list(){
        given()
            .that_the_controller_is_configured()
            .and().the_local_server_is_not_on_saved_servers_list();

        when()
            .getting_the_servers_list();

        then()
            .the_saved_servers_should_be_on_servers_list()
            .and().the_servers_list_should_include_the_local_server();
    }

    @Test
    public void local_server_already_on_servers_list(){
        given()
                .that_the_controller_is_configured()
                .and().the_servers_list_contains_a_local_server_with_no_name();

        when()
                .getting_the_servers_list();

        then()
                .the_servers_list_should_include_the_local_server()
                .but().the_local_server_should_not_be_duplicated()
                .and().the_local_server_should_have_a_name();
    }

    @Test
    public void error_on_login(){
        given()
                .that_the_controller_is_configured();

        when()
                .a_login_is_made_to("anyserver.com")
                .but().the_login_fails_with_an_api_error(401);

        then()
                .the_user_should_be_notified_that_there_is_an_error_with_login_or_password();
    }


    @Test
    public void error_on_signup(){
        given()
                .that_the_controller_is_configured();

        when()
                .a_signup_attempt_is_made_to("anyserver.com")
                .but().the_signup_fails_with_an_api_error(500);

        then()
                .the_user_should_be_notified_that_there_is_an_internal_error_on_server();
    }

    public static class LoginControllerSteps extends Stage<LoginControllerSteps>{

        @Mock
        ApiClient apiClient;

        @Mock
        BaseActivity baseActivity;

        @Mock(answer = Answers.RETURNS_MOCKS)
        FragmentManager fragmentManager;

        @Mock
        LoginSignupFragment loginFragment;

        @Mock
        LoginConnectionCommand loginCommand;
        @Mock
        ConnectionCommand<? super User> signupCommand;


        UserNotifierMockup notifierMockup;

        LoginController loginController;

        LoginInfo loginInfo;
        Endpoint serverEndpoint;
        ConnectionInfo connectionInfo;

        List<ServerInfo> savedServers;
        List<ServerInfo> capturedServersList;

        CaptureAnswer<Void> captureSubmitListener = new CaptureAnswer<>();
        CaptureAnswer<Void> captureLoginCommand = new CaptureAnswer<>();
        CaptureAnswer<Void> captureSignupCommand = new CaptureAnswer<>();

        public LoginControllerSteps(){
            MockitoAnnotations.initMocks(this);

            notifierMockup = new UserNotifierMockup();
            notifierMockup.mockup(baseActivity);

            mockFragments();
            MockingHelper.mockLocalPersistence();
            ApiClient.setInstance(apiClient);

            MockingHelper.getTextMock(baseActivity, R.string.localserver_address, "localhost");
            MockingHelper.getTextMock(baseActivity, R.string.localserver_name, "Este dispositivo");

            loginController = new LoginController(baseActivity);
            loginController.loginCommand(loginCommand);
            loginController.signupCommand(signupCommand);
        }

        @SuppressWarnings("unchecked")
        public void mockFragments() {
            Mockito.doReturn(fragmentManager).when(baseActivity).getSupportFragmentManager();
            Mockito.doReturn("").when(baseActivity).getText(Mockito.anyInt());

            Mockito.doReturn(loginFragment).when(fragmentManager).findFragmentById(R.id.loginFragment);
            Mockito.doAnswer(captureSubmitListener)
                    .when(loginFragment).setSubmitListener(any(SubmitListener.class));

            Mockito.doAnswer(captureLoginCommand)
                    .when(loginCommand).execute(any(ConnectionInfo.class), any(ResultListener.class));

            Mockito.doAnswer(captureSignupCommand)
                    .when(signupCommand).execute(any(ConnectionInfo.class), any(ResultListener.class));
        }

        public LoginControllerSteps that_the_controller_is_configured(){
            loginController.setupView();
            return this;
        }

        public LoginControllerSteps some_login_info() {
            loginInfo = new LoginInfo("login", "password");
            return this;
        }

        public LoginControllerSteps a_login_is_made_to(String server) {
            submitIsDone(server, LoginMode.Login);

            return this;
        }

        public LoginControllerSteps a_signup_attempt_is_made_to(String serverAddress) {
            submitIsDone(serverAddress, LoginMode.Signup);
            return this;
        }

        protected void submitIsDone(String server, LoginMode loginMode) {
            SubmitListener submitListener = captureSubmitListener.getArg(0, 0);

            serverEndpoint = new Endpoint(server);
            connectionInfo = new ConnectionInfo(serverEndpoint, loginInfo);

            submitListener.onSubmit(connectionInfo, loginMode);
        }

        public LoginControllerSteps the_login_command_should_be_called_with_correspondent_server_and_login_info() {
            //noinspection unchecked
            verify(loginCommand)
                    .execute(any(ConnectionInfo.class), any(ResultListener.class));

            ConnectionInfo capturedConnectionInfo = captureLoginCommand.getArg(0, 0);

            assertThat(capturedConnectionInfo, is(notNullValue()));

            assertThat(capturedConnectionInfo.getEndpoint(), equalTo(serverEndpoint));
            assertThat(capturedConnectionInfo.getLoginInfo(), equalTo(loginInfo));

            ResultListener<?> capturedListener = captureLoginCommand.getArg(0, 1);
            assertThat(capturedListener, is(notNullValue()));

            return this;
        }

        public LoginControllerSteps the_local_server_is_not_on_saved_servers_list() {

            return mock_saved_servers(Arrays.asList(
                    new ServerInfo("name", "address.com"),
                    new ServerInfo("", "10.0.0.10"),
                    new ServerInfo("lorem ipsum", "www.lorem.ipsum.org")
            ));
        }

        public LoginControllerSteps the_servers_list_contains_a_local_server_with_no_name() {
            return mock_saved_servers(Arrays.asList(
                    new ServerInfo("name", "address.com"),
                    new ServerInfo("", "10.0.0.10"),
                    new ServerInfo("lorem ipsum", "www.lorem.ipsum.org"),
                    new ServerInfo("", "localhost")
            ));
        }

        protected LoginControllerSteps mock_saved_servers(List<ServerInfo> servers){
            savedServers = new ArrayList<>(servers);

            Mockito.doReturn(new ArrayList<>(savedServers))
                    .when(LocalPersistence.instance()).getConnectedServers();

            return this;
        }

        public LoginControllerSteps getting_the_servers_list() {
            capturedServersList = loginController.getServerInfos();

            return this;
        }

        public LoginControllerSteps the_servers_list_should_include_the_local_server() {
            assertThat(capturedServersList, hasItem(new ServerInfoIsLocalMatcher()));

            return this;
        }

        public LoginControllerSteps the_saved_servers_should_be_on_servers_list() {
            for (ServerInfo expected : savedServers){
                assertThat(capturedServersList, hasItem(expected));
            }
            return this;
        }

        public LoginControllerSteps the_local_server_should_not_be_duplicated() {
            assertThat(capturedServersList, exactlyNItems(1, new ServerInfoIsLocalMatcher()));

            return this;
        }

        public LoginControllerSteps the_local_server_should_have_a_name() {
            Matcher<ServerInfo> is_local_and_has_name =
                    both(new ServerInfoIsLocalMatcher())
                    .and(hasProperty("name", not(isEmptyString())));

            assertThat(capturedServersList, hasItem(is_local_and_has_name));
            return this;
        }



        public LoginControllerSteps the_login_fails_with_an_api_error(int statusCode) {
            ResultListener<? super Endpoint> listener = captureLoginCommand.getArg(0, 1);

            listener.onFailure(new ApiException().setStatusCode(statusCode));

            return this;
        }

        public LoginControllerSteps the_signup_fails_with_an_api_error(int statusCode) {
            verify(signupCommand).execute(any(ConnectionInfo.class), any(ResultListener.class));

            ResultListener<? super Endpoint> listener = captureSignupCommand.getArg(0, 1);

            assertThat(listener, is(notNullValue()));
            listener.onFailure(new ApiException().setStatusCode(statusCode));

            return this;
        }

        public LoginControllerSteps the_user_should_be_notified_that_there_is_an_error_with_login_or_password() {
            return verifyUserNotificationWithText(R.string.login_failure_unauthorized);
        }

        public LoginControllerSteps the_user_should_be_notified_that_there_is_an_internal_error_on_server() {
            return verifyUserNotificationWithText(R.string.login_failure_server_error);
        }

        protected LoginControllerSteps verifyUserNotificationWithText(int textId) {
            verify(notifierMockup.getNotificationBuilderMockup()).show();
            verify(notifierMockup.getNotificationBuilderMockup()).text(textId);
            return this;
        }
    }

}
