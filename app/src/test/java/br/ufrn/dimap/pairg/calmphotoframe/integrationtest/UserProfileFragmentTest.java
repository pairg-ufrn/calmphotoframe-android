package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ScenarioStage;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BaseArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ViewTestHelper;
import br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.actionmode.ActionModeHandler;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.ChangePasswordDialog;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment.UserPassword;
import br.ufrn.dimap.pairg.calmphotoframe.view.image.ImageLoader;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

import static br.ufrn.dimap.pairg.calmphotoframe.utils.ViewUtils.getView;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class UserProfileFragmentTest extends RoboelectricJGivenTestCase<UserProfileFragmentTest.ProfileFragmentSteps> {

    @ScenarioStage
    ProfileFragmentSteps steps;

    @Test
    public void change_name_scenario(){
        steps.
                given().that_the_fragment_is_configured_with_a_user_info()

                .when()
                    .the_name_input_gain_focus()

                .then()
                    .the_action_mode_should_start()

                .when()
                    .the_name_input_is_filled()
                    .and().the_ime_action_is_sent()

                .then()
                    .the_save_name_action_should_be_called_receiving_an_changed_user_as_input()
                    .and().the_name_input_should_keep_the_changes();
    }

    @Test
    public void cancel_change_name_scenario(){
        steps.
                given().that_the_fragment_is_configured_with_a_user_info()

                .when()
                    .the_name_input_gain_focus()
                    .and().the_name_input_is_edited()
                    .and().the_action_mode_is_finished()

                .then()
                    .the_name_input_should_return_to_previous_value();
    }

    @Test
    public void change_password_scenario(){
        steps.
                given()
                    .that_the_fragment_is_configured_with_a_user_info()

                .when()
                    .the_user_clicks_on_change_password_trigger_view()

                .then()
                    .a_dialog_to_change_the_password_should_open()

                .when()
                    .the_the_dialog_is_filled_with_a_valid_password()
                    .and().the_dialog_is_submitted()

                .then()
                    .the_save_password_action_should_be_executed_with_the_correspondent_password_info();
    }


    @Test
    public void change_photo_scenario(){
        steps.
                given()
                    .that_the_fragment_is_configured_with_a_user_info()
                    .and().a_change_photo_command_is_set()

                .when()
                    .the_change_photo_trigger_is_clicked()
                    .and().the_change_photo_command_delivers_a_successful_result()

                .then()
                    .the_new_image_should_be_loaded();
    }


    public static class ProfileFragmentSteps extends Stage<ProfileFragmentSteps>{
        User user = new User();

        UserProfileFragment profileFragment;
        ChangePasswordDialog changePassDialogFragment;

        String expectedPassword = "sample password";
        String changedUserName = "Somebody";

        @Mock
        ActionListener<UserPassword> changePasswordAction;

        @Mock
        ArgsCommand<User, User> saveNameCommand;

        @Mock
        ActionModeHandler actionModeHandler;

        @Mock
        BaseArgsCommand<User, User> changePhotoCommand;

        @Mock
        Drawable drawableResult;

        @Mock
        ImageLoader<User> userImageLoader;

        ActionMode capturedActionMode;

        EditText capturedNameEditor;

        public ProfileFragmentSteps(){
            MockitoAnnotations.initMocks(this);

            profileFragment = new UserProfileFragment();
        }

        public ProfileFragmentSteps that_the_fragment_is_configured_with_a_user_info() {
            configureUser();

            profileFragment.setUser(user);
            profileFragment.setChangePasswordAction(changePasswordAction);
            profileFragment.setSaveNameCommand(saveNameCommand);
            profileFragment.setUserImageLoader(userImageLoader);

            SupportFragmentTestUtil.startFragment(profileFragment, AppCompatActivity.class);

            profileFragment.getViewBinder().setActionModeHandler(actionModeHandler);

            Mockito.doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    ActionMode.Callback callback = (ActionMode.Callback)invocation.getArguments()[0];
                    AppCompatActivity activity = (AppCompatActivity)profileFragment.getActivity();

                    ActionMode mode = activity.startSupportActionMode(callback);
                    capturedActionMode = (mode == null ? null : Mockito.spy(mode));

                    return capturedActionMode;
                }
            }).when(actionModeHandler).startActionMode(Mockito.any(ActionMode.Callback.class));

            capturedNameEditor = extractNameEditor();


            return this;
        }

        private void configureUser(){
            user.setName("John Smith");
            user.setLogin("mylogin");
            user.setActive(true);
            user.setHasProfileImage(false);
        }

        public ProfileFragmentSteps the_user_clicks_on_change_password_trigger_view() {
            View changePasswordTrigger = getView(profileFragment.getView(), R.id.profile_changepassword_trigger);
            changePasswordTrigger.performClick();

            return this;
        }


        public ProfileFragmentSteps a_dialog_to_change_the_password_should_open() {
            String fragTag = UserProfileFragment.CHANGEPASSWORD_DIALOG;
            FragmentManager fragmentManager = profileFragment.getActivity().getSupportFragmentManager();
            Fragment frag = fragmentManager.findFragmentByTag(fragTag);

            assertThat("Change password fragment (tag= " + fragTag + ")",
                    frag, is(notNullValue()));
            assertThat(frag, is(instanceOf(ChangePasswordDialog.class)));

            changePassDialogFragment = (ChangePasswordDialog)frag;

            return this;
        }

        public ProfileFragmentSteps the_the_dialog_is_filled_with_a_valid_password() {
            assertThat(changePassDialogFragment.getPasswordEditor(), is(notNullValue()));
            assertThat(changePassDialogFragment.getConfirmPassEditor(), is(notNullValue()));

            changePassDialogFragment.getPasswordEditor().setText(expectedPassword);
            changePassDialogFragment.getConfirmPassEditor().setText(expectedPassword);

            assertThat("Password field is valid", changePassDialogFragment.verifyPassword(), is(true));
            assertThat("Confirm password field is valid",
                    changePassDialogFragment.verifyPasswordConfirmation(), is(true));

            return this;
        }

        public ProfileFragmentSteps the_dialog_is_submitted() {
            changePassDialogFragment.getConfirmButton().performClick();

            return this;
        }

        public ProfileFragmentSteps the_save_password_action_should_be_executed_with_the_correspondent_password_info() {
            ArgumentCaptor<UserPassword> passwordCaptor = ArgumentCaptor.forClass(UserPassword.class);

            verify(changePasswordAction).execute(passwordCaptor.capture());

            UserPassword capturedPass = passwordCaptor.getValue();

            assertThat(capturedPass, hasProperty("password", equalTo(expectedPassword)));
            assertThat(capturedPass, hasProperty("user", equalTo(user)));

            return this;
        }

        public ProfileFragmentSteps the_name_input_gain_focus() {
            boolean gainedFocus = getNameEditor().requestFocus();

            assertTrue("Name input gained focus", gainedFocus);

            return this;
        }

        public ProfileFragmentSteps the_name_input_is_filled() {
            getNameEditor().setText(changedUserName);
            return this;
        }

        public ProfileFragmentSteps the_ime_action_is_sent() {
            ViewTestHelper.pressImeActionButton(getNameEditor());

            return this;
        }

        public ProfileFragmentSteps the_save_name_action_should_be_called_receiving_an_changed_user_as_input() {
            ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

            //noinspection unchecked
            verify(saveNameCommand).execute(userCaptor.capture(), Mockito.any(ResultListener.class));

            User capturedUser = userCaptor.getValue();

            assertThat(capturedUser, is(notNullValue()));
            assertThat(capturedUser, hasProperty("name", equalTo(changedUserName)));

            return this;
        }

        public ProfileFragmentSteps the_action_mode_should_start() {
            verify(actionModeHandler).startActionMode(Mockito.any(ActionMode.Callback.class));

            return this;
        }

        @SuppressLint("SetTextI18n")
        public ProfileFragmentSteps the_name_input_is_edited() {
            getNameEditor().setText("other text");

            profileFragment.getActivity().onBackPressed();

            return this;
        }

        public ProfileFragmentSteps the_action_mode_is_finished() {
            this.the_action_mode_should_start();

            capturedActionMode.finish();

            return this;
        }

        public ProfileFragmentSteps the_name_input_should_keep_the_changes() {
            assertThat(getNameEditor().getText().toString(), equalTo(changedUserName));
            return this;
        }

        public ProfileFragmentSteps the_name_input_should_return_to_previous_value() {
            assertThat(getNameEditor().getText().toString(), is(equalTo(user.getName())));
            return this;
        }

        public ProfileFragmentSteps a_change_photo_command_is_set() {
            profileFragment.setChangePhotoCommand(changePhotoCommand);

            return this;
        }

        public ProfileFragmentSteps the_change_photo_trigger_is_clicked() {
            View changePhotoTrigger = ViewUtils.getView(profileFragment.getView(), R.id.profile_changePhoto_trigger);
            changePhotoTrigger.performClick();

            return this;
        }

        @SuppressWarnings("unchecked")
        public ProfileFragmentSteps the_change_photo_command_delivers_a_successful_result() {
            ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
            ArgumentCaptor<ResultListener> listenerCaptor = ArgumentCaptor.forClass(ResultListener.class);

            verify(changePhotoCommand).execute(
                    userCaptor.capture(), listenerCaptor.capture());

            User capturedUser = userCaptor.getValue();
            assertThat(capturedUser, is(notNullValue()));

            capturedUser = capturedUser.clone();
            capturedUser.setHasProfileImage(true);

            ResultListener capturedListener = listenerCaptor.getValue();
            assertThat(capturedListener, is(notNullValue()));

            Listeners.onResult(capturedListener, capturedUser);

            return this;
        }

        public ProfileFragmentSteps the_new_image_should_be_loaded() {
            verify(userImageLoader).loadImage(Mockito.any(User.class), Mockito.any(ImageView.class));
            return this;
        }

        protected EditText extractNameEditor() {
            View view = getView(profileFragment.getView(), R.id.profile_name_edit);
            if(view instanceof EditText){
                return (EditText)view;
            }
            TextInputLayout textInputLayout = (TextInputLayout)view;
            return textInputLayout.getEditText();
        }

        protected EditText getNameEditor() {
            return capturedNameEditor;
        }
    }
}
