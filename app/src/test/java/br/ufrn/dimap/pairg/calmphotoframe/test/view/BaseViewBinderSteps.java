package br.ufrn.dimap.pairg.calmphotoframe.test.view;

import com.tngtech.jgiven.Stage;

import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.LayoutMockup;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;


public abstract class BaseViewBinderSteps<SELF extends Stage<?>, T> extends Stage<SELF> {
    protected BinderViewHolder<T> binder;

    protected MockedApi mockedApi = new MockedApi();
    protected LayoutMockup layoutMockup = new LayoutMockup();

    protected T entity;

    protected abstract void setupLayout(LayoutMockup layoutMockup);
    protected abstract T buildEntity();
    protected abstract BinderViewHolder<T> buildBinder();

    public BaseViewBinderSteps(){
        mockedApi.setup();

        this.binder = buildBinder();
    }

    public SELF the_binder_view_is_configured() {
        setupLayout(layoutMockup);

        binder.setup(layoutMockup.getViewsRoot());
        return self();
    }


    public SELF there_is_a_valid_entity_instance() {
        entity = buildEntity();

        return self();
    }

    public SELF the_binder_was_bound() {
        this.binder.bind(entity);

        return self();
    }

}
