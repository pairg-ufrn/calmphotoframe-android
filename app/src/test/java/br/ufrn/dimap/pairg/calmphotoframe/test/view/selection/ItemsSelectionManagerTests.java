package br.ufrn.dimap.pairg.calmphotoframe.test.view.selection;

import android.view.View;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.As;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.ItemsSelectionManager;
import br.ufrn.dimap.pairg.calmphotoframe.view.multiselection.MultiSelectionListener;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class ItemsSelectionManagerTests extends SimpleScenarioTest<ItemsSelectionManagerTests.Steps>{

    @Test
    public void singleModeSelection(){
        given()
                .a_configured_selection_manager();
        given()
                .it_is_set_to_single_mode_selection();

        when()
                .the_view_at_position_n_is_selected(0)
        .then()
                .the_listeners_should_receive_the_onStart_selection_event()
                .and().the_listeners_should_receive_the_selection_change_event_for_item_x_with_y_selected_items(0, 1);

        when()
                .the_view_at_position_n_is_selected(1)
        .then()
                .the_listeners_should_receive_the_deselection_event_for_item_x_with_y_selected_items(0, 0)
                .and().the_listeners_should_receive_the_selection_change_event_for_item_x_with_y_selected_items(1, 1)
                .and().one_item_should_be_selected()
                .and().the_onFinishSelection_event_should_not_be_called();

        when()
                .the_view_at_position_n_is_selected(1)
        .then()
                .the_listeners_should_receive_the_deselection_event_for_item_x_with_y_selected_items(1, 0)
                .and().the_onFinishSelection_event_should_be_called();
    }

    public static class Steps extends Stage<Steps>{
        ItemsSelectionManager selectionManager;

        @Mock
        ItemsSelectionManager.ItemsView itemsView;

        @Mock
        MultiSelectionListener selectionListener;

        @Mock
        View viewMock;

        long[] viewsId;

        public Steps() {
            MockitoAnnotations.initMocks(this);

            Mockito.when(viewMock.isEnabled()).thenReturn(true);

            viewsId = new long[]{123l, 200l, 100l};

            Mockito.when(itemsView.getViewCount()).thenReturn(viewsId.length);
            Mockito.when(itemsView.getIdFromPosition(anyInt())).then(new Answer<Long>() {
                @Override
                public Long answer(InvocationOnMock invocation) throws Throwable {
                    int pos = (Integer) invocation.getArguments()[0];
                    return viewsId[pos];
                }
            });

            Mockito.when(itemsView.getViewAt(anyInt())).thenReturn(viewMock);
        }

        public Steps a_configured_selection_manager() {
            selectionManager = new ItemsSelectionManager();
            selectionManager.setItemsView(itemsView);

            selectionManager.addSelectionListener(selectionListener);
            return this;
        }

        public Steps it_is_set_to_single_mode_selection() {
            selectionManager.setMultiSelectionEnabled(false);
            return this;
        }

        @As("the view at position $pos is selected")
        public Steps the_view_at_position_n_is_selected(int pos) {
            return toggle_selection(pos);
        }

        public Steps the_listeners_should_receive_the_onStart_selection_event() {
            verify(selectionListener).onStartSelection();
            return this;
        }

        @As("the listeners should receive the 'selection change event' for item $ with $ selected items")
        public Steps the_listeners_should_receive_the_selection_change_event_for_item_x_with_y_selected_items(
                int viewPosition, int expectedSelectionCount)
        {
            return verifySelectionChangedEvent(viewPosition, expectedSelectionCount, true);
        }
        @As("the listeners should receive the 'deselection change event' for item $ with $ selected items")
        public Steps the_listeners_should_receive_the_deselection_event_for_item_x_with_y_selected_items(
                int viewPosition, int expectedSelectionCount)
        {
            return verifySelectionChangedEvent(viewPosition, expectedSelectionCount, false);
        }

        public Steps verifySelectionChangedEvent(int viewPosition, int expectedSelectionCount, boolean selected) {
            verify(selectionListener).onItemSelectionChanged(expectedSelectionCount, viewPosition, viewsId[viewPosition], selected);
            return this;
        }

        private Steps toggle_selection(int position) {
            selectionManager.onItemLongClick(viewMock, position, viewsId[position]);
            return this;
        }

        public Steps one_item_should_be_selected() {
            assertThat(selectionManager.getSelectionCount(), is(equalTo(1)));
            return this;
        }


        public Steps the_onFinishSelection_event_should_not_be_called() {
            verify(selectionListener, never()).onEndSelection();
            return this;
        }

        public Steps the_onFinishSelection_event_should_be_called() {
            verify(selectionListener).onEndSelection();
            return null;
        }
    }
}
