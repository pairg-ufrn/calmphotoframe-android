package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import com.google.common.reflect.TypeToken;
import com.tngtech.jgiven.annotation.ScenarioStage;
import com.tngtech.jgiven.config.ConfigValue;
import com.tngtech.jgiven.impl.Scenario;
import com.tngtech.jgiven.impl.ScenarioBase;
import com.tngtech.jgiven.junit.JGivenClassRule;
import com.tngtech.jgiven.junit.JGivenMethodRule;
import com.tngtech.jgiven.junit.ScenarioModelHolder;
import com.tngtech.jgiven.report.analysis.CaseArgumentAnalyser;
import com.tngtech.jgiven.report.model.ReportModel;
import com.tngtech.jgiven.report.text.PlainTextReporter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

public abstract class RoboelectricJGivenTestCase<STEPS> extends RobolectricTestCase {
    @ClassRule
    public static final JGivenClassRule writerRule = new JGivenClassRule();

    @Rule
    public final JGivenMethodRule scenarioRule = new JGivenMethodRule(createScenario());

    static Class testClass;

    @SuppressWarnings( { "serial", "unchecked" } )
    protected Scenario<STEPS, STEPS, STEPS> createScenario() {
        Class<STEPS> stepsClass = (Class<STEPS>) new TypeToken<STEPS>( getClass() ) {}.getRawType();
        return Scenario.create( stepsClass );
    }

    @Before
    public void setupClass(){
        testClass = getClass();
    }

    @AfterClass
    public static void showReport(){
        ReportModel reportModel = ScenarioModelHolder.getInstance().getReportModel(testClass);
        reportModel.setClassName(testClass.getSimpleName());
        printReport(reportModel);
    }

    public static void printReport(ReportModel model ) {
        if( model == null || model.getScenarios().isEmpty() ) {
            System.out.println("model empty");
            return;
        }

        new CaseArgumentAnalyser().analyze( model );

        new PlainTextReporter(ConfigValue.TRUE).write( model ).flush();
    }
}
