package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tngtech.jgiven.Stage;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ObjectMapperConfiguration;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiEndpointHolder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiRouteBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.RequestBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.SessionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.HttpConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.cache.PhotoCache;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.impl.client.BasicCookieStore;

import static org.mockito.Mockito.doReturn;

public class BaseApiSteps<SELF extends BaseApiSteps<SELF>> extends Stage<SELF> {
    protected ApiClient apiClient;

    @Mock
    protected LocalPersistence persistence;

    @Mock
    protected Context context;

    @Mock
    protected HttpConnector httpConnector;

    protected CookieStore cookieStore;

    @Mock
    protected PhotoCache photoCache;

    protected ObjectMapper objectMapper;

    protected ApiRouteBuilder apiBuilder;

    protected ApiConnector apiConnector;

    protected ApiEndpointHolder apiEndpointHolder;
    protected SessionInfo sessionInfo;
    protected User loggedInUser;

    private List<RequestBuilder<?,?>> capturedRequestBuilders;

    @Mock
    protected ResultListener<Object> listenerMockup;

    public BaseApiSteps() {
        init();
    }

    /* *****************************************************************************************/

    public SELF a_logged_in_user() {
        return a_logged_in_user(new User(12345l, "myLogin"));
    }

    public SELF a_logged_in_user(User user) {
        this.sessionInfo = new SessionInfo();
        this.loggedInUser = user;
        return self();
    }

    public SELF that_the_client_is_disconnected() {
        apiEndpointHolder.set(null);
        return self();
    }
    public SELF that_the_client_is_connected(ApiEndpoint endpoint) {
        apiEndpointHolder.set(endpoint);
        return self();
    }

    public SELF a_logged_in_admin() {
        User admin = new User(123L, "adminlogin", true, true);
        return a_logged_in_user(admin);
    }

    /* *****************************************************************************************/

    protected void init() {
        MockitoAnnotations.initMocks(this);

        capturedRequestBuilders = new ArrayList<>();

        cookieStore = new BasicCookieStore();
        apiBuilder = new ApiRouteBuilder();
        objectMapper = ObjectMapperConfiguration.build();
        apiEndpointHolder = new ApiEndpointHolder();

        setup_mockups();

        buildApiClient();

        ApiClient.setInstance(apiClient);
    }

    private void setup_mockups() {
        doReturn(cookieStore).when(httpConnector).getCookieStore();

        apiConnector = Mockito.spy(new ApiConnector(httpConnector, apiBuilder, objectMapper, photoCache));

        doReturn(1L).when(persistence).getCurrentPhotoContext();
        doReturn(1L).when(persistence).getCurrentPhotoContext(Mockito.any(ApiClient.class));
        doReturn(true).when(persistence).storeCurrentPhotoContext(Mockito.anyLong());
        LocalPersistence.instance(persistence);
    }

    private void buildApiClient() {
        apiClient = Mockito.spy(new ApiClient(apiConnector, apiEndpointHolder));
    }

    /* *****************************************************************************************/


    protected <T> RequestBuilderAnswer<T> mockRequestFor(String url) {
        RequestBuilderAnswer<T> answer = new RequestBuilderAnswer<>();

        Mockito.doAnswer(answer)
                .when(apiConnector).request(url);

        return answer;
    }

    public <T> RequestBuilderAnswer<T> requestBuilderAnswer(){
        return new RequestBuilderAnswer<>();
    }

    public List<RequestBuilder<? , ?>> capturedRequests(){
        return capturedRequestBuilders;
    }

    public RequestBuilder<?, ?> lastRequest() {
        if(capturedRequests().isEmpty()){
            throw new IllegalStateException("There was no captured requests");
        }
        return capturedRequests().get(capturedRequests().size() - 1);
    }

    /* *****************************************************************************************/

    protected class RequestBuilderAnswer<T> implements Answer {
        T result;
        RequestBuilder<?,? super T> requestBuilder;

        RequestBuilderAnswer(){
            this(null);
        }
        RequestBuilderAnswer(T result){
            this.result = result;
            this.requestBuilder = Mockito.spy(new RequestBuilder<Object, Object>(apiConnector));

            Mockito.doReturn(null).when(requestBuilder).send();
        }

        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            String url = invocation.getArgumentAt(0, String.class);
            requestBuilder.url(url);

            capturedRequestBuilders.add(requestBuilder);

            return requestBuilder;
        }

        public RequestBuilderAnswer successfulResponse(){
            return successfulResponse(null);
        }
        public RequestBuilderAnswer successfulResponse(T value){
            this.result = value;
            Mockito.doAnswer(new SendRequestAnswer<>(value, requestBuilder))
                    .when(requestBuilder).send();

            return this;
        }

        public RequestBuilder<?,? super T> requestBuilderMockup(){
            return requestBuilder;
        }
    }
}
