package br.ufrn.dimap.pairg.calmphotoframe.test.photoframe;


import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ServerInfoIsLocalMatcher;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ServerInfoTest {

    @Test
    public void serverInfoFromString(){
//        Assert.assertNotNull(ServerInfo.fromText("name [address]"));
        assertFromTextConversion("address [name]", "name", "address");
        assertFromTextConversion("192.168.0.1 [name]", "name", "192.168.0.1");
        assertFromTextConversion("www.host.io [name]", "name", "www.host.io");
        assertFromTextConversion("www.host.io [mult word name]", "mult word name", "www.host.io");
        assertFromTextConversion("www.host.io", "", "www.host.io");
        assertFromTextConversion(" localhost ", "", "localhost");

        assertThat(ServerInfo.fromText("[only name]"), is(nullValue()));
        assertThat(ServerInfo.fromText("this is not an address"), is(nullValue()));
    }

    @Test
    public void isLocalTest() throws UnknownHostException, Endpoint.InvalidUrlException {
        Matcher<ServerInfo> isLocal = new ServerInfoIsLocalMatcher();

        assertThat(new ServerInfo("", "127.0.0.1"), isLocal);
        assertThat(new ServerInfo("", "[::1]"), isLocal);
        assertThat(new ServerInfo("", "localhost"), isLocal);
    }

    protected void assertFromTextConversion(CharSequence text, String name, String address) {
        ServerInfo converted = ServerInfo.fromText(text);
        assertThat("'" + text + "' can be converted to ServerInfo", converted, is(not(nullValue())));
        assertThat(converted.getName(), is(equalTo(name)));
        assertThat(converted.getAddress(), is(equalTo(address)));
    }
}
