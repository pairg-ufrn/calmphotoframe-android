package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class ViewTestHelper {

    //adapted from espresso code
    public static void pressImeActionButton(View view) {
        EditorInfo editorInfo = new EditorInfo();
        InputConnection inputConnection = view.onCreateInputConnection(editorInfo);
        if (inputConnection == null) {
            throw new IllegalStateException("View does not support input methods");
        }

        int actionId = editorInfo.actionId != 0 ? editorInfo.actionId :
                editorInfo.imeOptions & EditorInfo.IME_MASK_ACTION;

        if (actionId == EditorInfo.IME_ACTION_NONE) {
            throw new IllegalStateException("No available action on view");
        }

        if (!inputConnection.performEditorAction(actionId)) {
            throw new RuntimeException(String.format(
                    "Failed to perform action %#x. Input connection no longer valid", actionId));
        }
    }
}
