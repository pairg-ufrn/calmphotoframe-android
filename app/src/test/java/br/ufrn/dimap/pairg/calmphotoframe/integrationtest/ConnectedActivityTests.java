package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ScenarioStage;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.shadows.support.v4.ShadowLocalBroadcastManager;
import org.robolectric.shadows.support.v4.ShadowLocalBroadcastManager.Wrapper;
import org.robolectric.shadows.support.v4.Shadows;
import org.robolectric.util.ActivityController;

import java.net.SocketTimeoutException;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.activities.ConnectedActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.ConnectionHelper;
import br.ufrn.dimap.pairg.calmphotoframe.activities.LoginActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.NotificationBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.PhotoFrameControllerService;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameBroadcastReceiver;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.UserNotifierMockup;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

public class ConnectedActivityTests extends RoboelectricJGivenTestCase<ConnectedActivityTests.ConnectedActivitySteps>{

    @ScenarioStage
    ConnectedActivitySteps steps;


    @Test
    public void checkConnectionToRemoteServer(){
        steps
            .given()
                    .a_ConnectedActivity_is_active()

            .given()
                    .the_api_client_is_connected_to_a_remote_server()

            .when()
                    .the_activity_starts()

            .then()
                    .the_activity_should_check_if_it_has_a_valid_session();
    }

    @Test
    public void checkLocalServerOnActivityRestart(){

        steps
            .given()
                    .a_ConnectedActivity_is_active()

            .given()
                    .the_api_client_is_connected_to_a_local_server()
                    .but().the_server_was_finished()

            .when()
                    .the_activity_starts()

            .then()
                    .it_should_restart_the_server()

            .when()
                    .the_local_server_starts()

            .then()
                    .the_activity_should_check_if_it_has_a_valid_session()
        ;
    }

    @Test
    public void restartLocalServerWhenClosed(){
        steps
            .given()
                    .the_api_client_is_connected_to_a_local_server()
                    .and().a_connected_activity_is_visible()

            .when()
                    .the_local_server_is_finished()

            .then()
                    .the_activity_should_show_an_snackbar_with_option_to_restart_the_service()

            .when()
                    .the_snackbar_action_is_triggered()

            .then()
                    .the_local_server_should_be_restarted();
    }

    @Test
    public void monitorLocalServerEvents(){
        steps
                .given()
                        .the_api_client_is_connected_to_a_local_server()

                .when()
                        .a_ConnectedActivity_starts()
                .then()
                        .it_should_register_to_listen_the_local_server_events()

                .when()
                        .the_activity_stops()
                .then()
                        .it_should_unregister_its_listener();
    }

    @Test
    public void onSessionExpired(){
        steps
                .given()
                        .the_api_client_is_connected_to_a_remote_server()
                        .and().a_ConnectedActivity_is_active()
                .when()
                        .the_activity_starts()
                        .and().the_client_session_was_expired()
                .then()
                        .the_activity_should_show_an_snackbar_with_option_to_do_login()

                .when()
                        .the_snackbar_action_is_triggered()
                .then()
                        .the_login_activity_should_be_started();
    }


    @Test
    public void onConnectionError(){
        steps
                .given()
                        .the_api_client_is_connected_to_a_remote_server()
                        .and().a_ConnectedActivity_is_active()
                .when()
                        .the_activity_starts()
                        .but().a_connection_error_happens()
                .then()
                        .the_activity_should_show_an_snackbar_with_option_to_retry()

                .when()
                        .the_snackbar_action_is_triggered()
                .then()
                        .the_activity_should_check_the_connection_again();
    }
    public static class ConnectedActivitySteps extends Stage<ConnectedActivitySteps>{
        ActivityController<ExposedConnectedActivity> activityController;
//        ExposedConnectedActivity activity = Mockito.spy(new ExposedConnectedActivity());
        ExposedConnectedActivity activity;

        MockedApi mockedApi = new MockedApi();

        ApiEndpoint apiEndpoint;

        Runnable capturedAction;

        public ConnectedActivitySteps(){
            mockedApi.setup();
        }

        /* ************************************* Given ********************************************/

        public ConnectedActivitySteps a_ConnectedActivity_is_active() {
            activityController = Robolectric.buildActivity(ExposedConnectedActivity.class).create();
            activity = activityController.get();

            return this;
        }

        public ConnectedActivitySteps a_connected_activity_is_visible() {
            this.a_ConnectedActivity_is_active()
                .activityController.start().resume();

            return this;
        }

        public ConnectedActivitySteps a_ConnectedActivity_starts() {
            if(activity == null){
                this.a_ConnectedActivity_is_active();
            }

            this.activityController.start();

            return this;
        }

        public ConnectedActivitySteps the_api_client_is_connected_to_a_local_server() {
            apiEndpoint = new ApiEndpoint();
            mockedApi.mockApiEndpoint(apiEndpoint);

            return this;
        }

        public ConnectedActivitySteps the_api_client_is_connected_to_a_remote_server() {
            apiEndpoint = new ApiEndpoint("www.example.com");
            mockedApi.mockApiEndpoint(apiEndpoint);

            return this;
        }

        public ConnectedActivitySteps the_server_was_finished() {
            return this;
        }


        /* ************************************* When ********************************************/

        public ConnectedActivitySteps the_activity_starts() {
            activityController.start();
            return this;
        }


        public ConnectedActivitySteps the_local_server_starts() {
            sendBroadcast(new Intent(PhotoFrameControllerService.START_SERVER_ACTION));

            return this;
        }

        public ConnectedActivitySteps the_local_server_is_finished() {
            sendBroadcast(new Intent(PhotoFrameControllerService.STOP_SERVER_ACTION));

            return this;
        }

        public ConnectedActivitySteps the_snackbar_action_is_triggered() {
            capturedAction.run();

            return this;
        }

        public ConnectedActivitySteps the_activity_stops() {
            activityController.stop();
            return this;
        }

        public ConnectedActivitySteps the_client_session_was_expired() {
            onCheckConnectionError(new ApiException().setStatusCode(401));
            return this;
        }

        public ConnectedActivitySteps a_connection_error_happens() {
            onCheckConnectionError(new SocketTimeoutException());
            return this;
        }

        @SuppressWarnings("unchecked")
        protected void onCheckConnectionError(Throwable checkConnectionError) {
            ArgumentCaptor<ResultListener> listenerCaptor = ArgumentCaptor.forClass(ResultListener.class);

            verify(mockedApi.sessionApi).checkConnection(listenerCaptor.capture());

            listenerCaptor.getValue().onFailure(checkConnectionError);
        }

        /* ************************************* Then ********************************************/


        public ConnectedActivitySteps the_activity_should_show_an_snackbar_with_option_to_restart_the_service() {
            checkSnackbarWithAction(true);
            return this;
        }

        public ConnectedActivitySteps the_activity_should_show_an_snackbar_with_option_to_do_login() {
            checkSnackbarWithAction(true);
            return this;
        }
        public ConnectedActivitySteps the_activity_should_show_an_snackbar_with_option_to_retry() {
            checkSnackbarWithAction(true);
            return this;
        }

        @SuppressWarnings("unchecked")
        public ConnectedActivitySteps the_activity_should_check_the_connection_again() {
            verify(mockedApi.sessionApi, times(2)).checkConnection(Mockito.notNull(ResultListener.class));
            return this;
        }

        @SuppressWarnings("unchecked")
        public ConnectedActivitySteps the_activity_should_check_if_it_has_a_valid_session() {
            verify(mockedApi.sessionApi).checkConnection(Mockito.notNull(ResultListener.class));

            return this;
        }

        public ConnectedActivitySteps it_should_register_to_listen_the_local_server_events() {
            checkHasServerListener(true);
            return this;
        }

        public ConnectedActivitySteps it_should_unregister_its_listener() {
            checkHasServerListener(false);
            return this;
        }


        public ConnectedActivitySteps the_local_server_should_be_restarted() {
            this.it_should_restart_the_server();
            return this;
        }
        public ConnectedActivitySteps it_should_restart_the_server() {
            Intent capturedIntent = shadowOf(activity).getNextStartedService();

            Class clazz = PhotoFrameControllerService.class;

            assertIntentClass(capturedIntent, clazz);

            assertThat(capturedIntent.getAction(), equalTo(PhotoFrameControllerService.START_SERVER_ACTION));

            return this;
        }

        public ConnectedActivitySteps the_login_activity_should_be_started() {
            Intent intent = shadowOf(activity).getNextStartedActivity();

            assertIntentClass(intent, LoginActivity.class);
            return this;
        }

        /* *****************************************************************************************/

        protected void assertIntentClass(Intent capturedIntent, Class expectedClass) {
            assertThat(capturedIntent, is(notNullValue()));

            String serviceClassName = capturedIntent.getComponent().getClassName();
            assertThat(serviceClassName, is(equalTo(expectedClass.getName())));
        }

        protected void sendBroadcast(Intent intent) {
            ShadowLocalBroadcastManager broadcastManager = getBroadcastManager();

            broadcastManager.sendBroadcast(intent);
        }

        protected ShadowLocalBroadcastManager getBroadcastManager() {
            return Shadows.shadowOf(LocalBroadcastManager.getInstance(activity));
        }

        protected void checkSnackbarWithAction(boolean indefinite) {
            NotificationBuilder notificationBuilder = activity.userNotifierMockup.getNotificationBuilderMockup();

            ArgumentCaptor<Runnable> actionCaptor = ArgumentCaptor.forClass(Runnable.class);

            verify(notificationBuilder).show();
            verify(notificationBuilder).action(any(CharSequence.class), actionCaptor.capture());

            if(indefinite){
                verify(notificationBuilder).duration(NotificationBuilder.Duration.Indefinite);
            }

            capturedAction = actionCaptor.getValue();
            assertThat(capturedAction, is(notNullValue()));
        }


        public void checkHasServerListener(boolean shouldHaveListener) {
            List<Wrapper> receivers = getBroadcastManager().getRegisteredBroadcastReceivers();

            Matcher<Object> hasServerStatusListener =
                    both(is(instanceOf(PhotoFrameBroadcastReceiver.class)))
                            .and(hasProperty("serverListener", is(instanceOf(ConnectionHelper.class))));

            BroadcastReceiverMatcher startServerMatcher =
                    aReceiverRegisteredFor(PhotoFrameControllerService.START_SERVER_ACTION)
                        .that(hasServerStatusListener);
            BroadcastReceiverMatcher stopServerMatcher =
                    aReceiverRegisteredFor(PhotoFrameControllerService.STOP_SERVER_ACTION)
                        .that(hasServerStatusListener);


            if(shouldHaveListener) {
                assertThat(receivers, hasItem(startServerMatcher.that(hasServerStatusListener)));
                assertThat(receivers, hasItem(stopServerMatcher.that(hasServerStatusListener)));
            }
            else{
                assertThat(receivers, not(hasItem(startServerMatcher)));
                assertThat(receivers, not(hasItem(stopServerMatcher)));
            }
        }

        BroadcastReceiverMatcher aReceiverRegisteredFor(String action){
            return new BroadcastReceiverMatcher(action);
        }


        public static class ExposedConnectedActivity extends ConnectedActivity {

            public UserNotifierMockup userNotifierMockup = new UserNotifierMockup();

            @Override
            public void onRestart() {
                super.onRestart();
            }

            @Override
            public NotificationBuilder notifyUser() {
                return userNotifierMockup.getUserNotifierMockup().notifyUser();
            }

            @Override
            public NotificationBuilder notifyUser(CharSequence text) {
                return userNotifierMockup.getUserNotifierMockup().notifyUser(text);
            }

            @Override
            protected void refreshEndpointView(ApiEndpoint apiEndpoint)
            {}
        }
    }
}
