package br.ufrn.dimap.pairg.calmphotoframe.test.commands;

import android.content.Context;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.As;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collection;

import br.ufrn.dimap.pairg.calmphotoframe.controller.Builder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photos.InsertImagesCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.common.ProgressNotifier;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.PhotosApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ImageContentBuilderMocker;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.InOrderRequests;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InsertImagesCommandTests extends SimpleScenarioTest<InsertImagesCommandTests.InsertImagesSteps>{

    @Test
    public void imageRequestsShouldBeQueued(){
        given()
                .a_insert_images_command()
                .with().the_concurrency_level_set_to(2)

        .given()
                .there_are_many_images_to_be_inserted()
        .when()
                .executing_this_command()
        .then()
                .the_first_n_insert_image_requests_should_be_made(2);

        when()
                .those_requests_finish()
        .then()
                .the_next_n_insert_image_requests_should_be_made(2);
    }

    @Test
    public void listenImageInsertionProgress(){
        given()
                .a_insert_images_command()
        .given()
                .there_are_many_images_to_be_inserted()
        .when()
                .executing_this_command_passing_a_progress_listener_and_a_result_listener()
        .then()
                .the_result_listener_onStart_should_be_called()
                .and().the_progress_listener_onStart_should_be_called()

        .when()
                .one_request_finishes()
        .then()
                .the_progress_listener_onResult_should_be_called()

        .when()
                .one_request_fails()
        .then()
                .the_progress_listener_onFailure_should_be_called()

        .when()
                .all_requests_finish()
        .then()
                .the_listener_onResult_should_be_called_with_a_collection_containing_all_succeded_responses();

    }

    public static class InsertImagesSteps extends Stage<InsertImagesSteps> {
        InsertImagesCommand insertImagesCommand;

        MockedApi mockedApi = new MockedApi();
        ImageContentBuilderMocker imageContentBuilderMocker = new ImageContentBuilderMocker();

        @Mock
        Context context;
        @Mock
        ProgressNotifier progressNotifier;

        @Mock
        ResultListener<Collection<Photo>> listenerMockup;

        @Mock
        ResultListener<Photo> progressListener;
        InOrder progressListenerCalls;

        Collection<String> imagePaths;

        InOrderRequests<PhotosApi> inOrderRequests;


        public InsertImagesSteps(){
            MockitoAnnotations.initMocks(this);

            mockedApi.setup();
            imageContentBuilderMocker.setup();

            inOrderRequests = new InOrderRequests<>(mockedApi.photosApi)
                    .requestResponse(mock(RequestHandler.class));

            progressListenerCalls = inOrder(progressListener);

            Mockito.when(mockedApi.photosApi.insert(
                            any(ImageContent.class), any(Long.class), any(ResultListener.class)))
                    .then(inOrderRequests.getCaptureRequest());

            insertImagesCommand = new InsertImagesCommand(context);
            insertImagesCommand.setNotifierBuilder(new Builder<ProgressNotifier>() {
                @Override
                public ProgressNotifier build() {
                    return progressNotifier;
                }
            });

//            doReturn("Sample %1$d %2$d").when(context).getString(R.string.insertPhotos_progress);
        }

        public InsertImagesSteps there_are_many_images_to_be_inserted() {
            imagePaths = new ArrayList<>();
            for (int i=0; i < 10; ++i){
                imagePaths.add(String.format("file:///sample%d.jpeg", i + 1));
            }
            return this;
        }

        public InsertImagesSteps a_insert_images_command() {
            return this;
        }

        public InsertImagesSteps the_concurrency_level_set_to(int level) {
            insertImagesCommand.setConcurrencyLevel(level);

            return this;
        }

        public InsertImagesSteps executing_this_command() {
            insertImagesCommand.execute(imagePaths, listenerMockup);

            return this;
        }

        public InsertImagesSteps executing_this_command_passing_a_progress_listener_and_a_result_listener() {
            Mockito.when(listenerMockup.isListening()).thenReturn(true);
            Mockito.when(progressListener.isListening()).thenReturn(true);

            insertImagesCommand.execute(imagePaths, listenerMockup, progressListener);
            return this;
        }

        public InsertImagesSteps those_requests_finish() {
            //limit the number of requests to finish, because new ones will be made
            int requestsToFinish = inOrderRequests.getCaptureRequest().callsCount();

            finishRequests(requestsToFinish);
            return this;
        }

        @As("the first $ insert image requests should be made")
        public InsertImagesSteps the_first_n_insert_image_requests_should_be_made(int numImages) {
            inOrderRequests.verify(times(numImages))
                    .insert(any(ImageContent.class), any(Long.class), any(ResultListener.class));

            return this;
        }

        @As("the next $ insert image requests should be made")
        public InsertImagesSteps the_next_n_insert_image_requests_should_be_made(int numImages) {
            return this.the_first_n_insert_image_requests_should_be_made(numImages);
        }



        public InsertImagesSteps the_result_listener_onStart_should_be_called() {
            verify(listenerMockup).onStart();
            return this;
        }
        public InsertImagesSteps the_progress_listener_onStart_should_be_called() {
            verify(progressListener).onStart();
            return this;
        }

        public InsertImagesSteps one_request_finishes() {
            finishRequests(1);
            return this;
        }

        public InsertImagesSteps the_progress_listener_onResult_should_be_called() {
            verify(progressListener).onResult(any(Photo.class));
            return this;
        }

        public InsertImagesSteps one_request_fails() {
            failRequests(1);
            return this;
        }

        public InsertImagesSteps the_progress_listener_onFailure_should_be_called() {
            verify(progressListener).onFailure(any(Throwable.class));
            return this;
        }

        public InsertImagesSteps all_requests_finish() {
            finishRequests(imagePaths.size() - inOrderRequests.getFinishCount());
            return this;
        }

        public InsertImagesSteps the_listener_onResult_should_be_called_with_a_collection_containing_all_succeded_responses() {
            ArgumentCaptor<Collection> captor = ArgumentCaptor.forClass(Collection.class);

            verify(listenerMockup).onResult(captor.capture());

            assertThat(captor.getValue().size(), equalTo(inOrderRequests.getSuccessCount()));

            return this;
        }


        public void finishRequests(int requestsToFinish) {
            inOrderRequests.succeedRequests(requestsToFinish, mock(Photo.class));
        }

        public void failRequests(int count) {
            inOrderRequests.failRequests(count, mock(Throwable.class));
        }


    }
}
