package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.RequestBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;

public class RequestAnswer<T> implements Answer {
    T result;
    Throwable error;

    public RequestAnswer() {
        this(null);
    }
    public RequestAnswer(T result) {
        this.result = result;
    }

    public RequestAnswer<T> fail(Throwable error){
        this.error = error;
        return this;
    }

    @Override
    public Object answer(InvocationOnMock invocation) throws Throwable {
        for (Object argument : invocation.getArguments()) {
            if (argument != null && argument instanceof ResultListener) {
                ResultListener<? super T> listener = (ResultListener<? super T>) argument;

                if(error == null) {
                    listener.onResult(result);
                }
                else{
                    listener.onFailure(error);
                }
            }
        }
        return new FinishedRequestHandler();
    }
}
