package br.ufrn.dimap.pairg.calmphotoframe.test.commands;


import android.annotation.SuppressLint;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.controller.Builder;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.BasePhotoCollectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.photocollection.ConfirmAndRemoveItemsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.PhotoCollectionsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.PhotosApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.InOrderRequests;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.view.widgets.dialogs.SimpleMaterialDialogFragment;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class RemoveItemsCommandTests extends SimpleScenarioTest<RemoveItemsCommandTests.RemoveItemsSteps>{

    @Test
    public void removeMultiplePhotos(){
        given()
                .a_photo_component_selector()
                .with().some_photos_selected()
        .given()
                .a_configured_remove_items_command()
        .when()
                .executing_the_command()
        .then()
                .a_confirm_dialog_should_be_displayed()
        .when()
                .confirming_the_dialog()
        .then()
                .the_command_listener_onStart_should_be_called()
                .and().the_first_removePhoto_request_should_be_sent()
        .when()
                .those_requests_finish()
        .then()
                .the_progress_listener_onResult_method_should_be_called()
                .and().the_next_removePhoto_request_should_be_sent();
    }

    @Test
    public void removePhotosAndAlbums(){
        given()
                .a_photo_component_selector()
                .with().some_photos_selected()
                .and().some_albums_selected()
        .when()
                .executing_the_remove_command_using_that_selector()
                .and().confirming_the_dialog()
                .and().all_requests_succeed()
        .then()
                .the_command_listener_onStart_should_have_been_called_once()
                .and().the_progress_listener_should_have_been_called_once_for_each_item()
                .and().the_command_listener_should_receive_a_collection_with_all_removed_items_as_result();
    }


    public static class RemoveItemsSteps extends Stage<RemoveItemsSteps>{
        @Mock(answer = Answers.RETURNS_DEEP_STUBS)
        BaseActivity activity;
        @Mock(answer = Answers.RETURNS_MOCKS)
        FragmentManager fragmentManager;
        @Mock
        FragmentTransaction fragmentTransaction;

        @Mock
        BasePhotoCollectionCommand.PhotoComponentSelector selector;

        @Mock
        ResultListener<Collection<PhotoComponent>> resultListenerMockup;
        @Mock
        ResultListener<PhotoComponent> progressListenerMockup;

        MockedApi mockedApi = new MockedApi();
        InOrderRequests<PhotosApi> inOrderPhotoRequests;
        InOrderRequests<PhotoCollectionsApi> inOrderAlbumsRequest;

        ConfirmAndRemoveItemsCommand command;

        List<PhotoComponent> selectedComponents = new ArrayList<>();
        int selectedPhotos = 0, selectedAlbums = 0;

        SimpleMaterialDialogFragment capturedFragment;

        int concurrencyLevel = 1;

        @SuppressWarnings("unchecked")
        @SuppressLint("CommitTransaction")
        public RemoveItemsSteps() {
            MockitoAnnotations.initMocks(this);

            mockedApi.setup();
            inOrderPhotoRequests = new InOrderRequests<>(mockedApi.photosApi)
                .requestResponse(mock(RequestHandler.class))
                .capture(Mockito.when(mockedApi.photosApi.remove(any(Photo.class), any(ResultListener.class))));

            inOrderAlbumsRequest = new InOrderRequests<>(mockedApi.collectionsApi)
                    .capture(Mockito.when(
                                mockedApi.collectionsApi.removeAll(any(Collection.class), any(ResultListener.class)
                            ))
                    )
                    .requestResponse(mock(RequestHandler.class));

//            Mockito.when(mockedApi.photosApi.remove(any(Photo.class), any(ResultListener.class)))
//                    .then(inOrderPhotoRequests.getCaptureRequest());

            Mockito.doReturn(true).when(resultListenerMockup).isListening();
            Mockito.doReturn(true).when(progressListenerMockup).isListening();

            Mockito.doReturn("mocked string").when(activity).getString(anyInt());
            Mockito.when(activity.getResources().getString(anyInt())).thenReturn("mocked string");
            Mockito.doReturn(fragmentManager).when(activity).getSupportFragmentManager();
            Mockito.doReturn(fragmentTransaction).when(fragmentManager).beginTransaction();

            Mockito.doReturn(selectedComponents).when(selector).getSelectedItems();
        }

        public RemoveItemsSteps a_photo_component_selector() {
            return this;
        }

        public RemoveItemsSteps some_photos_selected() {
            selectedComponents.addAll(Arrays.asList(
                    new PhotoComponent(new Photo(1L)),
                    new PhotoComponent(new Photo(2L)),
                    new PhotoComponent(new Photo(3L))
            ));

            selectedPhotos = 3;

            return this;
        }

        public RemoveItemsSteps a_configured_remove_items_command() {
            command = new ConfirmAndRemoveItemsCommand(activity, selector);
            command.setNotifierBuilder(mock(Builder.class));
            command.setConcurrencyLevel(concurrencyLevel);
            command.onProgress(progressListenerMockup);
            return this;
        }

        public RemoveItemsSteps executing_the_command() {
            command.execute(resultListenerMockup);
            return this;
        }

        public RemoveItemsSteps a_confirm_dialog_should_be_displayed() {
            ArgumentCaptor<SimpleMaterialDialogFragment> dialogCaptor
                    = ArgumentCaptor.forClass(SimpleMaterialDialogFragment.class);
            verify(fragmentTransaction).add(dialogCaptor.capture(), any(String.class));

            capturedFragment = dialogCaptor.getValue();
            return this;
        }

        public RemoveItemsSteps confirming_the_dialog() {
            if(capturedFragment == null){
                this.a_confirm_dialog_should_be_displayed();
            }
            capturedFragment.getDialogResponseListener().onResponse(capturedFragment, true);
            return this;
        }


        public RemoveItemsSteps the_command_listener_onStart_should_be_called() {
            verify(resultListenerMockup).onStart();
            return this;
        }

        public RemoveItemsSteps the_first_removePhoto_request_should_be_sent() {
            return the_next_removePhoto_request_should_be_sent();
        }

        public RemoveItemsSteps the_next_removePhoto_request_should_be_sent() {
            inOrderPhotoRequests.verify().remove(any(Photo.class), any(ResultListener.class));
            return this;
        }

        public RemoveItemsSteps those_requests_finish() {
            inOrderPhotoRequests.succeedRequests(concurrencyLevel, mock(Photo.class));
            return this;
        }

        public RemoveItemsSteps the_progress_listener_onResult_method_should_be_called() {
            verify(progressListenerMockup).onResult(any(PhotoComponent.class));
            return this;
        }

        public RemoveItemsSteps some_albums_selected() {
            selectedComponents.addAll(Arrays.asList(
                    new PhotoComponent(new PhotoCollection(100L, "a1")),
                    new PhotoComponent(new PhotoCollection(102L, "a2")),
                    new PhotoComponent(new PhotoCollection(200L, "a3"))
            ));
            selectedAlbums = 3;
            return this;
        }

        public RemoveItemsSteps executing_the_remove_command_using_that_selector() {
            return a_configured_remove_items_command()
                    .executing_the_command();
        }

        public RemoveItemsSteps all_requests_succeed() {
            Mockito.verify(mockedApi.collectionsApi).removeAll(anyCollection(), any(ResultListener.class));
            Mockito.verify(mockedApi.photosApi).remove(any(Photo.class), any(ResultListener.class));

            inOrderAlbumsRequest.succeedRequests(1, getSelectedAlbums()); //only one call to remove albums

            for (PhotoComponent selected: selectedComponents) {
                if(selected.isPhoto()) {
                    inOrderPhotoRequests.succeedRequests(1,selected.getPhoto());
                }
            }
            return this;
        }

        public RemoveItemsSteps the_command_listener_onStart_should_have_been_called_once() {
            verify(resultListenerMockup, times(1)).onStart();
            return this;
        }

        public RemoveItemsSteps the_progress_listener_should_have_been_called_once_for_each_item() {
            verify(progressListenerMockup, times(selectedComponents.size())).onResult(any(PhotoComponent.class));
            return this;
        }

        @SuppressWarnings("unchecked")
        public RemoveItemsSteps the_command_listener_should_receive_a_collection_with_all_removed_items_as_result() {
            ArgumentCaptor<Collection> captor
                    = ArgumentCaptor.forClass(Collection.class);

            verify(resultListenerMockup, times(1)).onResult(captor.capture());

            Collection<PhotoComponent> capturedValue = (Collection<PhotoComponent>)captor.getValue();
            assertThat(capturedValue, Matchers.containsInAnyOrder(selectedComponents.toArray()));

            return this;
        }

        private Collection<PhotoCollection> getSelectedAlbums() {
            List<PhotoCollection> coll = new ArrayList<>();

            for(PhotoComponent comp : selectedComponents){
                if(comp.isCollection()){
                    coll.add(comp.getPhotoCollection());
                }
            }

            return coll;
        }
    }
}
