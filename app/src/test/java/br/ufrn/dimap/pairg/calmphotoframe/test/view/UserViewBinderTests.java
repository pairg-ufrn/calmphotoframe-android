package br.ufrn.dimap.pairg.calmphotoframe.test.view;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.rey.material.widget.TextView;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.LayoutMockup;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.RequestAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserImageViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserViewBinder;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

public class UserViewBinderTests extends SimpleScenarioTest<UserViewBinderTests.UserViewBinderSteps>{

    @Test
    public void setupView(){
        given()
                .the_user_view_binder_is_configured_with_some_view_ids()
                .and().there_is_a_valid_layout_inflated()

        .when()
                .the_layout_is_used_on_binder_setup()

        .then()
                .the_configured_views_should_be_saved_by_the_binder();
    }

    @Test
    public void bindView(){
        given()
                .the_view_binder_has_been_set_up()
                .and().there_is_a_valid_user_instance_with_profile_image()

        .when()
                .a_user_is_binded()

        .then()
                .the_user_name_should_be_set_into_name_view()
                .and().the_user_image_should_be_requested()
                .and().the_user_image_should_be_put_on_image_view();
    }

    public static class UserViewBinderSteps extends Stage<UserViewBinderSteps>{
        private UserViewBinder userViewBinder = new UserViewBinder();

        @Spy
        private UserImageViewBinder userImageViewBinder;
        @Mock
        private Drawable drawableMockup;

        private MockedApi mockedApi = new MockedApi();
        private LayoutMockup layoutMockup = new LayoutMockup();

        private int nameViewId = 12345, imageViewId = 54321;

        private User user;

        @Mock
        private Bitmap bitmapMockup;

        public UserViewBinderSteps(){
            MockitoAnnotations.initMocks(this);
            mockedApi.setup();

            userViewBinder.setImageViewBinder(userImageViewBinder);
            Mockito.doReturn(drawableMockup)
                    .when(userImageViewBinder).genTextDrawable(any(User.class));
        }

        @SuppressWarnings("ResourceType")
        public UserViewBinderSteps the_user_view_binder_is_configured_with_some_view_ids() {
            layoutMockup.addView(nameViewId, TextView.class);
            layoutMockup.addView(imageViewId, ImageView.class);

            userViewBinder.setUserNameId(nameViewId);
            userViewBinder.setImageViewId(imageViewId);

            return this;
        }

        public UserViewBinderSteps there_is_a_valid_layout_inflated() {
            return this;
        }

        public UserViewBinderSteps the_view_binder_has_been_set_up() {
            return the_user_view_binder_is_configured_with_some_view_ids()
                    .the_layout_is_used_on_binder_setup();
        }

        public UserViewBinderSteps there_is_a_valid_user_instance_with_profile_image() {
            user = new User(223344L, "login");
            user.setName("my name");
            user.setHasProfileImage(true);

            return this;
        }


        public UserViewBinderSteps the_layout_is_used_on_binder_setup() {
            View rootView = layoutMockup.getViewsRoot();

            userViewBinder.setup(rootView);

            return this;
        }

        @SuppressWarnings("unchecked")
        public UserViewBinderSteps a_user_is_binded() {
            Mockito.doAnswer(new RequestAnswer(bitmapMockup))
                    .when(mockedApi.usersApi)
                        .getProfileImage(
                                Mockito.eq(user.getId()), Mockito.any(ResultListener.class));

            this.userViewBinder.bind(user);
            return this;
        }


        @SuppressWarnings("ResourceType")
        public UserViewBinderSteps the_configured_views_should_be_saved_by_the_binder() {
            verify(layoutMockup.getViewsRoot()).findViewById(nameViewId);
            verify(layoutMockup.getViewsRoot()).findViewById(imageViewId);

            return this;
        }

        public UserViewBinderSteps the_user_name_should_be_set_into_name_view() {
            TextView nameView = (TextView) layoutMockup.getView(nameViewId);
            verify(nameView).setText(Mockito.eq(user.getName()));

            return this;
        }

        public UserViewBinderSteps the_user_image_should_be_requested() {
            //noinspection unchecked
            verify(mockedApi.usersApi).getProfileImage(Mockito.eq(user.getId()), Mockito.any(ResultListener.class));

            return this;
        }

        public UserViewBinderSteps the_user_image_should_be_put_on_image_view() {
            ImageView imageView = (ImageView)layoutMockup.getView(imageViewId);

            ArgumentCaptor<Bitmap> bitmapCaptor = ArgumentCaptor.forClass(Bitmap.class);
            verify(imageView).setImageBitmap(bitmapCaptor.capture());

            assertThat(bitmapCaptor.getValue(), is(bitmapMockup));

            return this;
        }
    }
}
