package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiConnector;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiEndpointHolder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.contexts.ContextsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.events.EventsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.PhotoCollectionsApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photos.PhotosApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session.SessionApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.users.UsersApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;

public class MockedApi {

    public ApiClient apiClient;

    @Mock(answer = Answers.RETURNS_MOCKS)
    public ApiConnector apiConnector;

    @Mock
    public SessionApi sessionApi;

    @Mock
    public UsersApi usersApi;

    @Mock
    public PhotosApi photosApi;

    @Mock
    public PhotoCollectionsApi collectionsApi;

    @Mock
    public ContextsApi contextsApi;

    @Mock
    public EventsApi eventsApi;

    @Mock
    public ApiEndpointHolder endpointHolder;

    public MockedApi(){
        MockitoAnnotations.initMocks(this);
        apiClient = Mockito.spy(new ApiClient(apiConnector, endpointHolder));
    }

    public void setup() {
        ApiClient.setInstance(apiClient);

        Mockito.when(apiClient.session()).thenReturn(sessionApi);
        Mockito.when(apiClient.users()).thenReturn(usersApi);
        Mockito.when(apiClient.photos()).thenReturn(photosApi);
        Mockito.when(apiClient.collections()).thenReturn(collectionsApi);
        Mockito.when(apiClient.contexts()).thenReturn(contextsApi);
        Mockito.when(apiClient.events()).thenReturn(eventsApi);
    }

    public void mockCurrentUser(User user){
        Mockito.doReturn(user).when(apiClient).getCurrentUser();
    }

    public void mockApiEndpoint(ApiEndpoint endpoint){
        Mockito.doReturn(endpoint).when(endpointHolder).getEntity();
        Mockito.doReturn(endpoint).when(endpointHolder).get();
    }
}
