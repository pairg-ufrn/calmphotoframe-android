package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CaptureAnswer<RetT> implements Answer<RetT>{

    List<InvocationOnMock> capturedCalls = new ArrayList<>();

    RetT returnValue;

    Answer<?> delegate;

    public CaptureAnswer() {
        this(null);
    }
    public CaptureAnswer(RetT returnValue) {
        this.returnValue = returnValue;
    }

    /** Captures a given mock invocation and answer it
     */
    @Override
    public RetT answer(InvocationOnMock invocation) throws Throwable {
        capturedCalls.add(invocation);

        if(delegate() != null){
            Object value = delegate().answer(invocation);

            if(value != null) {
                try {
                    return (RetT) value;
                } catch (ClassCastException ex) {
                    //do nothing
                }
            }
        }

        return returnValue;
    }

    public Answer<?> delegate() {
        return delegate;
    }

    public CaptureAnswer<RetT> delegate(Answer<?> delegate) {
        this.delegate = delegate;
        return this;
    }

    public CaptureAnswer<RetT> setReturnValue(RetT returnValue) {
        this.returnValue = returnValue;

        return this;
    }

    public <T> T getArg(int callIndex, int argumentIdx) {
        return (T) capturedCalls.get(callIndex).getArguments()[argumentIdx];
    }
    public Method getMethod(int callIndex) {
        return capturedCalls.get(callIndex).getMethod();
    }
    public int callsCount(){
        return capturedCalls.size();
    }

    public List<InvocationOnMock> calls() {
        return capturedCalls;
    }

    public InvocationOnMock call(int callIndex) {
        return calls().get(callIndex);
    }
}
