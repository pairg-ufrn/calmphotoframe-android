package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import android.content.BroadcastReceiver;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.robolectric.shadows.support.v4.ShadowLocalBroadcastManager;

public class BroadcastReceiverMatcher extends TypeSafeMatcher<ShadowLocalBroadcastManager.Wrapper> {
    private String action;

    private Matcher<? super BroadcastReceiver> matcher;

    /**
     * The broadcast receiver registered action
     */
    public BroadcastReceiverMatcher(String action) {
        this.action = action;
    }

    @Override
    protected boolean matchesSafely(ShadowLocalBroadcastManager.Wrapper item) {
        System.out.println("broadcastReceiver instance: " + item.broadcastReceiver);
        return item.intentFilter.matchAction(action)
                && (matcher == null || matcher.matches(item.broadcastReceiver));
    }

    public BroadcastReceiverMatcher that(Matcher<? super BroadcastReceiver> broadcastReceiverMatcher) {
        this.matcher = broadcastReceiverMatcher;
        return this;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a broadcast receiver for action ").appendValue(action);

        if (matcher != null) {
            description.appendText(" that ").appendDescriptionOf(matcher);
        }
    }

    @Override
    protected void describeMismatchSafely(ShadowLocalBroadcastManager.Wrapper item, Description mismatchDescription) {
        if (!item.intentFilter.matchAction(action)) {
            mismatchDescription.appendText("not match action ").appendValue(action);
        } else if (matcher != null) {
            matcher.describeMismatch(item.broadcastReceiver, mismatchDescription);
        }
    }
}
