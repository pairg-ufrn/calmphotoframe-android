package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import android.content.Context;

import org.mockito.Mockito;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;

public class MockingHelper {
    public static void mockLocalPersistence(){
        LocalPersistence.instance(Mockito.mock(LocalPersistence.class));
    }

    public static void getTextMock(Context context, int stringId, String text) {
        Mockito.doReturn(text).when(context).getText(stringId);
    }
}
