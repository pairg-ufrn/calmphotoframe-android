package br.ufrn.dimap.pairg.calmphotoframe.test.activities;

import android.content.pm.PackageManager;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class BaseActivityTests extends SimpleScenarioTest<BaseActivityTests.BaseActivitySteps> {

    @Test
    public void permissionRequest(){
        given()
                .a_permission_request_listener_registered_with_some_identifier()

        .when()
                .the_activity_receives_a_request_permission_response_with_that_identifier()

        .then()
                .the_listener_should_be_called()
                .and().the_listener_should_be_removed_from_activity();
    }

    public static class BaseActivitySteps extends Stage<BaseActivitySteps>{
        BaseActivity activity;

        @Mock
        BaseActivity.PermissionRequestListener permissionRequestListener;

        int requestId;
        String[] expectedPermissions;
        int[] expectedGrantResults;

        public BaseActivitySteps(){
            MockitoAnnotations.initMocks(this);
            activity = Mockito.spy(BaseActivity.class);

            requestId = 123456;
        }

        public BaseActivitySteps a_permission_request_listener_registered_with_some_identifier() {
            activity.listenToPermissionRequest(requestId, permissionRequestListener);

            return this;
        }

        public BaseActivitySteps the_activity_receives_a_request_permission_response_with_that_identifier() {
            expectedPermissions = new String[]{"random permission"};
            expectedGrantResults = new int[]{PackageManager.PERMISSION_GRANTED};
            activity.onRequestPermissionsResult(
                    requestId, expectedPermissions, expectedGrantResults);

            return this;
        }

        public BaseActivitySteps the_listener_should_be_called() {
            verify(permissionRequestListener).onRequestPermissionsResult(expectedPermissions, expectedGrantResults);

            return this;
        }

        public BaseActivitySteps the_listener_should_be_removed_from_activity() {
            List<BaseActivity.PermissionRequestListener> listeners = activity.getPermissionListeners(requestId);

            assertThat(listeners, not(hasItem(permissionRequestListener)));
            return this;
        }
    }
}
