package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.RequestBuilder;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;

public class SendRequestAnswer<T> implements Answer {
    T result;
    RequestBuilder<?, ? super T> requestBuilder;

    public SendRequestAnswer(T result, RequestBuilder<?, ? super T> requestBuilder) {
        this.result = result;
        this.requestBuilder = requestBuilder;
    }

    @Override
    public Object answer(InvocationOnMock invocation) throws Throwable {
        ResultListener<? super T> listener = requestBuilder.listener();

        Listeners.onResult(listener, result);

        return new FinishedRequestHandler();
    }
}
