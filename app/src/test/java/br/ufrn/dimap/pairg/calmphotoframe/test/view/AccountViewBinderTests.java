package br.ufrn.dimap.pairg.calmphotoframe.test.view;


import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.LayoutMockup;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.AccountViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.BinderViewHolder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserImageViewBinder;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.verify;

public class AccountViewBinderTests extends SimpleScenarioTest<AccountViewBinderTests.AccountViewBinderSteps>{

    @Test
    public void bindUser(){
        Logging.i(getClass().getSimpleName(), "TEST - bindUser");
        given().
                the_binder_view_is_configured()

        .when()
                .binding_the_user()

        .then()
                .the_view_values_should_be_filled();
    }

    @Test
    public void deactivateUser(){
        Logging.i(getClass().getSimpleName(), "TEST - deactivateUser");
        given()
                .the_binder_was_bound_to_an_activated_user()
                .and().a_command_to_change_user_active_state_was_set()

        .when()
                .the_activation_toggle_view_changes()

        .then()
                .the_command_should_be_called()
                .and().the_view_should_change_to_deactivated()
                .but().the_model_should_not_have_changed_yet();

        when()
                .the_command_result_is_successfull()

        .then()
                .the_bound_user_should_be_updated();
    }

    @Test
    public void deactivateUserFailed(){
        Logging.i(getClass().getSimpleName(), "TEST - deactivateUserFailed");

        given()
                .the_binder_was_bound_to_an_activated_user()
                .and().a_command_to_change_user_active_state_was_set()

        .when()
                .the_activation_toggle_view_changes()

        .then()
                .the_command_should_be_called()
                .and().the_view_should_change_to_deactivated()
                .but().the_model_should_not_have_changed_yet();

        when()
                .the_command_result_fails()

        .then()
                .the_view_should_be_restored_to_previews_state();
    }


    public static class AccountViewBinderSteps extends BaseViewBinderSteps<AccountViewBinderSteps, User>{
        @Mock
        ArgsCommand<AccountViewBinder.UserActivationParam, User> changeActiveStateCommand;

        @Captor
        ArgumentCaptor<ResultListener<? super User>> captureResultListener;

        @Captor
        ArgumentCaptor<AccountViewBinder.UserActivationParam> userActivationParamCaptor;

        @Spy
        private UserImageViewBinder userImageViewBinder;
        @Mock
        private Drawable drawableMockup;

        public AccountViewBinderSteps(){
            MockitoAnnotations.initMocks(this);

            getBinder().setImageViewBinder(userImageViewBinder);

            Mockito.doReturn(drawableMockup)
                    .when(userImageViewBinder).genTextDrawable(any(User.class));
        }

        @Override
        protected void setupLayout(LayoutMockup layoutMockup) {
            layoutMockup.addView(R.id.view_user_isactive, SwitchCompat.class);
            layoutMockup.addView(R.id.view_user_thumbnail, ImageView.class);
            layoutMockup.addView(R.id.user_name, TextView.class);
            layoutMockup.addView(R.id.user_login, TextView.class);
        }

        @Override
        protected User buildEntity() {
            return new User(123L, "login").setName("Lorem Ipsum");
        }

        @Override
        protected BinderViewHolder<User> buildBinder() {
            return Mockito.spy(new AccountViewBinder());
        }

        public AccountViewBinderSteps binding_the_user() {
            this.there_is_a_valid_entity_instance();

            assertThat(user(), is(Matchers.notNullValue()));

            binder.bind(user());
            return this;
        }

        protected User user() {
            return this.entity;
        }

        AccountViewBinder getBinder(){
            return (AccountViewBinder) binder;
        }

        public AccountViewBinderSteps the_view_values_should_be_filled() {
            layoutMockup.<TextView>verifyView(R.id.user_name).setText(eq(user().getName()));
            layoutMockup.<TextView>verifyView(R.id.user_login).setText(eq(user().getLogin()));
            layoutMockup.<SwitchCompat>verifyView(R.id.view_user_isactive).setChecked(eq(user().isActive()));
            layoutMockup.<ImageView>verifyView(R.id.view_user_thumbnail).setImageDrawable(isNotNull(Drawable.class));

            return this;
        }

        public AccountViewBinderSteps the_binder_was_bound_to_an_activated_user() {
            this.there_is_a_valid_entity_instance().user().setActive(true);
            this.the_binder_view_is_configured()
                    .and().binding_the_user();
            return this;
        }

        public AccountViewBinderSteps a_command_to_change_user_active_state_was_set() {
            this.getBinder().setActivateUserCommand(changeActiveStateCommand);
            return this;
        }

        public AccountViewBinderSteps the_activation_toggle_view_changes() {
            ArgumentCaptor<OnCheckedChangeListener> listenerCaptor = ArgumentCaptor.forClass(OnCheckedChangeListener.class);

            CompoundButton isActiveView = (CompoundButton) layoutMockup.getView(R.id.view_user_isactive);
            verify(isActiveView).setOnCheckedChangeListener(listenerCaptor.capture());

            listenerCaptor.getValue().onCheckedChanged(isActiveView, !user().isActive());

            return this;
        }

        public AccountViewBinderSteps the_command_should_be_called() {
            verify(changeActiveStateCommand).execute(userActivationParamCaptor.capture(), captureResultListener.capture());

            return this;
        }

        public AccountViewBinderSteps the_view_should_change_to_deactivated() {
            boolean expectedActive = userActivationParamCaptor.getValue().isActive();
            verify(getBinder()).bindToView(argThat(
                    Matchers.<User>hasProperty("active", equalTo(expectedActive))));

            return this;
        }

        public AccountViewBinderSteps the_view_should_be_restored_to_previews_state() {
            Matcher<User> userIsActive = Matchers.hasProperty("active", equalTo(true));
            Matcher<User> userIsInactive = Matchers.hasProperty("active", equalTo(false));

            InOrder inOrder = Mockito.inOrder(getBinder());
            inOrder.verify(getBinder()).bindToView(Mockito.argThat(userIsActive));
            inOrder.verify(getBinder()).bindToView(Mockito.argThat(userIsInactive));
            inOrder.verify(getBinder()).bindToView(Mockito.argThat(userIsActive));

            return this;
        }

        public AccountViewBinderSteps the_command_result_is_successfull() {
            ResultListener<? super User> listener = captureResultListener.getValue();
            AccountViewBinder.UserActivationParam param = userActivationParamCaptor.getValue();

            assertThat(listener, is(notNullValue()));

            User updatedUser = user().clone();
            updatedUser.setActive(param.isActive());

            listener.onResult(updatedUser);

            return this;
        }

        public AccountViewBinderSteps the_command_result_fails() {
            ResultListener<? super User> listener = captureResultListener.getValue();

            assertThat(listener, is(notNullValue()));

            listener.onFailure(Mockito.mock(Throwable.class));

            return this;
        }

        public AccountViewBinderSteps the_bound_user_should_be_updated() {
            assertThat(getBinder().getBoundUser().isActive(),
                    is(equalTo(userActivationParamCaptor.getValue().isActive())));

            return this;
        }

        public AccountViewBinderSteps the_model_should_not_have_changed_yet() {
            assertThat(getBinder().getBoundUser().isActive(),
                    is(not(equalTo(userActivationParamCaptor.getValue().isActive()))));
            return this;
        }
    }
}
