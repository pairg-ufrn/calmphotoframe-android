package br.ufrn.dimap.pairg.calmphotoframe.test.activities;


import android.app.Activity;
import android.content.Intent;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import br.ufrn.dimap.pairg.calmphotoframe.activities.NavActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.PickImagesActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.users.UserProfileController;
import br.ufrn.dimap.pairg.calmphotoframe.controller.OnActivityResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ArgsCommand;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ApiEndpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ImageContent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.CaptureAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.ImageContentBuilderMocker;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.RequestAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.UserNotifierMockup;
import br.ufrn.dimap.pairg.calmphotoframe.utils.IntentUtils;
import br.ufrn.dimap.pairg.calmphotoframe.view.fragments.users.UserProfileFragment;
import br.ufrn.dimap.pairg.calmphotoframe.view.listeners.ActionListener;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class UserProfileControllerTests extends SimpleScenarioTest<UserProfileControllerTests.ProfileControllerSteps>{

    @Test
    public void setupControllerView(){
        given()
                .an_activity_instance()
                .and().a_profile_fragment()
        .when()
                .the_controller_is_initiated()
        .then()
                .it_should_register_his_actions_on_the_fragment();
    }

    @Test
    public void changeUserPassword(){
        given()
                .that_the_controller_is_initiated()
                .and().there_is_a_logged_in_account()

        .when()
                .a_change_password_interaction_is_made()

        .then()
                .a_request_should_be_sent_to_update_the_current_user_password()
                .and().a_user_notification_should_have_been_shown_on_interface();
    }

    @Test
    public void failedToChangeUserPassword(){
        given()
                .that_the_controller_is_initiated()
                .and().there_is_a_logged_in_account()

        .when()
                .a_change_password_interaction_is_made_but_fail()

        .then()
                .a_request_should_be_sent_to_update_the_current_user_password()
                .and().the_error_should_be_notified_on_interface()
                .and().error_message_should_contain_a_retry_option();
    }


    @Test
    public void changeUserProfileImage(){
        given()
                .that_the_controller_is_initiated()
                .and().there_is_a_logged_in_account()

        .when()
                .a_change_photo_action_is_triggered()
                .and().a_new_profile_image_is_selected()

        .then()
                .an_image_selection_activity_should_have_been_triggered()
                .and().a_request_should_been_sent_to_update_the_user_profile_image()
                .and().the_current_user_should_be_updated()
                .and().a_user_notification_should_have_been_shown_on_interface()
                .and().the_navigation_drawer_on_activity_should_have_been_rebound_to_api_endpoint();
    }

    @Test
    public void changeUserName(){
        given()
                .that_the_controller_is_initiated()
                .and().there_is_a_logged_in_account()

        .when()
                .a_change_name_action_is_triggered()

        .then()
                .a_request_should_been_sent_to_update_the_user_name()
                .and().the_current_user_should_be_updated()
                .and().a_user_notification_should_have_been_shown_on_interface()
                .and().the_navigation_drawer_on_activity_should_have_been_rebound_to_api_endpoint();
    }


    @SuppressWarnings("unchecked")
    public static class ProfileControllerSteps extends Stage<ProfileControllerSteps>{
        UserProfileController profileController;


        MockedApi mockedApi = new MockedApi();
        UserNotifierMockup userNotifierMockup = new UserNotifierMockup();
        ImageContentBuilderMocker imageContentBuilderMocker = new ImageContentBuilderMocker();

        @Mock
        UserProfileFragment profileFragment;
        @Mock
        NavActivity activityMockup;


        @Mock
        ResultListener<Object> listenerMockup;

        @Mock
        IntentUtils intentUtilsMockup;
        @Mock
        Intent getImageIntentMockup;

        @Mock
        ImageContent.Builder imageContentBuilder;

        User loggedInUser, updatedUser;
        UserProfileFragment.UserPassword newPassword;

        CaptureAnswer<Void> captureStartActivity = new CaptureAnswer<>();


        public ProfileControllerSteps(){
            initMockups();

            mockedApi.mockApiEndpoint(new ApiEndpoint());

            profileController = new UserProfileController(activityMockup);
            profileController.setUserNotifier(userNotifierMockup.getUserNotifierMockup());
        }

        public void initMockups() {
            mockedApi.setup();
            imageContentBuilderMocker.setup();

            MockitoAnnotations.initMocks(this);

            userNotifierMockup.mockup(activityMockup);

            Mockito.when(profileFragment.getContext()).thenReturn(activityMockup);

            Mockito.doAnswer(captureStartActivity)
                    .when(activityMockup)
                    .startActivityForResult(Mockito.any(Intent.class), Mockito.any(OnActivityResultListener.class));

            IntentUtils.instance(intentUtilsMockup);
            Mockito.doReturn(getImageIntentMockup).when(intentUtilsMockup).buildGetImageIntent(Mockito.anyBoolean());


            Mockito.doAnswer(new RequestAnswer("")).when(mockedApi.usersApi)
                    .changePassword(Mockito.anyString(), Mockito.any(ResultListener.class));


            Mockito.doReturn(true).when(activityMockup).isActive();
        }

        //region **************************** Given **********************************************

        public ProfileControllerSteps an_activity_instance() {
            return this;
        }

        public ProfileControllerSteps a_profile_fragment() {
            return this;
        }

        public ProfileControllerSteps there_is_a_logged_in_account() {
            loggedInUser = new User(123L, "user login");

            mockedApi.mockCurrentUser(loggedInUser);

            return this;
        }

        public ProfileControllerSteps that_the_controller_is_initiated() {
            return the_controller_is_initiated();
        }

        //endregion
        //region ******************************** When *******************************************

        public ProfileControllerSteps the_controller_is_initiated() {
            profileController.setup(profileFragment);
            return this;
        }

        public ProfileControllerSteps a_change_password_interaction_is_made() {

            newPassword = new UserProfileFragment.UserPassword(loggedInUser, "any password");
            profileController.getChangePasswordAction().execute(newPassword);

            return this;
        }

        public ProfileControllerSteps a_change_password_interaction_is_made_but_fail() {
            Mockito.when(mockedApi.usersApi.changePassword(Mockito.anyString(), Mockito.any(ResultListener.class)))
                    .thenAnswer(new RequestAnswer<>().fail(new Exception()));

            return this.a_change_password_interaction_is_made();
        }

        public ProfileControllerSteps a_change_photo_action_is_triggered() {
            updatedUser = loggedInUser.clone();
            updatedUser.setHasProfileImage(true);
            Mockito.doAnswer(new RequestAnswer(updatedUser))
                    .when(mockedApi.usersApi)
                    .get(Mockito.eq(loggedInUser.getId()), Mockito.any(ResultListener.class));

            Mockito.doAnswer(new RequestAnswer(new Object()))
                    .when(mockedApi.usersApi)
                    .updateProfileImage(
                            Mockito.any(ImageContent.class),
                            Mockito.anyLong(),
                            Mockito.any(ResultListener.class));

            profileController.getChangePhotoCommand().execute(loggedInUser, listenerMockup);
            return this;
        }

        public ProfileControllerSteps a_new_profile_image_is_selected() {
            OnActivityResultListener onActivityResultListener = captureStartActivity.getArg(0, 1);

            assertThat(onActivityResultListener, is(notNullValue()));

            Intent mockIntent = Mockito.mock(Intent.class);

            Mockito.when(mockIntent.hasExtra(PickImagesActivity.EXTRA_SELECTED_IMAGES)).thenReturn(true);
            Mockito.when(mockIntent.getStringArrayExtra(PickImagesActivity.EXTRA_SELECTED_IMAGES)).thenReturn(
                    new String[]{"file:///example.jpeg"}
            );

            onActivityResultListener.onActivityResult(Activity.RESULT_OK, mockIntent);

            return this;
        }

        public ProfileControllerSteps a_change_name_action_is_triggered() {
            updatedUser = loggedInUser.clone();
            updatedUser.setName("new name");

            Mockito.doAnswer(new RequestAnswer(updatedUser))
                    .when(mockedApi.usersApi).update(Mockito.eq(updatedUser), Mockito.any(ResultListener.class));

            profileController.getSaveNameCommand().execute(updatedUser, listenerMockup);
            return this;
        }

        //endregion
        //region ******************************** Then *******************************************

        public ProfileControllerSteps it_should_register_his_actions_on_the_fragment() {
            verify(profileFragment).setSaveNameCommand(Mockito.any(ArgsCommand.class));
            verify(profileFragment).setChangePhotoCommand(Mockito.any(ArgsCommand.class));
            verify(profileFragment).setChangePasswordAction(Mockito.any(ActionListener.class));

            return this;
        }

        public ProfileControllerSteps a_request_should_be_sent_to_update_the_current_user_password() {
            verify(mockedApi.usersApi).changePassword(
                    Mockito.eq(newPassword.getPassword()),
                    Mockito.any(ResultListener.class));

            return this;
        }


        public ProfileControllerSteps a_user_notification_should_have_been_shown_on_interface() {
            verify(userNotifierMockup.getUserNotifierMockup()).notifyUser();

            ArgumentCaptor<CharSequence> textCaptor = ArgumentCaptor.forClass(CharSequence.class);

            verify(userNotifierMockup.getNotificationBuilderMockup()).text(textCaptor.capture());

            CharSequence notificationText = textCaptor.getValue();

            assertThat(notificationText, is(notNullValue()));
            assertThat(notificationText.toString(), not(isEmptyString()));

            return this;
        }

        public ProfileControllerSteps the_error_should_be_notified_on_interface() {
            verify(userNotifierMockup.getUserNotifierMockup()).notifyUser();
            verify(userNotifierMockup.getNotificationBuilderMockup()).show();

            return this;
        }

        public ProfileControllerSteps error_message_should_contain_a_retry_option() {
            verify(userNotifierMockup.getNotificationBuilderMockup())
                    .action(Mockito.notNull(CharSequence.class), Mockito.notNull(Runnable.class));

            return this;
        }

        public ProfileControllerSteps an_image_selection_activity_should_have_been_triggered() {
            verify(activityMockup).startActivityForResult(
                    Mockito.any(Intent.class), Mockito.any(OnActivityResultListener.class));
            return this;
        }

        public ProfileControllerSteps a_request_should_been_sent_to_update_the_user_profile_image() {
            verify(mockedApi.usersApi).updateProfileImage(
                    Mockito.notNull(ImageContent.class),
                    Mockito.anyLong(),
                    Mockito.any(ResultListener.class)
            );

            return this;
        }

        public ProfileControllerSteps a_request_should_been_sent_to_update_the_user_name() {
            verify(mockedApi.usersApi).update(Mockito.eq(updatedUser), Mockito.any(ResultListener.class));

            return this;
        }

        public ProfileControllerSteps the_current_user_should_be_updated(){
            verify(mockedApi.apiClient).updateCurrentUser(Mockito.eq(updatedUser));

            return this;
        }

        public ProfileControllerSteps the_navigation_drawer_on_activity_should_have_been_rebound_to_api_endpoint() {
            verify(activityMockup).rebindApiEndpoint();

            return this;
        }

        //endregion
    }
}
