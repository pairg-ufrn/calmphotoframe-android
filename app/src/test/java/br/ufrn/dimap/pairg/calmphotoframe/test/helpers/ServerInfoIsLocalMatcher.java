package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;

public class ServerInfoIsLocalMatcher extends TypeSafeMatcher<ServerInfo> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean matchesSafely(ServerInfo item) {
        return item != null
                && item.isLocal();
    }

    @Override
    protected void describeMismatchSafely(ServerInfo item, Description mismatchDescription) {
        mismatchDescription.appendValue(item.getAddress());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a server with a local address");
    }
}
