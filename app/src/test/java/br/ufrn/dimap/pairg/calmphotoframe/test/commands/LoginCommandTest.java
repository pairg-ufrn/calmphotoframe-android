package br.ufrn.dimap.pairg.calmphotoframe.test.commands;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.BasicResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.connection.LoginConnectionCommand;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.ApiClient;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.session.SessionApi;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ConnectionInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Endpoint;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.LoginInfo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.FinishedRequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.network.RequestHandler;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.CaptureAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockingHelper;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.RequestAnswer;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class LoginCommandTest extends SimpleScenarioTest<LoginCommandTest.LoginCommandSteps> {

    @Test
    public void connectToLocalEndpoint(){
        given()
            .a_local_endpoint()
            .and().some_login_info();

        when()
            .executing_the_login_command();

        then()
            .the_local_service_should_be_started()
            .and().a_login_request_should_be_made_to_the_server_with_given_login_info()
            .and().the_command_listener_should_receive_events();
    }

    public static class LoginCommandSteps extends Stage<LoginCommandSteps> {

        @Mock(answer = Answers.RETURNS_MOCKS)
        ApiClient apiClient;

        @Mock
        SessionApi sessionApi;

        @Mock
        BasicResultListener<Endpoint> listener;

        LoginConnectionCommand loginCommand;

        LoginInfo loginInfo;
        Endpoint serverEndpoint;

        CaptureAnswer<RequestHandler> captureStartServer = new CaptureAnswer<>();
        CaptureAnswer<RequestHandler> captureConnectRequest = new CaptureAnswer<>();

        @Captor
        ArgumentCaptor<ResultListener<? super Endpoint>> captureEndpointListener;
        @Captor
        ArgumentCaptor<ConnectionInfo> captureConnectionInfo;
        @Captor
        ArgumentCaptor<Endpoint> endpointCaptor;

        public LoginCommandSteps(){
            MockitoAnnotations.initMocks(this);

            loginCommand = Mockito.spy(new LoginConnectionCommand());
            setupMockups();
        }

        public void setupMockups() {
            MockingHelper.mockLocalPersistence();
            ApiClient.setInstance(apiClient);

            mockStartServer();
            setupApiMockups();
        }

        public void mockStartServer() {
            Mockito.doAnswer(captureStartServer)
                    .when(loginCommand).startLocalServer(Mockito.any(ResultListener.class));
        }

        public void setupApiMockups() {
            captureConnectRequest.setReturnValue(new FinishedRequestHandler());

            Mockito.when(apiClient.session()).thenReturn(sessionApi);

            Mockito.when(sessionApi.connect(
                                Mockito.any(ConnectionInfo.class), Mockito.any(ResultListener.class)))
                    .thenAnswer(captureConnectRequest);

            Mockito.when(listener.isListening()).thenReturn(true);
        }

        public LoginCommandSteps a_local_endpoint() {
            serverEndpoint = new Endpoint("localhost");
            captureStartServer.delegate(new RequestAnswer<>(serverEndpoint));

            return this;
        }

        public LoginCommandSteps some_login_info() {
            loginInfo = new LoginInfo("login", "password");
            return this;
        }

        public LoginCommandSteps executing_the_login_command() {
            ConnectionInfo connectionInfo = new ConnectionInfo(serverEndpoint, loginInfo);

            assertThat(connectionInfo.isLocal(), is(true));

            loginCommand.execute(connectionInfo, listener);
            return this;
        }

        public LoginCommandSteps the_local_service_should_be_started() {
            Mockito.verify(loginCommand).startLocalServer(Mockito.any(ResultListener.class));

            assertThat(captureStartServer.getArg(0, 0), is(instanceOf(ResultListener.class)));

            return this;
        }

        public LoginCommandSteps a_login_request_should_be_made_to_the_server_with_given_login_info() {
            Mockito.verify(sessionApi)
                    .connect(Mockito.any(ConnectionInfo.class), Mockito.any(ResultListener.class));

            ConnectionInfo connectionInfo = captureConnectRequest.getArg(0, 0);
            Endpoint endpoint = connectionInfo.getEndpoint();
            LoginInfo capturedLoginInfo = connectionInfo.getLoginInfo();

            assertThat("Endpoint should not be null", endpoint, is(notNullValue()));
            assertThat(endpoint, is(equalTo(serverEndpoint)));
            assertThat(capturedLoginInfo, is(equalTo(loginInfo)));


            return this;
        }

        public LoginCommandSteps the_command_listener_should_receive_events() {
            Mockito.verify(listener).onStart();

            return this;
        }
    }
}
