package br.ufrn.dimap.pairg.calmphotoframe.test.photoframe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.LocalPersistence;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.ServerInfo;
import edu.emory.mathcs.backport.java.util.Collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class LocalPersistenceTest extends SimpleScenarioTest<LocalPersistenceTest.LocalPersistenceSteps>{

    @Test
    public void emptyListIsNotNull(){
        given().there_is_no_saved_connected_servers();

        when().listing_the_connected_servers();

        then().the_connected_servers_list_should_not_be_null();
    }

    @Test
    public void insertConnectedServer(){
        when()
                .saving_some_servers()
                .and().listing_the_connected_servers();

        then()
                .it_should_return_those_servers_in_reverse_insertion_order();
    }
    @Test
    public void removeConnectedServer(){

        given().there_is_some_saved_connected_servers();

        ServerInfo serverInfo = when().removing_some_server();

        then().it_should_not_be_listed_anymore(serverInfo);
    }

    public static class LocalPersistenceSteps extends Stage<LocalPersistenceSteps>{


        @Mock(answer = Answers.RETURNS_MOCKS)
        Context context;

        @Mock
        SharedPreferences preferences;

        @Mock
        SharedPreferences.Editor editor;
        Map<Object, Object> sharedPreferencesDb = new HashMap<>();

        private LocalPersistence persistence;

        List<ServerInfo> capturedServers;

        public static final List<ServerInfo> SERVER_INFOS = Arrays.asList(
                new ServerInfo("Local", "10.0.0.10"),
                new ServerInfo("My server", "www.myserver.com")
        );

        public LocalPersistenceSteps(){
            setupMockups();
            persistence = new LocalPersistence(context);
        }

        @SuppressLint("CommitPrefEdits")
        private void setupMockups() {
            MockitoAnnotations.initMocks(this);
            Mockito.doReturn(preferences)
                    .when(context).getSharedPreferences(Mockito.any(String.class), Mockito.anyInt());
            Mockito.doReturn(editor)
                    .when(preferences).edit();

            Mockito.doAnswer(new SaveValueAnswer())
                    .when(editor).putString(Mockito.anyString(), Mockito.anyString());
            Mockito.doAnswer(new ReadPreferenceAnswer())
                    .when(preferences).getString(Mockito.anyString(), Mockito.anyString());
        }

        public LocalPersistenceSteps there_is_no_saved_connected_servers() {
            return this;
        }

        public LocalPersistenceSteps there_is_some_saved_connected_servers() {
            return saving_some_servers();
        }

        public LocalPersistenceSteps listing_the_connected_servers() {
            capturedServers = persistence.getConnectedServers();
            return this;
        }

        public LocalPersistenceSteps the_connected_servers_list_should_not_be_null() {
            assertThat(capturedServers, is(not(nullValue())));
            return this;
        }

        public LocalPersistenceSteps saving_some_servers() {
            for (ServerInfo serverInfo : SERVER_INFOS){
                persistence.addConnectedServer(serverInfo);
            }

            return this;
        }

        public LocalPersistenceSteps it_should_return_those_servers_in_reverse_insertion_order() {
            List<ServerInfo> expected = new ArrayList<>(SERVER_INFOS);
            Collections.reverse(expected);

            assertThat(capturedServers, equalTo(expected));

            return this;
        }

        public ServerInfo removing_some_server() {
            ServerInfo savedServer = SERVER_INFOS.get(0);

            persistence.removeConnectedServer(savedServer);

            return savedServer;
        }

        public LocalPersistenceSteps it_should_not_be_listed_anymore(ServerInfo removedServer) {
            List<ServerInfo> result = persistence.getConnectedServers();

            assertThat(result, not(hasItem(equalTo(removedServer))));

            return this;
        }

        private class SaveValueAnswer implements Answer {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object key = invocation.getArguments()[0];
                Object value = invocation.getArguments()[1];
                sharedPreferencesDb.put(key, value);

                return editor;
            }
        }

        private class ReadPreferenceAnswer implements Answer {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object key = invocation.getArguments()[0];
                Object defaultValue = invocation.getArguments()[1];

                Object result = sharedPreferencesDb.get(key);
                return result == null ? defaultValue : result;
            }
        }
    }
}
