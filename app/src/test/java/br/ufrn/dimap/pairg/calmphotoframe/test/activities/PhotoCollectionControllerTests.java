package br.ufrn.dimap.pairg.calmphotoframe.test.activities;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Date;

import br.ufrn.dimap.pairg.calmphotoframe.activities.BaseActivity;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection.PhotoCollectionController;
import br.ufrn.dimap.pairg.calmphotoframe.activities.photocollection.PhotoCollectionView;
import br.ufrn.dimap.pairg.calmphotoframe.controller.ExpandedPhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.controller.model.PhotoComponent;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Photo;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.PhotoCollection;
import br.ufrn.dimap.pairg.calmphotoframe.test.activities.PhotoCollectionControllerTests.CollectionControllerSteps;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;

public class PhotoCollectionControllerTests extends SimpleScenarioTest<CollectionControllerSteps>{

    @Test
    public void onAddPhoto(){
        given()
                .a_PhotoCollectionController_instance()
        .given()
                .the_controller_has_some_photos_and_albums()
        .when()
                .a_photo_is_added_to_controller()
        .then()
                .the_controller_should_update_the_view();
    }

    public static class CollectionControllerSteps extends Stage<CollectionControllerSteps>{
        PhotoCollectionController controller;


        @Mock
        BaseActivity activity;

        @Mock
        PhotoCollectionView collectionView;

        MockedApi mockedApi = new MockedApi();

        ExpandedPhotoCollection controllerData;
        Photo newPhoto;

        public CollectionControllerSteps(){
            MockitoAnnotations.initMocks(this);

            setupApi();

            controller = new PhotoCollectionController(activity);
            controller.setCollectionView(collectionView);
        }

        public void setupApi() {
            mockedApi.setup();
        }

        public CollectionControllerSteps a_PhotoCollectionController_instance() {
            return this;
        }

        public CollectionControllerSteps the_controller_has_some_photos_and_albums() {
            controllerData = new ExpandedPhotoCollection();
            controllerData.setPhotos(Arrays.asList(
                    new Photo(1L, "", "", "", new Date()),
                    new Photo(2L, "", "", "", new Date()),
                    new Photo(3L, "", "", "", new Date())
            ));

            controllerData.setSubcollections(Arrays.asList(
                    new PhotoCollection(123L, "name").setPhotoCoverId(12345L)
            ));

            controller.setCollection(controllerData);

            return this;
        }

        public CollectionControllerSteps a_photo_is_added_to_controller() {
            newPhoto = new Photo(1001L);

            controller.onAddPhoto(newPhoto);

            return this;
        }

        public CollectionControllerSteps the_controller_should_update_the_view() {
            ArgumentCaptor<PhotoComponent> componentCaptor = ArgumentCaptor.forClass(PhotoComponent.class);

            verify(collectionView).onAddPhotoComponent(componentCaptor.capture());

            PhotoComponent capturedValue = componentCaptor.getValue();
            assertThat(capturedValue.isPhoto(), is(true));
            assertThat(capturedValue.getPhoto(), is(equalTo(newPhoto)));

            return this;
        }
    }
}
