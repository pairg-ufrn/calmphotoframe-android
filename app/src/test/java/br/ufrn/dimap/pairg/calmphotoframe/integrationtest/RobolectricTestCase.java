package br.ufrn.dimap.pairg.calmphotoframe.integrationtest;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import br.ufrn.dimap.pairg.calmphotoframe.BuildConfig;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public abstract class RobolectricTestCase {

    @Before
    public void baseSetup(){
        //show logs in output stream
        ShadowLog.stream = System.out;
    }
}
