package br.ufrn.dimap.pairg.calmphotoframe.test.view;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.rey.material.widget.TextView;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.junit.SimpleScenarioTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;

import java.util.Date;

import br.ufrn.dimap.pairg.calmphotoframe.R;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.ApiException;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.Event;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.User;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.LayoutMockup;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.MockedApi;
import br.ufrn.dimap.pairg.calmphotoframe.test.helpers.RequestAnswer;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.DateFormatter;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.EventViewBinder;
import br.ufrn.dimap.pairg.calmphotoframe.view.viewbuilders.UserImageViewBinder;

import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

public class EventViewBinderTests extends SimpleScenarioTest<EventViewBinderTests.EventViewBinderSteps> {

    @Test
    public void setupView(){
        given()
                .the_view_binder_default_layout_is_inflated()

        .when()
                .the_layout_is_used_on_binder_setup()

        .then()
                .the_default_views_should_be_saved_by_the_binder();
    }

    @Test
    public void bindViewToEventWithNoUserProfileImage(){
        given()
                .the_view_binder_has_been_set_up()
                .and().there_is_a_valid_event_instance()
                .and().the_related_user_has_no_profile_image()

        .when()
                .the_instance_is_binded()

        .then()
                .an_event_message_should_be_set_to_view()
                .and().an_event_date_message_should_be_set_to_view()
                .and().the_default_user_image_should_be_put_on_image_view();
    }

    @Test
    public void bindViewToEventWithUserProfileImage(){
        given()
                .the_view_binder_has_been_set_up()
                .and().there_is_a_valid_event_instance()
                .and().the_related_user_has_a_profile_image()

        .when()
                .the_instance_is_binded()

        .then()
                .an_event_message_should_be_set_to_view()
                .and().an_event_date_message_should_be_set_to_view()
                .and().the_user_profile_image_should_be_put_on_image_view();
    }

    public static class EventViewBinderSteps extends Stage<EventViewBinderSteps> {
        private EventViewBinder viewBinder = new EventViewBinder();

        @Spy
        private UserImageViewBinder userImageViewBinder;
        @Mock
        private Drawable drawableMockup;

        private MockedApi mockedApi = new MockedApi();
        private LayoutMockup layoutMockup = new LayoutMockup();
        private View layoutRoot;

        private User user;
        private Event event;

        @Mock
        private Bitmap bitmapMockup;

        public EventViewBinderSteps(){
            MockitoAnnotations.initMocks(this);
            mockedApi.setup();

            viewBinder.setDateFormatter(Mockito.mock(DateFormatter.class));
            viewBinder.setUserImageViewBinder(userImageViewBinder);

            Mockito.doReturn(drawableMockup)
                    .when(userImageViewBinder).genTextDrawable(any(User.class));
        }

        public EventViewBinderSteps the_view_binder_default_layout_is_inflated() {
            layoutMockup.addView(R.id.view_event_message, TextView.class);
            layoutMockup.addView(R.id.view_event_date, TextView.class);
            layoutMockup.addView(R.id.view_user_thumbnail, ImageView.class);

            layoutRoot = layoutMockup.getViewsRoot();

            return this;
        }

        public EventViewBinderSteps the_view_binder_has_been_set_up() {
            return the_view_binder_default_layout_is_inflated()
                    .the_layout_is_used_on_binder_setup();
        }

        public EventViewBinderSteps there_is_a_valid_event_instance() {
            user = new User(223344L, "login");
            user.setName("my name");

            event = new Event().setUser(user).setMessage("Event message").setCreatedAt(new Date());

            return this;
        }
        public EventViewBinderSteps the_related_user_has_no_profile_image() {
            mockGetUserProfileImage(user.getId(), new RequestAnswer<Bitmap>().fail(new ApiException().setStatusCode(404)));

            return this;
        }
        public EventViewBinderSteps the_related_user_has_a_profile_image() {
            mockGetUserProfileImage(user.getId(), new RequestAnswer<>(bitmapMockup));
            return this;
        }


        public EventViewBinderSteps the_layout_is_used_on_binder_setup() {
            viewBinder.setup(layoutRoot);

            return this;
        }

        @SuppressWarnings("unchecked")
        public EventViewBinderSteps the_instance_is_binded() {
            this.viewBinder.bind(event);

            return this;
        }

        protected void mockGetUserProfileImage(Long userId, Answer answer) {
            Mockito.doAnswer(answer)
                    .when(mockedApi.usersApi)
                    .getProfileImage(
                            Mockito.eq(userId), any(ResultListener.class));
        }


        @SuppressWarnings("ResourceType")
        public EventViewBinderSteps the_default_views_should_be_saved_by_the_binder() {
            verify(layoutMockup.getViewsRoot()).findViewById(R.id.view_event_message);
            verify(layoutMockup.getViewsRoot()).findViewById(R.id.view_event_date);
            verify(layoutMockup.getViewsRoot()).findViewById(R.id.view_user_thumbnail);

            return this;
        }

        public EventViewBinderSteps an_event_message_should_be_set_to_view() {
            layoutMockup.<TextView>verifyView(R.id.view_event_message).setText(Mockito.eq(event.getMessage()));

            return this;
        }

        public EventViewBinderSteps an_event_date_message_should_be_set_to_view() {
            ArgumentCaptor<CharSequence> textCaptor = ArgumentCaptor.forClass(CharSequence.class);

            layoutMockup.<TextView>verifyView(R.id.view_event_message).setText(textCaptor.capture());

            assertThat(textCaptor.getValue().toString(), not(isEmptyOrNullString()));

            return this;
        }


        public EventViewBinderSteps the_default_user_image_should_be_put_on_image_view() {
            layoutMockup.<ImageView>verifyView(R.id.view_user_thumbnail)
                    .setImageDrawable(any(Drawable.class));

            verify(userImageViewBinder).genTextDrawable(any(User.class));

            return this;
        }

        public EventViewBinderSteps the_user_profile_image_should_be_put_on_image_view() {
            layoutMockup.<ImageView>verifyView(R.id.view_user_thumbnail)
                    .setImageBitmap(bitmapMockup);
            return this;
        }
    }
}

