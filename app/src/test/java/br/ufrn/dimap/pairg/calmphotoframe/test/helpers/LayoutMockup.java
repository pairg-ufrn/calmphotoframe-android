package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import android.content.Context;
import android.view.View;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;
import java.util.Map;

public class LayoutMockup {

    @Mock
    View rootView;

    @Mock
    Context contextMockup;

    Map<Integer, GetViewAnswer> idToMockup;

    public LayoutMockup(){
        MockitoAnnotations.initMocks(this);

        idToMockup = new HashMap<>();
    }

    public Context getContextMockup() {
        return contextMockup;
    }

    public void setContextMockup(Context contextMockup) {
        this.contextMockup = contextMockup;
    }

    public void addView(int id, final Class<? extends View> viewType){
        GetViewAnswer answer = new GetViewAnswer(viewType);

        idToMockup.put(id, answer);

        View v = answer.getView();
        Mockito.when(v.getContext()).then(new Answer<Context>() {
            @Override
            public Context answer(InvocationOnMock invocation) throws Throwable {
                return getContextMockup();
            }
        });

        Mockito.when(rootView.findViewById(id))
                .thenAnswer(answer);
    }

    public View getViewsRoot() {
        return rootView;
    }

    public View getView(int nameView) {
        GetViewAnswer answer = idToMockup.get(nameView);
        return answer == null ? null : answer.getView();
    }

    public <T> T verifyView(int viewId){
        @SuppressWarnings("unchecked")
        T view = (T)getView(viewId);

        return Mockito.verify(view);
    }

    public static class GetViewAnswer implements Answer<View> {
        private View mockView;

        public GetViewAnswer(Class<? extends View> viewType) {
            this.mockView = Mockito.mock(viewType);
        }

        public  View getView(){
            return mockView;
        }

        @Override
        public View answer(InvocationOnMock invocation) {
            return mockView;
        }
    }
}
