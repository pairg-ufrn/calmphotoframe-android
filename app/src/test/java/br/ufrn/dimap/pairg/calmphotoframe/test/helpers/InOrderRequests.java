package br.ufrn.dimap.pairg.calmphotoframe.test.helpers;

import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.OngoingStubbing;
import org.mockito.verification.VerificationMode;

import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.Listeners;
import br.ufrn.dimap.pairg.calmphotoframe.controller.commands.ResultListener;
import br.ufrn.dimap.pairg.calmphotoframe.photoframe.api.photocollection.PhotoCollectionsApi;
import br.ufrn.dimap.pairg.calmphotoframe.utils.Logging;

public class InOrderRequests<T> {

    InOrder inOrderCalls;
    T mockup;
    CaptureAnswer captureRequest;

    int finishedRequestsCount = 0, successCount=0;

    public InOrderRequests(T mockup) {
        this.mockup = mockup;
        inOrderCalls = Mockito.inOrder(mockup);
        captureRequest = new CaptureAnswer<>();
    }

    public InOrderRequests<T> requestResponse(Object response){
        captureRequest.setReturnValue(response);
        return this;
    }

    public InOrderRequests<T> capture(OngoingStubbing<?> stubbing){
        stubbing.thenAnswer(captureRequest);
        return this;
    }

    public T verify(){
        return inOrderCalls.verify(mockup);
    }
    public T verify(VerificationMode verificationMode){
        return inOrderCalls.verify(mockup, verificationMode);
    }

    public CaptureAnswer getCaptureRequest() {
        return captureRequest;
    }

    public int getFinishCount() {
        return finishedRequestsCount;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void succeedRequests(int requestsToFinish, Object content) {
        finishRequests(requestsToFinish, content, null);
    }

    public void failRequests(int count, Throwable throwable) {
        finishRequests(count, null, throwable);
    }

    public void finishRequests(int requestsToFinish, Object result, Throwable error) {
        int startIdx = finishedRequestsCount, endIdx = startIdx + requestsToFinish;

        Logging.d(getClass().getSimpleName(), "finishRequests(" + requestsToFinish + ", " + result + ", " + error + ")");

        for (int i=startIdx; i < endIdx; ++i){
            ResultListener listener = getListener(i);
            ++finishedRequestsCount;

            if(error != null){
                Listeners.onFailure(listener, error);
            }
            else {
                ++successCount;
                Listeners.onResult(listener, result);
            }
        }
    }

    public ResultListener getListener(int callIndex) {
        InvocationOnMock invocationOnMock = captureRequest.call(callIndex);
        for(Object arg : invocationOnMock.getArguments()){
            if(arg instanceof ResultListener){
                return (ResultListener)arg;
            }
        }
        return null;
    }
}
