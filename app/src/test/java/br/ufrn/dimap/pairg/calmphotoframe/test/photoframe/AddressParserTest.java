package br.ufrn.dimap.pairg.calmphotoframe.test.photoframe;

import org.junit.Test;

import java.net.InetAddress;

import br.ufrn.dimap.pairg.calmphotoframe.photoframe.model.IPParser;

import static java.net.InetAddress.getByAddress;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class AddressParserTest{

    @Test
    public void testIpv4Address(){
        checkParse("127.0.0.1", 127, 0, 0, 1);
        checkParseV6("A:C00:FEE:FEED:A:dead:B0D1:Beef", 0xA,0xC00,0xFEE,0xFEED,0xA,0xDEAD,0xB0D1,0xBEEF);
        checkParseV6("2605:2700:0:3::4713:93e3", 0x2605, 0x2700, 0, 3, 0, 0,  0x4713, 0x93E3);
        checkParseV6("::1", 0, 0 ,0 , 0 , 0 , 0, 0, 1);
        checkParseV6("1::", 1, 0 ,0 , 0 , 0 , 0, 0, 0);
        checkParseV6("::", 0, 0 ,0 , 0 , 0 , 0, 0, 0);
        checkParseV6("::ffff:192.168.173.22", 0, 0, 0, 0, 0, 0xFFFF, (192 << 8) + 168, (173 << 8) + 22);
    }

    @Test
    public void testConversion(){
        assertThat("FF", Integer.parseInt("FF", 16), equalTo(0xFF));
        assertThat("FEE", Integer.parseInt("FEE", 16), equalTo(0xFEE));
        assertThat("FEED", Integer.parseInt("FEED", 16), equalTo(0xFEED));
        assertThat(Integer.parseInt("FEE", 16) & 0xFF, equalTo(0xEE));

        InetAddress address = IPParser.parse("127.0.0.1");
        assertThat(address, hasProperty("loopbackAddress", equalTo(true)));
    }

    private void checkParseV6(String address, int ... bytePairs) {
        int[] addrBytes = splitBytePairs(bytePairs);

        checkParse(address, addrBytes);
    }

    private int[] splitBytePairs(int[] bytePairs) {
        int[] addrBytes = new int[bytePairs.length * 2];
        for (int i=0; i < bytePairs.length; ++i){
            int num = bytePairs[i];
            int upper = (num & 0xFF00) >> 8;
            int down = (num & 0xFF);
            addrBytes[i*2] = upper;
            addrBytes[i*2 + 1] = down;
        }
        return addrBytes;
    }

    private void checkParse(String address, int ... components) {
        byte[] result = IPParser.parseToBytes(address);

        assertThat("address: " + address, result, is(notNullValue()));
        assertThat("address: " + address, result, equalTo(convertToByteArray(components)));
    }

    private byte[] convertToByteArray(int[] arr) {
        byte[] result = new byte[arr.length];

        for (int i=0; i < arr.length; ++i){
            result[i] = (byte)arr[i];
        }

        return result;
    }

}

