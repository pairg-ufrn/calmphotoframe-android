#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import sys
from os import path

'''
class SilentOutput(ConanOutput):
    def __init__(self, *args, **kwargs):
        super(SilentOutput, self).__init__(None)

    def write(self, *args):
        pass
'''

class AndroidConfigure(object):

    abi_to_conan_arch = {
        'armeabi'    : "armv5",
        'armeabi-v7a': "armv7",
        'arm64-v8a'  : "armv8_64",
        'x86'        : "x86",
        'x86_64'     : "x86_64",
        'mips'       : "mips",
        'mips64'     : "mips64"
    }

    def is_android_build(self):
        return "ANDROID" in os.environ.keys()

    def configure(self, conanfile):
        if not self.is_android_build():
            return

        abi = os.environ.get('ANDROID_ABI', None)

        conanfile.output.info("Configuring for abi: '{}'".format(abi))

        if not abi: #TODO: enhance check for android build
            msg = "Failed to find a valid abi."
            msg += " Define 'ANDROID_ABI' in environment."
            msg += " Ex.: ANDROID_ABI=armeabi conan install."

            conanfile.output.warn(msg)
            return

        arch = self.abi_to_conan_arch.get(abi.lower())
        compiler, compiler_version = self._detect_compiler_and_version(conanfile)

        settings = conanfile.settings
        settings.os = "Android"
        settings.arch = arch
        settings.compiler = compiler
        settings.compiler.version = compiler_version

    def _detect_compiler_and_version(self, conanfile):
        # TODO: silent output
        result = detect.detect_defaults_settings(SilentOutput())

        compiler = None
        compiler_version = None

        for key, value in result:
            if key == "compiler":
                compiler = value
            elif key == "compiler.version":
                compiler_version = value

        return (compiler, compiler_version)
        

def execute_shell(cmd, **kw):
    errcode = subprocess.call(cmd, shell=True, **kw)

    if errcode != 0:
        raise OSError("Command '%s' failed. Error=%d" % (cmd, errcode))
        
def main():
    build_mode = "--build=missing" if "--force" not in sys.argv else "--build"
    
    os.environ['ANDROID']="1"
    os.environ['CONAN_CMAKE_GENERATOR']="Unix Makefiles"
    
    settings = '-s arch={} -s os=Android -s compiler=gcc -s compiler.version=4.9 '  
    conan_cmd = "conan install %s %s" % (settings, build_mode)
    cmd_template = 'droidcross run --abi {} --api-level {api_level} --stl {stl} "%s"' % (conan_cmd, )
        
    stl="gnustl_shared"    
        
    for abi, arch in AndroidConfigure.abi_to_conan_arch.iteritems():
        api_level = 12 if abi not in ["x86_64", "mips64", "arm64-v8a"] else 21

        cmd = cmd_template.format(abi, arch, api_level=api_level, stl=stl)
        
        conaninfo_file = "conaninfo.txt"
        if path.exists(conaninfo_file): 
            os.remove(conaninfo_file)
        
        print("Build for abi %s, api-level %d:" % (abi, api_level))
        print("cmd: %s" % (cmd,))
        
        execute_shell(cmd)
    

if __name__ == "__main__":
    main()
    
    
