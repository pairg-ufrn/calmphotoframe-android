package br.ufrn.dimap.pairg.calmphotoframe.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.AndroidPhotoFrame;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class AndroidPhotoFrameTest {

    private AndroidPhotoFrame photoFrameJNI;

    @Before
    public void setUp() throws Exception {

        Context instrumentationContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        photoFrameJNI = new AndroidPhotoFrame(instrumentationContext);
        photoFrameJNI.startWebService(null);
    }

    @After
    public void tearDown() throws Exception {
        photoFrameJNI.stopWebService();
    }

    public void testPhotoFrameIsRunning() {
        Assert.assertTrue(photoFrameJNI.serverIsRunning());
    }

    public void testStartLocalSession(){
        String localToken = photoFrameJNI.startLocalSession();

        assertThat(localToken, is(not(nullValue())));
        assertThat(localToken, not(isEmptyString()));
    }
}
