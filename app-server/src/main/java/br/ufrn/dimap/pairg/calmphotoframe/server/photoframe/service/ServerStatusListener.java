package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerFinishEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;

public interface ServerStatusListener {
	void onServerStart(ServerStartEvent startEvent);
	void onServerStop(ServerFinishEvent finishEvent);
	boolean isListening();
}