package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe;


import android.util.Log;

public class NativeLibLoader {
    private static final String TAG = NativeLibLoader.class.getName();

    public static void loadLibraries(String[] libraries) {
        for (String lib : libraries){
            try {
                System.loadLibrary(lib);
                Log.i(TAG, String.format("Loaded library (%s)", lib));
            }catch (Throwable throwable){
                String msg = String.format("Failed to load library (%s): \n%s", lib, throwable.getLocalizedMessage());
                Log.w(TAG, msg);
            }
        }
    }
}
