package br.ufrn.dimap.pairg.calmphotoframe.server.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.StartServerRequest;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerFinishEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.ServerStatusListener;

public class ServerActivity extends AppCompatActivity {
    private static final String TAG = ServerActivity.class.getSimpleName();

    StartServerRequest connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        TextView  tv = new TextView(this);
        setContentView(tv);

        connection = new StartServerRequest(this);
        connection.serverListener(new ServerStatusListener() {
            @Override
            public void onServerStart(ServerStartEvent startEvent) {
                Log.i(TAG, "Server started!");
            }

            @Override
            public void onServerStop(ServerFinishEvent finishEvent) {
                Log.i(TAG, "Server stopped!");
            }

            @Override
            public boolean isListening() {
                return true;
            }
        });
        connection.start();

    }
}
