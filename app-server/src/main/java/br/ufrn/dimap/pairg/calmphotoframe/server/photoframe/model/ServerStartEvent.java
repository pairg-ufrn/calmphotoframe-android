package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model;

public class ServerStartEvent {
    String connectionToken;

    public ServerStartEvent(String connectionToken) {
        this.connectionToken = connectionToken;
    }

    public String getConnectionToken() {
        return connectionToken;
    }
}
