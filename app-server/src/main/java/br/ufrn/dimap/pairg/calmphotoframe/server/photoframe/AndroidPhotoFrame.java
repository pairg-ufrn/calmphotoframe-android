package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe;

import java.io.File;
import java.util.Map;

import android.content.Context;
import android.os.Environment;

public class AndroidPhotoFrame extends NativePhotoFrame {
	private Context context;
	
	public AndroidPhotoFrame(Context context) {
		this.context = context.getApplicationContext();
	}
	
	@Override
	protected void putDefaultOptions(Map<String, String> serverOptions) {
		super.putDefaultOptions(serverOptions);

		serverOptions.put(OPTION_DOC_ROOT	, getMediaDir());
		serverOptions.put(OPTION_TMP_DIR	, getTmpDir());
		serverOptions.put(OPTION_IMAGE_DIR	, getImageDir());
		serverOptions.put(OPTION_DATA_DIR	, context.getFilesDir().getAbsolutePath());
		
		serverOptions.put(OPTION_UNSAFECOOKIE, "true");
	}

	private String getTmpDir() {
		File tmpDirFile = isExternalStorageAvailable() ? context.getExternalCacheDir() : context.getCacheDir();
		return tmpDirFile.getAbsolutePath();
	}
	private String getImageDir() {
		File imageDirFile = isExternalStorageAvailable() 
										? context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) 
										: context.getFilesDir();
		return imageDirFile.getAbsolutePath();
	}
	private String getMediaDir() {
		File imageDir = context.getExternalFilesDir(null);
		if(imageDir == null){
			imageDir = context.getFilesDir();
		}
		return imageDir.getAbsolutePath();
	}
	
	private boolean isExternalStorageAvailable() {
	    String state = Environment.getExternalStorageState();
	    return Environment.MEDIA_MOUNTED.equals(state);
	}

}
