package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.PhotoFrameControllerService;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerFinishEvent;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.model.ServerStartEvent;

/** A BroadcastReceiver that listen for notifications from PhotoFrameControllerService*/
public class PhotoFrameBroadcastReceiver extends BroadcastReceiver
{
	private IntentFilter startActionIntentFilter , stopActionIntentFilter;
	private ServerStatusListener serverListener;

	public PhotoFrameBroadcastReceiver() {
		this(null);
	}
	public PhotoFrameBroadcastReceiver(ServerStatusListener listener) {
		this.serverListener = listener;
		startActionIntentFilter = new IntentFilter(PhotoFrameControllerService.START_SERVER_ACTION);
		stopActionIntentFilter = new IntentFilter(PhotoFrameControllerService.STOP_SERVER_ACTION);
	}
	
    public ServerStatusListener getServerListener() {
		return serverListener;
	}
	public void setServerListener(ServerStatusListener serverListener) {
		this.serverListener = serverListener;
	}

	@Override
    public void onReceive(Context context, Intent intent) {
		if(getServerListener() == null){
			return;
		}
    	if(intent.getAction().equals(PhotoFrameControllerService.START_SERVER_ACTION)){
			getServerListener().onServerStart(extractStartEvent(intent));
    	}
    	else if(intent.getAction().equals(PhotoFrameControllerService.STOP_SERVER_ACTION)){
    		getServerListener().onServerStop(extractFinishEvent(intent));
    	}
    }

	private ServerStartEvent extractStartEvent(Intent intent) {
		String token = intent.getStringExtra(PhotoFrameControllerService.EXTRA_LOCALTOKEN);
		return new ServerStartEvent(token);
	}

	@SuppressWarnings("UnusedParameters")
    private ServerFinishEvent extractFinishEvent(Intent intent) {
		return new ServerFinishEvent();
	}

	public void register(Context context){
        // Register receiver
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
		if(broadcastManager != null){
            this.unregister(context);
			broadcastManager.registerReceiver(this, startActionIntentFilter);
			broadcastManager.registerReceiver(this, stopActionIntentFilter);
		}
    }
    public void unregister(Context context){
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
    }
}