package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameBroadcastReceiver;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.ServerStatusListener;

/**
 */
public class StartServerRequest {
    private Context context;
    private PhotoFrameBroadcastReceiver receiver;

    public StartServerRequest(Context context)
    {
        this.context = context;
        receiver = new PhotoFrameBroadcastReceiver();
    }

    public void start() {
        this.start(new Bundle());
    }

    public void start(Bundle arguments) {
        receiver.register(context);

        context.startService(buildStartServiceIntent(arguments));
    }

    public StartServerRequest serverListener(ServerStatusListener listener){
        this.receiver.setServerListener(listener);

        return this;
    }

    @NonNull
    private Intent buildStartServiceIntent(Bundle arguments) {
        Intent intent = new Intent(context, PhotoFrameControllerService.class);
        intent.setAction(PhotoFrameControllerService.START_SERVER_ACTION);
        if (arguments != null && !arguments.isEmpty()) {
            intent.putExtras(arguments);
        }
        return intent;
    }
}
