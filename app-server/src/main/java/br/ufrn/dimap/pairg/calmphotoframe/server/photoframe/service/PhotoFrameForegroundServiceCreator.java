package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.RemoteViews;

import br.ufrn.dimap.pairg.calmphotoframe.server.R;
import br.ufrn.dimap.pairg.calmphotoframe.server.activities.ServerActivity;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.PhotoFrameControllerService;

public class PhotoFrameForegroundServiceCreator {
	private static PhotoFrameForegroundServiceCreator singleton;
	public static PhotoFrameForegroundServiceCreator instance(){
		if(singleton == null){
			singleton = new PhotoFrameForegroundServiceCreator();
		}
		return singleton;
	}
	public Notification createForegroundServerNotification(Context context) {
		
		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context);
		
		/*smallIcon é necessário para notification aparecer.
		 * TODO: alterar ícone do serviço (?)*/
		notifBuilder.setSmallIcon(R.mipmap.ic_launcher);
		notifBuilder.setContent(createServiceNotificationView(context));
		notifBuilder.setContentIntent(createServiceSettingsPendingIntent(context));
		
		return notifBuilder.build();
	}
	/** TODO: deveria criar um intent que levasse a uma tela de configuração/gerenciamento do serviço */
	private PendingIntent createServiceSettingsPendingIntent(Context context) {
		Class<ServerActivity> activityClass = ServerActivity.class;
		// Creates an explicit intent for an Activity in your app
		Intent contentIntent = new Intent(context, activityClass);
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(activityClass);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(contentIntent);

		return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT );
	}
	private RemoteViews createServiceNotificationView(Context context) {
		
		RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_service);
		contentView.setImageViewResource(R.id.notification_image, R.mipmap.ic_launcher);
		contentView.setTextViewText(R.id.notification_title, context.getResources().getString(R.string.service_notification_title));
		contentView.setImageViewResource(R.id.close_icon, R.drawable.ic_close);
		
		PendingIntent pendingCloseIntent = createCloseServicePendingIntent(context);
		contentView.setOnClickPendingIntent(R.id.close_icon, pendingCloseIntent);
		
		return contentView;
	}
	private PendingIntent createCloseServicePendingIntent(Context context) {
		Intent closeServiceIntent = new Intent(context,PhotoFrameControllerService.class);
		closeServiceIntent.setAction(PhotoFrameControllerService.STOP_SERVER_ACTION);
		
		return PendingIntent.getService(context, 0, closeServiceIntent, 0);
	}
}
