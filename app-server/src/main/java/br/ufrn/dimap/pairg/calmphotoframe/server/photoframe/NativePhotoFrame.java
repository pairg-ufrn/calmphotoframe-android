package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import static br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.NativeLibLoader.loadLibraries;

public class NativePhotoFrame {
	//Core Options
	public static final String OPTION_DATA_DIR     	= "data_dir";
	public static final String OPTION_IMAGE_DIR    	= "image_dir";
	public static final String OPTION_TMP_DIR 	   	= "tmp_dir";
	public static final String OPTION_DOC_ROOT		= "document_root";
	public static final String OPTION_UNSAFECOOKIE 	= "unsafe_cookie";
	public static final String OPTION_BASE_DIR 		= "base_dir";
	
    static final String TAG = NativePhotoFrame.class.getSimpleName();
    
	static final String[] nativeLibraries = {
			"gnustl_shared",
			"calmphotoframe",
            "native-photoframe"
	};

	static{
		loadLibraries(nativeLibraries);
	}

    private final String secret;
    
    public NativePhotoFrame(){
        secret = generateSecret();
    }

    private String generateSecret() {
		return String.valueOf(System.currentTimeMillis());
	}

    public synchronized void startWebService(final Map<String, String> options){
        if(!serverIsRunning()){
            Log.i(TAG,"log before start server");
            Map<String, String> serverOptions = new HashMap<>();
            putDefaultOptions(serverOptions);
            overwriteOptions(serverOptions, options);
            
        	String[] keys = null;
        	String[] values = null;
        	
        	if(serverOptions != null && !serverOptions.isEmpty()){
                Log.i(TAG,"start server with configs.");

            	keys = new String[serverOptions.size()];
            	values = new String[keys.length];
            	int i =0;
            	for(String key : serverOptions.keySet()){
            		keys[i] = key;
            		values[i] = serverOptions.get(key);
            		++i;
            	}
        	}
        	
        	startServer(keys,values);
            Log.i(TAG,"log after start server");
        }
    }
    private void overwriteOptions(final Map<String, String> serverOptions, final Map<String, String> options) {
    	if(options != null && !options.isEmpty()){
    		for(Entry<String, String> entry : options.entrySet()){
    			serverOptions.put(entry.getKey(), entry.getValue());
    		}
    	}
	}

	protected void putDefaultOptions(Map<String, String> serverOptions) {
		//Subclasses can override this method
	}

	public synchronized void stopWebService(){
        if(serverIsRunning()){
            stopServer();
            Log.i(TAG,"finished photo frame server");
        }
    }
    
    public synchronized boolean serverIsRunning(){
    	return isRunning();
    }

    /** Start session with the local server.
     *
     * @return the token created for this session
     * */
	public synchronized String startLocalSession(){
		return startLocalSession(secret);
	}
    public native synchronized String getOption(String key);
    
	private native String startLocalSession(String clientSecret);
    private native void startServer(String[] keys, String[] values);
    private native void stopServer();
    private native boolean isRunning();
}
