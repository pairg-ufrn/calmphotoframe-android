package br.ufrn.dimap.pairg.calmphotoframe.server.photoframe;

import java.util.HashMap;
import java.util.Map;

import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.LongLivedIntentService;
import br.ufrn.dimap.pairg.calmphotoframe.server.photoframe.service.PhotoFrameForegroundServiceCreator;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class PhotoFrameControllerService extends LongLivedIntentService{

	private static final String CLASS = PhotoFrameControllerService.class.getName();
	public static final String START_SERVER_ACTION = CLASS + ".START";
	public static final String STOP_SERVER_ACTION  = CLASS + ".STOP";
    public static final String EXTRA_LOCALTOKEN = CLASS + ".LOCALTOKEN";

	private static final int SERVER_NOTIFICATION_ID = 1;
	public static final String TAG = PhotoFrameControllerService.class.toString();

	private AndroidPhotoFrame photoFrameController = null;

    @Override
	public void onCreate() {
		super.onCreate();	    
	    
		photoFrameController = new AndroidPhotoFrame(this);
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroyService");
		
		stopServerCommand();
	}
	
	@Override
	protected void onHandleIntent(Intent intent, int startId) {
		Log.d(this.getClass().toString(), "Handle Message " + startId);
		
		if(intent != null){
			String action = intent.getAction();
			if(action == null){
				return;
			}

			if(action.equals(START_SERVER_ACTION)){
				startServerCommand(intent.getExtras());
			}
			else if(action.equals(STOP_SERVER_ACTION)){
				stopServerCommand();
				stopSelf();
			}
		}
		else{
			/*TODO: lidar com serviço reiniciando. Ao reiniciar, serviço recebe intent nulo.*/
		}
	}

	protected void startServerCommand(Bundle arguments){
		if(!this.photoFrameController.serverIsRunning()){
			Map<String, String> options = extractOptions(arguments);
			photoFrameController.startWebService(options);
			startForegroundService();
		}
		this.notifyServerStarted();
	}

	private Map<String, String> extractOptions(Bundle arguments) {
		if(arguments == null || arguments.isEmpty()){
			arguments = new Bundle();
		}
		
		return getOptions(arguments);
	}
	private void startForegroundService() {
		Notification foregroundNotification = PhotoFrameForegroundServiceCreator.instance()
													.createForegroundServerNotification(this);
		startForeground(SERVER_NOTIFICATION_ID, foregroundNotification);
	}
	
	/** TODO: talvez obter 'arguments de algum tipo de configuração'*/
	private Map<String, String> getOptions(Bundle arguments) {
		Map<String, String> options = new HashMap<>();
		for(String key : arguments.keySet()){
			options.put(key, arguments.get(key).toString());
		}
		
		return options;
	}
	protected void stopServerCommand(){
		Log.d(TAG, "stop server command!");
		if(this.photoFrameController.serverIsRunning()){
			photoFrameController.stopWebService();
			if(!this.photoFrameController.serverIsRunning()){
				//Se parou servidor notifica
				this.notifyFinishService();
			}
		}
	}

	private void notifyServerStarted() {
        String token = photoFrameController.startLocalSession();

	    Intent localIntent = new Intent(START_SERVER_ACTION);
	    localIntent.putExtra(EXTRA_LOCALTOKEN, token);
	    
		//TODO: (?) talvez enviar broadcast não apenas localmente
	    LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
	    
	    Log.d(this.getPackageName(), "Sent broadcast with action: " + localIntent.getAction());
	}
	private void notifyFinishService() {
	    Intent localIntent = new Intent(STOP_SERVER_ACTION);

		//TODO: (?) talvez enviar broadcast não apenas localmente
	    LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
	    
	    Log.d(this.getPackageName(), "Sent broadcast with action: " + localIntent.getAction());
	}
}
