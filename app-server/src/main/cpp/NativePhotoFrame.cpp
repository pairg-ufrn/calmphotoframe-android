
#include <jni.h>
#include <map>
#include <vector>

#include "PhotoFrameService.h"
#include "Options.h"
#include "Configurations.h"
#include "utils/Log.h"

#include "data/Data.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
//#include "data/data-memory/MemoryDataBuilder.hpp"

using namespace std;
using namespace calmframe;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

PhotoFrameService & getLocalPhotoFrame(){
    static PhotoFrameService service;
    return service;
}

void loadConfigurations(JNIEnv * env, jobjectArray &  stringKeys, jobjectArray & stringValues,
                        Configurations & configs)
{
    if(stringKeys != NULL){
        int stringCount = env->GetArrayLength(stringKeys);

        for (int i=0; i<stringCount; i++) {
            jstring jKey = (jstring) env->GetObjectArrayElement( stringKeys, i);
            jstring jValue = (jstring) env->GetObjectArrayElement( stringValues, i);
            const char * key = env->GetStringUTFChars(jKey, 0);
            const char * value = env->GetStringUTFChars(jValue, 0);

            logInfo("getting option: [%s --> %s]", key,value);

            configs.put(key, std::string(value));

            env->ReleaseStringUTFChars(jKey, key);
            env->ReleaseStringUTFChars(jValue, value);
        }
    }
}

string toCppString(JNIEnv * env, jstring jStr){
    const char * cStr = env->GetStringUTFChars(jStr, 0);

    string str(cStr);

    env->ReleaseStringUTFChars(jStr, cStr);

    return str;
}

//Declaring JNI macros with a default value to remove false warnings from eclipse
#ifndef JNIEXPORT
#define JNIEXPORT
#endif
#ifndef JNICALL
#define JNICALL
#endif

JNIEXPORT jstring JNICALL Java_br_ufrn_dimap_pairg_calmphotoframe_server_photoframe_NativePhotoFrame_startLocalSession
        (JNIEnv * env, jobject thisObj, jstring jclientSecret){
    string clientSecret = toCppString(env, jclientSecret);
    string token = getLocalPhotoFrame().startLocalSession(clientSecret);
    logInfo("Started local session");
    return env->NewStringUTF(token.c_str());
}

JNIEXPORT
void JNICALL Java_br_ufrn_dimap_pairg_calmphotoframe_server_photoframe_NativePhotoFrame_startServer
        (JNIEnv * env, jobject thisObj, jobjectArray stringKeys, jobjectArray stringValues)
{
    MainLog().setLogLevel(LogLevel::Debug);
    Configurations & configs = Configurations::instance();

    loadConfigurations(env, stringKeys, stringValues, configs);

    logInfo("Loaded configs");

    string dataDir = configs.get(Options::DATA_DIR);
    logInfo("PhotoFrame %s: %s", Options::DATA_DIR, dataDir.c_str());

    calmframe::data::SqliteDataBuilder dataBuilder(dataDir);
    calmframe::data::Data::setup(dataBuilder);

    PhotoFrameService * service = &getLocalPhotoFrame();
    if(!service->startService(configs)){
        logInfo("Failed to start service");
        //delete service;
        service = NULL;
    }
}


JNIEXPORT
void JNICALL Java_br_ufrn_dimap_pairg_calmphotoframe_server_photoframe_NativePhotoFrame_stopServer
        (JNIEnv * env, jobject thisObj)
{
    getLocalPhotoFrame().stopService();
}

JNIEXPORT
jboolean JNICALL Java_br_ufrn_dimap_pairg_calmphotoframe_server_photoframe_NativePhotoFrame_isRunning(){
    return getLocalPhotoFrame().isRunning();
}

JNIEXPORT jstring JNICALL Java_br_ufrn_dimap_pairg_calmphotoframe_server_photoframe_NativePhotoFrame_getOption
        (JNIEnv * env, jobject thisObj, jstring jkeyName){
    const char * keyName = env->GetStringUTFChars(jkeyName, 0);

    std::string optValue = Configurations::instance().get(optValue);

    env->ReleaseStringUTFChars(jkeyName, keyName);
    return env->NewStringUTF(optValue.c_str());
}

#ifdef __cplusplus
}
#endif // __cplusplus
