from conans import ConanFile, CMake, tools

import os
from os import path

from conans.client import detect
from conans.client.output import ConanOutput

'''

class SilentOutput(ConanOutput):
    def __init__(self, *args, **kwargs):
        super(SilentOutput, self).__init__(None)

    def write(self, *args):
        pass

class AndroidConfigure(object):

    abi_to_conan_arch = {
        'armeabi'    : "armv5",
        'armeabi-v7a': "armv7",
        'arm64-v8a'  : "armv8_64",
        'x86'        : "x86",
        'x86_64'     : "x86_64",
        'mips'       : "mips",
        'mips64'     : "mips64"
    }

    def is_android_build(self):
        return "ANDROID" in os.environ.keys()

    def configure(self, conanfile):
        if not self.is_android_build():
            return

        abi = os.environ.get('ANDROID_ABI', None)

        conanfile.output.info("Configuring for abi: '{}'".format(abi))

        if not abi: #TODO: enhance check for android build
            msg = "Failed to find a valid abi."
            msg += " Define 'ANDROID_ABI' in environment."
            msg += " Ex.: ANDROID_ABI=armeabi conan install."

            conanfile.output.warn(msg)
            return

        arch = self.abi_to_conan_arch.get(abi.lower())
        compiler, compiler_version = self._detect_compiler_and_version(conanfile)

        settings = conanfile.settings
        settings.os = "Android"
        settings.arch = arch
        settings.compiler = compiler
        settings.compiler.version = compiler_version

    def _detect_compiler_and_version(self, conanfile):
        # TODO: silent output
        result = detect.detect_defaults_settings(SilentOutput())

        compiler = None
        compiler_version = None

        for key, value in result:
            if key == "compiler":
                compiler = value
            elif key == "compiler.version":
                compiler_version = value

        return (compiler, compiler_version)

'''

class PmsandroidserverConan(ConanFile):
    name = "pms-android-server"
    version = "0.0.1"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "txt"

    requires = (
        "calmphotoframe/0.5.1@paulobrizolara/experimental"
    )
    
    default_options = '''
    '''
#    def configure(self):
#        androidConf = AndroidConfigure()
#        androidConf.configure(self)
#
#        self.output.info("settings fields: {}".format(self.settings.fields))
#
#        for f in self.settings.fields:
#            self.output.info("{} -> {}".format(f, getattr(self.settings, f)))
#
#        self.output.info("compiler version: {}".format(self.settings.compiler.version))


    def imports(self):
        abi = os.environ.get("ANDROID_ABI", "native")
        libs_dir = path.join("native-libs", "lib", abi)
        inc_dir = path.join("native-libs", "include")

        import shutil
        self.output.info("Removing libs dir: %s" % libs_dir)
        shutil.rmtree(libs_dir, ignore_errors=True)
        os.makedirs(libs_dir)

        self.copy("*.a", dst=libs_dir, src="lib")
        self.copy("*.so.*", dst=libs_dir, src="lib")
        self.copy("*.so", dst=libs_dir, src="lib")
        
        self.copy("*.h", dst=inc_dir, src="include")
        self.copy("*.hpp", dst=inc_dir, src="include")
        
        
